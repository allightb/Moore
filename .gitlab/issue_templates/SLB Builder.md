# Builder development/change request

## Motivation

Outline of what is missing/wrong and a description of the changes needed.

## Sketch 

There can be cases where new or significantly different builders from the available codebase might be needed. If this is the case, please sketch out what kind of builder is needed for your development (_e.g._ cut variables, extra inputs etc.)

## Contact(s)

Author(s), listed with `@` handle.

## Checklist

- [ ] Assign other authors, if any.
- [ ] When ready, review and test the latest version of the implemeneted changes against the latest version of Moore.
- [ ] If all tests have passed **close this issue**.

/assign me 

/label ~"SLB Builder"

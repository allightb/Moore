# Report of a crash/segfault with Moore

## Brief summary of encountered problem

Minimal and factual description of what you were trying to do and where the crash occured. 

A logfile of the crash should be attached to the issue.

## Dataset you were running on

## Moore version used (or nightly, as appropriate)

Please include the platform Moore was compiled for

Including branch name if available and up-to-date with respect to the crash.

If you have not committed code recently and it would therefore take time to create a branch tag which can be used to reproduce your crash, please prioritise reporting the crash immediately. A maintainer will get in touch when they are available to investigate it and ask you to provide more information as needed.

## Option file used to execute Moore 

## Additional information

/assign me 

/label ~"Crash"


2022-01-27 Moore v53r4
===

This version uses
Allen [v1r9](../../../../Allen/-/tags/v1r9),
Rec [v34r0](../../../../Rec/-/tags/v34r0),
Lbcom [v33r6](../../../../Lbcom/-/tags/v33r6),
LHCb [v53r6](../../../../LHCb/-/tags/v53r6),
Gaudi [v36r4](../../../../Gaudi/-/tags/v36r4) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Moore [v53r3](/../../tags/v53r3), with the following changes:

### New features ~"new feature"

- ~selection | Generic charm builders; refactor and revisit charm/hyperons selections, !1221 (@mstahl)
- ~selection ~hlt2 | B_to_v0ll rare decay Hlt2 lines, !1209 (@hvc)
- ~selection ~hlt2 | B2OC: Add 308 new lines, enhancements and fixes, !1199 (@shunan) [lhcb-dpa/project#78]
- ~selection ~hlt2 | RD common builders in the ThOr framework, !1150 (@vlisovsk)
- ~hlt2 | Triggerlines for track reconstruction efficiency studies: add lines without UT and migrate to ThOr, !1236 (@rowina)
- ~hlt2 | Updating SL lines with ThOr functors and adding new lines, !1151 (@sklaver)
- ~hlt2 | Add a mue builder that applies brem correction and extend the dielectron with brem builder to access full information, !1022 (@tmombach)
- ~hlt2 ~"Flavour tagging" | B2ll lines and B2llgamma, !1094 (@tmombach)
- ~Calo ~PID | Setup of track-based electron shower algorithm in calo seq, !901 (@mveghel)
- Add the new Event Data to json format Algorithms, !993 (@anpappas)


### Fixes ~"bug fix" ~workaround

- ~hlt2 ~Configuration | Fix usage of PrAddUTHitsTool in explicitly UT-less tracking sequence, !1242 (@gunther) [gaudi/Gaudi#214]
- ~Configuration ~Calo ~Monitoring | Fix conflict between !1173 and !1175, !1198 (@rmatev)


### Enhancements ~enhancement

- ~hlt2 ~Tracking | Allow to disable get_global_ut_hits_tool by property, !1218 (@gunther)
- ~Configuration | Change config.py to avoid problems with online output, !1196 (@fsouzade)
- ~Configuration ~Calo | Return v1 and v2 calo objects in `make_calo`, !1215 (@cmarinbe) [#267]
- ~Configuration ~Persistency | Use rawbank views for sprucing, !1137 (@nskidmor)
- ~Functors | Feat: add doc autogeneration for functor helper functions, !1247 (@chasse)
- ~"MC checking" | Extending the options to run without MC hits to exclude also PrHitsMonitors, !1201 (@sklaver)
- ~Build | Disable some tests for the sanitizer builds, !1267 (@rmatev)
- Add Address killer for Sprucing, !1253 (@nskidmor)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection ~hlt2 | Add tests to check hlt2 output files, !1158 (@fsouzade)
- ~hlt1 ~Decoding ~"Event model" ~Persistency | Added a TOS test of SelReports for two-track Allen lines, !1180 (@spradlin) [Allen#252]
- ~Configuration | Remove leftovers from debugging (follow up !1137), !1251 (@rmatev)
- ~"MC checking" | Potentially fix bug in configuration of TrackResChecker, !1015 (@sstahl)
- Add another warning from ThorBPVLTIME to the exclusion list, !1266 (@graven) [Rec#265]
- Adapt to merge of Phys into Rec, !1250 (@rmatev)
- Create dedicated sprucing test lines, !1144 (@ngrieser)
- Update References for: Rec!2319, Moore!901 based on lhcb-master-mr/3449, !1262 (@lhcbsoft)
- Update References for: LHCb!3348, Rec!2636 based on lhcb-master-mr/3340, !1214 (@lhcbsoft)
- Update References for: Rec!2657 based on lhcb-master-mr/3329, !1213 (@lhcbsoft)
- Update References for: Rec!2644, Moore!1015 based on lhcb-master-mr/3277, !1206 (@lhcbsoft)
- Update References for: LHCb!3333, Rec!2628, Phys!1018 based on lhcb-master-mr/3268, !1203 (@lhcbsoft)


### Documentation ~Documentation

- HltEfficiencyChecker doc changes for MooreAnalysis!66, !1259 (@rjhunter)
- Add a template for reporting crashes in Moore, !1252 (@gligorov)
- Add information about reconfiguring Moore after adding new tests., !1204 (@ngrieser)
- Update ganga.rst to use a current platform, !1197 (@gciezare)
- Update standard_loki_thor_table.csv, !1185 (@jiew)

2018-02-15 Moore v26r6p1
========================================

Wrap-up release for 2017
----------------------------------------
Based on Gaudi v28r2, LHCb v42r6p1, Lbcom v20r6p1, Rec v21r6p1, Phys v23r7p1, Hlt v26r6p1.
This version is released on the 2017-patches branch.

### Imported packages
The `Phys/LoKiHlt` and `Phys/LoKiCore` are imported from LHCb/2017-patches in
order to pick up the unreleased lhcb/LHCb!948
No other changes are picked up, as seen by the output of
```
git diff LHCb/v42r6p1..9c04421cbda58e6dde4c26c609fc153080f8274b Phys/LoKiHlt Phys/LoKiCore
```

### Enhancements
- Improve filtering and writing with L0App, !120 (@rmatev)
- Propagate Moore's Simulation property to HltConf, !113 (@apearce)

### Bug fixes
- Allow configuration to override INFO level default., !125 (@apearce)

### Cleanup and testing
- Fix verbosity of hlt2prcheck test, !126 (@rmatev)
- Fix various tests, !119 (@rmatev)
- Add test to print L0-HLT1-HLT2-streams flow, !118 (@rmatev)
- Add exclusions and --tck option to diff-existing, !112 (@rmatev)
- Catch exceptions in timing table validator code, !106 (@rmatev)

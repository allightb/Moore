2022-10-07 Moore v54r0
===

This version uses
Allen [v3r0](../../../../Allen/-/tags/v3r0),
Rec [v35r0](../../../../Rec/-/tags/v35r0),
Lbcom [v34r0](../../../../Lbcom/-/tags/v34r0),
LHCb [v54r0](../../../../LHCb/-/tags/v54r0),
Detector [v1r4](../../../../Detector/-/tags/v1r4),
Gaudi [v36r7](../../../../Gaudi/-/tags/v36r7) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Moore [v53r7](/../../tags/v53r7), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | SigmaNet implementation in the Topological Trigger, !1585 (@nschulte)
- ~selection ~hlt2 | Multimuon HLT2 and Sprucing lines, !1077 (@tmombach)
- ~Tracking | Fit upstream tracks by PrKalmanFilter, !1721 (@ausachov)
- ~Tracking ~Monitoring | Add tests and enable configuration for sensor masking of VELO tracking, !1698 (@ldufour)
- ~Persistency | Require an explicit tck/encoding key to configure encoding, optional stable PersistReco locations, !1529 (@graven)


### Enhancements ~enhancement

- ~Calo | Make_cluster_shapes with GraphClustering, !1549 (@pagarcia)
- ~Persistency | Make packing configuration simpler, !1638 (@sesen)
- ~Monitoring | Adapt monitoring to take unfitted Velo tracks for PV monitoring, !1786 (@decianm)
- ~Monitoring | Add DecReportsMonitor to HLT2, !1780 (@sstahl)
- ~Monitoring | Split track types for monitoring and physics, add fitting (on demand) for monitoring, !1715 (@decianm)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection | Fix Hlt2/Sprucing rate tests, !1784 (@shunan) [#462]
- ~hlt1 | Fixed hlt1 tests after renaming forward tracks to long tracks, !1733 (@cagapopo)
- ~hlt2 | HLT2 sequence without PID, !1722 (@decianm)
- ~Tracking | Fix tracking efficiency line reference logs, !1736 (@jonrob)
- ~Tracking | Fix regular expression to remove UT track eff lines in no-UT scenario, !1728 (@decianm)
- ~Tracking ~RICH | Add no UT tracking and RICH only test, !1716 (@jonrob)
- ~Muon | Add no UT Calo reco tests, !1788 (@jonrob)
- ~Calo | Adapt make_xcal_digits detector location to dd4hep, !1785 (@cmarinbe)
- ~Calo | Exclude not needed dd4hep sub-dets in 'calo-only' test, !1783 (@jonrob)
- ~Persistency | Enable checksum tests, !1797 (@sesen) [#414]
- ~"MC checking" | Fix hit categories to get correct hit efficiency, !1764 (@decianm)
- ~Monitoring | Line monitoring: pass variables to monitor instead of boolean, !1753 (@mstahl)
- ~Build | Ignore PDG in docs linkcheck, !1741 (@rmatev)
- ~Build | Fix documentation CI jobs, !1729 (@rmatev) [#461]
- Update refs for LHCb!3226 and test cleanup, !1807 (@rmatev)
- Reopen Moore!1772 (Tests for GaudiAllen), !1800 (@mstahl)
- Follow up !1797, !1799 (@rmatev)
- Tests for GaudiAllen, !1772 (@mstahl)
- Configuration example for PrParameterisationData, !1781 (@gunther)
- Drop "Line" suffix in line names, !1730 (@mstahl) [#134]
- Fix CUDA build, !1773 (@raaij)
- Fix test_hlt2_thor_selections test in v3 platforms., !1757 (@rmatev)
- Remove exlusion of functor factory warning from hlt2 monitor, !1756 (@gunther)
- Add tests for reco without UT and Muon, but with Calo and RICH, !1743 (@decianm)
- Refactor(CODEOWNERS) remove @chasse, !1739 (@chasse)
- Allow 'resolution' tests to run again in sanitizer builds, !1724 (@jonrob)
- Remove make_transposed_raw_banks, !1719 (@nskidmor)
- Add Hlt2 and Sprucing event size qmtests, !1699 (@shunan)


### Documentation ~Documentation

- ~Configuration | Run HLT1 and HLT2 in same step, !1723 (@gunther)
- Refactor(CODEOWNERS) remove @chasse, !1739 (@chasse)
- Doc for running HLT1->HLT2 chain, !1718 (@nskidmor)


### Other

- ~selection ~hlt2 | Fix typo in IP chi2 cut, !1779 (@johndan)
- ~selection ~hlt2 | Xi_b -> Xi Gamma and Omega_b -> Omega Gamma lines, !1238 (@mireboll)
- ~hlt2 ~Tracking ~Utilities | Update options using PrDebugTrackingTool, !1747 (@gunther)
- ~hlt2 ~Monitoring | Add monitoring of number of candidates per line, !1748 (@sstahl)
- ~Calo ~Monitoring | Separate calib BXIDs from digit monitoring, !1685 (@alobosal)
- ~Functors | Enable functor cache in dd4hep builds, !1725 (@jonrob)
- ~"Event model" | Test reco sequence with v3::Tracks, !1652 (@decianm)
- ~Monitoring | Adapt to change in configuration of TrackMonitor, !1763 (@wouter)
- Update References for: Moore!1786 based on lhcb-master-mr/5802, !1803 (@lhcbsoft)
- Update references for Allen!856, !1802 (@rmatev)
- Update References for: Moore!1780 based on lhcb-master-mr/5826, !1801 (@lhcbsoft)
- Update References for: LHCb!3799, Moore!1797 based on lhcb-master-mr/5816, !1798 (@lhcbsoft)
- Update References for: LHCb!3750, Moore!1638, Analysis!918, DaVinci!747, MooreAnalysis!92 based on lhcb-master-mr/5796, !1796 (@lhcbsoft)
- Revert !1772 (Tests for GaudiAllen), !1792 (@rmatev)
- Update References for: LHCb!3795, Moore!1585 based on lhcb-master-mr/5783, !1790 (@lhcbsoft)
- Update References for: Allen!956, MooreOnline!139 based on lhcb-master-mr/5731, !1789 (@lhcbsoft)
- Update References for: Moore!1722 based on lhcb-master-mr/5732, !1787 (@lhcbsoft)
- Update References for: LHCb!3716, Rec!3082 based on lhcb-master-mr/5678, !1777 (@lhcbsoft)
- Test update for lhcb/LHCb!3777, !1775 (@graven)
- Update ref for Allen!948, !1769 (@rmatev)
- Update References for: Allen!948 based on lhcb-master-mr/5584, !1767 (@lhcbsoft)
- Update References for: Rec!2710 based on lhcb-master-mr/5619, !1766 (@lhcbsoft)
- Add RetinaClusters to digi files, !1760 (@gbassi)
- Update References for: Moore!1715 based on lhcb-master-mr/5607, !1765 (@lhcbsoft)
- Update References for: Moore!1743 based on lhcb-master-mr/5591, !1762 (@lhcbsoft)
- Fix the type for make_mc_track_info(), !1759 (@jzhuo)
- Update References for: Moore!1750 based on lhcb-master-mr/5569, !1754 (@lhcbsoft)
- Update MC sample used in flavour tagging qm test, !1750 (@cprouve)
- Update References for: Rec!2612, Moore!1712 based on lhcb-master-mr/5563, !1752 (@lhcbsoft)
- Update References for: Allen!807 based on lhcb-master-mr/5559, !1751 (@lhcbsoft)
- Update References for: Rec!3042, Rec!3074 based on lhcb-master-mr/5548, !1749 (@lhcbsoft)
- Update References for: LHCb!3754 based on lhcb-master-mr/5538, !1744 (@lhcbsoft)
- Update References for: LHCb!3665, Rec!2765, Moore!1652 based on lhcb-master-mr/5526, !1742 (@lhcbsoft)
- Fix tests for Rec!2612, !1712 (@ausachov)
- Update References for: Moore!1728 based on lhcb-master-mr/5500, !1735 (@lhcbsoft)
- Update References for: Moore!1723 based on lhcb-master-mr/5481, !1734 (@lhcbsoft)
- Disable some tests for the DD4Hep platform builds, !1726 (@decianm)
- Add documentation for (default) monitoring, !1706 (@mstahl) [#461]
- Convert Standard Particles to use ThOr framework, !1230 (@gmeier)
- Update References for: Rec!3068, Moore!1721 based on lhcb-master-mr/5450, !1731 (@lhcbsoft)
- Update References for: Rec!3043, Moore!1698 based on lhcb-master-mr/5314, !1713 (@lhcbsoft)
- Update ion sequence in standalone.py, !1695 (@baudurie)
- Adapt to Run3 detectors for TOS fractions, !1263 (@decianm)
- Make the Muon decoding / hit making configurable, !1714 (@decianm)
- Add tests for Allen muon hits decoding, !1704 (@samarian)
- Adapt configuration for change in PrKalmanFilter, !1666 (@decianm)

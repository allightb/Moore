2018-03-08 Moore v28r0
========================================

Development release for 2018
----------------------------------------
Based on 2018-patches
This version is released on Gaudi v29r3, LHCb v44r0, Lbcom v22r0, Rec v23r0, Phys v25r0, Hlt v28r0.

### Enhancements
- Prepare application and tests for 2018, !127 (@rmatev)

### Cleanup and testing
- Miscellaneous improvements to tests, !129 (@rmatev)

### Other
- Port v26r6p1 MRs from 2017-patches, !124, !128, !133 (@rmatev)

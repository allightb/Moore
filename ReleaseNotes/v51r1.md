2020-08-03 Moore v51r1
===

This version uses
Allen [v1r0](../../../../Allen/-/tags/v1r0),
Phys [v31r1](../../../../Phys/-/tags/v31r1),
Rec [v31r1](../../../../Rec/-/tags/v31r1),
Lbcom [v31r1](../../../../Lbcom/-/tags/v31r1),
LHCb [v51r1](../../../../LHCb/-/tags/v51r1),
Gaudi [v33r2](../../../../Gaudi/-/tags/v33r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to Moore [v51r0](../-/tags/v51r0), with the following changes:

### New features ~"new feature"

- ~selection ~hlt1 | Add GEC-only pass through line to Hlt1, !417 (@rjhunter)
- ~selection ~hlt2 | Add first QEE lines, !560 (@rjhunter)
- ~selection ~hlt2 | PID calibration lines, !530 (@dhill)
- ~selection ~hlt2 | Move HLT2 dimuon lines into onia module, !498 (@mengzhen)
- ~selection ~hlt2 | B2OC: Add b-baryon lines and related clean-ups, !480 (@abertoli)
- ~selection ~hlt2 | Added first B&Q basic filters and standard combinations, !451 (@gcavalle)
- ~selection ~hlt2 | B2OC: added Pi0 algorithms and neutral D*0 -> D0pi0 decays, !428 (@mkenzie) [#132]
- ~selection ~hlt2 | B2OC: adding new lines and a new functionality: BDT filtering of the output B mesons, !421 (@abertoli)
- ~selection ~hlt2 | Add inclusive B topological HLT2 lines, !322 (@gciezare)
- ~selection ~hlt2 ~Jets | Add jet lines, !338 (@rangel)
- ~selection ~hlt2 ~Composites ~Functors | Functor/combiner proceedings, !435 (@nnolte)
- ~hlt1 | Add configuration for HLT1 comparison, !412 (@rmatev) [#137]
- ~hlt1 ~Tracking | Configure HLT1 PrForward reconstruction to use momentum guided search window, !415 (@gunther)
- ~hlt1 ~"MC checking" | Add track resolution and IP resolution checker and option file for muonid efficiency check, !347 (@peilian) [#106,#138]
- ~hlt1 ~Accelerators | Call Allen from Moore, !378 (@dovombru) :star:
- ~hlt2 ~Configuration | Add switch to run selections on real-time reconstruction, !541 (@sstahl) [#179] :star:
- ~hlt2 ~Configuration ~Persistency | Basic HLT2 persistency, !324 (@apearce) [gaudi/Gaudi#102,gaudi/Gaudi#116,LBRTAPLAN-140]
- ~Configuration | Add application option for enabling auditors, !529 (@jonrob)
- ~Configuration | Allow passing lines maker with options, !486 (@rmatev)
- ~Configuration | Add options to Moore to configure format and timeFormat of the MessageSvc, !468 (@chasse)
- ~Calo ~Monitoring | Add initial Calo data monitoring, !390 (@cmarinbe)
- ~Conditions ~Build | Support for DD4hep in the framework, !432 (@sponce)
- Upstream project highlights :star:
  - ~Decoding ~Tracking | Templated VeloClusterTracking for Retina clustering and faster option, Rec!1982 (@ahennequ)
  - ~Tracking | Parametrised scatters (Ported from track-fit-workshop branch), Rec!2107 (@ldufour)
  - ~Tracking | Extend and modernize extrapolators, Rec!2031 (@graven)
  - ~Tracking | New Hough cluster search for the UV (stereo) hits in the SciFi Hybrid Seeding, Rec!1973 (@saiola)
  - ~Tracking ~Muon ~RICH ~Conditions | Add support for DD4hep in the framework, Rec!2003 (@sponce)
  - ~Tracking ~Calo | Utils for new track fit configuration, Rec!2060 (@ausachov)
  - ~Muon ~PID | Adding chi2corr to MuonIDHLT1, Rec!1958 (@masantim) [INT-2019]
  - ~Calo ~Composites | Add DataHandles to proto particle making sequence, Rec!1886 (@sstahl)
  - ~Calo ~"Event model" | Add CaloDigits v2 and CaloClusters v2, LHCb!2166 (@graven)
  - ~Calo ~"MC checking" | Add Calo ClusterEfficiency algorithm, Lbcom!482 (@cmarinbe)
  - ~Calo ~"MC checking" | Add CaloClusterResolution algorithm, Lbcom!431 (@ozenaiev)
  - ~Jets ~Composites | Add new jet reconstruction algorithms, Phys!617 (@rangel)
  - ~Composites ~Filters ~Functors | Add vectorised track combiner, Rec!2134 (@olupton) [GAUDI-1023]
  - ~Core | Add machinery for tracking dynamic allocations and enable its use in the scheduler, LHCb!2472 (@olupton)
  - ~Conditions ~Build | Support for DD4hep in the framework and for the VP subdetector, Lbcom!452 (@sponce)
  - ~Conditions ~Build | Support for DD4hep in the framework and for the VP subdetector, LHCb!2455 (@sponce)


### Fixes ~"bug fix" ~workaround

- ~selection ~hlt1 | Fix pp_comparison segfault, !439 (@ahennequ)
- ~selection ~hlt2 | Add upfront reco to jpsiphi, !418 (@egovorko)
- ~"MC checking" | Fix memory leaks in scripts to generate plots, !535 (@peilian)


### Enhancements ~enhancement

- ~selection ~hlt1 | Tweak HLT1 dimuon thresholds, !434 (@rjhunter) [#144]
- ~selection ~hlt2 | B2OC: changes to reduce rates and updates of BDT, !570 (@shunan)
- ~selection ~hlt2 | B2OC: fix to make_dsst_to_dsplusgamma in builders/d_builder.py, !544 (@abertoli)
- ~selection ~hlt2 | Allow requirements on 2- and 3- body topo output in B2OC prefilters, !518 (@abertoli)
- ~selection ~hlt2 | Add register_line_builder in dimuon.py for detached jpsi and psi2s lines, !416 (@mengzhen)
- ~selection ~hlt2 | Optimisation of the dimuon lines, !404 (@apiucci)
- ~hlt2 ~Tracking | Add configuration for the residual algorithms, !563 (@peilian)
- ~hlt2 ~Tracking | Run track fit on preselected Long and Downstream tracks only, !481 (@ausachov)
- ~hlt2 ~Calo ~"MC checking" | Add electron tracks MC checking to HLT2, !457 (@sponce)
- ~hlt2 ~RICH | Consistently set RICH radiator and detector properties, !460 (@jonrob)
- ~hlt2 ~RICH | Rich - Use average detectable photon yield algorithm, !443 (@jonrob)
- ~hlt2 ~RICH | Adapt to the change in RICH ray tracing properties, !437 (@jonrob)
- ~hlt2 ~RICH | Add some missing RICH HLT2 configuration options, !402 (@jonrob)
- ~hlt2 ~RICH ~Monitoring | RICH - Adapt to retuning of Ring Reuse settings, !465 (@jonrob)
- ~Configuration | Assert that PublicToolHandles are configured with public tools, !576 (@apearce) [#196]
- ~Configuration | Config speedup, !545 (@sstahl)
- ~Configuration | Support running Moore in Ganga, !532 (@apearce) [#93,#95,(#93,(#95] :star:
- ~Configuration ~Composites | Simplify CombineTracks wrapper, !538 (@olupton)
- ~Configuration ~Functors ~Build | Add HLT2 functor cache, !536 (@rmatev)
- ~Configuration ~"MC checking" | Add unpackers for MCHits to config as well as a test that runs all of them, !473 (@chasse)
- ~Configuration ~Core | Forbid the combination of lazy evaluation and unspecified order, !502, !519 (@nnolte, @gunther)
- ~Calo | Adapt to v2 CaloDigits and CaloClusters, !403 (@graven)
- ~Calo | Tune CaloFutureShowerOverlap and add cluster resolution tests, !386 (@ozenaiev)
- ~RICH ~PID | Add option to include seed (SciFi) tracks in RICH reconstruction, !510 (@saiola)
- ~RICH ~Monitoring | Add RICH DLL data monitor to HLT2 config., !410 (@jonrob)
- ~Composites ~Filters ~Functors | Switch to vectorised track combiner in HLT1 lines, !562 (@olupton)
- ~Persistency | Make explicit which part of the raw event to use, !459 (@sstahl)
- ~"MC checking" | Update run_cheated_pattern_recognition options to include IdealStateCreator, !484 (@chasse)
- ~Build | Fix CI by using flake8 from LbEnv, !444 (@rmatev) [#147]
- Upstream project highlights :star:
  - ~Tracking | Performance improvements for the SciFiForwardTracking, Rec!1994 (@chasse)
  - ~Tracking | Improve access to geometry in SciFiTrackForwardingStoreHit, Rec!1993 (@ahennequ)
  - ~Calo | Faster cluster corrections in calo shower overlap algorithm, Rec!1880 (@ozenaiev)
  - ~Calo | Migrate code in CaloFuture to Digits v2, Clusters v2, Rec!1740 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~hlt2 | Update hlt2_reco_baseline_with_mcchecking.py to run full reconstruction, !436 (@sstahl)
- ~Configuration | Remove debug print-outs, !578 (@apearce)
- ~Configuration | Prepare for the rename of DataObjectHandleBase to DataHandle, !569 (@clemenci)
- ~Configuration | Disable configuration profile test on 'slow' builds, !553 (@jonrob)
- ~Configuration | References updates for lhcb/Moore!541, !552 (@acasaisv)
- ~Configuration | Do not strictly require ConfigurableMeta inheritance, !442 (@nnolte)
- ~Decoding | Follow lhcb/Rec!2000: do not set removed properties, !431 (@graven)
- ~Tracking | Updated reference files for Moore MR 563, !579 (@abrearod)
- ~Tracking | Update refs for Moore!517, !558 (@jonrob)
- ~Tracking | Update reference files for MR Rec!2127, !555 (@acasaisv)
- ~Tracking | Added tests for parametrised scatters, !539 (@ldufour)
- ~Tracking | Adapt to TrackState Provider in TES, !537 (@sstahl)
- ~Tracking | Remove the unused input for tracking, !517 (@peilian)
- ~Tracking | Update references files for lhcb/Rec!2065, !514 (@frodrigu)
- ~Tracking | Update reference files for Rec!2089, !511 (@frodrigu)
- ~Tracking | Update refs for Rec!2031, !492 (@sgweber)
- ~Tracking | References for Rec!2015, !474 (@dovombru)
- ~Tracking | Update references for Rec!2028, !467 (@dovombru)
- ~Tracking | Update refs for Rec!1760, !458 (@mzdybal)
- ~Tracking | Remove deprecated option in SciFiTrackForwarding, !455 (@chasse)
- ~Tracking | Update refs for Rec!1994, !447 (@mzdybal)
- ~Tracking | Follow Rec!1993, !440 (@mstahl)
- ~Tracking | Update refs for Rec!2006 (fix in seeding), !438 (@mstahl)
- ~Tracking | Update references, !433 (@apearce)
- ~Tracking ~"MC checking" | Track fit hits monitoring tests, !505 (@valukash)
- ~Tracking ~Utilities | Update refs for Rec 2634, !572 (@jonrob)
- ~Calo | Fix references for hlt2_reco_calo_from_dst, !515 (@jonrob)
- ~Calo | Update references for lhcb/Rec!1971, !496 (@raaij)
- ~Calo | Adapt calo configuration to lhcb/Rec!1971, !490 (@graven)
- ~Calo | Ref updates for Rec!2027, !488 (@sgweber)
- ~Calo | Update refs (follow up !474), !477 (@rmatev)
- ~Calo | Update references for Rec!1987, !464 (@dovombru)
- ~Calo | Update references for hlt2_reco_baseline_with_monitoring, !463 (@rmatev)
- ~Calo | Update refs for LHCb!2465, !449 (@mzdybal)
- ~Calo | Update refs for Rec!1981, !425 (@rmatev)
- ~Calo ~"MC checking" | Add ECAL reconstruction efficiency test, !506 (@cmarinbe)
- ~Calo ~"MC checking" | Re-enabled calorimeter resolution MC tests, !475 (@ozenaiev) [gitlab.cern.ch/lhcb/Lbcom/-/merge_requests/431/diffs#03]
- ~RICH | Remove (HPD specific) RICH decoding errors monitor from data monitoring, !551 (@jonrob)
- ~RICH | Update references for Rec!2044, !485 (@sgweber)
- ~RICH | Update references for Moore!460, !469 (@dovombru)
- ~RICH | Update refs for Rec!2007, Rec!2024 and Rec!2030, !454 (@mzdybal)
- ~Composites | Update refs for Rec2134, !575 (@msaur)
- ~Composites | Updates the reference files for !393, !528 (@dcampora)
- ~Functors | Take advantage of changes in Rec!2141, !577 (@olupton)
- ~"MC checking" | Update refs for Rec!2053, !483 (@rmatev)
- ~"MC checking" | Explicitly configure VisPrimVertTool used by TrackResChecker, !354 (@graven)
- ~Accelerators | Update ref for Moore 378, !574 (@msaur)
- ~Build | Remove TCK/L0TCK dependency, !550 (@apearce)
- Extend timeout for test_generic_example_with_reco, !573 (@jonrob)
- Extend timeout for hlt1_reco_muonIDeff, !557 (@jonrob)
- Extend timeout for mc_matching_example, !556 (@jonrob)
- Add timeout setting to test_generic_example, !554 (@jonrob)
- Extend some test timeouts to adapt to longer runtime in d0 builds, !543 (@jonrob)
- Introduce missing reference validation in reco tests (@sponce), !533 (@dcampora)
- Increase timeout of HLT2 all-lines test, !527 (@apearce) [#187]
- Rename temporary ref file to make it clear it should not be updated when fetching new refs, !523 (@jonrob)
- Delete spurious YAPF file, !521 (@apearce)
- Add reference log sym links for skylake AVX512 builds, !512 (@jonrob)
- Bulk update of refs following recent MRs, !445 (@rmatev)
- Upstream project highlights :star:


### Documentation ~Documentation

- ~Persistency | Make explicit which part of the raw event to use, !459 (@sstahl)
- ~Build | Simplify building of documentation, !548 (@rmatev)
- Fix broken link in docs, !588 (@apearce)
- Update doc/tutorials/different_samples.rst, !587 (@sstahl)
- Update HltEfficiencyChecker docs for MooreAnalysis!4, !580 (@rjhunter)
- Update HltEfficiencyChecker documentation for MooreAnalysis!17, !571 (@rjhunter)
- Compute Ganga dataset using native SE filtering, !559 (@apearce) [#193]
- Tutorial on studying efficiencies and rates, !542 (@rmatev) [MooreAnalysis#8]
- Expand configurable documentation, !526 (@apearce) [#54]
- Expand control flow documentation, !524 (@apearce) [#174]
- Add CODEOWNERS file, !522, !581 (@apearce) [#65]
- Documentation for workaround for caching in TrackStateProvider, !507 (@saiola) [LHCBPS-1835]
- Point to documentation source in documentation, !503 (@sstahl)
- Aadapt doc to benchmark scripts migration to PRConfig, !489 (@chasse)
- Tidy up HLT2 analysis tutorial, !420 (@apearce)

2024-03-05 Moore v55r2
===

This version uses
Allen [v4r2](../../../../Allen/-/tags/v4r2),
Rec [v36r2](../../../../Rec/-/tags/v36r2),
Lbcom [v35r2](../../../../Lbcom/-/tags/v35r2),
LHCb [v55r2](../../../../LHCb/-/tags/v55r2),
Detector [v1r27](../../../../Detector/-/tags/v1r27),
Gaudi [v38r0](../../../../Gaudi/-/tags/v38r0) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) .

This version is released on the `master` branch.
Built relative to Moore [v55r1](/../../tags/v55r1), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~"MC checking" | Provide mc_categories for all Best and only apply eta25 when not defined differently, !3056 (@gunther)
- ~Monitoring | Fix bug in monitoring of the number of candidates, !2798 (@sstahl)
- BforSpectroscopy optimise, !2889 (@ipolyako)
- Put back AddressKillerAlg to moore config when writing dst, !3031 (@sesen)
- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Exclude absurd track warning and allow dbg for hlt2_protoparticles_fastest, !3079 (@gunther)
- [DPAWP1] Move excl_spruce_2023_2_data.qmt to using a fixed lines_for_TISTOS, !3037 (@lugrazet)
- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- ~selection | New HLT2 lines for dark photon decays from Charm decays, !3011 (@cacochat)
- ~selection | New tau to mu gamma line, !3012 (@cacochat)
- ~selection | Add Charm HLT2 lines Dsstar2DsPiPi, !2989 (@chenlong)
- ~selection | RD baryonic lines update, !2986 (@msaur)
- ~selection ~hlt2 | QEE: persist pions in same event as $\pi^0 \to e^+e^-\gamma$, !2977 (@fevolle) [gitlab.cern.ch/lhcb/Rec/-/merge_requests/3758#995258]
- ~selection ~hlt2 | Retuning of cut-based inclusive dilepton trigger, !2747 (@jagoodin) [#670,#7,(MooreAnalysis#36]
- ~selection ~hlt2 | Add Hlt2 Lb2L0Gamma line, !1232 (@pgironel)
- ~selection ~hlt2 | Add HLT2 line Bu2DspPPbar, !2956 (@ziyiw)
- ~selection ~hlt2 | Add Lambda EDM/MDM lines for Lb->LcX, Lc->LX decays, !2874 (@mengzhen)
- ~hlt2 | Sum of B2CC HLT2 lines (MR !2953 !2954 !3021 !3048 !3060 !3092), !3034 (@yimingli)
- ~hlt2 | RD: Add isolation to hlt2 lines (b --> xll and baryonic), !2995 (@palaguar)
- ~hlt2 | Updated Tuning to the Topological Triggers, !2823 (@nschulte) [#2,#7]
- ~hlt2 | Added merged Hlt2RD_BuToHpMuMu_Incl and Hlt2RD_BuToHpMuMu_Incl_SameSign lines, !2978 (@fabudine)
- ~hlt2 | Add lines for tracking efficiency in SMOG2, !2940 (@rsadek)
- ~hlt2 | Three additional HLT2 open charm lines for SMOG2, !2852 (@chgu)
- ~Configuration ~"MC checking" | Configure mc_checking for all requested fitted (Best) track types, !2992 (@gunther)
- ~Muon | Add fix hlt2_pr_kf_longmuon.py, !2610 (@svecchi)
- ~Jets | QEE: additional sprucing lines for QEE (di)jet lines. Requires !2848, !3007 (@ngrieser)
- ~Jets | Jet Lines for 2024, !2848 (@rangel) [INT-2016]
- ~PID | Tightened selections for the D* PID HLT2 line and prescaled the Omega PID line, !2968 (@mbovill)
- ~PID | Updated selection for Run3 neutral Calibration lines, !2644 (@desahoo)
- ~Persistency | Update hlt2_tistos, !3064 (@sesen)
- ~Monitoring | RD: fixes to radinclusive lines, !2944 (@alobosal)
- ~Monitoring | RecoMon: make correlation relative to ECAL energy (not clusters),  move correlations outside gec, !2695 (@tmombach)
- Fix v3 refs for topo, !3116 (@lhcbsoft)
- Update References for: Allen!1192 based on lhcb-master-mr/10842, !3115 (@lhcbsoft)
- Fix pp_thor_without_UT so that it reports throughput, !3113 (@ahennequ)
- Fix PLUME decoding warnings, !3112 (@cagapopo)
- Switch to from_file=False for hlt2_all_lines test, !3106 (@oozcelik)
- [RTA-WP4] updated selection for JPsi2mumu PID line to reduce BW, !3105 (@daproven)
- Add pv_tracks=True to enable PV unbias, !3100 (@peilian)
- SL: sprucing lines for b2taunu using heavy flavour tracking, !2928 (@mveghel)
- Update References for: Allen!1428 based on lhcb-master-mr/10845, !3111 (@lhcbsoft)
- Update References for: Moore!2823 based on lhcb-master-mr/10838, !3109 (@lhcbsoft)
- [QEE] Reduce EW Excl.Spruce footprint, !3096 (@lugrazet)
- Remove raw banks from full stream in hlt2_pp_commissioning (and book BnoC full lines), !3088 (@rjhunter)
- B2OC: 2024 rates tuning, !3061 (@abertoli)
- SL: new HLT2 and Spruce line for B_s0 -> D*s mu nu, !3020 (@cacochat)
- Tighten Bc->(Bs->Ds h) h' lines, !3099 (@anmorris)
- Update References for: Allen!1377 based on lhcb-master-mr/10779, !3097 (@lhcbsoft)
- For VDM pass through and persistence options in Spruce lines, !3019 (@nskidmor)
- RD Xtaul isolation, !2973 (@lmadhanm)
- Inclusive detached dilepton mva rate reduction, !2972 (@lecarus)
- Update References for: Moore!3079 based on lhcb-master-mr/10775, !3080 (@lhcbsoft)
- Update References for: Allen!1410 based on lhcb-master-mr/10743, !3076 (@lhcbsoft)
- Update References for: Moore!2889 based on lhcb-master-mr/10725, !3073 (@lhcbsoft)
- SL: increase trifake lepton line prescales, !3072 (@groberts)
- SL: Omega_bToOmega_cENu line removal, !3071 (@groberts)
- Tighten SLB Xi_b line cuts, !3067 (@anmorris)
- Update References for: Moore!3056 based on lhcb-master-mr/10717, !3059 (@lhcbsoft)
- Update References for: LHCb!4453 based on lhcb-master-mr/10719, !3058 (@lhcbsoft)
- Update References for: LHCb!4450, Rec!3759 based on lhcb-master-mr/10722, !3038 (@lhcbsoft)
- Fine-tuning of Hlt2RD_TauToPhiMu line thresholds, !2999 (@driccard)
- SL: reduce bandwidth of electronic B decays, !2962 (@groberts)
- RD: add BTohhGamma_GammaToEELL lines, !2939 (@fibishop)
- Update References for: Rec!3484, Moore!2610, Alignment!396 based on lhcb-master-mr/10686, !3050 (@lhcbsoft)
- Update References for: Allen!1445 based on lhcb-master-mr/10660, !3041 (@lhcbsoft)
- Update References for: LHCb!4269, Rec!3559, Moore!2622 based on lhcb-master-mr/10672, !2960 (@lhcbsoft)
- Update D0->KsKs selections, !2917 (@gtuci)
- SL: move fake line prescales from Spruce to HLT2, !2868 (@anmorris)
- BnoC HLT2 lines and one BTohh sprucing line, !2824 (@zewen)
- UThit position methods, !2622 (@hawu)
- Update References for: Allen!1315 based on lhcb-master-mr/10602, !3025 (@lhcbsoft)
- Update References for: Moore!2940 based on lhcb-master-mr/10603, !3024 (@lhcbsoft)
- Add HNL lines for an HNL decaying into two leptons, !2991 (@lohenry)
- [RTADPA BW tests] Retire obviated options files and (very slow) qmtest, !2971 (@rjhunter)
- Add parent isolation variables to lines in RD, !2925 (@matzeni)
- Adding FT flag for some RD lines, !2920 (@matzeni)
- B2OC branch to collect updates for Hlt2 2024, !2821 (@abertoli)
- Loosen cut trackchi2 cut in Velo tracking efficiency lines, !2676 (@rowina)
- Update References for: Allen!1415 based on lhcb-master-mr/10566, !3017 (@lhcbsoft)
- Removing XibToXicENu lines (including tau->e lines), !2942 (@groberts)
- [RTA BW tests]: Specify file extension on BW output files; write to dst for sprucing, !2871 (@rjhunter)

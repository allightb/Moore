2023-05-09 Moore v54r8
===

This version uses
Allen [v3r8](../../../../Allen/-/tags/v3r8),
Rec [v35r8](../../../../Rec/-/tags/v35r8),
Lbcom [v34r8](../../../../Lbcom/-/tags/v34r8),
LHCb [v54r8](../../../../LHCb/-/tags/v54r8),
Detector [v1r12](../../../../Detector/-/tags/v1r12),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r7](/../../tags/v54r7), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Build | Reduce the number of concurrent cache jobs by ~2x, !2233 (@rmatev)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Persistency | Follow  changes in LHCb!4055, !2227 (@graven)
- Removing obsolete python2 code, !2241 (@msaur)
- Upstream project highlights :star:


### Documentation ~Documentation

- Docs update for MooreAnalysis!120, !2274 (@rjhunter)
- Adding a comment about qmtexec to the documentation, !2239 (@msaur)
### Other

- ~selection | BandQ HLT2 lines for Jpsi2LambdaLambdabar - update, !2271 (@ziyiw)
- ~selection | BandQ HLT2 lines for Jpsi2LambdaLambdabar, !2242 (@ziyiw)
- ~selection ~hlt2 | Cut-based inclusive dilepton trigger, !1979 (@jagoodin) [#7]
- ~hlt1 ~Configuration | Update Moore-Allen python interface to use V3 converters rather than V2., !2157 (@spradlin) [Allen#371]
- ~hlt2 | Merge detached dilepton trigger, !2272 (@lecarus)
- ~hlt2 | Add a rerun HLT2 option in Moore, !2159 (@kmattiol)
- ~hlt2 | Detached dilepton trigger selection update, !1848 (@lecarus)
- ~UT | Adapt to new UT channel ID class and geometry, !785 (@xuyuan)
- ~Jets | Remove FilterDesktop and CombineParticles from jet lines using topobits, !2087 (@helder)
- ~Persistency | Preparing for raw banks SP, !2058 (@msaur) [#254,#401]
- Adding the new SL MiCo., !2282 (@jugarcia)
- Update References for: Detector!181, LHCb!2995, Lbcom!550, Rec!2403, Allen!865, Moore!785, Boole!334 based on lhcb-master-mr/7794, !2280 (@lhcbsoft)
- Added dedicated v3 refs for test_hlt2_flavourtagging_flavourtaggers and..., !2277 (@sponce)
- Follow changes LHCb!4110, !2276 (@graven)
- Update References for: Moore!2176 based on lhcb-master-mr/7785, !2179 (@lhcbsoft)
- Add back ChargedPacked lineskipper in test_hlt2_check_packed_data_checksums.qmt, !2176 (@graven)
- Revert "Merge branch 'ziyi_onia_jpsi2LmdLmd' into 'master'", !2270 (@sponce)
- Update References for: Allen!1191 based on lhcb-master-mr/7759, !2269 (@lhcbsoft)
- Update References for: LHCb!4104 based on lhcb-master-mr/7761, !2267 (@lhcbsoft)
- Update References for: Allen!1180 based on lhcb-master-mr/7755, !2266 (@lhcbsoft)
- Update References for: LHCb!3942, Moore!2058 based on lhcb-master-mr/7769, !2264 (@lhcbsoft)
- Update References for: Rec!3370, Allen!1124, Moore!2157 based on lhcb-master-mr/7731, !2261 (@lhcbsoft)
- Attempt to fix doc pipeline, !2258 (@sponce)
- Added dedicated detdesc refs for couple of tests that were missing it, !2257 (@sponce)
- Revert "Merge branch 'detached_dilepton_selection_update_branch' into 'master'", !2254 (@sponce)
- SLB label to SLB lines missing them, !2249 (@sklaver)
- Update References for: Allen!1165, Moore!2200, MooreOnline!216, MooreAnalysis!116 based on lhcb-master-mr/7694, !2247 (@lhcbsoft)
- GaudiAllen: actually filter events + fix tests, !2200 (@ahennequ)
- QEE: remove workaround for non-thread safe lines in options file as the lines have become thread safe, !2184 (@rjhunter)
- Qee passthrough W->mu nu Sprucing line, !2183 (@lugrazet)
- Update References for: LHCb!4077 based on lhcb-master-mr/7663, !2238 (@lhcbsoft)
- Fixed build of HLT1 functor, !2185 (@sponce)

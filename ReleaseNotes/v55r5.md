2024-04-03 Moore v55r5
===

This version uses
Allen [v4r5](../../../../Allen/-/tags/v4r5),
Rec [v36r5](../../../../Rec/-/tags/v36r5),
Lbcom [v35r5](../../../../Lbcom/-/tags/v35r5),
LHCb [v55r5](../../../../LHCb/-/tags/v55r5),
Detector [v1r30](../../../../Detector/-/tags/v1r30),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Moore [v55r4](/../../tags/v55r4), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Updating hlt2_production_options_2024.yaml to use HLT1-filtered MDF, !3244 (@msaur)
- Re-enabling various RecoConf tests on DD4hep and renaming legacy CPU-based HLT1 code, !2907 (@msaur)
- Upstream project highlights :star:


### Documentation ~Documentation

- ~Functors | Documentation for binding and chaining functors' composition, !3235 (@tfulghes)
### Other

- ~hlt2 | Reduce rates and bandwidth of IFT HLT2 lines with SMOG, !3211 (@oboenteg)
- ~hlt2 | Bandq append "Full" to full lines, !3233 (@yajing)
- ~PID | Option of default ProbNN value filling, !3223 (@mveghel)
- ~Persistency | Checks of persistency of extra outputs in sprucing stage, !3133 (@tizhou)
- Update References for: Allen!1483 based on lhcb-master-mr/11334, !3253 (@lhcbsoft)
- Update dd4hep references after 2907, !3247 (@cagapopo)
- Update References for: LHCb!4470, Moore!2907, Alignment!458, MooreOnline!326, Panoptes!317, DaVinci!1038 based on lhcb-master-mr/11314, !3241 (@lhcbsoft)
- [QEE] WZrd turbo lines have `Full` in the name. -> Won't be selected by regex correctly., !3234 (@lugrazet)
- Change directory to look in for expected_2024 min bias in tests, !3169 (@rjhunter)
- Update References for: Rec!3835, Moore!3223 based on lhcb-master-mr/11258, !3232 (@lhcbsoft)

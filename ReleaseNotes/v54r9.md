2023-06-12 Moore v54r9
===

This version uses
Allen [v3r9](../../../../Allen/-/tags/v3r9),
Rec [v35r9](../../../../Rec/-/tags/v35r9),
Lbcom [v34r9](../../../../Lbcom/-/tags/v34r9),
LHCb [v54r9](../../../../LHCb/-/tags/v54r9),
Detector [v1r13](../../../../Detector/-/tags/v1r13),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r8p1](/../../tags/v54r8p1), with the following changes:

### Fixes ~"bug fix" ~workaround

- ~selection ~hlt2 | Updates to new HLT2 trigger line for dark photon search, !2370 (@cacochat)
- Do not _invoke_ @configurable functions when defining default arguments in (@configurable) functions, !2323 (@graven)
- Update HltEfficiencyChecker docs for MooreAnalysis!119, !2259 (@rjhunter)


### Enhancements ~enhancement

- Add a sprucing bandwidth test, !2365 (@shunan) [#507,lhcb-dpa/project#199]


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Clean up Moore tests, !2376 (@cmarinbe)
- Remove Hlt2Conf.algorithms.py, !2306 (@pkoppenb) [Rec#358]
- Fix Allen-in-Gaudi tests for the DD4Hep builds, !2129 (@raaij)
- Remove PrResidualUTHits from configuration, !2322 (@decianm)
- Use new MC samples in tests with data-like decoding versions, !1703 (@decianm)
- Removal of FilterDesktop, !2248 (@pkoppenb)
- Upstream project highlights :star:


### Documentation ~Documentation

- Update HLT2 line writing documentation, !2080 (@mstahl)
- Update HltEfficiencyChecker docs for MooreAnalysis!119, !2259 (@rjhunter)
- Fix sphinx compilation, !2294 (@dovombru) [#567]


### Other

- ~selection | Charm Hlt2 lines for SL decays of charmed hadrons from beauty, !2262 (@yangjie)
- ~selection ~hlt2 | Low bias KSLL for D02KSHH low bias line, !2377 (@mamartin)
- ~selection ~hlt2 | Charm: Hexaquarks / Dibaryons, !2202 (@ipolyako)
- ~selection ~hlt2 | Inclusive lines of DDD Xi and Omega matched to a slightly displaced Velo track, !2350 (@mstahl)
- ~hlt2 | Tuning BtoXll, !2222 (@rmwillia)
- ~hlt2 ~Functors | Replace DOCA with SDOCA functors in RD, !2341 (@tfulghes)
- ~hlt2 ~Persistency | Adding turboraw stream, !2329 (@msaur)
- ~Configuration | Store T tracks in Phoenix, !2289 (@decianm)
- ~Configuration ~RICH | Set the min RICH photon probability value depending on cut selection being used, !2308 (@jonrob)
- ~"PV finding" | Add Allen PV mc checking test, !2278 (@dovombru)
- Update refs from lhcb-master/2047, !2413 (@rmatev)
- Update References for: Allen!1239 based on lhcb-master-mr/8204, !2410 (@lhcbsoft)
- Update References for: Allen!992 based on lhcb-master-mr/8203, !2409 (@lhcbsoft)
- Update References for: Allen!1211 based on lhcb-master-mr/8189, !2406 (@lhcbsoft)
- Update References for: LHCb!4116, MooreOnline!251, DaVinci!904 based on lhcb-master-mr/8181, !2401 (@lhcbsoft)
- Update References for: LHCb!4139 based on lhcb-master-mr/8172, !2399 (@lhcbsoft)
- Update References for: LHCb!4119, Moore!2307 based on lhcb-master-mr/8143, !2391 (@lhcbsoft)
- Update References for: Moore!2202 based on lhcb-master-mr/8140, !2390 (@lhcbsoft)
- Update References for: LHCb!4126, Moore!2336 based on lhcb-master-mr/8118, !2386 (@lhcbsoft)
- Tighten cuts on KstGamma line to remove prescale, !2375 (@isanders)
- Add persistreco=True to the remaining 8 SLB lines, !2342 (@anmorris)
- Reduce reliance on `_{hash}` and prefer meaningful deterministic unique names, !2336 (@graven)
- Make propagating of raw banks more explicit, !2307 (@sstahl)
- R(J/psi) Trigger, !2250 (@ejiang)
- Update References for: Moore!2329, MooreOnline!244 based on lhcb-master-mr/8056, !2384 (@lhcbsoft)
- New B2CC HLT2 line for B2JpsiEta decays, !2361 (@oozcelik)
- Prescale SpruceBandQ_BdToEtacKpPim_EtacToHHHH line for sprucing rate tuning, !2357 (@mengzhen)
- Fixed test_hlt2_bandwidth_5stream goign back to calo_raw_bank=false, !2353 (@sponce)
- QEE Hlt2 and Sprucing lines for ZMuMu without MuID, !2309 (@lugrazet)
- Do not copy Hlt1 raw banks twice (output type DST), !2302 (@sstahl)
- Update References for: Allen!1200 based on lhcb-master-mr/7977, !2345 (@lhcbsoft)
- Update References for: Moore!2255 based on lhcb-master-mr/7956, !2340 (@lhcbsoft)
- Update References for: Rec!3413, Moore!2322 based on lhcb-master-mr/7887, !2331 (@lhcbsoft)
- Update References for: Allen!1129, Moore!2129, MooreAnalysis!114 based on lhcb-master-mr/7933, !2312 (@lhcbsoft)
- Update References for: Moore!2180 based on lhcb-master-mr/7954, !2303 (@lhcbsoft)
- QEE: Hlt2 lines for quarkonia (Jpsi & U1S), !2255 (@lugrazet)
- Issue-525: To optimise the bnoc code, !2180 (@zewen)
- B2OC: a few tuning of Hlt2/Spruce lines, modernization of code, add 2 Hlt2 lines with neutrals, !2321 (@abertoli)
- Add back process option, !2320 (@sstahl)
- Update References for: Lbcom!652, Rec!3372, Moore!1703, Alignment!361, Alignment!374, MooreAnalysis!118 based on lhcb-master-mr/7872, !2310 (@lhcbsoft)
- Implement naming convention on QEE HLT2 lines and separate into Turbo/Full streams, !2299 (@rjhunter)
- Detached Dilepton Trigger Change MVA file paths to fix collision with old mva files, !2298 (@lecarus)
- Add a qmtest for Hlt2 bandwidth test and minor fixes, !2290 (@shunan) [#507]
- Update Wegamma spruce line and add three W rare decay HLT2 lines, !2284 (@hyin) [#532]
- Update References for: Rec!3408, Moore!2308, Panoptes!260 based on lhcb-master-mr/7842, !2315 (@lhcbsoft)
- Commissioning: lines with and without HLT1 filter, !2311 (@mstahl)
- Update References for: Allen!794, Moore!2278 based on lhcb-master-mr/7813, !2304 (@lhcbsoft)
- Fix HLT1->HLT2 tutorial in Moore.options, !2293 (@rangel)
- Cleaning up SLB lines, !2253 (@sklaver)

2024-01-26 Moore v55r0
===

This version uses
Allen [v4r0](../../../../Allen/-/tags/v4r0),
Rec [v36r0](../../../../Rec/-/tags/v36r0),
Lbcom [v35r0](../../../../Lbcom/-/tags/v35r0),
LHCb [v55r0](../../../../LHCb/-/tags/v55r0),
Detector [v1r25](../../../../Detector/-/tags/v1r25),
Gaudi [v37r2](../../../../Gaudi/-/tags/v37r2) and
LCG [104](http://lcginfo.cern.ch/release/104/) with ROOT 6.28.04.

This version is released on the `master` branch.
Built relative to Moore [v54r22](/../../tags/v54r22), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~selection ~hlt2 | Control flow fix in femtoscopy lines, !2791 (@dmagdali)
- ~Persistency | Truth matching fix for persistreco, !2781 (@sesen) [#674]
- Fix sign error in PbPb KShort monitor, !2833 (@mstahl)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~selection | Tune J/psi -> ee / mu mu tracking efficiency lines, !2611 (@ldufour)
- ~Configuration ~Tracking | Replace TrackCloneKiller with PrCloneKiller in light reco sequences, !2731 (@gunther) [#657]
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection | Remove surplus hadint lines to lower the rate, !2585 (@rowina)
- ~selection ~hlt2 | Removing deprecated HltLine, !2761 (@msaur) [#239]
- ~hlt2 | Re-add bremsstrahlung test, !2373 (@alopezhu) [#564]
- ~Tracking | Drop usage of PrMCDebugReconstructibleLong, !2679 (@gunther)
- ~Persistency | Remove reference to JSON argument in documentation of hlt2_check_output, !2708 (@ldufour)
- ~Build | Remove ref comparison from hlt2_pp_2022_reprocessing test, !2826 (@rmatev)
- Retire hlt2 2023 bandwidth test, !2845 (@rjhunter)
- Switching to 2024 sample for hlt2_bandwidth_2023_production_streams,, !2841 (@msaur)
- [RTA BW Tests] New Sprucing bandwidth test using latest-available HLT2 input., !2763 (@lugrazet)
- [RTA BW Tests] Use new exp24 minbias for AllenInMoore_Hlt1_Bandwidth test, !2749 (@lugrazet)
- Cleanup old SMOG HLT1 lines and related test, !2807 (@samarian)
- Adapt mc_matching_example to changes in PrDebugTrackingLosses, !2766 (@gunther)
- Upstream project highlights :star:


### Documentation ~Documentation

- Update docs to reflect move of HltEfficiencyChecker to DaVinci, !2812 (@rjhunter)
- HltEfficiencyChecker docs update for MooreAnalysis!134, !2811 (@rjhunter) [MooreAnalysis#33,MooreAnalysis#44]
- Renaming of allen_gaudi_node to allen_gaudi_config, !2728 (@mwiegert)
### Other

- ~selection ~hlt2 | Implementation of BdK1MuMu Line, !2170 (@jabrown)
- ~selection ~hlt2 | B2taunu with btracking lines, !1514 (@mveghel)
- ~selection ~hlt2 | Change electrons particles to include brems for Jpsi->ee maker, !2674 (@peilian)
- ~selection ~hlt2 | HLT2/lines: Tune charm detection asymmetry lines, !2672 (@ldufour)
- ~selection ~hlt2 | Add isMuon to B2CC dimuon lines, !2659 (@oozcelik)
- ~selection ~Filters | Add MCParticleFilter, !2780 (@ldufour)
- ~hlt1 | Follow removal of number of lines input from GaudiAllenReportsToRawEvent, !2520 (@raaij)
- ~hlt2 | BandQ HLT2 lines related to neutrals and downstream tracks, !2771 (@yajing)
- ~Configuration ~Persistency | Add explicit data dependency declarations for the targets of MC hit/particle linkers when reading linkers from file, !2838 (@graven)
- ~Tracking | Downstream/LongLivedParticle track reconstruction, !2337 (@jzhuo)
- ~RICH | Adapt to change in input data to RICH DetectorHits monitor, !2846 (@jonrob)
- ~RICH | Adapt to update to RICH pixel background algorithm, !2723 (@jonrob)
- ~PID ~"Event model" | Removing obsolete pid info checks, !2864 (@mveghel)
- ~Persistency | Clean up the Moore config, define Stream objects, !2802 (@sesen) [#678]
- ~Persistency | Modification of SelectiveCombineRawBankViewsToRawEvent following LHCb!4259, !2796 (@msaur)
- ~Persistency | Repurpose tagging_particles line  option, !2300 (@sesen)
- ~Monitoring | Update documentation for Hlt2 line monitors, !2739 (@mstahl)
- Update refs from lhcb-master/2279, !2885 (@rmatev)
- Update References for: LHCb!4419, Rec!3722, Allen!1396 based on lhcb-master-mr/10226, !2879 (@lhcbsoft)
- Update References for: LHCb!4416, Rec!3718, Moore!2864 based on lhcb-master-mr/10191, !2873 (@lhcbsoft)
- Fixed ref for allen_gaudi_downstream_with_mcchecking, !2870 (@sponce)
- Update References for: Rec!3646, Moore!2731 based on lhcb-master-mr/10184, !2867 (@lhcbsoft)
- Update References for: LHCb!4393, Moore!2802, DaVinci!1009 based on lhcb-master-mr/10204, !2859 (@lhcbsoft)
- Add lumi summary and routing bits to Allen-in-Moore, !2404 (@raaij)
- Update References for: Allen!1015 based on lhcb-master-mr/10177, !2866 (@lhcbsoft)
- Update References for: Rec!3645 based on lhcb-master-mr/10170, !2865 (@lhcbsoft)
- Update References for: Allen!1112, Moore!2631, MooreOnline!278 based on lhcb-master-mr/10162, !2862 (@lhcbsoft)
- Update References for: LHCb!4320, Rec!3624 based on lhcb-master-mr/10167, !2861 (@lhcbsoft)
- Update References for: LHCb!4097, Rec!3397, Moore!2252, DaVinci!1012, LHCbIntegrationTests!54 based on lhcb-master-mr/10158, !2858 (@lhcbsoft)
- Remove Hlt1GECPassthroughDecision, !2631 (@ahabdelm)
- Add NeutralPID object to FutureNeutralProtoParticleAlg, !2252 (@tnanut)
- Update References for: LHCb!4371, Rec!3668, Moore!2846 based on lhcb-master-mr/10149, !2854 (@lhcbsoft)
- Update References for: LHCb!4288, Rec!3589, Rec!3579, Rec!3578 based on lhcb-master-mr/10135, !2851 (@lhcbsoft)
- Update References for: Moore!2786 based on lhcb-master-mr/10132, !2850 (@lhcbsoft)
- Update References for: Detector!462, LHCb!4208, Rec!2868, Moore!1514 based on lhcb-master-mr/10128, !2849 (@lhcbsoft)
- Update References for: LHCb!4337, Lbcom!703, Rec!3642, Moore!2723 based on lhcb-master-mr/10124, !2839 (@lhcbsoft)
- Adapt bremsstrahlung test to changes to ParticleMassMonitor, !2818 (@alopezhu)
- Add default_VeloCluster_source, !2786 (@flazzari)
- Update References for: LHCb!4193, Rec!3538, Moore!2592 based on lhcb-master-mr/10106, !2843 (@lhcbsoft)
- Update References for: Moore!2300, DaVinci!974 based on lhcb-master-mr/10062, !2840 (@lhcbsoft)
- [RTA BW Tests] replace some 'problematic' files from hlt1 bw input, !2834 (@lugrazet)
- Change particle moving to produce selections instead of deep-copied containers, !2592 (@graven)
- Update References for: Rec!3702 based on lhcb-master-mr/10040, !2836 (@lhcbsoft)
- Avoid using `make_data_with_FetchDataFromFile` directly, !2831 (@graven)
- Update References for: LHCb!4304, Rec!3602 based on lhcb-master-mr/9987, !2828 (@lhcbsoft)
- Missing JpsiPi0 HLT2 line, !2606 (@oozcelik)
- Update References for: Rec!3630, Moore!2707 based on lhcb-master-mr/9958, !2822 (@lhcbsoft)
- RD: Add isolation for BuToKpLL and B0ToKpPimLL for SpruceEoY23, !2750 (@matzeni)
- Further cleanup cloner configuration, !2707 (@graven)
- BnoC HLT2/Spruce lines, !2569 (@zewen)
- Fixed forgotten ref file. Only hash changes, !2817 (@sponce)
- Manual fix of refs for LHCb!4367, !2816 (@sponce)
- Update References for: LHCb!4010, Rec!3351, Moore!2166, MooreAnalysis!131, Boole!526 based on lhcb-master-mr/9949, !2813 (@lhcbsoft)
- Fixed output file of hlt2_test_duplicate_filters to avoid collisions with another test, !2810 (@sponce)
- Combined SLB bandwidth reductions - Sprucing campaign 2023, !2805 (@anmorris)
- Use RawBank::View as input instead of RawEvent, !2166 (@graven) [#543]
- Update References for: LHCb!4380 based on lhcb-master-mr/9920, !2809 (@lhcbsoft)
- Update References for: Rec!3670 based on lhcb-master-mr/9917, !2808 (@lhcbsoft)
- Update References for: LHCb!4282 based on lhcb-master-mr/9893, !2804 (@lhcbsoft)
- Update References for: LHCb!4335 based on lhcb-master-mr/9875, !2797 (@lhcbsoft)
- B2OC: selection updates for Spruce_EoY23, !2753 (@abertoli)
- [QEE]: Turn-on ZTrkEff sprucing lines for Spruce_EoY23, !2734 (@lugrazet)
- Update References for: LHCb!4374, Rec!3677, Alignment!432 based on lhcb-master-mr/9838, !2789 (@lhcbsoft)
- Fix name of ref file, !2788 (@rmatev)
- Update References for: LHCb!4327 based on lhcb-master-mr/9829, !2787 (@lhcbsoft)
- Cleanup cloning code, !2177 (@graven)

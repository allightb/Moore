2022-11-23 Moore v54r2
===

This version uses
Allen [v3r2](../../../../Allen/-/tags/v3r2),
Rec [v35r2](../../../../Rec/-/tags/v35r2),
Lbcom [v34r2](../../../../Lbcom/-/tags/v34r2),
LHCb [v54r2](../../../../LHCb/-/tags/v54r2),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9),
Detector [v1r6](../../../../Detector/-/tags/v1r6) and
LCG [101a_LHCB_7](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Moore [v54r1](/../../tags/v54r1), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:
  - ~Tracking | Add Muon hits to PrKM, Rec!3036 (@pherrero)


### Fixes ~"bug fix" ~workaround

- ~hlt2 | Add missing PV filters for HLT 2 lines, !1855 (@mstahl)
- ~hlt2 | B2OC: sort line dictionaries, !1845 (@shunan) [#484]
- Persistency: reduce entries in decoding table to only those locations that are actually packed., !1885 (@graven)
- Fixes for running online, !1840 (@mstahl)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection | Remove obsolete RD builder, !1745 (@msaur)
- ~Tracking | Test for PrTracking debugging tool, !1850 (@gunther)
- Add tests of reconstruction and Hlt2 running on data, !1886 (@sstahl)
- Remove `allen_hlt1` option from `run_moore`, !1861 (@mstahl)
- Sprucing dev, !1837 (@nskidmor)
- Add options file to disable meta data writes to git, !1843 (@jonrob)


### Documentation ~Documentation

- Fix documentation on hlt1_to_hlt2.rst, !1866 (@bdepaula)
### Other

- ~selection | B2OC: disable trchi2todof_max cut in basic_builder.py/filter_particles, !1823 (@abertoli)
- ~selection ~hlt2 | Update HLT2 and Sprucing lines B -> X tau l, !1399 (@tfulghes)
- ~selection ~hlt2 | Update `dhhh` lines: joint lines with same final state, !1755 (@fsouzade)
- ~hlt2 | New SL line: Hlt2BpToD0TauNu_D0ToKPiPiPi_TauToPiPiPiNu, !1809 (@jnovoafe)
- ~hlt2 | Include missing HLT2 lines for strange decays, !1605 (@mramospe)
- ~hlt2 | Ttrack protoparticles and standard particles, !1477 (@isanders)
- ~Configuration | Follow https://gitlab.cern.ch/lhcb/LHCb/-/merge_requests/3806, !1812 (@amathad)
- ~Configuration ~Tracking | RecoConf change default of light reconstruction of fastReco to false, !1860 (@gunther)
- ~Configuration ~Calo ~Conditions | Fix CALO condition paths for DD4HEP, !1868 (@chenjia)
- ~Tracking | Example options for PrParameterisationData, !1894 (@gunther)
- ~Tracking | Add test for LongMuon tracks in PrKF, !1774 (@pherrero)
- ~Tracking ~FT ~Monitoring | Add FTTrackMonitor to data reconstruction monitoring, !1867 (@gunther)
- ~Tracking ~Monitoring | Monitor scifi hits with tracks, !1834 (@gunther)
- ~Calo | Fix HcalDigits location, !1829 (@jmarchan)
- ~Persistency | Make PatPV3D work with packing locations, !1849 (@mstahl)
- ~Monitoring | Add monitors of correlations of velo, scifi and muon hits, !1862 (@sstahl)
- ~Monitoring | Update default monitors, !1820 (@lutoscan)
- Update References for: Rec!3201 based on lhcb-master-mr/6349, !1902 (@lhcbsoft)
- Fixed the particle combiner of chic and kstar in x0_builder.py, !1896 (@yihou)
- Update References for: LHCb!3863, Moore!1874 based on lhcb-master-mr/6323, !1895 (@lhcbsoft)
- Update References for: LHCb!3876, Rec!3195, Moore!1885 based on lhcb-master-mr/6302, !1891 (@lhcbsoft)
- Add algorithm to combine hlt1 and hlt2 routing bits and rework RoutingBitsWriter, !1874 (@sstahl)
- Update References for: Rec!2773, Moore!1805 based on lhcb-master-mr/6153, !1864 (@lhcbsoft)
- Dummy lines using Relation Tables algorithms., !1805 (@tnanut)
- Update References for: Allen!1068 based on lhcb-master-mr/6288, !1889 (@lhcbsoft)
- Fix small regression in ref for thor selection legacy test, !1884 (@lhcbsoft)
- Adapt cuts for hadronic interaction lines, !1710 (@rowina)
- Update References for: LHCb!3822, Rec!3152, Moore!1828, Alignment!321, MooreOnline!169, Analysis!926, DaVinci!776, Boole!427 based on lhcb-master-mr/6222, !1881 (@lhcbsoft)
- Update References for: Moore!1846 based on lhcb-master-mr/6197, !1879 (@lhcbsoft)
- Hlt2 van der Meer settings, !1872 (@mstahl)
- Unsupport clang, dbg and dd4hep platforms for ttracks protoparticles test, !1871 (@isanders)
- Prepare Moore v54r1p1, !1870 (@rmatev)
- Add configuration to run TrackMasterFitter (light_reconstruction) without UT, !1846 (@ausachov)
- Use RawBank::View, !1828 (@sesen)
- Update References for: Moore!1477 based on lhcb-master-mr/6151, !1869 (@lhcbsoft)
- Update References for: LHCb!3855 based on lhcb-master-mr/6167, !1865 (@lhcbsoft)
- Update References for: Moore!1855 based on lhcb-master-mr/6133, !1857 (@lhcbsoft)
- Configure test-specific monitoring filenames for Gaudi-Allen tests, !1852 (@raaij)
- Update References for: LHCb!3692, Rec!3036, Allen!940, Moore!1774, Alignment!313 based on lhcb-master-mr/6125, !1856 (@lhcbsoft)
- Update References for: LHCb!3779, Rec!3122, Allen!1000, Alignment!314, Panoramix!122 based on lhcb-master-mr/6066, !1847 (@lhcbsoft)
- Fix CUDA stack build, !1842 (@raaij)

Welcome to Moore's documentation!
=================================

Moore is the LHCb high-level trigger (HLT) application. It is responsible for
filtering an input rate of 40 million collisions per second down to an output
rate of around 100 kHz. It does this in two stages:

1. HLT1, which performs a fast track reconstruction and makes a decision based
   on one- and two-track objects.
2. HLT2, which performs a high-fidelity reconstruction and makes a decision
   based on the full detector read-out information.

This site documents the various aspects of Moore, which is fundametally a group
of Python packages that configure algorithms, tools, data flow, and control
flow in order to run a `Gaudi`_-based application.

.. _Gaudi: https://gitlab.cern.ch/gaudi/Gaudi

.. toctree::
   :caption: User Guide
   :maxdepth: 3

   design/goals
   design/architecture

   reconstruction/hlt1
   reconstruction/hlt2

   tutorials/running
   tutorials/running_over_mc
   tutorials/running_over_expected24_simulation
   tutorials/running_with_ut
   tutorials/debugging
   tutorials/developing
   tutorials/hlt2_line
   tutorials/hlt2_analysis
   tutorials/thor_transition
   tutorials/run_with_reconstruction
   tutorials/hlt1_to_hlt2
   tutorials/streaming
   tutorials/ganga
   tutorials/hlt1_tracking_performance
   tutorials/different_samples
   tutorials/hltefficiencychecker

   selection/hlt2_guidelines
   selection/thor_functors

   tutorials/documentation

.. toctree::
   :caption: API Reference
   :maxdepth: 3

   moore/api_index

   selection/hlt1

   selection/hlt2

   selection/thor_functors_reference

   selection/hlt2_lines_reference

   pyconf/summary

   recoconf/recoconf


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

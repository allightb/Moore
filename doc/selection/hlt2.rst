HLT2 selection API
==================

HLT2 selection.

.. _hlt2_standard_makers:

Standard particle makers
------------------------

.. automodule:: Hlt2Conf.standard_particles
  :members:
  :undoc-members:

Standard primary vertex makers
------------------------------

.. automodule:: RecoConf.reconstruction_objects
  :members: make_pvs
  :undoc-members: make_pvs

.. _thor-selection-algorithms:

Selection algorithms – ThOr
---------------------------

These Python helper functions wrap C++ algorithm configurables that use
:doc:`thor_functors`.

The :doc:`../tutorials/thor_transition` tutorial is a good place to learn more
about ThOr functors if you are unfamiliar with them.

Particle filters
^^^^^^^^^^^^^^^^

Each filter takes a single container of particles as input. They all inherit
their functionality from the ``Pr::Filter`` C++ algorithm.

.. autofunction:: Hlt2Conf.algorithms_thor.ParticleFilter

  An example configuration::

      from GaudiKernel.SystemOfUnits import MeV
      import Functors as F
      from Hlt2Conf.standard_particles import (
          make_long_pions,
          make_pvs,
      )


      pions = make_long_pions()
      pvs = make_pvs()
      filtered = ParticleFilter(
          Input=pions,
          Cut=F.FILTER(F.require_all(
              F.PT > 500 * MeV,
              F.MINIPCHI2(pvs) > 4,
          )),
      )

Particle combiners
^^^^^^^^^^^^^^^^^^

The N-body combiner takes a list of :math:`N` particle containers as input along
with a decay descriptor specifying the single-level decay hypothesis to
reconstruct. They all inherit most of their functionality from the
``NBodyParticleCombiner`` C++ algorithm.

.. autofunction:: Hlt2Conf.algorithms_thor.ParticleCombiner

  Depending on :math:`N`, different cut parameters are made available which
  permit selections on intermediate pre-fit combinations of size :math:`N - 1`,
  :math:`N - 2`, and so on.

  An example two-body configuration::

      from GaudiKernel.SystemOfUnits import GeV, MeV
      import Functors as F
      from Hlt2Conf.standard_particles import (
          make_long_pions,
          make_pvs,
      )


      pions = make_long_pions()
      pvs = make_pvs()
      kshorts = ParticleCombiner(
          Inputs=[pions, pions],
          DecayDescriptor="KS0 -> pi+ pi-",
          CombinationCut=F.require_all(
              F.SUM(F.PT) > 2 * GeV,
              in_range(450 * MeV, F.MASS, 550 * MeV),
          ),
          CompositeCut=F.BPVFDCHI2(pvs) > 2,
      )

  A two-body decay has no intermediate combinations (only 'combinations' where
  :math:`N = 1`, selections on which can use a `ParticleFilter`) and so only
  selections on the two-body pre-fit combination are configurable:
  ``CombinationCut``.

  An example three-body configuration, using the previous two-body combination
  as input::

    dzero = ParticleCombiner(
        Inputs=[kshorts, pions, pions],
        DecayDescriptor="D0 -> KS0 pi+ pi-",
        Combination12Cut=F.MAXDOCACHI2CUT(10.),
        CombinationCut=F.require_all(
            in_range(1500 * MeV, F.MASS, 2500 * MeV),
            F.MAXDOCACHI2CUT(16.),
        ),
        CompositeCut=F.CHI2DOF < 12,
    )

  Now the ``Combination12Cut`` defines a selection on the intermediate ``KS0 pi+``
  combination. The ``CombinationCut`` property defines a selection for
  the full ``KS0 pi+ pi-`` combination.

Helpers
^^^^^^^

.. autofunction:: Functors.require_all
.. autofunction:: Functors.require_any


Selection algorithms – LoKi
---------------------------

These Python helper functions wrap C++ algorithm configurables that use LoKi
functors.

.. note::
  If at all possible you should use ThOr-based algorithms described in
  :ref:`thor-selection-algorithms` rather than LoKi-based variants.

  If you already have LoKi-based HLT2 lines you can follow the
  :doc:`../tutorials/thor_transition` tutorial to learn how to migrate.

Particle filters
^^^^^^^^^^^^^^^^

Each filter takes a single container of particles as input. They all inherit
their functionality from the ``FilterDesktop`` C++ algorithm.

.. autofunction:: Hlt2Conf.algorithms.ParticleFilter

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleFilter`.

.. autofunction:: Hlt2Conf.algorithms.ParticleFilterWithPVs

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleFilter`.

.. autofunction:: Hlt2Conf.algorithms.ParticleFilterWithTMVA

Particle combiners
^^^^^^^^^^^^^^^^^^

Each combiner takes a list of particle containers as input along with a decay
descriptor specifying the single-level decay hypothesis to reconstruct. They all
inherit most of their functionality from the ``CombineParticles`` C++ algorithm.

.. autofunction:: Hlt2Conf.algorithms.ParticleCombiner

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.

.. autofunction:: Hlt2Conf.algorithms.ParticleCombinerWithPVs

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.

.. autofunction:: Hlt2Conf.algorithms.NeutralParticleCombiner

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner` using ``"ParticleAdder"`` as the
  value of the ``ParticleCombiner`` property.

.. autofunction:: Hlt2Conf.algorithms.NeutralParticleCombinerWithPVs

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner` using ``"ParticleAdder"`` as the
  value of the ``ParticleCombiner`` property.

.. autofunction:: Hlt2Conf.algorithms.N3BodyCombiner

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.

.. autofunction:: Hlt2Conf.algorithms.N4BodyCombiner

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.

.. autofunction:: Hlt2Conf.algorithms.N3BodyCombinerWithPVs

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.

.. autofunction:: Hlt2Conf.algorithms.N4BodyCombinerWithPVs

  The equivalent ThOr-based function is
  `Hlt2Conf.algorithms_thor.ParticleCombiner`.


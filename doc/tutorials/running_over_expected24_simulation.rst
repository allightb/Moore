Emulating the trigger on MC for early 2024 data-taking
======================================================

Currently, running centralised MC productions including the trigger steps (HLT1,
HLT2 and sprucing) is not yet supported. Therefore, to make meaningful
comparisons between 2024 data and our simulation in ``expected_2024``
conditions, we will have to run the trigger stages ourselves over
centrally-produced simulation samples. This tutorial gives instructions on how
to do that if working from a ``detdesc`` stack (see the previous tutorial for
set-up instructions). The options files here can also be minimially modified to
run in an AnalysisProduction.

The examples here are based on without-UT reconstruction configurations, as
appropriate for the start of 2024 data-taking whilst central productions are
being readied. They should work from the ``master`` branch of all projects in
the stack. Bear in mind that there is danger of ``master`` diverging from the
configuration being actually used in data-taking - this danger will be removed
once we have central MC productions that reproduce the trigger from a TCK. Until
then, we'll try our best to keep these option aligned - if you spot a
divergence, please update these options to reflect it.

.. tip::
    If lots of the configuration options here are unfamiliar to you, please go
    back to earlier parts of the documentation.

For each of the examples below, there are instructions in the doc string on how
they should be run with an LHCb stack.

HLT1
----

.. literalinclude:: ../../Hlt/Hlt1Conf/options/hlt1_pp_expected_24_without_UT.py

.. note::
    A semi-arbitrary signal MC sample has been chosen for this example. Replace
    this with your own signal MC sample of choice.

HLT2
----

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_pp_expected_24_without_UT.py

.. note::
    Unlike HLT1, here we must select the HLT2 lines that are relevant to this
    decay. All trigger lines in HLT2 could be run, but this would be
    computationally expensive and inefficient. Here we select the relevant
    semileptonic WG lines, which should fire on this decay. They are full-stream
    lines, and so the output should be spruced as shown in the next example. In
    your case you may have Turbo lines (or perhaps Turbo *and* Full lines) that
    are relevant, and this example shows how to add a second stream to the job
    if required. Turbo output should have a ``SprucingPass`` job run over it;
    see the :ref:`spruce_pass` example.

Sprucing
--------

*For use on the output of Full-stream lines only*

.. literalinclude:: ../../Hlt/Hlt2Conf/options/sprucing/spruce_pp_expected_24.py

.. _spruce_pass:

SprucingPass
------------

*For use on the output of Turbo-stream lines only*

.. literalinclude:: ../../Hlt/Hlt2Conf/options/sprucing/sprucepass_pp_expected_24.py

.. note::
    This example **should not be used** on the output of the HLT2 example above,
    as the example above runs Full-stream lines. Use it only on the output of
    Turbo lines, modified appropriately for your use case.
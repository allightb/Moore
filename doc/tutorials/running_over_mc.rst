Running HLT1 and HLT2 over simulation
=====================================

This tutorial assumes that you are working within a local copy of the LHCb
software stack, as introduced in the
:doc:`Developing Moore tutorial <developing>`.

With a compiled stack, the process of running HLT2 over simulation breaks down into two steps:

1. Pick an existing/write a new options file defining the run
2. Run!

Once you have an options file (e.g. at ``Moore/my_dir/my_options.py``), step 2 is as simple as::

    Moore/run gaudirun.py Moore/my_dir/my_options.py

Which will produce a very informative log detailing what happened - hopefully ending with::

    ApplicationMgr                         INFO Application Manager Finalized successfully
    ApplicationMgr                         INFO Application Manager Terminated successfully

and any output files that you requested (more on that below). There are tips on
analysing the log output in the :doc:`Analysing HLT2 output <hlt2_analysis>`
section of the docs.

The following sections below give some instructions on step 1. There are
different sections depending on whether you are running over "old" (produced
before around mid-2022) simulation, or newer simulation samples. The latter are
compatible with the DD4HEP conditions database, while the former are not. In
both cases though, running Moore is done in the same way as shown above.

With the help of these examples, hopefully you will be able to run Moore over
any LHCb simulation sample you can find. However, different samples will
generally require configuration to be passed. The :ref:`extra_stuff` section
goes over a few common cases of different configuration that you may need. See
also the :doc:`Input samples with different attributes <different_samples>`
section of the documentation. If things still aren't working, the
`Upgrade HLT2`_ Mattermost channel is a good place to ask for help.

Finally, the last section of this page points to some modern simulation samples
that you can use.

.. tip::
    ``gaudirun.py`` can accept as many options files as you like e.g.
    ``Moore/run gaudirun.py my_options1.py my_options2.py`` and so on. This is
    useful if you wish to factorise the options: one use case might be that you
    wish to run the same set of HLT2 trigger lines over multiple samples. You
    can have one options file defining only the trigger lines, and then a few
    different options files defining the sample. Running over different samples
    then means changing only the latter options - no code need be duplicated.

Instructions for the stack with "old" simulation
------------------------------------------------

If the simulation sample you wish to run over was produced before mid-2022
(roughly), then the default platform of the LHCb stack will not be compatible.
This is because the stack now uses the DD4HEP condition database, as opposed to
the ``detdesc`` conditions database used before. Therefore, to run HLT2 over
this "old" simulation, we must compile the stack with a (non-default)
``detdesc``-compatible platform. Even in 2024, some simulation conditions are
not quite ready in DD4HEP, so at this time ``detdesc`` is still required for
most simulated samples.

Switching to a detdesc-compatible platform
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Using either an existing stack, or one you have freshly downloaded, you can find
instructions to switch platforms in the "Change the platform" section of the
``lb-stack-setup`` `README`_. A wise choice of platform would be one that is
compiling and testing successfully in the `nightly builds`_ - just look for one
that has the ``+detdesc`` piece to right of the compiler e.g.
``x86_64_v3-centos7-gcc12+detdesc-opt+g``.

With the steps in the README completed, it should be sufficient to ``make
Moore`` again, however if you already have a stack fully compiled, you may have
to do a ``make purge`` first.

With the stack (re-)installed, you just need to write an options file that sets
the configuration of the job you want to run. Writing options files for HLT1 and
HLT2 are the subjects of the next sections.

Options files for running HLT1 over simulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
*See also the* :doc:`HLT1->HLT2 section <hlt1_to_hlt2>` *of the documentation.*

A basic, tested options file for running ``Allen`` (as compiled for CPU and
steered as Gaudi algorithm through Moore) with the LHCb stack is
`hlt1_allen_via_moore_example.py`_:

.. literalinclude:: ../../Hlt/Hlt1Conf/options/hlt1_allen_via_moore_example.py

After the copyright block and the block of imports, the options file then sets
configuration parameters relating to the sample we want to run over (here using
a sample stored in the TestFileDB), how many events to run over, and some
details about the output you wish to receive.

In the case of HLT1, the Allen "sequence" defines the reconstruction
configuration and the trigger lines that will run. The retentions of each line
can be seen in the list of counters for ``GaudiAllenCountAndDumpLineDecisions``,
looking something like::

    GaudiAllenCountAndDumpLineDecisi...    INFO Number of counters : 67
     |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |
     |*"Selected by Hlt1BeamGasDecision"               |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1D2KKDecision"                  |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1D2KPiAlignmentDecision"        |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1D2KPiDecision"                 |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1D2PiPiDecision"                |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonDrellYanDecision"        |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonDrellYan_SSDecision"     |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonDrellYan_VLowMassDecision"|       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonDrellYan_VLowMass_SSDecision"|       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonHighMassAlignmentDecision"|       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonHighMassDecision"        |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonJpsiMassAlignmentDecision"|       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonLowMassDecision"         |       100 |          1 |( 1.000000 +- 0.9949874)% |
     |*"Selected by Hlt1DiMuonNoIPDecision"            |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonNoIP_ssDecision"         |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiMuonSoftDecision"            |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DiPhotonHighMassDecision"      |       100 |          2 |( 2.000000 +- 1.400000)% |
     |*"Selected by Hlt1DisplacedDiMuonAlignmentDecision"|       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DisplacedDiMuonDecision"       |       100 |          1 |( 1.000000 +- 0.9949874)% |
     |*"Selected by Hlt1DisplacedDielectronDecision"   |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1DisplacedLeptonsDecision"      |       100 |          1 |( 1.000000 +- 0.9949874)% |
     |*"Selected by Hlt1ErrorBankDecision"             |       100 |          0 |( 0.000000 +- 0.000000)% |
     |*"Selected by Hlt1GlobalDecision"                |       100 |         17 |( 17.00000 +- 3.756328)% |
     ...

Options files for running HLT2 over simulation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A very simple options file that is tested in the nightlies is
`hlt2_all_lines_with_reco.py`_, which looks like

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_all_lines_with_reco.py

Similarly to above, there are blocks of code for copyright, imports, and
configuration for the sample, number of events to run over and details of the
output sample. Unlike HLT1, where the sequence defines the lines that run, a
``make_lines`` function is commonly used in options files to define what trigger
lines you wish to run.  Here, the easy choice is taken to run all HLT2 lines
registered in ``Moore`` at this time. The final block

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_all_lines_with_reco.py
    :start-at: public_tools

finalises the configuration, ending in a call to ``run_moore``. Note here the line

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_all_lines_with_reco.py
    :start-at: from_file=True
    :end-at: from_file=True

which tells us that the reconstructed information stored in the file is used. In
general, you'll want to set this to ``False`` and run the current HLT2
reconstruction in your job.

A slightly more complicated and more typical options file (that is also tested
in the nightlies, and therefore should work out-of-the-box) is
`hlt2_example.py`_:

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_example.py

The first key difference here is that the PFNs (this time for a signal
simulation rather than minimum bias) are specified directly rather than using
the TestFileDB:

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_example.py
    :start-at: input_files
    :end-at: options.input_files

This requires that the user set also options related to the conditions used in
the simulation - in the previous example these were implicitly accessed from the
TestFileDB. Secondly, only a small selection of HLT2 lines are imported and
used. Finally, note that there is no ``reconstruction.bind`` call around the
``run_moore`` call. At the time of writing, the default reconstruction option
was ``from_file=True``, meaning here the reconstructed information stored in the
file is used to make candidates and selections.  See also the
:doc:`Enable real-time reconstruction <run_with_reconstruction>` page.

.. tip::
    There are many tests of HLT2 running on simulation that run in the nightlies
    every night, and each has a corresponding set of options files. Peruse them
    under ``Moore/Hlt/Hlt2Conf/tests/`` to see a wide range of different
    configurations you can use.

Instructions for the stack with DD4HEP-compatible simulation
------------------------------------------------------------

The above two options files ask to run over simulation that is "old", and
therefore running them with a stack using the default platform (or any platform
that doesn't have `detdesc` in the name) will fail with something like this::

    ApplicationMgr                         INFO Application Manager Finalized successfully
    ApplicationMgr                        ERROR Application Manager Terminated with error code 3

and if you scroll up through the log, you should see other ``ERROR`` or
``FATAL`` messages that caused this error code to be given.

As stated above, more modern simulation should work fine, and the tests in Moore
are gradually migrating to use newer samples. An example of the usage of a
newer sample is the ``hlt2_pp_thor_without_UT.py`` test (code `here
<https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/tests/qmtest/hlt2_pp_thor_without_UT.qmt>`_),
which uses the options file ``mdf_input_and_conds_hlt2.py``:

.. literalinclude:: ../../Hlt/Moore/tests/options/mdf_input_and_conds_hlt2.py

Running with this options file should work fine for DD4HEP and detdesc platforms.
If you are *not* using the TestFileDB, then to run with DD4HEP,
``options.geometry_version`` and ``options.conditions_version`` must be
provided. The geometry version refers to a directory in the Detector project,
for example ``run3/trunk`` refers to the ``trunk`` (main) description of the Run
3 detector description, which lives at ``Detector/compact/run3/trunk``. The
``conditions_version`` corresponds to a Git branch in `lhcb-conddb
<https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/>`_, and the
"conditions" more broadly tell us the state of the detector at a given time,
e.g. alignment of the VELO or some set of calibration parameters [#conddb_paper]_.

.. _extra_stuff:

Extra configuration parameters you may need
-------------------------------------------

Below are a few common extra bits of configuration you may need to get your
Moore job to work. You will have seen some of them in use above. See also
:doc:`Input samples with different attributes <different_samples>`.

input_raw_format
^^^^^^^^^^^^^^^^

See :doc:`Input samples with different attributes <different_samples>`.

Calorimeter raw banks
^^^^^^^^^^^^^^^^^^^^^

MC samples produced with the CondDB tag ``sim-20220614-vc-mu100`` or later (see
date in the tag) were produced with the new CALO raw banks.  Moore expects these
raw banks by default, and so no configuration would be needed for them to be
used in the HLT2 calorimeter reconstruction. If your sample is older, then you
should request for the old calorimeter raw banks to be used. This is done by
adding the lines:

.. code-block:: python

    from RecoConf.calorimeter_reconstruction import make_digits
    make_digits.global_bind(calo_raw_bank=False)

.. note::

    If your file has old CALO raw banks and you do not provide the above lines,
    the calorimeter reconstruction will not find the banks - therefore do no
    CALO reco - **but will not fail**.  It's therefore important to get this
    right if you are reconstructing signals reliant on the calorimeters, for
    example electrons.

Muon geometry decoding version
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Similarly, the ``muon_decoding_version`` has been updated over the years, and is
(as of January 2024) at version ``3`` by default. Most older samples require
version ``2``. If you provide the wrong version, the muon decoding will crash at
runtime, so you will know to add this option. To change the version, add the lines:

.. code-block:: python

    from RecoConf.muonid import make_muon_hits
    make_muon_hits.global_bind(geometry_version=2)

persistreco_version
^^^^^^^^^^^^^^^^^^^

For technical reasons, in July 2023 the structure of ``Moore`` output files was
adjusted in `this <https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/2338>`_
merge request. This becomes relevant when we are running ``Moore`` using the
reconstructed information *in the file*, rather than running the reconstruction
*on-the-fly*. To perform selections using the reconstructed information stored
in the file, ``Moore`` will need to know how the file structure is organised.
The structure is encoded in the ``persistreco_version`` parameter. Altogether,
if you are running with reconstruction from file and the file was produced with
by Moore/Brunel job ran before approximately July 2023, you will need to add the
line

.. code-block:: python

    options.persistreco_version = 0.0

If the reconstructed information in the file is of the latest version, then no
line is needed. Similarly, if you are running the reconstruction *on-the-fly*,
no line is needed.

Modern simulation samples generated for the HLT1 bandwidth division
-------------------------------------------------------------------
*Information in this section is more likely to become outdated than those above.*

A good source of modern simulation samples for both minimum bias and signal MC
are the samples produced to carry out the `HLT1 bandwidth division`_. At the
time of writing (January 2024), the HLT1 bandwidth division for 2024 is underway
in earnest, and lots of simulated samples can be found under
``/eos/lhcb/wg/rta/WP3/bandwidth_division/``. To run over these files, the
following configuration will be needed:

.. code-block:: python

    options.dddb_tag = "dddb-20231017"
    options.conddb_tag = "sim-20231017-vc-md100" # Or mu for "Mag Up"
    options.simulation = True
    options.input_type = 'ROOT'
    options.data_type = 'Upgrade'
    options.input_raw_format = 0.5

The availability and location of these files is not guaranteed to stay the same,
but you may find them a useful resource. If the files have moved/disappeared,
please edit this documentation to reflect this!

.. _README: https://gitlab.cern.ch/rmatev/lb-stack-setup
.. _Upgrade HLT2: https://mattermost.web.cern.ch/lhcb/channels/upgrade-hlt2
.. _nightly builds: https://lhcb-nightlies.web.cern.ch/nightly/lhcb-master/
.. _hlt1_allen_via_moore_example.py: https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt1Conf/options/hlt1_allen_via_moore_example.py
.. _hlt2_all_lines_with_reco.py: https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/options/hlt2_all_lines_with_reco.py
.. _hlt2_example.py: https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/options/hlt2_example.py
.. _HLT1 bandwidth division: https://indico.cern.ch/event/1352423/

.. rubric:: Footnotes

.. [#conddb_paper] https://cds.cern.ch/record/2701404/files/10.1051_epjconf_201921404037.pdf

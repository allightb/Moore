###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Must be run inside a Moore environment.

import os
import re
import sys
import csv
import datetime
import textwrap
import inspect

import Functors as F
import Functors.math as Fmath

MoorePkgVersion = sys.argv[1]  # "master"
RecPkgVersion = sys.argv[2]  # "master"

regex_parse_thor = re.compile(r"^(F\..+?|.+?)(\((.+?)\)|)$")


def read_LoKi_To_ThOr_table(filename):
    mapping = []

    expected_num_columns = 4

    with open(filename, "r") as f:
        csv_reader = csv.reader(f)
        next(csv_reader)  # skip header

        for row in csv_reader:
            assert len(
                [col.replace("`", "").strip() for col in row]
            ) == expected_num_columns, f'Row {[col.replace("`","") for col in row]} has unexpected number of columns'
            loki_functors = [
                loki.strip() for loki in row[0].replace("`", "").split("/")
            ]
            thor_name = row[1].replace("`", "").strip()
            # Skip rows which do not give a corresponding ThOr functor
            if not thor_name:
                continue
            for loki_name in loki_functors:
                mapping.append({
                    "loki_name": loki_name,
                    "thor_name": thor_name,
                    "equal_values": row[2],
                    "comment": row[3],
                })

            assert (
                regex_parse_thor.match(thor_name) is not None
            ), f"No good match to thor table entry {thor_name!r} in {filename}."

    return mapping


def LoKi_To_ThOr_mapping():
    standard_filename = os.path.join(
        os.path.dirname(__file__), "selection/standard_loki_thor_table.csv")

    array_filename = os.path.join(
        os.path.dirname(__file__), "selection/array_loki_thor_table.csv")

    standard = read_LoKi_To_ThOr_table(standard_filename)
    array = read_LoKi_To_ThOr_table(array_filename)

    return standard + array


def generate_LoKiDoc(mapping, namespace, thor_name):
    docs = ""
    thor_name = thor_name.strip()

    matching_loki_functors = set()

    for entry in mapping:
        thor_match = regex_parse_thor.match(entry["thor_name"])

        thortbl = thor_match.group(1).replace("F.", "")
        if thortbl != thor_name:
            continue

        arguments_needed = None
        if len(thor_match.groups()) > 2:
            arguments_needed = thor_match.group(3)

        comment = entry["comment"].strip()

        if arguments_needed is not None:
            comment = (fr" :math:`\equiv` `{namespace}.{thortbl}"
                       f"({arguments_needed})`" +
                       (" -- " + comment if len(comment) > 0 else ""))
        elif len(comment) > 0:
            comment = " -- " + comment

        matching_loki_functors.add("* " f"``{entry['loki_name']}``{comment}")
    matching_loki_functors_str = "\n       ".join(matching_loki_functors)
    if len(matching_loki_functors) > 0:
        docs = f"""
        
    LoKi Equivalent{'s' if len(matching_loki_functors) > 1 else ''}
       {matching_loki_functors_str}


"""
    return docs


def gen_docs(namespaces, ignore_names=[], not_skip=[]):
    mapping = LoKi_To_ThOr_mapping()
    gentime = datetime.datetime.today()

    for name, namespace in namespaces.items():
        # print a heading for the namespace
        print("")
        print(name)
        print("-------------")

        # make a new python function block for package/module.
        print(f".. py:module::  {name}\n")
        for functor_name in dir(namespace):
            if functor_name.startswith("_"):
                # probably not a functor.
                continue
            f = getattr(namespace, functor_name)
            if "@module" in repr(f.__doc__):
                print(
                    f".. {functor_name} skipped (has @module)",
                    file=sys.stderr,
                )
                continue

            # Check if the functor name is written in UPPERCASE
            # and create documentation for functors that contains
            # substrings specified in "not_skip" list
            # Omit the other functions from references
            if not functor_name.isupper() and not any(
                    x in functor_name
                    for x in not_skip) and "math" not in name:
                print(
                    f".. {functor_name} skipped - probably not a functor",
                    file=sys.stderr,
                )
                continue
            if functor_name in ignore_names:
                continue

            # if the object doesn't inherit from FunctorBase we assume
            # it's a functor-like helper function e.g. MAP_INPUT
            # in this case we use sphinx autogeneration
            if not isinstance(f, F.grammar.FunctorBase):
                print(f"\n.. autofunction:: {name}.{functor_name}\n")
                continue

            print("\n")

            coderep = None
            has_cppname = False
            pyrep = None
            if (hasattr(f, "_cppname") and f._cppname is not None
                    and len(f._cppname.strip()) != 0):
                coderep = f"``{f._cppname}`` " + (coderep if
                                                  coderep is not None else "")
                has_cppname = True
            if hasattr(f, "_code") and f._code is not None and not has_cppname:
                coderep = f" ``{f._code.strip()}`` " + (
                    coderep if coderep is not None else "")
            #Composed functors without argument
            if len(str(f).split("', '(")) == 2:
                coderep = None  #not necessary display the C++ string representation
                pyrep = str(f).split("', '(")[1].replace(")')", "").strip()

            # comma-separated list of argument names.
            args = (", ".join([p[0] for p in f._arguments]) if hasattr(
                f, "_arguments") else "")
            print(
                f":: generate {name}.{functor_name}({args})", file=sys.stderr)

            # Start writing the Sphinx function block for this functor.
            print(f".. py:function:: {functor_name}({args})\n")
            print(
                textwrap.indent(
                    inspect.cleandoc(f.__doc__), " " * 4, lambda _: True))
            print("")

            # If we have found the corresponding C++ header, let the reader
            # know about it.
            if coderep is not None:
                print("    C++ Representation")
                print(f"        {coderep}")
                print("")
            if pyrep is not None:
                print("    Python Representation")
                print(f"        {pyrep}")
                print("")

            print(generate_LoKiDoc(mapping, name, functor_name))

            # Add all functor parameters (including template ones) to the docs.
            if hasattr(f, "_arguments"):
                for i, ptup in enumerate(f._arguments):
                    paramname, pardesc, partype = ptup[:3]
                    try:
                        partypename = partype.__name__
                    except:
                        print("")
                    print(f"    :param {partypename} {paramname}: {pardesc}")
            if hasattr(f, "_template_arguments"):
                for i, ptup in enumerate(f._template_arguments):
                    paramname, pardesc, partype = ptup[:3]
                    try:
                        partypename = partype.__name__
                    except:
                        print("")
                    print(
                        f"    :param TemplateParameter-{partypename} {paramname}: {pardesc}"
                    )
        print("")
        print("")


gen_docs({
    "Functors": F,
    "Functors.math": Fmath
},
         ignore_names=[
             "test_bit",
             "top_level_namespace",
             "header_prefix",
             "WrappedBoundFunctor",
         ],
         not_skip=["SSPion"])

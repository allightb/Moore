HLT1 track reconstruction
=========================

Member functions of the ``RecoConf.legacy_rec_hlt1_tracking`` module are documented.
The functions allow a flexible and comprehensive configuration of the :doc:`HLT1 track reconstruction <../reconstruction/hlt1>`.

The central function of this module, which provides access to all types of tracks reconstructd in HLT1 is `make_legacy_rec_hlt1_tracks <RecoConf.legacy_rec_hlt1_tracking.make_legacy_rec_hlt1_tracks>`.
In addition an accessor to fitted forward tracks is provided by `make_legacy_rec_hlt1_fitted_tracks <RecoConf.legacy_rec_hlt1_tracking.make_legacy_rec_hlt1_fitted_tracks>`.

Of special interest are the following higher level functions, to which the user can `bind <PyConf.tonic.bind>` to for switching to a different reconstruction sequence:

- `make_pvs <RecoConf.legacy_rec_hlt1_tracking.make_pvs>` to switch between `make_TrackBeamLineVertexFinderSoA_pvs <RecoConf.legacy_rec_hlt1_tracking.make_TrackBeamLineVertexFinderSoA_pvs>` and `make_PatPV3DFuture_pvs <RecoConf.legacy_rec_hlt1_tracking.make_PatPV3DFuture_pvs>`
- `all_hlt1_forward_track_types <RecoConf.legacy_rec_hlt1_tracking.all_hlt1_forward_track_types>` to switch between `make_SciFiTrackForwarding_tracks` and `make_PrForwardTracking_tracks` (hit/cluster-making algorithms are chosen according to the track reconstruction algorithm)
- in principle also `all_velo_track_types <RecoConf.legacy_rec_hlt1_tracking.all_velo_track_types>`, with `make_VeloClusterTrackingSIMD_tracks <RecoConf.legacy_rec_hlt1_tracking.make_VeloClusterTrackingSIMD_tracks>` being currently the only supported maker

Members of ``RecoConf.legacy_rec_hlt1_tracking``
-------------------------------------

.. automodule:: RecoConf.legacy_rec_hlt1_tracking
  :members:

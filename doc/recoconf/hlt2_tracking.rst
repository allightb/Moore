HLT2 track reconstruction
=========================

Member functions of the ``RecoConf.hlt2_tracking`` module are documented.
The functions in this module allow a flexible and comprehensive configuration of the :doc:`HLT2 track reconstruction <../reconstruction/hlt2>`.

The central function of this module, which provides access to all types of tracks reconstructd in HLT2 is `make_hlt2_tracks <RecoConf.hlt2_tracking.make_hlt2_tracks>`.

Members of ``RecoConf.hlt2_tracking``
-------------------------------------

.. automodule:: RecoConf.hlt2_tracking
  :members:

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
stages:
  - check
  - build
  - docs
  - deploy

variables:
  TARGET_BRANCH: master
  # Parameters for downloading nightlies and building Moore
  BINARY_TAG: x86_64_v2-el9-gcc13-opt
  NO_LBLOGIN: "1" # prevent lbdocker containers to start LbLogin/LbEnv
  LB_NIGHTLY_SLOT: lhcb-master

check-copyright:
  stage: check
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-check-copyright origin/${TARGET_BRANCH} --exclude lhcbproject.yml

check-formatting:
  stage: check
  tags:
    - cvmfs
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    - if [ ! -e .clang-format ] ; then
    -   curl -o .clang-format "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/data/default.clang-format?inline=false"
    -   echo '.clang-format' >> .gitignore
    -   git add .gitignore
    - fi
    - curl -o lb-format "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-format --format-patch apply-formatting.patch origin/${TARGET_BRANCH}
  artifacts:
    paths:
      - apply-formatting.patch
    when: on_failure
    expire_in: 1 week

python-linting:
  stage: check
  tags:
    - cvmfs
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    # Only run the pyflakes linter and a few select pycodestyle errors
    - flake8 --exclude '*.opts.py' --select=F,E71,E9,W1,W6 $(find Hlt -name '*.py')

build:
  stage: build
  image: gitlab-registry.cern.ch/lhcb-docker/os-base/alma9-devel:latest
  tags:
    - cvmfs
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    - build_path=/cvmfs/lhcbdev.cern.ch/nightlies/${LB_NIGHTLY_SLOT}/latest
    - . $build_path/setupSearchPath.sh
    # setupSearchPath.sh only sets CMTPROJECTPATH but we need CMAKE_PREFIX_PATH
    - export CMAKE_PREFIX_PATH=${CMTPROJECTPATH}:${CMAKE_PREFIX_PATH}
    - export LCG_VERSION=$(jq -r '.projects | map(select(.name == "LCG")) | .[0].version' $build_path/slot-config.json)
    - lb-project-init
    - make CMAKEFLAGS=-DLOKI_BUILD_FUNCTOR_CACHE=OFF
    - ln -s ./build.$BINARY_TAG/run run
  # Allow failure as the nightly slot we depend on may have itself failed
  allow_failure: true
  artifacts:
    paths:
      - build.$BINARY_TAG
      - run
    untracked: true
    expire_in: 1 hr

build-docs:
  stage: docs
  image: gitlab-registry.cern.ch/lhcb-docker/os-base/alma9-devel:latest
  tags:
    - cvmfs
  # Change pip's cache directory so we can cache it
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  before_script:
    # As the previous stage is allowed to fail, skip this stage if the previous
    # did not complete successfully (i.e. did not create the `run` symlink)
    - test -L run || { echo "WARNING build job failed, stopping"; exit 77; }
  script:
    # First try producing just the graphs (gaudirun.py). If that fails, Moore
    # is likely incompatible with the last nightly, so fail with a warning.
    - ./run make -C doc graphs || { echo "WARNING build is incompatible with last nightly, stopping"; exit 77; }
    - reasons=
    - ./run python doc/make_functor_docs.py "${CI_COMMIT_SHA:0:8}" "master" > doc/selection/thor_functors_reference.generated.rst || reasons+='ERROR failed functor docs build\n'
    - ./run make -C doc linkcheck || reasons+='ERROR failed link check\n'
    - ./run make -C doc html || reasons+='ERROR failed html generation\n'
    - if [ -n "$reasons" ]; then echo -e $reasons; exit 1; fi
  allow_failure:
    exit_codes: 77
  artifacts:
    paths:
      - doc/_build/html/
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/pip

deploy-docs:
  stage: deploy
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
  variables:
    # Need to ensure that the path up to $CI_COMMIT_REF_NAME exists (the
    # deployment script does `mkdir`, not `mkdir -p`)
    # EOS_PATH: "/eos/user/a/apearce/www/mooredocs/$CI_COMMIT_REF_NAME"
    EOS_PATH: "/eos/project/l/lhcbwebsites/www/projects/moore/$CI_COMMIT_REF_NAME"
    CI_OUTPUT_DIR: "doc/_build/html"
    # Makes sure files on the remote are deleted if missing from the source
    METHOD: "rsync"
  before_script:
    # As the previous stage is allowed to fail, skip this stage if the previous
    # did not complete successfully (i.e. did not create the html directory)
    - test -d $CI_OUTPUT_DIR || { echo "WARNING docs not built, stopping"; exit 77; }
  script:
    - deploy-eos && echo -e "Deployed at\n\n\thttps://lhcbdoc.web.cern.ch/lhcbdoc/moore/$CI_COMMIT_REF_NAME/index.html"
  allow_failure:
    exit_codes: 77

include:
  - project: 'lhcb-rta/reference-update-bot'
    file: 'templates/ref-bot-ci-trigger-template.yml'

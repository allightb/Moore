<?xml version="1.0" ?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
#######################################################
# Purpose: Check that raw data content does not change
#######################################################
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
  <argument name="program"><text>gaudirun.py</text></argument>
   <argument name="prerequisites"><set>
    <tuple><text>test_lbexec_hlt2_pp_2022_reprocessing_data</text><enumeral>PASS</enumeral></tuple>
   </set></argument>
   <argument name="options"><text>
from Configurables import LHCbApp, IODataManager
from Configurables import LHCb__UnpackRawEvent, createODIN, PrintHeader
from Configurables import RawEventDump, RawBankSizeMonitor, ApplicationMgr

from GaudiConf import IOExtension
IOExtension().inputFiles([
    "hlt2_pp_2022_reprocessing_data_output_full.mdf",
    "hlt2_pp_2022_reprocessing_data_output_turboraw.mdf",
    "hlt2_pp_2022_reprocessing_data_output_turcal.mdf",
    "hlt2_pp_2022_reprocessing_data_output_lumi.mdf",
    ])
LHCbApp().DataType = "Upgrade"
IODataManager().DisablePFNWarning = True
ApplicationMgr().TopAlg += [
    LHCb__UnpackRawEvent(BankTypes=['ODIN'], RawBankLocations=["DAQ/RawBanks/ODIN"]),
    createODIN(RawBanks="DAQ/RawBanks/ODIN"),
    PrintHeader(),
    RawEventDump("RawEventDump", DumpData=False,
                 RawBanks=["Rich", "Muon", "DAQ", "ODIN", "HltDecReports", "HltRoutingBits",
                           "HltSelReports", "HltLumiSummary", "FTCluster", "VPRetinaCluster", "Calo", "Plume"]),
    # FIXME DstData's size and content is not reproducible so we must exclude it...
    RawBankSizeMonitor("RawBankSizeMon"),
    ]

</text></argument>
  <argument name="error_reference"><text>../refs/empty.ref</text></argument>
  <argument name="validator"><text>

from GaudiTesting.BaseTest import normalizeExamples, normalizeEOL
from Moore.qmtest.exclusions import skip_initialize, counter_preprocessor

pp = normalizeExamples+normalizeEOL+skip_initialize + LineSkipper([
    "INFO Number of counters : ",
    "SUCCESS 1D histograms in directory",
    "SUCCESS 1D profile histograms in directory",
])
cpp = counter_preprocessor + LineSkipper([
    "Total RawEvent size (bytes)",
    "DstData bank size (bytes)",
])

</text></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
</extension>

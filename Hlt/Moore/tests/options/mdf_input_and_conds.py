###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

options.set_input_and_conds_from_testfiledb('MiniBrunel_2018_MinBias_FTv4_MDF')
options.input_files = list(set(options.input_files))  # remove dups
options.evt_max = 1000

options.use_iosvc = True
options.event_store = 'EvtStoreSvc'

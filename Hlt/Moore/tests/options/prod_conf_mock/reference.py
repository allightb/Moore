###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.decoders import default_ft_decoding_version

from Moore.compat.tests.params import prod_conf_params

default_ft_decoding_version.global_bind(value=6)

options.input_files = prod_conf_params["InputFiles"]
options.dddb_tag = prod_conf_params["DDDBTag"]
options.conddb_tag = prod_conf_params["CondDBTag"]
options.histo_file = prod_conf_params["HistogramFile"]
options.evt_max = prod_conf_params["NOfEvents"]
options.output_file = (prod_conf_params["OutputFilePrefix"] + '.' +
                       prod_conf_params["OutputFileTypes"][0].lower())

options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'
options.input_type = "ROOT"
options.output_type = "ROOT"

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    run_moore(options, public_tools=public_tools)

# prod_conf_params = {
#     "XMLFileCatalog": "pool_xml_catalog.xml",
#     "XMLSummaryFile": "summary.xml",
# }

#!/bin/bash
###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail

# https://docs.python.org/3/using/cmdline.html#envvar-PYTHONHASHSEED
export PYTHONHASHSEED=0

gaudirun.py -n $MOOREROOT/tests/options/prod_conf_mock/{hlt2_pp_default,reference}.py -o prod_conf_ref.opts

gaudirun.py -n $MOOREROOT/tests/options/prod_conf_mock/{hlt2_pp_default,mock}.py -o prod_conf_mock.opts

diffopts.py \
    -e 'MessageSvc,Format' -e 'FileCatalog,Catalogs' -e 'EventSelector,Input' \
    -e 'CopyInputStream,Output' -e 'CounterSummarySvc,.*' -e 'EventClockSvc,EventTimeDecoder' \
    -e 'ApplicationMgr,ExtSvc' \
    prod_conf_{ref,mock}.opts

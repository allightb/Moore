###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""The bare minimum required to run Moore.

This comprises

1. A function which returns the list of DecisionLine objects to run.
2. Input data configuration. Although the list of input files can be empty,
   metadata like database tags and file type must still be specified, as the
   configuration is ill-defined without these values.
"""
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as IntDataProducer

from Moore import options, run_moore
from Moore.lines import Hlt1Line


def lines():
    return [
        Hlt1Line(name="Hlt1Passthrough", algs=[IntDataProducer()], prescale=1)
    ]


options.input_type = 'ROOT'
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

run_moore(options, lines)

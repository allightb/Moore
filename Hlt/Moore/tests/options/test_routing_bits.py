###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

import GaudiPython

from Configurables import (
    HistogramPersistencySvc,
    IODataManager,
    LHCbApp,
)
from GaudiConf import IOHelper


def routing_bits(rbbanks):
    """Return a list with the 96 routing bit values."""
    assert rbbanks.size() == 1  # stop if we have multiple rb banks
    d = rbbanks[0].data()  # get only bank
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    return list(map(int, reversed(bits)))


parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
args = parser.parse_args()

# Configure basic application with inputs
LHCbApp(EvtMax=1, DataType="Upgrade", Simulation=True)
IOHelper("MDF").inputFiles([args.input_mdf])
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
gaudi.run(1)
LHCb = GaudiPython.gbl.LHCb

found_events = bool(TES["/Event"])
found_set_routing_bit = False
found_routing_bits_rawbank = False
while TES["/Event"]:
    rawevent = TES["/Event/DAQ/RawEvent"]
    rbbanks = rawevent.banks(LHCb.RawBank.HltRoutingBits)
    if rbbanks.size() > 0:
        found_routing_bits_rawbank = True

        if any(rb == 1 for rb in routing_bits(rbbanks)):
            found_set_routing_bit = True

    gaudi.run(1)

if not found_events:
    print("Test ERROR No events found.")
if not found_routing_bits_rawbank:
    print("Test ERROR No routing bits raw bank found in any event.")
if not found_set_routing_bit:
    print("Test ERROR No routing bit set in any event.")

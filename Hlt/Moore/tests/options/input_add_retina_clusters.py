###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

options.input_files = [
    'root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2jpsi/digi/SMOG2jpsi_0-Extended.digi'
]
options.input_type = "ROOT"
options.evt_max = -1
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-mu100'
options.simulation = True

options.output_file = 'test.digi'
options.output_type = 'ROOT'

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.calorimeter_reconstruction import make_digits

default_ft_decoding_version.global_bind(value=6)
make_digits.global_bind(calo_raw_bank=False)

options.set_input_and_conds_from_testfiledb(
    'upgrade-magup-sim10-up08-digi15-up04-30000000-velo_open-digi')
options.evt_max = 100

###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Hlt/Moore
---------
#]=======================================================================]

gaudi_install(PYTHON)
gaudi_generate_confuserdb(Moore.compat.Configuration)

gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/python)
gaudi_add_tests(QMTest)

# Currently the tests on MC only work with DetDesc but they should be moved to DD4hep
if(USE_DD4HEP)
    # disable DetDesc specific tests
    set_property(
        TEST
            Moore.test_lbexec_allen_hlt1
            Moore.test_lbexec_hlt2
            Moore.test_lbexec_hlt2_pp_commissioning
        PROPERTY
            DISABLED TRUE
    )
else()
    # disable DD4HEP specific tests
    set_property(
        TEST
            Moore.test_lbexec_hlt2_pp_commissioning_data
            Moore.test_lbexec_hlt2_pp_2022_reprocessing_data
            Moore.test_lbexec_hlt2_pp_2022_reprocessing_data_check
            Moore.test_lbexec_hlt2_pp_2022_reprocessing_data_check_output
        PROPERTY
            DISABLED TRUE
    )
endif()

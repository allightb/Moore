###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PRConfig.TestFileDB import test_file_db

test_entry = test_file_db["upgrade_DC19_01_MinBiasMD"]

# Properties of the ProfConf configurable.
prod_conf_params = {
    "OptionFormat": "",
    "InputFiles": test_entry.filenames,
    "OutputFilePrefix": "Moore",
    "OutputFileTypes": ["DST"],
    "XMLFileCatalog": "pool_xml_catalog.xml",
    "XMLSummaryFile": "summary.xml",
    "HistogramFile": "hist.root",
    "DDDBTag": test_entry.qualifiers["DDDB"],
    "CondDBTag": test_entry.qualifiers["CondDB"],
    "DQTag": "",
    "NOfEvents": 10,
}

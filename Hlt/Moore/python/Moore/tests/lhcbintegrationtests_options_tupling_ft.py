###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Moore options to run LHCbIntegration test for Tupling_FT
from Moore import Options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import make_hlt2_lines
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.hlt2_global_reco import (
    reconstruction as reconstruction_from_reco,
    make_light_reco_pr_kf_without_UT,
)
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.muonid import make_muon_hits

from PyConf.Algorithms import (VPRetinaFullClusterDecoder,
                               VeloRetinaClusterTrackingSIMD)

from RecoConf.legacy_rec_hlt1_tracking import (
    make_velo_full_clusters, make_VeloClusterTrackingSIMD, make_RetinaClusters,
    make_reco_pvs, make_PatPV3DFuture_pvs)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT)


def hlt2_ft(options: Options):
    all_lines = {}
    default_lines = [
        'BdToDmPi_DmToPimPimKp'  #Bd->DPi
    ]
    extra_config = {'flavour_tagging': ['BdToDmPi_DmToPimPimKp']}
    make_hlt2_lines(
        line_dict=all_lines,
        all_lines=default_lines,
        extra_config=extra_config)

    def make_lines():
        lines = [builder() for builder in all_lines.values()]
        return lines

    public_tools = [
        trackMasterExtrapolator_with_simplified_geom(),
        stateProvider_with_simplified_geom()
    ]
    default_ft_decoding_version.global_bind(value=6)
    make_muon_hits.global_bind(geometry_version=3)


    with reconstruction.bind(from_file=False),\
        reconstruction_from_reco.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
        make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        make_VeloClusterTrackingSIMD.bind(algorithm=VeloRetinaClusterTrackingSIMD, SkipForward=4),\
        get_UpgradeGhostId_tool_no_UT.bind(velo_hits=make_RetinaClusters),\
        make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClusterDecoder), \
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        make_PatPV3DFuture_pvs.bind(velo_open=False):
        return run_moore(options, make_lines, public_tools)

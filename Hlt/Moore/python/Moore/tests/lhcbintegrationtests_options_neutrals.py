###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Moore options to run LHCbIntegration test for neutrals
from Moore import Options, run_moore
from Hlt2Conf.lines.rd.b_to_ll_hlt2 import Hlt2RD_BToEEGamma
from RecoConf.muonid import make_muon_hits
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.calorimeter_reconstruction import make_digits


def neutrals(options: Options):
    def make_lines():
        return [Hlt2RD_BToEEGamma()]

    public_tools = [stateProvider_with_simplified_geom()]
    make_muon_hits.global_bind(geometry_version=2)
    make_digits.global_bind(calo_raw_bank=False)
    with reconstruction.bind(from_file=False):
        return run_moore(
            options, make_lines, public_tools, exclude_incompatible=False)

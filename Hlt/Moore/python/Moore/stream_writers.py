###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from shutil import copy
import logging

from PyConf import configurable
from PyConf.Algorithms import (
    Gaudi__Hive__FetchLeavesFromFile, SelectiveCombineRawBankViewsToRawEvent,
    CombineRawBankViewsToRawEvent, RawEventSimpleMover, AddressKillerAlg)
from PyConf.components import force_location
from PyConf.application import (MDF_KEY, ROOT_KEY, mdf_writer, online_writer,
                                root_copy_input_writer, default_raw_banks,
                                root_writer)
from PyConf.utilities import ConfigurationError

from .config_tools import unique
from .streams import bank_types_for_detectors, get_bank_by_sourceid
from .streams import HLT1_REPORT_RAW_BANK_TYPES, HLT2_REPORT_RAW_BANK_TYPES

log = logging.getLogger(__name__)

ONLINE = 'Online'
ROOT_RAW_EVENT_LOCATION = '/Event/DAQ/RawEvent'

sim_veto_list = [
    '/Event/pSim', '/Event/Calo', '/Event/Unstripped', '/Event/HC',
    '/Event/Tracker', '/Event/PersistReco', '/Event/Velo', '/Event/Muon',
    '/Event/Rich', '/Event/Trigger', '/Event/pRec', '/Event/Rec',
    '/Event/DAQ/RawEvent'
]


@configurable
def stream_writer(options,
                  stream,
                  process,
                  propagate_mc,
                  analytics=False,
                  hlt_raw_banks=[],
                  routing_bits=[],
                  dst_data=[],
                  dec_reports=None,
                  extra_locations=[],
                  write_all_input_leaves=True):
    """Return node for writing output for a stream.

    These algorithms are not functional and must be explicitly scheduled,
    typically at the very end of the control flow.

    Args:
        options: Moore options
        stream (str): The name of the output stream. Will prefix `path` in the name of output file.
        process (str): "hlt1", "hlt2", "spruce" or "pass".
        propagate_mc (bool): If True and process is "spruce" or "pass", enable propagation of MC objects.
        analytics (bool): If True don't return wihtout writing output
        hlt_raw_banks (dict of bank type: list or dict of data handles ): banks to be persisted that are not detector raw banks
        dec_reports (HltDecReport): Dec reports needed for selective raw event combiner
        extra_locations (list of str): Extra locations for ROOT file
            persistence. Ignored for MDF files. Defaults to [].

    Returns:
        (post_algs): Lists of configured algorithms that
            should be scheduled at the end

    """
    writers = []

    if not options.output_file and not options.output_type == ONLINE and not analytics:
        return writers

    output_location = ROOT_RAW_EVENT_LOCATION
    if process == "hlt1":
        consolidate_views, veto_list = write_hlt1(
            stream, hlt_raw_banks, routing_bits, options.output_type)
    elif process == "hlt2":
        consolidate_views, veto_list = write_hlt2(
            stream, hlt_raw_banks, routing_bits, dst_data, dec_reports,
            options.output_type)
    elif process == "spruce":
        consolidate_views, veto_list = write_spruce(stream, hlt_raw_banks,
                                                    dst_data, dec_reports)
        output_location = consolidate_views.RawEvent
    elif process == "pass":
        consolidate_views, veto_list = write_pass(
            stream, hlt_raw_banks, dec_reports, options.input_manifest_file,
            options.output_manifest_file)
        output_location = consolidate_views.RawEvent

    writers.append(consolidate_views)

    if analytics:
        return writers

    # TODO do not use the magic "default" but perhaps just "" instead.
    stream_part = stream.name if stream.name != "default" else ""
    full_fname = options.output_file.format(stream=stream_part)
    if stream.name != "default" and stream_part.lower(
    ) not in full_fname.lower() and options.output_type != ONLINE:
        raise ConfigurationError("{stream.name} must be part of output_file")

    # TODO: Remove one case once lbexec and gaudirun.py agree on a convention.
    if options.output_type in [MDF_KEY, "RAW"]:
        writers.append(mdf_writer(full_fname, consolidate_views.RawEvent))
    elif options.output_type == ONLINE:
        writers.append(
            online_writer(
                consolidate_views.RawEvent,
                enable_tae=stream.name.upper() == "HLT2CALIB",
            ))
    elif options.output_type == ROOT_KEY:
        if output_location is ROOT_RAW_EVENT_LOCATION:
            # Output RawEvent Location is same as input RawEvent location
            # For hlt1/2 when output is ROOT and input format is < 4.0
            # We can't declate this as output so move the original event to another location before putting the new event to the output location
            raw_event_combiner = RawEventSimpleMover(
                RawEventInput=consolidate_views.RawEvent,
                RawEventOutput=output_location)
            writers.append(raw_event_combiner)

        # This part is only meant for simulation
        # always false for data and by default true for simulation
        if write_all_input_leaves and propagate_mc:
            # Copy the locations from the input when the output is ROOT
            input_leaves = Gaudi__Hive__FetchLeavesFromFile()
            writers.append(input_leaves)

            # Make the entire datastore anonymous. All persistent info of all addresses is entirely removed.
            # otherwise, root_writer saves a link to the input event/file
            writers.append(AddressKillerAlg())

            writers.append(
                root_copy_input_writer(
                    full_fname,
                    input_leaves, [output_location] + extra_locations,
                    tes_veto_locations=veto_list))
        else:
            # Make the entire datastore anonymous. All persistent info of all addresses is entirely removed.
            # otherwise, root_writer saves a link to the input event/file
            writers.append(AddressKillerAlg())
            writers.append(root_writer(full_fname, [output_location]))

    else:
        raise NotImplementedError()

    return writers


def write_hlt1(stream, hlt_raw_banks, routing_bits, output_type):

    # detector banks requested by stream (FIXME: does this really happen in hlt1)
    raw_banks = stream.raw_banks

    # report banks from reports_writers
    raw_banks += hlt_raw_banks

    # routing bit bank from reports_writers
    raw_banks += routing_bits[stream.name]

    # when writing dst for MC, raw events not to be copied
    # (needed only for old brunel files)
    veto_list = get_events(raw_banks)

    consolidate_views = CombineRawBankViewsToRawEvent(
        name="CombineRawBanks_for_" + stream.name,
        RawBankViews=raw_banks,
        outputs={'RawEvent': force_location(f"/Event/{stream.name}/RawEvent")})
    return consolidate_views, veto_list


def write_pass(stream, hlt_raw_banks, dec_reports, input_manifest_file,
               output_manifest_file):

    # raw banks requested by the stream (FIXME: does this really happen for pass?)
    raw_banks = stream.raw_banks
    # Copy DstData from Hlt2
    raw_banks += [default_raw_banks("DstData")]
    # report raw banks created
    raw_banks += hlt_raw_banks

    # report raw banks from Hlt1 and Hlt2
    for banktype in HLT1_REPORT_RAW_BANK_TYPES:
        raw_banks += [get_bank_by_sourceid("Hlt1", banktype)]

    # For lumi passthrough need HltLumiSummary.
    # These are only present for lumi_counter_lines that are not streamed for normal physics pass through
    # Asking for a bank that is not present has no consequences
    raw_banks += [get_bank_by_sourceid("Hlt1", "HltLumiSummary")]
    raw_banks += [get_bank_by_sourceid("Hlt2", "HltLumiSummary")]

    for banktype in HLT2_REPORT_RAW_BANK_TYPES:
        raw_banks += [get_bank_by_sourceid("Hlt2", banktype)]

    print(f"{stream} will have the following banks {raw_banks}")

    # Copy the manifest file from Hlt2
    if input_manifest_file and output_manifest_file:
        copy(input_manifest_file, output_manifest_file)

    # Use SelectiveCombineRawBankViewsToRawEvent once proper streaming is implemented
    consolidate_views = CombineRawBankViewsToRawEvent(
        name="CombineRawBanks_for_" + stream.name,
        RawBankViews=raw_banks,
        outputs={'RawEvent': force_location(f"/Event/{stream.name}/RawEvent")})
    return consolidate_views, sim_veto_list


def write_hlt2(stream, hlt_raw_banks, routing_bits, dst_data, dec_reports,
               output_type):

    # For hlt2, which banks written depends on the lines fired
    # make a map of line name:  bank locations
    map_lines_det_raw_banks = {}

    # List of all banks for all lines
    line_raw_banks = []

    for line in stream.lumi_lines:

        # Lumi lines save HltRoutingBits and ODIN banks, nothing else
        rb_to_persist = routing_bits[stream.name]
        rb_to_persist += [default_raw_banks("ODIN")]

        map_lines_det_raw_banks[line.decision_name] = tuple(
            rb.location for rb in rb_to_persist)

        line_raw_banks += rb_to_persist

    for line in stream.lumi_counter_lines:

        # detector raw banks requested by the line
        rb_to_persist = get_detector_banks(line.raw_banks)

        # detector raw banks requested by the stream
        rb_to_persist += stream.raw_banks

        # report raw banks
        rb_to_persist += hlt_raw_banks

        rb_to_persist += routing_bits[stream.name]

        for banktype in HLT1_REPORT_RAW_BANK_TYPES:
            rb_to_persist += [get_bank_by_sourceid("Hlt1", banktype)]

        # Lumi counter lines also persist LumiSummaries from Hlt1 and Hlt2
        rb_to_persist += [get_bank_by_sourceid("Hlt1", "HltLumiSummary")]
        rb_to_persist += [line.output_producer.OutputView]

        rb_to_persist = unique(rb_to_persist)
        map_lines_det_raw_banks[line.decision_name] = tuple(
            rb.location for rb in rb_to_persist)

        line_raw_banks += rb_to_persist

    for line in stream.physics_lines:

        # detector raw banks requested by the line
        rb_to_persist = get_detector_banks(line.raw_banks)

        # detector raw banks requested by the stream
        rb_to_persist += stream.raw_banks

        # report raw banks created or from previous step
        rb_to_persist += hlt_raw_banks

        # Physics lines have a DstData bank
        rb_to_persist += dst_data

        rb_to_persist += routing_bits[stream.name]

        # report raw banks from Hlt1
        for banktype in HLT1_REPORT_RAW_BANK_TYPES:
            rb_to_persist += [get_bank_by_sourceid("Hlt1", banktype)]

        rb_to_persist = unique(rb_to_persist)
        map_lines_det_raw_banks[line.decision_name] = tuple(
            rb.location for rb in rb_to_persist)

        line_raw_banks += rb_to_persist

    line_raw_banks = unique(line_raw_banks)

    # when writing dst for MC, raw events should not to be copied
    # (needed only for old brunel files)
    veto_list = get_events(line_raw_banks)

    consolidate_views = SelectiveCombineRawBankViewsToRawEvent(
        name="SelectiveCombineRawBanks_for_" + stream.name,
        DecReports=dec_reports,
        MapLinesRawBanks=map_lines_det_raw_banks,
        RawBankViews=line_raw_banks,
        outputs={'RawEvent': force_location(f"/Event/{stream.name}/RawEvent")})

    return consolidate_views, veto_list


def write_spruce(stream, hlt_raw_banks, dst_data, dec_reports):

    # For Spruce, which banks written depends on the lines fired
    # make a map of line name:  bank locations
    map_lines_det_raw_banks = {}

    # all banks for all lines
    line_raw_banks = []
    for line in stream.physics_lines:
        # detector raw banks requested by the line
        rb_to_persist = get_detector_banks(line.raw_banks)

        # detector raw banks requested by the stream
        rb_to_persist += stream.raw_banks

        # report raw banks created or from previous step
        rb_to_persist += hlt_raw_banks

        # Physics lines have a DstData bank
        rb_to_persist += dst_data

        # report raw banks from Hlt1 and Hlt2
        for banktype in HLT1_REPORT_RAW_BANK_TYPES:
            rb_to_persist += [get_bank_by_sourceid("Hlt1", banktype)]

        for banktype in HLT2_REPORT_RAW_BANK_TYPES:
            rb_to_persist += [get_bank_by_sourceid("Hlt2", banktype)]

        rb_to_persist = unique(rb_to_persist)
        map_lines_det_raw_banks[line.decision_name] = tuple(
            rb.location for rb in rb_to_persist)

        line_raw_banks += rb_to_persist
    line_raw_banks = unique(line_raw_banks)
    print(f"{stream} will have the following banks {line_raw_banks}")
    consolidate_views = SelectiveCombineRawBankViewsToRawEvent(
        name="SelectiveCombineRawBanks_for_" + stream.name,
        DecReports=dec_reports,
        MapLinesRawBanks=map_lines_det_raw_banks,
        RawBankViews=line_raw_banks,
        AllowSameSourceID=allow_rawbank_same_source_id_only_for_bad_2023_data(
        ),
        outputs={'RawEvent': force_location(f"/Event/{stream.name}/RawEvent")})

    return consolidate_views, sim_veto_list + ["/Event/HLT2"]


def get_detector_banks(banks):
    return [default_raw_banks(rb) for rb in bank_types_for_detectors(banks)]


def get_events(banks):
    banks_not_in_same_event = []
    for b in banks:
        if "UnpackRawEvent" in b.producer.name:
            banks_not_in_same_event.append(
                b.producer.inputs["RawEventLocation"].location)

    if ROOT_RAW_EVENT_LOCATION in banks_not_in_same_event:
        banks_not_in_same_event.remove(ROOT_RAW_EVENT_LOCATION)
    return banks_not_in_same_event


@configurable
def allow_rawbank_same_source_id_only_for_bad_2023_data(allow=False):
    """Part of 2023 data were processed with a bug in the HLT2 configuration resulting into some rawbanks being saved twice. This needs a special treatment where same sourceID is allowed and such a banks are then compared and filtered when combined into a new rawevent
    """
    return allow

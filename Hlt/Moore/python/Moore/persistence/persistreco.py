###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for persisting the entire HLT2 reconstruction.

The `persistreco_line_outputs` method defines what we mean by "PersistReco".
The data handles returned by that method point to the containers that will be
persisted if a PersistReco-enabled HLT2 line fires.

Some objects which are anyhow persisted as part of the usual line output, such
as primary vertices, are treated different if the line has PersistReco enabled.
See the cloning configuration for those differences.
"""

from PyConf.packing import persistable_locations


def persistreco_line_outputs(reco_objs):
    """Return a dict of data handles that defines reconstruction to be persisted for 'persistreco'
       given the 'persistreco_version'
    """
    prdict = {}

    for key, val in reco_objs.items():
        if val:
            if key in persistable_locations(): prdict[key] = val

    return prdict

###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Module providing functions to enable TISTOS functionality in Sprucing and downstream

To be able to TISTOS in user jobs running on Sprucing output, HLT2 candidates of lines
going into the Full stream need to be persisted. In order to be efficient with disk space
only the information of HLT2 candidates needs to be extracted from the HLT2 DstData raw bank.
This module provides the necessary functions.

"""
from .particle_moving import CopyParticles
from PyConf.tonic import configurable
from PyConf.reading import get_particles, upfront_decoder


@configurable
def list_of_full_stream_lines(lines=["Hlt2Topo2Body", "Hlt2Topo3Body"]):
    return lines


def make_reduced_hlt2_candidates():
    movers = []
    list_of_copied_info = []
    lines_for_TISTOS = list_of_full_stream_lines()
    print(
        f"NOTE: Persisting TISTOS info for following lines {lines_for_TISTOS}")

    with upfront_decoder.bind(source="Hlt2"):

        for line in lines_for_TISTOS:
            hlt2_particles = get_particles(
                f"/Event/HLT2/{line}/Particles",
                WriteEmptyContainerIfBufferNotKnown=True)
            mover = CopyParticles(hlt2_particles,
                                  ["HLT2", "TISTOS", f"{line}"])
            movers += [mover]
            list_of_copied_info += mover.outputs.values()

    return movers, list_of_copied_info

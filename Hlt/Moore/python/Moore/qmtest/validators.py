###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re


def hlt_decision_sum(stdout):
    m = re.search(r'NONLAZY_OR: hlt_decision .*Sum=(\d+)', stdout)
    if m:
        return int(m.group(1))


def at_least_one_decision(stdout, result, causes):
    """Test that at least one event fires."""
    n = hlt_decision_sum(stdout)
    if n is None:
        causes.append('hlt_decision counter not found')
    else:
        result['hlt_decision_sum'] = result.Quote(str(n))
        if n < 1:
            causes.append('no positive decisions found')

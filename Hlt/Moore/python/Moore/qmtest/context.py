###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import hashlib
import os
import re
import subprocess
try:
    from urllib.parse import urlparse
except ImportError:  # Python 2 compatibility
    from urlparse import urlparse


def xrdcp_part(input_url, output_fn, size):
    """Partially download an xrootd file.

    Args:
        input_url (str): A valid URL for xrdcp.
        output_fn (str): A path on the local filesystem.
        size (int): Maximum number of bytes to download.

    Raises:
        RuntimeError: If xrdcp failed.

    """
    cmd = ["xrdcp", "-s", input_url, "-"]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    # read one byte and check for early failure
    byte1 = p.stdout.read(1)
    retcode = p.poll()
    if retcode is not None:
        raise RuntimeError('xrdcp failed with code {}'.format(retcode))

    with open(output_fn, 'wb') as out:
        out.write(byte1)
        out.write(p.stdout.read(size - 1))

    retcode = p.poll()
    if retcode is not None:
        if retcode != 0:
            raise RuntimeError('{} failed with code {}'.format(cmd, retcode))
    else:
        p.terminate()


def xrdcp(input_url, output_fn):
    """Download an xrootd file.

    Args:
        input_url (str): A valid URL for xrdcp.
        output_fn (str): A path on the local filesystem.

    Raises:
        RuntimeError: If xrdcp failed.

    """
    cmd = ["xrdcp", "-f", "-s", input_url, output_fn]
    p = subprocess.Popen(cmd)
    retcode = p.wait()
    if retcode != 0:
        raise RuntimeError('{} failed with code {}'.format(cmd, retcode))


def xrd_size(url):
    """Return the size of an xrootd file."""
    a = urlparse(url)
    output = subprocess.check_output(['xrdfs', a.netloc, 'stat', a.path])
    m = re.search(br'Size:\s*([0-9]+)', output)
    if not m:
        raise RuntimeError('could not parse xrdfs output ' + output)
    return int(m.group(1))


def get_fs_avail_space(path):
    """Return the number of usable free bytes on FS containing path."""
    statvfs = os.statvfs(path)
    return statvfs.f_frsize * statvfs.f_bavail


def download_inputs_locally(urls, dest_dir, assertion_endswith, max_size=None):
    """Download raw inputs locally partially or fully.

    Downloads files identified by `urls`. If specified, download until
    `max_size` cumulative size is reached. The last file will be cut
    off.

    Args:
        urls: (xrootd) URLs to download.
        dest_dir: Path to destination directory.
        max_size (int): Limit of the total downloaded file size.

    Returns:
        list: Paths of the local files.

    Raises:
        OSError: If an error occurred while downloading a file.

    """
    size = 0
    local_fns = []
    local_files = {}  # url: (local_fn, size)
    for url in urls:
        if url.startswith('mdf:'):
            url = url[4:]
        if url not in local_files:
            # prepend hash of full URL in case basenames are not unique
            basename = '{}_{}'.format(
                hashlib.md5(url.encode()).hexdigest(), os.path.basename(url))
            assertion = 0
            for ending in assertion_endswith:
                assertion += basename.endswith(ending)
            assert assertion

            dest = os.path.join(dest_dir, basename)
            src_size = xrd_size(url)
            size_to_copy = (min(src_size,
                                int(max_size) - size)
                            if max_size else src_size)
            # check if we don't have the file already locally
            try:
                dest_size = os.path.getsize(dest)
            except OSError:
                dest_size = 0
            # if the local file is missing or smaller, download again
            if dest_size < size_to_copy:
                avail_space = get_fs_avail_space(dest_dir) + dest_size
                if avail_space < size_to_copy:
                    raise RuntimeError(
                        'Destination {!r} will not have enough available '
                        'space (need {:.1f}GB has {:.1f}GB).'.format(
                            dest_dir, size_to_copy / 1024**3,
                            avail_space / 1024**3))
                if size_to_copy < src_size:
                    xrdcp_part(url, dest, size_to_copy)
                else:
                    xrdcp(url, dest)
                dest_size = os.path.getsize(dest)
            try:
                os.chmod(dest, 0o666)
            except OSError:
                pass
            local_files[url] = (dest, dest_size)
        else:
            dest, dest_size = local_files[url]
        local_fns.append(dest)
        size += dest_size
        if max_size and size >= max_size:
            break
    return local_fns


def download_digi_inputs_locally(urls, dest_dir, max_size=None):
    """
    Calls download_inputs_locally specifically for digi or xdigi files.    
    """
    assertion_endswith = ['.digi', '.xdigi']
    return download_inputs_locally(
        urls=urls,
        dest_dir=dest_dir,
        max_size=max_size,
        assertion_endswith=assertion_endswith)


def download_mdf_inputs_locally(urls, dest_dir, max_size=None):
    """
    Calls download_inputs_locally specifically for mdf, mep or raw files.
    """
    assertion_endswith = ['.mdf', '.mep', '.raw']
    return download_inputs_locally(
        urls=urls,
        dest_dir=dest_dir,
        max_size=max_size,
        assertion_endswith=assertion_endswith)

###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import itertools


def streams_dict_to_lines_list(streams_dict):
    """Function to return flat list of DecisionLines from streams dict"""
    return list(itertools.chain(*streams_dict.values()))


def unique(x):
    """Return a list of the unique elements in x while preserving order."""
    return list(dict.fromkeys(x).keys())

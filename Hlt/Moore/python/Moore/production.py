###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import re

from Moore import Options
from Moore.streams import Streams
from PyConf.utilities import ConfigurationError

MC_PASSTHROUGH_LINE_NAME = "Hlt2MCPassThroughLine"


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.match(pattern_to_remove, name) is None
    }
    print("Removed lines: ", len(set(lines_dict) - set(filtered)))
    return filtered


def allen_hlt1_production(options: Options, sequence: str = "hlt1_pp_default"):
    from Moore import run_allen
    from RecoConf.hlt1_allen import allen_gaudi_config

    with allen_gaudi_config.bind(sequence=sequence):
        return run_allen(options)


def hlt2(options: Options, *raw_args):
    args = _parse_args(raw_args)
    return _hlt2(options, _make_all_lines, args.lines_regex,
                 args.require_deployed_trigger_key)


def hlt2_pp_commissioning(options: Options, *raw_args):
    from Hlt2Conf.settings.hlt2_pp_commissioning import make_streams
    args = _parse_args(raw_args)
    return _hlt2(options, make_streams, args.lines_regex,
                 args.require_deployed_trigger_key)


def hlt2_pp_2022_reprocessing(options: Options, *raw_args):
    from Hlt2Conf.settings.hlt2_pp_2022_reprocessing import make_streams
    args = _parse_args(raw_args)
    return _hlt2(options, make_streams, args.lines_regex,
                 args.require_deployed_trigger_key)


def _parse_args(raw_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--lines-regex",
        type=re.compile,
        help="Regex pattern to select which lines to run. "
        f"For simulation, if {MC_PASSTHROUGH_LINE_NAME} is not selected filtering will be applied.",
    )
    parser.add_argument(
        "--require-deployed-trigger-key",
        action='store_true',
        help=
        "When enabled, it is checked that the trigger configuration has been deployed on cvmfs. "
        "In case of failure, the job will crash.",
    )
    return parser.parse_args(raw_args)


def _hlt2(options, make_lines, lines_regex,
          require_deployed_trigger_key=False):
    from Moore import run_moore
    from RecoConf.reconstruction_objects import reconstruction
    from RecoConf import mc_checking
    from Moore.lines import Hlt2Line
    from PyConf.application import metainfo_repos, retrieve_encoding_dictionary
    if require_deployed_trigger_key:
        metainfo_repos.global_bind(repos=[])  # only use repos on cvmfs
        retrieve_encoding_dictionary.global_bind(
            require_key_present=True)  # require key is in repo
    if options.simulation:
        mc_checking.make_links_lhcbids_mcparticles_tracking_and_muon_system.global_bind(
            with_ut=False)

    # Do not configure simplified geometry as it does not exist for DD4HEP
    public_tools = []

    def _line_maker():

        lines = make_lines()

        if options.simulation:
            lines = _flatten_and_deduplicate_lines(lines)
            print(f"Adding {MC_PASSTHROUGH_LINE_NAME} for simulation")

            # FIXME this is a poor way of implementing flagging mode
            #       persistreco=True emulates Brunel...
            def _make_passthrough_line():
                return [
                    Hlt2Line(
                        name=MC_PASSTHROUGH_LINE_NAME,
                        algs=[],
                        prescale=1,
                        persistreco=True)
                ]

            lines.extend(make_lines(_make_passthrough_line))

        if lines_regex:
            if isinstance(lines, dict):
                raise TypeError("--lines-regex is not implemented for" +
                                "multiple output streams")
            n_before = len(lines)
            lines = [
                line for line in lines if lines_regex.fullmatch(line.name)
            ]
            print(
                f"Reduced {n_before} lines to {len(lines)} using {lines_regex}"
            )
        return lines

    with reconstruction.bind(from_file=False):
        config = run_moore(options, _line_maker, public_tools)

    return config


def _make_all_lines(maker=None):
    from Hlt2Conf.lines import all_lines
    from RecoConf.muonid import make_muon_hits

    # Remove lines which require hlt1_filter_code
    to_remove = [
        'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
        'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
        'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
        'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
        'Hlt2IFT_Femtoscopy_LambdaLambda',
        'Hlt2IFT_Femtoscopy_LambdaLambda_lowK', 'Hlt2IFT_Femtoscopy_XiP',
        'Hlt2IFT_Femtoscopy_XiP_lowK', 'Hlt2IFT_Femtoscopy_XiLambda',
        'Hlt2IFT_Femtoscopy_XiLambda_lowK', 'Hlt2IFT_Femtoscopy_XiXi',
        'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP',
        'Hlt2IFT_Femtoscopy_OmegaP_lowK', 'Hlt2IFT_Femtoscopy_OmegaLambda',
        'Hlt2IFT_Femtoscopy_OmegaXi', 'Hlt2IFT_Femtoscopy_OmegaOmega',
        'Hlt2QEE_DiElectronPrompt_PersistPhotons',
        'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
        'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
        'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
        'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
        'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS', 'Hlt2QEE_IncJet10GeV',
        'Hlt2QEE_IncDiJet10GeV', 'Hlt2QEE_IncDiJet15GeV',
        'Hlt2QEE_IncDiJet20GeV', 'Hlt2QEE_IncDiJet25GeV',
        'Hlt2QEE_IncDiJet30GeV', 'Hlt2QEE_IncDiJet35GeV'
    ]
    trunc_lines = all_lines
    for remove in to_remove:
        trunc_lines = remove_lines(trunc_lines, remove)
    print("Number of HLT2 lines {}".format(len(trunc_lines)))

    make_muon_hits.global_bind(geometry_version=3)
    if maker is None:
        return [builder() for builder in trunc_lines.values()]
    else:
        return maker()


def _make_pp_commissioning_lines(*args):
    from Hlt2Conf.settings.hlt2_pp_commissioning import make_streams
    streams = make_streams(*args)
    return _flatten_and_deduplicate_lines(streams)


def _flatten_and_deduplicate_lines(streams):
    # Use a set comprehension to deduplicate lines
    if isinstance(streams, Streams):
        return streams.lines
    elif isinstance(streams, list):
        return streams
    else:
        raise ConfigurationError(
            "The streams list neither Streams not lines list, check configuration"
        )

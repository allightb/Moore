###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, picosecond as ps
from PyConf.Algorithms import Monitor__ParticleRange as Monitor, Monitor__EventVariable
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs


@configurable
def run_default_monitoring(run=True):
    return run


def monitoring(algs, name, mon_vars):
    """Create a list of monitoring algorithms from the control flow (algs).
         The last item in the CF whose producer is a Combiner or ParticleContainerMerger is taken as input to monitor.
         From that particle, mass, pt, eta, (decay time), vertex chi2 and ip chi2 histograms will be filled.

      Args:
          algs (list of Algorithms): Control flow of this line.
          name (str): Name of the calling line.
          monitoring_variables (tuple(str)): Variables to create default monitoring plots for.
             These variables need to be implemented in .monitoring.py

      Returns:
          List of monitoring algorithms.
      """
    monitoring_algs = []
    producer = None

    def get_producer_with_selection(input_producer):
        if "BodyCombiner" in input_producer.typename:
            last_combiner = input_producer
            producer_output = input_producer.OutputParticles
            return producer_output, last_combiner
        elif "FunctionalDiElectronMaker" in input_producer.typename:
            last_combiner = input_producer
            producer_output = input_producer.Particles
            return producer_output, last_combiner
        if "Filter" in input_producer.typename and "Input" in input_producer.inputs:
            filter_input = input_producer.inputs["Input"].producer
            if "BodyCombiner" in filter_input.typename:
                last_combiner = filter_input
            elif "FunctionalDiElectronMaker" in filter_input.typename:
                last_combiner = filter_input
            elif "ParticleMaker" in filter_input.typename:
                last_combiner = input_producer
            elif "Filter" in filter_input.typename and "Input" in input_producer.inputs:
                producer_output, last_combiner = get_producer_with_selection(
                    filter_input)
            elif "ParticleContainerMerger" in filter_input.typename:
                # sometimes there are "InputContainers", but there is also plain "Input"...
                # ... also lists can be nested, so they need to be flattened
                flatten_mergers = lambda merger_inputs: merger_inputs if hasattr(merger_inputs,"producer") else flatten_mergers(merger_inputs[0])
                merger_input = flatten_mergers([
                    v for k, v in input_producer.__dict__["_inputs"].items()
                    if "Input" in k
                ][0])
                producer_output, last_combiner = get_producer_with_selection(
                    merger_input.producer)
            else:
                last_combiner = None
            # overwrite the recursion result
            producer_output = input_producer.Output
            return producer_output, last_combiner
        if input_producer.typename == "ParticleContainerMerger":
            flatten_mergers = lambda merger_inputs: merger_inputs if hasattr(merger_inputs,"producer") else flatten_mergers(merger_inputs[0])
            merger_input = flatten_mergers([
                v for k, v in input_producer.__dict__["_inputs"].items()
                if "Input" in k
            ][0])
            producer_output, last_combiner = get_producer_with_selection(
                merger_input.producer)
            producer_output = input_producer.OutputContainer
            return producer_output, last_combiner
        else:
            return None, None

    for alg in reversed(algs):
        if hasattr(alg, "producer"):
            producer = alg.producer
            producer_output, last_combiner = get_producer_with_selection(
                producer)
            if producer_output is not None and last_combiner is not None:
                break
    if producer is None or last_combiner is None:
        return monitoring_algs

    # parse ranges from the combiner code. we need to get the cuts in parseable format (code_repr)
    prop_dict = last_combiner.__dict__["_properties"]
    comp_cutstring = "" if "CompositeCut" not in prop_dict else prop_dict[
        "CompositeCut"].code_repr()
    comb_cutstring = "" if "CombinationCut" not in prop_dict else prop_dict[
        "CombinationCut"].code_repr()
    # concatenate filter cut if it sits on top of a combiner. for the parsing it doesn't really matter that this is not a real cut string
    if producer.typename == "ParticleRangeFilter" or producer.typename == "ChargedBasicsFilter":
        fltr = producer.__dict__["_properties"]["Cut"].code_repr()
        comp_cutstring += "" if "MVA" in fltr else fltr

    def _parse_range(functor, cut):
        #match the use of in_range with a given functor, and store the numbers in a capturing group (.*?)
        m = re.search(rf"in_range\( (.\S*?), {functor},(.*?) \)", cut)
        return None if m is None else (float(m.group(1)), float(m.group(2)))

    def _parse_limits(functor, cut, default_lo, default_hi):
        # example ( CHI2DOF < 10 ); match the brackets that separate cuts in code_repr() and capture the cut value
        lom = re.search(rf"\( {functor} > (.*?) \)", cut)
        him = re.search(rf"\( {functor} < (.*?) \)", cut)
        if lom is None and him is None:
            return None
        elif lom is None:
            return (default_lo, float(him.group(1)))
        elif him is None:
            return (float(lom.group(1)), default_hi)
        else:
            return (float(lom.group(1)), float(him.group(1)))

    def _parse_all(functor, default_lo, default_hi):
        # try to parse histogram ranges from combiner cuts. range takes precedence over parsing individual limits,
        # composite over combination, defaults are taken as last resort
        rg = _parse_range(functor, comp_cutstring)
        if rg is None:
            rg = _parse_limits(functor, comp_cutstring, default_lo, default_hi)
        if rg is None:
            rg = _parse_range(functor, comb_cutstring)
        if rg is None:
            rg = _parse_limits(functor, comb_cutstring, default_lo, default_hi)
        if rg is None:
            rg = (default_lo, default_hi)
        return rg

    def _parse_mass_functor_from_cut(cut):
        # extrawurst for delta mass and corrected mass
        if "MASS - CHILD(Index=1, Functor=MASS)" in cut:
            return {
                "functor": F.MASS - F.CHILD(1, F.MASS),
                "regex": r"\( MASS - CHILD\(Index=1, Functor=MASS\) \)"
            }
        if re.search(r"MASS( >| <|,)", cut):
            return {"functor": F.MASS, "regex": r"MASS"}
        if "BPVCORRM" in cut:
            return {
                "functor":
                F.BPVCORRM(make_pvs()),
                "regex":
                r"_BPVCORRM(?!ERR).*? _FORWARDARGS\(\) \), _FORWARDARGS\(\) \)"
            }
        else:
            return None

    def _parse_mass_functor():
        # parse the mass functor that is used. composite cut takes precedence over combination cut, delta mass over mass over (corrected mass).
        mass_functor_dict = _parse_mass_functor_from_cut(comp_cutstring)
        if mass_functor_dict is None:
            mass_functor_dict = _parse_mass_functor_from_cut(comb_cutstring)
        if mass_functor_dict is None:
            mass_functor_dict = {"functor": F.MASS, "regex": r"MASS"}
        return mass_functor_dict

    def _pv_filter_in_algs():
        for alg in reversed(algs):
            if "VoidFilter" in str(
                    alg.__dict__.get("_alg_type", None)) and str(
                        alg.__dict__.get("_name")).startswith("require_pvs"):
                return True
        return False

    for mon_var in mon_vars:
        # pt
        if mon_var.lower() == "pt":
            monitoring_algs.append(
                Monitor(
                    name="Monitor_" + name + "_pt",
                    Input=producer_output,
                    Variable=F.PT,
                    HistogramName="/" + name + "/pt",
                    Bins=80,
                    Range=_parse_all(r"\( RHO_COORDINATE @ THREEMOMENTUM \)",
                                     0., 40. * GeV)))
        # eta
        elif mon_var.lower() == "eta":
            monitoring_algs.append(
                Monitor(
                    name="Monitor_" + name + "_eta",
                    Input=producer_output,
                    Variable=F.ETA,
                    HistogramName="/" + name + "/eta",
                    Bins=40,
                    Range=(1.5, 5.5)))
        # mass
        elif mon_var.lower() == "m":
            mass_functor = _parse_mass_functor()
            #TODO: parse decay descriptor to set defaults in _parse_all. Might need https://gitlab.cern.ch/lhcb/Moore/-/issues/289
            monitoring_algs.append(
                Monitor(
                    name="Monitor_" + name + "_m",
                    Input=producer_output,
                    Variable=mass_functor["functor"],
                    HistogramName="/" + name + "/m",
                    Bins=100,
                    Range=_parse_all(mass_functor["regex"], 0., 7. * GeV)))
        # decay time
        elif mon_var.lower() == "dectime":
            monitoring_algs.append(
                Monitor(
                    name="Monitor_" + name + "_dectime",
                    Input=producer_output,
                    Variable=F.BPVLTIME(make_pvs()),
                    HistogramName="/" + name + "/dectime",
                    Bins=100,
                    #VTX_LTIME.bind( BEST_PV.bind( TES(DataHandles=[DataHandle('/Event/TrackBeamLineVertexFinderSoA/OutputVertices')], DataTypes=['LHCb::Event::PV::PrimaryVertexContainer']), _FORWARDARGS() ), _FORWARDARGS() )
                    Range=_parse_all(
                        r"VTX_LTIME.*? _FORWARDARGS\(\) \), _FORWARDARGS\(\) \)",
                        0 * ps, 8 * ps)))
        # vertex chi2
        elif mon_var.lower() == "vchi2":
            monitoring_algs.append(
                Monitor(
                    name="Monitor_" + name + "_vchi2",
                    Input=producer_output,
                    Variable=F.CHI2DOF,
                    HistogramName="/" + name + "/vchi2",
                    Bins=64,
                    Range=_parse_all(
                        r"\( VALUE_OR\(Value=nan\) \@ _CHI2DOF \)", 0., 32.)))
        # ip chi2
        elif mon_var.lower() == "ipchi2":
            if _pv_filter_in_algs():
                monitoring_algs.append(
                    Monitor(
                        name="Monitor_" + name + "_ipchi2",
                        Input=producer_output,
                        Variable=F.BPVIPCHI2(make_pvs()),
                        HistogramName="/" + name + "/ipchi2",
                        Bins=64,
                        # to not spell out BPVIPCHI2.bind( TES(DataHandles=[DataHandle('/Event/TrackBeamLineVertexFinderSoA/OutputVertices')], DataTypes=['LHCb::Event::PV::PrimaryVertexContainer']), _FORWARDARGS() )
                        # *? matches the previous token between zero and unlimited times, as few times as possible, expanding as needed (lazy)
                        Range=_parse_all(r"BPVIPCHI2.*? FORWARDARGS\(\) \)",
                                         0., 1000.)))
        # number of candidates
        elif mon_var.lower() == "n_candidates":
            monitoring_algs.append(
                Monitor__EventVariable(
                    name="Monitor_" + name + "_n_candidates",
                    Variable=F.SIZE(producer_output),
                    HistogramName="/" + name + "/n_candidates",
                    HistogramDef=("Monitor_" + name + "_n_candidates", 0, 100,
                                  100)))
        else:
            raise ValueError(
                f"{mon_var} is not a supported default monitoring histogram")

    return monitoring_algs

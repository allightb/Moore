###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from typing import Optional
from contextlib import contextmanager
from enum import Enum

from GaudiConf.LbExec import Options as DefaultOptions
from PyConf.application import ROOT_KEY
from PyConf.packing import persistreco_version

from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reco_objects_for_spruce import reconstruction as reconstruction_for_spruce, upfront_reconstruction as upfront_reconstruction_for_spruce


class ProcessTypes(str, Enum):
    Hlt1 = "Hlt1"
    Hlt2 = "Hlt2"
    TurboPass = "TurboPass"
    Spruce = "Spruce"
    ReconstructionOnly = "ReconstructionOnly"
    Monitoring = "Monitoring"
    UserDefined = "UserDefined"


class Options(DefaultOptions):
    tck: Optional[int] = None
    process: ProcessTypes = ProcessTypes.Hlt2

    @contextmanager
    def apply_binds(self):
        """
        This function configures the following functions, where their keyword
        arguments are globally bound to the user-specified values.
        This way users do not have to manually configure these functions themselves.
        - reconstruction() from reconstruction_objects
        - reconstruction() from reco_objects_for_spruce
        - upfront_reconstruction() from reco_objects_for_spruce

        """
        if self.process == ProcessTypes.Spruce:
            reconstruction.global_bind(spruce=True, from_file=True)
            reconstruction_for_spruce.global_bind(
                simulation=self.simulation and self.input_type == ROOT_KEY)
            upfront_reconstruction_for_spruce.global_bind(
                simulation=self.simulation and self.input_type == ROOT_KEY)
            persistreco_version.global_bind(version=self.persistreco_version)

        with super().apply_binds():
            yield

###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .lines import Hlt2LuminosityLine, Hlt2LumiCountersLine
from .config_tools import unique
from PyConf.application import default_raw_banks
from PyConf.utilities import ConfigurationError
from PyConf.Algorithms import LHCb__SelectViewForHltSourceID

DETECTORS = ['VP', 'UT', 'FT', 'Rich', 'Muon', 'Plume', 'Calo']

DETECTOR_RAW_BANKS = {
    'VP': ["VP", "VPRetinaCluster"],
    'UT': ["UT", "UTError"],
    'FT': ["FTCluster"],
    'Rich': ["Rich"],
    'Muon': ["Muon", "MuonError"],
    'Plume': ["Plume"],
    'Calo': ["Calo", "CaloError", "EcalPacked", "HcalPacked"],
}

# HltLumiReport bank is not in the list as it is only needed for LumiCounterLines.
HLT1_REPORT_RAW_BANK_TYPES = ['HltDecReports', 'HltSelReports']
HLT2_REPORT_RAW_BANK_TYPES = ['HltDecReports', 'HltRoutingBits']

# default stream bits for Hlt2
# must follow definition in https://gitlab.cern.ch/lhcb-online/lhcb-online-mover/-/blob/main/src/lbMover/routing.py
stream_bits = dict(
    ion=81,
    ionraw=82,
    turboraw=85,
    beamgas=86,
    full=87,
    turbo=88,
    hlt2calib=89,
    turcal=90,
    no_bias=91,
    passthrough=92,
    lumi=93,
    default=64,
)


class Stream(object):
    """Object fully qualifying a Moore stream.

    Args:
        name (str): name of the stream
        routing_bit (int): routing bit for the stream
        lines (list): lines in the stream
        detectors(list): detectors requested by the stream
    """

    def __init__(self,
                 name="default",
                 lines=[],
                 routing_bit=None,
                 detectors=DETECTORS):
        self.name = name
        self.routing_bit = self._set_routing_bit(name, routing_bit)
        self.lines = sorted(lines, key=lambda line: line.name)
        self.physics_lines = self._physics_lines()
        self.lumi_lines = self._lumi_lines()
        self.lumi_counter_lines = self._lumi_counter_lines()
        self.raw_banks = self._set_raw_banks(detectors)

    def _set_routing_bit(self, name, routing_bit):
        if routing_bit:
            return routing_bit
        elif self.name in stream_bits.keys():
            return stream_bits[self.name]
        else:
            return None

    def _set_raw_banks(self, detectors):
        bank_types = bank_types_for_detectors(detectors)
        bank_types.append("ODIN")
        return [default_raw_banks(b) for b in bank_types]

    def _physics_lines(self):
        physics_lines = [l for l in self.lines if l.produces_output]

        physics_lines = [
            l for l in physics_lines
            if not isinstance(l, (Hlt2LuminosityLine, Hlt2LumiCountersLine))
        ]
        return sorted(physics_lines, key=lambda l: l.name)

    def _lumi_lines(self):
        lumi_lines = [
            l for l in self.lines if isinstance(l, Hlt2LuminosityLine)
        ]
        return sorted(lumi_lines, key=lambda l: l.name)

    def _lumi_counter_lines(self):
        lumi_counter_lines = [
            l for l in self.lines if isinstance(l, Hlt2LumiCountersLine)
        ]
        if len(lumi_counter_lines) > 1:
            raise ConfigurationError(
                'Found multiple lumi counter lines: {}'.format(
                    [l.name for l in lumi_counter_lines]))

        return sorted(lumi_counter_lines, key=lambda l: l.name)

    def update(self, new_lines):
        self.lines = new_lines
        self.physics_lines = self._physics_lines()
        self.lumi_lines = self._lumi_lines()
        self.lumi_counter_lines = self._lumi_counter_lines()


class Streams(object):
    """Moore streams for a given process

    Args:
        streams(list): list of stream objects
    """

    def __init__(self, streams):
        self.streams = self._streams(streams)
        self.lines = self._lines()
        self.physics_lines = self._physics_lines()
        self.lumi_lines = self._lumi_lines()
        self.lumi_counter_lines = self._lumi_counter_lines()

    def _streams(self, streams):
        streams = list(
            filter(lambda stream: len(stream.lines) > 0,
                   sorted(streams, key=lambda stream: stream.name)))

        names = [stream.name for stream in streams]
        assert len(streams) == len(
            unique(names)), "Multiple streams with same name found in streams."
        return streams

    def _lines(self):
        """Function to return flat list of DecisionLines from streams"""
        lines = []
        for stream in self.streams:
            lines += stream.lines
        return unique(lines)

    def _physics_lines(self):
        """Function to return flat list of DecisionLines from streams"""
        lines = []
        for stream in self.streams:
            lines += stream.physics_lines
        return unique(lines)

    def _lumi_lines(self):
        """Function to return flat list of LumiLines from streams"""
        lines = []
        for stream in self.streams:
            lines += stream.lumi_lines
        return unique(lines)

    def _lumi_counter_lines(self):
        """Function to return flat list of LumiCountersLines from streams"""
        lines = []
        for stream in self.streams:
            lines += stream.lumi_counter_lines
        return unique(lines)

    def update(self):
        """Function to return flat list of DecisionLines from streams"""
        lines = []
        for stream in self.streams:
            lines += stream.lines
        self.lines = unique(lines)

        physics_lines = []
        for stream in self.streams:
            physics_lines += stream.physics_lines
        self.physics_lines = unique(physics_lines)

        lumi_lines = []
        for stream in self.streams:
            lumi_lines += stream.lumi_lines
        self.lumi_lines = unique(lumi_lines)

        lumi_counter_lines = []
        for stream in self.streams:
            lumi_counter_lines += stream.lumi_counter_lines
        self.lumi_counter_lines = unique(lumi_counter_lines)

    def print(self):
        for stream in self.streams:
            print("stream: ", stream.name, " routing_bit: ",
                  stream.routing_bit)
            print(" physics_lines:        ",
                  [l.name for l in stream.physics_lines])
            print(" lumi_lines:   ", [l.name for l in stream.lumi_lines])
            print(" lumi_counter_lines:   ",
                  [l.name for l in stream.lumi_counter_lines])
            print(" raw_banks:    ", stream.raw_banks)

    def get(self, stream_name):
        filtered = list(filter(lambda s: s.name is stream_name, self.streams))

        if len(filtered) == 1:
            return filtered[0]
        elif len(filtered) == 0:
            raise ConfigurationError(f"{stream_name} not found in streams.")
        else:
            raise ConfigurationError(
                f"Multiple streams with name {stream_name} found in streams.")


def make_default_streams(lines):
    stream = Stream(name="default", lines=lines, detectors=DETECTORS)
    return Streams(streams=[stream])


def get_default_routing_bits(stream):
    """ Define default routing bits via regular expressions. This is currently a placeholder to test the functionality."""
    from Moore.lines import Hlt2LuminosityLine
    # Temporarily hard-code a default mapping of streams to bits, see #268
    routingBits = {}
    # Bit for stream
    if (stream.routing_bit == 94 or stream.routing_bit == 95):
        raise ConfigurationError(
            f"Bits 94 and 95 are reserved. Do not use for stream {stream}.")
    if not stream.routing_bit:
        raise ConfigurationError(
            f"Routing bit is not set for stream {stream.name}.")

    routingBits[stream.routing_bit] = [l.decision_name for l in stream.lines]
    # Lumi bit, always 94
    routingBits[94] = [
        l.decision_name for l in stream.lines
        if isinstance(l, Hlt2LuminosityLine)
    ]
    # Physics bit, always 95
    routingBits[95] = [
        l.decision_name for l in stream.lines
        if not isinstance(l, Hlt2LuminosityLine)
    ]
    # NOTE: we must have at most one line that does not save the HltDecReports banks
    # (the lumi line in physics streams that nanofies). Events exclusively
    # selected by this line must be excluded from bit 95. Events selected by
    # this line must be assigned to bit 94.
    # The idea is that if Hlt2 HltDecReports are present, one can always
    # select based on them and expect raw banks according to the line.

    return routingBits


def get_bank_by_sourceid(source_id, banktype):
    return LHCb__SelectViewForHltSourceID(
        name=f"Select{source_id}{banktype}",
        Input=default_raw_banks(banktype),
        SourceID=source_id).Output


# Lines and streams should ask for which detectors they want the banks for
# to allow additional banks for same detector as they might me needed
def bank_types_for_detectors(detectors=DETECTORS):
    bank_types_for_dets = []
    for det in detectors:
        if det not in DETECTOR_RAW_BANKS.keys():
            raise ValueError(f'{det} is not a valid detector type name!')
        bank_types_for_dets += DETECTOR_RAW_BANKS[det]

    return bank_types_for_dets

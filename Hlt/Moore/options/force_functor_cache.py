###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiKernel.Configurable
from Configurables import ApplicationMgr

# Use ApplicationMgr.Environment instead of os.environ so that these
# settings are preserved when running from dumped (pkl) options.
ApplicationMgr().Environment.update({
    'LOKI_DISABLE_PYTHON': '1',  # Force use of cache for old style functors
    'THOR_DISABLE_JIT': '1',  # Force use of cache for new style functors
})

# Workaround for gaudi/Gaudi#117 that monkey patches gaudirun.py
GaudiKernel.Configurable.expandvars = lambda x: x

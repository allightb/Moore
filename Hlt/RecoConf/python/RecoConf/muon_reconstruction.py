###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable

from PyConf.Algorithms import (
    LHCb__Converters__Track__SOA__fromV1Track as TrackSOAFromV1,
    fromV2MuonPIDV1MuonPID, fromV2MuonPIDV1MuonPIDShared,
    MuonPIDV2ToMuonTracks, MuonPIDContainersSharedMerger as
    MergeMuonPIDsV1Shared, MuonPIDsEmptyProducer, TracksEmptyProducer)

from .hlt2_muonid import make_muon_ids
from RecoConf.core_algorithms import make_unique_id_generator


@configurable
def make_muon_input_tracks(best_tracks,
                           light_reco=False,
                           track_types=['Long', 'Downstream']):
    tracks = dict()
    for track_type in track_types:
        tracks[track_type] = TrackSOAFromV1(
            InputTracks=best_tracks[track_type]['v1']
            if light_reco else best_tracks['v1'],
            InputUniqueIDGenerator=make_unique_id_generator(),
            RestrictToType=track_type).OutputTracks
    return tracks


@configurable
def make_all_muon_pids(tracks, track_types=['Long', 'Downstream']):
    muonPidsConfs = dict()
    # Create the confs for each Muon reco for the different track types (Long, Downstream)
    relevant_track_types = set(track_types) & {'Long', 'Downstream'}
    for track_type, track_container in tracks.items():
        if track_type in relevant_track_types:
            muonPidsConfs[track_type] = make_muon_ids(track_type,
                                                      track_container)
    return muonPidsConfs


def make_conv_muon_pids(muonPidsConfs,
                        best_track_rels,
                        track_types=['Long', 'Downstream'],
                        shared_container=False):
    muon_pids_v1 = dict()
    muon_tracks = dict()
    MuonPIDConverter = fromV2MuonPIDV1MuonPIDShared if shared_container else fromV2MuonPIDV1MuonPID
    for track_type in track_types:
        if track_type in muonPidsConfs.keys():
            # Add muon hits of MuonPID to "muontracks"
            muon_tracks[track_type] = MuonPIDV2ToMuonTracks(
                name="MuonPIDV2ToMuonTracks_" + track_type + "_{hash}",
                InputMuonPIDs=muonPidsConfs[track_type]).OutputMuonTracks
            # Convert to Keyed Container for ProtoParticle
            muon_pids_v1[track_type] = MuonPIDConverter(
                name="fromV2MuonPIDV1MuonPID" + track_type + "_{hash}",
                InputMuonPIDs=muonPidsConfs[track_type],
                InputMuonTracks=muon_tracks[track_type],
                InputTrackRelations=best_track_rels[track_type]).OutputMuonPIDs
        else:
            muon_tracks[track_type] = TracksEmptyProducer(
                name="MuonTracksEmptyProducer_" + track_type + "_{hash}",
                allow_duplicate_instances_with_distinct_names=True).Output
            muon_pids_v1[track_type] = MuonPIDsEmptyProducer(
                name="MuonPIDsEmptyProducer_" + track_type + "_{hash}",
                InputMuonTracks=muon_tracks[track_type],
                allow_duplicate_instances_with_distinct_names=True).Output
    return muon_pids_v1, muon_tracks


def make_merged_muon_pids(muon_pids_v1, location=None):
    merged_pids = MergeMuonPIDsV1Shared(
        InputLocations=list(muon_pids_v1.values()),
        outputs={
            'OutputLocation': location
        }).OutputLocation
    return merged_pids

###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import importlib
from PyConf import configurable
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import (
    LHCb__Converters__Track__v1__fromV2TrackV1TrackVector as
    FromV2TrackV1Track, fromV3TrackV1Track,
    LHCb__Converters__Track__v2__fromV1TrackV2Track as fromV1TrackV2Track,
    GaudiAllenLumiSummaryToRawEvent, VoidFilter)
from RecoConf.core_algorithms import make_unique_id_generator

# Workaround for ROOT-10769
import warnings
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    import cppyy


@configurable
def allen_gaudi_config(sequence='hlt1_pp_default', node_name='hlt1_node'):
    """
    Provide the Allen top-level node
    """
    seq = importlib.import_module('AllenSequences.{}'.format(sequence))
    return getattr(seq, node_name)


def get_allen_line_names(suffix='Decision'):
    """
    Read Allen HLT1 line names from the Allen control node
    Inputs:
      suffix:  a string that is appended to every Allen line node name
    """
    hlt1_config = allen_gaudi_config()
    return [
        child.name.replace('_node', '') + suffix
        for child in hlt1_config['line_nodes']
    ]


def call_allen_raw_reports():
    """
    Configures GaudiAllenReportsToRawEvent transformer to Convert output of
    Allen dec_reporter and sel_report_writer to LHCb::RawBank::View objects.
    """
    from PyConf.Algorithms import GaudiAllenReportsToRawEvent

    hlt1_config = allen_gaudi_config()

    # Find the algorithms for the input required
    dec_reporter = hlt1_config['dec_reporter']
    selreps = hlt1_config['sel_reports']['dev_sel_reports']
    selrep_offsets = hlt1_config['sel_reports']['dev_selrep_offsets']
    routing_bits_writer = hlt1_config['routing_bits']

    return GaudiAllenReportsToRawEvent(
        allen_dec_reports=dec_reporter.dev_dec_reports_t,
        allen_selrep_offsets=selrep_offsets,
        allen_sel_reports=selreps,
        allen_routing_bits=routing_bits_writer.host_routingbits_t)


def make_allen_dec_reports(raw_reports=call_allen_raw_reports):
    """
    Make DecReports objects by decoding LHCb::RawEvent
    """
    decoder = decode_allen_dec_reports(raw_reports)
    return decoder.OutputHltDecReportsLocation


def make_allen_sel_reports(raw_reports=call_allen_raw_reports):
    """
    Make SelReports objects by decoding LHCb::RawEvent
    """
    decoder = decode_allen_sel_reports(raw_reports)
    return decoder.OutputHltSelReportsLocation


def make_allen_decision():
    from PyConf.Algorithms import GaudiAllenFilterEventsLineDecisions

    allen_config = allen_gaudi_config()

    # Find the algorithms for the input required
    global_decision = allen_config['global_decision']

    return GaudiAllenFilterEventsLineDecisions(
        allen_global_decision=global_decision.host_global_decision_t)


def call_allen_lumi_summary():
    """
    Configures GaudiAllenReportsToRawEvent transformer to Convert output of
    Allen dec_reporter and sel_report_writer to LHCb::RawBank::View objects.
    """
    from Functors import math as fmath
    from Functors import EVENTTYPE

    hlt1_config = allen_gaudi_config()

    # Find the algorithms for the input required
    lumi_reco = hlt1_config['lumi_reconstruction']

    lumi_bit = cppyy.gbl.LHCb.ODIN.EventTypes.Lumi
    odin = make_odin()
    odin_filter = VoidFilter(Cut=fmath.test_bit(EVENTTYPE(odin), lumi_bit))

    allen_lumi_summary = GaudiAllenLumiSummaryToRawEvent(
        allen_lumi_summaries=lumi_reco['host_lumi_summaries'],
        allen_lumi_summary_offsets=lumi_reco['host_lumi_summary_offsets'])

    node = CompositeNode(
        'allen_lumi_bank_node',
        combine_logic=NodeLogic.LAZY_AND,
        children=[odin_filter, allen_lumi_summary],
        force_order=True)

    return {
        "node": node,
        "lumi_summary": allen_lumi_summary.OutputLumiSummaryView
    }


def call_allen_decision_logger(gather_selections=None):
    """
    Configure GaudiAllenCountAndDumpLineDecisions to count and report
    Allen line decisions.
    """
    from PyConf.Algorithms import GaudiAllenCountAndDumpLineDecisions

    if gather_selections is None:
        hlt1_config = allen_gaudi_config()

        # Find the algorithms for the input required
        gather_selections = hlt1_config['gather_selections']

    return GaudiAllenCountAndDumpLineDecisions(
        allen_number_of_active_lines=gather_selections.
        host_number_of_active_lines_t,
        allen_names_of_active_lines=gather_selections.
        host_names_of_active_lines_t,
        allen_selections=gather_selections.dev_selections_t,
        allen_selections_offsets=gather_selections.dev_selections_offsets_t,
        Hlt1LineNames=get_allen_line_names())


def decode_allen_dec_reports(raw_reports=call_allen_raw_reports):
    from PyConf.Algorithms import HltDecReportsDecoder

    decoder = HltDecReportsDecoder(
        RawBanks=raw_reports().OutputDecView, SourceID='Hlt1')
    return decoder


def decode_allen_sel_reports(raw_reports=call_allen_raw_reports):
    from PyConf.Algorithms import HltSelReportsDecoder

    decoder = HltSelReportsDecoder(
        RawBanks=raw_reports().OutputSelView,
        DecReports=raw_reports().OutputDecView,
        SourceID='Hlt1')
    return decoder


def make_allen_calo_clusters():
    """
    Configures the adaptor between the calo clusters reconstructed in
    Allen and Gaudi-LHCb CaloClusters.
    """
    from PyConf.Algorithms import GaudiAllenCaloToCaloClusters

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings

    allen_ecal_clusters = hlt1_config['reconstruction']['ecal_clusters']

    ecal_clusters = GaudiAllenCaloToCaloClusters(
        allen_ecal_cluster_offsets=allen_ecal_clusters[
            'dev_ecal_cluster_offsets'],
        allen_ecal_clusters=allen_ecal_clusters['dev_ecal_clusters']
    ).AllenEcalClusters

    return {"ecal_clusters": ecal_clusters}


def make_allen_velo_tracks():
    """
    Configures the adaptor between the velo tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import (GaudiAllenVeloToV3Tracks,
                                   TrackContainersMerger)

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings
    allen_reco = hlt1_config['reconstruction']

    # Velo reconstruction
    velo_tracks = allen_reco['velo_tracks']
    velo_states = allen_reco['velo_states']

    velo_tracks_v3_alg = GaudiAllenVeloToV3Tracks(
        allen_tracks_mec=velo_tracks["dev_velo_multi_event_tracks_view"],
        allen_beamline_states_view=velo_states[
            "dev_velo_kalman_beamline_states_view"],
        InputUniqueIDGenerator=make_unique_id_generator())

    velo_tracks_fwd_v3 = velo_tracks_v3_alg.OutputTracksForward
    velo_tracks_bwd_v3 = velo_tracks_v3_alg.OutputTracksBackward

    velo_tracks_fwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_fwd_v3).OutputTracks

    velo_tracks_bwd_v1_keyed = fromV3TrackV1Track(
        InputTracks=velo_tracks_bwd_v3).OutputTracks

    velo_tracks_v1_keyed = TrackContainersMerger(
        InputLocations=[velo_tracks_fwd_v1_keyed, velo_tracks_bwd_v1_keyed
                        ]).OutputLocation

    velo_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=velo_tracks_v1_keyed).OutputTracksName

    velo_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=velo_tracks_v2).OutputTracksName

    return {
        "v3fwd": velo_tracks_fwd_v3,
        "v3bwd": velo_tracks_bwd_v3,
        "v2": velo_tracks_v2,
        "v1": velo_tracks_v1,
        "v1keyed": velo_tracks_v1_keyed
    }


def make_allen_velo_ut_tracks():
    """
    Configures the adaptor between the velo tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenUTToV3Tracks

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings
    allen_reco = hlt1_config['reconstruction']

    # Velo reconstruction
    velo_states = allen_reco["velo_states"]
    # UT reconstruction
    ut_tracks = allen_reco["ut_tracks"]

    ut_tracks_v3 = GaudiAllenUTToV3Tracks(
        allen_tracks_mec=ut_tracks["dev_ut_multi_event_tracks_view"],
        allen_beamline_states_view=velo_states[
            "dev_velo_kalman_beamline_states_view"],
        allen_endvelo_states_view=velo_states[
            "dev_velo_kalman_endvelo_states_view"],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    ut_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=ut_tracks_v3).OutputTracks

    ut_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=ut_tracks_v1_keyed).OutputTracksName

    ut_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=ut_tracks_v2).OutputTracksName

    return {
        "v3": ut_tracks_v3,
        "v2": ut_tracks_v2,
        "v1": ut_tracks_v1,
        "v1keyed": ut_tracks_v1_keyed
    }


def make_allen_forward_tracks():
    """
    Configures the adaptor between the forward tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenMEBasicParticlesToV3Tracks

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings
    allen_reco = hlt1_config['reconstruction']

    forward_tracks_v3 = GaudiAllenMEBasicParticlesToV3Tracks(
        allen_tracks_mec=allen_reco['long_track_particles']
        ['dev_multi_event_basic_particles'],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    forward_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=forward_tracks_v3).OutputTracks

    forward_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=forward_tracks_v1_keyed).OutputTracksName

    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName

    return {
        "v3": forward_tracks_v3,
        "v2": forward_tracks_v2,
        "v1": forward_tracks_v1,
        "v1keyed": forward_tracks_v1_keyed
    }


def make_allen_seed_and_match_tracks():
    """
    Configures the adaptor between the seed and match tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenMEBasicParticlesToV3Tracks

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_config['reconstruction']

    seed_and_match_tracks_v3 = GaudiAllenMEBasicParticlesToV3Tracks(
        allen_tracks_mec=allen_reco['long_track_particles']
        ['dev_multi_event_basic_particles'],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    seed_and_match_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=seed_and_match_tracks_v3).OutputTracks

    seed_and_match_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=seed_and_match_tracks_v1_keyed).OutputTracksName

    seed_and_match_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=seed_and_match_tracks_v2).OutputTracksName

    return {
        "v3": seed_and_match_tracks_v3,
        "v2": seed_and_match_tracks_v2,
        "v1": seed_and_match_tracks_v1,
        "v1keyed": seed_and_match_tracks_v1_keyed
    }


def make_allen_downstream_tracks():
    """
    Configures the adaptor between the downstream tracks reconstructed in
    Allen and Gaudi-LHCb tracks.
    """
    from PyConf.Algorithms import GaudiAllenMEBasicParticlesToV3Tracks

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings

    allen_reco = hlt1_config['reconstruction']

    downstream_tracks_v3 = GaudiAllenMEBasicParticlesToV3Tracks(
        allen_tracks_mec=allen_reco['downstream_tracks']
        ['dev_multi_event_downstream_track_particles_view'],
        InputUniqueIDGenerator=make_unique_id_generator()).OutputTracks

    downstream_tracks_v1_keyed = fromV3TrackV1Track(
        InputTracks=downstream_tracks_v3).OutputTracks

    downstream_tracks_v2 = fromV1TrackV2Track(
        InputTracksName=downstream_tracks_v1_keyed).OutputTracksName

    downstream_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=downstream_tracks_v2).OutputTracksName

    return {
        "v3": downstream_tracks_v3,
        "v2": downstream_tracks_v2,
        "v1": downstream_tracks_v1,
        "v1keyed": downstream_tracks_v1_keyed
    }


def make_allen_secondary_vertices():
    """
    Configures the adaptor between the secondary vertices reconstructed in
    Allen and Gaudi-LHCb vertices.
    """
    from PyConf.Algorithms import (
        GaudiAllenSVsToRecVertexV2,
        LHCb__Converters__RecVertex__v1__fromRecVertexv2RecVertexv1 as
        FromRecVertexv2RecVertexv1)

    hlt1_config = allen_gaudi_config(
    )  # Configure Allen sequence and apply bindings
    allen_reco = hlt1_config['reconstruction']

    tracks = make_allen_forward_tracks()
    forward_tracks = allen_reco['long_tracks']
    dihadrons = allen_reco['dihadron_secondary_vertices']
    dileptons = allen_reco['dilepton_secondary_vertices']
    v0s = allen_reco['v0_secondary_vertices']

    allen_v2_dihadrons = GaudiAllenSVsToRecVertexV2(
        allen_atomics_scifi=forward_tracks["dev_offsets_long_tracks"],
        allen_sv_offsets=dihadrons['dev_sv_offsets'],
        allen_secondary_vertices=dihadrons['dev_consolidated_svs'],
        InputTracks=tracks['v2']).OutputSVs
    dihadrons_v1 = FromRecVertexv2RecVertexv1(
        InputVerticesName=allen_v2_dihadrons,
        InputTracksName=tracks['v1']).OutputVerticesName

    allen_v2_dileptons = GaudiAllenSVsToRecVertexV2(
        allen_atomics_scifi=forward_tracks["dev_offsets_long_tracks"],
        allen_sv_offsets=dileptons['dev_sv_offsets'],
        allen_secondary_vertices=dileptons['dev_consolidated_svs'],
        InputTracks=tracks['v2']).OutputSVs
    dileptons_v1 = FromRecVertexv2RecVertexv1(
        InputVerticesName=allen_v2_dileptons,
        InputTracksName=tracks['v1']).OutputVerticesName

    allen_v2_v0s = GaudiAllenSVsToRecVertexV2(
        allen_atomics_scifi=forward_tracks["dev_offsets_long_tracks"],
        allen_sv_offsets=v0s['dev_sv_offsets'],
        allen_secondary_vertices=v0s['dev_consolidated_svs'],
        InputTracks=tracks['v2']).OutputSVs
    v0s_v1 = FromRecVertexv2RecVertexv1(
        InputVerticesName=allen_v2_v0s,
        InputTracksName=tracks['v1']).OutputVerticesName

    return {
        'v2_dihadrons': allen_v2_dihadrons,
        'v1_dihadrons': dihadrons_v1,
        'v2_dileptons': allen_v2_dileptons,
        'v1_dileptons': dileptons_v1,
        'v2_v0s': allen_v2_v0s,
        'v1_v0s': v0s_v1
    }


@configurable
def combine_raw_banks_with_MC_data_for_standalone_Allen_checkers(
        output_file, with_ut=True):
    """Combine raw banks and MC info required for Allen standalone running into one RawEvent

    Combining:
    - The Velo, UT, SciFi, Muon, ODIN banks
    - MC info from the TrackerDumper and the PVDumper required for the Allen standalone checker
    Written to binaries (inside the "geometry" directory):
    """

    from RecoConf.mc_checking import tracker_dumper, pv_dumper
    from PyConf.application import default_raw_event, mdf_writer
    from PyConf.Algorithms import RawEventCombiner
    from AllenCore.gaudi_allen_generator import make_transposed_raw_banks
    from Moore.streams import bank_types_for_detectors

    subdetectors = ["VP", "FTCluster", "Muon", "ODIN", "Calo", "EcalPacked"]

    if with_ut:
        subdetectors += ["UT"]
    allen_banks = make_transposed_raw_banks(subdetectors)

    algs = [allen_banks]
    trackerDumper = tracker_dumper(dump_to_root=False, with_ut=with_ut)
    algs.append(trackerDumper)
    pvDumper = pv_dumper()
    algs.append(pvDumper)

    detector_parts = [
        default_raw_event([bt]) for bt in bank_types_for_detectors()
    ]
    # get only unique elements while preserving order
    detector_parts = list(dict.fromkeys(detector_parts).keys())
    combiner = RawEventCombiner(RawEventLocations=detector_parts + [
        trackerDumper.OutputRawEventLocation,
        pvDumper.OutputRawEventLocation,
    ])

    algs.append(combiner)

    algs.append(
        mdf_writer(output_file, location=combiner.RawEvent, compression=0))

    return algs

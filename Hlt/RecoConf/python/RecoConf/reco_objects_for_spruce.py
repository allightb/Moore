###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from PyConf import configurable
from GaudiConf import reading
from PyConf.packing import reco_locations, pp2mcp_locations, unpackers_map
from PyConf.reading import upfront_decoder, dstdata_filter, postprocess_unpacked_data
from PyConf.reading import get_mc_vertices, get_mc_particles
from PyConf.components import force_location


def __make_dict(stream, locs):
    """Return a dictionary of object type: location list
       This is needed to decide which unpacker to use for a given object
    """
    dct = {k: [] for k in unpackers_map().keys()}
    for loc, typ in locs.items():
        if typ and loc.startswith(
                stream) and not loc.startswith(stream + "/MC"):
            alias = reading.type_map().get(typ)
            if alias: typ = alias
            if typ not in dct.keys():
                print("Warning: unknown type - ", locs.get(loc),
                      " at location ", loc, " -- skipping")
            else:
                dct[typ] += [force_location(loc)]
    return dct


def __unpackers(stream, locations, mappedBuffers):
    """Return a list of unpackers for reading reconstructed objects.

    This must run AFTER mc_unpackers if MC data!!!.

    Args:
        cfg:  configuration needed for finding linker locations
        locations: list of packed object locations to unpack
        configurables (bool): set to False to use PyConf Algorithm.
    """

    outputs = __make_dict(stream, locations)

    unp_map = unpackers_map()
    unpacker_algs = []

    for t, unp in unp_map.items():
        if t in outputs.keys():
            for loc in outputs[t]:
                unpacker = unp(
                    InputName=force_location(
                        getattr(
                            mappedBuffers, 'location', mappedBuffers
                        )  # check for either location or fullKey().key()
                    ),  # BUG / FIXME: why is force_location needed here??? inputName is a data handle!!!
                    outputs={'OutputName': force_location(loc)})

                unpacker_algs += [unpacker]

    return unpacker_algs


@configurable
def upfront_reconstruction(simulation=False):
    """Return a list DataHandles that define the upfront reconstruction output.

    This differs from `reconstruction` as it should not be used as inputs to
    other algorithms, but only to define the control flow, i.e. the return
    value of this function should be ran before all HLT2 lines.

    """

    stream = '/Event/HLT2'

    inv_map = {v: k for k, v in reading.type_map().items()}

    reco_loc = reco_locations(stream)
    dl = {v[0]: inv_map[v[1]] for v in reco_loc.values()}

    mc_algs = []
    if simulation:
        mc_algs = [
            get_mc_vertices(os.path.join(stream, 'MC/Vertices')).producer,
            get_mc_particles(os.path.join(stream, 'MC/Particles')).producer
        ]

        pp2mcp_loc = pp2mcp_locations(stream)
        dl.update((v[0], inv_map[v[1]]) for v in pp2mcp_loc.values())

    unpackers = __unpackers(stream, dl, upfront_decoder(source="Hlt2"))

    ### TODO:FIXME take advantage of the fact that the above have datahandles...
    # i.e. should _not_ have to return decoder here, and should just return the _output handles_ and not the algorithms
    # i.e. `upfront_reconstruction` should be a drop-in replacement for `reconstruction()`, with the same return type
    return [
        dstdata_filter(source='Hlt2'),
        upfront_decoder(source="Hlt2").producer
    ] + mc_algs + unpackers


@configurable
def reconstruction(simulation=False):
    """Return a {name: DataHandle} dict that define the reconstruction output."""

    stream = '/Event/HLT2'
    data = {}
    unpackers = upfront_reconstruction(simulation)
    reco_loc = reco_locations(stream)

    if simulation:
        reco_loc |= pp2mcp_locations(stream)

    for key, value in reco_loc.items():
        for v in unpackers:
            if "OutputName" in v.outputs.keys(
            ) and v.OutputName.location == value[0]:
                data[key] = v.OutputName

    # FIXME 'packable' set to true while 'SharedObjectsContainer`s are not persistable
    postprocess_unpacked_data(data, packable=True)

    return data

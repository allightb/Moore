###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Core algorithms for HLT processing
"""
from PyConf.Algorithms import UniqueIDGeneratorAlg


def make_unique_id_generator():
    """ Create the instance in charge of generating unique IDs.

    Ensure that the location of the ID generator remains always constant (it would
    be better if we keep it as the default location). Otherwise algorithms might
    end up using and comparing IDs from different instances.
    """
    return UniqueIDGeneratorAlg().Output

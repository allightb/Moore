###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define outputs of the HLT2 track reconstruction depending on MC for use by lines."""

from PyConf import configurable
from PyConf.dataflow import DataHandle
from PyConf.reading import get_mc_track_info
from typing import Callable

from PyConf.Algorithms import (
    PrCheatedSciFiTracking, fromPrSeedingTracksV1Tracks,
    fromPrVeloTracksV1Tracks, PrCheatedUpstreamTracking)

from RecoConf.hlt2_tracking import (make_PrStorePrUTHits_hits,
                                    make_PrStoreSciFiHits_hits)

from RecoConf.data_from_file import boole_links_digits_mcparticles, mc_unpackers

from RecoConf.mc_checking import (
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles)


@configurable
def make_cheatedSeeding_tracks(make_ft_hits=make_PrStoreSciFiHits_hits):
    """Makes seed tracks with PrCheatedSciFiTracking.

    Args:
        make_ft_hits (DataHandle): maker of FT hits, defaults to `make_PrStoreSciFiHits_hits <RecoConf.legacy_rec_hlt1_tracking.make_PrStoreSciFiHits_hits>`.

    Returns:
        A dict mapping v1 and v2 SciFi seeding tracks to ``'v1'`` and ``'v2'`` respectively.

    """
    scifi_tracks_pr = PrCheatedSciFiTracking(
        FTHitsLocation=make_ft_hits(),
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCPropertyLocation=get_mc_track_info(),
        LinkLocation=boole_links_digits_mcparticles()
        ["FTLiteClusters"]).OutputName

    scifi_tracks_v1 = fromPrSeedingTracksV1Tracks(
        InputTracksLocation=scifi_tracks_pr).OutputTracksLocation

    return {"v1": scifi_tracks_v1, "Pr": scifi_tracks_pr}


@configurable
def make_cheatedUpstream_tracks(
        velo_tracks: dict[DataHandle],
        make_ut_hits: Callable = make_PrStorePrUTHits_hits) -> DataHandle:
    """Cheat Upstream tracks from reconstructed Velo tracks using MC.

    Args:
        velo_tracks (dict[DataHandle]): Dict of v1 and Pr Velo tracks.
        make_ut_hits (Callable, optional): Defaults to make_PrStorePrUTHits_hits.

    Returns:
        DataHandle: Upstream tracks created from reconstructed Velo tracks with all true UT hits added.

    Note:
        Only Velo tracks matched to a MCParticle are used to create an Upstream track.
    """
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    velo_forward_v1 = fromPrVeloTracksV1Tracks(
        InputTracksLocation=velo_tracks["Pr"]).OutputTracksLocation
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=velo_forward_v1, LinksToLHCbIDs=links_to_lhcbids)
    return PrCheatedUpstreamTracking(
        InputTracksName=velo_tracks["Pr"],
        UTHits=make_ut_hits(),
        MCParticles=mc_unpackers()["MCParticles"],
        MCProperty=get_mc_track_info(),
        LHCbIDLinks=links_to_lhcbids,
        VeloTrackLinks=links_to_tracks).OutputName

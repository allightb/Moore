###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.application import default_raw_banks
from PyConf.Algorithms import VPClusterEfficiency
from PyConf.reading import get_mc_track_info
from .data_from_file import boole_links_digits_mchits, mc_unpackers
from .legacy_rec_hlt1_tracking import make_velo_full_clusters


def monitor_velo_clusters(make_raw=default_raw_banks):
    return VPClusterEfficiency(
        RawBanks=make_raw("VP"),
        VPClusterLocation=make_velo_full_clusters(),
        MCHitLocation=mc_unpackers()["MCVPHits"],
        MCParticleLocation=mc_unpackers()["MCParticles"],
        VPDigit2MCHitLinksLocation=boole_links_digits_mchits()["VPDigits"],
        MCProperty=get_mc_track_info())

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Load data from files and set up unpackers.

There are two things we have to deal with:

1. Loading the data from the file in to the TES, done by
   Gaudi::Hive::FetchDataFromFile.
2. Unpacking and preparing packed containers, if the 'reconstruction' is
   defined as the objects already present in the file.

In most LHCb applications, step 2 is done for you behind the scenes. The
DataOnDemandSvc is configured in LHCb/GaudiConf/DstConf.py to unpack containers
when they are requested. It also configures adding RICH, MUON, and combined PID
information to ProtoParticles when they're unpacked. Because we don't have the
DataOnDemandSvc here, we have to do this by hand.

The interesting 'user-facing' exports of this module are:

* [reco,mc]_from_file(): Dict of names to locations that can be loaded from a file.
* [reco,mc]_unpackers(): Dict from unpacked object name to Algorithm that produces a
  container of those objects.
"""
import collections

#####
# New unpackers can not unpack old dst files from brunel
# so here we are using old unpackers which unpack from packed objects
# while new unpackers will unpack from packed data buffers
#####
from Gaudi.Configuration import ERROR
from Configurables import (UnpackMuonPIDs, UnpackRichPIDs, UnpackCaloHypo,
                           UnpackProtoParticle, UnpackRecVertex, UnpackTrack,
                           UnpackMCParticle, UnpackMCVertex, UnpackMCHit,
                           MCRichHitUnpacker, UnpackMCCaloHit,
                           MCRichDigitSummaryUnpacker)

from PyConf.components import Algorithm, force_location
from PyConf.application import make_data_with_FetchDataFromFile
from PyConf.Tools import (
    ChargedProtoParticleAddRichInfo, ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddCaloHypos, ChargedProtoParticleAddCombineDLLs)


def _unpacker(forced_type, packed_loc, unpacked_loc, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    assert 'name' not in kwargs
    _unpack_configurable = {
        'LHCb::PackedRecVertices': UnpackRecVertex,
        'LHCb::PackedCaloHypos': UnpackCaloHypo,
        'LHCb::PackedMuonPID': UnpackMuonPIDs,
        'LHCb::PackedRichPID': UnpackRichPIDs,
        'LHCb::PackedTracks': UnpackTrack,
        'LHCb::PackedProtoParticles': UnpackProtoParticle,
        'LHCb::PackedMCParticles': UnpackMCParticle,
        'LHCb::PackedMCVertices': UnpackMCVertex,
        'LHCb::PackedMCHit': UnpackMCHit,
        'LHCb::PackedMCCaloHits': UnpackMCCaloHit,
        'LHCb::PackedMCRichHits': MCRichHitUnpacker,
        'LHCb::PackedMCRichDigitSummarys': MCRichDigitSummaryUnpacker,
    }
    return Algorithm(
        _unpack_configurable[forced_type],
        name=f"Unpack_{unpacked_loc.replace('/','_')}",
        outputs={'OutputName': force_location(unpacked_loc)},
        InputName=make_data_with_FetchDataFromFile(
            packed_loc, force_type=forced_type),
        **kwargs)


def reco_unpackers():
    # for each key, specify both the location in the TES and the type that is
    # expected to be made available by reading it from a file
    _packed_reco_from_file = {
        'PVs': ('LHCb::PackedRecVertices', '/Event/pRec/Vertex/Primary'),
        'CaloElectrons': ('LHCb::PackedCaloHypos',
                          '/Event/pRec/Calo/Electrons'),
        'CaloPhotons': ('LHCb::PackedCaloHypos', '/Event/pRec/Calo/Photons'),
        'CaloMergedPi0s': ('LHCb::PackedCaloHypos',
                           '/Event/pRec/Calo/MergedPi0s'),
        'CaloSplitPhotons': ('LHCb::PackedCaloHypos',
                             '/Event/pRec/Calo/SplitPhotons'),
        'MuonPIDs': ('LHCb::PackedMuonPID', '/Event/pRec/Muon/MuonPID'),
        'RichPIDs': ('LHCb::PackedRichPID', '/Event/pRec/Rich/PIDs'),
        'Tracks': ('LHCb::PackedTracks', '/Event/pRec/Track/Best'),
        'NeutralProtos': ('LHCb::PackedProtoParticles',
                          '/Event/pRec/ProtoP/Neutrals'),
        'ChargedProtos': ('LHCb::PackedProtoParticles',
                          '/Event/pRec/ProtoP/Charged')
    }

    def _reco_unpacker(key, **kwargs):
        """Return unpacker that reads from file and unpacks to a forced output location."""
        packed_type, packed_location = _packed_reco_from_file[key]
        # If the structure is not like this, pointers point to to the wrong place...
        # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
        unpacked_location = packed_location.replace('/pRec', '/Rec').replace(
            '/pHLT2', '/HLT2')
        return _unpacker(packed_type, packed_location, unpacked_location,
                         **kwargs)

    muonPIDs = _reco_unpacker('MuonPIDs')
    richPIDs = _reco_unpacker('RichPIDs', OutputLevel=ERROR)
    electrons = _reco_unpacker('CaloElectrons')
    photons = _reco_unpacker('CaloPhotons')
    charged_protos = _reco_unpacker(
        'ChargedProtos',
        AddInfo=[
            # only overwrite, not remove old info to not invalidate global pid object
            ChargedProtoParticleAddRichInfo(
                InputRichPIDLocation=richPIDs, RemoveOldInfo=False),
            ChargedProtoParticleAddMuonInfo(
                InputMuonPIDLocation=muonPIDs, RemoveOldInfo=False),
            # NOTE missing CaloHypo links to CellID due to missing Cluster persistency is not reported
            ChargedProtoParticleAddCaloHypos(
                ElectronHypos=electrons,
                PhotonHypos=photons,
                ReportMissing=False),
            ChargedProtoParticleAddCombineDLLs(RemoveOldInfo=False),
        ])

    # Ordered so that dependents are unpacked first
    return collections.OrderedDict([
        ('PVs', _reco_unpacker('PVs')),
        ('CaloElectrons', electrons),
        ('CaloPhotons', photons),
        ('CaloMergedPi0s', _reco_unpacker('CaloMergedPi0s')),
        ('CaloSplitPhotons', _reco_unpacker('CaloSplitPhotons')),
        ('MuonPIDs', muonPIDs),
        ('RichPIDs', richPIDs),
        ('Tracks', _reco_unpacker('Tracks')),
        ('NeutralProtos', _reco_unpacker('NeutralProtos')),
        ('ChargedProtos', charged_protos),
    ])


def mc_unpackers():
    # for each key, specify both the location in the TES and the type that is
    # expected to be made available by reading it from a file
    _packed_mc_from_file = {
        'MCParticles': ('LHCb::PackedMCParticles', '/Event/pSim/MCParticles'),
        'MCVertices': ('LHCb::PackedMCVertices', '/Event/pSim/MCVertices'),
        'MCVPHits': ('LHCb::PackedMCHit', '/Event/pSim/VP/Hits'),
        'MCUTHits': ('LHCb::PackedMCHit', '/Event/pSim/UT/Hits'),
        'MCFTHits': ('LHCb::PackedMCHit', '/Event/pSim/FT/Hits'),
        'MCRichHits': ('LHCb::PackedMCRichHits', '/Event/pSim/Rich/Hits'),
        'MCEcalHits': ('LHCb::PackedMCCaloHits', '/Event/pSim/Ecal/Hits'),
        'MCHcalHits': ('LHCb::PackedMCCaloHits', '/Event/pSim/Hcal/Hits'),
        'MCMuonHits': ('LHCb::PackedMCHit', '/Event/pSim/Muon/Hits'),
        'MCRichDigitSummaries': ('LHCb::PackedMCRichDigitSummarys',
                                 '/Event/pSim/Rich/DigitSummaries'),
    }

    def _mc_unpacker(key, **kwargs):
        """Return unpacker that reads from file and unpacks to a forced output location."""
        packed_type, packed_location = _packed_mc_from_file[key]
        # If the structure is not like this, pointers point to to the wrong place...
        # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
        unpacked_location = packed_location.replace('/pSim', '/MC').replace(
            '/MC/MC', '/MC/')
        return _unpacker(packed_type, packed_location, unpacked_location,
                         **kwargs)

    # Ordered so that dependents are unpacked first
    # Make sure that MC particles and MC vertices are unpacked together,
    # see https://gitlab.cern.ch/lhcb/LHCb/issues/57 for details.
    mc_vertices = _mc_unpacker('MCVertices')
    return collections.OrderedDict([
        ('MCRichDigitSummaries', _mc_unpacker('MCRichDigitSummaries')),
        ('MCParticles', _mc_unpacker('MCParticles',
                                     ExtraInputs=[mc_vertices])),
        ('MCVertices', mc_vertices),
        ('MCVPHits', _mc_unpacker('MCVPHits')),
        ('MCUTHits', _mc_unpacker('MCUTHits')),
        ('MCFTHits', _mc_unpacker('MCFTHits')),
        ('MCRichHits', _mc_unpacker('MCRichHits')),
        ('MCEcalHits', _mc_unpacker('MCEcalHits')),
        ('MCHcalHits', _mc_unpacker('MCHcalHits')),
        ('MCMuonHits', _mc_unpacker('MCMuonHits')),
    ])


def boole_links_digits_mcparticles():
    """Return a dict of locations for MC linker tables (to mcparticles) created by Boole."""
    locations = {
        "EcalDigitsV1": "/Event/Link/Raw/Ecal/Digits",
        "EcalDigits": "/Event/Link/Raw/Ecal/Digits2MCParticles",
        "FTLiteClusters": "/Event/Link/Raw/FT/LiteClusters",
        "HcalDigitsV1": "/Event/Link/Raw/Hcal/Digits",
        "HcalDigits": "/Event/Link/Raw/Hcal/Digits2MCParticles",
        "MuonDigits": "/Event/Link/Raw/Muon/Digits",
        "UTClusters": "/Event/Link/Raw/UT/Clusters",
        "UTDigits": "/Event/Link/Raw/UT/TightDigits",
        "VPDigits": "/Event/Link/Raw/VP/Digits",
    }
    mc_particles = mc_unpackers()['MCParticles']
    return {
        key: make_data_with_FetchDataFromFile(
            loc, force_type='LHCb::LinksByKey', ExtraInputs=[mc_particles])
        for key, loc in locations.items()
    }


def boole_links_digits_mchits():
    """Return a dict of locations for MC linker tables (to mchits) created by Boole.

    These locations are only propagated out of Boole for eXtendend DIGI and DST types.
    """
    locations_and_dep = {
        "FTLiteClusters": ("/Event/Link/Raw/FT/LiteClusters2MCHits",
                           'MCFTHits'),
        "UTClusters": ("/Event/Link/Raw/UT/Clusters2MCHits", 'MCUTHits'),
        "UTDigits": ("/Event/Link/Raw/UT/TightDigits2MCHits", 'MCUTHits'),
        "VPDigits": ("/Event/Link/Raw/VP/Digits2MCHits", 'MCVPHits')
    }
    return {
        key: make_data_with_FetchDataFromFile(
            loc,
            force_type='LHCb::LinksByKey',
            ExtraInputs=[mc_unpackers()[dep]])
        for key, (loc, dep) in locations_and_dep.items()
    }


def brunel_links():
    """Return a dict of locations for MC linker tables created by Brunel."""
    locations = {
        "CaloElectrons": "/Event/Link/Rec/Calo/Electrons",
        "CaloMergedPi0s": "/Event/Link/Rec/Calo/MergedPi0s",
        "CaloPhotons": "/Event/Link/Rec/Calo/Photons",
        "CaloSplitPhotons": "/Event/Link/Rec/Calo/SplitPhotons",
        "Tracks": "/Event/Link/Rec/Track/Best",
    }
    return {
        key: make_data_with_FetchDataFromFile(
            loc, force_type='LHCb::LinksByKey')
        for key, loc in locations.items()
    }

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import (
    TrackVertexMonitor, TrackFitMatchMonitor, TrackFitMatchMonitor_PrKalman,
    TrackMonitor, TrackMonitor_PrKalman, TrackPV2HalfMonitor,
    DeterministicPrescaler, PrSciFiHitsMonitor, FTTrackMonitor_PrKalman,
    FTTrackMonitor, TrackVPOverlapMonitor)
from PyConf import configurable
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt2_tracking import make_PrStoreSciFiHits_hits


def get_default_monitoring_track_types_for_light_reco():
    """Returns default monitoring track types of light reco sequence.

    Returns:
        Dict with lists of strings.
    """
    return [
        "BestLong", "BestSeed", "BestVelo", "BestVeloBackward",
        "BestDownstream", "BestUpstream"
    ]


@configurable
def get_monitoring_track_types_for_light_reco(
        skip_UT=False,
        track_types=get_default_monitoring_track_types_for_light_reco()):
    """Returns monitoring track types of light reco sequence.

    Returns:
        Dict with lists of strings.
    """
    if skip_UT:
        if "BestDownstream" in track_types:
            track_types.remove("BestDownstream")
        if "BestUpstream" in track_types:
            track_types.remove("BestUpstream")

    return {"Best": track_types}


@configurable
def monitor_tracking(tracks,
                     velo_tracks,
                     tracks_for_pvs,
                     pvs,
                     use_pr_kf=False,
                     prescale=1.0):
    prescaler = DeterministicPrescaler(
        name="TrackMonitoringPrescaler",
        AcceptFraction=prescale,
        SeedName="TrackMonitoringPrescaler",
        ODINLocation=make_odin())

    vertex_moni = TrackVertexMonitor(
        name="TrackVertexMonitor", PVContainer=pvs, TrackContainer=tracks)

    if use_pr_kf:
        fitmatch_moni = TrackFitMatchMonitor_PrKalman(
            name="TrackFitMatchMonitor", TrackContainer=tracks)

        track_moni = TrackMonitor_PrKalman(
            name="TrackMonitor",
            TracksInContainer=tracks,
            StatesFor2DHits=["VELO", "UT", "FT"])

        velo_track_moni = TrackMonitor(
            name="VeloTrackMonitor",
            TracksInContainer=velo_tracks,
            typesToMonitor=["Velo", "VeloBackward"],
            StatesFor2DHits=["VELO"])

        vpoverlap_moni = TrackVPOverlapMonitor(
            name="TrackVPOverlapMonitor", TrackContainer=tracks)

        ft_track_moni = FTTrackMonitor_PrKalman(
            name="FTTrackMonitor", TracksInContainer=tracks)
    else:
        fitmatch_moni = TrackFitMatchMonitor(
            name="TrackFitMatchMonitor", TrackContainer=tracks)

        track_moni = TrackMonitor(
            name="TrackMonitor",
            TracksInContainer=tracks,
            StatesFor2DHits=["VELO", "UT", "FT"])

        velo_track_moni = TrackMonitor(
            name="VeloTrackMonitor",
            TracksInContainer=velo_tracks,
            typesToMonitor=["Velo", "VeloBackward"],
            StatesFor2DHits=["VELO"])

        vpoverlap_moni = TrackVPOverlapMonitor(
            name="TrackVPOverlapMonitor", TrackContainer=tracks)

        ft_track_moni = FTTrackMonitor(
            name="FTTrackMonitor", TracksInContainer=tracks)

    pv2half_moni = TrackPV2HalfMonitor(
        name="TrackPV2HalfMonitor",
        TrackContainer=tracks_for_pvs,
        ODINLocation=make_odin())
    scifi_hits_moni = PrSciFiHitsMonitor(
        name="PrSciFiHitsMonitor",
        SciFiHits=make_PrStoreSciFiHits_hits(),
        ODINLocation=make_odin())

    return CompositeNode(
        "Monitoring", [
            prescaler,
            vertex_moni,
            fitmatch_moni,
            track_moni,
            velo_track_moni,
            vpoverlap_moni,
            pv2half_moni,
            scifi_hits_moni,
            ft_track_moni,
        ],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)

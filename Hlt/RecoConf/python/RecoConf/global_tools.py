###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Methods to define public tools.

This file collects functions to define public tools used by reconstruction and selections.

"""

from PyConf.Tools import (TrackStateProvider, TrackMasterExtrapolator,
                          SimplifiedMaterialLocator, TrackInterpolator)


def trackMasterExtrapolator_with_simplified_geom():
    """Configure TrackMasterExtrapolator to use simplified geometry.

    The simplified geometry allows for a significant faster extrapolation.
    """

    return TrackMasterExtrapolator(
        public=True, MaterialLocator=SimplifiedMaterialLocator())


def stateProvider_with_simplified_geom():
    """Configure TrackStateProvider to use simplified geometry.

    The simplified geometry allows for a significant faster extrapolation.
    """

    masterextrapolator = TrackMasterExtrapolator(
        name="test", MaterialLocator=SimplifiedMaterialLocator())

    trackinterpolator = TrackInterpolator(Extrapolator=masterextrapolator)

    return TrackStateProvider(
        public=True,
        Extrapolator=masterextrapolator,
        Interpolator=trackinterpolator)

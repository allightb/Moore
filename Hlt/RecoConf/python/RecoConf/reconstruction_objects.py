###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Module to allow switching between prompt reconstruction and reconstruction from file.

The user can switch between the two options in a top level configuration file
by binding the argument from_file of the function reconstruction.

"""

from PyConf import configurable
from RecoConf.reco_objects_from_file import (
    reconstruction as reconstruction_from_file,
    upfront_reconstruction as upfront_reconstruction_from_file,
)
from RecoConf.reco_objects_for_spruce import (
    reconstruction as reconstruction_for_spruce,
    upfront_reconstruction as upfront_reconstruction_for_spruce,
)

from RecoConf.hlt2_global_reco import reconstruction as reconstruction_from_reco


@configurable(cached=True)
def reconstruction(from_file=True, spruce=False):
    """Return reconstruction objects.

    Note it is advised to use this function if more than one object is needed,
    rather than the accessors below as it makes the configuration slower.
    """

    if spruce:
        assert from_file, 'For sprucing, from_file must be set to True (default value)'
    if from_file:
        if spruce:
            reco = reconstruction_for_spruce()
            upfront_reconstruction = upfront_reconstruction_for_spruce()
        else:
            reco = reconstruction_from_file()
            upfront_reconstruction = upfront_reconstruction_from_file()
    else:
        reco = reconstruction_from_reco()
        upfront_reconstruction = reco["UpfrontReconstruction"]

    objects = {key: value for key, value in reco.items()}
    objects["UpfrontReconstruction"] = upfront_reconstruction

    return objects


def upfront_reconstruction():
    """Return sequence to create charged ProtoParticles.

    """
    return reconstruction()["UpfrontReconstruction"]


def make_charged_protoparticles(track_type):
    """Return a DataHandle to the container of charged ProtoParticles.

    """
    return reconstruction()[f'{track_type}Protos']


def make_neutral_protoparticles():
    """Return a DataHandle to the container of neutral ProtoParticles.

    """
    return reconstruction()["NeutralProtos"]


def get_NeutralPID():
    """
    Return a DataHandle to the NeutralPID object.
    """

    return reconstruction()["NeutralPIDs"]


def make_pvs():
    """Return a DataHandle to the container of PVs

    """
    return reconstruction()["PVs"]


def make_extended_pvs():
    """Return a DataHandle to the container of PVs

    """
    return reconstruction()["ExtendedPVs"]


def make_tracks(track_type='Tracks'):
    """Return a DataHandle to the container of (all) tracks

    """
    return reconstruction()[track_type]


def make_rich_pids():
    """Return a DataHandle to the container of RichPIDS

    """
    return reconstruction()['RichPIDs']


def make_muon_pids():
    """Return a DataHandle to the container of MuonPIDs

    """
    return reconstruction()['MuonPIDs']


def make_calo_electrons():
    """Return a DataHandle to the container of Calo Electrons

    """
    return reconstruction()['CaloElectrons']


def make_calo_photons():
    """Return a DataHandle to the container of Calo Photons

    """
    return reconstruction()['CaloPhotons']


def make_calo_splitphotons():
    """Return a DataHandle to the container of Calo SplitPhotons

    """
    return reconstruction()['CaloSplitPhotons']


def make_calo_mergedPi0s():
    """Return a DataHandle to the container of Calo Merged Pi0s

    """
    return reconstruction()['CaloMergedPi0ss']

###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.Algorithms import (FunctionalChargedProtoParticleMaker,
                               FutureNeutralProtoPAlg,
                               ProtoParticlesEmptyProducer)
from PyConf.Tools import (
    CaloFutureElectron,
    CaloFutureHypoEstimator,
    CaloFutureHypo2CaloFuture,
    FutureGammaPi0XGBoostTool,
    FutureGammaPi0SeparationTool,
    FutureNeutralIDTool,
    ChargedProtoParticleAddCaloInfo,
    ChargedProtoParticleAddBremInfo,
    ChargedProtoParticleAddRichInfo,
    ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddCombineDLLs,
    ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo,
)

import Functors as F


def hypo_estimator(calo_pids):
    return CaloFutureHypoEstimator(
        DigitLocation=calo_pids["digitsEcal"],
        Hypo2Calo=CaloFutureHypo2CaloFuture(
            EcalDigitsLocation=calo_pids["digitsEcal"],
            HcalDigitsLocation=calo_pids["digitsHcal"]),
        Pi0Separation=FutureGammaPi0SeparationTool(),
        Pi0SeparationXGB=FutureGammaPi0XGBoostTool(
            DigitLocation=calo_pids["digitsEcal"]),
        NeutralID=FutureNeutralIDTool(),
        Electron=CaloFutureElectron(),
    )


@configurable
def make_neutral_protoparticles(calo_pids,
                                location_protos=None,
                                location_PIDs=None):
    """Create neutral ProtoParticles from Calorimeter reconstruction output.

    Args:
        calo_pids: dictionary containing all necessary inputs to create neutral ProtoParticles.

    Returns:
        DataHandle to the container of neutral ProtoParticles

    """
    if calo_pids is None:
        neutral_protos_locations = {
            'ProtoParticleLocation': ProtoParticlesEmptyProducer().Output,
            'NeutralPID': location_PIDs
        }  # neutral protos have to be written out, even if the Calo did not run
        # The full data flow is functional, we only keep this for compatibility with reco from file.
    else:
        neutral_protos = FutureNeutralProtoPAlg(
            CaloHypoEstimator=hypo_estimator(calo_pids),
            MergedPi0s=calo_pids["v1_mergedPi0s"],
            Photons=calo_pids["v1_photons"],
            SplitPhotons=calo_pids["v1_splitPhotons"],
            TrackMatchTable=calo_pids["v2_clustertrackmatches"],
            outputs={
                'ProtoParticleLocation': location_protos,
                'NeutralPID': location_PIDs
            },
        )
        neutral_protos_locations = {
            'ProtoParticleLocation': neutral_protos.ProtoParticleLocation,
            'NeutralPID': neutral_protos.NeutralPID
        }

    return neutral_protos_locations


@configurable
def make_charged_protoparticles(tracks,
                                rich_pids,
                                calo_pids,
                                muon_pids,
                                track_types=["Long", "Downstream", "Upstream"],
                                location=None,
                                name=None,
                                fill_probnn_defaults=False):
    """Create charged ProtoParticles from tracking and particle identification reconstruction outputs.

    Args:
        tracks: input tracks to create charged protos
        rich_pids: input rich_pids created from tracks
        calo_pids: input calo_pids from Calorimeter reconstruction
        muon_pids: input muon_pids created from tracks
        track_types: accepted track types
        location: location of output (for persistency etc.)
        name: name of algorithm

    Returns:
        dict with DataHandle to the container of charged ProtoParticles and the sequence of algorithms to create them.

    """
    addInfo = []

    accepted_types = {
        "calo": ["Long", "Downstream", "Ttrack"],
        "brem": ["Long", "Downstream", "Upstream", "Velo"],
        "ANN": ["Long", "Downstream", "Upstream"]
    }

    track_predicates = {
        "Long": F.TRACKISLONG,
        "Downstream": F.TRACKISDOWNSTREAM,
        "Upstream": F.TRACKISUPSTREAM,
        "Ttrack": F.TRACKISTTRACK,
        "Velo": F.TRACKISVELO
    }

    # add CALO info
    if calo_pids is not None:
        calo_track_types = set(track_types) & set(accepted_types["calo"])
        brem_track_types = set(track_types) & set(accepted_types["brem"])
        if len(calo_track_types):
            input_pids = [
                pid for tt, pid in calo_pids["v1_chargedpids"].items()
                if tt in calo_track_types
            ]
            addInfo += [
                ChargedProtoParticleAddCaloInfo(
                    name='addCaloInfo',
                    InputPIDs=input_pids,
                    ID2CaloHypoTable=calo_pids["v1_electrons_relations"],
                )
            ]
        if len(brem_track_types):
            addInfo.append(
                ChargedProtoParticleAddBremInfo(
                    name='addBremInfo',
                    InputPIDs=[
                        pid for tt, pid in calo_pids["v1_breminfos"].items()
                        if tt in brem_track_types
                    ],
                    ID2CaloHypoTable=calo_pids["v1_photons_relations"],
                ))

    # add RICH info
    if rich_pids is not None:
        addInfo += [
            ChargedProtoParticleAddRichInfo(
                name='addRichInfo', InputRichPIDLocation=rich_pids)
        ]

    # add MUON info
    if muon_pids is not None:
        addInfo += [
            ChargedProtoParticleAddMuonInfo(
                name='addMuonInfo', InputMuonPIDLocation=muon_pids)
        ]

    if (calo_pids is not None) or (rich_pids is not None) or (muon_pids is
                                                              not None):
        addInfo.append(ChargedProtoParticleAddCombineDLLs())

    if ("Upstream" in track_types and (rich_pids is not None)) or (
        ("Downstream" in track_types or "Long" in track_types) and
        (calo_pids is not None) and (rich_pids is not None) and
        (calo_pids is not None) and (rich_pids is not None) and
        (muon_pids is not None)):
        # Set up ann pids
        addInfo += [
            ANNGlobalPID__ChargedProtoParticleAddANNPIDInfo(
                name="AddANNPID" + TrackType + PIDType,
                NetworkVersion="MCUpTuneV1",
                PIDType=PIDType,
                TrackType=TrackType,
                FillDefaults=fill_probnn_defaults,
            ) for TrackType in track_types
            if TrackType in accepted_types["ANN"] for PIDType in
            ["Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"]
        ]

    # this should always be run
    is_list = type(tracks) is list
    charged_protos = FunctionalChargedProtoParticleMaker(
        name=name,
        Inputs=[track["v1"] for track in tracks] if is_list else [tracks],
        Code=F.require_any(*[track_predicates[i] for i in track_types]),
        AddInfo=addInfo,
        outputs={
            'Output': location,
            'OutputPIDs': None
        })

    return charged_protos.Output

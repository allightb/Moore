##############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.utilities import DISABLE_TOOL
import Functors as F
from functools import partial
from PyConf.Algorithms import (FunctionalChargedProtoParticleMaker,
                               StandaloneMuonRec, VeloMuonBuilder,
                               MuonUTTracking, PrKalmanFilter_Downstream,
                               TrackBestTrackCreator, SeedMuonBuilder)
from RecoConf.core_algorithms import make_unique_id_generator
from PyConf.Tools import (
    TrackMasterExtrapolator,
    PrAddUTHitsTool,
    MuonMeasurementProvider,
    ChargedProtoParticleAddMuonInfo,
)
from RecoConf.muonid import make_muon_hits
from RecoConf.muon_reconstruction import make_all_muon_pids, make_conv_muon_pids
from RecoConf.legacy_rec_hlt1_tracking import (
    make_legacy_rec_hlt1_tracks, make_PrStorePrUTHits_hits, make_pvs)
from RecoConf.hlt2_tracking import (
    make_seeding_tracks, make_PrLongLivedTracking_tracks,
    make_PrHybridSeeding_tracks, get_UpgradeGhostId_tool,
    make_hlt2_tracks_without_UT, convert_tracks_to_v3_from_v1,
    get_track_master_fitter, get_global_materiallocator,
    get_global_measurement_provider, make_PrStoreSciFiHits_hits)


@configurable
def make_charged_seed():
    """ Seedmuon tracks make from the Standalone muon tracks for tracking efficiency study """
    withmuon_provider = partial(
        get_global_measurement_provider,
        vp_provider=DISABLE_TOOL,
        ut_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreVP=True,
        ignoreUT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    seedtracks = SeedMuonBuilder(
        MuonTracksLocation=StandaloneMuonRec(
            MuonHitsLocation=make_muon_hits()),
        SciFiTracksLocation=make_PrHybridSeeding_tracks()["v1"],
        Fitter=fitter())

    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[seedtracks])
    return charged_protos.Output


@configurable
def make_charged_downstream():
    """ Downstream tracks without clone killer for tracking efficiency study """
    scifi_tracks = make_seeding_tracks()
    down_tracks = make_PrLongLivedTracking_tracks(scifi_tracks)
    kf_down = PrKalmanFilter_Downstream(
        name="PrKalmanFilter_Downstream",
        MaxChi2=2.8,  ## copy from the default PrKalman fit
        MaxChi2PreOutlierRemoval=20,
        Input=down_tracks["Pr"],
        HitsUT=make_PrStorePrUTHits_hits(),
        HitsFT=make_PrStoreSciFiHits_hits(),
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        InputUniqueIDGenerator=make_unique_id_generator())

    best_tracks = {}
    best_tracks["v1"] = TrackBestTrackCreator(
        name="BestTrackCreator_Downstream",
        TracksInContainers=[kf_down],
        MaxChi2DoF=2.8,
        GhostIdTool=get_UpgradeGhostId_tool(),
        DoNotRefit=True,
        AddGhostProb=True,
        FitTracks=False).TracksOutContainer

    tracks_v3, tracks_rels = convert_tracks_to_v3_from_v1({
        'Downstream':
        best_tracks['v1']
    })

    muonRecConfs = make_all_muon_pids(
        tracks=tracks_v3, track_types=['Downstream'])
    muon_pids, muon_tracks = make_conv_muon_pids(
        muonRecConfs, tracks_rels, track_types=['Downstream'])
    addInfo = [
        ChargedProtoParticleAddMuonInfo(
            name='addMuonInfo', InputMuonPIDLocation=muon_pids['Downstream'])
    ]

    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[best_tracks["v1"]], AddInfo=addInfo)
    return charged_protos.Output


@configurable
def make_VeloMuon_tracks():
    """ Velomuon tracks make from the Standalone muon tracks for tracking efficiency study """
    withmuon_provider = partial(
        get_global_measurement_provider,
        ut_provider=DISABLE_TOOL,
        ft_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreUT=True,
        ignoreFT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    velomuontracks = VeloMuonBuilder(
        MuonTracksLocation=StandaloneMuonRec(
            MuonHitsLocation=make_muon_hits()),
        VeloTracksLocation=make_legacy_rec_hlt1_tracks()['Velo']['v1'],
        Fitter=fitter())

    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[velomuontracks])
    return charged_protos.Output


@configurable
def make_standalone_muontracks():
    '''
     Standalone muon tracks, be used to link velo tracks / add ut hits for tracking efficiency study
    '''
    # Tracks
    muon_tracks = StandaloneMuonRec(
        MuonHitsLocation=make_muon_hits(), SecondLoop=False)
    return {"v1": muon_tracks}


@configurable
def make_muonut_particles():
    """ MuonUT tracks made from Standalone muon tracks for tracking efficiency study """
    # Tracks
    muon_tracks = make_standalone_muontracks()["v1"]
    addUTtool = PrAddUTHitsTool(
        MaxChi2Tol=7.0,
        YTolSlope=40000.,
        XTolSlope=40000.,
        XTol=25,
        MinAxProj=5.5,
        MajAxProj=25,
        UTHitsLocation=make_PrStorePrUTHits_hits())

    withmuon_provider = partial(
        get_global_measurement_provider,
        vp_provider=DISABLE_TOOL,
        ft_provider=DISABLE_TOOL,
        muon_provider=MuonMeasurementProvider,
        ignoreVP=True,
        ignoreFT=True,
        ignoreMuon=False)
    fitter = partial(
        get_track_master_fitter, get_measurement_provider=withmuon_provider)
    muonut = MuonUTTracking(
        InputMuonTracks=muon_tracks,
        RecVertices=make_pvs(),
        Fitter=fitter(),
        Extrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()),
        AddUTHitsTool=addUTtool)

    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[muonut])
    return charged_protos.Output


@configurable
def make_charged_velo():
    hlt2_tracks = make_hlt2_tracks_without_UT(
        light_reco=True, fast_reco=False, use_pr_kf=True)
    velo_tracks = hlt2_tracks["Velo"]["v1"]

    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[velo_tracks], Code=F.require_all(~F.TRACKISVELOBACKWARD))
    return charged_protos.Output

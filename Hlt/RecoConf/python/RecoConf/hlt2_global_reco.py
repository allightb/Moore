###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Methods defining ProtoParticle makers, used as input to Particle selections.

The 'global' reconstruction is that which produces the final output of the
'full' HLT2 reconstruction: charged and neutral ProtoParticle containers.
"""
from .legacy_rec_hlt1_tracking import make_reco_pvs, make_FTRawBankDecoder_clusters, make_VeloClusterTrackingSIMD_hits
from .hlt2_tracking import make_hlt2_tracks, make_hlt2_tracks_without_UT, convert_tracks_to_v3_from_v1, get_persistable_tracks_per_type
from .rich_reconstruction import make_rich_pids, default_rich_reco_options, make_rich_pixels
from .calorimeter_reconstruction import make_calo
from .muon_reconstruction import make_all_muon_pids, make_conv_muon_pids

from .protoparticles import (
    make_charged_protoparticles as make_charged_protoparticles_from,
    make_neutral_protoparticles as make_neutral_protoparticles_from,
)

from PyConf import configurable
from PyConf.packing import persistable_location
from PyConf.Algorithms import (RecSummaryMaker, TracksEmptyProducer,
                               RichPIDsEmptyProducer, PVsEmptyProducer,
                               RecVertexEmptyProducer)


def make_reconstruction(light_reco=True,
                        usePatPVFuture=False,
                        use_pr_kf=True,
                        skipUT=False,
                        skipRich=False,
                        skipCalo=False,
                        skipMuon=False,
                        fastReco=False):
    """Return reconstruction objects of the legacy, fastest or the light reconstruction, with possibility to skip the UT

    """
    rich_and_charged_proto_track_types = ["Long"] if skipUT else [
        "Long", "Downstream", "Upstream"
    ]
    hlt2_tracks_maker = make_hlt2_tracks_without_UT if skipUT else make_hlt2_tracks
    hlt2_tracks = hlt2_tracks_maker(
        light_reco=light_reco, fast_reco=fastReco, use_pr_kf=use_pr_kf)

    persisted_tracks = get_persistable_tracks_per_type(hlt2_tracks)

    # track conversion to v3 for muon / calo
    tracks_v3, tracks_rels = convert_tracks_to_v3_from_v1(persisted_tracks)

    # PVs
    pvs = make_reco_pvs(hlt2_tracks['Velo'], persistable_location('PVs'))

    # calorimeter
    calo_pids = make_calo(
        tracks_v3, pvs["v3"], trackrels=tracks_rels) if not skipCalo else None

    # Muons
    muon_pids = {}
    muon_tracks = {}
    if not skipMuon:
        muonRecConfs = make_all_muon_pids(
            tracks=tracks_v3, track_types=rich_and_charged_proto_track_types)
        muon_pids, muon_tracks = make_conv_muon_pids(
            muonRecConfs,
            tracks_rels,
            track_types=rich_and_charged_proto_track_types)

    # RICH
    rich_pids = {}
    if not skipRich:
        rich_options = default_rich_reco_options()
        for track_type in ["Long", "Downstream", "Upstream"]:
            if track_type in rich_and_charged_proto_track_types:
                rich_pids[track_type] = make_rich_pids(
                    track_type, persisted_tracks[track_type],
                    rich_options)['RichPIDs']
            else:
                rich_pids[track_type] = RichPIDsEmptyProducer(
                    name=f"RichPIDsEmptyProducer_{track_type}" + "_{hash}",
                    allow_duplicate_instances_with_distinct_names=True).Output

    # split-per-track-type protoparticles
    charged_protos = {}
    for track_type in ["Long", "Downstream", "Upstream"]:
        charged_protos[track_type] = make_charged_protoparticles_from(
            name=f"ChargedProtoParticleMaker_{track_type}" + "_{hash}",
            tracks=persisted_tracks[track_type],
            rich_pids=rich_pids.get(track_type, None),
            calo_pids=calo_pids,
            muon_pids=muon_pids.get(track_type, None),
            track_types=[track_type],
            location=persistable_location(f'{track_type}Protos'))

    # neutral protoparticles
    neutral_protos = make_neutral_protoparticles_from(
        calo_pids=calo_pids,
        location_protos=persistable_location('NeutralProtos'),
        location_PIDs=None)

    ####
    # Define the output
    ####
    output = {
        "AllTrackHandles": hlt2_tracks,
        "VeloTracks": persisted_tracks.get('Velo', None),
        "FittedVeloTracks": persisted_tracks.get('FittedVelo', None),
        "Ttracks": persisted_tracks.get('Ttrack', None),
        "ExtendedPVs": pvs["v3"],
        "PVs": pvs["v1"],
        "NeutralProtos": neutral_protos['ProtoParticleLocation'],
        "NeutralPIDs": neutral_protos['NeutralPID'],
        "UpfrontReconstruction": []
    }

    for track_type, tracks in persisted_tracks.items():
        if track_type in charged_protos.keys():
            output[f'{track_type}Tracks'] = tracks

    for track_type, protos in charged_protos.items():
        output[f'{track_type}RichPIDs'] = rich_pids.get(track_type, None)
        output[f'{track_type}MuonPIDs'] = muon_pids.get(track_type, None)
        output[f'{track_type}Protos'] = protos

    if not skipCalo:
        output.update({
            "AllCaloHandles": calo_pids,
            "CaloElectrons": calo_pids["v1_electrons"],
            "CaloPhotons": calo_pids["v1_photons"],
            "CaloMergedPi0s": calo_pids["v1_mergedPi0s"],
            "CaloSplitPhotons": calo_pids["v1_splitPhotons"]
        })

    # for now to keep ttrack config as is alive
    # this is for specific fitted ttracks, not the standard unfitted decloned ones
    if "BestSeed" in hlt2_tracks:
        output["BestSeed"] = hlt2_tracks["BestSeed"]["v1"]

    return output


@configurable
def make_calo_only_reconstruction():
    """Return reconstruction objects of the calo only reconstruction

    """
    # make fake container of tracks and PVs
    tracks = {}
    for tt in ['Long', 'Downstream', 'Upstream', 'Ttrack', 'Velo']:
        tracks[tt] = TracksEmptyProducer(
            name=f"EmptyTrackProducer_{tt}",
            allow_duplicate_instances_with_distinct_names=True)
    extended_pvs = PVsEmptyProducer()
    pvs = RecVertexEmptyProducer()

    # Add Calo
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(tracks)
    calo = make_calo(tracks_v3, extended_pvs, trackrels=trackrels)

    neutral_protos = make_neutral_protoparticles_from(
        calo_pids=calo, location_PIDs=None)

    return {
        "AllCaloHandles": calo,
        "NeutralProtos": neutral_protos['ProtoParticleLocation'],
        "LongTracks": tracks['Long'],
        "DownstreamTracks": tracks['Downstream'],
        "Ttracks": tracks['Ttrack'],
        "VeloTracks": tracks['Velo'],
        "NeutralPID": neutral_protos['NeutralPID'],
        "ExtendedPVs": extended_pvs,
        "PVs": pvs,
        # The full data flow is functional, we only keep this for compatibility with reco from file.
        "UpfrontReconstruction": [],
        "CaloElectrons": calo["v1_electrons"],
        "CaloPhotons": calo["v1_photons"],
        "CaloMergedPi0s": calo["v1_mergedPi0s"],
        "CaloSplitPhotons": calo["v1_splitPhotons"],
    }


@configurable
def make_legacy_reconstruction(skipUT=False,
                               skipRich=False,
                               skipCalo=False,
                               skipMuon=False):
    """Return reconstruction objects of the legacy reconstruction
    """
    return make_reconstruction(
        light_reco=False,
        use_pr_kf=False,
        usePatPVFuture=False,
        skipUT=skipUT,
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=False)


@configurable
def make_light_reconstruction(usePatPVFuture=False,
                              use_pr_kf=True,
                              skipUT=False,
                              skipRich=False,
                              skipCalo=False,
                              skipMuon=False,
                              fastReco=False):
    return make_reconstruction(
        light_reco=True,
        usePatPVFuture=usePatPVFuture,
        use_pr_kf=use_pr_kf,
        skipUT=skipUT,
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=fastReco)


@configurable
def make_light_reco_pr_kf_without_UT(skipRich=False,
                                     skipCalo=False,
                                     skipMuon=False):
    """Shortcut for running the light reconstruction, with PrKalmanFilter, without UT
    """
    return make_light_reconstruction(
        skipUT=True,
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=False)


@configurable
def make_light_reco_pr_kf(skipRich=False, skipCalo=False, skipMuon=False):
    """Shortcut for running the light reconstruction, with PrKalmanFilter, with UT
    """
    return make_light_reconstruction(
        skipRich=skipRich,
        skipCalo=skipCalo,
        skipMuon=skipMuon,
        fastReco=False)


@configurable
def make_fastest_reconstruction(skipUT=False):
    """Shortcut for running the fastest reconstruction
    """
    return make_light_reconstruction(skipUT=skipUT, fastReco=True)


def make_rec_summary(reco):
    calo = reco['AllCaloHandles']

    # FIXME for now quick fix to not handle upstream tracks as not to trigger them in the scheduler
    # needs proper handling with optional existance?
    tracks_up = TracksEmptyProducer(
        name="TracksEmptyProducer_Upstream_ForRecSummary_{hash}",
        allow_duplicate_instances_with_distinct_names=True).Output

    return RecSummaryMaker(
        LongTracks=reco["LongTracks"],
        DownstreamTracks=reco["DownstreamTracks"],
        VeloTracks=reco["VeloTracks"],
        UpstreamTracks=tracks_up,
        Ttracks=reco["Ttracks"],
        PVs=reco["PVs"],
        SciFiClusters=make_FTRawBankDecoder_clusters(),
        VeloClusters=make_VeloClusterTrackingSIMD_hits(),
        eCalClusters=calo['ecalClusters'],
        EDigits=calo['digitsEcal'],
        HDigits=calo['digitsHcal'],
        RichPixels=make_rich_pixels(
            default_rich_reco_options())['RichDecodedData'],
        outputs={
            "Output": persistable_location('RecSummary')
        }).Output


@configurable
def reconstruction(make_reconstruction=make_light_reco_pr_kf_without_UT):
    """Hook to allow switching between reconstructions.

    make_reconstruction (function): which reconstruction to run.

    Note it is advised to use this function if more than one object is needed,
    rather than the accessors below  as it makes the configuration slower.
    """
    reco_output = make_reconstruction()
    # Create the summary of the reconstruction
    reco_output["RecSummary"] = make_rec_summary(reco_output)
    return reco_output


def make_charged_protoparticles(track_type):
    """Return a DataHandle to the container of charged ProtoParticles.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """
    return reconstruction()[track_type]


def upfront_reconstruction():
    """Return sequence to create charged ProtoParticles.

    Be aware that the node needs to be configures with the mode `force_order=True`.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """

    return reconstruction()["UpfrontReconstruction"]


def make_neutral_protoparticles():
    """Return a DataHandle to the container of neutral ProtoParticles.

    The neutral ProtoParticle making is functional, there is no need to
    schedule the upfront reconstruction explicitly.
    """

    return reconstruction()["NeutralProtos"]


def make_pvs():
    """Return a DataHandle to the container of PVs
    """

    return reconstruction()["PVs"]


def make_tracks(track_type="Long"):
    """Return a DataHandle to the container of certain tracks
    """
    return reconstruction()[f"{track_type}Tracks"]

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CaloFutureClusterResolution, CaloFutureHypoResolution
from PyConf.Algorithms import CaloFutureDigit2MCLinks2Table, CaloClusterMCTruth
from PyConf.Algorithms import CaloClusterEfficiency, CaloHypoEfficiency, CaloChargedPIDsChecker
from .data_from_file import boole_links_digits_mcparticles, mc_unpackers
from .mc_checking import make_links_lhcbids_mcparticles_tracking_system, make_links_tracks_mcparticles


def get_mc_info(calo):
    # get digits and clusters
    ecalDigits = calo["digitsEcal"]
    ecalClusters = calo["ecalClusters"]

    # get MCParticles
    mcparts = mc_unpackers()["MCParticles"]

    # get Digit2MC table
    tableMCCaloDigits = CaloFutureDigit2MCLinks2Table(
        CaloDigits=ecalDigits,
        MCParticles=mcparts,
        Link=boole_links_digits_mcparticles()["EcalDigitsV1"],
    ).Output

    # get Clusterv22MC table
    tableMCCaloClusters = CaloClusterMCTruth(
        InputRelations=tableMCCaloDigits,
        Input=boole_links_digits_mcparticles()['EcalDigits'],
        MCParticleLocation=mcparts,
        Clusters=ecalClusters).Output

    return [mcparts, tableMCCaloDigits, tableMCCaloClusters]


def check_calo_cluster_resolution(tuplePrefix, clusters, tableMCCaloClusters):
    # look only at photons from Bd->K*gamma
    # NB it works for legacy clusters
    # (currently clusters after shower overlap,
    # eventually it should be out of use and removed)
    return [
        CaloFutureClusterResolution(
            input=clusters,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='clusters' + tuplePrefix,
            PDGID=[22],
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name='CaloClusterResolution' + tuplePrefix)
    ]


def check_calo_photon_resolution(tuplePrefix, photons, tableMCCaloClusters):
    # look only at photons from Bd->K*gamma
    # NB photons are CaloHypos
    return [
        CaloFutureHypoResolution(
            input=photons,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='photons' + tuplePrefix,
            PDGID=[22],
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name="CaloHypoResolution" + tuplePrefix)
    ]


def check_calo_pi0_resolution(tuplePrefix, pi0, tableMCCaloClusters):
    # look only at pi0 from Bd->pi+pi-pi0
    # NB pi0 are CaloHypos
    return [
        CaloFutureHypoResolution(
            input=pi0,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='pi0' + tuplePrefix,
            PDGID=[111],  # brother 211 & -211
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name="CaloHypoResolution" + tuplePrefix)
    ]


def check_calo_cluster_efficiency(calo, parentID=511, name="", minET=50.):
    # get MC tables
    mcparts, tableMCDigits, tableMCClusters = get_mc_info(calo)
    # from B: default
    return [
        CaloClusterEfficiency(
            inputRelationsCluster=tableMCClusters,
            inputRelationsDigitOld=tableMCDigits,
            inputRelationsDigit=boole_links_digits_mcparticles()['EcalDigits'],
            inputMCParticles=mcparts,
            recoClusters=calo["ecalClusters"],
            caloDigits=calo["digitsEcal"],
            minMCfraction=0.9,
            minMatchFraction=0.9,
            minET=minET,
            minEndVtxZ=7000.,
            name="CaloClusterEff" + name,
            PDGIDParent=parentID)
    ]


def check_pi0_cluster_efficiency(calo, fromB=511, name=""):
    # get MC tables
    mcparts, tableMCDigits, tableMCClusters = get_mc_info(calo)
    # resolved case
    # particle - photon
    # parent - pi0
    # grandparent - fromB
    eff_resolved = CaloClusterEfficiency(
        inputRelationsCluster=tableMCClusters,
        inputRelationsDigitOld=tableMCDigits,
        inputRelationsDigit=boole_links_digits_mcparticles()['EcalDigits'],
        inputMCParticles=mcparts,
        recoClusters=calo["ecalClusters"],
        caloDigits=calo["digitsEcal"],
        PDGID=22,
        PDGIDParent=111,
        PDGIDGrandparent=fromB,
        minMCfraction=0.9,
        minMatchFraction=0.9,
        minET=50.0,
        minEndVtxZ=7000.,
        name="CaloClusterEff_resolvedPi0" + name)

    # merged case
    # particle - pi0
    # parent - B0
    eff_merged = CaloClusterEfficiency(
        inputRelationsCluster=tableMCClusters,
        inputRelationsDigitOld=tableMCDigits,
        inputRelationsDigit=boole_links_digits_mcparticles()['EcalDigits'],
        inputMCParticles=mcparts,
        recoClusters=calo["ecalClusters"],
        caloDigits=calo["digitsEcal"],
        PDGID=111,
        PDGIDParent=fromB,
        minMCfraction=0.9,
        minMatchFraction=0.9,
        minET=50.0,
        minEndVtxZ=0.,
        name="CaloClusterEff_mergedPi0" + name)

    return [eff_resolved, eff_merged]


def check_calo_hypo_efficiency(
        calo,
        parentID=[511, 521, 531, 541, 5122, 5132, 5232, 5332],
        name="",
        minET=50.):
    # get MC tables
    mcparts, _, tableMCCaloClusters = get_mc_info(calo)
    # get efficiency
    return [
        CaloHypoEfficiency(
            clusters=calo["ecalClusters"],
            photonHypos=calo["v2_photons"],
            electrHypos=calo["v2_electrons"],
            Relations=tableMCCaloClusters,
            MCParticles=mcparts,
            minMatchFraction=0.9,
            minET=minET,
            minEndVtxZ=7000.,
            name="CaloHypoEff" + name,
            PDGIDParent=parentID,
        )
    ]


def check_calo_charged_pids(tracks,
                            pids,
                            brems,
                            checktracktype=False,
                            tracktype="Long"):
    mcparts = mc_unpackers()["MCParticles"]
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=tracks, LinksToLHCbIDs=links_to_lhcbids)
    return CaloChargedPIDsChecker(
        Tracks=tracks,
        ChargedPIDs=pids,
        BremInfos=brems,
        MCLinks=links_to_tracks,
        MCParticles=mcparts,
        CheckTrackType=checktracktype,
        TrackType=tracktype)


def check_calo_efficiency_all(calo, highET=True):
    data = check_calo_cluster_efficiency(
        calo, parentID=0, name="_ET250", minET=250.)  # any parent
    if highET:
        data += check_calo_cluster_efficiency(
            calo, parentID=0, name="_ET3000", minET=3000.)  # any parent
    data += check_calo_cluster_efficiency(
        calo, parentID=11, name="_Brem_ET50", minET=50.)  # any parent
    data += check_pi0_cluster_efficiency(
        calo, fromB=0, name="_All")  # pi0 from any parent
    data += check_calo_hypo_efficiency(
        calo, parentID=[], name="_ET250", minET=250.)  # any parent
    data += check_calo_hypo_efficiency(
        calo, parentID=[
            11,
        ], name="_Brem_ET50", minET=50.)  # from e+-
    return data


def check_calo_efficiency_fromB(calo, hypos=True):
    data = check_calo_cluster_efficiency(calo)  # default - fromB
    data += check_pi0_cluster_efficiency(calo)  # default - fromB
    if hypos:
        data += check_calo_hypo_efficiency(calo)  # default - fromB
    return data

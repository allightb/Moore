###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.legacy_rec_hlt1_tracking import (all_upstream_track_types,
                                               make_legacy_rec_hlt1_tracks)
from RecoConf.hlt2_tracking import (
    make_PrForwardTracking_tracks, make_PrVeloUTFilter_tracks,
    all_hlt2_forward_track_types, get_PrForwardTracksFromVeloUTV1_converter)
from RecoConf.mc_checking import get_track_checkers
from RecoConf.event_filters import require_gec


def test_ut_filter_modes():

    hlt1_tracks = make_legacy_rec_hlt1_tracks()
    with make_PrForwardTracking_tracks.bind(
            momentum_window=False, delta_quality=0.24, add_ut_hits=True):
        forward_tracks_no_mom = all_hlt2_forward_track_types(
            hlt1_tracks["Upstream"],
            make_forward_tracks=make_PrForwardTracking_tracks,
            converter=get_PrForwardTracksFromVeloUTV1_converter)
    with make_PrForwardTracking_tracks.bind(
            momentum_window=True, delta_quality=0.24, add_ut_hits=True):
        forward_tracks_with_mom = all_hlt2_forward_track_types(
            hlt1_tracks["Upstream"],
            make_forward_tracks=make_PrForwardTracking_tracks,
            converter=get_PrForwardTracksFromVeloUTV1_converter)

    data = [
        forward_tracks_no_mom["Pr"],
        forward_tracks_with_mom["Pr"],
    ]
    data += get_track_checkers({"Forward": forward_tracks_no_mom})
    data += get_track_checkers({"Forward": forward_tracks_with_mom})

    return Reconstruction('hlt2_forward_track_reco_ut_filtered', data,
                          [require_gec()])


with all_upstream_track_types.bind(
        make_velo_ut_tracks=make_PrVeloUTFilter_tracks):
    run_reconstruction(options, test_ut_filter_modes)

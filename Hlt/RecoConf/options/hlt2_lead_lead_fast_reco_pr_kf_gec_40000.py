###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.legacy_rec_hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_reco_pvs, make_PatPV3DFuture_pvs)
from RecoConf.hlt2_tracking import make_TrackBestTrackCreator_tracks

from RecoConf.event_filters import require_gec
from RecoConf.muonid import make_muon_hits

from RecoConf.standalone import standalone_hlt2_light_reco

from PyConf.Algorithms import PrHybridSeeding
from PyConf.packing import persistreco_writing_version

with standalone_hlt2_light_reco.bind(do_mc_checking=False, do_data_monitoring=False, use_pr_kf=False, fast_reco=False),\
    require_gec.bind(cut=40000,skipUT=False), \
    PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True), \
    make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.8),\
    make_muon_hits.bind(geometry_version=3),\
    persistreco_writing_version.bind(version=1.1):
    config = run_reconstruction(options, standalone_hlt2_light_reco)

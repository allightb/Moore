###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from functools import partial
from PyConf.Algorithms import PrKalmanFilterToolExampleAlgo_V1V1, PrKalmanFilterToolExampleAlgo_V1V1_noUT, StandaloneMuonRec, MuonSeeding, MuonTrackSelector
from PyConf.Algorithms import TrackMuonMatching
from PyConf.Tools import KalmanFilterTool, TrackMasterExtrapolator, MuonMeasurementProvider
from RecoConf.core_algorithms import make_unique_id_generator
from RecoConf.legacy_rec_hlt1_tracking import (
    make_VeloClusterTrackingSIMD_hits, make_PrStorePrUTHits_hits)
from RecoConf.hlt2_tracking import (
    make_PrStoreSciFiHits_hits, get_global_materiallocator,
    get_track_master_fitter, get_global_measurement_provider)
from RecoConf.standalone import reco_prefilters
from RecoConf.mc_checking import check_track_resolution
from RecoConf.muonid import make_muon_hits


def run():

    meas_prov = partial(
        get_global_measurement_provider,
        muon_provider=MuonMeasurementProvider,
        ignoreMuon=False)

    fitter = partial(
        get_track_master_fitter, get_measurement_provider=meas_prov)

    muontracks = MuonSeeding(
        InputMuonTracks=StandaloneMuonRec(
            MuonHitsLocation=make_muon_hits(), SecondLoop=False, Chi2Cut=True),
        Fitter=fitter(),
        Extrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()))

    selected_muon_tracks = MuonTrackSelector(TracksInputContainer=muontracks,\
        MuonPcut=6000.,
        inCaloAcceptance=True,
        noOverlap=False,
        minHitStation=3).TracksOutputContainer

    from RecoConf.hlt2_tracking import make_hlt2_tracks
    tracks = make_hlt2_tracks(False, False, False)

    matched_tracks = TrackMuonMatching(
        MuonTracksLocation=selected_muon_tracks,
        TracksLocation=tracks['Best']['v1'],
        trackType="Long",
        AllCombinations=False,  # default true
        MatchAtFirstMuonHit=True,
        MatchChi2Cut=20.0,
        Fitter=fitter()).TracksOutputLocation

    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ut_hits = make_PrStorePrUTHits_hits()
    ft_hits = make_PrStoreSciFiHits_hits()
    muon_hits = make_muon_hits()

    max_chi2_v1 = 2.5

    fitted_matched_tracks = PrKalmanFilterToolExampleAlgo_V1V1(
        HitsVP=vp_hits,
        HitsUT=ut_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name="PrKalmanFilterToolLongMuonV1",
        Input=matched_tracks).OutputTracks

    tracks["v1"] = fitted_matched_tracks

    resolutions = []
    reso = check_track_resolution(tracks, per_hit_resolutions=True)
    resolutions.append(reso)

    from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT
    tracks_without_UT = make_hlt2_tracks_without_UT()

    matched_tracks_without_UT = TrackMuonMatching(
        MuonTracksLocation=selected_muon_tracks,
        TracksLocation=tracks_without_UT['BestLong']['v1'],
        trackType="Long",
        AllCombinations=False,  # default true
        MatchAtFirstMuonHit=True,
        MatchChi2Cut=20.0,
        Fitter=fitter()).TracksOutputLocation

    fitted_matched_tracks_without_UT = PrKalmanFilterToolExampleAlgo_V1V1_noUT(
        HitsVP=vp_hits,
        HitsFT=ft_hits,
        HitsMuon=muon_hits,
        TrackFitter=KalmanFilterTool(
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()),
            MaxChi2PreOutlierRemoval=20,
            MaxChi2=max_chi2_v1,
            UniqueIDGenerator=make_unique_id_generator()),
        name="PrKalmanFilterToolLongMuonV1_noUT",
        Input=matched_tracks_without_UT).OutputTracks

    tracks_without_UT["v1"] = fitted_matched_tracks_without_UT

    reso = check_track_resolution(tracks_without_UT, per_hit_resolutions=True)
    resolutions.append(reso)

    data = [fitted_matched_tracks, fitted_matched_tracks_without_UT]
    data += resolutions
    return Reconstruction('hlt2_reco_pr_kf_tool', data,
                          reco_prefilters(skipUT=True))


options.histo_file = "longmuon_reso_histos.root"
run_reconstruction(options, run)

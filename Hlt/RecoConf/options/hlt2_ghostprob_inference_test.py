###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# configuration
from optparse import OptionParser
parser = OptionParser()
parser.add_option("-d", "--data", dest="data")
parser.add_option("-m", "--model", dest="model", default="Long_noUT")
parser.add_option("-w", "--weights", dest="weights", default="")
parser.add_option("-p", "--paramfile", dest="paramfile", default="")
parser.add_option("--nEntries", dest="nEntries", type="int", default=-1)
(options, args) = parser.parse_args()

model_name = options.model

weights_filename = ''
if options.weights and not options.paramfile:
    weights_filename = options.weights
elif options.paramfile and not options.weights:
    import os
    weights_filename = os.environ['PARAMFILESROOT'] + "/" + options.paramfile

# import data
import ROOT as R
rdf_all = R.RDataFrame(
    f'TrainingTupleAlg_GhostProb_{model_name}/TrainingTuple', options.data)
rdf_columns = rdf_all.GetColumnNames()

# limit to a sensible maximum events
rdf = rdf_all.Range(options.nEntries) if options.nEntries > 0 else rdf_all


# import model
def get_model(name):
    if name == 'Long_noUT':
        from TrackTools.ghostprob_models import GhostProb_Long_noUT as model
        return model
    elif name == 'Ttrack':
        from TrackTools.ghostprob_models import GhostProb_Ttrack as model
        return model
    return None


model = get_model(model_name)
feats = model.features()

print(f"Defined model '{model_name}' with features:", feats)
print("model coverted to PyTorch:", model)

# training data
import numpy as np
import pandas as pd
others = [
    'MC_TRUEID', 'MC_MOTHER_ID', 'MC_GMOTHER_ID', 'MC_TRUE_P', 'model_output'
]
obs = feats + others

df = pd.DataFrame(rdf.AsNumpy(obs))

df['sig'] = np.where(
    (np.abs(df['MC_TRUEID']) != 0) & (np.abs(df['MC_TRUEID']) != 11), True,
    False)
df['bkg'] = np.where((np.abs(df['MC_TRUEID']) == 0), True, False)
df['flag'] = np.where(df['sig'], True, False)
selected_df = df[(df['sig'] | df['bkg'])]

datafeats = selected_df[feats]
dataflag = selected_df[['flag']]
dataref = selected_df[['model_output']]

# convert to PyTorch compatible data
import torch

X = torch.tensor(datafeats.values, dtype=torch.float32)
y = torch.tensor(dataflag.values, dtype=torch.float32)
r = torch.tensor(dataref.values, dtype=torch.float32)

# check performance
from sklearn.metrics import roc_auc_score

model.read_from_json(weights_filename)
model.eval()

y = y.detach().cpu().numpy()
pred = model.forward_flattened(X)
ref = r.detach().cpu().numpy()
diff = pred - ref
diff_mean = diff.mean()
diff_std = diff.std()

print(
    f"difference between model and ref (probability; default) is {diff_mean:.4f} +/- {diff_std:.4f}"
)

auc_pred = roc_auc_score(y, pred)
auc_ref = roc_auc_score(y, ref)
print(f"AUC of prediction is {auc_pred:.5f} and of reference is {auc_ref:.5f}")

if round(abs(diff_mean), 3) > 0. or round(abs(diff_std), 2) > 0.:
    raise ValueError(
        'difference between LHCb SIMD NN inference and PyTorch too large')

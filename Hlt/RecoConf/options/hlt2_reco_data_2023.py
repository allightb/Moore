###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT
from RecoConf.legacy_rec_hlt1_tracking import (
    make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks)

with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
     make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
     make_PatPV3DFuture_pvs.bind(velo_open=True),\
     make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
     make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.2),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=4.2):
    run_reconstruction(options, standalone_hlt2_light_reco_without_UT)

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.data_from_file import mc_unpackers
from RecoConf.legacy_rec_hlt1_tracking import (make_legacy_rec_hlt1_tracks,
                                               make_PrStoreSciFiHits_hits)

from RecoConf.hlt2_tracking import (make_seeding_tracks,
                                    get_global_ut_hits_tool)

from RecoConf.mc_checking import (
    make_links_lhcbids_mcparticles_tracking_system)

from PyConf.Algorithms import (PrMatchNN, PrTrackAssociator,
                               PrForwardTrackingVelo)
from PyConf.Tools import (PrMCDebugMatchToolNN, PrMCDebugForwardTool)
from PyConf.reading import get_mc_track_info

options.ntuple_file = ('pr_tracking_ntuple.root')
options.evt_max = 100

# run with
# ./Moore/run gaudirun.py Moore/Hlt/Moore/tests/options/default_input_and_conds_hlt2.py Moore/Hlt/RecoConf/options/tracking_developments/run_pr_tracking_debug.py


def run_tracking_debug():
    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()

    hlt1_tracks = make_legacy_rec_hlt1_tracks()
    seed_tracks = make_seeding_tracks()

    # add MCLinking to the (fitted) V1 tracks
    links_to_velo_tracks = PrTrackAssociator(
        SingleContainer=hlt1_tracks["Velo"]["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    links_to_seed_tracks = PrTrackAssociator(
        SingleContainer=seed_tracks["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    match_debug = PrMatchNN(
        VeloInput=hlt1_tracks["Velo"]["Pr"],
        SeedInput=seed_tracks["Pr"],
        MatchDebugToolName=PrMCDebugMatchToolNN(
            VeloTracks=hlt1_tracks["Velo"]["v1"],
            SeedTracks=seed_tracks["v1"],
            VeloTrackLinks=links_to_velo_tracks,
            SeedTrackLinks=links_to_seed_tracks,
            TrackInfo=get_mc_track_info(),
            MCParticles=mc_unpackers()["MCParticles"]),
        AddUTHitsToolName=get_global_ut_hits_tool(enable=True),
    ).MatchOutput

    forward_debug = PrForwardTrackingVelo(
        InputTracks=hlt1_tracks["Velo"]["Pr"],
        SciFiHits=make_PrStoreSciFiHits_hits(),
        AddUTHitsToolName=get_global_ut_hits_tool(enable=True),
        DebugTool=PrMCDebugForwardTool(
            InputTracks=hlt1_tracks["Velo"]["v1"],
            InputTrackLinks=links_to_velo_tracks,
            MCParticles=mc_unpackers()["MCParticles"],
            SciFiHitLinks=links_to_hits,
            SciFiHits=make_PrStoreSciFiHits_hits(),
            TrackInfo=get_mc_track_info(),
        ))

    data = [match_debug, forward_debug]

    return Reconstruction('run_tracking_debug', data)


run_reconstruction(options, run_tracking_debug)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import VPRetinaFullClustering
from Moore.config import Reconstruction
from Moore.config import run_allen_reconstruction
from Moore import options
from RecoConf.hlt1_allen import make_allen_downstream_tracks, allen_gaudi_config
from RecoConf.legacy_rec_hlt1_tracking import make_velo_full_clusters
from RecoConf.mc_checking import (
    check_tracking_efficiency, make_links_tracks_mcparticles,
    make_links_lhcbids_mcparticles_tracking_system)
from RecoConf.mc_checking_categories import (get_mc_categories,
                                             get_hit_type_mask)
from AllenCore.gaudi_allen_generator import allen_runtime_options


def make_reconstruction():

    downstream_tracks = make_allen_downstream_tracks()

    # make links to lhcb id for mc matching
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()

    # build the PrChecker algorihm
    links_to_downstream_tracks = make_links_tracks_mcparticles(
        InputTracks=downstream_tracks["v1keyed"],
        LinksToLHCbIDs=links_to_lhcbids)

    pr_checker_for_downstream_track = check_tracking_efficiency(
        TrackType="Downstream",
        InputTracks=downstream_tracks["v1keyed"],
        LinksToTracks=links_to_downstream_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("Downstream"),
        HitTypesToCheck=get_hit_type_mask("Downstream"),
    )

    return Reconstruction("track_efficiency",
                          [pr_checker_for_downstream_track], [])


options.histo_file = "Hlt1DownstreamTrackingResolutionAllen.root"

with (make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
      allen_gaudi_config.bind(
          sequence='hlt1_pp_forward_then_matching_and_downstream'),
      allen_runtime_options.bind(
          filename="allen_gaudi_downstream_monitor.root")):
    run_allen_reconstruction(options, make_reconstruction)

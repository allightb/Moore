###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import Reconstruction, run_allen_reconstruction
from Moore import options

from RecoConf.calorimeter_mc_checking import check_calo_cluster_resolution
from RecoConf.calorimeter_reconstruction import make_calo_reduced
from RecoConf.data_from_file import boole_links_digits_mcparticles, mc_unpackers
from RecoConf.hlt1_allen import make_allen_calo_clusters, allen_gaudi_config
from AllenCore.gaudi_allen_generator import allen_runtime_options

from PyConf.Algorithms import CaloFutureDigit2MCLinks2Table, CaloClusterMCTruth


def make_reconstruction():
    data_producers = []

    # Get Allen calo clusters
    allen_calo_clusters = make_allen_calo_clusters()
    ecal_clusters_allen = allen_calo_clusters["ecal_clusters"]

    # Get calo digits
    calo = make_calo_reduced()
    ecal_digits = calo['digitsEcal']

    # get MCParticles
    mcparts = mc_unpackers()["MCParticles"]

    # get Digit2MC table
    tableMCCaloDigits = CaloFutureDigit2MCLinks2Table(
        CaloDigits=ecal_digits,
        MCParticles=mcparts,
        Link=boole_links_digits_mcparticles()["EcalDigitsV1"],
    ).Output

    # get Clusterv22MC table
    tableMCCaloClusters = CaloClusterMCTruth(
        InputRelations=tableMCCaloDigits,
        Input=boole_links_digits_mcparticles()['EcalDigits'],
        MCParticleLocation=mcparts,
        Clusters=ecal_clusters_allen).Output

    # Build CaloFutureClusterResolution
    calo_clusters_resolution = check_calo_cluster_resolution(
        "AllenClustersRes", ecal_clusters_allen, tableMCCaloClusters)
    data_producers += calo_clusters_resolution

    return Reconstruction('calo_cluster_resolution', data_producers)


options.evt_max = 1000
options.histo_file = "Hlt1RecoAllenCaloResolutionHistos.root"
options.ntuple_file = "Hlt1RecoAllenCaloResolutionNTuple.root"

with (allen_gaudi_config.bind(sequence='hlt1_cosmics'),
      allen_runtime_options.bind(filename="allen_calo_res_monitor.root")):
    run_allen_reconstruction(options, make_reconstruction)

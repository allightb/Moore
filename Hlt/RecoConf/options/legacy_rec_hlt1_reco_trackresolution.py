###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.legacy_rec_hlt1_tracking import make_legacy_rec_hlt1_tracks, make_VeloKalman_fitted_tracks
from RecoConf.mc_checking import check_track_resolution
from PyConf.reading import get_mc_track_info


def hlt1_reco_trackresolution():
    track_type = "Forward"
    tracks = make_legacy_rec_hlt1_tracks()[track_type]
    fitted_tracks = make_VeloKalman_fitted_tracks(
        make_legacy_rec_hlt1_tracks())
    pr_checker = check_track_resolution(tracks)
    pr_checker_fitted = check_track_resolution(fitted_tracks)

    return Reconstruction('track_resolution',
                          [get_mc_track_info(), pr_checker, pr_checker_fitted],
                          [require_gec()])


options.histo_file = "Hlt1ForwardTrackingResolution.root"
run_reconstruction(options, hlt1_reco_trackresolution)

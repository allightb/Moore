###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CompareRecAllenUTHitsMCCheck
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Allen.config import setup_allen_non_event_data_service
from AllenConf.ut_reconstruction import decode_ut
from RecoConf.data_from_file import mc_unpackers
from RecoConf.legacy_rec_hlt1_tracking import make_PrStorePrUTHits_hits
from Moore import options

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service(bank_types=["UT"])
allen_decoded_ut = decode_ut()

with make_PrStorePrUTHits_hits.bind(isCluster=False):
    rec_ut_hits = make_PrStorePrUTHits_hits()

    test_hits = CompareRecAllenUTHitsMCCheck(
        ut_hit_offsets=allen_decoded_ut["dev_ut_hit_offsets"],
        ut_hits=allen_decoded_ut["dev_ut_hits"],
        UnpackedUTHits=mc_unpackers()['MCUTHits'],
        UTHitsLocation=rec_ut_hits)

    cf_node = CompositeNode(
        'compare_ut_hits_with_mcchecking',
        combine_logic=NodeLogic.NONLAZY_OR,
        children=[non_event_data_node, test_hits],
        force_order=True)
    config.update(configure(options, cf_node))

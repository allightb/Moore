###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_calo_efficiency

options.evt_max = 1000
options.ntuple_file = 'outputfile_calo_eff_gamma_light_reco.root'

with standalone_hlt2_calo_efficiency.bind(light_reco=True):
    run_reconstruction(options, standalone_hlt2_calo_efficiency)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CompareRecAllenFTClusters
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Allen.config import setup_allen_non_event_data_service
from AllenConf.scifi_reconstruction import decode_scifi
from RecoConf.legacy_rec_hlt1_tracking import make_FTRawBankDecoder_clusters
from Moore import options

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service(
    bank_types=["FTCluster"])
allen_decoded_scifi = decode_scifi()

ft_lite_clusters = make_FTRawBankDecoder_clusters()

test_clusters = CompareRecAllenFTClusters(
    scifi_offsets=allen_decoded_scifi["dev_scifi_hit_offsets"],
    scifi_hits=allen_decoded_scifi["dev_scifi_hits"],
    FTClusterLocation=ft_lite_clusters)

cf_node = CompositeNode(
    'compare_ft_clusters',
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_clusters],
    force_order=True)
config.update(configure(options, cf_node))

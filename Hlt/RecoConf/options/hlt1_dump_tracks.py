###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input, configure
from Hlt1Conf.algorithms import DumpContainer
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.legacy_rec_hlt1_tracking import (
    make_legacy_rec_hlt1_tracks, make_legacy_rec_hlt1_fitted_tracks)
from PyConf.Algorithms import (ProduceSOATracks)
from Functors import (P, PT, ETA, PHI, POSITION_X, POSITION_Y, POSITION_Z, TX,
                      TY, COV, CHI2DOF, CLOSESTTOBEAM, NDOF, QOVERP)


def dump_hlt1_reco(dump_dummies=False):
    """Configure algorithms to dump HLT1 reconstructed objects to a ROOT TTree

    This uses the DumpContainer algorithm to dump the output of the VeloKalman
    algorithm to a TTree in a ROOT file.

    Parameters
    ----------
    dump_dummies : bool
        Write a second TTree/ROOT file containing fake "VeloKalman fitted"
        tracks produced by the ProduceSOATracks algorithm. This is
        useful for checking the accuracy of the fake track generation.
    """
    hlt1_tracks = make_legacy_rec_hlt1_tracks()
    fitted_tracks = make_legacy_rec_hlt1_fitted_tracks(hlt1_tracks)
    # Set up the dumper for fitted tracks
    Branches = {
        'P': P,
        'PT': PT,
        'ETA': ETA,
        'PHI': PHI,
        'NDOF': NDOF,
        'QOVERP': QOVERP,
        'CHI2DOF': CHI2DOF,
        'X': POSITION_X @ CLOSESTTOBEAM,
        'Y': POSITION_Y @ CLOSESTTOBEAM,
        'Z': POSITION_Z @ CLOSESTTOBEAM,
        'TX': TX @ CLOSESTTOBEAM,
        'TY': TY @ CLOSESTTOBEAM,
        'COV_X_X': COV(Row=0, Col=0) @ CLOSESTTOBEAM,
        'COV_Y_Y': COV(Row=1, Col=1) @ CLOSESTTOBEAM,
        'COV_X_TX': COV(Row=0, Col=2) @ CLOSESTTOBEAM,
        'COV_Y_TY': COV(Row=1, Col=3) @ CLOSESTTOBEAM,
        'COV_TX_TX': COV(Row=2, Col=2) @ CLOSESTTOBEAM,
        'COV_TY_TY': COV(Row=3, Col=3) @ CLOSESTTOBEAM,
    }
    # Collect together the algorithms we are interested in running
    nodes = []
    # Dump the real VeloKalman output
    fitted_dumper = DumpContainer(
        fitted_tracks['Pr'],
        Branches,
        dump_filename='PrFittedForwardTracks_MC.root')
    nodes.append(fitted_dumper)
    if dump_dummies:
        # If requested, also dump the fake PrFittedForwardTracks generated by
        # the dummy producer
        fake_dumper = DumpContainer(
            ProduceSOATracks().Output,
            Branches,
            dump_filename='PrFittedForwardTracks_Fake.root')
        nodes.append(fake_dumper)

    return CompositeNode(
        'hlt1_reco_dumping',
        children=nodes,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)


# Don't both processing many events in the nightlies
options.evt_max = 10

config = configure_input(options)
top_cf_node = dump_hlt1_reco()
config.update(configure(options, top_cf_node))

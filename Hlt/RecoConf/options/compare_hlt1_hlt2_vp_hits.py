###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CompareRecAllenVPHits
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Allen.config import setup_allen_non_event_data_service
from AllenConf.velo_reconstruction import decode_velo
from RecoConf.legacy_rec_hlt1_tracking import make_RetinaClusters
from Moore import options

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service(bank_types=["VP"])
allen_decoded_velo = decode_velo()

test_hits = CompareRecAllenVPHits(
    vp_hits_num=allen_decoded_velo["dev_module_cluster_num"],
    vp_hit_offsets=allen_decoded_velo["dev_offsets_estimated_input_size"],
    vp_hits=allen_decoded_velo["dev_sorted_velo_cluster_container"],
    VPHitsLocation=make_RetinaClusters())

cf_node = CompositeNode(
    'compare_vp_hits',
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_hits],
    force_order=True)
config.update(configure(options, cf_node))

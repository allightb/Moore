###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.standalone import phoenix_data_dump_hlt2
from Moore.qmtest.context import download_mdf_inputs_locally

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'UT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
    'Muon'
]

options.evt_max = 20
options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
options.phoenix_filename = "LHCb_EventDataset.json"
options.set_input_and_conds_from_testfiledb('2023_07_04_run268773')

AVERAGE_EVENT_SIZE = 500 * 1000  # upper limit of average event size
options.input_files = download_mdf_inputs_locally(
    options.input_files,
    dest_dir='.',
    max_size=options.evt_max * AVERAGE_EVENT_SIZE)

with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"):
    run_reconstruction(options, phoenix_data_dump_hlt2)

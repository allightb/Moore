###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction

from Moore.config import Reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.legacy_rec_hlt1_tracking import make_all_pvs, make_velo_full_clusters
from RecoConf.hlt1_allen import make_allen_forward_tracks, make_allen_velo_tracks, allen_gaudi_config
from RecoConf.hlt2_global_reco import reconstruction
from RecoConf.mc_checking import checker_trigger_objects
from AllenConf.hlt1_reconstruction import hlt1_reconstruction
from PyConf.Algorithms import VPRetinaFullClustering


def reco():

    allen_forward_tracks = make_allen_forward_tracks()
    allen_velo_tracks = make_allen_velo_tracks()

    allen_pvs = hlt1_reconstruction(with_ut=False)["pvs"]

    pvs = make_all_pvs()["v1"]

    tracks = reconstruction()["LongTracks"]
    Velotracks = reconstruction()["VeloTracks"]

    checker = checker_trigger_objects(
        allen_forward_tracks["v1"],
        tracks,
        allen_pvs,
        pvs,
        allen_velo_tracks["v1keyed"],
        Velotracks,
        with_ut=False)

    return Reconstruction('hlt1', checker)


options.histo_file = "HLT1HLT2Checker_fitted_profile.root"
with (allen_gaudi_config.bind(sequence="hlt1_pp_no_ut"),
      default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
      make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering)):
    run_allen_reconstruction(options, reco)

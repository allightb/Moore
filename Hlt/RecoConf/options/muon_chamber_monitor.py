###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV
import Functors as F
from PyConf.Algorithms import MuonChamberMonitor
from RecoConf.reconstruction_objects import make_pvs, reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.standard_particles import make_long_muons, make_ismuon_long_muon
from Hlt2Conf.algorithms_thor import ParticleFilter
from Moore import options
from Moore.config import Reconstruction, run_reconstruction
from RecoConf.muonid import make_muon_hits


def muonchamber_mon():
    pvs = make_pvs()

    mus_tag = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(F.P > 3 * GeV, F.PT > 1.2 * GeV,
                          F.MINIPCHI2(pvs) > 9)))
    mus_probe = ParticleFilter(
        make_long_muons(),
        F.FILTER(
            F.require_all(F.INMUON, F.P > 3 * GeV,
                          F.MINIPCHI2(pvs) > 20)))

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    hits = make_muon_hits()

    muon_chamber_mon_mums_tagged = MuonChamberMonitor(
        Probes=mups_probe,
        Tags=mums_tag,
        MuonHits=hits,
        name="MuonChamberMonitor_MumTagged")
    muon_chamber_mon_mups_tagged = MuonChamberMonitor(
        Probes=mums_probe,
        Tags=mups_tag,
        MuonHits=hits,
        name="MuonChamberMonitor_MupTagged")

    return Reconstruction(
        'muonchamber_mon',
        [muon_chamber_mon_mums_tagged, muon_chamber_mon_mups_tagged])


options.histo_file = 'histo.root'

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    run_reconstruction(options, muonchamber_mon, public_tools)

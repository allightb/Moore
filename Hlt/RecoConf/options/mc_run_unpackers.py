###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Simple testing options file that runs all of the unpacking algorithms
defined in mc_unpackers().
"""

from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.data_from_file import mc_unpackers

options.evt_max = 5


def test():
    return Reconstruction('Test_MC_Unpackers', list(mc_unpackers().values()))


run_reconstruction(options, test)

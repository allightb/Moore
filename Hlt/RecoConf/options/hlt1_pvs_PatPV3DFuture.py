###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from RecoConf.legacy_rec_hlt1_tracking import (all_velo_track_types,
                                               make_PatPV3DFuture_pvs)
from Moore.config import Reconstruction
from DDDB.CheckDD4Hep import UseDD4Hep
from RecoConf.mc_checking import get_pv_checkers

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = ['/world', 'VP']


def run_PatPV3DFuture_reco_pv():
    velo_tracks = all_velo_track_types()
    pvs = make_PatPV3DFuture_pvs(velo_tracks, None, True, False, False)
    pvs_3dseeding = make_PatPV3DFuture_pvs(velo_tracks, None, True, False,
                                           True)

    data = []
    data += get_pv_checkers(pvs['v3'], velo_tracks, False)
    data += get_pv_checkers(pvs_3dseeding['v3'], velo_tracks, False)

    return Reconstruction('hlt1_pvs_PatPV3DFuture', data)


run_reconstruction(options, run_PatPV3DFuture_reco_pv)

###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.data_from_file import mc_unpackers
from RecoConf.legacy_rec_hlt1_tracking import make_VeloClusterTrackingSIMD_hits
from RecoConf.hlt2_tracking import (
    make_hlt2_tracks, make_PrKalmanFilter_tracks, make_PrStorePrUTHits_hits,
    make_PrStoreSciFiHits_hits)
from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system
from PyConf.reading import get_mc_track_info
from PyConf.Algorithms import (PrTrackAssociator, PrDebugTrackingLosses)

input_files_Bd2KstEE = [
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000147_1.xdigi",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000303_1.xdigi",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000037_1.xdigi",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000032_1.xdigi",
    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000268_1.xdigi",
    "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000264_1.xdigi",
    "root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00151655/0000/00151655_00000076_1.xdigi",
]

options.input_files = input_files_Bd2KstEE
options.conddb_tag = "sim-20210617-vc-mu100"
options.dddb_tag = "dddb-20210617"
options.simulation = True
options.input_type = 'ROOT'

options.ntuple_file = 'tracking_losses_ntuple_Bd2KstEE.root'
options.evt_max = 1000


# run with
# ./Moore/run gaudirun.py  Moore/Hlt/RecoConf/options/tracking_developments/run_pr_tracking_losses.py
# PrDebugTrackingLosses is tested by mc_matching_example.py
def run_tracking_losses():

    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()
    hlt2_tracks = make_hlt2_tracks(light_reco=True, use_pr_kf=True)
    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ut_hits = make_PrStorePrUTHits_hits()
    ft_hits = make_PrStoreSciFiHits_hits()
    fitted_forward_tracks = make_PrKalmanFilter_tracks(
        input_tracks=hlt2_tracks["Forward"]["Pr"],
        hits_vp=vp_hits,
        hits_ut=ut_hits,
        hits_ft=ft_hits)

    # add MCLinking to the (fitted) V1 tracks
    links_to_velo_tracks = PrTrackAssociator(
        SingleContainer=hlt2_tracks["Velo"]["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    links_to_long_tracks = PrTrackAssociator(
        SingleContainer=hlt2_tracks["Forward"]["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    with PrTrackAssociator.bind(FractionOK=0.5):
        loose_links_to_long_tracks = PrTrackAssociator(
            SingleContainer=hlt2_tracks["Forward"]["v1"],
            LinkerLocationID=links_to_hits,
            MCParticleLocation=mc_unpackers()["MCParticles"],
            MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    links_to_fitted_tracks = PrTrackAssociator(
        SingleContainer=fitted_forward_tracks,
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    tracking_losses = PrDebugTrackingLosses(
        name="PrDebugTrackingLosses",
        TrackType="Long",
        StudyTracks=hlt2_tracks["Forward"]["v1"],
        VeloTracks=hlt2_tracks["Velo"]["v1"],
        MCParticles=mc_unpackers()["MCParticles"],
        MCVPHits=mc_unpackers()["MCVPHits"],
        MCUTHits=mc_unpackers()["MCUTHits"],
        MCFTHits=mc_unpackers()["MCFTHits"],
        VeloTrackLinks=links_to_velo_tracks,
        TrackLinks=links_to_long_tracks,
        LooseTrackLinks=loose_links_to_long_tracks,
        FittedTrackLinks=links_to_fitted_tracks,
        TrackInfo=get_mc_track_info(),
    )

    data = [tracking_losses]
    return Reconstruction('run_tracking_losses', data)


run_reconstruction(options, run_tracking_losses)

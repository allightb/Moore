###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from PyConf.application import make_odin
from Moore.config import Reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.legacy_rec_hlt1_tracking import (
    make_reco_pvs, make_PatPV3DFuture_pvs, make_RetinaClusters)

from PyConf.application import default_raw_banks
from RecoConf.hlt2_tracking import (TrackBestTrackCreator,
                                    make_PrStoreUTHit_empty_hits,
                                    get_global_measurement_provider)
from PyConf.Algorithms import (
    VPHitEfficiencyMonitor,
    VeloRetinaClusterTrackingSIMDFull,
    fromPrVeloTracksV1TracksMerger,
    TrackEventFitter,
    TrackListRefiner,
    TrackSelectionToContainer,
)
from PyConf.Tools import (
    TrackMasterFitter,
    TrackMasterExtrapolator,
    SimplifiedMaterialLocator,
    TrackInterpolator,
    TrackLinearExtrapolator,
)

import Functors as F

make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)
"""
Options file to determine the hit efficiencies in the VELO

In the VPHitEfficiencyMonitor, the hit efficiency of sensors is determined and stored in an output ROOT file. Note: This is done independantly fromt the tracking efficiency.
The VPHitEfficiencyMonitor will produce the hit efficiency per sensor. To cover the entire VELO, a loop over sensor_under_study is needed. To make sure the hit efficiencies 
are not biassed, the sensor_under_study needs to be masked in tracking. Therefore, contrary to the options in VP_tracking_monitor.py, the tracking needs to be repeated for each sensor. 
The tracks are fitted using the TrackMasterFitter and all extrapolators used are LinearExtrapolators. This includes the extrapolator used in the TrackInterpolator.
As input for the VPHitEfficiencyMonitor, data is intended. To have 1 entry in all sensors requires on O(10^3) events at avg_mu=2.2. A more detailed map of the hit efficiencies,
for instance to use in Boole, needs an order of events of ~O(10^10). (assuming 16x16 pixel bins)
"""


def make_my_sequence():
    odin = make_odin()
    data = [odin]

    with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
        TrackMasterFitter.bind(FastMaterialApproximation=True),  \
        get_global_measurement_provider.bind(ignoreUT=True, velo_hits=make_RetinaClusters, ignoreFT=True, ignoreMuon=True, ut_hits=make_PrStoreUTHit_empty_hits):

        trackExtrapolator = TrackLinearExtrapolator()
        trackInterpolator = TrackInterpolator(Extrapolator=trackExtrapolator)

        for sensor_under_study in [20, 21, 22, 23, 96, 97, 98, 99, 100]:

            my_SensorMask = [
                sensor in [sensor_under_study] for sensor in range(208)
            ]

            vpClustering = VeloRetinaClusterTrackingSIMDFull(
                RawBanks=default_raw_banks("VPRetinaCluster"),
                SensorMasks=tuple(my_SensorMask),
                MaxScatterSeeding=0.1,
                MaxScatterForwarding=0.1,
                MaxScatter3hits=0.02,
                SkipForward=3)
            clusters = vpClustering.HitsLocation
            vpTracks = vpClustering.TracksLocation
            vpTracks_backwards = vpClustering.TracksBackwardLocation

            vpTracks_v1 = fromPrVeloTracksV1TracksMerger(  # converts Pr -> v1 tracks and merges forward/backward
                InputTracksLocation1=vpTracks,
                InputTracksLocation2=vpTracks_backwards).OutputTracksLocation

            my_TrackMasterFitter = TrackMasterFitter(
                MeasProvider=get_global_measurement_provider(),
                MaterialLocator=SimplifiedMaterialLocator(),
                Extrapolator=TrackMasterExtrapolator(
                    MaterialLocator=SimplifiedMaterialLocator()),
                MaxNumberOutliers=2,
                NumberFitIterations=10,
                MaxUpdateTransports=10)

            fittedTracks = TrackEventFitter(
                TracksInContainer=vpTracks_v1,
                Fitter=(my_TrackMasterFitter),
                MaxChi2DoF=2.8,
                name="TrackEventFitter_{hash}").TracksOutContainer

            bestTracks = TrackBestTrackCreator(
                name="TrackBestTrackCreator_{hash}",
                TracksInContainers=[fittedTracks],
                DoNotRefit=True,
                AddGhostProb=False,
                FitTracks=False,
                MaxChi2DoF=2.8,
            ).TracksOutContainer

            # do track selection
            tracks_selection = TrackListRefiner(
                inputLocation=bestTracks,
                Code=F.require_all(
                    F.NVPHITS >= 3,
                    F.P >= 0,  # Here to make it explicit
                    F.PT >= 0)).outputLocation
            filtered_tracks = TrackSelectionToContainer(
                name="TrackSelectionToContainer_{hash}",
                InputLocation=tracks_selection).OutputLocation

            my_vp_efficiency_alg_TMF = VPHitEfficiencyMonitor(
                name="VPHitEfficiencyMonitorSensor{0}".format(
                    sensor_under_study),
                TrackLocation=filtered_tracks,
                PrVPHitsLocation=clusters,
                MaxTrackCov=100.0,
                SensorUnderStudy=sensor_under_study,
                Interpolator=trackInterpolator,
                Extrapolator=trackExtrapolator,
            )

            data += [my_vp_efficiency_alg_TMF]

    return Reconstruction('hlt2_hit_eff_reco', data, [])


run_reconstruction(options, make_my_sequence)

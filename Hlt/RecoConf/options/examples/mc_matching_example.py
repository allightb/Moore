###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import m, mm
from PyConf.Algorithms import (PrTrackAssociator, PrMatchNN,
                               PrForwardTrackingVelo, PrParameterisationData,
                               PrDebugTrackingLosses)
from PyConf.Tools import (PrMCDebugMatchToolNN, PrMCDebugForwardTool,
                          PrDebugTrackingTool)
from PyConf.reading import get_mc_track_info
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.legacy_rec_hlt1_tracking import make_legacy_rec_hlt1_tracks

from RecoConf.hlt2_tracking import (
    make_seeding_tracks, get_global_ut_hits_tool,
    make_PrKalmanFilter_Seed_tracks, make_PrStoreSciFiHits_hits)
from RecoConf.mc_checking import (
    check_tracking_efficiency, make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles, make_default_IdealStateCreator)
from RecoConf.data_from_file import mc_unpackers
from RecoConf.mc_checking_categories import (get_mc_categories,
                                             get_hit_type_mask)


def mc_matching():
    track_type = "Forward"
    hlt1_tracks = make_legacy_rec_hlt1_tracks()
    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=hlt1_tracks[track_type], LinksToLHCbIDs=links_to_hits)
    pr_checker = check_tracking_efficiency(
        TrackType=track_type,
        InputTracks=hlt1_tracks[track_type],
        LinksToTracks=links_to_tracks,
        LinksToLHCbIDs=links_to_hits,
        MCCategories=get_mc_categories(track_type),
        HitTypesToCheck=get_hit_type_mask(track_type),
    )

    seed_tracks = make_seeding_tracks()

    # add MCLinking to the (fitted) V1 tracks
    links_to_velo_tracks = PrTrackAssociator(
        SingleContainer=hlt1_tracks["Velo"]["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    links_to_seed_tracks = PrTrackAssociator(
        SingleContainer=seed_tracks["v1"],
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    with PrTrackAssociator.bind(FractionOK=0.5):
        loose_links_to_long_tracks = PrTrackAssociator(
            SingleContainer=seed_tracks["v1"],
            LinkerLocationID=links_to_hits,
            MCParticleLocation=mc_unpackers()["MCParticles"],
            MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    match_debug = PrMatchNN(
        VeloInput=hlt1_tracks["Velo"]["Pr"],
        SeedInput=seed_tracks["Pr"],
        MatchDebugToolName=PrMCDebugMatchToolNN(
            VeloTracks=hlt1_tracks["Velo"]["v1"],
            SeedTracks=seed_tracks["v1"],
            VeloTrackLinks=links_to_velo_tracks,
            SeedTrackLinks=links_to_seed_tracks,
            TrackInfo=get_mc_track_info(),
            MCParticles=mc_unpackers()["MCParticles"]),
        AddUTHitsToolName=get_global_ut_hits_tool(enable=True),
    ).MatchOutput

    forward_mc_debug = PrForwardTrackingVelo(
        InputTracks=hlt1_tracks["Velo"]["Pr"],
        SciFiHits=make_PrStoreSciFiHits_hits(),
        AddUTHitsToolName=get_global_ut_hits_tool(enable=True),
        DebugTool=PrMCDebugForwardTool(
            InputTracks=hlt1_tracks["Velo"]["v1"],
            InputTrackLinks=links_to_velo_tracks,
            MCParticles=mc_unpackers()["MCParticles"],
            SciFiHitLinks=links_to_hits,
            SciFiHits=make_PrStoreSciFiHits_hits(),
            TrackInfo=get_mc_track_info(),
        ))

    forward_debug = PrForwardTrackingVelo(
        InputTracks=hlt1_tracks["Velo"]["Pr"],
        SciFiHits=make_PrStoreSciFiHits_hits(),
        AddUTHitsToolName=get_global_ut_hits_tool(enable=True),
        DebugTool=PrDebugTrackingTool())

    param_data = PrParameterisationData(
        MCParticles=mc_unpackers()["MCParticles"],
        MCVPHits=mc_unpackers()["MCVPHits"],
        MCFTHits=mc_unpackers()["MCFTHits"],
        TrackInfo=get_mc_track_info(),
    )

    fitted_seed_tracks = make_PrKalmanFilter_Seed_tracks(
        input_tracks=seed_tracks["Pr"], hits_ft=make_PrStoreSciFiHits_hits())

    links_to_fitted_tracks = PrTrackAssociator(
        SingleContainer=fitted_seed_tracks,
        LinkerLocationID=links_to_hits,
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesInput=mc_unpackers()["MCVertices"]).OutputLocation

    tracking_losses = PrDebugTrackingLosses(
        TrackType="Ttrack",
        RecAs="fromB",
        StudyTracks=seed_tracks["v1"],
        VeloTracks=hlt1_tracks["Velo"]["v1"],
        MCParticles=mc_unpackers()["MCParticles"],
        MCVPHits=mc_unpackers()["MCVPHits"],
        MCUTHits=mc_unpackers()["MCUTHits"],
        MCFTHits=mc_unpackers()["MCFTHits"],
        VeloTrackLinks=links_to_velo_tracks,
        TrackLinks=links_to_seed_tracks,
        LooseTrackLinks=loose_links_to_long_tracks,
        FittedTrackLinks=links_to_fitted_tracks,
        IdealStateCreator=make_default_IdealStateCreator(),
        IdealStatesZ=[9410 * mm],
        RadLengthBetweenZ=(7 * m, 10 * m),
        TrackInfo=get_mc_track_info(),
    )

    data = [
        match_debug, forward_debug, forward_mc_debug, param_data,
        tracking_losses
    ]

    return Reconstruction('mc_matching', [pr_checker] + data, [require_gec()])


options.histo_file = "mc_matching_example_histo.root"
options.ntuple_file = "mc_matching_example_ntuple.root"
run_reconstruction(options, mc_matching)

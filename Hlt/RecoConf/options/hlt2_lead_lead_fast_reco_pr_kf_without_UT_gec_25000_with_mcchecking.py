###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_reconstruction
from RecoConf.legacy_rec_hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks,
    get_UpgradeGhostId_tool_no_UT,
)

from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import check_track_resolution

from PyConf.Algorithms import PrHybridSeeding
from PyConf.packing import persistreco_writing_version

options.histo_file = 'hlt2_lead_lead_track_monitoring_with_mc_histos.root'
options.ntuple_file = 'hlt2_lead_lead_track_monitoring_with_mc_ntuples.root'

from RecoConf.standalone import standalone_hlt2_light_reco_without_UT


with standalone_hlt2_light_reco_without_UT.bind(do_mc_checking=True, do_data_monitoring=False, use_pr_kf=True, fast_reco=True),\
    check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True),\
    require_gec.bind(cut=30_000, skipUT=True),\
    PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
    get_UpgradeGhostId_tool_no_UT.bind(for_PbPb=True),\
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.7),\
    make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
    persistreco_writing_version.bind(version=1.1):
    config = run_reconstruction(options, standalone_hlt2_light_reco_without_UT)

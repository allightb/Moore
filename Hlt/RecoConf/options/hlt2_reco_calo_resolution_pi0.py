###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options, run_reconstruction
from RecoConf.standalone import standalone_hlt2_calo_resolution_pi0

options.evt_max = 1000

isQMTTest = 'QMTTEST_NAME' in os.environ
myName = (os.environ["QMTTEST_NAME"]
          if isQMTTest else "dst_input_and_conds_calo_res_pi0")

options.ntuple_file = 'ntuple_hlt2_reco_calo_resolution_pi0.root'

run_reconstruction(options, standalone_hlt2_calo_resolution_pi0)

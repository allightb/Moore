###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/env python
"""Compare clustering performarces for CPU and FPGA clustering algorithms.
The efficiency is calculated using TEfficiency with TEfficiency::kFWilson method.
"""

import os, sys
import argparse
import math
from ROOT import (TH1F, TCanvas, TFile, kRed, kGreen, kBlue, kBlack, kAzure,
                  kGray, kOrange, kMagenta, kCyan, kTRUE, gPad, gROOT,
                  TEfficiency, TLegend)

from PyConf.components import unique_name_ext_re, findRootObjByName

gROOT.SetBatch(kTRUE)


def getEfficiencyHistoNames():
    return ["eta", "phi", "r", "r<7mm", "p", "pt", "z", "module"]


def getCuts():
    return ["", " Velo reco"]


def getResidualHistoNames():
    return ["x", "y"]


def getResidualCuts():
    return [
        "", " - VELO reco", " - sensor edges", " - long pixels", " - long reco"
    ]


def getDecays():
    return [""]  #, " - phikk", " - gammaee"]


def get_colors():
    return [kBlack, kRed, kAzure, kOrange, kMagenta + 2, kGreen + 3, kCyan + 2]


def get_markers():
    return [20, 24, 21, 22, 23, 25]


def get_fillstyles():
    return [3354, 3004, 3003, 3325, 3144, 3244, 3444]


def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--filename',
        type=str,
        nargs='+',
        default=['./histo_CPU.root', './histo_FPGA.root'],
        help='input files to compare, including path')
    parser.add_argument(
        '--label',
        nargs='+',
        type=str,
        default=['CPU', 'FPGA'],
        help='label of input files')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    parser.add_argument(
        '--dist',
        default=False,
        action='store_true',
        help='draw histogram of all files')
    parser.add_argument(
        '--outfile',
        type=str,
        default='cluster_efficiency_plots.root',
        help='output file')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    return parser


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def get_eff(eff, hist, tf, histoName_num, histoName_den, label, var,
            plotstyle):
    eff = {}
    hist = {}
    empty_histos = False
    for i, lab in enumerate(label):
        try:
            numerator = findRootObjByName(tf[lab], histoName_num)
        except Exception:
            numerator = None
        try:
            denominator = findRootObjByName(tf[lab], histoName_den)
        except Exception:
            denominator = None
        if (not denominator or denominator.GetEntries() == 0):
            empty_histos = True
            print("empty", lab, histoName_num, histoName_den)
            continue
        if (not numerator):
            numerator = TH1F(
                '', '', denominator.GetNbinsX(), denominator.GetBinLowEdge(1),
                denominator.GetBinLowEdge(denominator.GetNbinsX() + 1))
        numerator.Sumw2()
        denominator.Sumw2()

        teff = TEfficiency(numerator, denominator)
        teff.SetStatisticOption(TEfficiency.kFWilson)
        eff[lab] = teff.CreateGraph()
        eff[lab].SetName(lab)
        title = lab

        sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
        from ConfigPrCheckerEfficiencyHistos import efficiencyHistoDict
        xtitle = efficiencyHistoDict()[var]["xTitle"]
        ytitle = "Inefficiency"
        title = "{};{};{}".format(title, xtitle, ytitle)
        eff[lab].SetTitle(title)

        hist[lab] = denominator.Clone()
        hist[lab].Scale(1 / hist[lab].Integral())
        hist[lab].SetName("h_denominator")
        hist[lab].SetTitle("reconstructible")

    return empty_histos, eff, hist


def PrCheckerEfficiency(filename, outfile, label, plotstyle, dist, savepdf):
    sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
    from LHCbStyle import setLHCbStyle, set_style
    from ComparisonUtils import draw_sample
    from ConfigPrCheckerEfficiencyHistos import (efficiencyHistoDict,
                                                 LabelsLocation)
    setLHCbStyle()

    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()

    tf = {}
    tf = get_files(tf, filename, label)
    outputfile = TFile(outfile, "recreate")

    efficiencyHistos = getEfficiencyHistoNames()
    cuts = getCuts()

    # number of clusters - stats
    print("Stats: number of clusters")
    for i, lab in enumerate(label):
        histo_num = findRootObjByName(
            tf[lab],
            "VPClusterEfficiency" + unique_name_ext_re() + "/# clusters")
        num_clusters = 0
        num_events = histo_num.GetEntries()
        for i in range(histo_num.GetNbinsX()):
            num_clusters += histo_num.GetBinLowEdge(
                i) * histo_num.GetBinContent(i)
        print(lab, ":", num_clusters, "clusters")

    # efficiency - stats
    print("Stats: integrated efficiency")
    for i, lab in enumerate(label):
        histo_num = findRootObjByName(
            tf[lab], "VPClusterEfficiency" + unique_name_ext_re() +
            "/# MCHits not found - phi")
        histo_den = findRootObjByName(
            tf[lab], "VPClusterEfficiency" + unique_name_ext_re() +
            "/# MCHits reconstructible - phi")
        hits_total = histo_den.GetEntries()
        hits_found = hits_total - histo_num.GetEntries()
        print("Clustering efficiency {} = {:.3f} +/- {:.3f}".format(
            lab, hits_found / hits_total * 100,
            math.sqrt((hits_found + 1) * (hits_found + 2) / (hits_total + 2) /
                      (hits_total + 3) - (hits_found + 1) * (hits_found + 1) /
                      (hits_total + 2) / (hits_total + 2))))

    print("Stats: integrated efficiency - VELO reco")
    for i, lab in enumerate(label):
        histo_num = findRootObjByName(
            tf[lab], "VPClusterEfficiency" + unique_name_ext_re() +
            "/# MCHits not found Velo reco - phi")
        histo_den = findRootObjByName(
            tf[lab], "VPClusterEfficiency" + unique_name_ext_re() +
            "/# MCHits reconstructible Velo reco - phi")
        hits_total = histo_den.GetEntries()
        hits_found = hits_total - histo_num.GetEntries()
        print("Clustering efficiency {} = {:.3f} +/- {:.3f}".format(
            lab, hits_found / hits_total * 100,
            math.sqrt((hits_found + 1) * (hits_found + 2) / (hits_total + 2) /
                      (hits_total + 3) - (hits_found + 1) * (hits_found + 1) /
                      (hits_total + 2) / (hits_total + 2))))

    # calculate inefficiency
    for var in efficiencyHistos:
        for cut in cuts:
            canvastitle = "inefficiency_" + cut.replace(" ", "") + "_" + var

            # get inefficiency
            histoName_num = "VPClusterEfficiency" + unique_name_ext_re(
            ) + "/# MCHits not found" + cut + " - " + var
            histoName_den = "VPClusterEfficiency" + unique_name_ext_re(
            ) + "/# MCHits reconstructible" + cut + " - " + var
            eff = {}
            hist = {}
            empty_histos, eff, hist = get_eff(eff, hist, tf, histoName_num,
                                              histoName_den, label, var,
                                              plotstyle)
            if empty_histos:
                continue
            name = "inefficiency_" + cut.replace(" ", "") + "_" + var
            canvas = TCanvas(name, canvastitle)
            ymax = []
            for i, lab in enumerate(label):
                eff[lab].Draw("{}".format("AP" if i == 0 else "P SAME"))
                set_style(eff[lab], colors[i], markers[i], styles[i])
                ymax.append(max(eff[lab].GetY()))
            gPad.Update()
            graph = eff[label[0]]
            graph.GetYaxis().SetTitleOffset(1)
            graph.SetMinimum(0.)
            graph.SetMaximum(max(ymax) * 1.1)

            if dist:
                for i, lab in enumerate(label):
                    hist[lab].Scale(max(ymax) / hist[lab].GetMaximum() / 1.05)
                    if i == 0:
                        set_style(hist[lab], kGray, markers[i], styles[i])
                    else:
                        set_style(hist[lab], colors[i] - 10, markers[i],
                                  styles[i])
                    hist[lab].Draw("hist SAME")
            else:
                set_style(hist[label[0]], kBlue - 10, markers[i], styles[i])
                hist[label[0]].Scale(
                    max(ymax) / hist[label[0]].GetMaximum() / 1.05)
                hist[label[0]].Draw("hist SAME")

            effs = [eff[lab] for lab in label]
            hists = [hist[lab] for lab in label] if dist else [hist[label[0]]]

            labels_location = LabelsLocation()

            x, y = labels_location.x_legend, labels_location.y_legend
            legend = TLegend(x, y, x + 0.3,
                             y + (len(effs) + len(hists)) * 0.05)
            legend.SetTextSize(0.048)
            legend.SetFillStyle(0)
            for eff in effs:
                legend.AddEntry(eff, eff.GetTitle(), "lpe")
            for hist in hists:
                legend.AddEntry(hist, hist.GetTitle(), "f")
            legend.Draw()

            draw_sample(canvas,
                        labels_location.x_label, labels_location.y_label, 0.06,
                        cut.replace(" ", ""), num_events)
            canvasName = "inefficiency_" + cut.replace(" ",
                                                       "") + "_" + var + ".pdf"
            if savepdf:
                canvas.SaveAs(canvasName)
            canvas.SetRightMargin(0.05)
            canvas.Write()

    # plot residuals
    residualHistos = getResidualHistoNames()
    residualCuts = getResidualCuts()
    for var in residualHistos:
        for cut in residualCuts:
            baseResidualName = ("VPClusterEfficiency" + unique_name_ext_re() +
                                "/Residuals along " + var + cut + " \\[mm\\]")
            canvastitle = "residual_" + var + cut
            name = "residual_" + var + cut
            canvas = TCanvas(name, canvastitle)

            labels_location = LabelsLocation()
            x, y = labels_location.x_legend, labels_location.y_legend - 0.15
            legend = TLegend(x, y, x + 0.3, y + 0.3)
            legend.SetTextSize(0.048)
            legend.SetFillStyle(0)

            for i, lab in enumerate(label):
                histo_res = findRootObjByName(tf[lab], baseResidualName)
                histo_res.Scale(1.0 / histo_res.Integral())
                histo_res.GetXaxis().SetTitle(var + " cluster - " + var +
                                              " MC hit [mm]")
                histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
                set_style(histo_res, colors[i], markers[i], styles[i])
                legend.AddEntry(histo_res, lab, "f")

            legend.Draw()
            draw_sample(canvas, labels_location.x_label,
                        labels_location.y_label, 0.06, cut, num_events)
            gPad.Update()

            if savepdf:
                canvas.SaveAs(canvastitle + ".pdf")
            canvas.SetRightMargin(0.05)
            canvas.Write()

    # plot residuals per module
    residualHistos = getResidualHistoNames()
    for var in residualHistos:
        for module in range(52):
            baseResidualName = "VPClusterEfficiency" + unique_name_ext_re(
            ) + "/Residuals along " + var + " \\[mm\\] - module" + str(module)
            canvastitle = "residual_" + var + "_module" + str(module)
            name = "residual_" + var + "_module" + str(module)
            canvas = TCanvas(name, canvastitle)

            labels_location = LabelsLocation()
            x, y = labels_location.x_legend, labels_location.y_legend - 0.15
            legend = TLegend(x, y, x + 0.3, y + 0.3)
            legend.SetTextSize(0.048)
            legend.SetFillStyle(0)

            for i, lab in enumerate(label):
                histo_res = findRootObjByName(tf[lab], baseResidualName)
                histo_res.Scale(1.0 / histo_res.Integral())
                histo_res.GetXaxis().SetTitle(var + " cluster - " + var +
                                              " MC hit [mm]")
                histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
                set_style(histo_res, colors[i], markers[i], styles[i])
                legend.AddEntry(histo_res, lab, "f")

            legend.Draw()
            draw_sample(canvas, labels_location.x_label,
                        labels_location.y_label, 0.06, "module " + str(module),
                        num_events)
            gPad.Update()

            if savepdf:
                canvas.SaveAs(canvastitle + ".pdf")
            canvas.SetRightMargin(0.05)
            canvas.Write()

    # number of clusters vs r / vs module
    residualHistos = getResidualHistoNames()
    residualCuts = getResidualCuts()
    for var in ["r", "module"]:
        for cut in residualCuts:
            baseResidualName = "VPClusterEfficiency" + unique_name_ext_re(
            ) + "/# cluster distribution - " + var
            canvastitle = "num_cluters_vs_" + var
            name = "num_cluters_vs_" + var
            canvas = TCanvas(name, canvastitle)

            labels_location = LabelsLocation()
            x, y = labels_location.x_legend, labels_location.y_legend - 0.15
            legend = TLegend(x, y, x + 0.3, y + 0.3)
            legend.SetTextSize(0.048)
            legend.SetFillStyle(0)

            for i, lab in enumerate(label):
                histo_res = findRootObjByName(tf[lab], baseResidualName)
                histo_res.Scale(1.0 / histo_res.Integral())
                histo_res.SetMinimum(0)
                histo_res.GetXaxis().SetTitle(
                    efficiencyHistoDict()[var]["xTitle"])
                histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
                set_style(histo_res, colors[i], markers[i], styles[i])
                legend.AddEntry(histo_res, lab, "f")

            legend.Draw()
            draw_sample(canvas, labels_location.x_label,
                        labels_location.y_label, 0.06, "", num_events)
            gPad.Update()

            if savepdf:
                canvas.SaveAs(canvastitle + ".pdf")
            canvas.SetRightMargin(0.05)
            canvas.Write()

    # number of clusters vs r per module
    for module in range(52):
        baseResidualName = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/# cluster distribution - r - module" + str(module)
        canvastitle = "num_clusters_vs_r" + "_module" + str(module)
        name = "num_clusters_vs_r" + "_module" + str(module)
        canvas = TCanvas(name, canvastitle)

        labels_location = LabelsLocation()
        x, y = labels_location.x_legend, labels_location.y_legend - 0.15
        legend = TLegend(x, y, x + 0.3, y + 0.3)
        legend.SetTextSize(0.048)
        legend.SetFillStyle(0)

        for i, lab in enumerate(label):
            histo_res = findRootObjByName(tf[lab], baseResidualName)
            histo_res.Scale(1.0 / histo_res.Integral())
            histo_res.GetXaxis().SetTitle("r [mm]")
            histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
            set_style(histo_res, colors[i], markers[i], styles[i])
            legend.AddEntry(histo_res, lab, "f")

        legend.Draw()
        draw_sample(canvas, labels_location.x_label, labels_location.y_label,
                    0.06, "module " + str(module), num_events)
        gPad.Update()

        if savepdf:
            canvas.SaveAs(canvastitle + ".pdf")
        canvas.SetRightMargin(0.05)
        canvas.Write()

    # cluster dimensions
    baseResidualName = "VPClusterEfficiency" + unique_name_ext_re(
    ) + "/# pixel per cluster distribution"
    canvastitle = "pixel_per_cluster"
    name = "pixel_per_cluster"
    canvas = TCanvas(name, canvastitle)

    labels_location = LabelsLocation()
    x, y = labels_location.x_legend, labels_location.y_legend - 0.15
    legend = TLegend(x, y, x + 0.3, y + 0.3)
    legend.SetTextSize(0.048)
    legend.SetFillStyle(0)

    for i, lab in enumerate(label):
        histo_res = findRootObjByName(tf[lab], baseResidualName)
        histo_res.Scale(1.0 / histo_res.Integral())
        histo_res.GetXaxis().SetTitle("# pixel / cluster")
        histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
        set_style(histo_res, colors[i], markers[i], styles[i])
        legend.AddEntry(histo_res, lab, "f")

    legend.Draw()
    draw_sample(canvas, labels_location.x_label, labels_location.y_label, 0.06,
                "", num_events)
    gPad.Update()

    if savepdf:
        canvas.SaveAs(canvastitle + ".pdf")
    canvas.SetRightMargin(0.05)
    canvas.Write()

    # cluster dimensions per module
    for module in range(52):
        baseResidualName = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/# pixel per cluster distribution - module" + str(module)
        canvastitle = "pixel_per_cluster_module" + str(module)
        name = "pixel_per_cluster_module" + str(module)
        canvas = TCanvas(name, canvastitle)

        labels_location = LabelsLocation()
        x, y = labels_location.x_legend, labels_location.y_legend - 0.15
        legend = TLegend(x, y, x + 0.3, y + 0.3)
        legend.SetTextSize(0.048)
        legend.SetFillStyle(0)

        for i, lab in enumerate(label):
            histo_res = findRootObjByName(tf[lab], baseResidualName)
            histo_res.Scale(1.0 / histo_res.Integral())
            histo_res.GetXaxis().SetTitle("# pixel / cluster")
            histo_res.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
            set_style(histo_res, colors[i], markers[i], styles[i])
            legend.AddEntry(histo_res, lab, "f")

        legend.Draw()
        draw_sample(canvas, labels_location.x_label, labels_location.y_label,
                    0.06, "module " + str(module), num_events)
        gPad.Update()

        if savepdf:
            canvas.SaveAs(canvastitle + ".pdf")
        canvas.SetRightMargin(0.05)
        canvas.Write()

    # purity plots
    basePurityName = "VPClusterEfficiency" + unique_name_ext_re(
    ) + "/Purity distribution"
    canvastitle = "purity_distribution"
    name = "purity_distribution"
    canvas = TCanvas(name, canvastitle)
    canvas.SetLogy()

    labels_location = LabelsLocation()
    x, y = labels_location.x_legend, labels_location.y_legend - 0.15
    legend = TLegend(x, y, x + 0.3, y + 0.3)
    legend.SetTextSize(0.048)
    legend.SetFillStyle(0)

    for i, lab in enumerate(label):
        histo_pur = findRootObjByName(tf[lab], basePurityName)
        histo_pur.Scale(1.0 / histo_pur.Integral())
        histo_pur.GetXaxis().SetTitle("purity")
        histo_pur.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
        set_style(histo_pur, colors[i], markers[i], styles[i])
        legend.AddEntry(histo_pur, lab, "f")

    legend.Draw()
    draw_sample(canvas, labels_location.x_label, labels_location.y_label, 0.06,
                "", num_events)
    gPad.Update()

    if savepdf:
        canvas.SaveAs(canvastitle + ".pdf")
    canvas.SetRightMargin(0.05)
    canvas.Write()

    basePurityPixelName = "VPClusterEfficiency" + unique_name_ext_re(
    ) + "/Purity vs # pixels"
    basePixelName = "VPClusterEfficiency" + unique_name_ext_re(
    ) + "/# pixel distribution"
    canvastitle = "purity_pixels"
    name = "purity_pixels"
    canvas = TCanvas(name, canvastitle)

    labels_location = LabelsLocation()
    x, y = labels_location.x_legend, labels_location.y_legend - 0.15
    legend = TLegend(x, y, x + 0.3, y + 0.3)
    legend.SetTextSize(0.048)
    legend.SetFillStyle(0)

    for i, lab in enumerate(label):
        histo_pur_pix = findRootObjByName(tf[lab], basePurityPixelName)
        histo_pur_pix.GetXaxis().SetTitle("# pixels")
        histo_pur_pix.GetYaxis().SetTitle("purity")
        histo_pur_pix.GetYaxis().SetRangeUser(0.0001, 1.05)
        histo_pur_pix.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
        set_style(histo_pur_pix, colors[i], markers[i], styles[i])
        legend.AddEntry(histo_pur_pix, lab, "f")
        if i == 0:
            histo_pix = findRootObjByName(tf[lab], basePixelName)
            histo_pix.Scale(1.0 / histo_pix.Integral())
            set_style(histo_pix, kGray, markers[i], styles[i])
            histo_pix.Draw("HIST SAME")
            legend.AddEntry(histo_pix, "# pixels per MCHit", "f")

    legend.Draw()
    draw_sample(canvas, labels_location.x_label, labels_location.y_label, 0.06,
                "", num_events)
    gPad.Update()

    if savepdf:
        canvas.SaveAs(canvastitle + ".pdf")
    canvas.SetRightMargin(0.05)
    canvas.Write()

    # clusters associated to hit
    baseAssociatedName = "VPClusterEfficiency" + unique_name_ext_re(
    ) + "/# associated cluster to hit"
    canvastitle = "associated_distribution"
    name = "associated_distribution"
    canvas = TCanvas(name, canvastitle)
    canvas.SetLogy()

    labels_location = LabelsLocation()
    x, y = labels_location.x_legend, labels_location.y_legend - 0.15
    legend = TLegend(x, y, x + 0.3, y + 0.3)
    legend.SetTextSize(0.048)
    legend.SetFillStyle(0)

    for i, lab in enumerate(label):
        histo_as = findRootObjByName(tf[lab], baseAssociatedName)
        histo_as.Scale(1.0 / histo_as.Integral())
        histo_as.GetXaxis().SetTitle("# clusters")
        histo_as
        histo_as.Draw("{}".format("HIST" if i == 0 else "HIST SAME"))
        set_style(histo_as, colors[i], markers[i], styles[i])
        legend.AddEntry(histo_as, lab, "f")

    legend.Draw()
    draw_sample(canvas, labels_location.x_label, labels_location.y_label, 0.06,
                "", num_events)
    gPad.Update()

    if savepdf:
        canvas.SaveAs(canvastitle + ".pdf")
    canvas.SetRightMargin(0.05)
    canvas.Write()

    # efficiency vs occupancy
    histo_reco_occ_tot = {}
    histo_miss_occ_tot = {}
    for i, lab in enumerate(label):
        histo_reco_occ_tot[lab] = TH1F("reco_occupancy" + lab,
                                       "reco_occupancy" + lab, 50, 0, 25)
        histo_miss_occ_tot[lab] = TH1F("miss_occupancy" + lab,
                                       "miss_occupancy" + lab, 50, 0, 25)

    for mod in range(208):
        baseOccupancyRecoName = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/Reconstructible MCHit x,y VP sensor " + str(mod)
        baseOccupancyMissName = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/Missed MCHit x,y VP sensor " + str(mod)
        canvastitle = "inefficiency_vs_occupancy-sensor" + str(mod)
        name = "inefficiency_vs_occupancy-sensor" + str(mod)
        canvas = TCanvas(name, canvastitle)

        labels_location = LabelsLocation()
        x, y = labels_location.x_legend, labels_location.y_legend
        legend = TLegend(x + 0.4, y - 0.2, x + 0.7, y - 0.5)
        legend.SetTextSize(0.048)
        legend.SetFillStyle(0)

        ineff = {}

        for i, lab in enumerate(label):
            histo_reco2D = findRootObjByName(tf[lab], baseOccupancyRecoName)
            try:
                histo_miss2D = findRootObjByName(tf[lab],
                                                 baseOccupancyMissName)
            except Exception:
                histo_miss2D = None

            if (not histo_reco2D or histo_reco2D.GetEntries() == 0):
                continue
            if (not histo_miss2D):
                histo_miss2D = histo_reco2D.Clone()
                histo_miss2D.Reset("ICESM")

            area = (histo_reco2D.GetXaxis().GetXmax() - histo_reco2D.GetXaxis(
            ).GetXmin()) * (
                histo_reco2D.GetYaxis().GetXmax() -
                histo_reco2D.GetYaxis().GetXmin()
            ) / histo_reco2D.GetNbinsX() / histo_reco2D.GetNbinsY() / 100

            histo_reco_occupancy = TH1F("reco_occupancy", "reco_occupancy", 50,
                                        0, 25)
            histo_miss_occupancy = TH1F("miss_occupancy", "miss_occupancy", 50,
                                        0, 25)

            for row in range(histo_reco2D.GetNbinsX()):
                for col in range(histo_reco2D.GetNbinsY()):
                    num_reco_hits = histo_reco2D.GetBinContent(row, col)
                    num_miss_hits = histo_miss2D.GetBinContent(row, col)
                    occupancy = num_reco_hits / area / num_events
                    occupancy_bin = histo_reco_occupancy.GetXaxis().FindBin(
                        occupancy)
                    histo_reco_occupancy.SetBinContent(
                        occupancy_bin,
                        histo_reco_occupancy.GetBinContent(occupancy_bin) +
                        num_reco_hits)
                    histo_miss_occupancy.SetBinContent(
                        occupancy_bin,
                        histo_miss_occupancy.GetBinContent(occupancy_bin) +
                        num_miss_hits)

            histo_reco_occ_tot[lab].Add(histo_reco_occupancy)
            histo_miss_occ_tot[lab].Add(histo_miss_occupancy)

            histo_miss_occupancy.Sumw2()
            histo_reco_occupancy.Sumw2()

            teff = TEfficiency(histo_miss_occupancy, histo_reco_occupancy)
            teff.SetStatisticOption(TEfficiency.kFWilson)

            ineff[lab] = teff.CreateGraph()

        ymax = []
        for i, lab in enumerate(label):
            title = lab
            xtitle = "Occupancy [#MCHits/cm^{2}]"
            ytitle = "Inefficiency"
            title = "{};{};{}".format(title, xtitle, ytitle)
            ineff[lab].SetTitle(title)
            ineff[lab].Draw("{}".format("AP" if i == 0 else "P SAME"))
            set_style(ineff[lab], colors[i], markers[i], styles[i])
            legend.AddEntry(ineff[lab], lab, "lpe")
            ymax.append(max(ineff[lab].GetY()))

        gPad.Update()
        graph = ineff[label[0]]
        graph.GetYaxis().SetTitleOffset(1)
        graph.SetMinimum(0.)
        graph.SetMaximum(max(ymax) * 1.1)

        histo_reco_occupancy.Scale(
            max(ymax) / histo_reco_occupancy.GetMaximum() / 1.05)
        histo_reco_occupancy.Draw("SAME HIST")
        set_style(histo_reco_occupancy, kGray, markers[i], styles[i])
        legend.AddEntry(histo_reco_occupancy, "reconstructible", "f")

        legend.Draw()
        draw_sample(canvas, labels_location.x_label - 0.4,
                    labels_location.y_label, 0.06, "sensor " + str(mod),
                    num_events)
        gPad.SetRightMargin(0.08)
        gPad.Update()

        if savepdf:
            canvas.SaveAs(canvastitle + ".pdf")
        canvas.SetRightMargin(0.05)
        canvas.Write()

    #inefficiency vs occupancy whole detector
    canvastitle = "inefficiency_vs_occupancy"
    name = "inefficiency_vs_occupancy"
    canvas = TCanvas(name, canvastitle)

    labels_location = LabelsLocation()
    x, y = labels_location.x_legend, labels_location.y_legend
    legend = TLegend(x + 0.4, y - 0.2, x + 0.7, y - 0.5)
    legend.SetTextSize(0.048)
    legend.SetFillStyle(0)

    ineff = {}
    ymax = []
    for i, lab in enumerate(label):
        histo_reco_occ_tot[lab].Sumw2()
        histo_miss_occ_tot[lab].Sumw2()
        teff = TEfficiency(histo_miss_occ_tot[lab], histo_reco_occ_tot[lab])
        teff.SetStatisticOption(TEfficiency.kFWilson)
        ineff[lab] = teff.CreateGraph()
        ymax.append(max(ineff[lab].GetY()))

    for i, lab in enumerate(label):
        title = lab
        xtitle = "Occupancy [#MCHits/cm^{2}]"
        ytitle = "Inefficiency"
        title = "{};{};{}".format(title, xtitle, ytitle)
        ineff[lab].SetTitle(title)
        ineff[lab].Draw("{}".format("AP" if i == 0 else "P SAME"))
        set_style(ineff[lab], colors[i], markers[i], styles[i])
        legend.AddEntry(ineff[lab], lab, "lpe")

    gPad.Update()
    graph = ineff[label[0]]
    graph.SetMinimum(0.)
    graph.SetMaximum(max(ymax) * 1.1)

    histo_reco_occ_tot[label[0]].Scale(
        max(ymax) / histo_reco_occ_tot[label[0]].GetMaximum() / 1.05)
    histo_reco_occ_tot[label[0]].Draw("SAME HIST")
    set_style(histo_reco_occ_tot[label[0]], kGray, markers[i], styles[i])
    legend.AddEntry(histo_reco_occ_tot[label[0]], "reconstructible", "f")

    legend.Draw()
    draw_sample(canvas, labels_location.x_label - 0.4, labels_location.y_label,
                0.06, "", num_events)
    gPad.SetRightMargin(0.08)
    gPad.Update()

    if savepdf:
        canvas.SaveAs(canvastitle + ".pdf")
    canvas.SetRightMargin(0.05)
    canvas.Write()

    #inefficiency vs closest MCHit
    decaySel = getDecays()
    for sel in decaySel:
        canvastitle = "inefficiency_vs_distance" + sel.replace(" ", "")

        # get inefficiency
        histoName_num = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/Non reconstructed MCHit - closest reconstructible MCHit" + sel
        histoName_den = "VPClusterEfficiency" + unique_name_ext_re(
        ) + "/Reconstructible MCHit - closest reconstructible MCHit" + sel
        name = "inefficiency_vs_distance" + sel.replace(" ", "")
        canvas = TCanvas(name, canvastitle)

        eff = {}
        hist = {}
        empty_histos, eff, hist = get_eff(eff, hist, tf, histoName_num,
                                          histoName_den, label,
                                          "dist_close_hit", plotstyle)
        if empty_histos:
            continue
        ymax = []
        for i, lab in enumerate(label):
            eff[lab].Draw("{}".format("AP" if i == 0 else "P SAME"))
            set_style(eff[lab], colors[i], markers[i], styles[i])
            ymax.append(max(eff[lab].GetY()))
        gPad.Update()
        graph = eff[label[0]]
        graph.GetYaxis().SetTitleOffset(1)
        graph.SetMinimum(0.)
        graph.SetMaximum(max(ymax) * 1.1)

        if dist:
            for i, lab in enumerate(label):
                hist[lab].Scale(max(ymax) / hist[lab].GetMaximum() / 1.05)
                if i == 0:
                    set_style(hist[lab], kGray, markers[i], styles[i])
                else:
                    set_style(hist[lab], colors[i] - 10, markers[i], styles[i])
                hist[lab].Draw("hist SAME")
        else:
            set_style(hist[label[0]], kBlue - 10, markers[i], styles[i])
            hist[label[0]].Scale(
                max(ymax) / hist[label[0]].GetMaximum() / 1.05)
            hist[label[0]].Draw("hist SAME")

        effs = [eff[lab] for lab in label]
        hists = [hist[lab] for lab in label] if dist else [hist[label[0]]]

        labels_location = LabelsLocation()

        x, y = labels_location.x_legend, labels_location.y_legend
        legend = TLegend(x, y, x + 0.3, y + (len(effs) + len(hists)) * 0.05)
        legend.SetTextSize(0.048)
        legend.SetFillStyle(0)
        for eff in effs:
            legend.AddEntry(eff, eff.GetTitle(), "lpe")
        for hist in hists:
            legend.AddEntry(hist, hist.GetTitle(), "f")
        legend.Draw()
        gPad.Update()

        draw_sample(canvas, labels_location.x_label, labels_location.y_label,
                    0.06, sel, num_events)
        canvasName = "inefficiency_vs_distance" + sel.replace(" ", "") + ".pdf"
        if savepdf:
            canvas.SaveAs(canvasName)
        canvas.SetRightMargin(0.05)
        canvas.Write()

    # inefficiency vs r
    for module in range(0, 52):
        for var in ["r", "r<7mm"]:
            for cut in cuts:
                canvastitle = "inefficiency_" + cut.replace(
                    " ", "") + "_" + var + "_module" + str(module)

                # get inefficiency
                histoName_num = "VPClusterEfficiency" + unique_name_ext_re(
                ) + "/# MCHits not found" + cut + " - " + var + " - module" + str(
                    module)
                histoName_den = "VPClusterEfficiency" + unique_name_ext_re(
                ) + "/# MCHits reconstructible" + cut + " - " + var + " - module" + str(
                    module)
                eff = {}
                hist = {}
                empty_histos, eff, hist = get_eff(eff, hist, tf, histoName_num,
                                                  histoName_den, label, var,
                                                  plotstyle)
                if empty_histos:
                    continue
                name = "inefficiency_" + cut.replace(
                    " ", "") + "_" + var + "_module" + str(module)
                canvas = TCanvas(name, canvastitle)
                ymax = []
                for i, lab in enumerate(label):
                    eff[lab].Draw("{}".format("AP" if i == 0 else "P SAME"))
                    set_style(eff[lab], colors[i], markers[i], styles[i])
                    ymax.append(max(eff[lab].GetY()))
                gPad.Update()
                graph = eff[label[0]]
                graph.GetYaxis().SetTitleOffset(1)
                graph.SetMinimum(0.)
                graph.SetMaximum(max(ymax) * 1.1)

                if dist:
                    for i, lab in enumerate(label):
                        hist[lab].Scale(
                            max(ymax) / hist[lab].GetMaximum() / 1.05)
                        if i == 0:
                            set_style(hist[lab], kGray, markers[i], styles[i])
                        else:
                            set_style(hist[lab], colors[i] - 10, markers[i],
                                      styles[i])
                        hist[lab].Draw("hist SAME")
                else:
                    set_style(hist[label[0]], kBlue - 10, markers[i],
                              styles[i])
                    hist[label[0]].Scale(
                        max(ymax) / hist[lab].GetMaximum() / 1.05)
                    hist[label[0]].Draw("hist SAME")

                effs = [eff[lab] for lab in label]
                hists = [hist[lab]
                         for lab in label] if dist else [hist[label[0]]]

                labels_location = LabelsLocation()

                x, y = labels_location.x_legend, labels_location.y_legend
                legend = TLegend(x, y, x + 0.3,
                                 y + (len(effs) + len(hists)) * 0.05)
                legend.SetTextSize(0.048)
                legend.SetFillStyle(0)
                for eff in effs:
                    legend.AddEntry(eff, eff.GetTitle(), "lpe")
                for hist in hists:
                    legend.AddEntry(hist, hist.GetTitle(), "f")
                legend.Draw()

                draw_sample(canvas, labels_location.x_label,
                            labels_location.y_label, 0.06,
                            "module " + str(module), num_events)
                canvasName = "inefficiency_" + cut.replace(
                    " ", "") + "_" + var + "_module" + str(module) + ".pdf"
                if savepdf:
                    canvas.SaveAs(canvasName)
                canvas.SetRightMargin(0.05)
                canvas.Write()

    outputfile.Write()
    outputfile.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    assert (len(args.filename) == len(args.label))
    PrCheckerEfficiency(**vars(args))

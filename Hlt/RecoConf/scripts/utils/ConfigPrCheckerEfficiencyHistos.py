###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def efficiencyHistoDict():
    basedict = {
        "eta": {},
        "p": {},
        "pt": {},
        "phi": {},
        "r": {},
        "r<7mm": {},
        "z": {},
        "module": {},
        "dist_close_hit": {}
    }

    basedict["eta"]["xTitle"] = "#it{#eta}"
    basedict["eta"]["variable"] = "Eta"

    basedict["p"]["xTitle"] = "#it{p} [GeV/#it{c}]"
    basedict["p"]["variable"] = "P"

    basedict["pt"]["xTitle"] = "#it{p}_{T} [GeV/#it{c}]"
    basedict["pt"]["variable"] = "Pt"

    basedict["phi"]["xTitle"] = "#it{#phi} [rad]"
    basedict["phi"]["variable"] = "Phi"

    basedict["r"]["xTitle"] = "#it{r} [mm]"
    basedict["r"]["variable"] = "r"

    basedict["r<7mm"]["xTitle"] = "#it{r} [mm]"
    basedict["r<7mm"]["variable"] = "r"

    basedict["z"]["xTitle"] = "#it{z} [mm]"
    basedict["z"]["variable"] = "z"

    basedict["module"]["xTitle"] = "module"
    basedict["module"]["variable"] = "module"

    basedict["dist_close_hit"]["xTitle"] = "Distance to closest MCHit [mm]"
    basedict["dist_close_hit"]["variable"] = "Distance to closest MCHit [mm]"

    return basedict


class LabelsLocation:
    def __init__(self,
                 xlab=0.66,
                 ylab=0.76,
                 xleg=0.2,
                 yleg=0.75,
                 xs=0.37,
                 ys=0.25):
        self.x_label = xlab
        self.y_label = ylab
        self.x_legend = xleg
        self.y_legend = yleg
        self.x_sample = xs
        self.y_sample = ys

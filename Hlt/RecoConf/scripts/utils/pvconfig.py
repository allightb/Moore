###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

from ROOT import TH1F
from ROOT import kRed, kBlue, kOrange, kMagenta, kGreen, kCyan


def get_categories(multi, isol, smog):
    cut = {}
    cut["all"] = {"cut": ""}
    if isol:
        cut["isolated"] = {"cut": "&&isol==1"}
        if not smog:
            cut["close"] = {"cut": "&&isol==0"}
    if multi:
        cut["1st"] = {"cut": "&&multimc==1"}
        cut["2nd"] = {"cut": "&&multimc==2"}
        cut["3rd"] = {"cut": "&&multimc==3"}
        cut["4th"] = {"cut": "&&multimc==4"}
        cut["5th"] = {"cut": "&&multimc==5"}

    return cut


def get_style():
    return {
        "color": [kRed, kBlue, kOrange, kMagenta, kGreen, kCyan],
        "marker": [21, 20, 22, 23, 24, 25]
    }


def get_default_tree_name(is_checker):
    if is_checker:
        return "PrimaryVertexChecker/101"
    else:
        return "VertexCompare/102"


def transfer_variable(dep):
    dictionary = {
        "pullx": "dx/errx",
        "pully": "dy/erry",
        "pullz": "dz/errz",
    }
    return dictionary[dep]


def get_variable_ranges(smog):
    dictionary = {
        "zMC": {
            "bins": 50,
            "min": -200,
            "max": 200
        },
        "rMC": {
            "bins": 50,
            "min": 0.0,
            "max": 0.2
        },
        "xMC": {
            "bins": 50,
            "min": -0.2,
            "max": 0.2
        },
        "yMC": {
            "bins": 50,
            "min": -0.2,
            "max": 0.2
        },
        "dx": {
            "bins": 50,
            "min": -0.10,
            "max": 0.10
        },
        "dy": {
            "bins": 50,
            "min": -0.10,
            "max": 0.10
        },
        "dz": {
            "bins": 50,
            "min": -0.40,
            "max": 0.40
        },
        "pullx": {
            "bins": 50,
            "min": -3.50,
            "max": 3.50
        },
        "pully": {
            "bins": 50,
            "min": -3.50,
            "max": 3.50
        },
        "pullz": {
            "bins": 50,
            "min": -3.50,
            "max": 3.50
        },
        "nrectrmc": {
            "bins": 66,
            "min": 4.0,
            "max": 70.0
        },
        "mtruemcpv": {
            "bins": 20,
            "min": 0.0,
            "max": 20.0
        },
        "nmcpv": {
            "bins": 20,
            "min": 0.0,
            "max": 20.0
        },
    }
    if smog:
        dictionary["zMC"] = {"bins": 100, "min": -500, "max": 200}

    return dictionary


def get_y_axis(dependence):
    dictionary = {
        "dx": "Resolution #Delta x [#mu m]",
        "dy": "Resolution #Delta y [#mu m]",
        "dz": "Resolution #Delta z [#mu m]",
        "pullx": "Pull #Delta x/#sigma_{x}",
        "pully": "Pull #Delta y/#sigma_{y}",
        "pullz": "Pull #Delta z/#sigma_{z}"
    }
    return dictionary[dependence]


def get_x_axis(dependence):
    dictionary = {
        "zMC": "z [mm]",
        "rMC": "radial distance [mm]",
        "nrectrmc": "number of tracks in Primary Vertex"
    }
    return dictionary[dependence]


def set_style(graph, color, marker, xaxis, yaxis, title):
    graph.SetTitle("")
    graph.SetLineColor(color)
    graph.SetMarkerColor(color)
    graph.SetMarkerSize(1.3)
    graph.SetMarkerStyle(marker)
    graph.GetYaxis().SetTitleOffset(0.85)
    if type(graph) == TH1F:
        graph.SetFillColor(color)
        graph.SetLineWidth(1)
        graph.SetStats(False)
        graph.GetYaxis().SetTitleOffset(1.1)
    graph.GetYaxis().SetTitleSize(0.06)
    graph.GetYaxis().SetLabelSize(0.06)
    graph.GetXaxis().SetTitleSize(0.06)
    graph.GetXaxis().SetLabelSize(0.06)
    graph.GetXaxis().SetTitleFont(132)
    graph.GetXaxis().SetLabelFont(132)
    graph.GetYaxis().SetTitleFont(132)
    graph.GetYaxis().SetLabelFont(132)

    if title != "":
        graph.SetTitle(title)
    if xaxis != "":
        graph.GetXaxis().SetTitle(xaxis)
    if yaxis != "":
        graph.GetYaxis().SetTitle(yaxis)


def set_legend_simple(legend, label, hist):
    legend.SetTextSize(0.04)
    legend.SetTextFont(12)
    legend.SetFillColor(4000)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)
    legend.SetNColumns(2)
    for lab in label:
        legend.AddEntry(hist["all"][lab], "{lab}".format(lab=lab), "lep")

    return legend


def set_legend(legend, label, gr, gr_type, hist=None, dist=False):
    legend.SetTextSize(0.04)
    legend.SetTextFont(12)
    legend.SetFillColor(4000)
    legend.SetShadowColor(0)
    legend.SetBorderSize(0)
    legend.SetTextFont(132)
    legend.SetNColumns(2)
    for lab in label:
        legend.AddEntry(gr["all"][lab][gr_type], "{lab}".format(lab=lab),
                        "lep")
    if dist:
        legend.AddEntry(hist["all"][label[0]]["den"], "Distribution MC", "f")
        for lab in label:
            legend.AddEntry(hist["all"][lab]["nom"],
                            "Distribution {lab}".format(lab=lab), "lep")

    return legend


def get_text_cor():
    return {"x": [0.17, 0.65, 0.17, 0.65], "y": [0.92, 0.92, 0.75, 0.75]}


def basic_cut():
    return "nrectrmc>=4 && dz < 2.0"

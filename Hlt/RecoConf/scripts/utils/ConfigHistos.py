###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from collections import defaultdict


def efficiencyHistoDict():
    basedict = {
        "eta": {},
        "p": {},
        "pt": {},
        "phi": {},
        "nPV": {},
        "docaz": {},
        "z": {}
    }

    basedict["eta"]["xTitle"] = "#eta"
    basedict["eta"]["variable"] = "Eta"

    basedict["p"]["xTitle"] = "p [MeV]"
    basedict["p"]["variable"] = "P"

    basedict["pt"]["xTitle"] = "p_{T} [MeV]"
    basedict["pt"]["variable"] = "Pt"

    basedict["phi"]["xTitle"] = "#phi [rad]"
    basedict["phi"]["variable"] = "Phi"

    basedict["nPV"]["xTitle"] = "# of PVs"
    basedict["nPV"]["variable"] = "nPV"

    basedict["docaz"]["xTitle"] = "docaz [mm]"
    basedict["docaz"]["variable"] = "docaz"

    basedict["z"]["xTitle"] = "PV z coordinate [mm]"
    basedict["z"]["variable"] = "z"

    return basedict


def ghostHistoDict():
    basedict = {"eta": {}, "nPV": {}, "pt": {}, "p": {}}

    basedict["eta"]["xTitle"] = "#eta"
    basedict["eta"]["variable"] = "Eta"

    basedict["nPV"]["xTitle"] = "# of PVs"
    basedict["nPV"]["variable"] = "nPV"

    basedict["pt"]["xTitle"] = "p_{T} [MeV]"
    basedict["pt"]["variable"] = "Pt"

    basedict["p"]["xTitle"] = "p [MeV]"
    basedict["p"]["variable"] = "P"

    return basedict


def getCuts():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "MuonMatch": {},
        "Match": {},
        "Seed": {},
        "Downstream": {},
        "BestLong": {},
        "BestDownstream": {},
        "LongGhostFiltered": {},
        "DownstreamGhostFiltered": {}
    }

    basedict["Velo"] = [
        "01_velo", "02_long", "03_long_P>5GeV", "04_long_strange",
        "05_long_strange_P>5GeV", "06_long_fromB", "07_long_fromB_P>5GeV",
        "11_long_fromB_P>3GeV_Pt>0.5GeV", "12_UT_long_fromB_P>3GeV_Pt>0.5GeV"
    ]
    basedict["Upstream"] = [
        "01_velo", "02_velo+UT", "03_velo+UT_P>5GeV", "07_long",
        "08_long_P>5GeV", "09_long_fromB", "10_long_fromB_P>5GeV",
        "14_long_fromB_P>3GeV_Pt>0.5GeV", "15_UT_long_fromB_P>3GeV_Pt>0.5GeV"
    ]
    basedict["Forward"] = [
        "01_long", "02_long_P>5GeV", "03_long_strange",
        "04_long_strange_P>5GeV", "05_long_fromB", "06_long_fromB_P>5GeV",
        "10_long_fromB_P>3GeV_Pt>0.5GeV", "11_UT_long_fromB_P>3GeV_Pt>0.5GeV"
    ]
    basedict["MuonMatch"] = ["01_long", "02_long_muon", "04_long_pion"]

    basedict["Match"] = [
        "01_long", "02_long_P>5GeV", "03_long_strange",
        "04_long_strange_P>5GeV", "05_long_fromB", "06_long_fromB_P>5GeV",
        "10_long_fromB_P>3GeV_Pt>0.5GeV", "11_UT_long_fromB_P>3GeV_Pt>0.5GeV"
    ]

    basedict["Seed"] = [
        "01_hasT",
        "02_long",
        "03_long_P>5GeV",
        "04_long_fromB",
        "05_long_fromB_P>5GeV",
        "08_noVelo+UT+T_strange",
        "09_noVelo+UT+T_strange_P>5GeV",
        "12_noVelo+UT+T_SfromDB_P>5GeV",
    ]

    basedict["Downstream"] = [
        "01_UT+T", "05_noVelo+UT+T_strange", "06_noVelo+UT+T_strange_P>5GeV",
        "13_noVelo+UT+T_SfromDB", "14_noVelo+UT+T_SfromDB_P>5GeV"
    ]

    basedict["BestLong"] = [
        "01_long", "02_long_P>5GeV", "03_long_strange",
        "04_long_strange_P>5GeV", "05_long_fromB", "06_long_fromB_P>5GeV",
        "10_long_fromB_P>3GeV_Pt>0.5GeV"
    ]

    basedict["BestDownstream"] = [
        "01_UT+T", "05_noVelo+UT+T_strange", "06_noVelo+UT+T_strange_P>5GeV",
        "13_noVelo+UT+T_SfromDB", "14_noVelo+UT+T_SfromDB_P>5GeV"
    ]

    basedict["LongGhostFiltered"] = [
        "01_long", "02_long_P>5GeV", "03_long_strange",
        "04_long_strange_P>5GeV", "05_long_fromB", "06_long_fromB_P>5GeV",
        "10_long_fromB_P>3GeV_Pt>0.5GeV"
    ]

    basedict["DownstreamGhostFiltered"] = [
        "01_UT+T", "05_noVelo+UT+T_strange", "06_noVelo+UT+T_strange_P>5GeV",
        "13_noVelo+UT+T_SfromDB", "14_noVelo+UT+T_SfromDB_P>5GeV"
    ]

    return basedict


def categoriesDict():
    basedict = defaultdict(lambda: defaultdict(dict))
    ########
    ### VELO
    ########
    basedict["Velo"]["01_velo"]["title"] = "Velo, 2 <#eta< 5"
    basedict["Velo"]["02_long"]["title"] = "Long, 2 <#eta< 5"
    basedict["Velo"]["03_long_P>5GeV"]["title"] = "Long, p>5GeV, 2<#eta< 5"
    basedict["Velo"]["04_long_strange"][
        "title"] = "Long, from Strange, 2 <#eta < 5"
    basedict["Velo"]["05_long_strange_P>5GeV"][
        "title"] = "Long, from Strange, p>5GeV, 2 <#eta < 5"
    basedict["Velo"]["06_long_fromB"]["title"] = "Long from B, 2<#eta<5"
    basedict["Velo"]["07_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV, 2<#eta<5"
    basedict["Velo"]["11_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from B, p>3GeV, pt>0.5GeV, 2<#eta<5"
    basedict["Velo"]["11_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from strange, p>3GeV, pt>0.5GeV, 2<#eta<5"
    basedict["Velo"]["12_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "UT Long, from B, p>3GeV, pt>0.5GeV, 2<#eta<5"
    basedict["Velo"]["01_velo"]["plotElectrons"] = False
    basedict["Velo"]["02_long"]["plotElectrons"] = True
    basedict["Velo"]["03_long_P>5GeV"]["plotElectrons"] = False
    basedict["Velo"]["04_long_strange"]["plotElectrons"] = False
    basedict["Velo"]["05_long_strange_P>5GeV"]["plotElectrons"] = False
    basedict["Velo"]["06_long_fromB"]["plotElectrons"] = True
    basedict["Velo"]["07_long_fromB_P>5GeV"]["plotElectrons"] = True
    basedict["Velo"]["11_long_fromB_P>3GeV_Pt>0.5GeV"]["plotElectrons"] = False
    basedict["Velo"]["11_long_strange_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Velo"]["12_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False

    basedict["Velo"]["02_long"]["Electrons"] = "08_long_electrons"
    basedict["Velo"]["06_long_fromB"]["Electrons"] = "09_long_fromB_electrons"
    basedict["Velo"]["07_long_fromB_P>5GeV"][
        "Electrons"] = "10_long_fromB_electrons_P>5GeV"
    basedict["Velo"]["11_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "11_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    ########
    ### UPSTREAM
    ########
    basedict["Upstream"]["01_velo"]["title"] = "Velo, 2 <#eta < 5"
    basedict["Upstream"]["02_velo+UT"]["title"] = "VeloUT, 2 <#eta < 5"
    basedict["Upstream"]["03_velo+UT_P>5GeV"][
        "title"] = "VeloUT, p>5GeV, 2 <#eta < 5"
    basedict["Upstream"]["07_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["Upstream"]["08_long_P>5GeV"][
        "title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["Upstream"]["09_long_fromB"]["title"] = "Long from B, 2 <#eta < 5"
    basedict["Upstream"]["10_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV, 2 <#eta < 5"
    basedict["Upstream"]["14_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long, from B, p>3GeV, pt>0.5GeV"
    basedict["Upstream"]["14_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long, from strange, p>3GeV, pt>0.5GeV"
    basedict["Upstream"]["15_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long, from B, p>3GeV, pt>0.5GeV"

    basedict["Upstream"]["01_velo"]["plotElectrons"] = False
    basedict["Upstream"]["02_velo+UT"]["plotElectrons"] = False
    basedict["Upstream"]["03_velo+UT_P>5GeV"]["plotElectrons"] = False
    basedict["Upstream"]["07_long"]["plotElectrons"] = True
    basedict["Upstream"]["08_long_P>5GeV"]["plotElectrons"] = False
    basedict["Upstream"]["09_long_fromB"]["plotElectrons"] = True
    basedict["Upstream"]["10_long_fromB_P>5GeV"]["plotElectrons"] = True
    basedict["Upstream"]["14_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Upstream"]["14_long_strange_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Upstream"]["15_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Upstream"]["07_long"]["Electrons"] = "11_long_electrons"
    basedict["Upstream"]["09_long_fromB"][
        "Electrons"] = "12_long_fromB_electrons"
    basedict["Upstream"]["10_long_fromB_P>5GeV"][
        "Electrons"] = "13_long_fromB_electrons_P>5GeV"
    basedict["Upstream"]["14_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "14_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    ########
    ### FORwARD
    ########
    basedict["Forward"]["01_long"]["Electrons"] = "07_long_electrons"
    basedict["Forward"]["05_long_fromB"][
        "Electrons"] = "08_long_fromB_electrons"
    basedict["Forward"]["06_long_fromB_P>5GeV"][
        "Electrons"] = "09_long_fromB_electrons_P>5GeV"
    basedict["Forward"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "10_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    basedict["Forward"]["01_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["Forward"]["02_long_P>5GeV"][
        "title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["Forward"]["03_long_strange"][
        "title"] = "Long, from strange, 2 <#eta < 5"
    basedict["Forward"]["04_long_strange_P>5GeV"][
        "title"] = "Long, from strange, p>5GeV, 2 <#eta < 5"
    basedict["Forward"]["05_long_fromB"]["title"] = "Long from B, 2 <#eta < 5"
    basedict["Forward"]["06_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV 2 <#eta < 5"
    basedict["Forward"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["Forward"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from strange, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["Forward"]["11_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "UT Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"

    basedict["Forward"]["01_long"]["plotElectrons"] = True
    basedict["Forward"]["02_long_P>5GeV"]["plotElectrons"] = False
    basedict["Forward"]["03_long_strange"]["plotElectrons"] = False
    basedict["Forward"]["04_long_strange_P>5GeV"]["plotElectrons"] = False
    basedict["Forward"]["05_long_fromB"]["plotElectrons"] = True
    basedict["Forward"]["06_long_fromB_P>5GeV"]["plotElectrons"] = True
    basedict["Forward"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Forward"]["11_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False

    ########
    ### MUONMATCH
    ########
    basedict["MuonMatch"]["01_long"][
        "title"] = "Long, forward track, 2 <#eta< 5"
    basedict["MuonMatch"]["02_long_muon"][
        "title"] = "Long, #mu, forward track, 2 <#eta< 5"
    basedict["MuonMatch"]["04_long_pion"][
        "title"] = "Long, #pi, forward track, 2 <#eta< 5"

    ########
    ### MATCH
    ########
    basedict["Match"]["01_long"]["Electrons"] = "07_long_electrons"
    basedict["Match"]["05_long_fromB"]["Electrons"] = "08_long_fromB_electrons"
    basedict["Match"]["06_long_fromB_P>5GeV"][
        "Electrons"] = "09_long_fromB_electrons_P>5GeV"
    basedict["Match"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "10_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    basedict["Match"]["01_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["Match"]["02_long_P>5GeV"]["title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["Match"]["03_long_strange"][
        "title"] = "Long, from strange, 2 <#eta < 5"
    basedict["Match"]["04_long_strange_P>5GeV"][
        "title"] = "Long, from strange, p>5GeV, 2 <#eta < 5"
    basedict["Match"]["05_long_fromB"]["title"] = "Long from B, 2 <#eta < 5"
    basedict["Match"]["06_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV 2 <#eta < 5"
    basedict["Match"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["Match"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from strange, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["Match"]["11_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "UT Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"

    basedict["Match"]["01_long"]["plotElectrons"] = True
    basedict["Match"]["02_long_P>5GeV"]["plotElectrons"] = False
    basedict["Match"]["03_long_strange"]["plotElectrons"] = False
    basedict["Match"]["04_long_strange_P>5GeV"]["plotElectrons"] = False
    basedict["Match"]["05_long_fromB"]["plotElectrons"] = True
    basedict["Match"]["06_long_fromB_P>5GeV"]["plotElectrons"] = True
    basedict["Match"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Match"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["Match"]["11_UT_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False

    ########
    ### SEED
    ########
    basedict["Seed"]["01_hasT"]["Electrons"] = "13_hasT_electrons"
    basedict["Seed"]["02_long"]["Electrons"] = "14_long_electrons"
    basedict["Seed"]["03_long_P>5GeV"][
        "Electrons"] = "16_long_electrons_P>5GeV"
    basedict["Seed"]["04_long_fromB"]["Electrons"] = "15_long_fromB_electrons"
    basedict["Seed"]["05_long_fromB_P>5GeV"][
        "Electrons"] = "17_long_fromB_electrons_P>5GeV"

    basedict["Seed"]["01_hasT"]["title"] = "T, 2 <#eta < 5"
    basedict["Seed"]["02_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["Seed"]["03_long_P>5GeV"]["title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["Seed"]["04_long_fromB"]["title"] = "Long, from B, 2 <#eta < 5"
    basedict["Seed"]["05_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV, 2 <#eta < 5"
    basedict["Seed"]["08_noVelo+UT+T_strange"][
        "title"] = "Down from strange, 2 <#eta < 5"
    basedict["Seed"]["09_noVelo+UT+T_strange_P>5GeV"][
        "title"] = "Down from strange, p>5GeV, 2 <#eta < 5"
    basedict["Seed"]["12_noVelo+UT+T_SfromDB_P>5GeV"][
        "title"] = "Down from strange from B/D, p>5GeV, 2 <#eta < 5"

    basedict["Seed"]["01_hasT"]["plotElectrons"] = False
    basedict["Seed"]["02_long"]["plotElectrons"] = True
    basedict["Seed"]["03_long_P>5GeV"]["plotElectrons"] = False
    basedict["Seed"]["04_long_fromB"]["plotElectrons"] = True
    basedict["Seed"]["05_long_fromB_P>5GeV"]["plotElectrons"] = False
    basedict["Seed"]["08_noVelo+UT+T_strange"]["plotElectrons"] = False
    basedict["Seed"]["09_noVelo+UT+T_strange_P>5GeV"]["plotElectrons"] = False
    basedict["Seed"]["12_noVelo+UT+T_SfromDB_P>5GeV"]["plotElectrons"] = False

    ########
    ### DOWNSTREAM
    ########
    basedict["Downstream"]["01_UT+T"]["title"] = "UT+T, 2 <#eta < 5"
    basedict["Downstream"]["05_noVelo+UT+T_strange"][
        "title"] = "Down from strange, 2 <#eta < 5"
    basedict["Downstream"]["06_noVelo+UT+T_strange_P>5GeV"][
        "title"] = "Down from strange, p>5GeV, 2 <#eta < 5"
    basedict["Downstream"]["13_noVelo+UT+T_SfromDB"][
        "title"] = "Down from strange from B/D, 2 <#eta < 5"
    basedict["Downstream"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "title"] = "Down from strange from B/D, p>5GeV, 2 <#eta < 5"

    basedict["Downstream"]["01_UT+T"]["plotElectrons"] = False
    basedict["Downstream"]["05_noVelo+UT+T_strange"]["plotElectrons"] = False
    basedict["Downstream"]["06_noVelo+UT+T_strange_P>5GeV"][
        "plotElectrons"] = False
    basedict["Downstream"]["13_noVelo+UT+T_SfromDB"]["plotElectrons"] = False
    basedict["Downstream"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "plotElectrons"] = False

    ########
    ### BESTLONG
    ########
    basedict["BestLong"]["01_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["BestLong"]["02_long_P>5GeV"][
        "title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["BestLong"]["03_long_strange"][
        "title"] = "Long, from strange, 2 <#eta < 5"
    basedict["BestLong"]["04_long_strange_P>5GeV"][
        "title"] = "Long, from strange, p>5GeV, 2 <#eta < 5"
    basedict["BestLong"]["05_long_fromB"]["title"] = "Long from B, 2 <#eta < 5"
    basedict["BestLong"]["06_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV 2 <#eta < 5"
    basedict["BestLong"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["BestLong"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from strange, p>3GeV, pt>0.5GeV, 2 <#eta < 5"

    basedict["BestLong"]["01_long"]["plotElectrons"] = True
    basedict["BestLong"]["02_long_P>5GeV"]["plotElectrons"] = False
    basedict["BestLong"]["03_long_strange"]["plotElectrons"] = False
    basedict["BestLong"]["04_long_strange_P>5GeV"]["plotElectrons"] = False
    basedict["BestLong"]["05_long_fromB"]["plotElectrons"] = True
    basedict["BestLong"]["06_long_fromB_P>5GeV"]["plotElectrons"] = True
    basedict["BestLong"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["BestLong"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False

    basedict["BestLong"]["01_long"]["Electrons"] = "07_long_electrons"
    basedict["BestLong"]["05_long_fromB"][
        "Electrons"] = "08_long_fromB_electrons"
    basedict["BestLong"]["06_long_fromB_P>5GeV"][
        "Electrons"] = "09_long_fromB_electrons_P>5GeV"
    basedict["BestLong"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "10_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    ########
    ### BESTDOWNSTREAM
    ########
    basedict["BestDownstream"]["01_UT+T"]["title"] = "UT+T, 2 <#eta < 5"
    basedict["BestDownstream"]["05_noVelo+UT+T_strange"][
        "title"] = "Down from strange, 2 <#eta < 5"
    basedict["BestDownstream"]["06_noVelo+UT+T_strange_P>5GeV"][
        "title"] = "Down from strange, p>5GeV, 2 <#eta < 5"
    basedict["BestDownstream"]["13_noVelo+UT+T_SfromDB"][
        "title"] = "Down from strange from B/D, 2 <#eta < 5"
    basedict["BestDownstream"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "title"] = "Down from strange from B/D, p>5GeV, 2 <#eta < 5"

    basedict["BestDownstream"]["01_UT+T"]["plotElectrons"] = False
    basedict["BestDownstream"]["05_noVelo+UT+T_strange"][
        "plotElectrons"] = False
    basedict["BestDownstream"]["06_noVelo+UT+T_strange_P>5GeV"][
        "plotElectrons"] = False
    basedict["BestDownstream"]["13_noVelo+UT+T_SfromDB"][
        "plotElectrons"] = False
    basedict["BestDownstream"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "plotElectrons"] = False

    ########
    ### LONGGHOSTFILTERED
    ########
    basedict["LongGhostFiltered"]["01_long"]["title"] = "Long, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["02_long_P>5GeV"][
        "title"] = "Long, p>5GeV, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["03_long_strange"][
        "title"] = "Long, from strange, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["04_long_strange_P>5GeV"][
        "title"] = "Long, from strange, p>5GeV, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["05_long_fromB"][
        "title"] = "Long from B, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["06_long_fromB_P>5GeV"][
        "title"] = "Long from B, p>5GeV 2 <#eta < 5"
    basedict["LongGhostFiltered"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from B, p>3GeV, pt>0.5GeV, 2 <#eta < 5"
    basedict["LongGhostFiltered"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "title"] = "Long from strange, p>3GeV, pt>0.5GeV, 2 <#eta < 5"

    basedict["LongGhostFiltered"]["01_long"]["plotElectrons"] = True
    basedict["LongGhostFiltered"]["02_long_P>5GeV"]["plotElectrons"] = False
    basedict["LongGhostFiltered"]["03_long_strange"]["plotElectrons"] = False
    basedict["LongGhostFiltered"]["04_long_strange_P>5GeV"][
        "plotElectrons"] = False
    basedict["LongGhostFiltered"]["05_long_fromB"]["plotElectrons"] = True
    basedict["LongGhostFiltered"]["06_long_fromB_P>5GeV"][
        "plotElectrons"] = True
    basedict["LongGhostFiltered"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False
    basedict["LongGhostFiltered"]["10_long_strange_P>3GeV_Pt>0.5GeV"][
        "plotElectrons"] = False

    basedict["LongGhostFiltered"]["01_long"]["Electrons"] = "07_long_electrons"
    basedict["LongGhostFiltered"]["05_long_fromB"][
        "Electrons"] = "08_long_fromB_electrons"
    basedict["LongGhostFiltered"]["06_long_fromB_P>5GeV"][
        "Electrons"] = "09_long_fromB_electrons_P>5GeV"
    basedict["LongGhostFiltered"]["10_long_fromB_P>3GeV_Pt>0.5GeV"][
        "Electrons"] = "10_long_fromB_electrons_P>3GeV_Pt>0.5GeV"

    ########
    ### DOWNSTREAMGHOSTFILTERED
    ########
    basedict["DownstreamGhostFiltered"]["01_UT+T"][
        "title"] = "UT+T, 2 <#eta < 5"
    basedict["DownstreamGhostFiltered"]["05_noVelo+UT+T_strange"][
        "title"] = "Down from strange, 2 <#eta < 5"
    basedict["DownstreamGhostFiltered"]["06_noVelo+UT+T_strange_P>5GeV"][
        "title"] = "Down from strange, p>5GeV, 2 <#eta < 5"
    basedict["DownstreamGhostFiltered"]["13_noVelo+UT+T_SfromDB"][
        "title"] = "Down from strange from B/D, 2 <#eta < 5"
    basedict["DownstreamGhostFiltered"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "title"] = "Down from strange from B/D, p>5GeV, 2 <#eta < 5"

    basedict["DownstreamGhostFiltered"]["01_UT+T"]["plotElectrons"] = False
    basedict["DownstreamGhostFiltered"]["05_noVelo+UT+T_strange"][
        "plotElectrons"] = False
    basedict["DownstreamGhostFiltered"]["06_noVelo+UT+T_strange_P>5GeV"][
        "plotElectrons"] = False
    basedict["DownstreamGhostFiltered"]["13_noVelo+UT+T_SfromDB"][
        "plotElectrons"] = False
    basedict["DownstreamGhostFiltered"]["14_noVelo+UT+T_SfromDB_P>5GeV"][
        "plotElectrons"] = False

    return basedict

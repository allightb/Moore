###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.mc_checking import tracker_dumper
from RecoConf.legacy_rec_hlt1_tracking import make_RetinaCluster_raw_bank, make_velo_full_clusters, make_RetinaClusters
from PyConf.Algorithms import VPRetinaFullClustering
from Allen.config import allen_non_event_data_config
from RecoConf.hlt1_allen import combine_raw_banks_with_MC_data_for_standalone_Allen_checkers

import os

key = os.getenv('TESTFILE_KEY')

if not key:
    raise ValueError("No sample specified from which to dump MDF files")
options.evt_max = 10

if key == 'SMOG2_pppHe':
    options.input_type = 'ROOT'
else:
    from PRConfig.TestFileDB import test_file_db
    qualifiers = test_file_db[key].qualifiers
    file_format = test_file_db[key].qualifiers['Format']
    options.input_type = 'ROOT' if file_format != 'MDF' else file_format

input_files = os.getenv('INPUT_FILES').split("!")
print(input_files)
options.input_files = input_files
base_dir = os.getenv('BASE_DIR')

if key == 'SMOG2_pppHe':
    options.dddb_tag = "dddb-20190223"
    options.conddb_tag = "sim-20180530-vc-mu100"
else:
    options.set_conds_from_testfiledb(key)

options.evt_max = 10
options.output_file = os.getenv('OUTPUT_FILE') + '.mdf'
print(options.output_file)
options.output_type = 'MDF'
geom_dir = base_dir + "/" + key + "/geometry_" + options.dddb_tag + "_" + options.conddb_tag

with_retina_clusters = False


def dump_mdf():
    algs = combine_raw_banks_with_MC_data_for_standalone_Allen_checkers(
        output_file=options.output_file)
    return Reconstruction('write_mdf', algs)


if with_retina_clusters:

    with allen_non_event_data_config.bind(dump_geometry=True, out_dir=geom_dir),\
         make_RetinaClusters.bind(make_raw=make_RetinaCluster_raw_bank),\
         make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),\
         tracker_dumper.bind(velo_hits=make_RetinaClusters):
        run_allen_reconstruction(options, dump_mdf)

else:
    with allen_non_event_data_config.bind(
            dump_geometry=True, out_dir=geom_dir):
        run_allen_reconstruction(options, dump_mdf)

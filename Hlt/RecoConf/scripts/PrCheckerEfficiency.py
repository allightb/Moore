#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Script for accessing histograms of reconstructible and
# reconstructed tracks for different tracking categories
# created by hlt1_reco_baseline_with_mcchecking
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Dorothea vom Bruch (dorothea.vom.bruch@cern.ch)
# date:   10/2018
# updated by Peilian Li
#

import os, sys
import argparse
from ROOT import TMultiGraph, TLatex, TCanvas, TFile
from ROOT import kGreen, kBlue, kBlack, kAzure, kGray, kOrange, kMagenta, kCyan, gPad
from ROOT import gROOT
from ROOT import TEfficiency
gROOT.SetBatch(True)

from PyConf.components import unique_name_ext_re, findRootObjByName


def getEfficiencyHistoNames():
    return ["eta", "p", "pt", "phi", "nPV", "docaz"]


def getTrackers(mode):
    if mode == "HLT1":
        return ["Velo", "Upstream", "Forward"]
    else:
        # here we return "everything" as HLT1 is rerun in HLT2
        return [
            "Velo", "Upstream", "Forward", "Match", "Seed", "Downstream",
            "BestLong", "BestDownstream", "LongGhostFiltered",
            "DownstreamGhostFiltered"
        ]


def getOriginFolders():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "Match": {},
        "Seed": {},
        "Downstream": {},
        "BestLong": {},
        "BestDownstream": {},
        "LongGhostFiltered": {},
        "DownstreamGhostFiltered": {}
    }

    basedict["Velo"]["folder"] = "VeloTrackChecker" + unique_name_ext_re(
    ) + "/"
    basedict["Upstream"][
        "folder"] = "UpstreamTrackChecker" + unique_name_ext_re() + "/"
    basedict["Forward"]["folder"] = "ForwardTrackChecker" + unique_name_ext_re(
    ) + "/"
    basedict["Match"]["folder"] = "MatchTrackChecker/"
    basedict["Seed"]["folder"] = "SeedTrackChecker/"
    basedict["Downstream"]["folder"] = "DownstreamTrackChecker/"
    basedict["BestLong"]["folder"] = "BestLongTrackChecker/"
    basedict["BestDownstream"]["folder"] = "BestDownstreamTrackChecker/"
    basedict["LongGhostFiltered"]["folder"] = "LongGhostFilteredTrackChecker/"
    basedict["DownstreamGhostFiltered"][
        "folder"] = "DownstreamGhostFilteredTrackChecker/"

    return basedict


def getTrackNames():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "Match": {},
        "Seed": {},
        "Downstream": {},
        "BestLong": {},
        "BestDownstream": {},
        "LongGhostFiltered": {},
        "DownstreamGhostFiltered": {}
    }

    basedict["Velo"] = "Velo"
    basedict["Upstream"] = "VeloUT"
    basedict["Forward"] = "Forward"
    basedict["Match"] = "Match"
    basedict["Seed"] = "Seed"
    basedict["Downstream"] = "Downstream"
    basedict["BestLong"] = "Long"
    basedict["BestDownstream"] = "Downstream"
    basedict["LongGhostFiltered"] = "Long"
    basedict["DownstreamGhostFiltered"] = "Downstream"

    return basedict


def get_colors():
    return [kBlack, kAzure, kGreen + 3, kMagenta + 2, kOrange, kCyan + 2]


def get_markers():
    return [20, 24, 21, 22, 23, 25]


def get_fillstyles():
    return [3004, 3003, 3325, 3144, 3244, 3444]


def getGhostHistoNames():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "Match": {},
        "Seed": {},
        "Downstream": {},
        "BestLong": {},
        "BestDownstream": {},
        "LongGhostFiltered": {},
        "DownstreamGhostFiltered": {}
    }

    basedict["Velo"] = ["eta", "nPV"]
    basedict["Upstream"] = ["eta", "nPV", "pt", "p"]
    basedict["Forward"] = ["eta", "nPV", "pt", "p"]
    basedict["Match"] = ["eta", "nPV", "pt", "p"]
    basedict["Seed"] = ["eta", "nPV", "pt", "p"]
    basedict["Downstream"] = ["eta", "nPV", "pt", "p"]
    basedict["BestLong"] = ["eta", "nPV", "pt", "p"]
    basedict["BestDownstream"] = ["eta", "nPV", "pt", "p"]
    basedict["LongGhostFiltered"] = ["eta", "nPV", "pt", "p"]
    basedict["DownstreamGhostFiltered"] = ["eta", "nPV", "pt", "p"]

    return basedict


def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--filename',
        type=str,
        default=['MCMatching_MiniBias.root'],
        nargs='+',
        help='input files, including path')
    parser.add_argument(
        '--outfile',
        type=str,
        default='efficiency_plots.root',
        help='output file')
    parser.add_argument(
        '--mode',
        type=str,
        default='HLT2',
        choices=['HLT1', 'HLT2'],
        help='HLT1 or HLT2 track types')
    parser.add_argument(
        '--label', nargs='+', default=["EffChecker"], help='label for files')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    parser.add_argument(
        '--dist',
        default=False,
        action='store_true',
        help='draw histogram of all files')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    return parser


def get_files(tf, filename, label):
    for i, f in enumerate(filename):
        tf[label[i]] = TFile(f, "read")
    return tf


def get_eff(eff, hist, tf, histoName, label, var):
    eff = {}
    hist = {}
    for i, lab in enumerate(label):
        numeratorName = histoName + "_reconstructed"
        numerator = findRootObjByName(tf[lab], numeratorName)
        denominatorName = histoName + "_reconstructible"
        denominator = findRootObjByName(tf[lab], denominatorName)
        if numerator.GetEntries() == 0 or denominator.GetEntries() == 0:
            continue

        teff = TEfficiency(numerator, denominator)
        teff.SetStatisticOption(7)
        eff[lab] = teff.CreateGraph()
        eff[lab].SetName(lab)
        eff[lab].SetTitle(lab + "not electron")
        if (histoName.find('strange') != -1):
            eff[lab].SetTitle(lab + " from stranges")
        if (histoName.find('electron') != -1):
            eff[lab].SetTitle(lab + " electron")

        hist[lab] = denominator.Clone()
        hist[lab].SetName("h_numerator_notElectrons")
        hist[lab].SetTitle(var + " distribution, not electron")
        if (histoName.find('strange') != -1):
            hist[lab].SetTitle(var + " distribution, stranges")
        if (histoName.find('electron') != -1):
            hist[lab].SetTitle(var + " distribution, electron")

    return eff, hist


def get_ghost(eff, hist, tf, histoName, label):
    ghost = {}
    for i, lab in enumerate(label):
        numeratorName = histoName + "_Ghosts"
        denominatorName = histoName + "_Total"
        numerator = findRootObjByName(tf[lab], numeratorName)
        denominator = findRootObjByName(tf[lab], denominatorName)
        print("Numerator = " + numeratorName)
        print("Denominator = " + denominatorName)
        teff = TEfficiency(numerator, denominator)
        teff.SetStatisticOption(7)
        ghost[lab] = teff.CreateGraph()
        print(lab)
        ghost[lab].SetName(lab)

    return ghost


def PrCheckerEfficiency(filename, outfile, mode, label, plotstyle, dist,
                        savepdf):
    sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
    from LHCbStyle import setLHCbStyle, set_style
    from ConfigHistos import (efficiencyHistoDict, ghostHistoDict,
                              categoriesDict, getCuts)
    from Legend import place_legend  # noqa decorate TPad
    import re
    setLHCbStyle()

    markers = get_markers()
    colors = get_colors()
    styles = get_fillstyles()

    tf = {}
    tf = get_files(tf, filename, label)
    outputfile = TFile(outfile, "recreate")

    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.05)

    efficiencyHistoDict = efficiencyHistoDict()
    efficiencyHistos = getEfficiencyHistoNames()
    ghostHistos = getGhostHistoNames()
    ghostHistoDict = ghostHistoDict()
    categories = categoriesDict()
    cuts = getCuts()
    trackers = getTrackers(mode)
    folders = getOriginFolders()
    names = getTrackNames()

    for tracker in trackers:
        outputfile.cd()
        trackerDir = outputfile.mkdir(tracker)
        trackerDir.cd()

        for cut in cuts[tracker]:
            cutDir = trackerDir.mkdir(cut)
            cutDir.cd()
            folder = folders[tracker]["folder"]
            print(folder)
            histoBaseName = "Track/" + folder + tracker + "/" + re.escape(
                cut) + "_"

            # calculate efficiency
            for histo in efficiencyHistos:
                canvastitle = "efficiency_" + histo + ", " + categories[
                    tracker][cut]["title"]
                # get efficiency for not electrons category
                histoName = histoBaseName + "" + efficiencyHistoDict[histo][
                    "variable"]
                print("not electrons: " + histoName)
                eff = {}
                hist_den = {}
                eff, hist_den = get_eff(eff, hist_den, tf, histoName, label,
                                        histo)
                name = "efficiency_" + histo
                canvas = TCanvas(name, canvastitle)
                mg = TMultiGraph()
                for i, lab in enumerate(label):
                    mg.Add(eff[lab])
                    set_style(eff[lab], colors[i], markers[i], styles[i])
                mg.Draw("AP")
                mg.GetYaxis().SetRangeUser(0, 1.05)
                xtitle = efficiencyHistoDict[histo]["xTitle"]
                mg.GetXaxis().SetTitle(xtitle)
                mg.GetYaxis().SetTitle("Efficiency")
                if dist:
                    for i, lab in enumerate(label):
                        rightmax = 1.05 * hist_den[lab].GetMaximum()
                        scale = gPad.GetUymax() / rightmax
                        hist_den[lab].Scale(scale)
                        if i == 0:
                            set_style(hist_den[lab], kGray + 1, markers[i],
                                      styles[i])
                        else:
                            set_style(hist_den[lab], colors[i] - 10,
                                      markers[i], styles[i])
                        hist_den[lab].Draw("hist SAME")
                else:
                    rightmax = 0.95 * hist_den[label[0]].GetMaximum()
                    scale = gPad.GetUymax() / rightmax
                    set_style(hist_den[label[0]], kBlue - 10, markers[i],
                              styles[i])
                    hist_den[label[0]].Scale(scale)
                    hist_den[label[0]].Draw("hist SAME")

                canvas.PlaceLegend()
                for lab in label:
                    eff[lab].Draw("P SAME")
                cutName = categories[tracker][cut]["title"]
                latex.DrawLatex(0.7, 0.85, "LHCb simulation")
                track_name = names[tracker] + " tracks"
                latex.DrawLatex(0.7, 0.75, track_name)
                latex.DrawLatex(0.35, 0.3, cutName)
                canvasName = tracker + "_" + cut + "_" + histo + ".pdf"
                if savepdf:
                    canvas.SaveAs(canvasName)
                canvas.SetRightMargin(0.05)
                canvas.Write()

                # get efficiency for electrons category
                if categories[tracker][cut]["plotElectrons"]:
                    histoNameElec = "Track/" + folder + tracker + "/" + categories[
                        tracker][cut]["Electrons"]
                    histoName = histoNameElec + "_" + efficiencyHistoDict[
                        histo]["variable"]
                    print("electrons: " + histoName)
                    eff_elec = {}
                    hist_elec = {}
                    eff_elec, hist_elec = get_eff(eff_elec, hist_elec, tf,
                                                  histoName, label, histo)

                    #draw efficiency of electron
                    canvas_elec = TCanvas(name + "_elec",
                                          canvastitle + "_elec")
                    mg_elec = TMultiGraph()
                    for i, lab in enumerate(label):
                        mg_elec.Add(eff_elec[lab])
                        set_style(eff_elec[lab], colors[i], markers[i],
                                  styles[i])
                        set_style(hist_elec[lab], colors[i] - 10, markers[i],
                                  styles[i])
                    mg_elec.Draw("AP")
                    mg_elec.GetYaxis().SetRangeUser(0, 1.05)
                    mg_elec.GetXaxis().SetTitle(xtitle)
                    mg_elec.GetYaxis().SetTitle("Efficiency")
                    if dist:
                        for lab in label:
                            rightmax = 1.05 * hist_elec[lab].GetMaximum()
                            scale = gPad.GetUymax() / rightmax
                            hist_elec[lab].Scale(scale)
                            hist_elec[lab].Draw("hist SAME")
                            set_style(hist_elec[label[0]], kGray + 1,
                                      markers[i], styles[i])
                    else:
                        rightmax = 1.05 * hist_elec[label[0]].GetMaximum()
                        scale = gPad.GetUymax() / rightmax
                        hist_elec[label[0]].Scale(scale)
                        hist_elec[label[0]].Draw("hist SAME")
                        set_style(hist_elec[label[0]], kBlue - 10, markers[i],
                                  styles[i])

                    canvas_elec.PlaceLegend()
                    for lab in label:
                        eff_elec[lab].Draw("P SAME")
                    latex.DrawLatex(0.7, 0.85, "LHCb simulation")
                    track_name = names[tracker] + " tracks"
                    latex.DrawLatex(0.7, 0.75, track_name)
                    latex.DrawLatex(0.35, 0.3, cutName)
                    canvasName_elec = tracker + "_" + cut + "_" + histo + "_elec.pdf"
                    if savepdf:
                        canvas_elec.SaveAs(canvasName_elec)
                    canvas_elec.SetRightMargin(0.05)
                    canvas_elec.Write()

        # calculate ghost rate
        histoBaseName = "Track/" + folder + tracker + "/"
        for histo in ghostHistos[tracker]:
            trackerDir.cd()
            title = "ghost_rate_vs_" + histo

            gPad.SetTicks()
            histoName = histoBaseName + ghostHistoDict[histo]["variable"]

            ghost = {}
            hist_den = {}
            ghost = get_ghost(ghost, hist_den, tf, histoName, label)
            canvas = TCanvas(title, title)
            mg = TMultiGraph()
            for i, lab in enumerate(label):
                mg.Add(ghost[lab])
                set_style(ghost[lab], colors[i], markers[i], styles[i])

            xtitle = ghostHistoDict[histo]["xTitle"]
            mg.GetXaxis().SetTitle(xtitle)
            mg.GetYaxis().SetTitle("ghost rate")
            mg.Draw("ap")
            for lab in label:
                ghost[lab].Draw("P SAME")
            latex.DrawLatex(0.7, 0.85, "LHCb simulation")
            track_name = names[tracker] + " tracks"
            latex.DrawLatex(0.7, 0.75, track_name)
            #canvas.PlaceLegend()
            if savepdf:
                canvas.SaveAs("ghost_rate_" + histo + ".pdf")
            canvas.Write()

    outputfile.Write()
    outputfile.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    PrCheckerEfficiency(**vars(args))

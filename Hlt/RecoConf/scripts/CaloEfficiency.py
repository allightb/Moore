#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Script for accessing histograms of reconstructible and
# reconstructed ECAL clusters
#
# The efficency is calculated using TEfficiency
# and Bayesian error bars
#
# author: Carla Marin (carla.marin@cern.ch)
# date:   04/2021
# based on PrCheckerEfficiency_HLT1.py
#

import os, sys
import argparse
from ROOT import TCanvas, TFile, TH1F, TLatex
from ROOT import gROOT
from ROOT import TEfficiency


def argument_parser():
    parser = argparse.ArgumentParser(description="location of the tuple file")
    parser.add_argument(
        '--filename',
        type=str,
        default='outputfile_calo_eff_gamma.root',
        help='input file, including path')
    parser.add_argument(
        '--outfile',
        type=str,
        default='efficiency_plots.root',
        help='output file')
    parser.add_argument(
        '--plotstyle',
        default=os.getcwd(),
        help='location of LHCb utils plot style file')
    parser.add_argument(
        '--matchFraction',
        type=float,
        default=0.9,
        help='Matching fraction requirement')
    parser.add_argument(
        '--savepdf',
        default=False,
        action='store_true',
        help='save plots in pdf format')
    parser.add_argument(
        '--areas',
        default=False,
        action='store_true',
        help='split efficiency for ECAL regions')
    return parser


def get_variables(areas=False):
    variables = {
        "cl_e": {
            "xlabel": "Cluster E [MeV]",
            "binning": [30, 0, 1e5],
        },
        "cl_et": {
            "xlabel": "Cluster E_{T} [MeV]",
            "binning": [30, 0, 1e4],
        },
        "cl_x": {
            "xlabel": "Cluster X [mm]",
            "binning": [30, -4000, 4000],
        },
        "cl_y": {
            "xlabel": "Cluster Y [mm]",
            "binning": [30, -3200, 3200],
        },
    }
    # use less bins when splitting by areas
    if areas:
        for v, props in variables.items():
            props["binning"][0] = 10
    return variables


def ClusterEfficiency(filename,
                      outfile,
                      plotstyle,
                      matchFraction=0.9,
                      savepdf=False,
                      areas=False):
    sys.path.append(os.path.expandvars(plotstyle + "/utils/"))
    from LHCbStyle import setLHCbStyle
    setLHCbStyle()
    gROOT.SetBatch(False)

    # read tuple
    fin = TFile.Open(filename)
    t = fin.Get("CaloClusterEff/clusEff")
    fout = TFile.Open(outfile, "recreate")

    vars = get_variables(areas)

    # lhcb label
    latex = TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.05)

    # split by areas
    areas_dict = {0: "all"}
    if areas:
        areas_dict = {0: "Outer", 1: "Middle", 2: "Inner"}

    for var, prop in vars.items():
        canvas = TCanvas("canv_{}".format(var))
        eff_d = {}
        for a_int, a_title in areas_dict.items():
            cut_area = ""
            cut_total = "(f_cl>{})".format(matchFraction)
            if areas:
                cut_area = "(cl_area=={})".format(a_int)
                cut_total += " && {}".format(cut_area)
            # create histos
            h_den = TH1F("h_{}_den_{}".format(var, a_int),
                         "h_{}_den_{}".format(var, a_int), *prop["binning"])
            h_num = TH1F("h_{}_num_{}".format(var, a_int),
                         "h_{}_num_{}".format(var, a_int), *prop["binning"])
            t.Draw("{0}>>h_{0}_den_{1}".format(var, a_int), cut_area)
            t.Draw("{0}>>h_{0}_num_{1}".format(var, a_int), cut_total)

            # compute efficiency
            teff = TEfficiency(h_num, h_den)
            teff.SetStatisticOption(7)
            eff = teff.CreateGraph()
            eff.SetName(a_title)
            eff_d[a_int] = eff

        # draw
        draw_opt = "AP"
        for a_int, eff in eff_d.items():
            eff.GetYaxis().SetRangeUser(0, 1.05)
            eff.GetXaxis().SetTitle(prop["xlabel"])
            eff.GetYaxis().SetTitle("Efficiency")
            eff.SetMarkerColor(a_int + 1)
            eff.SetLineColor(a_int + 1)
            eff.Draw(draw_opt)
            draw_opt = "P"

        if areas:
            canvas.BuildLegend()

        # label
        latex.DrawLatex(0.7, 0.3, "LHCb simulation")

        # save
        if savepdf:
            canvas.SaveAs("eff_{}.pdf".format(var))
        canvas.SetRightMargin(0.1)
        canvas.Write()

    # write and close
    fout.Write()
    fout.Close()


if __name__ == '__main__':
    parser = argument_parser()
    args = parser.parse_args()
    ClusterEfficiency(**vars(args))

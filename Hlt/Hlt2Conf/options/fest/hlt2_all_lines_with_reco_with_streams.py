###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running all HLT2 lines with on-the-fly reconstruction and storing
the output in two streams. HLT2 lines starting with "Topo" go to the sprucing
stream and persistreco is enabled for them. The rest go to the turbo stream.
Created for the June 2021 FEST campaign.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines_with_reco_streams.py
"""
from Moore import options, run_moore
from Moore.streams import Stream, Streams
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines import all_lines
import re

options.evt_max = 200
options.output_file = 'hlt2_all_lines_with_reco.{stream}.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt2_all_lines_with_reco_with_streams.tck.json"
# temporarily use HiveWhiteBoard, see lhcb/LHCb!2878
options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.match(pattern_to_remove, name) is None
    }
    print("Removed lines: ", set(lines_dict) - set(filtered))
    return filtered


def make_streams():
    turbo_lines, spruc_lines = [], []
    for name, builder in hlt2_lines.items():
        if name.startswith("Hlt2Topo"):
            spruc_lines.append(builder(persistreco=True))
        else:
            turbo_lines.append(builder())

    turbo = Stream(
        name="turbo",
        lines=turbo_lines,
        routing_bit=87,
        detectors=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'])

    sprucing = Stream(
        name="sprucing",
        lines=spruc_lines,
        routing_bit=85,
        detectors=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'])

    return Streams(streams=[turbo, sprucing])


# Remove lines which contain jets
pattern_to_remove = "(?i)(hlt2jets)"
hlt2_lines = remove_lines(all_lines, pattern_to_remove)

# Remove lines which require hlt1_filter_code
to_remove = [
    'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
    'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
    'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
    'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
    'Hlt2IFT_Femtoscopy_LambdaLambda', 'Hlt2IFT_Femtoscopy_LambdaLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiP', 'Hlt2IFT_Femtoscopy_XiP_lowK',
    'Hlt2IFT_Femtoscopy_XiLambda', 'Hlt2IFT_Femtoscopy_XiLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiXi', 'Hlt2IFT_Femtoscopy_OmegaP',
    'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP_lowK',
    'Hlt2IFT_Femtoscopy_OmegaLambda', 'Hlt2IFT_Femtoscopy_OmegaXi',
    'Hlt2IFT_Femtoscopy_OmegaOmega', 'Hlt2QEE_DiElectronPrompt_PersistPhotons',
    'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS', 'Hlt2QEE_IncJet10GeV',
    'Hlt2QEE_IncDiJet10GeV', 'Hlt2QEE_IncDiJet15GeV', 'Hlt2QEE_IncDiJet20GeV',
    'Hlt2QEE_IncDiJet25GeV', 'Hlt2QEE_IncDiJet30GeV', 'Hlt2QEE_IncDiJet35GeV'
]
for remove in to_remove:
    hlt2_lines = remove_lines(hlt2_lines, remove)
print("Number of HLT2 lines {}".format(len(hlt2_lines)))

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(
        options, make_streams, public_tools, exclude_incompatible=False)

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from Moore.streams import Stream, Streams
from Moore.config import filter_lines as remove_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

# Select lines for testing purposes
from Hlt2Conf.lines import all_lines


def make_lines():
    return [builder() for builder in all_lines.values()]


def make_streams():
    turbo_lines, spruc_lines = [], []
    for name, builder in hlt2_lines.items():
        if name.startswith("Hlt2Topo"):
            spruc_lines.append(builder(persistreco=True))
        else:
            turbo_lines.append(builder())

    turbo = Stream(
        name="turbo",
        lines=turbo_lines,
        routing_bit=88,
        detectors=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'])

    sprucing = Stream(
        name="sprucing",
        lines=spruc_lines,
        routing_bit=87,
        detectors=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'])

    return Streams(streams=[turbo, sprucing])


options.output_type = 'MDF'
options.output_file = '{stream}_hlt2_all_lines_with_reco_with_streams_mdf.mdf'
options.output_manifest_file = "hlt2_all_lines_with_reco_with_streams_mdf.tck.json"

options.use_iosvc = False
options.event_store = 'HiveWhiteBoard'

hlt2_lines = all_lines

# Remove special lines if they cause problems in this specific test
to_remove = [
    "Hlt2Calib_BeamGas",  # triggers too often at the moment
]
for remove in to_remove:
    hlt2_lines = remove_lines(hlt2_lines, remove)
print("Number of HLT2 lines {}".format(len(hlt2_lines)))

options.lines_maker = make_streams

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(
        options, public_tools=public_tools, exclude_incompatible=False)

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_2or3bodytopo_realtime_dst.dst

HLT2 writes a DST as will be the case for simulation

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_2or3bodytopo_realtime_dst.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line

input_files = [
    # Signal MC B->DsK, 13264031
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000001_1.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 100
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'

options.output_file = 'hlt2_2or3bodytopo_realtime_dst.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "hlt2_2or3bodytopo_realtime_dst.tck.json"

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 2  #4,6
default_ft_decoding_version.global_bind(value=ft_decoding_version)

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)


def make_lines():
    return [twobody_line(persistreco=True), threebody_line(persistreco=True)]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(
        options, make_lines, public_tools, exclude_incompatible=False)

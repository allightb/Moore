###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running all HLT2 standard particle makers from reconstructed data.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_standard_particles.py
"""
from Moore import options, run_moore
from Moore.config import Hlt2Line
from Hlt2Conf import standard_particles
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import upfront_reconstruction

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

options.set_input_and_conds_from_testfiledb('upgrade_2018_BdKstee_LDST')
options.persistreco_version = 0.0
options.input_raw_format = 4.3
options.evt_max = 10

# Get all standard particle makers
makers_to_run = [
    (name, getattr(standard_particles, name))
    for name in dir(standard_particles)
    # Ignore non-particle-maker functions
    if name.startswith('make_')
]


def make_lines():
    lines = []
    for name, maker in makers_to_run:
        lines.append(
            Hlt2Line(
                # Name must start with either Hlt1 or Hlt2
                name="Hlt2" + name,
                algs=upfront_reconstruction() + [maker()],
            ))
    return lines


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)

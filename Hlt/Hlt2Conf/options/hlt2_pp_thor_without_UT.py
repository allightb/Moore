###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a throughput test using ThOr-based selection algorithms.
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.ttrack_selections_reco import make_ttrack_reco
from Hlt2Conf.lines import all_lines
from DDDB.CheckDD4Hep import UseDD4Hep
import re


def remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


## explicitly remove tracking efficiency lines, as they call the reconstruction in the subdetectors
to_remove = [
    "Hlt2TrackEff_DiMuon_.*", "Hlt2TrackEff_ZToMuMu_.*",
    "Hlt2IFT_SMOG2KshortVeloLong.*", "Hlt2IFT_SMOG2TrackEff_DiMuon_.*"
]

## removing lines that require hlt1_filter_code
to_remove += [
    'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
    'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
    'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
    'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
    'Hlt2IFT_Femtoscopy_LambdaLambda', 'Hlt2IFT_Femtoscopy_LambdaLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiP', 'Hlt2IFT_Femtoscopy_XiP_lowK',
    'Hlt2IFT_Femtoscopy_XiLambda', 'Hlt2IFT_Femtoscopy_XiLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiXi', 'Hlt2IFT_Femtoscopy_OmegaP',
    'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP_lowK',
    'Hlt2IFT_Femtoscopy_OmegaLambda', 'Hlt2IFT_Femtoscopy_OmegaXi',
    'Hlt2IFT_Femtoscopy_OmegaOmega', 'Hlt2QEE_DiElectronPrompt_PersistPhotons',
    'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS', 'Hlt2QEE_IncJet10GeV',
    'Hlt2QEE_IncDiJet10GeV', 'Hlt2QEE_IncDiJet15GeV', 'Hlt2QEE_IncDiJet20GeV',
    'Hlt2QEE_IncDiJet25GeV', 'Hlt2QEE_IncDiJet30GeV', 'Hlt2QEE_IncDiJet35GeV'
]
trunc_lines = all_lines

for remove in to_remove:
    trunc_lines = remove_lines(trunc_lines, remove)

print("Removed lines: ", all_lines.keys() - trunc_lines.keys())


def make_lines():
    return [builder() for builder in trunc_lines.values()]


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]

options.scheduler_legacy_mode = False

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = [
        '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal',
        'Muon'
    ]


with reconstruction.bind(from_file=False),\
     make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_ttrack_reco.bind(skipCalo=False),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):
    config = run_moore(options, make_lines, public_tools)

###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on Turbo-stream output of Moore/Hlt/Hlt2Conf/options/hlt2_pp_expected_24_without_UT.py
Please note that that example does not run Turbo lines by default - change it for your use case.

.dst output can be tupled with FunTuple.

If you copy this code to a file with path
    Moore/sprucepass_pp_expected_24.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/sprucepass_pp_expected_24.py
"""
from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from Moore.streams import DETECTORS, Stream, Streams
from Moore.lines import PassLine

# Input-specific options
options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.input_files = ['hlt2_output__turbo.mdf']
options.input_type = 'MDF'
options.input_raw_format = 0.5
options.simulation = True
options.data_type = "Upgrade"

# Output options
options.output_file = 'spruce_pass_output__{stream}.dst'
options.output_type = 'ROOT'

# Misc options
options.scheduler_legacy_mode = False
options.input_process = 'Hlt2'
options.evt_max = -1

WG = 'qee'


def make_streams():
    # adjust hlt2_filter_code according to WG of choice
    # example here corresponds to output of QEE HLT2 turbo lines
    # See Hlt/Hlt2Conf/python/Hlt2Conf/sprucing_settings/Sprucing_2023_2_production.py for inspiration
    # detectors will only be persisted if already saved by HLT2
    streams = [
        Stream(
            WG,
            lines=[
                PassLine(
                    name=f"Pass_{WG}", hlt2_filter_code="Hlt2QEE.*Decision")
            ],
            detectors=DETECTORS)
    ]
    return Streams(streams=streams)


with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_streams, public_tools=[])

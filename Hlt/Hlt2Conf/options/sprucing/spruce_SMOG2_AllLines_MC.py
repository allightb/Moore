###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.reco_objects_for_spruce import reconstruction as reco_spruce, upfront_reconstruction as upfront_spruce

from Hlt2Conf.lines.ift import sprucing_lines

input_files = [
    #'/data/DATA/data.lhcb/boente/run3/moore_output/hlt2/hlt2_SMOG2_testlines_MB_pAr_pp_SMOG2_expected2023_VELO48mm_chenxi_FullTestPerformance2023_evt_200000_withoutUT_patPV3D.dst'
    '/data/DATA/data.lhcb/boente/run3/moore_output/hlt2/hlt2_SMOG2_testlines_MB_pAr_pp_SMOG2_2022_KSTARtestNoPVprescale_evt_100000_withoutUT_patPV3D.dst'
]

options.input_raw_format = 0.3  #dst input
options.input_process = 'Hlt2'
options.input_files = input_files
#options.input_manifest_file = "/data/DATA/data.lhcb/boente/run3/moore_output/hlt2/jsons/hlt2_SMOG2_testlines_MB_pAr_pp_SMOG2_expected2023_VELO48mm_chenxi_FullTestPerformance2023_evt_200000_withoutUT_patPV3Dmy_hlt2.tck.json"
options.input_manifest_file = '/data/DATA/data.lhcb/boente/run3/moore_output/hlt2/jsons/hlt2_SMOG2_testlines_MB_pAr_pp_SMOG2_2022_KSTARtestNoPVprescale_evt_100000_withoutUT_patPV3Dmy_hlt2.tck.json'
options.input_type = 'ROOT'

options.evt_max = -1

options.output_file = 'spruce_pAr_pp_TestKstarLine_MC_KStarTestNoPVPrescale_100000MBEvents_noUT.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_pAr_pp_TestKstarLine_MC_KStarTestNoPVPrescale_100000MBEvents_noUT.tck.json"

#================================
# Data tags
#================================

#dddb_tag and conddb_tag are for DetDesc build only (MC)
options.simulation = True
options.dddb_tag = 'dddb-20221004'
options.conddb_tag = 'sim-20221220-vc-md100'

#2023 conditions
#options.dddb_tag = 'dddb-20230313'
#options.conddb_tag = 'sim-20230626-vc-md100'

#options.event_store = 'EvtStoreSvc'


def make_lines():
    return [builder() for builder in sprucing_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
        from_file=True, spruce=True), reco_spruce.bind(
            simulation=True), upfront_spruce.bind(simulation=True):
    config = run_moore(options, make_lines, public_tools)

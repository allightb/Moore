###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test HLT2 filters on Sprucing lines. Runs on `hlt2_2or3bodytopo_realtime.mdf` from `hlt2_2or3bodytopo_realtime.py`

Run like any other options file:

    ./Moore/run gaudirun.py spruce_hlt2filter.py

"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Moore.lines import SpruceLine
from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines.b_to_open_charm import b_to_dh

input_files = ['hlt2_2or3bodytopo_realtime.mdf']

options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.input_files = input_files
options.input_manifest_file = "hlt2_2or3bodytopo_realtime.tck.json"
options.input_type = 'MDF'

options.evt_max = -1
options.simulation = True
options.dddb_tag = 'dddb-20180815'
options.conddb_tag = 'sim-20180530-vc-md100'

options.output_file = 'spruce_HLT2filter.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_HLT2filter.tck.json"


def Sprucing_line_1(name='Spruce_filteronTopo2'):
    line_alg = b_to_dh.make_BdToDsmK_DsmToKpKmPim(process='spruce')
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        #hlt1_filter_code="HLT_PASS_RE('Hlt1GECPassthroughDecision')",
        hlt2_filter_code="Hlt2Topo2BodyDecision")


def Sprucing_line_2(name='Spruce_filteronTopo3'):
    line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_FEST(process='spruce')
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        #hlt1_filter_code="HLT_PASS_RE('Hlt1PassthroughDecision')",
        hlt2_filter_code="Hlt2Topo3BodyDecision")


def make_lines():
    return [Sprucing_line_1(), Sprucing_line_2()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_lines, public_tools)

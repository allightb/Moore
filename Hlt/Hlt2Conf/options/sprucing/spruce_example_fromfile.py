###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running second exclusive hlt2 line on output of topo{2,3} persistreco hlt2 lines (original reco from file). Produces spruce_fromfilereco.dst

Run like any other options file:

    ./Moore/run gaudirun.py spruce_b2oc_example_fromfile.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.test.spruce_test import Test_sprucing_line

input_files = ['hlt2_2or3bodytopo_fromfile.mdf']

options.input_raw_format = 0.3
options.input_process = 'Hlt2'
options.input_files = input_files
options.input_manifest_file = "hlt2_2or3bodytopo_fromfile.tck.json"
options.input_type = 'MDF'

# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 100
options.simulation = True
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'spruce_fromfilereco.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_example_fromfile.tck.json"


def make_lines():
    return [
        Test_sprucing_line(name="Spruce_Test_line"),
    ]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_lines, public_tools)

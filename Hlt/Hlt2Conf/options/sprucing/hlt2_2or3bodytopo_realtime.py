###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Use this script to update the samples in /eos/lhcb/wg/rta/samples/mc/Hlt1Hlt2filtered_MinBias_sprucing

Update .mdf file AND the tck

This script runs the TOPO lines over HLT1 filtered Min Bias events

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_2or3bodytopo_realtime.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line
from RecoConf.decoders import default_ft_decoding_version

input_files = [
    # https://gitlab.cern.ch/lhcb-b2oc/b2ocupgrade/-/blob/master/Moore_input_files/minbias_hlt1filt.ldst
    # has a large number of hlt1 filtered min bias input files (>100) in a "ready to plug" format, here
    # only the first 5 are listed
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000001_1.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000002_1.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000003_1.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000004_1.ldst',
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000005_1.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3

options.evt_max = 5000
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'hlt2_2or3bodytopo_realtime_newPacking_newDst.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt2_2or3bodytopo_realtime_newPacking_newDst.tck.json"

ft_decoding_version = 2  #4,6
default_ft_decoding_version.global_bind(value=ft_decoding_version)


def make_lines():
    return [twobody_line(persistreco=True), threebody_line(persistreco=True)]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(
        options, make_lines, public_tools, exclude_incompatible=False)

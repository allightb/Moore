###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines from reconstructed data and the raw event.

Identical to `hlt2_lines_reco_mix.py`, except that these options enable
saving the full MC event, not just the subset that was involved in the
positive trigger decision.
"""
import json

from Moore import options, run_moore
from Moore import persistence
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.test.hlt2_test import dzero2kpi_line

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.persistreco_version = 0.0
options.input_raw_format = 4.3
options.evt_max = 200

options.output_file = 'hlt2_lines_reco_mix_full_mc.dst'
options.output_type = 'ROOT'


def make_lines():
    lines = []
    # Instantiate lines taking the reconstruction from the file
    with reconstruction.bind(from_file=True):
        lines += [dzero2kpi_line()]
    # Instantiate the same lines, but running the reconstruction on demand
    with reconstruction.bind(from_file=False):
        for default_name, builder in dzero2kpi_line():
            # Lines must have unique names
            line = builder(name=default_name + "RecoFromRaw")
            lines.append(line)

    # Dump the configuration so we can read it back in downstream tests
    with open("hlt2_lines_reco_mix_full_mc.lines.json", "w") as outfile:
        d = [
            dict(
                name=l.name,
                extra_outputs=[eo_name for eo_name, _ in l.extra_outputs])
            for l in lines
        ]
        json.dump(d, outfile)
    return lines


public_tools = [stateProvider_with_simplified_geom()]
with persistence.persist_line_outputs.bind(clone_mc=False):
    run_moore(options, make_lines, public_tools)

###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run the QEE sprucing lines on output of di-bjet HLT2 lines. Produces qee_bjet_spruce.dst

Run after hlt2_bJets_example.py

Run like any other options file:

    ./Moore/run gaudirun.py spruce_qee_bbbar.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from Hlt2Conf.lines.qee import sprucing_lines
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

input_files = ['hlt2_qee_bjet.mdf']

options.input_raw_format = 0.5
options.input_process = 'Hlt2'
options.input_files = input_files
options.input_type = 'MDF'
options.input_manifest_file = "hlt2_qee_bjet.tck.json"
options.evt_max = -1
options.simulation = True
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'

options.output_file = 'qee_bjet_spruce.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "qee_bjet_spruce.tck.json"


def make_lines():
    return [line() for line in sprucing_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
        from_file=True, spruce=True), list_of_full_stream_lines.bind(lines=[]):
    config = run_moore(options, make_lines, public_tools)

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a test if a dst or mdf file contains particles for all fired trigger lines.

Runs over the output of the hlt2_all_lines test, manually running the unpackers
and DecReports decoder, and checks per event that each firing trigger line has
a non-zero number of Particle objects in the expected location.

When using this script *outside of the QMTests*, please note that it's expected
all lines to have produced particles. When running over an MC production,
this means that one needs to take care of skipping over 
the 'Hlt2MCPassThroughLine'.

Takes command-line arguments:

    python hlt2_check_output.py <DST or MDF file>
"""
import sys

import GaudiPython as GP
from GaudiConf import IOHelper
from Configurables import (ApplicationMgr, LHCbApp, IODataManager)

from GaudiConf.reading import do_unpacking
from PyConf.application import configured_ann_svc


def error(msg):
    print("Hlt2CheckOutput ERROR: ", msg)


def warning(msg):
    print("Hlt2CheckOutput WARNING: ", msg)


LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
algs = do_unpacking(input_process='Hlt2')

decdecoder = None
for alg in algs:
    for m in getattr(alg, "Members",
                     []):  # deal with sequencers (but only one layer of them)
        if "HltDecReportsDecoder" in m.getFullName():
            decdecoder = m
    if "HltDecReportsDecoder" in alg.getFullName():
        decdecoder = alg
assert decdecoder

appmgr = ApplicationMgr(TopAlg=algs)
appmgr.ExtSvc += [configured_ann_svc()]

input_file = sys.argv[1]
input_type = "ROOT" if input_file.find(".dst") != -1 else "RAW"
IOHelper(input_type).inputFiles([input_file], clear=True)

stream = None
if input_file.startswith('sprucing'): stream = 'Sprucing'
if input_file.startswith('turbo'): stream = 'Turbo'
if not stream:
    warning(f'could not determine stream type from file name: {input_file}')

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True
#this is not used anymore, so commented out for now

# Flag to record whether we saw any events
# The test can't check anything if no event fired
found_events = False
found_child_relations = False
while TES['/Event']:
    print('Checking next event.')

    #TES.dump()
    decRep = TES[str(decdecoder.OutputHltDecReportsLocation)].decReports()

    for name, report in decRep.items():
        # for _these_ tests: Hlt2Topo is in 'Spruce' and the rest in 'Turbo' _if_ (and only if) we could recognize the stream
        # TODO: could also check that the particles are _not_ present when in the 'other' stream(s)...
        if stream == 'Sprucing' and not name.startswith("Hlt2Topo"): continue
        if stream == 'Turbo' and name.startswith("Hlt2Topo"): continue
        if report.decision():
            print('Checking line {}'.format(name))
            prefix = '/Event/HLT2/{}'.format(name[:-len("Decision")])
            container = TES[prefix + '/Particles']
            if not container:
                error("no Particles container in " + prefix + "/Particles")
            if container.size() == 0:
                error("empty Particles container in " + prefix + "/Particles")

            found_events = True
    appMgr.run(1)

if not found_events:
    error('ERROR: no positive decisions found')

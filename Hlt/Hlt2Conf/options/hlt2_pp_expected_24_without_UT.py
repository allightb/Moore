###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on output of Moore/Hlt/Hlt1Conf/options/hlt1_pp_expected_24_without_UT.py
Configured for full-stream lines. Can also add turbo lines as shown (commented out).
Output should be spruced before being tupled.

If you copy this code to a file with path
    Moore/hlt2_pp_expected_24_without_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt2_pp_expected_24_without_UT.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    make_TrackBestTrackCreator_tracks,
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_PrKalmanFilter_Seed_tracks,
)
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.event_filters import require_gec
from Moore.streams import Stream, Streams
from Hlt2Conf.lines.semileptonic import all_lines as full_lines  # all full-stream lines
import sys

# Input-specific options
options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.input_files = ['hlt1_output.mdf']
options.input_type = 'MDF'
options.input_raw_format = 0.5
options.simulation = True
options.data_type = "Upgrade"

# Output options
options.output_file = 'hlt2_output__{stream}.mdf'
options.output_type = 'MDF'

# Misc options
options.scheduler_legacy_mode = False
options.n_threads = 1  # can be multi-threaded
options.evt_max = -1


def make_streams():
    streams = [
        Stream(
            "full",
            lines=[builder() for builder in full_lines.values()],
            routing_bit=98,
            detectors=[]
            # detectors=DETECTORS - import from Moore.streams
        ),  # Turbo or Full case - no detector raw banks, for TurCal add them as above.
        # Add turbo lines as well if you like:
        # Would need to import e.g. from Hlt2Conf.lines.b2oc import all_lines as turbo_lines
        # Stream(
        #    "turbo",
        #    lines=[builder() for builder in turbo_lines.values()],
        #    routing_bit=99,
        #    detectors=[]
        # )
    ]
    return Streams(streams=streams)


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
# NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
with reconstruction.bind(from_file=False), hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
    require_gec.bind(skipUT=True),\
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.7, max_chi2ndof=sys.float_info.max),\
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.),\
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.),\
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.):
    config = run_moore(options, make_streams, public_tools)

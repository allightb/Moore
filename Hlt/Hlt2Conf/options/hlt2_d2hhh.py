###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_d2hhh.py
"""
from Moore import options, run_moore

from Hlt2Conf.lines.charm.d_to_hhh import all_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

# TODO stateProvider_with_simplified_geom must go away from option files

input_files = [
    # MinBias 30000000
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
    # 'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
    # D*-tagged D0 to KK, 27163002
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/27163002/LDST
    #'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000012_2.ldst'
    # D+->K-K+pi+
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/21163000/LDST
    #'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070313/0000/00070313_00000003_2.ldst',
    #'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070313/0000/00070313_00000007_2.ldst',

    # D+->K-K+pi+ Hlt1 filtered
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/21163000/LDST
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod//lhcb/MC/Upgrade/LDST/00076732/0000/00076732_00000001_1.ldst',
    #'root://eoslhcb.cern.ch//eos/lhcb/grid/prod//lhcb/MC/Upgrade/LDST/00076732/0000/00076732_00000002_1.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = -1
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-mu100'

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 2
default_ft_decoding_version.global_bind(value=ft_decoding_version)


def make_lines():
    return [builder() for builder in all_lines.values()]


options.lines_maker = make_lines

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, public_tools=public_tools)

###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example option on how to test on any specific Sprucing line in B2OC.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_spruce_example.py

To run on a specific line that is booked by unique function (in spruce_b2oc.py):

from Hlt2Conf.lines.b_to_open_charm.spruce_b2oc import BdToDsmK_DsmToKpKmPim
sprucing_lines = {}
from Hlt2Conf.lines.b_to_open_charm.spruce_b2oc import make_sprucing_lines
default_lines = [
    'BdToDsmK_DsmToKpKmPim',
]
extra_config = {
    'flavour_tagging': [
        'BdToDsmK_DsmToKpKmPim',
    ]
}
make_sprucing_lines(
    line_dict=all_lines,
    all_lines=default_lines,
    extra_config=extra_config)

# To use input data from /eos locations:
input_files = [
   # output of Hlt2 full stream as of December 2023
   'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/hlt2_full_stream_Dec2023/MagDown/hlt2_full_stream_0.mdf',
]
options.input_files = input_files
options.simulation = True
options.input_type = "MDF"
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from PyConf.application import metainfo_repos

###############################################################################
# configure which lines to test
###############################################################################

# Run on ALL B2OC lines:
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines

###############################################################################
# configure input data set
###############################################################################

# Configure input data from testfiledb
options.set_input_and_conds_from_testfiledb(
    'upgrade-minbias-hlt2-full-output-Dec2023')

metainfo_repos.global_bind(extra_central_tags=['key-7e074b7d'])
options.input_process = 'Hlt2'
options.input_raw_format = 0.3

# Set a reasonable number of events
options.evt_max = 100

###############################################################################
# configure output files
###############################################################################

# Write the output file
options.output_file = 'spruce_b2oc_lines.dst'
options.output_type = 'ROOT'
options.output_manifest_file = "spruce_b2oc_lines.tck.json"
# Hlt2 monitoring histos
options.histo_file = 'spruce_b2oc_monitoring.root'


def make_lines():
    lines = [builder() for builder in sprucing_lines.values()]
    return lines


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(
        options, make_lines, public_tools, exclude_incompatible=True)

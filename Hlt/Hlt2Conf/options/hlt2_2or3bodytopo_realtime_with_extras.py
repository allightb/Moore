###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_2or3bodytopo_realtime.mdf

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_2or3bodytopo_realtime_with_extras.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf, make_light_reco_pr_kf_without_UT

from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (upfront_reconstruction)

from Hlt2Conf.lines.topological_b import make_filtered_topo_twobody, make_filtered_topo_threebody, line_prefilters

input_files = [
    # Signal MC B->DsK, 13264031
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000001_1.ldst'
]


def twobody_with_extras_line(name="Hlt2_Topo2Body_with_extras",
                             prescale=1,
                             persistreco=True):
    candidates = make_filtered_topo_twobody(MVACut=0.992)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=persistreco,
    )


def threebody_with_extras_line(name="Hlt2_Topo3Body_with_extras",
                               prescale=1,
                               persistreco=True):
    candidates = make_filtered_topo_threebody(MVACut=0.999)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=persistreco,
    )


def make_lines():
    return [
        twobody_with_extras_line(persistreco=True),
        threebody_with_extras_line(persistreco=True)
    ]


options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 100
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'
options.geometry_version = 'run3/trunk'
options.conditions_version = 'master'

options.output_file = 'hlt2_2or3bodytopo_realtime_with_extras.mdf'
options.output_type = 'MDF'
options.output_manifest_file = "hlt2_2or3bodytopo_realtime_with_extras.tck.json"

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 2  #4,6
default_ft_decoding_version.global_bind(value=ft_decoding_version)

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

skipUT = False
my_reco = make_light_reco_pr_kf_without_UT if skipUT else make_light_reco_pr_kf

public_tools = [stateProvider_with_simplified_geom()]
with hlt2_reconstruction.bind(
        make_reconstruction=my_reco), reconstruction.bind(from_file=False):
    config = run_moore(
        options, make_lines, public_tools, exclude_incompatible=False)

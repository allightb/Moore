###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.test.spruce_test import Test_sprucing_line, Test_extraoutputs_sprucing_line, Test_persistreco_sprucing_line, Test_sprucing_rawbank_sp_line
from Hlt2Conf.lines.b_to_open_charm import b_to_dh
from Moore.lines import PassLine, SpruceLine
from Moore.persistence import persist_line_outputs
from PyConf.application import metainfo_repos
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

from Moore.streams import Stream, Streams, DETECTORS

public_tools = [stateProvider_with_simplified_geom()]


def spruce_streaming(options: Options):
    """
    Test streaming of sprucing lines. Produces two spruce_streaming.dst files prepended by stream name. RawBankSP functionality is tested via stream B.
    """

    def make_streams():
        stream_A = Stream(
            name="test_stream_A",
            lines=[Test_sprucing_line(name="Spruce_test_stream_A_line")],
            detectors=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'])

        stream_B = Stream(
            name="test_stream_B",
            lines=[
                Test_sprucing_rawbank_sp_line(name="Spruce_test_stream_B_line")
            ],
            detectors=['VP', 'UT', 'Muon', 'Calo'])

        return Streams(streams=[stream_A, stream_B])

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_all_lines_realtime(options: Options):
    """Test running all Sprucing lines

    Run over HLT1 filtered Min bias sample that has been processed by HLT2 lines.
    and then in the readback application  configure the GitANNSvc to use the branch key-ac5f2a28
    """

    metainfo_repos.global_bind(extra_central_tags=['key-ac5f2a28'])

    def make_lines():
        return [builder() for builder in sprucing_lines.values()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime(options: Options):
    """
    Test running Sprucing line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_example_realtimereco.dst
    """

    def make_streams():
        return Streams(streams=[
            Stream(
                name="test",
                lines=[Test_sprucing_line(name="Spruce_Test_line")],
                detectors=["Rich"])
        ])

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_sp_passthrough(options: Options):
    """Test pass-through Sprucing line. Produces spruce_passthrough.dst
    Run like any other options file:
    ./Moore/run gaudirun.py spruce_passthrough.py
    """

    def make_streams():
        return Streams(streams=[
            Stream(
                name="test",
                lines=[
                    PassLine(
                        "Passthrough",
                        hlt2_filter_code='Hlt2_test_stream_B_lineDecision')
                ],
                detectors=DETECTORS)
        ])

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_passthrough(options: Options):
    """Test pass-through Sprucing line. Produces spruce_passthrough.dst
    Run like any other options file:
    ./Moore/run gaudirun.py spruce_passthrough.py
    """

    def pass_through_line(name="Passthrough"):
        """Return a Sprucing line that performs no selection
        """
        return PassLine(name=name)

    def make_lines():
        return [pass_through_line()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_hlt2filter(options: Options):
    """Test HLT2 filters on Sprucing lines. Runs on `hlt2_2or3bodytopo_realtime.mdf` from `hlt2_2or3bodytopo_realtime.py`

    Run like any other options file:

    ./Moore/run gaudirun.py spruce_hlt2filter.py

    """

    def Sprucing_line_1(name='Spruce_filteronTopo2'):
        line_alg = b_to_dh.make_BdToDsmK_DsmToKpKmPim(process='spruce')
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            #hlt1_filter_code="HLT_PASS_RE('Hlt1GECPassthroughDecision')",
            hlt2_filter_code="Hlt2Topo2BodyDecision")

    def Sprucing_line_2(name='Spruce_filteronTopo3'):
        line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_FEST(process='spruce')
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            #hlt1_filter_code="HLT_PASS_RE('Hlt1PassthroughDecision')",
            hlt2_filter_code="Hlt2Topo3BodyDecision")

    def make_lines():
        return [Sprucing_line_1(), Sprucing_line_2()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_extraoutputs(options: Options):
    """
    Test Sprucing line with `extra_outputs` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_extraoutputs.dst
    """

    def make_lines():
        return [
            Test_extraoutputs_sprucing_line(
                name="Spruce_Test_line_extraoutputs"),
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_persistreco(options: Options):
    """Test Sprucing line with `persistreco` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_persistreco.dst
    """

    def make_lines():
        return [
            Test_persistreco_sprucing_line(name="Spruce_Test_line_persistreco")
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_all_lines_realtime_test_old_json(options: Options):
    """Test running all Sprucing lines on topo{2,3} persistreco hlt2 output (use real time reco). Produces spruce_all_lines_realtimereco_newPacking.dst

    Use this for rate tests

    Run like any other options file:

    ./Moore/run gaudirun.py spruce_all_lines_realtime.py
    """

    def make_lines():
        return [builder() for builder in sprucing_lines.values()]

    ##Old file has the "Line" suffix on lines
    with persist_line_outputs.bind(
            allow_missing_containers=True), list_of_full_stream_lines.bind(
                lines=["Hlt2Topo2BodyLine", "Hlt2Topo3BodyLine"]):
        return run_moore(options, make_lines, public_tools)

###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.packing import persistreco_writing_version

from Moore.lines import Hlt2Line
from Moore.config import has_global_event_cut
from RecoConf.event_filters import require_gec
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.monitoring.data_quality_PbPb import all_lines as monitoring_lines


def _pbpb_prefilters():
    return [require_gec(cut=30_000, skipUT=True)]


def _hlt2_PbPb_hadronic_line(name='Hlt2PbPb_Hadronic', prescale=1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=
        r"^Hlt1HeavyIon(PbPbPeripheral|PbSMOGHadronic|PbPbCentral)Decision",
        persistreco=True,
        prescale=prescale,
    )


def _hlt2_PbPb_inclusive_line(name='Hlt2PbPb_Inclusive', prescale=1):
    """
      A line that would require "everything but Hlt2PbPb_Hadronic" would remove only
      pathological events, since in almost all cases another Hlt1 line fires as well.
      Hence this line is fully inclusive.
    """
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=r"^Hlt1(?!ODINLumi).*Decision",
        persistreco=True,
        prescale=prescale,
    )


def _make_lines_and_require_gec(*lines_dicts):
    all_lines = []
    for lines in lines_dicts:
        for linef in lines.values():
            line = linef()
            node = line.node
            if has_global_event_cut(node):
                all_lines.append(line)
            else:
                from logging import getLogger
                getLogger(__name__).warning(
                    f"Line {node.name} has no GEC and will not be processed.")
    return all_lines


def _make_streams():
    lumi_line = lumi_nanofy_line()

    ion_lines = (_make_lines_and_require_gec(monitoring_lines) +
                 [_hlt2_PbPb_inclusive_line(), lumi_line])
    ionraw_lines = [_hlt2_PbPb_hadronic_line(), lumi_line]
    lumi_lines = [b() for b in lumi_calibration_lines.values()]

    return dict(
        ion=ion_lines,
        ionraw=ionraw_lines,
        lumi=lumi_lines,
    )


def make_streams(real_make_streams=_make_streams):
    from PyConf.Algorithms import PrHybridSeeding
    from RecoConf.legacy_rec_hlt1_tracking import (make_reco_pvs,
                                                   make_PatPV3DFuture_pvs)
    from RecoConf.hlt2_tracking import (
        make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
        make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks,
        get_UpgradeGhostId_tool_no_UT)
    from RecoConf.hlt2_global_reco import (reconstruction,
                                           make_fastest_reconstruction)

    with make_fastest_reconstruction.bind(skipUT=True),\
    reconstruction.bind(make_reconstruction=make_fastest_reconstruction),\
    PrHybridSeeding.bind(RemoveBeamHole=True, RemoveClones_forLead=True),\
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
    get_UpgradeGhostId_tool_no_UT.bind(for_PbPb=True),\
    make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.7),\
    make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
    persistreco_writing_version.bind(version=1.1):
        return real_make_streams()

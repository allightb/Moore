###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the first data with HLT2.
"""
from Moore.streams import Stream, Streams
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import all_lines as inclusive_detached_dilepton_lines
from Hlt2Conf.lines.pid import all_lines as pid_lines
from Hlt2Conf.lines.trackeff import turcal_lines as trackeff_turcal_lines
from Hlt2Conf.lines.trackeff import turbo_lines as trackeff_turbo_lines
from Hlt2Conf.lines.monitoring import all_lines as monitoring_lines
from Hlt2Conf.lines.b_to_open_charm import all_lines as b_to_open_charm_lines
from Hlt2Conf.lines.b_to_open_charm import all_calib_lines as b_to_open_charm_calib_lines
from Hlt2Conf.lines.rd import full_lines as rd_full_lines
from Hlt2Conf.lines.rd import turbo_lines as rd_turbo_lines
from Hlt2Conf.lines.bandq import hlt2_full_lines as bandq_full_lines
from Hlt2Conf.lines.bandq import hlt2_turbo_lines as bandq_turbo_lines
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.qee import hlt2_turbo_lines as qee_turbo_lines
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.b_to_charmonia import all_lines as b_to_charmonia_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.charmonium_to_dimuon import full_lines as charmonium_to_dimuon_prompt_full_lines
from Hlt2Conf.lines.charmonium_to_dimuon import turbo_lines as charmonium_to_dimuon_prompt_turbo_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines
from Hlt2Conf.lines.bnoc import hlt2_lines as bnoc_turbo_lines
from Hlt2Conf.lines.bnoc import full_lines as bnoc_full_lines
from Hlt2Conf.lines.ift import ift_full_lines
from Hlt2Conf.lines.ift import ift_turbo_lines

from Hlt2Conf.lines.commissioning import (
    _lc_to_pkpi_line,
    _diphoton_line,
    _kshort_ll_line,
    _kshort_ll_tos_line,
    _phi_to_kk_detached_line,
    _lambda_ll_line,
    _lambda_ll_tos_line,
    _d0_to_kpi_line,
    _dp_to_kpipi_line,
    _dp_to_kpipi_tos_line,
)

import re


def _remove_lines(lines_dict, pattern_to_remove):
    filtered = {
        name: line
        for name, line in lines_dict.items()
        if re.search(pattern_to_remove, name) is None
    }
    return filtered


## for removing lines that are incompatible with DD4HEP
to_remove = [
    "Hlt2TrackEff_DiMuon_Downstream",
    "Hlt2TrackEff_DiMuon_MuonUT",
    "Hlt2TrackEff_ZToMuMu_Downstream",
    "Hlt2TrackEff_ZToMuMu_MuonUT",
]


def _make_lines(*lines_dicts):
    all_lines = []
    for lines in lines_dicts:
        trunc_lines = lines
        for remove in to_remove:
            trunc_lines = _remove_lines(trunc_lines, remove)
        if len(lines) != len(trunc_lines):
            print("Manually removed lines due to DD4HEP: ",
                  lines.keys() - trunc_lines.keys())
        all_lines += [builder() for builder in trunc_lines.values()]
    return all_lines


def _make_streams():
    lumi_line = lumi_nanofy_line()

    full_lines = _make_lines(
        topological_b_lines,
        inclusive_detached_dilepton_lines,
        charmonium_to_dimuon_detached_lines,
        charmonium_to_dimuon_prompt_full_lines,
        b_to_open_charm_calib_lines,
        rd_full_lines,
        qee_full_lines,
        semileptonic_lines,
        ift_full_lines,
        bnoc_full_lines,
        bandq_full_lines,
    ) + [lumi_line]
    turbo_lines = _make_lines(
        b_to_open_charm_lines,
        rd_turbo_lines,
        bandq_turbo_lines,
        qee_turbo_lines,
        charm_lines,
        b_to_charmonia_lines,
        charmonium_to_dimuon_prompt_turbo_lines,
        bnoc_turbo_lines,
        ift_turbo_lines,
        trackeff_turbo_lines,
    ) + [lumi_line]
    turcal_lines = _make_lines(
        pid_lines,
        trackeff_turcal_lines,
        monitoring_lines,
    ) + [
        _diphoton_line(),
        _kshort_ll_line(),
        _kshort_ll_tos_line(),
        _phi_to_kk_detached_line(),
        _lambda_ll_line(),
        _lambda_ll_tos_line(),
        _d0_to_kpi_line(),
        _dp_to_kpipi_line(),
        _dp_to_kpipi_tos_line(),
        _lc_to_pkpi_line(),
        lumi_line,
    ]
    lumi_lines = _make_lines(lumi_calibration_lines)

    # make sure persistreco is true for full and turcal lines
    problems = []
    for l in full_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the FULL stream" +
                            "and must have persistreco=True")
    for l in turcal_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the TURCAL stream" +
                            "and must have persistreco=True")
    if problems:
        raise RuntimeError("Misconfigured lines found:\n" +
                           "\n".join(problems))

    return Streams(streams=[
        Stream("full", lines=full_lines, detectors=[]),
        Stream("turboraw", lines=turbo_lines),
        Stream("turbo", lines=turbo_lines, detectors=[]),
        Stream("turcal", lines=turcal_lines),
        Stream("lumi", lines=lumi_lines)
    ])


def make_streams(real_make_streams=_make_streams):

    from RecoConf.decoders import default_VeloCluster_source
    from RecoConf.legacy_rec_hlt1_tracking import (
        make_VeloClusterTrackingSIMD, make_reco_pvs, make_PatPV3DFuture_pvs)
    from RecoConf.hlt2_tracking import (
        make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
        make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks)
    # Workaround to enable running of Tracking efficiency lines using special muon reconstruction
    from PyConf.Tools import TrackMasterFitter

    with make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
        make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        make_PatPV3DFuture_pvs.bind(velo_open=True),\
        TrackMasterFitter.bind(FastMaterialApproximation=True):

        return real_make_streams()

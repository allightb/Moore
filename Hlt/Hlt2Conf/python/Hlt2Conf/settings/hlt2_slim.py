###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the first data with HLT2.
"""
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm)

from Moore.streams import Stream, Streams
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import upfront_reconstruction
from Hlt2Conf.lines.topological_b import threebody_line, twobody_line
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.standard_particles import (make_long_pions, make_long_protons,
                                         make_photons)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
import Functors as F


def hlt2_track_mva_line(name='Hlt2Commissioning_TrackMVAPassThrough',
                        prescale=1):
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1TrackMVADecision",
        persistreco=True,
        prescale=prescale)


def _filter_long_pions():
    return ParticleFilter(make_long_pions(),
                          F.FILTER(F.require_all(F.PT > 100 * MeV)))


def _filter_long_protons():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(F.require_all(F.PT > 400 * MeV, F.P > 9 * GeV)))


def _filter_photons():
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(F.PT > 200 * MeV,
                          F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.85,
                          F.IS_PHOTON > 0.7, F.IS_NOT_H > 0.4)))


def diphoton_line(name='Hlt2Commissioning_DiPhoton', prescale=1):
    diphoton = ParticleCombiner(
        name="Commissioning_DiPhoton_Combiner",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(0 * MeV, F.MASS, 1200 * MeV), F.PT > 2 * GeV),
        CompositeCut=F.ALL,
    )
    return Hlt2Line(
        name=name, algs=[diphoton], persistreco=True, prescale=prescale)


def kshort_ll_line(name="Hlt2Commissioning_KsToPimPip_LL"):
    kshorts = ParticleCombiner(
        [_filter_long_pions(), _filter_long_pions()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Ks_LL",
        CombinationCut=F.require_all(
            F.math.in_range(405 * MeV, F.MASS, 590 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(435 * MeV, F.MASS, 560 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.END_VZ < 700 * mm,
        ),
    )
    return Hlt2Line(
        name=name, algs=upfront_reconstruction() + [kshorts], persistreco=True)


def lambda_ll_line(name="Hlt2Commissioning_L0ToPimPip_LL"):
    lambdas = ParticleCombiner(
        [_filter_long_protons(), _filter_long_pions()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1230 * MeV,
            F.MAXDOCACUT(1 * mm),
            (F.CHILD(1, F.P) - F.CHILD(2, F.P)) > 1 * GeV,
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1080 * MeV, F.MASS, 1200 * MeV),
            F.PT > 600 * MeV,
            F.END_VZ < 700 * mm,
        ),
    )
    return Hlt2Line(
        name=name, algs=upfront_reconstruction() + [lambdas], persistreco=True)


def sigmap_line(name='Hlt2Commissioning_SpToPPi0', prescale=1):
    protons = ParticleFilter(_filter_long_protons(), F.FILTER(F.PT > 1. * GeV))
    pi0 = ParticleCombiner(
        name="Commissioning_Pi0_Combiner",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(110 * MeV, F.MASS, 165 * MeV), F.PT > 800 * MeV),
        CompositeCut=F.ALL,
    )
    sigmas = ParticleCombiner(
        name="Commissioning_Sigmap_Combiner",
        Inputs=[protons, pi0],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="[Sigma+ -> p+ pi0]cc",
        CombinationCut=F.require_all(
            F.math.in_range(1100 * MeV, F.MASS, 1400 * MeV),
            F.PT > 2000 * MeV,
            (F.CHILD(1, F.P) - F.CHILD(2, F.P)) > 1 * GeV,
        ),
        CompositeCut=F.ALL,
    )
    return Hlt2Line(
        name=name, algs=[protons, sigmas], persistreco=True, prescale=prescale)


def _make_streams():
    lumi_line = lumi_nanofy_line()

    full_lines = [
        hlt2_track_mva_line(),
        threebody_line(),
        twobody_line(), lumi_line
    ]
    turbo_lines = [
        diphoton_line(),
        sigmap_line(),
        kshort_ll_line(),
        lambda_ll_line(), lumi_line
    ]
    lumi_lines = [builder() for builder in lumi_calibration_lines.values()]

    return Streams(streams=[
        Stream("full", lines=full_lines),
        Stream("turbo", lines=turbo_lines),
        Stream("lumi", lines=lumi_lines)
    ])


def make_streams():

    from RecoConf.decoders import default_VeloCluster_source
    from RecoConf.legacy_rec_hlt1_tracking import (
        make_VeloClusterTrackingSIMD, make_reco_pvs, make_PatPV3DFuture_pvs)
    from RecoConf.muonid import make_muon_hits
    from RecoConf.calorimeter_reconstruction import make_digits
    from RecoConf.hlt2_tracking import (
        make_PrKalmanFilter_noUT_tracks, make_PrKalmanFilter_Seed_tracks,
        make_PrKalmanFilter_Velo_tracks, make_TrackBestTrackCreator_tracks)
    # Workaround to enable running of Tracking efficiency lines using special muon reconstruction
    from PyConf.Tools import TrackMasterFitter

    with make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=4.),\
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=6.),\
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.),\
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=5.),\
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
        make_VeloClusterTrackingSIMD.bind(SkipForward=4),\
        make_muon_hits.bind(geometry_version=3),\
        make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
        make_digits.bind(calo_raw_bank=True),\
        TrackMasterFitter.bind(FastMaterialApproximation=True):

        return _make_streams()

###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Hlt2Conf.lines.b_to_charmonia import all_lines as b_to_charmonia_lines
from Hlt2Conf.lines.b_to_open_charm import (
    all_lines as b_to_open_charm_lines,
    all_calib_lines as b_to_open_charm_calib_lines,
)
from Hlt2Conf.lines.bandq import (
    hlt2_full_lines as bandq_full_lines,
    hlt2_turbo_lines as bandq_turbo_lines,
)

from Hlt2Conf.lines.bnoc import (
    hlt2_lines as bnoc_turbo_lines,
    full_lines as bnoc_full_lines,
)
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.ift import (ift_full_lines, ift_turbo_lines)
from Hlt2Conf.lines.inclusive_detached_dilepton import all_lines as inclusive_detached_dilepton_lines
from Hlt2Conf.lines.monitoring import all_lines as monitoring_lines
from Hlt2Conf.lines.pid import all_lines as pid_lines
from Hlt2Conf.lines.qee import (
    hlt2_full_lines as qee_full_lines,
    hlt2_turbo_lines as qee_turbo_lines,
)
from Hlt2Conf.lines.rd import (
    full_lines as rd_full_lines,
    turbo_lines as rd_turbo_lines,
)
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.trackeff import (
    turcal_lines as trackeff_turcal_lines,
    turbo_lines as trackeff_turbo_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon import (
    full_lines as charmonium_to_dimuon_prompt_full_lines,
    turbo_lines as charmonium_to_dimuon_prompt_turbo_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.calibration import all_lines as hlt2_calibration_lines
from Hlt2Conf.lines.nobias import all_lines as hlt2_no_bias_lines
from Hlt2Conf.lines.commissioning import (
    _lc_to_pkpi_line,
    _diphoton_line,
    _kshort_ll_line,
    _kshort_ll_tos_line,
    _phi_to_kk_detached_line,
    _lambda_ll_line,
    _lambda_ll_tos_line,
    _d0_to_kpi_line,
    _dp_to_kpipi_line,
    _dp_to_kpipi_tos_line,
)

from Moore.lines import Hlt2Line
from Moore.streams import (Stream, Streams, DETECTORS)
from Moore.config import filter_lines

from typing import Callable


def _make_lines(*lines_dicts: dict[str, Callable]) -> list[Hlt2Line]:
    """Helper to remove unsupported lines and invoke the line builder.

    Args:
        lines_dicts (Tuple[dict[str, Callable], ...]): Line to dictionaries to build lines from.

    Returns:
        list[Hlt2Line]: List of HLT2 trigger lines.
    """

    # for removing lines that are incompatible with running conditions
    to_remove = [
        "Hlt2TrackEff_DiMuon_Downstream",
        "Hlt2TrackEff_DiMuon_MuonUT",
        "Hlt2TrackEff_ZToMuMu_Downstream",
        "Hlt2TrackEff_ZToMuMu_MuonUT",
    ]

    all_lines = []
    for lines in lines_dicts:
        trunc_lines = lines
        for remove in to_remove:
            trunc_lines = filter_lines(trunc_lines, remove)
        if len(lines) != len(trunc_lines):
            from logging import getLogger
            getLogger(__name__).info(
                "Incompatible lines have been removed: %s",
                lines.keys() - trunc_lines.keys())
        all_lines += [builder() for builder in trunc_lines.values()]
    return all_lines


# TODO: It's a temporary solution to stream these events to hlt2calb.
# They should never enter any stream but be moved somewhere directly from the buffer.
# See discussion in https://gitlab.cern.ch/lhcb/Moore/-/issues/735
def _hlt2_large_event_line(name: str = "Hlt2LargeEvent", prescale: float = 1):
    """Passthrough for events too large for HLT1 processing.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1PassthroughLargeEventDecision",
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo', 'Plume'],
        prescale=prescale,
    )


def _make_streams() -> Streams:
    """Defines which trigger lines are run, which stream they belong to, and if the stream should persist raw banks.

    Raises:
        RuntimeError: If lines are misconfigured. Currently only checks for persistreco config in Full and Turcal.

    Returns:
        Streams: Object containing all streams with their corresponding trigger lines and raw banks.
    """
    lumi_nano_line = lumi_nanofy_line()

    full_lines = _make_lines(
        topological_b_lines,
        inclusive_detached_dilepton_lines,
        charmonium_to_dimuon_detached_lines,
        charmonium_to_dimuon_prompt_full_lines,
        b_to_open_charm_calib_lines,
        rd_full_lines,
        qee_full_lines,
        semileptonic_lines,
        bnoc_full_lines,
        ift_full_lines,
        bandq_full_lines,
    ) + [lumi_nano_line]

    turbo_lines = _make_lines(
        b_to_open_charm_lines,
        rd_turbo_lines,
        bandq_turbo_lines,
        qee_turbo_lines,
        charm_lines,
        b_to_charmonia_lines,
        charmonium_to_dimuon_prompt_turbo_lines,
        bnoc_turbo_lines,
        ift_turbo_lines,
        trackeff_turbo_lines,
    ) + [lumi_nano_line]

    turcal_lines = _make_lines(
        pid_lines,
        trackeff_turcal_lines,
        monitoring_lines,
    ) + [
        _diphoton_line(),
        _kshort_ll_line(),
        _kshort_ll_tos_line(),
        _phi_to_kk_detached_line(),
        _lambda_ll_line(),
        _lambda_ll_tos_line(),
        _d0_to_kpi_line(),
        _dp_to_kpipi_line(),
        _dp_to_kpipi_tos_line(),
        _lc_to_pkpi_line(),
        lumi_nano_line,
    ]

    lumi_lines = _make_lines(lumi_calibration_lines)

    # Hlt2LumiCounters can only be invoked once, so we take it from
    # the list of lumi lines
    # FIXME: Hlt2LumiCounters triggers on Hlt1ODINLumiDecision but
    # nobias uses Hlt1ODIN1kHzLumiDecision
    # https://gitlab.cern.ch/lhcb/Moore/-/issues/745
    no_bias_lines = _make_lines(hlt2_no_bias_lines) + [
        lumi_nano_line,
        next((l for l in lumi_lines if l.name == "Hlt2LumiCounters")),
    ]

    hlt2calib_lines = _make_lines(hlt2_calibration_lines) + [
        lumi_nano_line,
        _hlt2_large_event_line(),
    ]

    # make sure persistreco is true for full and turcal lines
    problems = []
    for l in full_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the FULL stream" +
                            "and must have persistreco=True")
    for l in turcal_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(f"Line {l.name} is in the TURCAL stream" +
                            "and must have persistreco=True")
    if problems:
        raise RuntimeError("Misconfigured lines found:\n" +
                           "\n".join(problems))
    # NOTE: hlt2calib configures raw bank persistency by line
    return Streams(streams=[
        Stream("full", lines=full_lines, detectors=[]),
        Stream("turbo", lines=turbo_lines, detectors=[]),
        Stream("turcal", lines=turcal_lines, detectors=DETECTORS),
        Stream("lumi", lines=lumi_lines, detectors=[]),
        Stream("no_bias", lines=no_bias_lines, detectors=DETECTORS),
        Stream("hlt2calib", lines=hlt2calib_lines, detectors=[]),
    ])


def make_streams(real_make_streams: Callable = _make_streams) -> Streams:
    """Wrapper around the stream maker used to apply the necessary reconstruction binds.

    Args:
        real_make_streams (Callable, optional): Function defining the streams. Defaults to _make_streams.

    Returns:
        Streams: Object containing all streams with their corresponding trigger lines and raw banks.
    """
    from RecoConf.hlt2_global_reco import (
        reconstruction,
        make_light_reco_pr_kf_without_UT,
    )
    from RecoConf.hlt2_tracking import (
        make_TrackBestTrackCreator_tracks,
        make_PrKalmanFilter_noUT_tracks,
        make_PrKalmanFilter_Velo_tracks,
        make_PrKalmanFilter_Seed_tracks,
    )
    from RecoConf.decoders import default_VeloCluster_source
    from RecoConf.event_filters import require_gec
    from RecoConf.protoparticles import make_charged_protoparticles
    from Hlt2Conf.settings.defaults import get_default_hlt1_filter_code_for_hlt2
    import sys

    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf
    # FIXME: once ProbNNs are trained for 2024 drop make_charged_protoparticles.bind(fill_probnn_defaults=True)
    with reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
     require_gec.bind(skipUT=True),\
     default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),\
     make_charged_protoparticles.bind(fill_probnn_defaults=True),\
     make_TrackBestTrackCreator_tracks.bind(max_ghost_prob=0.7, max_chi2ndof=sys.float_info.max),\
     make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.),\
     make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.),\
     make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.),\
     get_default_hlt1_filter_code_for_hlt2.bind(code=r"Hlt1(?!PassthroughLargeEvent).*Decision"):

        return real_make_streams()

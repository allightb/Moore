###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# needed to check if we didn't find something in TES
import cppyy, re
#cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True
not_found = cppyy.bind_object(0, cppyy.gbl.DataObject)


def error(msg):
    print("Check ERROR", msg)


# These locations do not exist in old brunel outputs or any file produced
# before LHCb!3622, for the moment ignore the errors in unpacking checks.
# until we have some versioning of the reco locations
UnexpectedReco = ["MuonTracks"]


def check_persistreco(TES, locations, N=3, unexpected_locs=[]):

    for loc in locations:
        unpacked = TES[loc]

        if any(x in loc for x in UnexpectedReco):
            continue

        if any(x in loc for x in unexpected_locs):
            continue
        if unpacked == not_found:
            error("Unpacked location not found: " + loc)

        if "Rec/Summary" in loc:
            if unpacked.size() < 1:
                error("RecSummary not persisted.")
        print(loc, " has size ", len(unpacked))

        if len(unpacked) < N:
            if "Vertex" not in loc:  ## Do not expect N_TURBO+ vertices
                error(loc + " has only " + str(len(unpacked)) +
                      f" entries, expected >={N}.")


def check_MCoutput(TES, RECO_ROOT, fs=4, mcpart=100):
    """
    Check MC TES locations are present for dst workflows

    Note that if at any point an MDF has been used in the workflow these locations will not exist
    (MDFs only support RawBanks)
    """

    MC_rel = TES[RECO_ROOT + '/Relations/ChargedPP2MCP']
    if not MC_rel:
        error("MC relations table not propagated")
    if not MC_rel.relations().size() >= fs:
        error("MC relations table not correctly propagated")
    print("MC relations table size is ", MC_rel.relations().size())

    MC_part = TES[RECO_ROOT + '/MC/Particles']
    if not MC_part:
        error("MC particles not propagated")
    if not MC_part.size() > mcpart:
        error("MC particles not correctly propagated")
    print(
        "MC particles " + RECO_ROOT + '/MC/Particles' + " container has size ",
        MC_part.size())

    MC_part_packed = TES[RECO_ROOT + '/pSim/MCParticles'].mcParts().size()
    print("Packed MC particles container has size ", MC_part_packed)
    if not MC_part.size() == MC_part_packed:
        error("MC object packing not working: " + str(MC_part.size()) +
              " unpacked, vs. :" + str(MC_part_packed) + " packed")

    MC_vert = TES[RECO_ROOT + '/MC/Vertices']
    if not MC_vert:
        error("MC vertices not propagated")
    if not MC_vert.size() > mcpart:
        error("MC vertices not correctly propagated")
    print("MC vertices " + RECO_ROOT + '/MC/Vertices' + " container has size ",
          MC_vert.size())

    header = TES['/Event/Gen/Header']
    if not header:
        error("MC Headers not being propagated")
    print("MC eventtype is ", header.evType())
    MCTrackInfo = TES['/Event/MC/TrackInfo']
    if not MCTrackInfo:
        error("MC TrackInfo not being propagated")


def check_banks(TES, stream, banks=[9]):
    """
    Check a RawBank is populated
    RawBank numbers can be found here gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62
    """
    #
    for bank in banks:
        bank_loc = f'/Event/{stream}/RawEvent'
        print("bank ", bank)
        RawBank = TES[bank_loc].banks(int(bank))
        if not RawBank or RawBank.size() < 1:
            error("Expected raw bank " + str(bank))
        print("RawBank ", bank, " has size ", RawBank.size())


def check_not_banks(TES, stream, banks=[9]):
    """
    Check a RawBank is not populated
    RawBank numbers can be found here gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h#L62
    """
    #
    for bank in banks:
        bank_loc = f'/Event/{stream}/RawEvent'
        RawBank = TES[bank_loc].banks(bank)
        if not RawBank:
            print("RawBank ", bank, " absent as expected")
            continue
        if RawBank.size() >= 0:
            error("Did not expect raw bank " + str(bank))
            print("RawBank ", bank, " has size ", RawBank.size())


def check_particles(TES, prefix, N=3):
    """
    Check particles exist and the relations between them

    Assumes decay like X->YZ where Z is a final state particle
    """

    container = TES[prefix + '/Particles']
    if not container:
        error("No particles propagated contrary to DecReports")
    if not container.size() > 0:
        error("Zero particles propagated contrary to DecReports")
    print(prefix + "/Particles has size ", container.size())

    # Check a protoparticle (only for final state particles - hence check if basic else drill down two more level - brittle)
    if container[0].daughtersVector()[1].isBasicParticle():
        proto = container[0].daughtersVector()[1].proto()
    elif container[0].daughtersVector()[1].daughtersVector(
    )[0].isBasicParticle():
        proto = container[0].daughtersVector()[1].daughtersVector()[0].proto()
    else:
        proto = container[0].daughtersVector()[1].daughtersVector(
        )[0].daughtersVector()[0].proto()

    if not proto:
        error("Proto particles are not saved")
    print("proto: ", proto)

    mothers = container.size()
    firstdaughters = container[0].daughtersVector().size()
    print("mothers: ", mothers, " firstdaughters: ", firstdaughters)
    if mothers + firstdaughters < N:
        error("Decay tree of particles not being saved correctly.")


def check_decreports(TES, decs=[], stage="Hlt2"):

    reportloc = TES['/Event/' + stage + '/DecReports']
    dec_format = []
    for dec in decs:
        dec = dec if dec.endswith("Decision") else dec + "Decision"
        dec_format.append(dec)

    decisions = {
        dec: reportloc.decReport(dec).decision()
        for dec in dec_format
    }
    print(stage, " requested reports are ", decisions)

    check = [k for k, v in decisions.items() if v == 1]
    print("check ", check)
    if not check:
        error(
            "None of the triggers you asked for fired - HLT2 DecReports or streaming not working as expected."
        )
    return decisions


def check_decreports_regex(TES, regdecs=[], stage="Hlt2"):

    reportloc = TES['/Event/' + stage + '/DecReports']
    reports = {
        dec: reportloc.decReport(dec).decision()
        for dec in reportloc.decReports().keys()
    }
    #print("dec reports are ", reports)
    pos_lines = [
        line for line in reports if reportloc.decReport(line).decision()
    ]
    print(f"Lines that fired : {pos_lines}")
    print(f"regdecs : {regdecs}")
    r = re.compile('|'.join(regdecs))
    pos_lines_match = list(filter(r.match, pos_lines))
    print("pos_lines_match", pos_lines_match)
    if not pos_lines_match:
        error(
            "None of the triggers you asked for fired - HLT2 DecReports or streaming not working as expected."
        )
    return pos_lines_match


def check_hlt2_topo_candidates(TES):

    reportloc = TES['/Event/Hlt2/DecReports']
    n_topo2, n_topo3 = 0, 0
    if reportloc.decReport("Hlt2Topo2BodyDecision").decision():
        if TES["/Event/Spruce/HLT2/TISTOS/Hlt2Topo2Body/Particles"]:
            n_topo2 = len(
                TES["/Event/Spruce/HLT2/TISTOS/Hlt2Topo2Body/Particles"])
            print("topo 2 body candidates", n_topo2)
            for candidate in TES[
                    "/Event/Spruce/HLT2/TISTOS/Hlt2Topo2Body/Particles"]:
                for dec_product in candidate.daughtersVector():
                    lhcbids = []
                    for lhcbid in dec_product.proto().track().lhcbIDs():
                        lhcbids.append(lhcbid.lhcbID())
                    print("lhcbids ", lhcbids)
                    if len(lhcbids) == 0:
                        error("No LHCbIDs saved for topo candidate")
        else:
            n_topo2 = -1
            error("No Topo2 body location in TES")
        if n_topo2 < 1:
            error("Less topo2 candidates than expected")

    if reportloc.decReport("Hlt2Topo3BodyDecision").decision():
        if TES["/Event/Spruce/HLT2/TISTOS/Hlt2Topo3Body/Particles"]:
            n_topo3 = len(
                TES["/Event/Spruce/HLT2/TISTOS/Hlt2Topo3Body/Particles"])
            print("topo 3 body candidates", n_topo3)
            for candidate in TES[
                    "/Event/Spruce/HLT2/TISTOS/Hlt2Topo3Body/Particles"]:
                basic_dec_products = [
                    candidate.daughtersVector()[1],
                    candidate.daughtersVector()[0].daughtersVector()[0],
                    candidate.daughtersVector()[0].daughtersVector()[1]
                ]
                for dec_product in basic_dec_products:
                    lhcbids = []
                    for lhcbid in dec_product.proto().track().lhcbIDs():
                        lhcbids.append(lhcbid.lhcbID())
                    print("lhcbids ", lhcbids)
                    if len(lhcbids) == 0:
                        error("No LHCbIDs saved for topo candidate")
        else:
            n_topo3 = -1
            error("No Topo3 body location in TES")
        if n_topo3 < 1:
            error("Less topo3 candidates than expected")

    return n_topo2, n_topo3

###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Generic lines used for testing purposes. Some are snapshots of production lines, but are not used for production.
Follow https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_line.html#code-design-guidelines

Note on naming: this module names all combiners their names and names of lines begin with "Test_" and "Hlt2Test_", respectively, to be able to easily group them in performace tests and spot them when inspecting log files.
Filters are not named, see https://gitlab.cern.ch/lhcb/Moore/-/issues/378 and https://gitlab.cern.ch/lhcb/Moore/-/issues/380
"""

import Functors as F

from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um, ps
from GaudiKernel.SystemOfUnits import picosecond
from PyConf.Algorithms import Monitor__ParticleRange, ParticleUnbiasedPVAdder
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from RecoConf.reconstruction_objects import make_pvs, make_extended_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs, require_gec

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import (
    make_long_electrons_with_brem, make_long_muons, make_has_rich_long_pions,
    make_long_pions, make_photons, make_has_rich_down_protons, make_down_pions,
    make_has_rich_long_kaons, make_long_kaons, make_has_rich_long_protons,
    make_ismuon_long_muon)

hlt2_test_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


#Dummy line, included to trigger neutral reconstruction.
@register_line_builder(hlt2_test_lines)
def photons_line(name="Hlt2Test_Photons", prescale=1):
    photons = make_photons()
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(F.P > 40 * GeV))
    # test monitoring of neutral basics
    photon_mon = Monitor__ParticleRange(
        name="Monitor_Test_Photons_IsPhoton",
        Input=photons,
        Variable=F.VALUE_OR(F.NaN) @ F.IS_PHOTON,
        HistogramName="/Test_Photons/IsPhoton",
        Bins=100,
        Range=(0, 1))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg, photon_mon])


#Dummy line, testing on top of neutrals the AdditionalInfo functors
@register_line_builder(hlt2_test_lines)
def filtered_photons_line(name="Hlt2Test_FilteredPhotons", prescale=1):
    photons = make_photons()
    code = F.require_all(F.P > 40 * GeV, F.IS_PHOTON > 0.5, F.IS_NOT_H > 0.3,
                         F.CALO_NEUTRAL_SHOWER_SHAPE < 1500)
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on multiple relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTables as FilterRelTables,
    LHCb__Phys__RelationTables__MakeRelTables as MakeRelTables,
)


@register_line_builder(hlt2_test_lines)
def mRT_photons_line(name="Hlt2Test_mRTPhotons", prescale=1):
    def tr(**outputs):
        return {"OutputLocation": [v for (k, v) in outputs.items()]}

    ai = ["IsPhoton", "ShowerShape", "IsNotH"]
    tables = MakeRelTables(
        InputParticles=make_photons(),
        AdditionalInfo=ai,
        outputs={prop: None
                 for prop in ai},
        output_transform=tr).outputs
    predicateCut = (  (F.COLUMN(ColumnLabel=tables["IsPhoton"].location) > 0.5) \
                    & (F.COLUMN(ColumnLabel=tables["ShowerShape"].location) <  1500) \
                    & (F.COLUMN(ColumnLabel=tables["IsNotH"].location) >0.3) \
                    )
    photons = FilterRelTables(InputTables = [ dh for dh in tables.values()], \
                                RelCuts = predicateCut).OutputLocation
    code = F.require_all(F.P > 40 * GeV)
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on single relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTable as FilterRelTable,
    LHCb__Phys__RelationTables__AiToRelTable as AiToRelTable,
)


@register_line_builder(hlt2_test_lines)
def sRT_photons_line(name="Hlt2Test_sRTPhotons", prescale=1):
    relTab = AiToRelTable(
        InputParticles=make_photons(),
        AdditionalInfo="ShowerShape").OutputLocation
    predicateCut = (F.IDENTITY < 1500)
    photons = FilterRelTable(
        InputTable=relTab, Predicate=predicateCut).OutputLocation
    code = F.require_all(
        F.P > 40 * GeV,
        F.IS_PHOTON > 0.5,
        F.IS_NOT_H > 0.3,
    )
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


#From Hlt2Conf/lines/monitoring/pi0_line.py
@register_line_builder(hlt2_test_lines)
def pi0_to_gammagamma_line(name="Hlt2Test_Pi0ToGammaGamma", prescale=1):
    code = F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.IS_NOT_H > 0.2)
    photons = ParticleFilter(Input=make_photons(), Cut=F.FILTER(code))

    combination_code = F.require_all(
        F.PT > 1400 * MeV,
        F.math.in_range(100 * MeV, F.MASS, 200 * MeV),
    )
    pi0s = ParticleCombiner(
        name="Test_Pi0ToGammaGamma_Combiner",
        Inputs=[photons, photons],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
    )
    return Hlt2Line(name=name, algs=[pi0s], prescale=prescale)


#From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(hlt2_test_lines)
def xicz_to_lpipi_dd_line(name="Hlt2Test_Xic0ToL0PimPip_DD"):
    pvs = make_pvs()
    down_pions = ParticleFilter(make_down_pions(), F.FILTER(F.PT > 180 * MeV))
    down_protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(
            F.require_all(F.PT > 700 * MeV, F.P > 10 * GeV, F.PID_P > -5.)))
    dd_lambdas = ParticleCombiner(
        [down_protons, down_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Test_Hyperons_Lambda_DD",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.),
            F.PT > 0.9 * GeV,
            F.P > 13 * GeV,
            F.SUM(F.PT) > 1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 1 * GeV,
            F.P > 14 * GeV,
            F.CHI2DOF < 9.,
        ),
    )
    long_xicz_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Test_Hyperons_Xic0ToL0PimPip_DD",
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 150 * um,
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dd_lambdas, xic0s])


#From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(hlt2_test_lines)
def oc_to_ximkpipi_lll_line(name="Hlt2Test_Oc0ToXimKmPipPip_LLL"):
    pvs = make_pvs()
    long_lambda_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(32.))))
    long_lambda_protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 9 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_P > -5.)))
    ll_lambdas = ParticleCombiner(
        [long_lambda_protons, long_lambda_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Test_Hyperons_LambdaFromHyperon_LL",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 550 * MeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(make_pvs()) > 8 * mm,
            F.BPVFDCHI2(make_pvs()) > 480.,
        ),
    )
    long_xi_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(8.))))
    lll_xis = ParticleCombiner(
        [ll_lambdas, long_xi_pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Test_Hyperons_Xim_LLL",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 9.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 4 * mm,
            F.BPVFDCHI2(pvs) > 16.,
        ),
    )
    long_oc_kaons = ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 5 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K > 0.), ),
    )
    long_oc_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K < 5.), ),
    )
    oc0s = ParticleCombiner(
        [lll_xis, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name="Test_Hyperons_Oc0ToXimKmPipPip_LLL",
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 275 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), ll_lambdas, lll_xis, oc0s])


#From former LoKi example line
def _make_phi():
    kaons = make_long_kaons()
    filteredKaons = ParticleFilter(
        name="FilteredPIDkaons", Input=kaons, Cut=F.FILTER(F.PID_K > -5))
    combination_code = F.require_all(
        F.math.in_range(980. * MeV, F.MASS, 1060. * MeV),
        F.MAXDOCACHI2CUT(30.),
        F.SUM(F.CHI2DOF < 5.) > 0)
    vertex_code = F.require_all(F.CHI2 < 10, F.PT > 1 * GeV, F.CHI2DOF < 15,
                                F.BPVFDCHI2(make_pvs()) > 0)
    return ParticleCombiner(
        name="Test_Phi2KKMaker",
        Inputs=[kaons, filteredKaons],
        DecayDescriptor=
        "phi(1020) -> K+ K-",  # mstahl: how does the combiner handle the different inputs? is cc needed because of this?
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def _make_bs2jpsiphi(jpsi, phi, pvs):
    combination_code = F.require_all(
        F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV))
    vertex_code = F.require_all(
        F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV), F.CHI2DOF < 20,
        F.BPVLTIME(pvs) > 0.01 * ps)
    return ParticleCombiner(
        name="Test_Bs2JPsiPhiCombiner_{hash}",
        Inputs=[jpsi, phi],
        DecayDescriptor="B_s0 -> J/psi(1S) phi(1020)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def _make_BdsToPhiPhi(phi, pvs):
    combination_code = F.require_all(
        in_range(5000, F.MASS, 6000),
        (F.CHILD(1, F.PT) * F.CHILD(2, F.PT)) > 1.5 * GeV * GeV)
    vertex_code = F.require_all(F.CHI2DOF < 15, in_range(5000, F.MASS, 6000),
                                F.BPVLTIME(pvs) > 0.2 * picosecond)
    return ParticleCombiner(
        name="Test_BdsToPhiPhiCombiner",
        Inputs=[phi, phi],
        DecayDescriptor="B_s0 -> phi(1020) phi(1020)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


# test monitoring
def my_mass_mon(datahandle, name, min, max):
    return


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_line(name='Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum',
                                     prescale=1):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXDOCACHI2 <
        20,  # mstahl: mixture of MAXDOCACHI2 and MAXDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor=
        "J/psi(1S) -> mu+ mu-",  # remove 'cc' and no use for cc in case of self-conjugate decays. 
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    phi_mon = Monitor__ParticleRange(
        name="Monitor_Phi_for_Test_Bs0ToJpsiPhi_m",
        Input=phi,
        Variable=F.MASS,
        HistogramName="/Phi_for_Test_Bs0ToJpsiPhi/m",
        Bins=100,
        Range=(980 * MeV, 1060 * MeV))

    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi_mon, jpsi, bs],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_pvunbiasing_with_refit_line(
        name='Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum_PVUnbiasingWithRefit',
        prescale=1):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXDOCACHI2 <
        20,  # mstahl: mixture of MAXDOCACHI2 and MAXDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor=
        "J/psi(1S) -> mu+ mu-",  # remove 'cc' and no use for cc in case of self-conjugate decays. 
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    # create a new B list with unbiased PVs, with default mode=1
    # "The unbias mode (0:unbias,1:refit,2:unimplemented)"};
    bs_unbiasedpv = ParticleUnbiasedPVAdder(
        OutputLevel=2, InputParticles=bs, PrimaryVertices=make_extended_pvs())

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), jpsi, bs, bs_unbiasedpv],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_pvunbiasing_line(
        name='Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum_PVUnbiasing', prescale=1):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXDOCACHI2 <
        20,  # mstahl: mixture of MAXDOCACHI2 and MAXDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor=
        "J/psi(1S) -> mu+ mu-",  # remove 'cc' and no use for cc in case of self-conjugate decays. 
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    # create a new B list with unbiased PVs, without refitting mode=0
    # "The unbias mode (0:unbias,1:refit,2:unimplemented)"};
    bs_unbiasedpv = ParticleUnbiasedPVAdder(
        OutputLevel=2,
        InputParticles=bs,
        PrimaryVertices=make_extended_pvs(),
        Mode=0)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), jpsi, bs, bs_unbiasedpv],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2ee_phi2kk_line(name='Hlt2Test_BsToJpsiPhi_JPsi2ee_PhiToKK',
                                   prescale=1):
    pvs = make_pvs()
    electrons = make_long_electrons_with_brem()
    filteredElectrons = ParticleFilter(
        name="FilteredPIDElectrons",
        Input=electrons,
        Cut=F.FILTER(F.PID_E > 0.))
    mother_code = F.CHI2 < 16.
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV), F.MAXDOCACHI2 < 20,
        F.SUM(F.PT > 0.5 * GeV) > 0)
    jpsi = ParticleCombiner(
        name=name,
        Inputs=[electrons, filteredElectrons],
        DecayDescriptor="[J/psi(1S) -> e+ e-]cc",
        CombinationCut=combination_code,
        CompositeCut=mother_code)

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi, bs],
        prescale=prescale,
    )


#From Hlt2Conf/lines/charm/d0_to_hh.py


def _make_dstars(dzeros, decay_descriptor, name):
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.P > 1 * GeV,
                F.PID_K < 5.,
            ), ),
    )
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < 165 * MeV,
        CompositeCut=F.require_all(F.MASS - F.CHILD(1, F.MASS) < 160 * MeV,
                                   F.CHI2DOF < (16.)),
    )


def _make_sl_bs(dzeros, muons):
    pvs = make_pvs()
    return ParticleCombiner(
        [dzeros, muons],
        DecayDescriptor="[B- -> D0 mu-]cc",
        name="Test_Taggers_BBuilderFor_D0_{hash}",
        CombinationCut=F.require_all(
            in_range(2.3 * GeV, F.MASS, 10 * GeV), F.MAXDOCACHI2CUT(10.)),
        CompositeCut=F.require_all(
            F.CHI2DOF < 9., in_range(2.3 * GeV, F.MASS, 10 * GeV),
            in_range(2.8 * GeV, F.BPVCORRM(pvs), 8.5 * GeV),
            F.BPVDIRA(pvs) > 0.999,
            F.CHILD(1, F.END_VZ) - F.END_VZ > -3 * mm),
    )


def _make_dzeros(pvs):
    kaons = ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                _MIPCHI2_MIN(4.),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
            ), ),
    )

    return ParticleCombiner(
        [kaons, kaons],
        DecayDescriptor="[D0 -> K+ K-]cc",
        name="Test_D0ToKpKm_Builder_{hash}",
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(pvs) > 25.,
            F.BPVDIRA(pvs) > 0.99985,
        ),
    )


@register_line_builder(hlt2_test_lines)
def dzero2kpkm_line(name='Hlt2Test_D0ToKpKm', prescale=1):
    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), dzeros],
        prescale=prescale)


@register_line_builder(hlt2_test_lines)
def dstarp2dzeropip_dzero2kpkm_line(name='Hlt2Test_DstpToD0Pip_D0ToKpKm',
                                    prescale=1):

    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    dstars = _make_dstars(
        dzeros,
        decay_descriptor="[D*(2010)+ -> D0 pi+]cc",
        name="Test_D0ToHH_D0ToKpKm_{hash}")

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_gec()] +
        [require_pvs(pvs), dzeros, dstars],
        prescale=prescale)


@register_line_builder(hlt2_test_lines)
def b2d0mu_dzero2kpkm_line(name='Hlt2Test_BToD0MumX_D0ToKpKm', prescale=1):

    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    muons = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=pvs),
                F.PT > 1 * GeV,
                F.P > 2 * GeV,
                F.PID_MU > 0.,
            ), ),
    )
    bs = _make_sl_bs(dzeros, muons)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_gec()] +
        [require_pvs(pvs), dzeros, bs],
        prescale=prescale)


#From Hlt2Conf/lines/bnoc/hlt2_bnoc.py
@register_line_builder(hlt2_test_lines)
def Bds_PhiPhi_line(name='Hlt2Test_BdsToPhiPhi', prescale=1):
    pvs = make_pvs()
    phi = _make_phi()
    BdsToPhiPhi = _make_BdsToPhiPhi(phi, pvs)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi, BdsToPhiPhi])

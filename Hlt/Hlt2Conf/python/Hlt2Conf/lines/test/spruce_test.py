###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Booking of test sprucing lines, notice PROCESS = 'spruce'

In Test_sprucing_line a mass monitor for Ds mass is included,
to save the outputs add the following line to your yaml:
histo_file : 'my_histograms.root'

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of sprucing_lines

To be noted:
"bare" line builders, like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""
from __future__ import absolute_import, division, print_function

from Moore.config import SpruceLine

from PyConf.Algorithms import Monitor__ParticleRange

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

import Functors as F

from Functors.math import in_range
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_kaons, make_long_pions)

sprucing_lines = {}

##############################################
# From the B2OC BToDh_Builder
##############################################


def _make_kaons(k_pidk_min=-10., p_min=2 * GeV, pt_min=250 * MeV, minipchi2=4):
    pvs = make_pvs()
    kaons = ParticleFilter(make_has_rich_long_kaons(),
                           F.FILTER(F.require_all(F.P > p_min, F.PT > pt_min)))
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    if minipchi2 is not None:
        code = F.MINIPCHI2(pvs) > minipchi2
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


def _make_ds(kaons, pvs):
    pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV,
                          F.MINIPCHI2(pvs) > 4)))
    return ParticleCombiner([kaons, kaons, pions],
                            name='Test_Ds2KKPiCombiner_{hash}',
                            DecayDescriptor='[D_s+ -> K+ K- pi+]cc',
                            Combination12Cut=F.require_all(
                                F.MASS < 2010 * MeV,
                                F.DOCA(1, 2) < 0.5 * mm),
                            CombinationCut=F.require_all(
                                in_range(1930 * MeV, F.MASS, 2010 * MeV),
                                F.SUM(F.PT) > 1800 * MeV,
                                F.DOCA(1, 3) < 0.5 * mm,
                                F.DOCA(2, 3) < 0.5 * mm,
                            ),
                            CompositeCut=F.require_all(
                                F.CHI2DOF < 10,
                                F.BPVFDCHI2(pvs) > 36,
                                F.BPVIPCHI2(pvs) > 0,
                                F.BPVDIRA(pvs) > 0,
                                in_range(1930 * MeV, F.MASS, 2010 * MeV)))


def _make_b2x(particles, descriptor, name="Test_B2XCombiner_{hash}"):
    pvs = make_pvs()

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=F.require_all(
            in_range(5000 * MeV, F.MASS, 7000 * MeV),
            F.SUM(F.PT) > 6 * GeV),
        CompositeCut=F.require_all(
            in_range(5000 * MeV, F.MASS, 7000 * MeV), F.CHI2DOF < 20,
            F.BPVIPCHI2(pvs) < 25,
            F.BPVLTIME(pvs) > 0.3 * picosecond,
            F.BPVDIRA(pvs) > 0.999))


#@register_line_builder(sprucing_lines)
def Test_sprucing_line(name='SpruceTest_SpruceTest', prescale=1):
    pvs = make_pvs()
    kaons = _make_kaons()
    d = _make_ds(kaons, pvs)
    line_alg = _make_b2x(particles=[d, kaons], descriptor='[B0 -> D_s- K+]cc')

    d_mon = Monitor__ParticleRange(
        name=name + "_Ds_Monitor",
        HistogramName="m",
        Input=d,
        Variable=F.MASS,
        Bins=50,
        Range=((1968.35 - 5 * 5.0) * MeV, (1968.35 + 5 * 5.0) * MeV))

    return SpruceLine(
        name=name,
        prescale=prescale,
        raw_banks=['Calo'],
        algs=upfront_reconstruction() + [require_pvs(pvs), d_mon, line_alg])


def Test_extraoutputs_sprucing_line(name='SpruceTest_ExtraOutputs',
                                    prescale=1):
    pvs = make_pvs()
    kaons = _make_kaons()
    d = _make_ds(kaons, pvs)
    line_alg = _make_b2x(particles=[d, kaons], descriptor='[B0 -> D_s- K+]cc')

    LongT = ParticleFilter(make_long_pions(), F.FILTER(F.MINIPCHI2(pvs) > 0))

    return SpruceLine(
        name=name,
        prescale=prescale,
        extra_outputs=[("LongTracks", LongT)],
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=True,
        algs=upfront_reconstruction() + [require_pvs(pvs), line_alg])


def Test_persistreco_sprucing_line(name='SpruceTest_PersistReco', prescale=1):
    pvs = make_pvs()
    kaons = _make_kaons()
    d = _make_ds(kaons, pvs)
    line_alg = _make_b2x(particles=[d, kaons], descriptor='[B0 -> D_s- K+]cc')
    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=True,
        algs=upfront_reconstruction() + [require_pvs(pvs), line_alg])


### line for testing selective persistency of detector raw banks
### currently it is asking for one addiitonal 'FT' bank
def Test_sprucing_rawbank_sp_line(name='SpruceTest_rawbank_sp', prescale=1):
    pvs = make_pvs()
    kaons = _make_kaons(p_min=5 * GeV, pt_min=500 * MeV, k_pidk_min=None)
    d = _make_ds(kaons, pvs)

    line_alg = _make_b2x(particles=[d, kaons], descriptor='[B0 -> D_s- K+]cc')

    return SpruceLine(
        name=name,
        prescale=prescale,
        raw_banks=['FT'],
        algs=upfront_reconstruction() + [require_pvs(pvs), line_alg])

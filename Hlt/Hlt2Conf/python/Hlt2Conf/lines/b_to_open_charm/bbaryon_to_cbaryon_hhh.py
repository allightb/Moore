###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

#from Hlt2Conf.algorithms import ParticleFilter

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ Pi- Pi+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@check_process
def make_LbToLcpPiPiPi_LcpToPKPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 ->  pi- pi- pi+ Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #    line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ Pi- Pi+ Pi-
#######################################################################
# Lambda_c+ --> p K- K+
@check_process
def make_LbToLcpPiPiPi_LcpToPKK(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkk()
    line_alg = b_builder.make_lb2lchhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 ->  pi- pi- pi+ Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #    line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


# WS: Lambda_c+ --> p K- K+
@check_process
def make_LbToLcpPiPiPiWS_LcpToPKK(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkk()
    line_alg = b_builder.make_lb2lchhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 ->  pi- pi+ pi+ Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #    line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


# Lambda_c+ --> p pi- pi+
@check_process
def make_LbToLcpPiPiPi_LcpToPPiPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_ppipi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 ->  pi- pi- pi+ Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #    line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


# WS: Lambda_c+ --> p pi- pi+
@check_process
def make_LbToLcpPiPiPiWS_LcpToPPiPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_ppipi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 ->  pi- pi+ pi+ Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #    line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ K- Pi+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@check_process
def make_LbToLcpKPiPi_LcpToPKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Lambda_b0 -> K- pi+ pi- Lambda_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #   ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ K- K+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@check_process
def make_LbToLcpKKPi_LcpToPKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[kaon, kaon, pion, cbaryon],
        descriptors=['[Lambda_b0 -> K- K+ pi- Lambda_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #   ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ p p~ Pi-, Lambda_c+ --> p K- pi+
######################################################################
@check_process
def make_LbToLcpPbarPPi_LcpToPKPi(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_lb2lchhh(
        particles=[proton, proton, pion, cbaryon],
        descriptors=['[Lambda_b0 -> p~- p+ pi- Lambda_c+]cc'])
    #    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #        ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b0 -> Xi_c+ Pi- Pi+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@check_process
def make_Xib0ToXicpPiPiPi_XicpToPKPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Xi_b0 -> pi- pi- pi+ Xi_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b0 -> Xi_c+ K- Pi+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@check_process
def make_Xib0ToXicpKPiPi_XicpToPKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Xi_b0 -> K- pi+ pi- Xi_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b0 -> Xi_c+ K- K+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@check_process
def make_Xib0ToXicpKKPi_XicpToPKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[kaon, kaon, pion, cbaryon],
        descriptors=['[Xi_b0 -> K- K+ pi- Xi_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b0 -> Xi_c+ p p~ Pi-, Xi_c+ --> p K- pi+
####################################################################
@check_process
def make_Xib0ToXicpPbarPPi_XicpToPKPi(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[proton, proton, pion, cbaryon],
        descriptors=['[Xi_b0 -> p~- p+ pi- Xi_c+]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #    ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b- -> Xi_c0 Pi- Pi+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@check_process
def make_XibmToXic0PiPiPi_Xic0ToPKKPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Xi_b- -> pi- pi- pi+ Xi_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b- -> Xi_c0 K- Pi+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@check_process
def make_XibmToXic0KPiPi_Xic0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Xi_b- -> K- pi+ pi- Xi_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b- -> Xi_c0 K- K+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@check_process
def make_XibmToXic0KKPi_Xic0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[kaon, kaon, pion, cbaryon],
        descriptors=['[Xi_b- -> K- K+ pi- Xi_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Xi_b- -> Xi_c0 p p~ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@check_process
def make_XibmToXic0PbarPPi_Xic0ToPKKPi(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xib2xichhh(
        particles=[proton, proton, pion, cbaryon],
        descriptors=['[Xi_b- -> p~- p+ pi- Xi_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #    ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Omega_c0 Pi- Pi+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToOmc0PiPiPi_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Omega_b- -> pi- pi- pi+ Omega_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Omega_c0 Pi- Pi+ Pi+, Omega_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToOmc0PiPiPiWS_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[pion, pion, pion, cbaryon],
        descriptors=['[Omega_b- -> pi- pi+ pi+ Omega_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Omega_c0 K- Pi+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToOmc0KPiPi_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Omega_b- -> K- pi+ pi- Omega_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Omega_c0 K- K+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToOmc0KKPi_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[kaon, kaon, pion, cbaryon],
        descriptors=['[Omega_b- -> K- K+ pi- Omega_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Omega_c0 p p~ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToOmc0PbarPPi_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[proton, proton, pion, cbaryon],
        descriptors=['[Omega_b- -> p~- p+ pi- Omega_c0]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #    ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


####################################################################
# Form the Omega_b- -> Xi_c0 K- Pi+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@check_process
def make_OmbmToXic0KmPipPim_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Omega_b- -> K- pi+ pi- Xi_c0]cc'])
    return line_alg


####################################################################
# Form the Omega_b- -> Xi_c0 K- Pi+ Pi+, Xi_c0 --> p K- K- pi+ (WS)
####################################################################
@check_process
def make_OmbmToXic0KmPipPimWS_Omc0ToPKKPi(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_omegab2omegachhh(
        particles=[kaon, pion, pion, cbaryon],
        descriptors=['[Omega_b- -> K- pi+ pi+ Xi_c0]cc'])
    return line_alg

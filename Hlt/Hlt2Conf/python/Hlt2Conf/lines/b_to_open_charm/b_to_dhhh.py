###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDhhh lines
"""

from GaudiKernel.SystemOfUnits import GeV, mm, picosecond

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter


@check_process
def make_BuToD0PiPiPi_D0ToKsLLHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- -> D0 pi- pi- pi+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToD0PiPiPi_D0ToKsDDHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- -> D0 pi- pi- pi+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToD0KPiPi_D0ToKsLLHH(process):
    pion = basic_builder.make_tight_pions()
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToD0KPiPi_D0ToKsDDHH(process):
    pion = basic_builder.make_tight_pions()
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'],
        sum_pt_min=6 * GeV)
    return line_alg


###########################################################################################
# Form the B+- -> D0 Pi Pi Pi and D0 K Pi Pi for ADS/GLW, all D0->hh modes needed
############################################################################################


@check_process
def make_BuToD0PiPiPi_D0ToHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- ->  D0 pi- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0KPiPi_D0ToHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


# WS lines:
@check_process
def make_BuToD0KPiPi_D0ToHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPiPi_D0ToHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- -> D0 pi- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


###########################################################################################
# Form the B+- -> D0 Pi Pi Pi and D0 K Pi Pi for D0->hhhh (RS and WS) modes needed
############################################################################################


@check_process
def make_BuToD0KPiPi_D0ToHHHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPiPi_D0ToHHHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- -> D0 pi- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0KPiPi_D0ToHHHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pion, pion],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPiPi_D0ToHHHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pion, pion],
        descriptors=['B+ -> D0 pi+ pi+ pi-', 'B- -> D0 pi- pi- pi+'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##################################################################################
# Form the B+- -> D0 KKPi, all D0->Kpi
###################################################################################
@check_process
def make_BuToD0KKPi_D0ToKPi(process, MVACut=0.7):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    d = d_builder.make_dzero_to_kpi()
    line = b_builder.make_b2chhh(
        particles=[kaon, kaon, pion, d],
        descriptors=['B+ -> K+ K- pi+ D0', 'B- -> K- K+ pi- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line, pvs, MVACut)
    return line_alg


##################################################################################
# Form the B+- -> D0 Pbar P Pi,  only D0->KPi modes needed ?
###################################################################################
@check_process
def make_BuToD0PbarPPi_D0ToKPi(process, MVACut=0.7):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    d = d_builder.make_dzero_to_kpi()
    line = b_builder.make_b2chhh(
        particles=[proton, proton, pion, d],
        descriptors=['B+ -> p+ p~- pi+ D0', 'B- -> p~- p+ pi- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line, pvs, MVACut)
    return line_alg


#####################################################
# Form the B0 -> D- PiPiPi, KPiPi, KKPi, and PbarPPi
#####################################################
@check_process
def make_BdToDmPiPiPi_DmToPimPimKp(process):
    pion = basic_builder.make_pions()
    d = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_b2chhh(
        particles=[pion, pion, pion, d],
        descriptors=['[B0 -> pi+ pi+ pi- D-]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


@check_process
def make_BdToDmKPiPi_DmToPimPimKp(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    d = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, pion, pion, d],
        descriptors=['[B0 -> K+ pi- pi+ D-]cc', '[B0 -> K- pi+ pi+ D-]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


@check_process
def make_BdToDmKKPi_DmToPimPimKp(process):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons()
    d = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, pion, d],
        descriptors=['[B0 -> K+ K- pi+ D-]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


@check_process
def make_BdToDmPbarPPi_DmToPimPimKp(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    d = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_b2chhh(
        particles=[proton, proton, pion, d],
        descriptors=['[B0 -> p+ p~- pi+ D-]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #    ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


############################################################################################
# Form the Bs -> Ds- PiPiPi, KPiPi, KKPi, and PbarPPi
# Only Ds -> KKPi
############################################################################################
@check_process
def make_BdToDsmPiPiPi_DsmToKmKpPim(process, MVACut=0.2):
    pion = basic_builder.make_pions()
    d = d_builder.make_dsplus_to_kpkmpip()
    b = b_builder.make_b2chhh(
        particles=[pion, pion, pion, d],
        descriptors=['[B0 -> pi+ pi+ pi- D_s-]cc'],
        vtx_chi2pdof_max=7,
        bpvdira_min=0.99994,
        hhh_am_max=3.5 * GeV,
        sum_pt_min=5.5 * GeV,
        bpvltime_min=0.3 * picosecond,
        bcvtx_sep_min=0.)
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BdToDsmKPiPi_DsmToKmKpPim(process, MVACut=0.2):
    pion = basic_builder.make_pions()
    kaon = basic_builder.make_kaons(k_pidk_min=5)
    d = d_builder.make_dsplus_to_kpkmpip()
    b = b_builder.make_b2chhh(
        particles=[kaon, pion, pion, d],
        descriptors=['[B0 -> K+ pi- pi+ D_s-]cc'],
        vtx_chi2pdof_max=7,
        bpvdira_min=0.99994,
        hhh_am_max=3.5 * GeV,
        sum_pt_min=5.5 * GeV,
        bpvltime_min=0.3 * picosecond,
        bcvtx_sep_min=0.)
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BdToDsmKKPi_DsmToKmKpPim(process):
    pion = basic_builder.make_pions(pi_pidk_max=0.)
    kaon = basic_builder.make_kaons(k_pidk_min=5.)
    d = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, pion, d],
        descriptors=['[B0 -> K+ K- pi+ D_s-]cc'])
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
    #    kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


@check_process
def make_BdToDsmPbarPPi_DsmToKmKpPim(process):
    pion = basic_builder.make_pions()
    proton = basic_builder.make_protons()
    d = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_b2chhh(
        particles=[proton, proton, pion, d],
        descriptors=['[B0 -> p+ p~- pi+ D_s-]cc'],
        hhh_asumpt_min=2 * GeV)
    #track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
    #    ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
    #    ppidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    return line_alg


# TODO: WS equivalents


############################################################################################
# Form the Bd/s -> Dst- PiPiPi, KPiPi, KKPi, KKK, and PbarPPi
############################################################################################
@check_process
def make_BdToDstpPiPiPi_DstpToD0Pi_D0ToHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=5)
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[pion, pion, pion, dst],
        descriptors=['[B0 -> pi+ pi- pi- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpPiPiPi_DstpToD0Pi_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=5)
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[pion, pion, pion, dst],
        descriptors=['[B0 -> pi+ pi- pi- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpKPiPi_DstpToD0Pi_D0ToHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=5)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=-5)
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, pion, pion, dst],
        descriptors=[
            '[B0 -> K- pi+ pi- D*(2010)+]cc', '[B0 -> K+ pi- pi- D*(2010)+]cc'
        ])
    return line_alg


@check_process
def make_BdToDstpKPiPi_DstpToD0Pi_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=5)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=-5)
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, pion, pion, dst],
        descriptors=[
            '[B0 -> K- pi+ pi- D*(2010)+]cc', '[B0 -> K+ pi- pi- D*(2010)+]cc'
        ])
    return line_alg


@check_process
def make_BdToDstpKKPi_DstpToD0Pi_D0ToHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=5)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=-5)
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, pion, dst],
        descriptors=[
            '[B0 -> K+ K- pi- D*(2010)+]cc', '[B0 -> K- K- pi+ D*(2010)+]cc'
        ])
    return line_alg


@check_process
def make_BdToDstpKKPi_DstpToD0Pi_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, pion, dst],
        descriptors=[
            '[B0 -> K+ K- pi- D*(2010)+]cc', '[B0 -> K- K- pi+ D*(2010)+]cc'
        ])
    return line_alg


@check_process
def make_BdToDstpKKK_DstpToD0Pi_D0ToHH(process):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=-5)
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, kaon, dst],
        descriptors=['[B0 -> K+ K- K- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpKKK_DstpToD0Pi_D0ToHHHH(process):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=-5)
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=5, k_pidk_min=-5)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[kaon, kaon, kaon, dst],
        descriptors=['[B0 -> K+ K- K- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpPbarPPi_DstpToD0Pi_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    proton = basic_builder.make_soft_protons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[proton, proton, pion, dst],
        descriptors=['[B0 -> p+ p~- pi- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpPbarPPi_DstpToD0Pi_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    proton = basic_builder.make_soft_protons()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[proton, proton, pion, dst],
        descriptors=['[B0 -> p+ p~- pi- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpPbarPK_DstpToD0Pi_D0ToHH(process):
    proton = basic_builder.make_soft_protons()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[proton, proton, kaon, dst],
        descriptors=['[B0 -> p+ p~- K- D*(2010)+]cc'])
    return line_alg


@check_process
def make_BdToDstpPbarPK_DstpToD0Pi_D0ToHHHH(process):
    proton = basic_builder.make_soft_protons()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2chhh(
        particles=[proton, kaon, kaon, dst],
        descriptors=['[B0 -> p+ p~- K- D*(2010)+]cc'])
    return line_alg

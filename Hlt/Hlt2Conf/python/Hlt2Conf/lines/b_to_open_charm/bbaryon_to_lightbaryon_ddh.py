###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
all_lines = {}


#############################################################################
# Form the Lb -> D+ D- p K-,  D+ --> hhh
##############################################################################
@check_process
def make_LbToDpDmPK_DpToHHH(process):
    d = d_builder.make_dplus_to_hhh()
    proton = basic_builder.make_tight_protons()
    kaon = basic_builder.make_tight_kaons()
    line_alg = b_builder.make_b2x(
        particles=[d, d, proton, kaon],
        descriptors=['[Lambda_b0 -> D+ D- p+ K-]cc'])
    return line_alg


#############################################################################
# Form the Xibc -> D0 D0 p pi-, D0 D+ p pi-, D0 --> Kpi & K3pi, D+ --> Kpipi
##############################################################################


def make_Xibc0ToD0D0PPim_D0ToKPiOrKPiPiPi(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    pion = basic_builder.make_tightpid_tight_pions(pt_min=350 * MeV)
    line_alg = b_builder.make_xibc2ccx(
        particles=[Dz, Dz, proton, pion],
        descriptors=['Xi_bc0 -> D0 D0 p+ pi-', 'Xi_bc~0 -> D0 D0 p~- pi+'],
        sum_pt_hbach_min=1.25 * GeV,
    )
    return line_alg


@check_process
def make_XibcpToD0DpPPim_D0ToKPiOrKPiPiPi_DpToKmPipPip(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
    proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    pion = basic_builder.make_tightpid_tight_pions(pt_min=350 * MeV)
    line_alg = b_builder.make_xibc2ccx(
        particles=[Dz, Dp, proton, pion],
        descriptors=['Xi_bc0 -> D0 D+ p+ pi-', 'Xi_bc~0 -> D0 D- p~- pi+'],
        sum_pt_hbach_min=1.25 * GeV,
    )
    return line_alg

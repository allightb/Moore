###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV, mm

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

#############################################################################
# Form the Xi_bc+ -> D0 D0 p,  D0 --> K- pi+
# NB: Here, D0 == Kpi & K3pi
##############################################################################


@check_process
def make_XibcpToPD0D0_D0ToKPiOrKPiPiPi(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    protons = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_xibc2ccx(
        particles=[Dz, Dz, protons],
        descriptors=['Xi_bc+ -> D0 D0 p+', 'Xi_bc~- -> D0 D0 p~-'],
    )
    return line_alg


########################
# Add some B2DDH lines #
# Author: Ruiting Ma   #
########################


# Lambda_b0 -> D0 D_s- p+
@check_process
def make_LbToD0DsmP_D0ToKPiOrKPiPiPi_DsmToHHH(process):
    dzero = d_builder.make_dzero_to_kpi_or_kpipipi()
    dsplus = d_builder.make_dsplus_to_hhh(am_min=1868.35, am_max=2068.35)
    proton = basic_builder.make_tight_protons(p_pidp_min=0)
    line_alg = b_builder.make_lb(
        particles=[dzero, dsplus, proton],
        descriptors=['Lambda_b0 -> D0 D_s- p+', 'Lambda_b0 -> D0 D_s+ p~-'],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        sum_pt_min=6 * GeV)
    return line_alg


##########################
# DDLambda lines
##########################


@check_process
def make_LbToDpDmLambdaLL_DpToHHH(process):
    d = d_builder.make_dplus_to_hhh(am_min=1769.66, am_max=1969.66)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=['[Lambda_b0 -> D+ D- Lambda0]cc'],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToDpDmLambdaDD_DpToHHH(process):
    d = d_builder.make_dplus_to_hhh(am_min=1769.66, am_max=1969.66)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d, d, hyperon],
        descriptors=['[Lambda_b0 -> D+ D- Lambda0]cc'],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToHH(process):  #2x2-body
    d_HH = d_builder.make_dzero_to_hh(
        am_min=1764.84,
        am_max=1964.84,
    )
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_HH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKsLLHH(process):  #3(LL)x3(LL)-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_KsLLHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKsDDHH(process):  #3(DD)x3(DD)-body
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_KsDDHH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToHHHH(process):  #4x4-body
    d_hhhh = d_builder.make_dzero_to_hhhh(
        am_min=1764.84,
        am_max=1964.84,
    )
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_hhhh, d_hhhh, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToHH_D0ToKsLLHH(process):  #2x3(LL)-body
    d_HH = d_builder.make_dzero_to_hh(
        am_min=1764.84,
        am_max=1964.84,
    )
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_KsLLHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToHH_D0ToKsDDHH(process):  #2x3(DD)-body
    d_HH = d_builder.make_dzero_to_hh(
        am_min=1764.84,
        am_max=1964.84,
    )
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToHH_D0ToHHHH(process):  #2x4-body
    d_HH = d_builder.make_dzero_to_hh(
        am_min=1764.84,
        am_max=1964.84,
    )
    d_HHHH = d_builder.make_dzero_to_hhhh(
        am_min=1764.84,
        am_max=1964.84,
    )
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKsLLHH_D0ToKsDDHH(process):  #3(LL)x3(DD)-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKsLLHH_D0ToHHHH(process):  #3(LL)x4-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    d_HHHH = d_builder.make_dzero_to_hhhh(
        am_min=1764.84,
        am_max=1964.84,
    )
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaLL_D0ToKsDDHH_D0ToHHHH(process):  #3(DD)x4-body
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        am_min=1764.84,
        am_max=1964.84,
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10)
    d_HHHH = d_builder.make_dzero_to_hhhh(
        am_min=1764.84,
        am_max=1964.84,
    )
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[d_KsDDHH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        bcvtx_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToHH(process):  #2x2-body
    d_HH = d_builder.make_dzero_to_hh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_HH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKsLLHH(process):  #3(LL)x3(LL)-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_KsLLHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKsDDHH(process):  #3(DD)x3(DD)-body
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_KsDDHH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToHHHH(process):  #4x4-body
    d_HHHH = d_builder.make_dzero_to_hhhh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_HHHH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ])
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToHH_D0ToKsLLHH(process):  #2x3(LL)-body
    d_HH = d_builder.make_dzero_to_hh()
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_KsLLHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToHH_D0ToKsDDHH(process):  #2x3(DD)-body
    d_HH = d_builder.make_dzero_to_hh()
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToHH_D0ToHHHH(process):  #2x4-body
    d_HH = d_builder.make_dzero_to_hh()
    d_HHHH = d_builder.make_dzero_to_hhhh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_HH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKsLLHH_D0ToKsDDHH(process):  #3(LL)x3(DD)-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_KsDDHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKsLLHH_D0ToHHHH(process):  #3(LL)x4-body
    d_KsLLHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    d_HHHH = d_builder.make_dzero_to_hhhh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_KsLLHH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToD0D0LambdaDD_D0ToKsDDHH_D0ToHHHH(process):  #3(DD)x4-body
    d_KsDDHH = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    d_HHHH = d_builder.make_dzero_to_hhhh()
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[d_KsDDHH, d_HHHH, hyperon],
        descriptors=[
            'Lambda_b0 -> D0 D0 Lambda0', 'Lambda_b~0 -> D0 D0 Lambda~0'
        ],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_LbToDstDLambdaLL_DstToD0Pi_D0ToHH_DToHHH(process):
    dz = d_builder.make_dzero_to_hh()
    dplus = d_builder.make_dplus_to_hhh(
        am_min=1769.66,
        am_max=1969.66,
    )
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ],
        bcvtx_fromdst_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToDstDLambdaDD_DstToD0Pi_D0ToHH_DToHHH(process):
    dz = d_builder.make_dzero_to_hh()
    dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg


@check_process
def make_LbToDstDLambdaLL_DstToD0Pi_D0ToKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpipipi()
    dplus = d_builder.make_dplus_to_hhh(am_min=1769.66, am_max=1969.66)
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ],
        bcvtx_fromdst_sep_min=0 * mm,
        bcvtx_sep_min_child2=0 * mm)
    return line_alg


@check_process
def make_LbToDstDLambdaDD_DstToD0Pi_D0ToKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpipipi()
    dplus = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dz)
    hyperon = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_lb(
        particles=[dst, dplus, hyperon],
        descriptors=[
            '[Lambda_b0 -> D*(2010)- D+ Lambda0]cc',
            '[Lambda_b0 -> D*(2010)+ D- Lambda0]cc'
        ])
    return line_alg

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Definition of some B2OC B2DDh lines
Lines added by Chen Chen, Ruiting Ma and Huanhuan Liu
Adjustments and additional lines by Linxuan Zhu
Additional B->D0anti-D0X lines added by Paras Naik

Line algorithms constructed by a generic `make_b2ddh` builder
which parses the decay descriptors of each line.

All lines in this file have same cuts(*) for child particles,
defined in `b2ddh_hlt2_kwargs` and `b2ddh_spruce_kwargs`.
Therefore builders in this file shouldn't be used for anyone else,
because tuning cut for these lines shouldn't affect others.

(*) if a D0 decays to Kshh, then different cuts are specified to the D builder, see below

For other B2DDh lines, add them in `b_to_ddh.py`
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm  #, picosecond

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

#from RecoConf.reconstruction_objects import make_pvs

#import Functors as F
#from Functors.math import in_range
#from Hlt2Conf.algorithms_thor import ParticleContainersMerger
#from Hlt2Conf.algorithms_thor import ParticleCombiner
#from Functors import require_all

####################################
# Tools to parse decay descriptors #
####################################


def std_notation(particle, builders):
    std_name = particle.replace("-", "+").replace("~", "")
    if std_name not in builders.keys():
        raise KeyError("{} not in particles: {}".format(
            std_name, builders.keys()))
    return std_name


def parse_descriptor(descriptors, KS0="LL"):
    """
    All descriptors must contain the same set and order of combined particles
    """
    descriptor = descriptors[0]
    descriptor = descriptor.replace("[", "").replace("]", "").replace(
        "cc", "").replace("CC", "")
    daus = descriptor.split("->")[1]
    daus_list = daus.split(" ")
    while "" in daus_list:
        daus_list.remove("")
    for i, dau in enumerate(daus_list):
        if dau == "KS0": daus_list[i] = "KS0_" + KS0
    return daus_list


b2ddh_builders = {
    "D+": d_builder.make_dplus_to_kpipi_or_kkpi,
    "D_s+": d_builder.make_dsplus_to_hhh,
    "D0": d_builder.make_dzero_to_kpi_or_kpipipi,
    "D0kpi": d_builder.make_dzero_to_kpi,
    "D0k3pi": d_builder.make_dzero_to_kpipipi,
    "D0_hh": d_builder.make_dzero_to_hh,
    "D0_KsLLhh": d_builder.make_dzero_to_kshh,
    "D0_KsDDhh": d_builder.make_dzero_to_kshh,
    "D0_hhhh": d_builder.make_dzero_to_hhhh,
    "D*(2010)+": d_builder.make_dstar_to_dzeropi_cf,
    "K*(892)0": basic_builder.make_kstar0,
    "KS0_DD": basic_builder.make_ks_DD,
    "KS0_LL": basic_builder.make_ks_LL,
    "K+": basic_builder.make_tight_kaons,
    "pi+": basic_builder.make_tight_pions,
    "phi(1020)": basic_builder.make_phi,
    "rho(770)0": basic_builder.make_rho0
}

b2ddh_hlt2_kwargs = {
    "rho(770)0": {
        "am_min": 250. * MeV,
        "am_max": 3000. * MeV,
        "pi_pidk_max": 5,
        "adoca12_max": 0.2 * mm,
    },
    "K*(892)0": {
        "am_min": 600. * MeV,
        "am_max": 2000. * MeV,
    },
    "phi(1020)": {
        "am_min": 900. * MeV,
        "am_max": 3000. * MeV,
    },
    #"D+": {
    #    "pi_pidk_max": 20,
    #    "k_pidk_min": -10,
    #},
    #"K+": {
    #    "k_pidk_min": 0,
    #},
}

dplus_loose_cut = {
    "D+": {
        "am_min": 1769.66,
        "am_max": 1969.66,
    },
}

dplus_tight_cut = {
    "D+": {
        "k_pidk_min": 0,
        "am_min": 1769.66,
        "am_max": 1969.66,
    },
}

dzero_loose_cut = {
    "D0": {
        #"k_pidk_min": 0,
        #"pi_pidk_max"  : 1,
        "am_min": 1764.84,
        "am_max": 1964.84,
        #"bpvipchi2_min": 5,
    },
}

dstar_loose_cut = {
    "D*(2010)+": {
        "am_min": 1764.84,
        "am_max": 1964.84,
    }
}

ds_loose_cut = {
    "D_s+": {
        "am_min": 1868.35,
        "am_max": 2068.35,
    },
}

dzero_tight_cut = {
    "D0": {
        "k_pidk_min": 0,
        "am_min": 1764.84,
        "am_max": 1964.84,
    },
}

dstar_tight_cut = {
    "D*(2010)+": {
        "k_pidk_min": 0,
        "am_min": 1764.84,
        "am_max": 1964.84,
    }
}

ds_tight_cut = {
    "D_s+": {
        "k_pidk_min": 0,
        "am_min": 1868.35,
        "am_max": 2068.35,
    },
}

b2ddh_spruce_kwargs = b2ddh_hlt2_kwargs


def make_b2ddh(process,
               descriptors,
               cuts=None,
               KS0="LL",
               D0ToHH=False,
               D0ToKsLLHH=False,
               D0ToKsDDHH=False,
               D0ToHHHH=False,
               DsToKHH=False,
               D0ToKpi=False,
               D0ToK3pi=False,
               b2dstdsth_sep_min=None,
               b2dstdh_sep_min=None,
               b2ddh_sep_min=None,
               AllowDiffInputsForSameIDChildren=False):
    assert KS0 in ['LL', 'DD']

    if cuts: kwargs_cuts = cuts
    elif process == 'spruce': kwargs_cuts = b2ddh_spruce_kwargs
    elif process == 'hlt2': kwargs_cuts = b2ddh_hlt2_kwargs

    daughters = parse_descriptor(descriptors, KS0=KS0)
    daughters = [std_notation(dau, b2ddh_builders) for dau in daughters]

    builders = {}
    if not AllowDiffInputsForSameIDChildren:
        for dau in daughters:
            if dau in builders.keys(): continue
            kwargs = kwargs_cuts.get(dau)
            if not kwargs: kwargs = {}
            if dau == 'D0' and D0ToHH:  # 2x2-body
                builders[dau] = b2ddh_builders['D0_hh'](**kwargs)
            elif (dau == 'D_s+' and DsToKHH) or (dau == 'D_s-' and DsToKHH):
                builders[dau] = b2ddh_builders['D_s+'](
                    **kwargs, DsToKKPi=True, DsToKPiPi=True, DsToPiPiPi=False)
            elif dau == 'D0' and D0ToKpi:
                builders[dau] = b2ddh_builders['D0kpi'](**kwargs)
            elif dau == 'D0' and D0ToK3pi:
                builders[dau] = b2ddh_builders['D0k3pi'](**kwargs)
            elif dau == 'D0' and D0ToKsLLHH:  #3(LL)x3(LL)-body
                builders[dau] = b2ddh_builders['D0_KsLLhh'](
                    k_shorts=basic_builder.make_ks_LL(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0' and D0ToKsLLHH:  #3(DD)x3(DD)-body
                builders[dau] = b2ddh_builders['D0_KsDDhh'](
                    k_shorts=basic_builder.make_ks_DD(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0' and D0ToHHHH:  #4x4-body
                builders[dau] = b2ddh_builders['D0_hhhh'](**kwargs)
            else:
                builders[dau] = b2ddh_builders[dau](**kwargs)
        particles = [builders[dau] for dau in daughters]
    else:  # AllowDiffInputsForSameIDChildren is True --- this is only needed if there are two identical D mesons in the decay descriptor, i.e. "D0 D0"
        for i, dau in enumerate(daughters):
            if dau == "D0":
                daughters[i] = "D0_" + str(
                    i
                )  # this only works if "D0 D0" appears immediately after "->" in the decay descriptor
        for dau in daughters:
            if dau in builders.keys(): continue
            kwargs = kwargs_cuts.get('D0' if (
                (dau == 'D0_0') or (dau == 'D0_1')) else dau)
            if not kwargs: kwargs = {}
            if dau == 'D0_0' and D0ToHH and D0ToKsLLHH:  #2x3(LL)-body
                builders[dau] = b2ddh_builders['D0_hh'](**kwargs)
            elif dau == 'D0_1' and D0ToHH and D0ToKsLLHH:
                builders[dau] = b2ddh_builders['D0_KsLLhh'](
                    k_shorts=basic_builder.make_ks_LL(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_0' and D0ToHH and D0ToKsDDHH:  #2x3(DD)-body
                builders[dau] = b2ddh_builders['D0_hh'](**kwargs)
            elif dau == 'D0_1' and D0ToHH and D0ToKsDDHH:
                builders[dau] = b2ddh_builders['D0_KsDDhh'](
                    k_shorts=basic_builder.make_ks_DD(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_0' and D0ToHH and D0ToHHHH:  #2x4-body
                builders[dau] = b2ddh_builders['D0_hh'](**kwargs)
            elif dau == 'D0_1' and D0ToHH and D0ToHHHH:
                builders[dau] = b2ddh_builders['D0_hhhh'](**kwargs)
            elif dau == 'D0_0' and D0ToKsLLHH and D0ToKsDDHH:  #3(LL)x3(DD)-body
                builders[dau] = b2ddh_builders['D0_KsLLhh'](
                    k_shorts=basic_builder.make_ks_LL(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_1' and D0ToKsLLHH and D0ToKsDDHH:
                builders[dau] = b2ddh_builders['D0_KsDDhh'](
                    k_shorts=basic_builder.make_ks_DD(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_0' and D0ToKsLLHH and D0ToHHHH:  #3(LL)x4-body
                builders[dau] = b2ddh_builders['D0_KsLLhh'](
                    k_shorts=basic_builder.make_ks_LL(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_1' and D0ToKsLLHH and D0ToHHHH:
                builders[dau] = b2ddh_builders['D0_hhhh'](**kwargs)
            elif dau == 'D0_0' and D0ToKsDDHH and D0ToHHHH:  #3(DD)x4-body
                builders[dau] = b2ddh_builders['D0_KsDDhh'](
                    k_shorts=basic_builder.make_ks_DD(),
                    pi_pidk_max=20,
                    k_pidk_min=-10)
            elif dau == 'D0_1' and D0ToKsDDHH and D0ToHHHH:
                builders[dau] = b2ddh_builders['D0_hhhh'](**kwargs)
            else:
                builders[dau] = b2ddh_builders[dau](**kwargs)
        particles = [builders[dau] for dau in daughters]

    return b_builder.make_b2x(
        particles=particles,
        descriptors=descriptors,
        name='B2OCB2DDHBuilder_{hash}',
        sum_pt_min=6. * GeV,
        b2dstdsth_sep_min=b2dstdsth_sep_min,
        b2dstdh_sep_min=b2dstdh_sep_min,
        b2ddh_sep_min=b2ddh_sep_min,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren)


#################################
# Definition of line algorithms #
#################################


@check_process
def make_BdToD0DK_D0ToKPiOrKPiPiPi_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D- K+', 'B0 -> D0 D+ K-'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToD0DPi_D0ToKPi_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D- pi+', 'B0 -> D0 D+ pi-'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
            **dzero_loose_cut,
        },
        D0ToKpi=True,
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToD0DPi_D0ToKPiPiPi_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D- pi+', 'B0 -> D0 D+ pi-'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
            **dzero_loose_cut,
        },
        D0ToK3pi=True,
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstD0K_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D0 K-', 'B0 -> D*(2010)- D0 K+'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstD0Pi_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D0 pi-', 'B0 -> D*(2010)- D0 pi+'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDpDmKsDD_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D+ D- KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDpDmKsLL_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D+ D- KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstDmKsDD_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D- KS0', 'B0 -> D*(2010)- D+ KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstDmKSLL_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D- KS0', 'B0 -> D*(2010)- D+ KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstpDstmKsDD_DstpToD0Pi_D0ToKPiorKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDstpDstmKsLL_DstpToD0Pi_D0ToKPiorKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToHH(process, MVACut=0.2):  # 2x2-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKsLLHH(process, MVACut=0.2):  # 3(LL)x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKsDDHH(process, MVACut=0.2):  # 3(DD)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToHHHH(process, MVACut=0.2):  # 4x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHHHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):  # 2x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsLLHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):  # 2x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToHH_D0ToHHHH(process, MVACut=0.2):  # 2x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKsLLHH_D0ToKsDDHH(process,
                                            MVACut=0.2):  # 3(LL)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKsLLHH_D0ToHHHH(process, MVACut=0.2):  # 3(LL)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKsDDHH_D0ToHHHH(process, MVACut=0.2):  # 3(DD)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToHH(process, MVACut=0.2):  # 2x2-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKsLLHH(process, MVACut=0.2):  # 3(LL)x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKsDDHH(process, MVACut=0.2):  # 3(DD)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToHHHH(process, MVACut=0.2):  # 4x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHHHH=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):  # 2x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsLLHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):  # 2x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToHH_D0ToHHHH(process, MVACut=0.2):  # 2x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKsLLHH_D0ToKsDDHH(process,
                                            MVACut=0.2):  # 3(LL)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKsLLHH_D0ToHHHH(process, MVACut=0.2):  # 3(LL)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKsDDHH_D0ToHHHH(process, MVACut=0.2):  # 3(DD)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToDpDmKst_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B0 -> D+ D- K*(892)0", "B0 -> D- D+ K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstpDmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B0 -> D*(2010)+ D- K*(892)0", "B0 -> D*(2010)- D+ K*(892)~0"
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
            **dstar_loose_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDstmDpKst_DstmToD0Pi_D0ToKPiOrKPiPiPi_DpToHHH(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B0 -> D*(2010)- D+ K*(892)0", "B0 -> D*(2010)+ D- K*(892)~0"
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
            **dstar_loose_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDspDsmKst_DspToKHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B0 -> D_s+ D_s- K*(892)0", "B0 -> D_s- D_s+ K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
        },
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDpDmK_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["[B+ -> D+ D- K+]cc"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstDK_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)+ D- K+]cc', '[B+ -> D*(2010)- D+ K+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstpDstmK_DstToD0Pi_D0ToKPiOrKPiPiPi(process, MVACut=0.2):
    cuts = {
        "K+": {
            "k_pidk_min": -5,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)+ D*(2010)- K+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
            **cuts,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDspDsmK_DspToKHH(process, MVACut=0.2):
    cuts = {
        "K+": {
            "k_pidk_min": -5
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=["[B+ -> D_s+ D_s- K+]cc"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_tight_cut,
            **cuts,
        },
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDpDmPi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["[B+ -> D+ D- pi+]cc"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstDPi_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            '[B+ -> D*(2010)+ D- pi+]cc', '[B+ -> D*(2010)- D+ pi+]cc'
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstpDstmPi_DstpToD0Pi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)+ D*(2010)- pi+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDspDsmPi_DspToKHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["[B+ -> D_s+ D_s- pi+]cc"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
        },
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToD0DpKsDD_D0ToKPiOrKPiPiPi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D0 D+ KS0', 'B- -> D0 D- KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToD0DpKsLL_D0ToKPiOrKPiPiPi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D0 D+ KS0', 'B- -> D0 D- KS0'],
        KS0="LL",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dzero_tight_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstpD0KsDD_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D*(2010)+ D0 KS0', 'B- -> D*(2010)- D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstpD0KsLL_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D*(2010)+ D0 KS0', 'B- -> D*(2010)- D0 KS0'],
        KS0="DD",
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToD0DpKst_D0ToKPi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B+ -> D0 D+ K*(892)0", "B- -> D0 D- K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dzero_tight_cut,
        },
        D0ToKpi=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToD0DpKst_D0ToKPiPiPi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B+ -> D0 D+ K*(892)0", "B- -> D0 D- K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_tight_cut,
            **dzero_tight_cut,
        },
        D0ToK3pi=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDstpD0Kst_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B+ -> D*(2010)+ D0 K*(892)0", "B- -> D*(2010)- D0 K*(892)~0"
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_tight_cut,
            **dstar_tight_cut,
        },
        b2dstdh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDspDmPi_DspToHHH_DmToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D_s+ D- pi+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
            **dplus_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDstmDspPi_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)- D_s+ pi+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_loose_cut,
            **ds_loose_cut,
        },
        b2dstdh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDsD0Pi_DsToHHH_D0ToKPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 pi-', 'B0 -> D_s- D0 pi+'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        D0ToKpi=True,
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BdToDsD0Pi_DsToHHH_D0ToKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 pi-', 'B0 -> D_s- D0 pi+'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        D0ToK3pi=True,
        b2ddh_sep_min=(0) * mm)

    return line_alg


@check_process
def make_BuToDsD0Phi_DspToKHH_D0ToKPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 phi(1020)', 'B0 -> D_s- D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        D0ToKpi=True,
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDsD0Phi_DspToKHH_D0ToKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 phi(1020)', 'B0 -> D_s- D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        D0ToK3pi=True,
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToHH(process, MVACut=0.2):  # 2x2-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKsLLHH(process, MVACut=0.2):  # 3(LL)x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKsDDHH(process, MVACut=0.2):  # 3(LL)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToHHHH(process, MVACut=0.2):  # 4x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHHHH=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):  # 2x3(LL)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsLLHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):  # 2x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToHH_D0ToHHHH(process, MVACut=0.2):  # 2x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKsLLHH_D0ToKsDDHH(process,
                                           MVACut=0.2):  # 3(LL)x3(DD)-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToKsDDHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKsLLHH_D0ToHHHH(process, MVACut=0.2):  # 3(LL)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsLLHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKsDDHH_D0ToHHHH(process, MVACut=0.2):  # 3(LL)x4-body
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
        },
        b2ddh_sep_min=(0) * mm,
        D0ToKsDDHH=True,
        D0ToHHHH=True,
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToDpDmPhi_DpToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D+ D- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dplus_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDsDPhi_DsToHHH_DToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D- phi(1020)', 'B0 -> D_s- D+ phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
            **dplus_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDspDsmPhi_DspToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D_s- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
        },
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDstDsPhi_DstToD0Pi_D0ToKPiOrKPiPiPi_DsToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            'B0 -> D*(2010)- D_s+ phi(1020)', 'B0 -> D*(2010)+ D_s- phi(1020)'
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_loose_cut,
            **ds_loose_cut,
        },
        b2dstdh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiOrKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDstpDstmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            'B+ -> D*(2010)+ D*(2010)- K*(892)0',
            'B- -> D*(2010)- D*(2010)+ K*(892)~0'
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dstar_tight_cut,
        },
        b2dstdsth_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDsD0Rho0_DspToKHH_D0ToKPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D_s+ D0 rho(770)0', 'B- -> D_s- D0 rho(770)0'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        DsToKHH=True,
        D0ToKpi=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BuToDsD0Rho0_DspToKHH_D0ToKPiPiPi(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D_s+ D0 rho(770)0', 'B- -> D_s- D0 rho(770)0'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **dzero_loose_cut,
            **ds_loose_cut,
        },
        DsToKHH=True,
        D0ToK3pi=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDspDmRho0_DspToKHH_DmToHHH(process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D- rho(770)0', 'B0 -> D_s- D+ rho(770)0'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
            **dplus_loose_cut,
        },
        DsToKHH=True,
        b2ddh_sep_min=(0) * mm)
    return line_alg


@check_process
def make_BdToDstmDspRho0_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToKHH(
        process, MVACut=0.2):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            'B0 -> D*(2010)- D_s+ rho(770)0', 'B0 -> D*(2010)+ D_s- rho(770)0'
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **ds_loose_cut,
            **dstar_loose_cut,
        },
        DsToKHH=True,
        b2dstdh_sep_min=(0) * mm)
    return line_alg

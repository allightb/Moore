# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This tightware is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Neutron Lines
"""
from GaudiKernel.SystemOfUnits import MeV, GeV
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import (cbaryon_builder,
                                                     b_builder, basic_builder)


# https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p3/bhadroncompleteevent/strippinglb2lclcnlc2pkpibeauty2charmline.html
@check_process
def make_Lb0ToLcpLcmN0(process):
    lc = cbaryon_builder.make_lc_to_pkpi(
        pi_pidk_max=20.,
        p_pidp_min=-10.,
        k_pidk_min=-10.,
    )

    line_alg = b_builder.make_b2x(
        [lc, lc],
        descriptors=["Lambda_b0 -> Lambda_c+ Lambda_c~-"],
        am_min=4000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=4000 * MeV,
        am_max_vtx=7000 * MeV,
    )

    return line_alg


@check_process
def make_Lb0ToPbarPN0(process):
    proton = basic_builder.make_protons(
        p_pidp_min=0, p_min=1.5 * GeV, pt_min=500 * MeV)

    line_alg = b_builder.make_b2x(
        [proton, proton],
        descriptors=["Lambda_b0 -> p+ p~-"],
        am_min=4000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=4000 * MeV,
        am_max_vtx=7000 * MeV,
    )

    return line_alg

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DD lines
"""

#from Hlt2Conf.algorithms_thor import ParticleContainersMerger
from Hlt2Conf.lines.b_to_open_charm.utils import check_process
from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder

##############################################
# B decaying to two charged charm mesons
##############################################


@check_process
def make_BdToDpDm_DpToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, d], descriptors=['B0 -> D+ D-'])
    return line_alg


@check_process
def make_BdToDspDsm_DspToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, d], descriptors=['B0 -> D_s+ D_s-'])
    return line_alg


@check_process
def make_BdToDspDm_DspToHHH_DmToHHH(process):
    if process == 'spruce':
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        d = d_builder.make_dplus_to_hhh()
        ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[ds, d], descriptors=['[B0 -> D_s+ D-]cc'])
    return line_alg


@check_process
def make_BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        d = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, d], descriptors=['[B0 -> D*(2010)+ D-]cc'])
    return line_alg


@check_process
def make_BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        d = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh()
        d = d_builder.make_dplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, d], descriptors=['[B0 -> D*(2010)+ D-]cc'])
    return line_alg


@check_process
def make_BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh()
        ds = d_builder.make_dsplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, ds], descriptors=['[B0 -> D*(2010)+ D_s-]cc'])
    return line_alg


@check_process
def make_BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH(process):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero = d_builder.make_dzero_to_hhhh()
        ds = d_builder.make_dsplus_to_hhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, ds], descriptors=['[B0 -> D*(2010)+ D_s-]cc'])
    return line_alg


@check_process
def make_BdToDstpDstm_DstpToD0Pi_D0ToHH(process):
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()
    dst_hh = d_builder.make_dstar_to_dzeropi(dzero_hh)
    line_alg = b_builder.make_b2x(
        particles=[dst_hh, dst_hh], descriptors=['B0 -> D*(2010)+ D*(2010)-'])
    return line_alg


@check_process
def make_BdToDstpDstm_DstpToD0Pi_D0ToHH_D0ToHHHH(process):
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()
        dzero_hhhh = d_builder.make_dzero_to_hhhh()
    dst_hh = d_builder.make_dstar_to_dzeropi(dzero_hh)
    dst_hhhh = d_builder.make_dstar_to_dzeropi(dzero_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst_hh, dst_hhhh],
        descriptors=['[B0 -> D*(2010)+ D*(2010)-]cc'])
    return line_alg


@check_process
def make_BdToDstpDstm_DstpToD0Pi_D0ToHHHH(process):
    if process == 'spruce':
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hhhh = d_builder.make_dzero_to_hhhh()
    dst_hhhh = d_builder.make_dstar_to_dzeropi(dzero_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst_hhhh, dst_hhhh],
        descriptors=['B0 -> D*(2010)+ D*(2010)-'])
    return line_alg


##############################################
# B decaying to two neutral charm mesons
##############################################

# Split of previous BdToD0D0_D0ToHHOrHHHH line into three lines


@check_process
def make_BdToD0D0_D0ToHH(process):  # 2x2-body modes (isolated)
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()

    line_alg = b_builder.make_b2x(
        particles=[dzero_hh, dzero_hh], descriptors=['B0 -> D0 D0'])
    return line_alg


@check_process
def make_BdToD0D0_D0ToHH_D0ToHHHH(process):  # 2x4-body modes (isolated)
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()
        dzero_hhhh = d_builder.make_dzero_to_hhhh()

    line_alg = b_builder.make_b2x(
        particles=[dzero_hh, dzero_hhhh],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0_D0ToHHHH(process):  # 4x4-body modes (isolated)
    if process == 'spruce':
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_hhhh = d_builder.make_dzero_to_hhhh()

    line_alg = b_builder.make_b2x(
        particles=[dzero_hhhh, dzero_hhhh], descriptors=['B0 -> D0 D0'])
    return line_alg


# Additional D decays modes added by Paras Naik


@check_process
def make_BdToD0D0_D0ToHH_D0ToKsLLHH(process):  # 2x3(LL)-body modes
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)

    line_alg = b_builder.make_b2x(
        particles=[dzero_hh, dzero_KshhLL],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0_D0ToHH_D0ToKsDDHH(process):  # 2x3(DD)-body modes
    if process == 'spruce':
        dzero_hh = d_builder.make_dzero_to_hh(
            pi_pidk_max=None, k_pidk_min=None)
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)
    if process == 'hlt2':
        dzero_hh = d_builder.make_dzero_to_hh()
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)

    line_alg = b_builder.make_b2x(
        particles=[dzero_hh, dzero_KshhDD],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0_D0ToKsLLHH(process):  # 3(LL)x3(LL)-body modes
    if process == 'spruce':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
    if process == 'hlt2':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)

    line_alg = b_builder.make_b2x(
        particles=[dzero_KshhLL, dzero_KshhLL], descriptors=['B0 -> D0 D0'])
    return line_alg


@check_process
def make_BdToD0D0_D0ToKsLLHH_D0ToKsDDHH(process):  # 3(LL)x3(DD)-body modes
    if process == 'spruce':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)
    if process == 'hlt2':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)

    line_alg = b_builder.make_b2x(
        particles=[dzero_KshhLL, dzero_KshhDD],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0_D0ToKsDDHH(process):  # 3(DD)x3(DD)-body modes
    if process == 'spruce':
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)
    if process == 'hlt2':
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)

    line_alg = b_builder.make_b2x(
        particles=[dzero_KshhDD, dzero_KshhDD], descriptors=['B0 -> D0 D0'])
    return line_alg


@check_process
def make_BdToD0D0_D0ToKsLLHH_D0ToHHHH(process):  # 3(LL)x4-body modes
    if process == 'spruce':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_hhhh = d_builder.make_dzero_to_hhhh()

    line_alg = b_builder.make_b2x(
        particles=[dzero_KshhLL, dzero_hhhh],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


@check_process
def make_BdToD0D0_D0ToKsDDHH_D0ToHHHH(process):  # 3(DD)x4-body modes
    if process == 'spruce':
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_hhhh = d_builder.make_dzero_to_hhhh(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10)
        dzero_hhhh = d_builder.make_dzero_to_hhhh()

    line_alg = b_builder.make_b2x(
        particles=[dzero_KshhDD, dzero_hhhh],
        descriptors=['B0 -> D0 D0'],
        AllowDiffInputsForSameIDChildren=True)
    return line_alg


##############################################
# B decaying to one charged and one neutral charm meson
##############################################


@check_process
def make_BuToD0Dp_D0ToHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            pi_pidk_max=None, k_pidk_min=None, am_min=1769.62, am_max=1969.62)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=64.)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, bpvvdchi2_min=64.)
    b = b_builder.make_b2dd(
        particles=[dzero, dp], descriptors=['B+ -> D0 D+', 'B- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Dsp_D0ToHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            pi_pidk_max=None, k_pidk_min=None, am_min=1868.47, am_max=2068.47)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=64.)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, bpvvdchi2_min=64.)
    b = b_builder.make_b2dd(
        particles=[dp, dzero], descriptors=['B+ -> D_s+ D0', 'B- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Dp_D0ToHHHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            pi_pidk_max=None, k_pidk_min=None, am_min=1769.62, am_max=1969.62)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=64.)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, bpvvdchi2_min=64.)
    b = b_builder.make_b2dd(
        particles=[dzero, dp], descriptors=['B+ -> D0 D+', 'B- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Dsp_D0ToHHHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            pi_pidk_max=None, k_pidk_min=None, am_min=1868.47, am_max=2068.47)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=64.)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, bpvvdchi2_min=64.)
    b = b_builder.make_b2dd(
        particles=[dp, dzero], descriptors=['B+ -> D_s+ D0', 'B- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDstD0_DstToD0Pi_D0ToHH_D0ToHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=64.)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    b = b_builder.make_b2dstd(
        particles=[dst, dzero],
        descriptors=['B+ -> D*(2010)+ D0', 'B- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=64.)
        dzero2 = d_builder.make_dzero_to_hh(bpvvdchi2_min=64.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_b2dstd(
        particles=[dst, dzero1],
        descriptors=['B+ -> D*(2010)+ D0', 'B- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_hh(bpvvdchi2_min=64.)
        dzero2 = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=64.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_b2dstd(
        particles=[dst, dzero1],
        descriptors=['B+ -> D*(2010)+ D0', 'B- -> D*(2010)+ D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=64.)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    b = b_builder.make_b2dstd(
        particles=[dst, dzero],
        descriptors=['B+ -> D*(2010)+ D0', 'B- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


##############################################
# Bc decaying to one charged and one neutral charm meson
##############################################


@check_process
def make_BcToD0Dp_D0ToHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            bpvvdchi2_min=49., am_min=1769.62, am_max=1969.62)
    b = b_builder.make_bc2dd(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dsp_D0ToHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
        dp = d_builder.make_dsplus_to_kpkmpip(
            bpvvdchi2_min=49., am_min=1868.47, am_max=2068.47)
    b = b_builder.make_bc2dd(
        particles=[dp, dzero],
        descriptors=['B_c+ -> D_s+ D0', 'B_c- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dp_D0ToHHHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            bpvvdchi2_min=49., am_min=1769.62, am_max=1969.62)
    b = b_builder.make_bc2dd(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dsp_D0ToHHHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
        dp = d_builder.make_dsplus_to_kpkmpip(
            bpvvdchi2_min=49., am_min=1868.47, am_max=2068.47)
    b = b_builder.make_bc2dd(
        particles=[dp, dzero],
        descriptors=['B_c+ -> D_s+ D0', 'B_c- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dp_D0ToKsDDHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_DD())
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            bpvvdchi2_min=49., am_min=1769.62, am_max=1969.62)
    b = b_builder.make_bc2dd(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dsp_D0ToKsDDHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_DD())
        dp = d_builder.make_dsplus_to_kpkmpip(
            bpvvdchi2_min=49., am_min=1868.47, am_max=2068.47)
    b = b_builder.make_bc2dd(
        particles=[dp, dzero],
        descriptors=['B_c+ -> D_s+ D0', 'B_c- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dp_D0ToKsLLHH_DpToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            am_min=1769.62, am_max=1969.62, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_LL())
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            bpvvdchi2_min=49., am_min=1769.62, am_max=1969.62)
    b = b_builder.make_bc2dd(
        particles=[dzero, dp], descriptors=['B_c+ -> D0 D+', 'B_c- -> D0 D-'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToD0Dsp_D0ToKsLLHH_DspToHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dp = d_builder.make_dsplus_to_kpkmpip(
            am_min=1868.47, am_max=2068.47, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_LL())
        dp = d_builder.make_dsplus_to_kpkmpip(
            bpvvdchi2_min=49., am_min=1868.47, am_max=2068.47)
    b = b_builder.make_bc2dd(
        particles=[dp, dzero],
        descriptors=['B_c+ -> D_s+ D0', 'B_c- -> D_s- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHH_D0ToHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
        dzero2 = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
        dzero2 = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)+ D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_LL())
        dzero2 = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_DD())
        dzero2 = d_builder.make_dzero_to_hh(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsLLHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_LL())
        dzero2 = d_builder.make_dzero_to_kpipipi(bpvvdchi2_min=49.)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsDDHH(process, MVACut=0.2):
    if process == 'spruce':
        dzero1 = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=None,
            k_pidk_min=None)
        dzero2 = d_builder.make_dzero_to_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        dzero1 = d_builder.make_dzero_to_kshh(
            bpvvdchi2_min=49., k_shorts=basic_builder.make_ks_DD())
        dzero2 = d_builder.make_dzero_to_kpipipi(
            bpvvdchi2_min=49., pi_pidk_max=None, k_pidk_min=None)
    dst = d_builder.make_dstar_to_dzeropi(dzero2)
    b = b_builder.make_bc2dstd(
        particles=[dst, dzero1],
        descriptors=['B_c+ -> D*(2010)+ D0', 'B_c- -> D*(2010)- D0'])
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


#############################################################################
# Form the Tbc -> D0 D0, D0 --> Kpi & K3pi
##############################################################################


@check_process
def make_TbcToD0D0_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    return b_builder.make_tbc2ccx(
        particles=[Dz, Dz],
        descriptors=['Xi_bc0 -> D0 D0'],
    )

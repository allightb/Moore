###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds B2OC B decays, and defines the common default cuts applied to the B2X combinations
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from Hlt2Conf.algorithms_thor import ParticleContainersMerger
from PyConf import configurable

from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all


@configurable
def _make_b2x(
        particles,
        descriptor,
        name="B2OCB2XCombiner_{hash}",
        am_min=5050 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20.,  # was 10 in Run1+2
        bpvipchi2_max=25.,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999,
        bcvtx_sep_min=None,
        bcvtx_fromdst_sep_min=None,
        b2dstdsth_sep_min=None,
        b2dstdh_sep_min=None,
        b2ddh_sep_min=None,
        bcvtx_sep_min_child2=None,
        bpvfdchi2_min=None,
        AllowDiffInputsForSameIDChildren=False):
    '''
    Default B decay maker: defines default cuts and B mass range.
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if bcvtx_fromdst_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.CHILD(1, F.END_VZ)) -
                        F.END_VZ) > bcvtx_fromdst_sep_min

    if b2ddh_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > b2ddh_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > b2ddh_sep_min

    if b2dstdsth_sep_min is not None:
        vertex_code &= (
            F.CHILD(1, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdsth_sep_min
        vertex_code &= (
            F.CHILD(2, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdsth_sep_min

    if b2dstdh_sep_min is not None:
        vertex_code &= (
            F.CHILD(1, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdh_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > b2dstdh_sep_min

    if bcvtx_sep_min_child2 is not None:
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min_child2

    if bpvfdchi2_min is not None:
        vertex_code = require_all(vertex_code,
                                  F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren)


''' for iteration on descriptors elements '''


@configurable
def make_b2x(particles,
             descriptors,
             name='B2OCB2XMerger_{hash}',
             **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def make_loose_mass_b2x(particles,
                        descriptors,
                        name='B2OCB2XLooseMassMerger_{hash}',
                        am_min=4850 * MeV,
                        am_max=6000 * MeV,
                        am_min_vtx=4850 * MeV,
                        am_max_vtx=6000 * MeV,
                        **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2x(
                particles=particles,
                descriptor=descriptor,
                name=name,
                am_min=am_min,
                am_max=am_max,
                am_min_vtx=am_min_vtx,
                am_max_vtx=am_max_vtx,
                **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_lifetime_unbiased_b2x(particles,
                                descriptor,
                                name="B2OCB2XLTUCombiner_{hash}",
                                am_min=5050 * MeV,
                                am_max=5650 * MeV,
                                am_min_vtx=5050 * MeV,
                                am_max_vtx=5650 * MeV,
                                sum_pt_min=5 * GeV,
                                vtx_chi2pdof_max=20.):  # was 10 in Run1+2
    '''
    LifeTime Unbiased B decay maker: defines default cuts and B mass range.
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_lifetime_unbiased_b2x(particles,
                               descriptors,
                               name='B2OCB2XLTUMerger_{hash}',
                               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_lifetime_unbiased_b2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2chh(
        particles,
        descriptor,
        name="B2OCB2DHHCombiner_{hash}",
        am_min=4900 * MeV,
        am_max=6100 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=6000 * MeV,
        sum_pt_min=5.5 * GeV,
        vtx_chi2pdof_max=20,  # was 10 in Run1+2
        bpvipchi2_max=9,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.99995,
        hh_asumpt_min=1 * GeV,
        hh_am_max=5.2 * GeV,
        hh_adoca12_max=0.5 * mm,
        hbach_pt_min=500 * MeV,
        hbach_p_min=5 * GeV,
        bcvtx_sep_min=-0.5 * mm,
        chad_is_grandchild=False):
    '''
    Specialised 3-body B decay maker
    '''

    combination12_code = require_all(F.MASS < hh_am_max,
                                     F.MAX(F.P) > hbach_p_min,
                                     F.MAX(F.PT) > hbach_pt_min,
                                     F.SUM(F.PT) > hh_asumpt_min,
                                     F.SDOCA(1, 2) < hh_adoca12_max)

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if chad_is_grandchild:
        vertex_code &= (
            F.CHILD(3, F.CHILD(1, F.END_VZ)) - F.END_VZ) > bcvtx_sep_min
    else:
        vertex_code &= (F.CHILD(3, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_b2chh(particles,
               descriptors,
               name='B2OCB2CHHMerger_{hash}',
               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2chh(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2cch(
        particles,
        descriptor,
        name="B2OCB2DDHCombiner_{hash}",
        am_min=5000 * MeV,
        am_max=5800 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=5800 * MeV,
        sum_pt_min=5.5 * GeV,
        vtx_chi2pdof_max=20,  # was 10 in Run1+2
        bpvipchi2_max=9,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.99995,
        hbach_pt_min=500 * MeV,
        hbach_p_min=5 * GeV,
        bcvtx_sep_min=-0.5 * mm,
        AllowDiffInputsForSameIDChildren=False):
    '''
    Specialised 3-body double charm B decay maker
    '''

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min,
        (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren)


''' for iteration on descriptors elements '''


@configurable
def make_b2cch(particles,
               descriptors,
               name='B2OCB2CCHMerger_{hash}',
               **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2cch(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def make_b2cstarhh(particles,
                   descriptors,
                   name='B2OCB2CSTARHHMerger_{hash}',
                   **decay_arguments):
    assert len(descriptors) > 0

    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2chh(
                particles=particles,
                descriptor=descriptor,
                chad_is_grandchild=True,
                **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2chhh(
        particles,
        descriptor,
        name="B2OCB2DHHHCombiner_{hash}",
        am_min=5050 * MeV,
        am_max=5750 * MeV,
        am_min_vtx=5100 * MeV,
        am_max_vtx=5700 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=10,  # was 10 in Run1+2
        bpvipchi2_max=9,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.99993,
        hhh_asumpt_min=1.25 * GeV,
        hhh_am_max=4.0 * GeV,
        adoca_max=0.6 * mm,
        hbach_pt_min=500 * MeV,
        hbach_p_min=5 * GeV,
        bcvtx_sep_min=None):
    '''
    Specialised 4-body Hb --> (h h h) H_c  decay maker
    '''

    combination123_code = require_all(F.MASS < hhh_am_max,
                                      F.SUM(F.PT) > hhh_asumpt_min,
                                      F.SDOCA(1, 3) < adoca_max,
                                      F.SDOCA(2, 3) < adoca_max)
    # missing: AHASCHILD(ISBASIC & HASTRACK & (PT > {habch_pt_min}) & (P > {hbach_p_min}) )

    combination12_code = require_all(F.SDOCA(1, 2) < adoca_max)

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min,
        F.SDOCA(1, 4) < adoca_max,
        F.SDOCA(2, 4) < adoca_max,
        F.SDOCA(3, 4) < adoca_max)

    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= (F.CHILD(4, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_b2chhh(particles,
                descriptors,
                name='B2OCB2CHHHMerger_{hash}',
                **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2chhh(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


''' b hadrons builders using the make_b2x and make_b2chhh defined above '''


@configurable
def _make_b2dd(
        particles,
        descriptor,
        name="B2OCB2DDCombiner_{hash}",
        am_min=4800 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=4800 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20.,  # was 10 in Run1+2
        bpvipchi2_max=20.,
        bpvltime_min=0.05 * picosecond,
        bpvdira_min=0.999,
        bcvtx_sep_min=-1 * mm):
    '''
    Specialised B->DD builder
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.CHILD(1, F.SUM(F.PT)) + F.CHILD(2, F.SUM(F.PT)) > sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2dd(particles,
              descriptors,
              name='B2OCB2DDMerger_{hash}',
              **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2dd(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def _make_b2dstd(
        particles,
        descriptor,
        name="B2OCB2DstDCombiner_{hash}",
        am_min=4800 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=4800 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20.,  # was 10 in Run1+2
        bpvipchi2_max=20.,
        bpvltime_min=0.05 * picosecond,
        bpvdira_min=0.999,
        bcvtx_sep_min=-1 * mm):
    '''
    Specialised B->D*D builder
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.CHILD(1, F.CHILD(1, F.SUM(F.PT))) + F.CHILD(2, F.SUM(F.PT)) >
        sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= (
            F.CHILD(1, F.CHILD(1, F.END_VZ)) - F.END_VZ) > bcvtx_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2dstd(particles,
                descriptors,
                name='B2OCB2DstDMerger_{hash}',
                **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2dstd(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def make_bc2x(particles,
              descriptors,
              name="B2OCBC2XCombiner_{hash}",
              am_min=6000 * MeV,
              am_max=6600 * MeV,
              am_min_vtx=6000 * MeV,
              am_max_vtx=6600 * MeV,
              sum_pt_min=5 * GeV,
              vtx_chi2pdof_max=20,
              bpvipchi2_max=20,
              bpvltime_min=0.05 * picosecond,
              bpvdira_min=0.999):

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        sum_pt_min=sum_pt_min,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        bpvipchi2_max=bpvipchi2_max,
        bpvltime_min=bpvltime_min,
        bpvdira_min=bpvdira_min)


@configurable
def make_bc2dd(particles,
               descriptors,
               name="B2OCBC2DDCombiner_{hash}",
               am_min=5050 * MeV,
               am_max=6700 * MeV,
               am_min_vtx=5050 * MeV,
               am_max_vtx=6700 * MeV,
               sum_pt_min=5.5 * GeV,
               **decay_arguments):

    return make_b2dd(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        sum_pt_min=sum_pt_min,
        **decay_arguments)


@configurable
def make_bc2dstd(particles,
                 descriptors,
                 name="B2OCBC2DstDCombiner_{hash}",
                 am_min=5050 * MeV,
                 am_max=6700 * MeV,
                 am_min_vtx=5050 * MeV,
                 am_max_vtx=6700 * MeV,
                 sum_pt_min=5.5 * GeV,
                 **decay_arguments):

    return make_b2dstd(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        sum_pt_min=sum_pt_min,
        **decay_arguments)


@configurable
def make_lb(particles,
            descriptors,
            name="B2OCLb2XCombiner_{hash}",
            am_min=5400 * MeV,
            am_max=5900 * MeV,
            am_min_vtx=5400 * MeV,
            am_max_vtx=5900 * MeV,
            **decay_arguments):

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_lb2chh(particles,
                descriptors,
                name="B2OCLb2XcHHCombiner_{hash}",
                am_min=5400 * MeV,
                am_max=6100 * MeV,
                am_min_vtx=5400 * MeV,
                am_max_vtx=6100 * MeV,
                **decay_arguments):

    return make_b2chh(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_lb2lchhh(particles,
                  descriptors,
                  name="B2OCLb2LcHHHCombiner_{hash}",
                  am_min=5400 * MeV,
                  am_max=5900 * MeV,
                  am_min_vtx=5400 * MeV,
                  am_max_vtx=5900 * MeV,
                  **decay_arguments):

    return make_b2chhh(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_xib(particles,
             descriptors,
             name="B2OCXib2XCombiner_{hash}",
             am_min=5600 * MeV,
             am_max=6100 * MeV,
             am_min_vtx=5600 * MeV,
             am_max_vtx=6100 * MeV,
             **decay_arguments):

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_xib2chh(particles,
                 descriptors,
                 name="B2OCXib2LcHHCombiner_{hash}",
                 am_min=5500 * MeV,
                 am_max=6900 * MeV,
                 am_min_vtx=5500 * MeV,
                 am_max_vtx=6900 * MeV,
                 **decay_arguments):

    return make_b2chh(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_xib2xichhh(particles,
                    descriptors,
                    name="B2OCXib2XicHHHCombiner_{hash}",
                    am_min=5600 * MeV,
                    am_max=6100 * MeV,
                    am_min_vtx=5600 * MeV,
                    am_max_vtx=6100 * MeV,
                    **decay_arguments):

    return make_b2chhh(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_omegab(particles,
                descriptors,
                name="B2OCOmegab2XCombiner_{hash}",
                am_min=5850 * MeV,
                am_max=6350 * MeV,
                am_min_vtx=5850 * MeV,
                am_max_vtx=6350 * MeV,
                **decay_arguments):

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_loose_omegab(particles,
                      descriptors,
                      name="B2OCOmegab2XLooseCombiner_{hash}",
                      am_min=5400 * MeV,
                      am_max=6800 * MeV,
                      am_min_vtx=5500 * MeV,
                      am_max_vtx=6700 * MeV,
                      **decay_arguments):

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_omegab2omegachhh(particles,
                          descriptors,
                          name="B2OCOmegab2OmegacHHHCombiner_{hash}",
                          am_min=5850 * MeV,
                          am_max=6350 * MeV,
                          am_min_vtx=5850 * MeV,
                          am_max_vtx=6350 * MeV,
                          **decay_arguments):

    return make_b2chhh(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def _make_xibc2x(
        particles,
        descriptor,
        name="B2OCXibc2XCombiner_{hash}",
        am_min=6300 * MeV,
        am_max=7900 * MeV,
        am_min_vtx=6400 * MeV,
        am_max_vtx=7800 * MeV,
        sum_pt_min=6.0 * GeV,
        vtx_chi2pdof_max=10,  # was 10 in Run1+2
        bpvipchi2_max=16,
        bpvltime_min=0.1 *
        picosecond,  # reduced from 0.2 ps for regular b-hadron
        bpvdira_min=0.999,
        adocachi2_max=25,
        comb12_cut_add=None,
        comb123_cut_add=None,
        vtx_cut_add=None,
        comb_cut_add=None):
    '''
    Default Xibc decay maker: defines default cuts and Xibc mass range.
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if comb_cut_add is not None:
        combination_code &= comb_cut_add

    if vtx_cut_add is not None:
        vertex_code &= vtx_cut_add

    Nbody = len(descriptor.split("->")[1].split())
    if Nbody >= 3:
        combination12_code = require_all(
            F.SDOCACHI2(1, 2) < adocachi2_max, F.MASS < am_max)
        if comb12_cut_add is not None:
            combination12_code &= comb12_cut_add

    if Nbody >= 4:
        combination123_code = require_all(
            F.SDOCACHI2(1, 3) < adocachi2_max,
            F.SDOCACHI2(2, 3) < adocachi2_max, F.MASS < am_max)
        if comb123_cut_add is not None:
            combination123_code &= comb123_cut_add

    if Nbody == 2:
        return ParticleCombiner(
            particles,
            name=name,
            DecayDescriptor=descriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)
    elif Nbody == 3:
        return ParticleCombiner(
            particles,
            name=name,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)
    else:
        return ParticleCombiner(
            particles,
            name=name,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            Combination123Cut=combination123_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)


''' for iteration on descriptors elements '''


@configurable
def make_xibc2x(particles,
                descriptors,
                name='B2OCXibc2XMerger_{hash}',
                **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_xibc2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


@configurable
def make_tbc2x(
        # knowing that if Tbc is stable it should be not far from
        # m(B) + m(D) ~ 7145 MeV, definitely not higher
        particles,
        descriptors,
        name="B2OCTbc2XCombiner_{hash}",
        am_min=6700 * MeV,
        am_max=7600 * MeV,
        am_min_vtx=6800 * MeV,
        am_max_vtx=7500 * MeV,
        **decay_arguments):
    return make_xibc2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)


@configurable
def make_xibc2cx(particles,
                 descriptors,
                 name="B2OCXibc2CXCombiner_{hash}",
                 bcvtx_sep_min=-0.5 * mm,
                 comb_cut_add=None,
                 sum_pt_hbach_min=0 * GeV,
                 **decay_arguments):

    if bcvtx_sep_min:
        vtx_cut_add = (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if sum_pt_hbach_min > 0:
        comb_cut_sumpt = F.SUM(F.PT) - F.CHILD(1, F.PT) > sum_pt_hbach_min
        if comb_cut_add is None: comb_cut_add = comb_cut_sumpt
        else: comb_cut_add &= comb_cut_sumpt

    return make_xibc2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        vtx_cut_add=vtx_cut_add,
        comb_cut_add=comb_cut_add,
        **decay_arguments)


@configurable
def make_xibc2ccx(particles,
                  descriptors,
                  name="B2OCXibc2CCXCombiner_{hash}",
                  bcvtx_sep_min=-0.5 * mm,
                  bc2vtx_sep_min=-0.5 * mm,
                  comb_cut_add=None,
                  sum_pt_hbach_min=0 * GeV,
                  **decay_arguments):

    if bcvtx_sep_min is not None:
        vtx_cut_add = (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if bc2vtx_sep_min is not None:
        vtx_cut_add &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bc2vtx_sep_min

    if sum_pt_hbach_min > 0:
        comb_cut_sumpt = F.SUM(F.PT) - F.CHILD(1, F.PT) - F.CHILD(
            2, F.PT) > sum_pt_hbach_min
        if comb_cut_add is None: comb_cut_add = comb_cut_sumpt
        else: comb_cut_add &= comb_cut_sumpt

    return make_xibc2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        vtx_cut_add=vtx_cut_add,
        comb_cut_add=comb_cut_add,
        **decay_arguments)


@configurable
def make_tbc2cx(particles,
                descriptors,
                name="B2OCTbc2CXCombiner_{hash}",
                bcvtx_sep_min=-0.5 * mm,
                comb_cut_add=None,
                sum_pt_hbach_min=0 * GeV,
                **decay_arguments):

    if bcvtx_sep_min:
        vtx_cut_add = (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if sum_pt_hbach_min > 0:
        comb_cut_sumpt = F.SUM(F.PT) - F.CHILD(1, F.PT) > sum_pt_hbach_min
        if comb_cut_add is None: comb_cut_add = comb_cut_sumpt
        else: comb_cut_add &= comb_cut_sumpt

    return make_tbc2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        vtx_cut_add=vtx_cut_add,
        comb_cut_add=comb_cut_add,
        **decay_arguments)


@configurable
def make_tbc2ccx(particles,
                 descriptors,
                 name="B2OCTbc2CCXCombiner_{hash}",
                 bcvtx_sep_min=-0.5 * mm,
                 bc2vtx_sep_min=-0.5 * mm,
                 comb_cut_add=None,
                 sum_pt_hbach_min=0 * GeV,
                 **decay_arguments):

    if bcvtx_sep_min is not None:
        vtx_cut_add = (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if bc2vtx_sep_min is not None:
        vtx_cut_add &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bc2vtx_sep_min

    if sum_pt_hbach_min > 0:
        comb_cut_sumpt = F.SUM(F.PT) - F.CHILD(1, F.PT) - F.CHILD(
            2, F.PT) > sum_pt_hbach_min
        if comb_cut_add is None: comb_cut_add = comb_cut_sumpt
        else: comb_cut_add &= comb_cut_sumpt

    return make_tbc2x(
        particles=particles,
        descriptors=descriptors,
        name=name,
        vtx_cut_add=vtx_cut_add,
        comb_cut_add=comb_cut_add,
        **decay_arguments)


@configurable
def make_bmeson_neutral_adder(particles,
                              descriptor,
                              name="B2OCBmesonNeutralAdder_{hash}",
                              am_min=4700 * MeV,
                              am_max=5800 * MeV,
                              am_min_vtx=4700 * MeV,
                              am_max_vtx=5800 * MeV,
                              sum_pt_min=4 * GeV,
                              vtx_chi2pdof_max=9.,
                              bpvipchi2_max=9.):

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        ParticleCombiner="ParticleAdder")


@configurable
def make_bhadron_neutral_adder(particles,
                               descriptor,
                               name="B2OCBhadronNeutralAdder",
                               am_min=5050 * MeV,
                               am_max=5650 * MeV,
                               am_min_vtx=5050 * MeV,
                               am_max_vtx=5650 * MeV):

    combination_code = in_range(am_min, F.MASS, am_max)

    vertex_code = in_range(am_min_vtx, F.MASS, am_max_vtx)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        ParticleCombiner="ParticleAdder")


@configurable
def make_omegab_with_neutral(particles,
                             descriptor,
                             name="B2OCOmegab2XNeutralCombiner_{hash}",
                             am_min=5400 * MeV,
                             am_max=6800 * MeV,
                             am_min_vtx=5500 * MeV,
                             am_max_vtx=6700 * MeV,
                             **decay_arguments):

    return make_bhadron_neutral_adder(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        am_min_vtx=am_min_vtx,
        am_max_vtx=am_max_vtx,
        **decay_arguments)

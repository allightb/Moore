###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Definition of B2OC selections for the Dh topology

Non trivial imports:
basic builders for D mesons and h, prefilters, BDT post filter

Returns:
every function returns a line_alg

"""
from Hlt2Conf import standard_particles
from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond, mm  # will be needed once the full set of lines is back

from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter  # , b_bdt_filter  #, b_to_dh_bdt_filter
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

##############################################
# From the BToDh_Builder
##############################################


@check_process
def make_BdToDsmK_DsmToKpKmPim(process, MVACut=0.1):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dsplus_to_kpkmpip(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=0)
        d = d_builder.make_dsplus_to_kpkmpip()
    b = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmK_DsmToKpPipPim(process, MVACut=0.1):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dsplus_to_kppippim(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=0)
        d = d_builder.make_dsplus_to_kppippim()
    b = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmK_DsmToPipPipPim(process, MVACut=0.1):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dsplus_to_pippippim(pi_pidk_max=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=0)
        d = d_builder.make_dsplus_to_pippippim()
    b = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmPi_DsmToKpKmPim(process, MVACut=0.1):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dsplus_to_kpkmpip(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions(pi_pidk_max=0)
        d = d_builder.make_dsplus_to_kpkmpip()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D_s- pi+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmPi_DsmToKpPipPim(process, MVACut=0.1):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dsplus_to_kppippim(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions(pi_pidk_max=0)
        d = d_builder.make_dsplus_to_kppippim()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D_s- pi+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmPi_DsmToPipPipPim(process, MVACut=0.1):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dsplus_to_pippippim(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions(pi_pidk_max=0)
        d = d_builder.make_dsplus_to_pippippim()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D_s- pi+]cc'],
        am_min=5000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=7000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsmPi_DsmToKpKmPim_Calib(process,
                                      MVACut=0.5):  # request from Vava
    pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    d = d_builder.make_dsplus_to_kpkmpip()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D_s- pi+]cc'],
        am_min=5000 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=6000 * MeV)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BdToDmPi_DmToPimPimKp(process, MVACut=0.1):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions(pi_pidk_max=0)
        d = d_builder.make_dplus_to_kmpippip()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D- pi+]cc'],
        am_min=4950 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=4950 * MeV,
        am_max_vtx=6000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDmPiSB_DmToPimPimKp(process):
    pion = basic_builder.make_tight_pions(pi_pidk_max=0)
    d = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D- pi+]cc'],
        am_min=5500 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=5500 * MeV,
        am_max_vtx=6000 * MeV)
    return line_alg


@check_process
def make_BdToDmPi_DmToPimPimKp_Calib(process, MVACut=0.5):
    pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    d = d_builder.make_dplus_to_kmpippip()
    b = b_builder.make_b2x(
        particles=[d, pion],
        descriptors=['[B0 -> D- pi+]cc'],
        am_min=4950 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=4950 * MeV,
        am_max_vtx=6000 * MeV)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(b, pvs, MVACut)
    return line_alg


@check_process
def make_BdToDmK_DmToPimPimKp(process, MVACut=0.1):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=0)
        d = d_builder.make_dplus_to_kmpippip()
    b = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D- K+]cc'],
        am_min=4950 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=4950 * MeV,
        am_max_vtx=6000 * MeV)
    if process == 'hlt2':
        pvs = make_pvs()
        line_alg = b_sigmanet_filter(b, pvs, MVACut)
    elif process == 'spruce':
        line_alg = b
    return line_alg


@check_process
def make_BdToDsstmK_DsstmToDsmGamma_DsmToHHH(process):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
        ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = b_builder.make_b2x(
        particles=[dsst, kaon],
        descriptors=['B0 -> D*_s- K+', 'B0 -> D*_s+ K-'],
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond)
    return line_alg


@check_process
def make_BdToDsstmPi_DsstmToDsmGamma_DsmToHHH(process):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
        ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = b_builder.make_b2x(
        particles=[dsst, pion],
        descriptors=['B0 -> D*_s- pi+', 'B0 -> D*_s+ pi-'])
    return line_alg


@check_process
def make_BuToDsstpGamma_DsstpToDspGamma_DspToHHH(process):
    gamma = basic_builder.make_photons(CL_min=0.4, et_min=1.5 * GeV)
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds, coneR2cut=0.23)
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[dsst, gamma],
        descriptor='[B+ -> D*_s+ gamma]cc',
        name="B2OCBNeutralAdder_DsstGamma")
    return line_alg


@check_process
def make_BuToDspGamma_DspToHHH(process):
    gamma = basic_builder.make_photons(CL_min=0.4, et_min=1.5 * GeV)
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[ds, gamma],
        descriptor='[B+ -> D_s+ gamma]cc',
        name="B2OCBNeutralAdder_DsGamma")
    return line_alg


@check_process
def make_BuToDsstpPi0Resolved_DsstpToDspGamma_DspToHHH(process):
    pi0 = basic_builder.make_resolved_pi0s(pt_min=1.8 * GeV)
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds, coneR2cut=0.23)
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[dsst, pi0],
        descriptor='[B+ -> D*_s+ pi0]cc',
        name="B2OCBNeutralAdder_DsstResolvedPi0")
    return line_alg


@check_process
def make_BuToDspPi0Resolved_DspToHHH(process):
    pi0 = basic_builder.make_resolved_pi0s(pt_min=1.8 * GeV)
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[ds, pi0],
        descriptor='[B+ -> D_s+ pi0]cc',
        name="B2OCBNeutralAdder_DsResolvedPi0")
    return line_alg


@check_process
def make_BuToDsstpPi0Merged_DsstpToDspGamma_DspToHHH(process):
    pi0 = basic_builder.make_merged_pi0s()
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds, coneR2cut=0.23)
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[dsst, pi0],
        descriptor='[B+ -> D*_s+ pi0]cc',
        name="B2OCBNeutralAdder_DsstMergedPi0")
    return line_alg


@check_process
def make_BuToDspPi0Merged_DspToHHH(process):
    pi0 = basic_builder.make_merged_pi0s()
    if process == 'spruce':
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bmeson_neutral_adder(
        particles=[ds, pi0],
        descriptor='[B+ -> D_s+ pi0]cc',
        name="B2OCBNeutralAdder_DsMergedPi0")
    return line_alg


##############################################
# GLW/ADS lines (Shunan's lines)
##############################################


@check_process
def make_BuToD0K_D0ToHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0PiSB_D0ToHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=5500 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=5500 * MeV,
        am_max_vtx=6000 * MeV,
        bcvtx_sep_min=0 * mm)
    return line_alg


@check_process
def make_BuToD0K_D0ToHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->Dh D->hhpi0
##############################################


@check_process
def make_BuToD0Pi_D0ToHHPi0Resolved(process):
    pi0 = basic_builder.make_resolved_pi0s()
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, pion], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return line_alg


@check_process
def make_BuToD0K_D0ToHHPi0Resolved(process):
    pi0 = basic_builder.make_resolved_pi0s()
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, kaon], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHPi0Merged(process):
    pi0 = basic_builder.make_merged_pi0s()
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, pion], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return line_alg


@check_process
def make_BuToD0K_D0ToHHPi0Merged(process):
    pi0 = basic_builder.make_merged_pi0s()
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
        d = d_builder.make_dzero_to_hhpi0(
            pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, kaon], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHPi0ResolvedWS(process):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0_ws(
        pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, pion], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return line_alg


@check_process
def make_BuToD0K_D0ToHHPi0ResolvedWS(process):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhpi0_ws(
        pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, kaon], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHPi0MergedWS(process):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0_ws(
        pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, pion], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return line_alg


@check_process
def make_BuToD0K_D0ToHHPi0MergedWS(process):
    pi0 = basic_builder.make_merged_pi0s()
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhpi0_ws(
        pi0=pi0, pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_b2x(
        particles=[d, kaon], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return line_alg


##############################################
# GGSZ lines (Mikkel's lines)
##############################################


@check_process
def make_BuToD0Pi_D0ToKsLLHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsLLHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# GGSZ lines for part reco D decays
##############################################
# Lines for partially reconstructed GGSZ lines with wider mass windows
# but (still to be implemented) tighter cuts


@check_process
def make_BuToD0Pi_PartialD0ToKsLLHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_PartialD0ToKsDDHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_PartialD0ToKsLLHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_PartialD0ToKsDDHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_PartialD0ToKsLLHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_PartialD0ToKsDDHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_PartialD0ToKsLLHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_PartialD0ToKsDDHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1000 * MeV)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV,
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->D*h GGSZ lines (Nathan's Lines)
##############################################


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHH(process, MVACut=0.7):
    if process == 'spruce':
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->Dh D->4h
##############################################


@check_process
def make_BuToD0K_D0ToHHHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1784.84 * MeV,
        am_max=1944.84 * MeV,
        adoca12_max=0.2 * mm,
        adoca13_max=0.2 * mm,
        adoca14_max=0.2 * mm,
        adoca23_max=0.2 * mm,
        adoca24_max=0.2 * mm,
        adoca34_max=0.2 * mm,
        vchi2pdof_max=4,
        bpvipchi2_min=4)
    if process == 'hlt2':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, kaon],
            descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
            bcvtx_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    elif process == 'spruce':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, kaon],
            descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
            bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1784.84 * MeV,
        am_max=1944.84 * MeV,
        adoca12_max=0.2 * mm,
        adoca13_max=0.2 * mm,
        adoca14_max=0.2 * mm,
        adoca23_max=0.2 * mm,
        adoca24_max=0.2 * mm,
        adoca34_max=0.2 * mm,
        vchi2pdof_max=4,
        bpvipchi2_min=4)
    if process == 'hlt2':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, pion],
            descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
            bcvtx_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    elif process == 'spruce':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, pion],
            descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
            bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToHHHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhhh_ws(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1784.84 * MeV,
        am_max=1944.84 * MeV,
        adoca12_max=0.2 * mm,
        adoca13_max=0.2 * mm,
        adoca14_max=0.2 * mm,
        adoca23_max=0.2 * mm,
        adoca24_max=0.2 * mm,
        adoca34_max=0.2 * mm,
        vchi2pdof_max=4,
        bpvipchi2_min=4)
    if process == 'hlt2':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, kaon],
            descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
            bcvtx_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    elif process == 'spruce':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, kaon],
            descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
            bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToHHHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh_ws(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1784.84 * MeV,
        am_max=1944.84 * MeV,
        adoca12_max=0.2 * mm,
        adoca13_max=0.2 * mm,
        adoca14_max=0.2 * mm,
        adoca23_max=0.2 * mm,
        adoca24_max=0.2 * mm,
        adoca34_max=0.2 * mm,
        vchi2pdof_max=4,
        bpvipchi2_min=4)
    if process == 'hlt2':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, pion],
            descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
            bcvtx_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    elif process == 'spruce':
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[d, pion],
            descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
            bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->Dh D->Ks0pi0
##############################################


@check_process
def make_BuToD0Pi_D0ToKsLLPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_LL(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsLLPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_LL(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_DD(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_DD(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_LL(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_LL(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_DD(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_kspi0(
        k_shorts=basic_builder.make_ks_DD(), pi0=pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->Dh D->Ks0hhpi0
##############################################


@check_process
def make_BuToD0Pi_D0ToKsLLHHPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_LL(am_min=467 * MeV, am_max=527 * MeV),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0,
        am_min=1814.84,
        am_max=1914.84)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm,
        sum_pt_min=6 * GeV,
        bpvfdchi2_min=55)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHHPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_DD(am_min=467 * MeV, am_max=527 * MeV),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0,
        am_min=1814.84,
        am_max=1914.84)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm,
        sum_pt_min=6 * GeV,
        bpvfdchi2_min=55)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHHPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_LL(am_min=467 * MeV, am_max=527 * MeV),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0,
        am_min=1814.84,
        am_max=1914.84)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm,
        sum_pt_min=6 * GeV,
        bpvfdchi2_min=55)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHHPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_DD(am_min=467 * MeV, am_max=527 * MeV),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0,
        am_min=1814.84,
        am_max=1914.84)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm,
        sum_pt_min=6 * GeV,
        bpvfdchi2_min=55)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsLLHHPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHHPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHHPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHHPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsLLHHWSPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHHWSPi0Resolved(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHHWSPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHHWSPi0Resolved(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s(
        pt_min=750. * MeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsLLHHWSPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0Pi_D0ToKsDDHHWSPi0Merged(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsLLHHWSPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_LL(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToD0K_D0ToKsDDHHWSPi0Merged(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s(
        pt_min=2. * GeV, p_min=5. * GeV, CL_min=0.4)
    d = d_builder.make_dzero_to_kshhpi0_ws(
        k_shorts=basic_builder.make_ks_DD(),
        pi0=pi0,
        pi_pidk_max=0,
        k_pidk_min=0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        bcvtx_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->D*(Dgam, Dpi0)h D->hh
##############################################


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzerogamma(
            dzero, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    if process == 'spruce':
        pi0 = basic_builder.make_resolved_pi0s()
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        pi0 = basic_builder.make_resolved_pi0s(pt_min=0.250 * GeV)
        dzero = d_builder.make_dzero_to_hh(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzeropi0(
            dzero, pi0, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzerogamma(
            dzero, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    if process == 'spruce':
        pi0 = basic_builder.make_resolved_pi0s()
        dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        pi0 = basic_builder.make_resolved_pi0s(pt_min=0.250 * GeV)
        dzero = d_builder.make_dzero_to_hh(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzeropi0(
            dzero, pi0, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh_ws(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzerogamma(
            dzero, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    if process == 'spruce':
        pi0 = basic_builder.make_resolved_pi0s()
        dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        pi0 = basic_builder.make_resolved_pi0s(pt_min=0.250 * GeV)
        dzero = d_builder.make_dzero_to_hh_ws(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzeropi0(
            dzero, pi0, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, pion],
            descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    if process == 'spruce':
        dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        dzero = d_builder.make_dzero_to_hh_ws(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzerogamma(
            dzero, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    if process == 'spruce':
        pi0 = basic_builder.make_resolved_pi0s()
        dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
        dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm)
    elif process == 'hlt2':
        pi0 = basic_builder.make_resolved_pi0s(pt_min=0.250 * GeV)
        dzero = d_builder.make_dzero_to_hh_ws(
            pi_pidk_max=20,
            k_pidk_min=-10,
            am_min=1794.84 * MeV,
            am_max=1934.84 * MeV)
        dzerost = d_builder.make_dzerost_to_dzeropi0(
            dzero, pi0, dst_pt_min=1.5 * GeV)
        line_alg = b_builder.make_loose_mass_b2x(
            particles=[dzerost, kaon],
            descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
            bcvtx_fromdst_sep_min=0 * mm,
            bpvdira_min=0.9999,
            bpvipchi2_max=15.,
            bpvfdchi2_min=50.)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


##############################################
# B->D*(Dgam, Dpi0)h D->hhhh
##############################################


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHH(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToHHHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHH(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHHWS(process, MVACut=0.7):
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, pion],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Gamma_D0ToHHHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


@check_process
def make_BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHHWS(process, MVACut=0.7):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzerost, kaon],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'],
        bcvtx_fromdst_sep_min=0 * mm)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


#
# test line(s), add any new line above this(these)
#
@check_process
def make_BdToDsmK_DsmToHHH_FEST(process):
    kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    d = d_builder.make_dsplus_to_hhh(
        am_min=1500 * MeV, pi_pidk_max=None, k_pidk_min=None)
    line_alg = b_builder.make_b2x(
        particles=[d, kaon],
        descriptors=['[B0 -> D_s- K+]cc'],
        am_min=3500 * MeV,
        am_min_vtx=3500 * MeV,
        sum_pt_min=2.5 * GeV,
        bpvltime_min=0.05 * picosecond)
    return line_alg


###########################################
# Bc to open charm lines
###########################################


# Form the Bc -> D0 K, D0 -> K Pi
@check_process
def make_BcToD0K_D0ToHH(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.1 * picosecond)
    return line_alg


# Form the Bc -> D0bar Pi-, D0bar -> K- Pi+
@check_process
def make_BcToD0Pi_D0ToHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.1 * picosecond)
    return line_alg


# Form the Bc -> D0 K- WS, D0 -> K+ Pi-
@check_process
def make_BcToD0K_D0ToHHWS(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 Pi- WS, D0 -> K+ Pi-
@check_process
def make_BcToD0Pi_D0ToHHWS(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


##############################################
# Bc -> Dh, D->Kspipi
##############################################


# Form the Bc -> D0 Pi-, D0 -> KS Pi+ Pi-  LL
@check_process
def make_BcToD0Pi_D0ToKsLLHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 Pi-, D0 -> KS Pi+ Pi-  DD
@check_process
def make_BcToD0Pi_D0ToKsDDHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 K-, D0 -> KS Pi+ Pi-  LL
@check_process
def make_BcToD0K_D0ToKsLLHH(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 K-, D0 -> KS Pi+ Pi-  DD
@check_process
def make_BcToD0K_D0ToKsDDHH(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 Pi- WS, D0 -> KS Pi+ Pi-  LL
@check_process
def make_BcToD0Pi_D0ToKsLLHHWS(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 Pi- WS, D0 -> KS Pi+ Pi-  DD
@check_process
def make_BcToD0Pi_D0ToKsDDHHWS(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 K- WS, D0 -> KS Pi+ Pi-  LL
@check_process
def make_BcToD0K_D0ToKsLLHHWS(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 K- WS, D0 -> KS Pi+ Pi-  DD
@check_process
def make_BcToD0K_D0ToKsDDHHWS(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_kshh_ws(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


######################################
# Bc -> D0 K/ Pi, D0 -> HHHH
######################################


# Form the Bc -> D0 K, D0 -> HHHH
@check_process
def make_BcToD0K_D0ToHHHH(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.2 * picosecond)
    return line_alg


# Form the Bc -> D0 Pi, D0 bar-> HHHH
@check_process
def make_BcToD0Pi_D0ToHHHH(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.2 * picosecond)
    return line_alg


# Form the Bc -> D0 K WS, D0 -> HHHH
@check_process
def make_BcToD0K_D0ToHHHHWS(process):
    kaon = basic_builder.make_tight_kaons()
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, kaon],
        descriptors=['B_c+ -> D0 K+', 'B_c- -> D0 K-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc -> D0 Pi WS, D0 -> HHHH
@check_process
def make_BcToD0Pi_D0ToHHHHWS(process):
    pion = basic_builder.make_tight_pions()
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_bc2x(
        particles=[d, pion],
        descriptors=['B_c+ -> D0 pi+', 'B_c- -> D0 pi-'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D(s)- Kbar*0
@check_process
def make_BcToDsmKst0_DsmToHHH(process):
    kzerost = basic_builder.make_kstar0()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, kzerost],
        descriptors=['[B_c- -> D_s- K*(892)~0]cc'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D- Kbar*0
@check_process
def make_BcToDmKst0_DmToHHH(process):
    kzerost = basic_builder.make_kstar0()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, kzerost],
        descriptors=['[B_c- -> D- K*(892)~0]cc'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D(s)- Kbar*0 WS
@check_process
def make_BcToDsmKst0_DsmToHHHWS(process):
    kzerost = basic_builder.make_kstar0()
    d = d_builder.make_dsplus_to_hhh_ws(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1868.35 * MeV,
        am_max=2068.35 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, kzerost],
        descriptors=['[B_c- -> D_s- K*(892)~0]cc'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D- Kbar*0 WS
@check_process
def make_BcToDmKst0_DmToHHHWS(process):
    kzerost = basic_builder.make_kstar0()
    d = d_builder.make_dplus_to_hhh_ws(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1769.66 * MeV,
        am_max=1969.66 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, kzerost],
        descriptors=['[B_c- -> D- K*(892)~0]cc'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D(s)- phi
@check_process
def make_BcToDsmPhi_DsmToHHH(process, MVACut=0.2):
    phi = standard_particles.make_phi2kk(
        am_max=1040 * MeV)  # consider additional filtering
    d = d_builder.make_dsplus_to_hhh(am_min=1940 * MeV, am_max=2000 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, phi],
        descriptors=['B_c- -> D_s- phi(1020)', 'B_c+ -> D_s+ phi(1020)'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.2 * picosecond)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


# Form the Bc- -> D- phi
@check_process
def make_BcToDmPhi_DmToHHH(process, MVACut=0.2):
    phi = standard_particles.make_phi2kk(
        am_max=1040 * MeV)  # consider additional filtering
    d = d_builder.make_dplus_to_hhh(am_min=1840 * MeV, am_max=1900 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, phi],
        descriptors=['B_c- -> D- phi(1020)', 'B_c+ -> D+ phi(1020)'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
        bpvltime_min=0.2 * picosecond)
    pvs = make_pvs()
    line_alg = b_sigmanet_filter(line_alg, pvs, MVACut)
    return line_alg


# Form the Bc- -> D(s)- Ks LL
@check_process
def make_BcToDsmKsLL_DsmToHHH(process):
    k_shorts = basic_builder.make_ks_LL()
    d = d_builder.make_dsplus_to_hhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1868.35 * MeV,
        am_max=2068.35 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, k_shorts],
        descriptors=['B_c- -> D_s- KS0', 'B_c+ -> D_s+ KS0'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D- Ks LL
@check_process
def make_BcToDmKsLL_DmToHHH(process):
    k_shorts = basic_builder.make_ks_LL()
    d = d_builder.make_dplus_to_hhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1769.66 * MeV,
        am_max=1969.66 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, k_shorts],
        descriptors=['B_c- -> D- KS0', 'B_c+ -> D+ KS0'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D(s)- Ks DD
@check_process
def make_BcToDsmKsDD_DsmToHHH(process):
    k_shorts = basic_builder.make_ks_DD()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, k_shorts],
        descriptors=['B_c- -> D_s- KS0', 'B_c+ -> D_s+ KS0'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D- Ks DD
@check_process
def make_BcToDmKsDD_DmToHHH(process):
    k_shorts = basic_builder.make_ks_DD()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_bc2x(
        particles=[d, k_shorts],
        descriptors=['B_c- -> D- KS0', 'B_c+ -> D+ KS0'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D(s)- f0(980)
@check_process
def make_BcToDsmF0_DsmToHHH(process):
    f0980 = basic_builder.make_f0980_to_pippim()
    d = d_builder.make_dsplus_to_hhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1868.35 * MeV,
        am_max=2068.35 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, f0980],
        descriptors=['B_c- -> D_s- f_0(980)', 'B_c+ -> D_s+ f_0(980)'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


# Form the Bc- -> D- f0(980)
@check_process
def make_BcToDmF0_DmToHHH(process):
    f0980 = basic_builder.make_f0980_to_pippim()
    d = d_builder.make_dplus_to_hhh(
        pi_pidk_max=20,
        k_pidk_min=-10,
        am_min=1769.66 * MeV,
        am_max=1969.66 * MeV)
    line_alg = b_builder.make_bc2x(
        particles=[d, f0980],
        descriptors=['B_c- -> D- f_0(980)', 'B_c+ -> D+ f_0(980)'],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV)
    return line_alg


#############################################################################
# Form the Tbc -> D+ K-, D0 KS0, D0 --> Kpi & K3pi, D+ --> Kpipi
##############################################################################
@check_process
def make_TbcToDpKm_DpToKmPipPip(process):
    if process == 'spruce':
        Dp = d_builder.make_dplus_to_kmpippip()
        kaon = basic_builder.make_tightpid_tight_kaons()
    if process == 'hlt2':
        Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
    line_alg = b_builder.make_tbc2cx(
        particles=[Dp, kaon], descriptors=['[Xi_bc0 -> D+ K-]cc'])
    return line_alg


@check_process
def make_TbcToD0KsLL_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    k_shorts = basic_builder.make_ks_LL()
    line_alg = b_builder.make_tbc2cx(
        particles=[Dz, k_shorts], descriptors=['Xi_bc0 -> D0 KS0'])
    return line_alg


@check_process
def make_TbcToD0KsDD_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    k_shorts = basic_builder.make_ks_DD()
    line_alg = b_builder.make_tbc2cx(
        particles=[Dz, k_shorts], descriptors=['Xi_bc0 -> D0 KS0'])
    return line_alg

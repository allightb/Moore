###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV

from Hlt2Conf.lines.b_to_open_charm.utils import check_process
from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

####################################
#########TT
####################################


# Form the Lb -> Lambda D0, D0 -> h h
@check_process
def make_LbToLambdaTTD0_D0ToHH(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_hh(am_min=1.83 * GeV, am_max=1.89 * GeV)
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


# Form the Lb -> Lambda D0, D0 -> h h h h
@check_process
def make_LbToLambdaTTD0_D0ToHHHH(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_hhhh(am_min=1.83 * GeV, am_max=1.89 * GeV)
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaTTD0_D0ToHHHHWS(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_hhhh_ws(am_min=1.83 * GeV, am_max=1.89 * GeV)
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


# Form the Lb -> Lambda D0, D0 -> Ks h h
@check_process
def make_LbToLambdaTTD0_D0ToKsLLHH(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'])
    return line_alg


@check_process
def make_LbToLambdaTTD0_D0ToKsDDHH(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'])
    return line_alg


@check_process
def make_LbToLambdaTTD0_D0ToKsTTHH(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_TT(),
        am_min=1.83 * GeV,
        am_max=1.89 * GeV)
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'])
    return line_alg


@check_process
def make_LbToLambdaTTD0_D0ToKsLLHHWS(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaTTD0_D0ToKsDDHHWS(process):
    lm = basic_builder.make_lambda_TT()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


############################################################
#####LL+DD#######
############################################################


# Form the Lb -> Lambda D0, D0 -> h h
@check_process
def make_LbToLambdaLLD0_D0ToHH(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToHH(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


# Form the Lb -> Lambda D0, D0 -> h h h h
@check_process
def make_LbToLambdaLLD0_D0ToHHHH(process):
    if process == 'spruce':
        lm = basic_builder.make_lambda_LL()
        dz = d_builder.make_dzero_to_hhhh()
    elif process == 'hlt2':
        lm = basic_builder.make_lambda_LL()
        dz = d_builder.make_dzero_to_hhhh()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToHHHH(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_hhhh()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaLLD0_D0ToHHHHWS(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_hhhh_ws()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToHHHHWS(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_hhhh_ws()
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


# Form the Lb -> Lambda D0, D0 -> Ks h h
@check_process
def make_LbToLambdaLLD0_D0ToKsLLHH(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaLLD0_D0ToKsDDHH(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToKsLLHH(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToKsDDHH(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[dz, lm],
        descriptors=['Lambda_b0 -> D0 Lambda0', 'Lambda_b~0 -> D0 Lambda~0'],
        bcvtx_sep_min=0,
        bcvtx_sep_min_child2=0)
    return line_alg


@check_process
def make_LbToLambdaLLD0_D0ToKsLLHHWS(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaLLD0_D0ToKsDDHHWS(process):
    lm = basic_builder.make_lambda_LL()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToKsLLHHWS(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


@check_process
def make_LbToLambdaDDD0_D0ToKsDDHHWS(process):
    lm = basic_builder.make_lambda_DD()
    dz = d_builder.make_dzero_to_kshh_ws(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_lb(
        particles=[lm, dz],
        descriptors=['Lambda_b0 -> Lambda0 D0', 'Lambda_b~0 -> Lambda~0 D0'])
    return line_alg


###########################################################
# Form the Xi_bc0 -> D0 p,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi-
##########################################################


@check_process
def make_XibcpToPD0_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        proton = basic_builder.make_tight_protons()
    elif process == 'hlt2':
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        proton = basic_builder.make_tightpid_tight_protons(p_pidp_min=-2)
    line_alg = b_builder.make_xibc2cx(
        particles=[cmeson, proton],
        descriptors=['Xi_bc+ -> D0 p+', 'Xi_bc~- -> D0 p~-'])
    return line_alg


##############################################
# Form the Lb->DsP
##############################################
@check_process
def make_LbToDsmP_DsmToHHH(process):
    if process == 'spruce':
        proton = basic_builder.make_protons(p_pidp_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        proton = basic_builder.make_protons()
        ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_lb(
        particles=[ds, proton], descriptors=['[Lambda_b0 -> D_s- p+]cc'])
    return line_alg


###########################################################
# Form the Lambda_b0 -> Xi- D_s+,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D_s+ -> K+ K- pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_LbToXimDsp_DspToKKPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dsplus_to_kpkmpip(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_lb(
        particles=[hyperons, cmeson],
        descriptors=['[Lambda_b0 -> Xi- D_s+]cc'])
    return line_alg


@check_process
def make_LbToXimDsp_DspToKKPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dsplus_to_kpkmpip(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dsplus_to_kpkmpip()
    line_alg = b_builder.make_lb(
        particles=[hyperons, cmeson],
        descriptors=['[Lambda_b0 -> Xi- D_s+]cc'])
    return line_alg


###########################################################
# Form the Xi_b0 -> Xi- D+,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D+ -> K- pi+ pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_Xib0ToXimDp_DpToKPiPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dplus_to_kmpippip(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson], descriptors=['[Xi_b0 -> Xi- D+]cc'])
    return line_alg


@check_process
def make_Xib0ToXimDp_DpToKPiPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dplus_to_kmpippip(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dplus_to_kmpippip()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson], descriptors=['[Xi_b0 -> Xi- D+]cc'])
    return line_alg


###########################################################
# Form the Xi_b- -> Xi- D0,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D0 -> K- pi+ & K- pi- pi+ pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    if process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson],
        descriptors=['Xi_b- -> Xi- D0', 'Xi_b~+ -> Xi~+ D0'])
    return line_alg


@check_process
def make_XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    line_alg = b_builder.make_xib(
        particles=[hyperons, cmeson],
        descriptors=['Xi_b- -> Xi- D0', 'Xi_b~+ -> Xi~+ D0'])
    return line_alg


###########################################################
# Form the Omega_b- -> Xi- D0,
# Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-, D0 -> K- pi+ & K- pi- pi+ pi+
# Xi- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_LL(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()

    line_alg = b_builder.make_omegab(
        particles=[hyperons, cmeson],
        descriptors=['Omega_b- -> Xi- D0', 'Omega_b~+ -> Xi~+ D0'])
    return line_alg


@check_process
def make_OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi(process):
    if process == 'spruce':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions(pi_pidk_max=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_xim_to_lambda0pim(
            lambdas=basic_builder.make_lambda_DD(),
            pions=basic_builder.make_pions())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    line_alg = b_builder.make_omegab(
        particles=[hyperons, cmeson],
        descriptors=['Omega_b- -> Xi- D0', 'Omega_b~+ -> Xi~+ D0'])
    return line_alg


###########################################################
# Form the Omega_b- -> Omega- D0,
# Omega- -> Lambda0 K-, Lambda0 -> p+ pi-, D0 -> K- pi+ & K- pi- pi+ pi+
# Omega- decays into Lambda0(LL or DD) and K-(L)
##########################################################
@check_process
def make_OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaLLK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_LL(),
            kaons=basic_builder.make_kaons())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    line_alg = b_builder.make_omegab(
        particles=[hyperons, cmeson],
        descriptors=['Omega_b- -> Omega- D0', 'Omega_b~+ -> Omega~+ D0'])
    return line_alg


@check_process
def make_OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaDDK(process):
    if process == 'spruce':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons(k_pidk_min=None))
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
    elif process == 'hlt2':
        hyperons = basic_builder.make_omegam_to_lambda0km(
            lambdas=basic_builder.make_lambda_DD(),
            kaons=basic_builder.make_kaons())
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
    line_alg = b_builder.make_omegab(
        particles=[hyperons, cmeson],
        descriptors=['Omega_b- -> Omega- D0', 'Omega_b~+ -> Omega~+ D0'])
    return line_alg


#############################################################################
# Form the Xibc -> D0 Lambda, D0 --> Kpi & K3pi
# Lambda0 : LL and DD
##############################################################################


@check_process
def make_Xibc0ToD0LambdaLL_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    lambdas = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_xibc2cx(
        particles=[Dz, lambdas],
        descriptors=['Xi_bc0 -> D0 Lambda0', 'Xi_bc~0 -> D0 Lambda~0'])
    return line_alg


@check_process
def make_Xibc0ToD0LambdaDD_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        Dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    if process == 'hlt2':
        Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    lambdas = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_xibc2cx(
        particles=[Dz, lambdas],
        descriptors=['Xi_bc0 -> D0 Lambda0', 'Xi_bc~0 -> D0 Lambda~0'])
    return line_alg


##
#############################################################################
# Form the Xibc -> D+ Lambda, D+ -> Kpipi,
# Lambda0 : LL and DD
##############################################################################


@check_process
def make_XibcpToDpLambdaLL_DpToKmPipPip(process):
    if process == 'spruce':
        Dp = d_builder.make_dplus_to_kmpippip()
    if process == 'hlt2':
        Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
    lambdas = basic_builder.make_lambda_LL()
    line_alg = b_builder.make_xibc2cx(
        particles=[Dp, lambdas], descriptors=['[Xi_bc+ -> D+ Lambda0]cc'])
    return line_alg


@check_process
def make_XibcpToDpLambdaDD_DpToKmPipPip(process):
    if process == 'spruce':
        Dp = d_builder.make_dplus_to_kmpippip()
    if process == 'hlt2':
        Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
    lambdas = basic_builder.make_lambda_DD()
    line_alg = b_builder.make_xibc2cx(
        particles=[Dp, lambdas], descriptors=['[Xi_bc+ -> D+ Lambda0]cc'])
    return line_alg

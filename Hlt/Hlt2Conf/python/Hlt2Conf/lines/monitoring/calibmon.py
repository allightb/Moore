###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration of Hlt2 lines for the CalibMon task
Uses the following postscaled Hlt2 lines:
  - Hlt2CalibMon_DstToD0Pi
  - Hlt2CalibMon_L0ToPPi_LL
  - Hlt2CalibMon_BpToJpsiKp_JpsiToEmEp_Tagged
  - Hlt2CalibMon_JpsiToMupMum_Detached_Tagged
  - Hlt2CalibMon_DiMuon_VeloMuon
  - Hlt2CalibMon_DiMuon_SeedMuon
"""
from GaudiKernel.SystemOfUnits import (TeV, GeV, MeV, mm, micrometer as um, ps)
import Functors as F
from Functors.math import in_range
from PyConf.Algorithms import (
    Monitor__ParticleRange, OdinTypesFilter, HltRoutingBitsFilter,
    MuonProbeToLongMatcher)  #, MuonChamberMonitor, MuonRawInUpgradeToHits
from DecayTreeFitter import DecayTreeFitter
from PyConf.application import (make_odin,
                                default_raw_banks)  #, default_raw_event
from Moore.config import (Hlt2Line, register_line_builder)
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from Hlt2Conf.standard_particles import (
    make_long_kaons, make_long_pions, make_long_protons,
    make_long_electrons_with_brem, make_long_muons, make_ismuon_long_muon)
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter,
                                      ParticleContainersMerger)
from Hlt2Conf.probe_muons import (
    make_velomuon_muons,
    make_seed_muons)  #, make_downstream_muons, make_muonut_muons
# from Hlt2Conf.hlt1_tistos import hlt1_tos_on_any_filter # requires Moore!3006

all_lines = {}


def _calibmon_filters():
    rb_filter = HltRoutingBitsFilter(
        RawBanks=default_raw_banks('HltRoutingBits'),
        # RequireMask=(1 << 14, 0, 0),
        RequireMask=(1 << 1, 0, 0),
        PassOnError=False)
    odin_bb_filter = OdinTypesFilter(
        ODIN=make_odin(), BXTypes=['BeamCrossing'])
    return upfront_reconstruction() + [
        odin_bb_filter, rb_filter,
        require_pvs(make_pvs())
    ]


def _make_hist_list(hist_dict, line_name):
    hist_list = []
    for histname, params in hist_dict.items():
        hist_list.append(
            Monitor__ParticleRange(
                Input=params['input'],
                Variable=params['variable'],
                HistogramName=f"/{line_name}/{histname}",
                Bins=params['bins'],
                Range=params['range'],
            ))
    return hist_list


@register_line_builder(all_lines)
def calibmon_dst_to_d0_pi(name="Hlt2CalibMon_DstToD0Pi"):
    pvs = make_pvs()
    kaons = ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT > 250 * MeV, F.CHI2DOF < 3,
                          F.MINIPCHI2CUT(IPChi2Cut=16, Vertices=pvs))))
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT > 250 * MeV, F.CHI2DOF < 3,
                          F.MINIPCHI2CUT(IPChi2Cut=16, Vertices=pvs))))
    slow_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.P > 1 * GeV, F.PT > 100 * MeV, F.CHI2DOF < 3)))
    d0s = ParticleCombiner(
        [kaons, pions],
        name='CalibMon_D0ToKPi_Combiner_{hash}',
        DecayDescriptor="[D0 -> K- pi+]cc",
        CombinationCut=F.require_all(
            in_range(1769.84 * MeV, F.MASS, 1959.84 * MeV),
            F.PT > 1500 * MeV,
            F.MAXDOCACUT(80 * um),
        ),
        CompositeCut=F.require_all(
            in_range(1800 * MeV, F.MASS, 1915 * MeV),
            F.CHI2DOF < 10,
            F.BPVFDCHI2(pvs) > 49,
            F.BPVDIRA(pvs) > 0.99997,
            (F.MASSWITHHYPOTHESES(
                ("pi-", "K+")) < 1839.84 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi-", "K+")) > 1889.84 * MeV),
            (F.MASSWITHHYPOTHESES(
                ("pi-", "pi+")) < 1839.84 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi-", "pi+")) > 1889.84 * MeV),
            (F.CHILD(1, F.PT) > 1 * GeV) | (F.CHILD(2, F.PT) > 1 * GeV),
        ),
    )
    dsts = ParticleCombiner(
        [d0s, slow_pions],
        name='CalibMon_DstToD0Pi_Combiner_{hash}',
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=F.require_all(
            in_range(1897.76 * MeV, F.MASS,
                     2122.76 * MeV), F.MASS - F.CHILD(1, F.MASS) > 135 * MeV,
            F.MASS - F.CHILD(1, F.MASS) < 160 * MeV),
        CompositeCut=F.require_all(
            in_range(1945.26 * MeV, F.MASS, 2085.26 * MeV), F.CHI2DOF < 10),
    )

    dst_hist_dict = {
        'nPVs': {
            'input': d0s,
            'variable': F.SIZE(pvs),
            'bins': 10,
            'range': (0, 10)
        },
        'D_PT': {
            'input': d0s,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 40000)
        },
        'D_BPVIPCHI2': {
            'input': d0s,
            'variable': F.BPVIPCHI2(pvs),
            'bins': 100,
            'range': (0, 5000)
        },
        'K_ETA': {
            'input': kaons,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'K_P': {
            'input': kaons,
            'variable': F.P,
            'bins': 100,
            'range': (0, 250000)
        },
        'K_PT': {
            'input': kaons,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 20000)
        },
        'K_TCHI2DOF': {
            'input': kaons,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 4)
        },
        'K_GHOSTPROB': {
            'input': kaons,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
        'pi_ETA': {
            'input': pions,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'pi_P': {
            'input': pions,
            'variable': F.P,
            'bins': 100,
            'range': (0, 300000)
        },
        'pi_PT': {
            'input': pions,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 25000)
        },
        'pi_TCHI2DOF': {
            'input': pions,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 4)
        },
        'pi_GHOSTPROB': {
            'input': pions,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
    }

    dst_hist_list = _make_hist_list(dst_hist_dict, name)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [dsts] + dst_hist_list,
        postscale=0)


@register_line_builder(all_lines)
def calibmon_l0_to_p_pi(name="Hlt2CalibMon_L0ToPPi_LL"):
    pvs = make_pvs()
    protons = ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.P > 2 * GeV, F.PT < 1 * TeV, F.CHI2DOF < 4,
                          F.MINIPCHI2CUT(IPChi2Cut=36, Vertices=pvs))))
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.CHI2DOF < 4,
                          F.MINIPCHI2CUT(IPChi2Cut=36, Vertices=pvs))))
    l0lls = ParticleCombiner(
        [protons, pions],
        name='CalibMon_L0ToPPi_LL_Combiner_{hash}',
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=F.require_all(
            in_range(1065.683 * MeV, F.MASS, 1165.683 * MeV)),
        CompositeCut=F.require_all(
            in_range(1095.683 * MeV, F.MASS, 1135.683 * MeV),
            F.CHI2DOF < 30,
            F.MINIPCHI2(pvs) < 50,
            (F.MASSWITHHYPOTHESES(
                ("pi+", "pi-")) < 477.611 * MeV) | (F.MASSWITHHYPOTHESES(
                    ("pi+", "pi-")) > 517.611 * MeV),
            F.BPVLTIME(pvs) > 2 * ps,
        ),
    )

    # We want to fill a bunch of histograms. Define a partial dictionary for Lambda mass plots, and bin edges for differential PID efficiencies
    l_mass_part_dict = {
        'input': l0lls,
        'bins': 100,
        'range': (1095.683, 1135.683)
    }
    pt_bins = (0, 1.5, 3, 6, 1000)
    p_bins = (3, 30, 60, 100)
    eta_bins = (2, 3.5, 4.9)
    pid_bins = (-1000, -10, -5, 0, 5)
    # This is the dict we will use to configure the monitoring alg. Define some 1D histograms at initialization
    l0_hist_dict = {
        'integrated__L0_M__p_PID_P-neg':
        dict(l_mass_part_dict, variable=F.MASS * (F.CHILD(1, F.PID_P) < 0)),
        'integrated__L0_M__p_PID_PK-neg':
        dict(
            l_mass_part_dict,
            variable=F.MASS * (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) < 0)),
        'nPVs': {
            'input': l0lls,
            'variable': F.SIZE(pvs),
            'bins': 10,
            'range': (0, 10)
        },
        'L0_PT': {
            'input': l0lls,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 2200)
        },
        'L0_BPVIPCHI2': {
            'input': l0lls,
            'variable': F.BPVIPCHI2(pvs),
            'bins': 100,
            'range': (0, 60)
        },
        'p_ETA': {
            'input': protons,
            'variable': F.ETA,
            'bins': 100,
            'range': (2, 5)
        },
        'p_P': {
            'input': protons,
            'variable': F.P,
            'bins': 100,
            'range': (5000, 50000)
        },
        'p_PT': {
            'input': protons,
            'variable': F.PT,
            'bins': 100,
            'range': (0, 100000)
        },
        'p_TCHI2DOF': {
            'input': protons,
            'variable': F.CHI2DOF,
            'bins': 100,
            'range': (0, 5)
        },
        'p_GHOSTPROB': {
            'input': protons,
            'variable': F.GHOSTPROB,
            'bins': 100,
            'range': (0, 1)
        },
    }
    # Update the dict in loops
    for pid_cut in pid_bins:
        l0_hist_dict[f"integrated__L0_M__p_PID_P-{pid_cut}"] = dict(
            l_mass_part_dict,
            variable=F.MASS * (F.CHILD(1, F.PID_P) > pid_cut))
        l0_hist_dict[f"integrated__L0_M__p_PID_PK-{pid_cut}"] = dict(
            l_mass_part_dict,
            variable=F.MASS *
            (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
        for pt_idx in range(0, len(pt_bins) - 1):
            for p_idx in range(0, len(p_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      p_bins[p_idx] * GeV, F.CHILD(1, F.P),
                                      p_bins[p_idx + 1] * GeV) &
                         (F.CHILD(1, F.PID_P) > pid_cut)))
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      p_bins[p_idx] * GeV, F.CHILD(1, F.P),
                                      p_bins[p_idx + 1] * GeV) &
                         (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
                    )
            for eta_idx in range(0, len(eta_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      eta_bins[p_idx], F.CHILD(1, F.ETA),
                                      eta_bins[eta_idx + 1]) &
                         (F.CHILD(1, F.PID_P) > pid_cut)))
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}__L0_M__p_PID_P-{pid_cut}"] = dict(
                        l_mass_part_dict,
                        variable=F.MASS *
                        (in_range(pt_bins[pt_idx] * GeV, F.CHILD(1, F.PT),
                                  pt_bins[pt_idx + 1] * GeV) & in_range(
                                      eta_bins[p_idx], F.CHILD(1, F.ETA),
                                      eta_bins[eta_idx + 1]) &
                         (F.CHILD(1, F.PID_P) - F.CHILD(1, F.PID_K) > pid_cut))
                    )

    l0_hist_list = _make_hist_list(l0_hist_dict, name)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [l0lls] + l0_hist_list,
        postscale=0)


def _make_bs(jpsis, kaons, pvs):
    return ParticleCombiner(
        [jpsis, kaons],
        name='CalibMon_BToJpsiK_Combiner_{hash}',
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=in_range(5 * GeV, F.MASS, 5.7 * GeV),
        CompositeCut=F.require_all(
            in_range(5.1 * GeV, F.MASS, 5.6 * GeV), F.CHI2 < 9,
            F.BPVFDCHI2(pvs) > 150,
            F.BPVIPCHI2(pvs) < 25))


@register_line_builder(all_lines)
def calibmon_b_to_jpsi_k_jpsi_to_em_ep_tagged(
        name="Hlt2CalibMon_BpToJpsiKp_JpsiToEmEp_Tagged"):
    pvs = make_pvs()
    kaons = ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 1 * GeV, F.P > 3 * GeV,
                          F.MINIPCHI2(pvs) > 9, F.PID_K > 5)))
    es_tag = ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(
            F.require_all(F.PT > 1.5 * GeV, F.P > 6 * GeV,
                          F.MINIPCHI2(pvs) > 9, F.PID_E > 5,
                          F.GHOSTPROB < 0.65)))
    es_probe = ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(
            F.require_all(F.PT > 0.5 * GeV, F.P > 3 * GeV,
                          F.MINIPCHI2(pvs) > 9, F.GHOSTPROB < 0.65)))
    eps_tag = ParticleFilter(es_tag, Cut=F.FILTER(F.CHARGE > 0))
    eps_probe = ParticleFilter(es_probe, Cut=F.FILTER(F.CHARGE > 0))
    ems_tag = ParticleFilter(es_tag, Cut=F.FILTER(F.CHARGE < 0))
    ems_probe = ParticleFilter(es_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsi_combination_cut = F.require_all(
        in_range(2.1 * GeV, F.MASS, 3.6 * GeV),
        F.ALV(1, 2) < 0.999999875)  # 0.5 mrad
    jpsi_composite_cut = F.require_all(
        in_range(2.2 * GeV, F.MASS, 3.5 * GeV), F.CHI2DOF < 25)
    jpsis_ep_tagged = ParticleCombiner(
        [eps_tag, ems_probe],
        DecayDescriptor="J/psi(1S) -> e+ e-",
        name='CalibMon_JpsiToee_Combiner_{hash}',
        CombinationCut=jpsi_combination_cut,
        CompositeCut=jpsi_composite_cut)
    jpsis_em_tagged = ParticleCombiner(
        [ems_tag, eps_probe],
        DecayDescriptor="J/psi(1S) -> e- e+",
        name='CalibMon_JpsiToee_Combiner_{hash}',
        CombinationCut=jpsi_combination_cut,
        CompositeCut=jpsi_composite_cut)
    b_ep_tagged = _make_bs(jpsis_ep_tagged, kaons, pvs)
    b_em_tagged = _make_bs(jpsis_em_tagged, kaons, pvs)

    bs = ParticleContainersMerger([b_ep_tagged, b_em_tagged])

    def _get_dtf_mass(particles):
        dtf = DecayTreeFitter(
            name='CalibMon_BToJpsiK_DTF_{hash}',
            input_particles=particles,
            input_pvs=pvs,
            mass_constraints=["J/psi(1S)"],
            constrain_to_ownpv=True)
        return dtf(F.VALUE_OR(Value=0) @ F.MASS)

    pid_bins = (0, 5)
    b_hist_dict = {}
    b_mass_part_dict = {'bins': 50, 'range': (5100, 5600)}

    b_hist_dict["B_M"] = dict(
        b_mass_part_dict, input=bs, variable=_get_dtf_mass(bs))
    for pid_cut in pid_bins:
        b_hist_dict[f"B_M_ep_tagged_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=b_ep_tagged,
            variable=_get_dtf_mass(b_ep_tagged) * (F.CHILD(
                1, F.CHILD(2, F.PID_E > pid_cut))))
        b_hist_dict[f"B_M_em_tagged_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=b_em_tagged,
            variable=_get_dtf_mass(b_em_tagged) * (F.CHILD(
                1, F.CHILD(2, F.PID_E > pid_cut))))
        b_hist_dict[f"B_M_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=bs,
            variable=_get_dtf_mass(bs) * (F.CHILD(
                1, F.CHILD(2, F.PID_E > pid_cut))))

    b_hist_list = _make_hist_list(b_hist_dict, name)

    return Hlt2Line(
        name=name, algs=_calibmon_filters() + [bs] + b_hist_list, postscale=0)


def _make_detached_jpsis(mup, mun, pvs):
    return ParticleCombiner(
        [mup, mun],
        name='CalibMon_Detached_JpsiToMuMu_Combiner_{hash}',
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=F.require_all(
            in_range(2890 * MeV, F.MASS, 3300 * MeV),
            F.SDOCACHI2(1, 2) < 6.0),
        CompositeCut=F.require_all(
            in_range(2915 * MeV, F.MASS, 3275 * MeV), F.PT > 1 * GeV,
            F.CHI2 < 15,
            F.BPVFDCHI2(pvs) > 150,
            F.MINIPCHI2(pvs) > 5,
            F.BPVDIRA(pvs) > 0.995))


@register_line_builder(all_lines)
def calibmon_jpsi_to_mup_mum_detached_tagged(
        name="Hlt2CalibMon_JpsiToMupMum_Detached_Tagged"):

    pvs = make_pvs()
    mus_tag = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(F.P > 3 * GeV, F.PT > 1.2 * GeV,
                          F.MINIPCHI2(pvs) > 9)))
    mus_probe = ParticleFilter(
        make_long_muons(),
        F.FILTER(
            F.require_all(F.INMUON, F.P > 3 * GeV,
                          F.MINIPCHI2(pvs) > 20)))

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    detached_jpsis_mum_tagged = _make_detached_jpsis(mups_probe, mums_tag, pvs)
    detached_jpsis_mup_tagged = _make_detached_jpsis(mups_tag, mums_probe, pvs)
    detached_jpsis = ParticleContainersMerger(
        [detached_jpsis_mum_tagged, detached_jpsis_mup_tagged])

    p_bins = (3000, 6000, 10000, float('inf'))
    pt_bins = (0, 800, float('inf'))
    jpsi_hist_dict = {}
    jpsi_mass_part_dict = {'bins': 90, 'range': (2915, 3275)}
    for p_idx in range(0, len(p_bins) - 1):
        for pt_idx in range(0, len(pt_bins) - 1):
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx+1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}_mum_tag"] = dict(
                    jpsi_mass_part_dict,
                    input=detached_jpsis_mum_tagged,
                    variable=F.MASS * (in_range(
                        pt_bins[pt_idx], F.CHILD(1, F.PT),
                        100 * TeV if pt_bins[pt_idx + 1] == float('inf') else
                        pt_bins[pt_idx + 1]) & in_range(
                            p_bins[p_idx], F.CHILD(1, F.P),
                            100 * TeV if p_bins[p_idx + 1] == float('inf') else
                            p_bins[p_idx + 1])))
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx+1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}_mup_tag"] = dict(
                    jpsi_mass_part_dict,
                    input=detached_jpsis_mup_tagged,
                    variable=F.MASS * (in_range(
                        pt_bins[pt_idx], F.CHILD(2, F.PT),
                        100 * TeV if pt_bins[pt_idx + 1] == float('inf') else
                        pt_bins[pt_idx + 1]) & in_range(
                            p_bins[p_idx], F.CHILD(2, F.P),
                            100 * TeV if p_bins[p_idx + 1] == float('inf') else
                            p_bins[p_idx + 1])))
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx+1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}_muprobe_ISMUON_mum_tag"] = dict(
                    jpsi_mass_part_dict,
                    input=detached_jpsis_mum_tagged,
                    variable=F.MASS * ((F.CHILD(1, F.ISMUON)) & in_range(
                        pt_bins[pt_idx], F.CHILD(1, F.PT),
                        100 * TeV if pt_bins[pt_idx + 1] == float('inf') else
                        pt_bins[pt_idx + 1]) & in_range(
                            p_bins[p_idx], F.CHILD(1, F.P),
                            100 * TeV if p_bins[p_idx + 1] == float('inf') else
                            p_bins[p_idx + 1])))
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx+1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}_muprobe_ISMUON_mup_tag"] = dict(
                    jpsi_mass_part_dict,
                    input=detached_jpsis_mup_tagged,
                    variable=F.MASS * ((F.CHILD(2, F.ISMUON)) & in_range(
                        pt_bins[pt_idx], F.CHILD(2, F.PT),
                        100 * TeV if pt_bins[pt_idx + 1] == float('inf') else
                        pt_bins[pt_idx + 1]) & in_range(
                            p_bins[p_idx], F.CHILD(2, F.P),
                            100 * TeV if p_bins[p_idx + 1] == float('inf') else
                            p_bins[p_idx + 1])))
    jpsi_hist_list = _make_hist_list(jpsi_hist_dict, name)

    # TODO: the line below does not work yet, we might as well do F.require_all(F.CHILD(1, F.P)>10*GeV, F.CHILD(2, F.P)>10*GeV) and not worry about functor composition
    # detached_jpsis_mu_p_filtered = ParticleFilter(
    #     detached_jpsis, Cut=F.FILTER((F.P > 10000) @ F.GET_ALL_DESCENDANTS()))

    # hits = MuonRawInUpgradeToHits(
    #     OutputLevel=0,
    #     RawBanks=default_raw_banks("Muon", default_raw_event),
    #     ErrorRawBanks=default_raw_banks("MuonError", default_raw_event),
    #     UseErrorBank=False,
    #     PrintStat=False)

    # muon_chamber_mon_mums_tagged = MuonChamberMonitor(Probes=mups_probe, Tags=mums_tag, MuonHits=hits, name="MuonChamberMonitor_MumTagged")
    # muon_chamber_mon_mups_tagged = MuonChamberMonitor(Probes=mums_probe, Tags=mups_tag, MuonHits=hits, name="MuonChamberMonitor_MupTagged")

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [detached_jpsis] + jpsi_hist_list,
        #  + [
        #     detached_jpsis_mu_p_filtered
        # ],  # + muon_chamber_mon_mums_tagged + muon_chamber_mon_mups_tagged
        postscale=0,
    )


def _filter_probe_muons(particles):
    return ParticleFilter(
        particles,
        F.FILTER(F.require_all(F.CHI2DOF < 5, F.PT > 1 * GeV, F.P > 5 * GeV)))


def _make_velomuon_jpsis(tag_and_probe_particles):
    return ParticleCombiner(
        tag_and_probe_particles,
        name='CalibMon_VeloMuon_JpsiToMuMu_Combiner_{hash}',
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0" in tag_and_probe_particles[0].__dict__["_producer"].
        __dict__["_properties"]["Cut"].code_repr() else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2600 * MeV, F.MASS, 3700 * MeV), F.MAXDOCACUT(0.1 * mm)),
        CompositeCut=F.require_all(
            in_range(2700 * MeV, F.MASS, 3600 * MeV), F.PT > 500 * MeV,
            F.CHI2DOF < 2))


def _fill_hist_lists_for_dimuon_trackeff(
        line_name, jpsis, jpsis_mum_tagged, jpsis_mup_tagged, matched_jpsis,
        matched_jpsi_mum_tagged, matched_jpsi_mup_tagged, pvs):
    pt_bins = (1000, 2000, 3000, 4000, 10000)
    eta_bins = (2, 3, 4, 5)
    npv_bins = (1, 2, 4, 6, 20)
    jpsi_hist_dict = {}
    matched_jpsi_hist_dict = {}
    jpsi_mass_part_dict = {'bins': 90, 'range': (2700, 3600)}

    def _make_match_and_tag_hists(binname, functor):
        jpsi_hist_dict[f"{binname}"] = dict(
            jpsi_mass_part_dict, input=jpsis, variable=functor)
        matched_jpsi_hist_dict[f"{binname}_mup_probe_matched"] = dict(
            jpsi_mass_part_dict, input=matched_jpsis, variable=functor)
        jpsi_hist_dict[f"{binname}_mup_probe"] = dict(
            jpsi_mass_part_dict, input=jpsis_mum_tagged, variable=functor)
        jpsi_hist_dict[f"{binname}_mum_probe"] = dict(
            jpsi_mass_part_dict, input=jpsis_mup_tagged, variable=functor)
        matched_jpsi_hist_dict[f"{binname}_mup_probe_matched"] = dict(
            jpsi_mass_part_dict,
            input=matched_jpsi_mum_tagged,
            variable=functor)
        matched_jpsi_hist_dict[f"{binname}_mum_probe_matched"] = dict(
            jpsi_mass_part_dict,
            input=matched_jpsi_mup_tagged,
            variable=functor)

    _make_match_and_tag_hists("Jpsi_M", F.MASS)
    _make_match_and_tag_hists("muprobe_PT", F.CHILD(2, F.PT))
    _make_match_and_tag_hists("muprobe_ETA", F.CHILD(2, F.ETA))
    _make_match_and_tag_hists("muprobe_PHI", F.CHILD(2, F.PHI))

    for pt_idx in range(0, len(pt_bins) - 1):
        # can't use in_range here. JIT compilation fails horribly
        _make_match_and_tag_hists(
            f"Jpsi_M_muprobe_PT-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}",
            F.MASS * ((pt_bins[pt_idx] < F.CHILD(2, F.PT)) &
                      (F.CHILD(2, F.PT) > pt_bins[pt_idx + 1])))
        for eta_idx in range(0, len(eta_bins) - 1):
            _make_match_and_tag_hists(
                f"Jpsi_M_muprobe_PT-{pt_bins[pt_idx]}-{pt_bins[pt_idx+1]}_muprobe_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}",
                F.MASS * ((in_range(pt_bins[pt_idx], F.CHILD(2, F.PT), pt_bins[
                    pt_idx + 1]) & in_range(eta_bins[eta_idx], F.CHILD(
                        2, F.ETA), eta_bins[eta_idx + 1]))))
    for eta_idx in range(0, len(eta_bins) - 1):
        _make_match_and_tag_hists(
            f"Jpsi_M_muprobe_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx+1]}",
            F.MASS * ((eta_bins[eta_idx] < F.CHILD(2, F.ETA)) &
                      (F.CHILD(2, F.ETA) > eta_bins[eta_idx + 1])))
    for npv_idx in range(0, len(npv_bins) - 1):
        _make_match_and_tag_hists(
            f"Jpsi_M_nPVs-{npv_bins[npv_idx]}-{npv_bins[npv_idx+1]}",
            F.MASS * ((npv_bins[npv_idx] < F.SIZE(pvs)) &
                      (F.SIZE(pvs) > npv_bins[npv_idx + 1])))

    return _make_hist_list(jpsi_hist_dict, line_name), _make_hist_list(
        matched_jpsi_hist_dict, line_name)


@register_line_builder(all_lines)
def calibmon_dimuon_velomuon(name="Hlt2CalibMon_DiMuon_VeloMuon"):
    pvs = make_pvs()
    #TODO: TOS on HLT1TrackMVA OR HLT1TrackMuonMVA; needs Moore!3006
    mus_tag = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(F.CHI2DOF < 3, F.PT > 500 * MeV, F.P > 7 * GeV,
                          F.PID_MU > -1,
                          F.MINIP(pvs) > 0.2 * mm)))
    mus_probe = _filter_probe_muons(make_velomuon_muons())

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_velomuon_jpsis([mums_tag, mups_probe])
    jpsis_mup_tagged = _make_velomuon_jpsis([mups_tag, mums_probe])
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(
        make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(
        make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=True,
        checkFT=False,
        checkUT=False,
        checkMuon=True).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=True,
        checkFT=False,
        checkUT=False,
        checkMuon=True).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged])

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name, jpsis, jpsis_mum_tagged, jpsis_mup_tagged, matched_jpsis,
        matched_jpsi_mum_tagged, matched_jpsi_mup_tagged, pvs)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [jpsis] + jpsi_hist_list + [matched_jpsis] +
        matched_jpsi_hist_list,
        postscale=0,
        monitoring_variables=())


def _make_seed_jpsis(tag_and_probe_particles):
    return ParticleCombiner(
        tag_and_probe_particles,
        name='CalibMon_SeedMuon_JpsiToMuMu_Combiner_{hash}',
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0" in tag_and_probe_particles[0].__dict__["_producer"].
        __dict__["_properties"]["Cut"].code_repr() else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2600 * MeV, F.MASS, 3700 * MeV), F.MAXDOCACUT(50 * mm)),
        CompositeCut=F.require_all(in_range(2700 * MeV, F.MASS, 3600 * MeV)))


@register_line_builder(all_lines)
def calibmon_dimuon_seedmuon(name="Hlt2CalibMon_DiMuon_SeedMuon"):
    pvs = make_pvs()
    #TODO: TOS on HLT1TrackMVA OR HLT1TrackMuonMVA; needs Moore!3006
    mus_tag = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(F.CHI2DOF < 3, F.PT > 500 * MeV, F.P > 10 * GeV,
                          F.PID_MU > -2,
                          F.MINIP(pvs) > 0.1 * mm,
                          F.MINIPCHI2(pvs) > 38)))
    mus_probe = _filter_probe_muons(make_seed_muons())

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_seed_jpsis([mums_tag, mups_probe])
    jpsis_mup_tagged = _make_seed_jpsis([mups_tag, mums_probe])
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(
        make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(
        make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=False,
        checkFT=True,
        checkUT=False,
        checkMuon=True).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=False,
        checkFT=True,
        checkUT=False,
        checkMuon=True).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged])

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name, jpsis, jpsis_mum_tagged, jpsis_mup_tagged, matched_jpsis,
        matched_jpsi_mum_tagged, matched_jpsi_mup_tagged, pvs)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters() + [jpsis] + jpsi_hist_list + [matched_jpsis] +
        matched_jpsi_hist_list,
        postscale=0,
        monitoring_variables=())

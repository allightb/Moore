###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make dimuon lines for charmonium decays. Shared by B&Q and B2CC.
"""
from PyConf import configurable

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, GeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

from Hlt2Conf.standard_particles import make_ismuon_long_muon

from Moore.config import Hlt2Line, register_line_builder

from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters

from Hlt2Conf.lines.config_pid import nopid_muons

# old: mass window [MeV] around the Jpsi and Psi2s,
# old: applied to Jpsi and Psi2s lines [2018: 120 MeV]
# new: exactly define the mass upper, lower limit of Jpsi.
# new: Easier for users to define asymmetric mass window considering the FSR.
_JPSI_PDG_MASS_ = 3096.9 * MeV
_PSI2S_PDG_MASS_ = 3686.1 * MeV
_MASSWINDOW_LOW_JPSI_ = 150 * MeV
_MASSWINDOW_HIGH_JPSI_ = 150 * MeV
_MASSWINDOW_LOW_PSI2S_ = 120 * MeV
_MASSWINDOW_HIGH_PSI2S_ = 120 * MeV
_MASSMIN_JPSI = _JPSI_PDG_MASS_ - _MASSWINDOW_LOW_JPSI_
_MASSMAX_JPSI = _JPSI_PDG_MASS_ + _MASSWINDOW_HIGH_JPSI_
_MASSMIN_PSI2S = _PSI2S_PDG_MASS_ - _MASSWINDOW_LOW_PSI2S_
_MASSMAX_PSI2S = _PSI2S_PDG_MASS_ + _MASSWINDOW_HIGH_PSI2S_

_PIDMU_JPSI = -5.
_PIDMU_PSI2S = -5.
_PIDMU_UPSILON = -5.

full_lines = {}
turbo_lines = {}


# define a new muon combiner, to make the cut on muons more configurable.
@configurable
def make_charmonium_muons(make_particles=make_ismuon_long_muon,
                          name='charmonium_muons_{hash}',
                          minPt_muon=0. * MeV,
                          minP_muon=0. * GeV,
                          minIPChi2_muon=0,
                          minIP_muon=0,
                          minPIDmu=-999,
                          maxIPChi2_muon=None):

    pvs = make_pvs()

    code = F.require_all(F.PT > minPt_muon, F.P > minP_muon,
                         F.MINIPCHI2(pvs) > minIPChi2_muon,
                         F.MINIP(pvs) > minIP_muon, F.ISMUON)

    # ignoring minPIDmu cut here, thus ignoring it in ALL lines in this file
    if not nopid_muons():
        code &= F.require_all(F.PID_MU > minPIDmu)

    if maxIPChi2_muon is not None:
        code &= (F.MINIPCHI2(pvs) < maxIPChi2_muon)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


# the new dimuon base for charmonium reconstruction
@configurable
def make_charmonium_dimuon_base(name='charmonium_dimuon_base_{hash}',
                                DecayDescriptor='J/psi(1S) -> mu+ mu-',
                                maxVertexChi2=25,
                                maxDOCAChi2=10000.,
                                minPt_muon=0. * MeV,
                                minP_muon=0. * GeV,
                                minIPChi2_muon=0,
                                minIP_muon=0,
                                minPIDmu=-999,
                                minPt_dimuon=0. * MeV,
                                minMass_dimuon=0. * MeV,
                                maxMass_dimuon=100000. * MeV):

    # get the long muons
    muons = make_charmonium_muons(
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minIPChi2_muon=minIPChi2_muon,
        minIP_muon=minIP_muon,
        minPIDmu=minPIDmu)

    combination_code = F.require_all(
        in_range(minMass_dimuon, F.MASS, maxMass_dimuon),
        F.DOCACHI2(1, 2) < maxDOCAChi2)

    # require that the muons come from the same vertex
    vertex_code = F.require_all(F.CHI2DOF < maxVertexChi2, F.PT > minPt_dimuon)

    return ParticleCombiner(
        name=name,
        Inputs=[muons, muons],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


#@configurable
#def make_charmonium_samesign_dimuon_base(name='charmonium_samesign_dimuon_base_{hash}'):
#    return make_charmonium_dimuon_base(name=name, DecayDescriptor='J/psi(1S) -> mu+ mu+')


@configurable
def make_charmonium_samesign_dimuon_base(
        name='charmonium_samesign_dimuon_base_{hash}',
        DecayDescriptor='J/psi(1S) -> mu+ mu+',
        maxVertexChi2=25,
        maxDOCAChi2=10000.,
        minPt_muon=0. * MeV,
        minP_muon=0. * GeV,
        minIPChi2_muon=0,
        minIP_muon=0,
        minPIDmu=-999,
        minPt_dimuon=0. * MeV,
        minMass_dimuon=0. * MeV,
        maxMass_dimuon=100000. * MeV):

    # get the long muons
    muons = make_charmonium_muons(
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minIPChi2_muon=minIPChi2_muon,
        minIP_muon=minIP_muon,
        minPIDmu=minPIDmu)

    combination_code = F.require_all(
        in_range(minMass_dimuon, F.MASS, maxMass_dimuon),
        F.DOCACHI2(1, 2) < maxDOCAChi2)

    # require that the muons come from the same vertex
    vertex_code = F.require_all(F.CHI2DOF < maxVertexChi2, F.PT > minPt_dimuon)

    return ParticleCombiner(
        name=name,
        Inputs=[muons, muons],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_charmonium_dimuon(name='charmonium_dimuon_{hash}',
                           DecayDescriptor='J/psi(1S) -> mu+ mu-',
                           minPt_dimuon=0 * MeV,
                           minPt_muon=300 * MeV,
                           minP_muon=0 * MeV,
                           minIPChi2_muon=0.,
                           maxVertexChi2=25,
                           minPIDmu=-5,
                           bpvdls_min=0.,
                           maxDOCAChi2=10000.,
                           minMass_dimuon=0. * MeV,
                           maxMass_dimuon=10000. * MeV):

    make_particles = make_charmonium_dimuon_base(
        DecayDescriptor=DecayDescriptor,
        maxVertexChi2=maxVertexChi2,
        minIPChi2_muon=minIPChi2_muon,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPIDmu=minPIDmu,
        maxDOCAChi2=maxDOCAChi2,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)

    pvs = make_pvs()

    code = F.require_all(F.PT > minPt_dimuon, F.CHI2DOF < maxVertexChi2,
                         F.BPVDLS(pvs) > bpvdls_min)

    return ParticleFilter(make_particles, name=name, Cut=F.FILTER(code))


@configurable
def make_charmonium_samesign_dimuon(name='charmonium_samesign_dimuon_{hash}',
                                    DecayDescriptor='J/psi(1S) -> mu+ mu+',
                                    minPt_dimuon=0 * MeV,
                                    minPt_muon=300 * MeV,
                                    minP_muon=0 * MeV,
                                    maxVertexChi2=25,
                                    minIPChi2_muon=0.,
                                    minPIDmu=-5,
                                    bpvdls_min=0.,
                                    maxDOCAChi2=10000.,
                                    minMass_dimuon=0. * MeV,
                                    maxMass_dimuon=10000. * MeV):

    make_particles = make_charmonium_samesign_dimuon_base(
        DecayDescriptor=DecayDescriptor,
        maxVertexChi2=maxVertexChi2,
        minIPChi2_muon=minIPChi2_muon,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPIDmu=minPIDmu,
        maxDOCAChi2=maxDOCAChi2,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)

    pvs = make_pvs()

    code = F.require_all(F.PT > minPt_dimuon, F.CHI2DOF < maxVertexChi2,
                         F.BPVDLS(pvs) > bpvdls_min)

    return ParticleFilter(make_particles, name=name, Cut=F.FILTER(code))


@configurable
def make_jpsi(name='jpsi_{hash}',
              minMass_dimuon=_MASSMIN_JPSI,
              maxMass_dimuon=_MASSMAX_JPSI,
              minPt_muon=300 * MeV,
              minP_muon=0 * MeV,
              minPt_Jpsi=0 * MeV,
              maxVertexChi2=25):

    code = (F.PT > minPt_Jpsi)

    dimuon = make_charmonium_dimuon(
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        minPt_dimuon=minPt_Jpsi,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPIDmu=_PIDMU_JPSI,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxVertexChi2=maxVertexChi2)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


@configurable
def make_psi2s(name='psi2s_{hash}',
               minMass_dimuon=_MASSMIN_PSI2S,
               maxMass_dimuon=_MASSMAX_PSI2S,
               minPt_muon=300 * MeV,
               minP_muon=0 * MeV,
               minPt_Psi2S=0 * MeV,
               maxPt_Psi2S=100000.0 * GeV,
               maxVertexChi2=25):

    code = F.require_all(F.PT > minPt_Psi2S, F.PT < maxPt_Psi2S)

    dimuon = make_charmonium_dimuon(
        DecayDescriptor='psi(2S) -> mu+ mu-',
        minPt_dimuon=minPt_Psi2S,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPIDmu=_PIDMU_PSI2S,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxVertexChi2=maxVertexChi2)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


@configurable
def make_jpsi_tight(name='jpsi_tight_{hash}'):
    return make_jpsi(
        minPt_Jpsi=3000 * MeV, minPt_muon=650 * MeV, minP_muon=10 * GeV)


@configurable
def make_psi2s_tight(name='psi2s_tight_{hash}'):
    return make_psi2s(
        minPt_Psi2S=3000 * MeV, minPt_muon=650 * MeV, minP_muon=10 * GeV)


#############
#define lines
##############
@register_line_builder(turbo_lines)
@configurable
def JpsiToMuMu_line(name='Hlt2_JpsiToMuMu', prescale=1):
    line_alg = make_jpsi()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def Psi2SToMuMu_line(name='Hlt2_Psi2SToMuMu', prescale=1):
    line_alg = make_psi2s()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(full_lines)
@configurable
def JpsiToMuMuTight_line(name='Hlt2_DiMuonJPsiTightFull',
                         prescale=1,
                         persistreco=True):
    line_alg = make_jpsi_tight()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Psi2SToMuMuTight_line(name='Hlt2_DiMuonPsi2STightFull',
                          prescale=1,
                          persistreco=True):
    line_alg = make_psi2s_tight()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


all_lines = {}
all_lines.update(turbo_lines)
all_lines.update(full_lines)

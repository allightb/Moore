###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_c+ -> p+ K- pi+.
"""

from GaudiKernel.SystemOfUnits import ns

from Moore.config import Hlt2Line, register_line_builder

from Hlt2Conf.lines.pid.utils import (
    cbaryon_builders,
    filters as flt,
)

all_lines = {}


@register_line_builder(all_lines)
def LcToPKPi(name="Hlt2PID_LcToPKPi", prescale=1):
    lambdacs = cbaryon_builders.make_lc_to_pkpi(
        mipchi2_max=10, bpvltime_min=0.00015 * ns, bpvfdchi2_min=80)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [lambdacs],
        prescale=prescale,
        persistreco=True,
    )

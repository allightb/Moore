###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_0 -> p pi- LL and DD.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Moore.config import Hlt2Line, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_pions,
    make_down_pions,
    make_long_protons,
    make_down_protons,
)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from Hlt2Conf.lines.pid.utils import constants, filters as flt

all_lines = {}


def filter_protons(particles,
                   p_min=2.0 * GeV,
                   pt_min=0 * GeV,
                   pt_max=1000 * GeV):
    code = F.require_all(F.P > p_min, F.PT > pt_min, F.PT < pt_max)
    return ParticleFilter(particles, F.FILTER(code))


def make_l0lls(
        protons,
        pions,
        pvs,
        mm_min=constants.LAMBDA_M - 50 * MeV,
        mm_max=constants.LAMBDA_M + 50 * MeV,
        m_min=constants.LAMBDA_M - 20 * MeV,
        m_max=constants.LAMBDA_M + 20 * MeV,
        bpvltime_min=0.002 * ns,
        ks_veto_window=20 * MeV,
        vchi2_max=30,
        mipchi2_max=50,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2_max,
        F.MINIPCHI2(pvs) < mipchi2_max,
        (F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) > constants.K0_M + ks_veto_window),
        F.BPVLTIME(pvs) > bpvltime_min,
    )

    return ParticleCombiner(
        [protons, pions],
        name='PID_L0ToPPi_LL_Combiner_{hash}',
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_l0dds(
        protons,
        pions,
        pvs,
        mm_min=constants.LAMBDA_M - 80 * MeV,
        mm_max=constants.LAMBDA_M + 80 * MeV,
        m_min=constants.LAMBDA_M - 21 * MeV,
        m_max=constants.LAMBDA_M + 24 * MeV,
        bpvvdz_min=400 * mm,
        ks_veto_window=20 * MeV,
        vchi2_max=30,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.BPVVDZ(pvs) > bpvvdz_min,
        F.CHI2DOF < vchi2_max,
        (F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(
            ("pi+", "pi-")) > constants.K0_M - ks_veto_window),
    )

    return ParticleCombiner(
        [protons, pions],
        name='PID_L0ToPPi_DD_Combiner_{hash}',
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def L0ToPPi_LL_LowPT(name="Hlt2PID_L0ToPPi_LL_LowPT", prescale=0.002):
    pvs = make_pvs()
    protons = flt.filter_long_particles(
        filter_protons(make_long_protons(), pt_max=1.5 * GeV),
        pvs,
        trchi2_max=4,
        mipchi2_min=36)
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=4, mipchi2_min=36)
    l0lls = make_l0lls(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_LL_MidPT(name="Hlt2PID_L0ToPPi_LL_MidPT", prescale=0.01):
    pvs = make_pvs()
    protons = flt.filter_long_particles(
        filter_protons(make_long_protons(), pt_min=1.5 * GeV, pt_max=3 * GeV),
        pvs,
        trchi2_max=4,
        mipchi2_min=36)
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=4, mipchi2_min=36)
    l0lls = make_l0lls(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_LL_HighPT(name="Hlt2PID_L0ToPPi_LL_HighPT", prescale=0.1):
    pvs = make_pvs()
    protons = flt.filter_long_particles(
        filter_protons(make_long_protons(), pt_min=3 * GeV, pt_max=6 * GeV),
        pvs,
        trchi2_max=4,
        mipchi2_min=36)
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=4, mipchi2_min=36)
    l0lls = make_l0lls(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_LL_VeryHighPT(name="Hlt2PID_L0ToPPi_LL_VeryHighPT", prescale=1):
    pvs = make_pvs()
    protons = flt.filter_long_particles(
        filter_protons(make_long_protons(), pt_min=6 * GeV),
        pvs,
        trchi2_max=4,
        mipchi2_min=36)
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=4, mipchi2_min=36)
    l0lls = make_l0lls(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_DD_LowPT(name="Hlt2PID_L0ToPPi_DD_LowPT", prescale=0.002):
    pvs = make_pvs()
    protons = flt.filter_down_particles(
        filter_protons(
            make_down_protons(), pt_min=0.175 * GeV, pt_max=1.5 * GeV),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=4,
    )
    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=4,
    )
    l0dds = make_l0dds(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_DD_MidPT(name="Hlt2PID_L0ToPPi_DD_MidPT", prescale=0.01):
    pvs = make_pvs()
    protons = flt.filter_down_particles(
        filter_protons(make_down_protons(), pt_min=1.5 * GeV, pt_max=3 * GeV),
        p_min=3.0 * GeV,
        trchi2_max=4,
    )
    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=4,
    )
    l0dds = make_l0dds(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_DD_HighPT(name="Hlt2PID_L0ToPPi_DD_HighPT", prescale=0.1):
    pvs = make_pvs()
    protons = flt.filter_down_particles(
        filter_protons(make_down_protons(), pt_min=3.0 * GeV, pt_max=6 * GeV),
        p_min=3.0 * GeV,
        pt_min=3.0 * GeV,
        trchi2_max=4,
    )
    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=4,
    )
    l0dds = make_l0dds(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def L0ToPPi_DD_VeryHighPT(name="Hlt2PID_L0ToPPi_DD_VeryHighPT", prescale=1):
    pvs = make_pvs()
    protons = flt.filter_down_particles(
        filter_protons(make_down_protons(), pt_min=6.0 * GeV),
        p_min=3.0 * GeV,
        pt_min=6.0 * GeV,
        trchi2_max=4,
    )
    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=3.0 * GeV,
        pt_min=0.175 * GeV,
        trchi2_max=4,
    )
    l0dds = make_l0dds(protons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds],
        prescale=prescale,
        persistreco=True,
    )

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Neutral PID Hlt2 particle filters/combiners for the following:
exclusive B -> hh gamma builder for KstG and PhiG
photons with high pt cut

Run2 TurCal ref: https://cds.cern.ch/record/2764341/files/LHCb-INT-2021-002.pdf?
author: Biplab Dey, Debashis Sahoo
date: 14.02.2024
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, ps, mm
import Functors as F
from RecoConf.reconstruction_objects import make_pvs
from Functors.math import in_range

from PyConf import configurable

#from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (make_rd_detached_kstar0s)

#from Hlt2Conf.standard_particles import (make_photons)
from Hlt2Conf.standard_particles import (
    make_photons,  # phiG/KstG
    make_has_rich_long_pions,
    make_has_rich_long_kaons,
    make_long_muons,
    make_resolved_pi0s)

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter


@configurable
def make_prompt_slow_pions(
        name="pid_prompt_slow_pions",
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        # these two follow the D->Kpi PID selection.
        pt_min=50. * MeV,
        p_min=500.0 * MeV,
        mipchi2dvprimary_max=25.):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min, F.PID_K < 0.,
                         F.MINIPCHI2(pvs) < mipchi2dvprimary_max)

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_filter_tracks(make_particles=make_has_rich_long_pions,
                       make_pvs=make_pvs,
                       name="pid_has_rich_long_pions",
                       pt_min=250. * MeV,
                       p_min=2.0 * GeV,
                       pid=None,
                       mipchi2dvprimary_min=None):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
    )

    if pid is not None:
        code &= pid

    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_calib_has_rich_detached_pions(
        name="pid_has_rich_detached_pions_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,
        pid=(F.PID_K <= 0.)):
    """
    Return RD detached pions with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_calib_has_rich_detached_kaons(
        name="pid_has_rich_detached_kaons_{hash}",
        p_min=2. * GeV,
        pt_min=250. * MeV,
        mipchi2dvprimary_min=4.,
        pid=(F.PID_K > 0.)):
    """
    Return RD detached kaons with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_calib_detached_kstar0s(
        name="pid_detached_kstar0s_{hash}",
        make_calib_detached_pions=make_calib_has_rich_detached_pions,
        make_calib_detached_kaons=make_calib_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=592. * MeV,
        am_max=1192. * MeV,
        pi_p_min=2. * GeV,
        pi_pt_min=100. * MeV,
        pi_ipchi2_min=4.,
        pi_pid=(F.PID_K < 8.),
        k_p_min=2. * GeV,
        k_pt_min=100. * MeV,
        k_ipchi2_min=4.,
        k_pid=(F.PID_K > -2.),
        kstar0_pt_min=400. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=25.,
        bpvipchi2_min=None,
        same_sign=False):
    '''
    Build Kstar0 candidates. Approximately corresponding to the Run2
    "StdVeryLooseDetachedKstar" cuts.
    '''

    pions = make_calib_detached_pions(
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=pi_pid)
    kaons = make_calib_detached_kaons(
        p_min=k_p_min,
        pt_min=k_pt_min,
        mipchi2dvprimary_min=k_ipchi2_min,
        pid=k_pid)
    descriptor = '[K*(892)0 -> K+ pi-]cc'
    if same_sign: descriptor = '[K*(892)0 -> K+ pi+]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2cut))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.PT > kstar0_pt_min)

    if bpvipchi2_min is not None:
        pvs = make_pvs()
        vertex_code &= (F.BPVIPCHI2(pvs) > bpvipchi2_min)

    return ParticleCombiner([kaons, pions],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_DstDz2Kpipiz(  # can be resolved or merged
        name="pid_DstDz2KpipizPi",
        track_pt_min=600. * MeV,
        pi0=make_resolved_pi0s,
        make_pvs=make_pvs,
        slow_pion=make_prompt_slow_pions):

    pvs = make_pvs()
    Kpi = make_calib_detached_kstar0s(
        am_min=100. * MeV,
        am_max=4000. * MeV,
        pi_p_min=2.5 * GeV,
        pi_pt_min=track_pt_min,
        pi_ipchi2_min=25.,
        pi_pid=(F.PID_K < 0.),
        k_p_min=5. * GeV,
        k_pt_min=track_pt_min,
        k_ipchi2_min=25.,
        k_pid=(F.PID_K > 0.),
        kstar0_pt_min=0. * MeV,
        adocachi2cut=30.,
        vchi2pdof_max=9.)

    Kpi_filt_code = F.require_all(
        (F.CHILD(1, F.PID_K) - F.CHILD(2, F.PID_K)) > 0.,  # extra
    )
    Kpi = ParticleFilter(Kpi, F.FILTER(Kpi_filt_code), name=name + "_Kpi")

    d0_m_min = 1600. * MeV
    d0_m_max = 2100. * MeV

    comb_code_D0 = F.require_all(
        F.PT > 4000. * MeV,
        in_range(d0_m_min - 200. * MeV, F.MASS, d0_m_max + 200. * MeV),
    )
    vert_code_D0 = F.require_all(
        in_range(d0_m_min, F.MASS, d0_m_max),
        F.BPVDIRA(pvs) > 0.9999,
        F.BPVIPCHI2(pvs) < 9,
        F.BPVFDCHI2(pvs) > 75.,
    )

    d0 = ParticleCombiner(
        name=name + "_D0KpiPi0",
        Inputs=[Kpi, pi0],
        DecayDescriptor="[D0 -> K*(892)~0 pi0]cc",
        CombinationCut=comb_code_D0,
        CompositeCut=vert_code_D0)

    deltaM_min = 135.421 * MeV
    deltaM_max = 155.421 * MeV

    comb_code_Dstp = F.require_all(
        F.PT > 4000. * MeV,
        in_range(deltaM_min, F.MASS - F.CHILD(1, F.MASS), deltaM_max),
    )
    vert_code_Dstp = F.require_all(F.BPVIPCHI2(pvs) < 9, )

    return ParticleCombiner(
        name=name,
        Inputs=[d0, slow_pion],
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=comb_code_Dstp,
        CompositeCut=vert_code_Dstp)


# for Bd->KstG and Bs->PhiG
@configurable
def make_exclusive_highpt_photons(
        name='calib_high_pt_photons_{hash}',
        make_particles=make_photons,
        et_min=2.0 * GeV
):  # loose, aligned with RD Hlt2RD_BdToKstGamma_Line. Run2 TurCal had 2.6 GeV.
    """Exclusive HighPt Photons"""

    code = F.require_all(F.PT > et_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_b2xgamma_excl(intermediate, photons, pvs, descriptor, comb_m_min,
                       comb_m_max, pt_min, dira_min, bpv_ipchi2_max, name):
    combination_code = F.require_all(
        F.PT > pt_min,
        in_range(comb_m_min - 100. * MeV, F.MASS, comb_m_max + 100. * MeV),
    )
    vertex_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < bpv_ipchi2_max)
    # return particle combiner
    return ParticleCombiner(
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_prompt_ds(name="pid_prompt_ds"):

    pvs = make_pvs()
    k_code = F.require_all(F.PT > 500. * MeV,
                           F.MINIPCHI2(pvs) > 2., F.PID_K > 0.)
    kaon = ParticleFilter(
        make_has_rich_long_kaons(), F.FILTER(k_code), name="pid_kaon")

    pi_code = F.require_all(F.PT > 250. * MeV,
                            F.MINIPCHI2(pvs) > 2., F.PID_K < 0.)
    pion = ParticleFilter(
        make_has_rich_long_pions(), F.FILTER(pi_code), name="pid_pion")

    # +/- 20MeV window
    ds_m_min = 1938 * MeV
    ds_m_max = 1998 * MeV
    ds_comb_code = F.require_all(
        in_range(ds_m_min - 20 * MeV, F.MASS, ds_m_max + 20 * MeV),
        F.MAXDOCACHI2 < 50,
        F.PT > 2. * GeV,
    )

    ds_vert_code = F.require_all(
        in_range(ds_m_min, F.MASS, ds_m_max),
        F.BPVDIRA(pvs) > 0.99995,
        F.BPVIPCHI2(pvs) < 150,
        F.BPVFDCHI2(pvs) > 75, F.CHI2DOF < 5,
        F.BPVLTIME(pvs) > 0.2 * ps)

    return ParticleCombiner(
        name=name,
        Inputs=[kaon, kaon, pion],
        DecayDescriptor="[D_s+ -> K+ K- pi+]cc",
        CombinationCut=ds_comb_code,
        CompositeCut=ds_vert_code)


@configurable
def make_prompt_dimuons(name="pid_prompt_dimuon"):

    pvs = make_pvs()
    muon_code = F.require_all(
        F.PT > 500. * MeV,
        F.P > 10000. * MeV,
        F.MINIPCHI2(pvs) < 10.,
        F.PID_MU > 3.,
        F.ISMUON,
    )
    prompt_muon = ParticleFilter(
        make_long_muons(), F.FILTER(muon_code), name="pid_prompt_muon")

    dimuon_comb_code = F.require_all(F.PT > 1. * GeV, )
    dimuon_vert_code = F.require_all(
        F.MAXDOCACUT(0.2 * mm),
        F.CHI2DOF < 10.,
        F.BPVFDCHI2(pvs) < 45,
        F.MINIPCHI2(pvs) < 15,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[prompt_muon, prompt_muon],
        DecayDescriptor="rho(770)0 -> mu+ mu-",
        CombinationCut=dimuon_comb_code,
        CompositeCut=dimuon_vert_code)


# D(s) -> etap (-> rho g) pi


@configurable
def make_d2etappi(name="pid_d2etappi"):

    pvs = make_pvs()
    pi_code = F.require_all(F.PT > 500. * MeV, F.P > 1000. * MeV,
                            F.MINIPCHI2(pvs) > 4., F.PID_K < 0.)
    pi = ParticleFilter(
        make_has_rich_long_pions(), F.FILTER(pi_code), name="pi")

    rho_comb_code = F.require_all(F.MAXDOCACHI2 < 50)

    rho_vert_code = F.require_all(
        F.CHI2DOF < 9,
        F.MASS > 600. * MeV,
    )

    rho = ParticleCombiner(
        name="pid_rho0_4etap",
        Inputs=[pi, pi],
        DecayDescriptor="rho(770)0 -> pi+ pi-",
        CombinationCut=rho_comb_code,
        CompositeCut=rho_vert_code)

    photon = ParticleFilter(
        make_photons(),
        F.FILTER(F.PT > 1000. * MeV),
        name="pid_medium_pt_gamma")

    etap_comb_code = F.require_all(in_range(800. * MeV, F.MASS, 1100. * MeV))
    etap_vert_code = F.require_all(in_range(930. * MeV, F.MASS, 990. * MeV), )

    etap = ParticleCombiner(
        name="pid_etap",
        Inputs=[rho, photon],
        DecayDescriptor="eta_prime -> rho(770)0 gamma",
        CombinationCut=etap_comb_code,
        CompositeCut=etap_vert_code)

    d_comb_code = F.require_all(
        in_range(1650. * MeV, F.MASS, 2300. * MeV),
        F.PT > 2000. * MeV,
    )
    d_vert_code = F.require_all(
        in_range(1800. * MeV, F.MASS, 2050. * MeV),
        F.CHI2DOF < 20,
        F.BPVDIRA(pvs) > 0.999,
        F.BPVIPCHI2(pvs) < 10,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[etap, pi],
        DecayDescriptor="[D_s+ -> eta_prime pi+]cc",
        CombinationCut=d_comb_code,
        CompositeCut=d_vert_code)

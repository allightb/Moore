###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of charmonium decays that are shared between several PID selections.
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

import Functors as F
from Functors.math import in_range

from PyConf import configurable
from Hlt2Conf.standard_particles import (
    make_long_electrons_no_brem, make_long_electrons_with_brem,
    make_has_rich_long_protons, _make_dielectron_with_brem)
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

from Hlt2Conf.lines.pid.utils import constants, filters as flt


@configurable
def make_jpsi_to_ee(
        e_pos,
        e_neg,
        am_min=constants.JPSI_M - 996.9 * MeV,  #2.1GeV
        am_max=constants.JPSI_M + 1203.1 * MeV,  #4.3GeV
        m_min=constants.JPSI_M - 896.9 * MeV,  #2.2GeV
        m_max=constants.JPSI_M + 1103.1 * MeV,  #4.2GeV
):
    """
    Return a J/psi(1S) -> e+ e- candidates.
    """
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), )

    composite_code = F.require_all(in_range(m_min, F.MASS, m_max), )
    return ParticleCombiner(
        [e_pos, e_neg],
        DecayDescriptor="J/psi(1S) -> e+ e-",
        name='PID_JpsiToee_Combiner_{hash}',
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_tag_electrons(
        charge,
        brem,
        dlle_min=5,
        mipchi2_min=9,
        pt_min=1500 * MeV,
        p_min=6000 * MeV,
):
    pvs = make_pvs()
    if brem:
        particles = make_long_electrons_with_brem()
    else:
        particles = make_long_electrons_no_brem()

    return ParticleFilter(
        flt.filter_particles(
            particles=particles,
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(
                F.PID_E > dlle_min,
                F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
            ), ),
    )


def make_tag_electrons_noPIDe(
        charge,
        brem,
        mipchi2_min=25,
        pt_min=1500 * MeV,
        p_min=6000 * MeV,
):
    pvs = make_pvs()
    if brem:
        particles = make_long_electrons_with_brem()
    else:
        particles = make_long_electrons_no_brem()

    return ParticleFilter(
        flt.filter_particles(
            particles=particles,
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(F.CHARGE > 0 if charge > 0 else F.CHARGE < 0, ), ),
    )


def make_probe_electrons(
        charge,
        brem,
        mipchi2_min=9,
        pt_min=500 * MeV,
        p_min=3000 * MeV,
):
    pvs = make_pvs()
    if brem:
        particles = make_long_electrons_with_brem()
    else:
        particles = make_long_electrons_no_brem()

    return ParticleFilter(
        flt.filter_particles(
            particles=particles,
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(F.CHARGE > 0 if charge > 0 else F.CHARGE < 0, ), ),
    )


def make_probe_electrons_noPIDe(
        charge,
        brem,
        mipchi2_min=25,
        pt_min=500 * MeV,
        p_min=3000 * MeV,
):
    pvs = make_pvs()
    if brem:
        particles = make_long_electrons_with_brem()
    else:
        particles = make_long_electrons_no_brem()

    return ParticleFilter(
        flt.filter_particles(
            particles=particles,
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(F.CHARGE > 0 if charge > 0 else F.CHARGE < 0, ), ),
    )


def make_electrons_for_dielectron_noPIDe(
        tag_charge,
        probe_charge,
        tag_mipchi2_min=25,
        tag_pt_min=1500 * MeV,
        tag_p_min=6000 * MeV,
        probe_mipchi2_min=25,
        probe_pt_min=500 * MeV,
        probe_p_min=3000 * MeV,
):
    pvs = make_pvs()
    particles = make_long_electrons_no_brem()

    if tag_charge > 0 and probe_charge < 0:
        return ParticleFilter(
            flt.filter_particles(
                particles=particles,
                pvs=pvs,
            ),
            F.FILTER(
                F.require_all((
                    (F.CHARGE > 0) & (F.PT > tag_pt_min) & (F.P > tag_p_min) &
                    (F.MINIPCHI2(pvs) > tag_mipchi2_min))
                              | ((F.CHARGE < 0) & (F.PT > probe_pt_min) &
                                 (F.P > probe_p_min) &
                                 (F.MINIPCHI2(pvs) > probe_mipchi2_min))), ))
    else:
        return ParticleFilter(
            flt.filter_particles(
                particles=particles,
                pvs=pvs,
            ),
            F.FILTER(
                F.require_all((
                    (F.CHARGE < 0) & (F.PT > tag_pt_min) & (F.P > tag_p_min) &
                    (F.MINIPCHI2(pvs) > tag_mipchi2_min))
                              | ((F.CHARGE > 0) & (F.PT > probe_pt_min) &
                                 (F.P > probe_p_min) &
                                 (F.MINIPCHI2(pvs) > probe_mipchi2_min))), ))


def make_electrons_for_dielectron_PIDe(
        tag_charge,
        probe_charge,
        tag_mipchi2_min=9,
        tag_pt_min=1500 * MeV,
        tag_p_min=6000 * MeV,
        tag_dlle_min=5,
        probe_mipchi2_min=9,
        probe_pt_min=500 * MeV,
        probe_p_min=3000 * MeV,
):
    pvs = make_pvs()
    particles = make_long_electrons_no_brem()

    if tag_charge > 0 and probe_charge < 0:
        return ParticleFilter(
            flt.filter_particles(
                particles=particles,
                pvs=pvs,
            ),
            F.FILTER(
                F.require_all(((F.CHARGE > 0) & (F.PT > tag_pt_min) & (
                    F.P > tag_p_min) & (F.MINIPCHI2(pvs) > tag_mipchi2_min) &
                               (F.PID_E > tag_dlle_min))
                              | ((F.CHARGE < 0) & (F.PT > probe_pt_min) &
                                 (F.P > probe_p_min) &
                                 (F.MINIPCHI2(pvs) > probe_mipchi2_min))), ))
    else:
        return ParticleFilter(
            flt.filter_particles(
                particles=particles,
                pvs=pvs,
            ),
            F.FILTER(
                F.require_all(((F.CHARGE < 0) & (F.PT > tag_pt_min) & (
                    F.P > tag_p_min) & (F.MINIPCHI2(pvs) > tag_mipchi2_min) &
                               (F.PID_E > tag_dlle_min))
                              | ((F.CHARGE > 0) & (F.PT > probe_pt_min) &
                                 (F.P > probe_p_min) &
                                 (F.MINIPCHI2(pvs) > probe_mipchi2_min))), ))


def make_dielectron_with_brem(
        electrons,
        diElectonID="J/psi(1S)",
        isOS=True,
        pt_diE_min=0 * MeV,
        m_diE_min=constants.JPSI_M - 996.9 *
        MeV,  #2.1GeV  # minimum dielectron mass filter while making dielectrons
        m_diE_max=constants.JPSI_M + 1203.1 * MeV,  #4.3 GeV
        m_Jpsi_min=constants.JPSI_M - 896.9 *
        MeV,  #2.2GeV  # minimum dielectron mass filter while making dielectrons
        m_Jpsi_max=constants.JPSI_M + 1103.1 * MeV,  #4.2 GeV
):

    dielectron_with_brem = _make_dielectron_with_brem(
        electrons=electrons,
        pt_diE=pt_diE_min,
        m_diE_min=m_diE_min,
        m_diE_max=m_diE_max,
        m_diE_ID=diElectonID,
        opposite_sign=isOS)

    code_jpsi = F.require_all(in_range(m_Jpsi_min, F.MASS, m_Jpsi_max), )

    return ParticleFilter(
        dielectron_with_brem,
        F.FILTER(code_jpsi),
        name="dielectron_jpsi_{hash}")


@configurable
def make_jpsi_to_pp(
        p_pos,
        p_neg,
        am_min=constants.JPSI_M - 220 * MeV,
        am_max=constants.JPSI_M + 220 * MeV,
        m_min=constants.JPSI_M - 200 * MeV,
        m_max=constants.JPSI_M + 200 * MeV,
        doca_max=10.0,
):
    """
    Return a J/psi(1S) -> p+ p- candidates.
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXDOCACHI2CUT(doca_max),
    )

    composite_code = F.require_all(in_range(m_min, F.MASS, m_max), )
    return ParticleCombiner(
        [p_pos, p_neg],
        DecayDescriptor="J/psi(1S) -> p+ p~-",
        name='PID_JpsiToPP_Combiner_{hash}',
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_tag_protons(
        charge,
        dllp_min=5,
        mipchi2_min=25,
        pt_min=1500 * MeV,
        p_min=3000 * MeV,
        trchi2_max=3,
        ghostprob_max=1e9,
):
    pvs = make_pvs()
    return ParticleFilter(
        flt.filter_particles(
            make_has_rich_long_protons(),
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(
                F.GHOSTPROB < ghostprob_max,
                F.PID_P > dllp_min,
                F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
            ), ),
    )


def make_probe_protons(
        charge,
        mipchi2_min=16,
        pt_min=800 * MeV,
        p_min=3000 * MeV,
        trchi2_max=5,
):
    pvs = make_pvs()
    return ParticleFilter(
        flt.filter_particles(
            make_has_rich_long_protons(),
            pvs=pvs,
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min,
        ),
        F.FILTER(
            F.require_all(F.CHARGE > 0 if charge > 0 else F.CHARGE < 0, ), ),
    )


def make_tag_muons(
        particles,
        pvs,
        charge,
        trghostprob_max=999.,
        p_min=3 * GeV,
        pt_min=1.2 * GeV,
        mipchi2dv_min=9.0,
):
    code = F.require_all(
        F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
        F.GHOSTPROB < trghostprob_max,
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
        F.ISMUON,
    )
    return ParticleFilter(particles, F.FILTER(code))


# TODO: Add HLT1 TIS filter
def make_probe_muons(
        particles,
        pvs,
        charge,
        p_min=3 * GeV,
        pt_min=0 * MeV,
        mipchi2dv_min=20.0,
):
    code = F.require_all(
        F.CHARGE > 0 if charge > 0 else F.CHARGE < 0,
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
    )
    return ParticleFilter(particles, F.FILTER(code))


def make_jpsis(
        mu_pos,
        mu_neg,
        am_min=constants.JPSI_M - 185 * MeV,
        am_max=constants.JPSI_M + 185 * MeV,
        m_min=constants.JPSI_M - 175 * MeV,
        m_max=constants.JPSI_M + 175 * MeV,
        achi2doca_max=5.0,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.DOCACHI2(1, 2) < achi2doca_max)
    mother_code = F.require_all(in_range(m_min, F.MASS, m_max))

    return ParticleCombiner(
        [mu_pos, mu_neg],
        name='PID_JpsiToMuMu_Combiner_{hash}',
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )


def make_detached_jpsis(
        particles,
        pvs,
        vfaspf_max=15.0,
        pt_min=1.0 * GeV,
        bpvvdchi2_min=150.0,
        mipchi2dv_min=5.0,
        bpvdira=0.995,
):
    vertex_code = F.require_all(
        F.CHI2 < vfaspf_max,
        F.PT > pt_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
        F.BPVDIRA(pvs) > bpvdira,
    )

    return ParticleFilter(particles, F.FILTER(vertex_code))

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for B+ -> [J/psi -> p+ p-] K+.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.config import Hlt2Line
from Moore.config import register_line_builder

import Functors as F
from Functors.math import in_range

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Hlt2Conf.standard_particles import make_long_kaons

from Hlt2Conf.lines.pid.utils import constants, filters as flt
from Hlt2Conf.lines.pid.utils.charmonium import make_jpsi_to_pp, make_tag_protons, make_probe_protons

all_lines = {}


def make_bs(
        jpsis,
        kaons,
        pvs,
        am_min=constants.BP_M - 520 * MeV,
        am_max=constants.BP_M + 520 * MeV,
        m_min=constants.BP_M - 500 * MeV,
        m_max=constants.BP_M + 500 * MeV,
        comb_pt_min=2000 * MeV,
        vchi2pdof_max=25,
        mipchi2_max=25,
        bpvfdchi2_min=15,
        bpvdira_min=0.9999,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > comb_pt_min)

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.CHI2 < vchi2pdof_max,
    )

    return ParticleCombiner(
        [jpsis, kaons],
        name='PID_BToJpsiK_Combiner_{hash}',
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToPmPpTagged(name="Hlt2PID_BToJpsiK_JpsiToPmPpTagged",
                              prescale=1):
    pvs = make_pvs()
    kaons = flt.filter_particles(
        make_long_kaons(),
        pvs,
        pt_min=0.3 * GeV,
        p_min=3 * GeV,
        trchi2_max=3,
        mipchi2_min=9.0,
    )
    p_tags = make_tag_protons(+1)
    p_probes = make_probe_protons(-1)
    jpsis = make_jpsi_to_pp(p_neg=p_probes, p_pos=p_tags)
    bs = make_bs(jpsis, kaons, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [bs],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToPpPmTagged(name="Hlt2PID_BToJpsiK_JpsiToPpPmTagged",
                              prescale=1):
    pvs = make_pvs()
    kaons = flt.filter_particles(
        make_long_kaons(),
        pvs,
        pt_min=0.3 * GeV,
        p_min=3 * GeV,
        trchi2_max=3,
        mipchi2_min=9.0,
    )
    p_tags = make_tag_protons(-1)
    p_probes = make_probe_protons(+1)
    jpsis = make_jpsi_to_pp(p_pos=p_probes, p_neg=p_tags)
    bs = make_bs(jpsis, kaons, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [bs],
        prescale=prescale,
        persistreco=True,
    )

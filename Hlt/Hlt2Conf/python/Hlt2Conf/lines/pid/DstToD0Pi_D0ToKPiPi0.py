###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Registration of pi0 lines
Dst+ -> D0 (-> K- pi+ pi0) pi+ 
Run2 TurCal ref: https://cds.cern.ch/record/2764341/files/LHCb-INT-2021-002.pdf?
Table 5
author: Biplab Dey, Debashis Sahoo
date: 14.02.2024
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from GaudiKernel.SystemOfUnits import MeV
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_merged_pi0s, make_resolved_pi0s
from Hlt2Conf.lines.pid.utils.neutral_pid import make_prompt_slow_pions, make_DstDz2Kpipiz

all_lines = {}


@register_line_builder(all_lines)
def dstdzkpipiz_R_line(name="Hlt2PID_DstToD0Pi_D0ToKPiPi0Resolved",
                       prescale=1.):

    pvs = make_pvs()
    dst = make_DstDz2Kpipiz(
        slow_pion=make_prompt_slow_pions(),
        pi0=make_resolved_pi0s(PtCut=1000 * MeV),
        name="PID_DstD0KPiPi0Resolved_Combiner")

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dst],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def dstdzkpipiz_M_line(name="Hlt2PID_DstToD0Pi_D0ToKPiPi0Merged", prescale=1.):

    pvs = make_pvs()
    dst = make_DstDz2Kpipiz(
        track_pt_min=300. * MeV,
        slow_pion=make_prompt_slow_pions(),
        pi0=make_merged_pi0s(),
        name="PID_DstD0KPiPi0Merged_Combiner")

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dst],
        prescale=prescale,
        persistreco=True)

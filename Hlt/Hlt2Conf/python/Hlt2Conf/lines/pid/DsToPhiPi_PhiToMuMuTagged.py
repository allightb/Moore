###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for Ds+ -> (phi -> mu+ mu-) pi+.
"""
import Functors as F

from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV

from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import (
    make_long_muons,
    make_ismuon_long_muon,
    make_long_pions,
)

from Hlt2Conf.algorithms_thor import (
    ParticleFilter,
    ParticleCombiner,
)

from Hlt2Conf.lines.pid.utils import constants, filters as flt

from Hlt2Conf.lines.pid.utils.charmonium import (
    make_probe_muons,
    make_tag_muons,
)

all_lines = {}


def make_phis(
        mu_pos,
        mu_neg,
        am_min=constants.PHI_M - 40 * MeV,
        am_max=constants.PHI_M + 40 * MeV,
        m_min=constants.PHI_M - 25 * MeV,
        m_max=constants.PHI_M + 25 * MeV,
        achi2doca_max=9.0,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.DOCACHI2(1, 2) < achi2doca_max)
    mother_code = F.require_all(in_range(m_min, F.MASS, m_max))

    return ParticleCombiner(
        [mu_pos, mu_neg],
        name='PID_PhiToMuMu_Combiner_{hash}',
        DecayDescriptor="phi(1020) -> mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )


def make_pions(
        particles,
        pvs,
        p_min=3 * GeV,
        pt_min=0.3 * GeV,
        mipchi2dv_min=9.0,
        dllkpi_max=5.0,
):
    code = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2dv_min,
        F.PID_K < dllkpi_max,
    )
    return ParticleFilter(particles, F.FILTER(code))


def make_ds(
        phis,
        pions,
        pvs,
        am_min=constants.DS_M - 80 * MeV,
        am_max=constants.DS_M + 80 * MeV,
        m_min=constants.DS_M - 70 * MeV,
        m_max=constants.DS_M + 70 * MeV,
        comb_pt_min=1000 * MeV,
        vchi2pdof_max=25,
        mipchi2_max=20,
        bpvfdchi2_min=15,
        bpvdira_min=0.9999,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > comb_pt_min)
    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.CHI2 < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
    )

    return ParticleCombiner(
        [phis, pions],
        name='PID_DsToPhiPi_Combiner_{hash}',
        DecayDescriptor="[D_s+ -> phi(1020) pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def DsToPhiPi_PhiToMuMupTagged_Detached(
        name="Hlt2PID_DsToPhiPi_PhiToMuMupTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(
        make_ismuon_long_muon(), pvs, +1, pt_min=0.5 * GeV, mipchi2dv_min=25)
    probe_muons = make_probe_muons(make_long_muons(), pvs, -1)
    phis = make_phis(mu_pos=tag_muons, mu_neg=probe_muons)
    pions = make_pions(make_long_pions(), pvs)
    ds = make_ds(phis, pions, pvs)
    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [phis, ds],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def DsToPhiPi_PhiToMuMumTagged_Detached(
        name="Hlt2PID_DsToPhiPi_PhiToMuMumTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(
        make_ismuon_long_muon(), pvs, -1, pt_min=0.5 * GeV, mipchi2dv_min=25)
    probe_muons = make_probe_muons(make_long_muons(), pvs, +1)
    phis = make_phis(mu_neg=tag_muons, mu_pos=probe_muons)
    pions = make_pions(make_long_pions(), pvs)
    ds = make_ds(phis, pions, pvs)
    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [phis, ds],
        prescale=prescale,
        persistreco=True,
    )

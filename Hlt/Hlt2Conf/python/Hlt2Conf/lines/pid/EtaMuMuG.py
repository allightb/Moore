###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Registration of low-PT rad line
Eta->mu+ mu- gamma 
Run2 TurCal ref: https://cds.cern.ch/record/2764341/files/LHCb-INT-2021-002.pdf?
Table 11
author: Biplab Dey, Debashis Sahoo
date: 20.11.2023
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Functors.math import in_range
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from RecoConf.event_filters import require_pvs

from Hlt2Conf.standard_particles import make_photons

from Hlt2Conf.lines.pid.utils.neutral_pid import make_prompt_dimuons

all_lines = {}


@register_line_builder(all_lines)
def EtaMuMuG_line(name="Hlt2PID_EtaMuMuG", prescale=1.):

    pvs = make_pvs()
    dimuon = make_prompt_dimuons()
    photon = ParticleFilter(
        make_photons(), F.FILTER(F.PT > 500. * MeV), name="pid_low_pt_gamma")
    eta_comb_code = F.require_all(
        in_range(400. * MeV, F.MASS, 700. * MeV), F.PT > 1. * GeV)
    eta_vert_code = F.require_all(
        in_range(410. * MeV, F.MASS, 690. * MeV),
        F.BPVIPCHI2(pvs) < 10,
    )

    eta = ParticleCombiner(
        name="pid_prompt_eta",
        Inputs=[dimuon, photon],
        DecayDescriptor="eta -> rho(770)0 gamma",
        CombinationCut=eta_comb_code,
        CompositeCut=eta_vert_code)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), eta],
        prescale=prescale,
        persistreco=True)

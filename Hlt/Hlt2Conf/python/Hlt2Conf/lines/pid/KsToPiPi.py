###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for Ks0 -> pi+ pi- LL and DD.
"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Moore.config import Hlt2Line, register_line_builder

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.standard_particles import make_long_pions, make_down_pions

from Hlt2Conf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.pid.utils import constants, filters as flt

from PyConf.Algorithms import Monitor__ParticleRange
from Functors.math import log

all_lines = {}


def make_ks0lls(
        pions,
        pvs,
        am_min=constants.K0_M - 50.0 * MeV,
        am_max=constants.K0_M + 50.0 * MeV,
        m_min=constants.K0_M - 30.0 * MeV,
        m_max=constants.K0_M + 30.0 * MeV,
        bpvvdz_max=2200 * mm,
        vchi2_min=0,
        vchi2_max=16,
        vchi2pdof_max=30,
        bpvvdchi2_min=25,
        bpvltime_min=0.002 * ns,
        lambda_veto=9 * MeV,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        in_range(vchi2_min, F.CHI2, vchi2_max),
        F.CHI2DOF < vchi2pdof_max,
        (F.MASSWITHHYPOTHESES(
            ("p+", "pi-")) < constants.LAMBDA_M - lambda_veto)
        | (F.MASSWITHHYPOTHESES(
            ("p+", "pi-")) > constants.LAMBDA_M + lambda_veto),
        (F.MASSWITHHYPOTHESES(
            ("pi+", "p~-")) < constants.LAMBDA_M - lambda_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi+", "p~-")) > constants.LAMBDA_M + lambda_veto),
        F.BPVVDZ(pvs) < bpvvdz_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
    )

    return ParticleCombiner(
        [pions, pions],
        name='PID_KsToPiPi_LL_Combiner_{hash}',
        DecayDescriptor="KS0 -> pi+ pi-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_ks0dds(
        pions,
        pvs,
        am_min=constants.K0_M - 80.0 * MeV,
        am_max=constants.K0_M + 80.0 * MeV,
        m_min=constants.K0_M - 30.0 * MeV,
        m_max=constants.K0_M + 30.0 * MeV,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2200 * mm,
        vchi2_min=0,
        vchi2_max=16,
        bpvvdchi2_min=25,
        bpvipchi2_max=150,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        in_range(bpvvdz_min, F.BPVVDZ(pvs), bpvvdz_max),
        in_range(vchi2_min, F.CHI2, vchi2_max),
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
    )

    return ParticleCombiner(
        [pions, pions],
        name='PID_KsToPiPi_DD_Combiner_{hash}',
        DecayDescriptor="KS0 -> pi+ pi-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(all_lines)
def KsToPiPi_LL(name="Hlt2PID_KsToPiPi_LL", prescale=0.0005):
    pvs = make_pvs()
    pions = flt.filter_long_particles(
        make_long_pions(), pvs, trchi2_max=3, mipchi2_min=36)
    ks0lls = make_ks0lls(pions, pvs)

    ks_fd_mon = Monitor__ParticleRange(
        Input=ks0lls,
        Variable=log(F.BPVFD(make_pvs())),
        #Variable=F.Mass,
        HistogramName=f"/{name}/Ks_FDlog",
        Bins=60,
        Range=(-2, 8),
    )

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [ks0lls, ks_fd_mon],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def KsToPiPi_DD(name="Hlt2PID_KsToPiPi_DD", prescale=0.0005):
    pvs = make_pvs()
    pions = flt.filter_down_particles(
        make_down_pions(), p_min=3.0 * GeV, pt_min=0.175 * GeV, trchi2_max=3)
    ks0dds = make_ks0dds(pions, pvs)

    ks_fd_mon_dd = Monitor__ParticleRange(
        Input=ks0dds,
        Variable=log(F.BPVFD(make_pvs())),
        #Variable=F.Mass,
        HistogramName=f"/{name}/Ks_FDlog",
        Bins=60,
        Range=(-2, 8),
    )

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [ks0dds, ks_fd_mon_dd],
        prescale=prescale,
        persistreco=True,
    )

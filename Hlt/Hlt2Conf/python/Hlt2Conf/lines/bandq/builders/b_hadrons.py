###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q b-hadrons
"""
import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, picosecond, mm

from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all
from PyConf import configurable

#############################################
# b-hadron makers for all types of decays   #
# Cuts on mass window, vtxchi2,  detachment #
#############################################

_MASS_MIN = 120 * MeV  # minimal mass in case of pi0->gg


@configurable
def _make_generic(particles,
                  descriptor,
                  name='bandq_generic_{hash}',
                  am_min=5100 * MeV,
                  am_max=5550 * MeV,
                  m_min=5140 * MeV,
                  m_max=5510 * MeV,
                  achi2_doca_max=25,
                  vtx_chi2pdof_max=20,
                  comb_cut_add=None,
                  vtx_cut_add=None):

    combination_code = (in_range(am_min, F.MASS, am_max))

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max)

    if comb_cut_add is not None:
        combination_code &= comb_cut_add

    if vtx_cut_add is not None:
        vertex_code &= vtx_cut_add

    if len(particles) == 2:
        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)

    if len(particles) == 3:

        combination12_code = require_all(F.MASS < am_max - _MASS_MIN,
                                         F.DOCACHI2(1, 2) < achi2_doca_max)

        combination_code &= require_all(
            F.DOCACHI2(1, 3) < achi2_doca_max,
            F.DOCACHI2(2, 3) < achi2_doca_max)

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)

    if len(particles) == 4:
        combination12_code = require_all(F.MASS < am_max - 2 * _MASS_MIN,
                                         F.DOCACHI2(1, 2) < achi2_doca_max)

        combination123_code = require_all(F.MASS < am_max - _MASS_MIN,
                                          F.DOCACHI2(1, 3) < achi2_doca_max,
                                          F.DOCACHI2(2, 3) < achi2_doca_max)

        combination_code &= require_all(
            F.DOCACHI2(1, 4) < achi2_doca_max,
            F.DOCACHI2(2, 4) < achi2_doca_max,
            F.DOCACHI2(3, 4) < achi2_doca_max)

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            Combination123Cut=combination123_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)

    if len(particles) >= 5:
        combination12_code = require_all(F.MASS < am_max - 3 * _MASS_MIN,
                                         F.DOCACHI2(1, 2) < achi2_doca_max)

        combination123_code = require_all(F.MASS < am_max - 2 * _MASS_MIN,
                                          F.DOCACHI2(1, 3) < achi2_doca_max,
                                          F.DOCACHI2(2, 3) < achi2_doca_max)

        combination1234_code = require_all(F.MASS < am_max - _MASS_MIN,
                                           F.DOCACHI2(1, 4) < achi2_doca_max,
                                           F.DOCACHI2(2, 4) < achi2_doca_max,
                                           F.DOCACHI2(3, 4) < achi2_doca_max)

        combination_code &= require_all(
            F.DOCACHI2(1, 5) < achi2_doca_max,
            F.DOCACHI2(2, 5) < achi2_doca_max,
            F.DOCACHI2(3, 5) < achi2_doca_max,
            F.DOCACHI2(4, 5) < achi2_doca_max)

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            Combination123Cut=combination123_code,
            Combination1234Cut=combination1234_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)


@configurable
def make_b_hadron(
        particles,
        descriptor,
        name='bandq_b_hadron_{hash}',
        am_min=5100 * MeV,
        am_max=5550 * MeV,
        m_min=5140 * MeV,
        m_max=5510 * MeV,
        achi2_doca_max=25,
        vtx_chi2pdof_max=20,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.995,  # [AVOID BIAS IN DIRECTION WRT TO PV], 0.995 (tan<0.1) is safe enough even for B from Tbb
        minVDz=0. * mm,
        minRho=0. * mm):

    vtx_cut_add = require_all(F.BPVLTIME() > bpvltime_min,
                              F.BPVVDRHO() > minRho,
                              F.BPVVDZ() > minVDz,
                              F.BPVDIRA() > bpvdira_min)

    return _make_generic(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        vtx_cut_add=vtx_cut_add)


####################################
# B0/B0s                           #
####################################


@configurable
def make_bds(particles, descriptor, name='bandq_Bds_{hash}',
             **decay_arguments):
    """
    Return B&Q B0 and Bs with charged final state particles
    """
    return make_b_hadron(particles, descriptor, name=name, **decay_arguments)


@configurable
def make_withneutrals_bds(particles,
                          descriptor,
                          name='bandq_withNeutrals_Bds_{hash}',
                          am_min=4500 * MeV,
                          am_max=6000 * MeV,
                          m_min=4550 * MeV,
                          m_max=5950 * MeV,
                          bpvltime_min=0.3 * picosecond,
                          **decay_arguments):
    """
    Return B&Q B0 and Bs with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# B+                               #
####################################


@configurable
def make_bu(particles,
            descriptor,
            name='bandq_Bu_{hash}',
            minVDz=0. * mm,
            minRho=0. * mm,
            **decay_arguments):
    """
    Return B&Q B+.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        minVDz=minVDz,
        minRho=minRho,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_bu(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bu_{hash}',
                         am_min=4500 * MeV,
                         am_max=6000 * MeV,
                         m_min=4550 * MeV,
                         m_max=5950 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q B+ with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Bc+                              #
####################################


@configurable
def make_bc(particles,
            descriptor,
            name='bandq_Bc_{hash}',
            am_min=6050 * MeV,
            am_max=6555 * MeV,
            m_min=6090 * MeV,
            m_max=6510 * MeV,
            bpvltime_min=0.1 * picosecond,
            **decay_arguments):
    """
    Return B&Q Bc+.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_bc(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bc_{hash}',
                         am_min=4500 * MeV,
                         am_max=6750 * MeV,
                         m_min=4550 * MeV,
                         m_max=6700 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q Bc with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Lb                               #
####################################


@configurable
def make_lb(particles,
            descriptor,
            name='bandq_Lb_{hash}',
            am_min=5350 * MeV,
            am_max=5850 * MeV,
            m_min=5390 * MeV,
            m_max=5810 * MeV,
            **decay_arguments):
    """
    Return B&Q Lb.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


#Wide mass window due to worse resolution
@configurable
def make_withneutrals_lb(particles,
                         descriptor,
                         name='bandq_withNeutrals_Bds_{hash}',
                         am_min=5000 * MeV,
                         am_max=6500 * MeV,
                         m_min=5050 * MeV,
                         m_max=6450 * MeV,
                         bpvltime_min=0.3 * picosecond,
                         **decay_arguments):
    """
    Return B&Q Lb with final state involving neutrals
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)


####################################
# Xib,Sigmab,Omegab                #
####################################


@configurable
def make_b_baryons(particles,
                   descriptor,
                   name='bandq_b_baryons_{hash}',
                   am_min=5350 * MeV,
                   am_max=6460 * MeV,
                   m_min=5390 * MeV,
                   m_max=6410 * MeV,
                   **decay_arguments):
    """
    Return B&Q Xib, Sigmab and Omegab.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


####################################
# Xibc & di-baryons                #
####################################


@configurable
def make_xibc(particles,
              descriptor,
              name='bandq_Xibc_{hash}',
              am_min=6250 * MeV,
              am_max=8000 * MeV,
              m_min=6290 * MeV,
              m_max=7950 * MeV,
              bpvltime_min=0.1 * picosecond,
              **decay_arguments):
    """
    Return B&Q Xibc.
    """
    return make_b_hadron(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvltime_min=bpvltime_min,
        **decay_arguments)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q double charm combinations.
"""

from Hlt2Conf.algorithms_thor import ParticleContainersMerger
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import c_to_hadrons
from Hlt2Conf.lines.charm import d_to_hhh, d0_to_hh, cbaryon_spectroscopy
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s
from Hlt2Conf.lines.charm import d0_to_kshh, d0_to_hhhh


@configurable
def _make_doublecharm(particles,
                      descriptor,
                      name='bandq_doublecharm_singledecay_template_{hash}',
                      allowDiffInputsForSameIDChildren=False):
    combination_code = require_all(F.ALL)
    vertex_code = require_all(F.ALL)
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        AllowDiffInputsForSameIDChildren=allowDiffInputsForSameIDChildren)


@configurable
def make_doublecharm(particles,
                     descriptors,
                     name='bandq_doublecharm_template_{hash}',
                     allowDiffInputsForSameIDChildren_dc=False):
    assert len(descriptors) > 0
    c_hadrons = []
    for descriptor in descriptors:
        c_hadrons.append(
            _make_doublecharm(
                particles=particles,
                descriptor=descriptor,
                allowDiffInputsForSameIDChildren=
                allowDiffInputsForSameIDChildren_dc))
    return ParticleContainersMerger(c_hadrons, name=name)


@configurable
def make_doublecharm_samesign(name='bandq_doublecharm_samesign_{hash}'):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmPipPip_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmKpPip_{hash}',
        descriptor='[D_s+ -> K- K+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi(
    )  #taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = cbaryon_spectroscopy._make_xic0_pkkpi(
    )  #taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip(
    )  #taken from BandQ combiner
    jpsi = make_jpsi()  #taken from BandQ & B2CC shared combiner
    psi2s = make_psi2s()  #taken from BandQ & B2CC shared combiner
    c_hadron = ParticleContainersMerger(
        [dz, dp, ds, lc, xicz, omegac, jpsi, psi2s],
        name='bandq_charmToHadrons_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        descriptors=[
            '[psi(3770) -> D0 D0]cc',
            '[psi(3770) -> D0 D+]cc',
            '[psi(3770) -> D0 D_s+]cc',
            '[psi(3770) -> D0 Lambda_c+]cc',
            '[psi(3770) -> D0 Xi_c0]cc',
            '[psi(3770) -> D0 Omega_c0]cc',
            '[psi(3770) -> D+ D+]cc',
            '[psi(3770) -> D+ D_s+]cc',
            '[psi(3770) -> D+ Lambda_c+]cc',
            '[psi(3770) -> D+ Xi_c0]cc',
            '[psi(3770) -> D+ Omega_c0]cc',
            '[psi(3770) -> D_s+ D_s+]cc',
            '[psi(3770) -> D_s+ Lambda_c+]cc',
            '[psi(3770) -> D_s+ Xi_c0]cc',
            '[psi(3770) -> D_s+ Omega_c0]cc',
            '[psi(3770) -> Lambda_c+ Lambda_c+]cc',
            '[psi(3770) -> Lambda_c+ Xi_c0]cc',
            '[psi(3770) -> Lambda_c+ Omega_c0]cc',
            '[psi(3770) -> Xi_c0 Xi_c0]cc',
            '[psi(3770) -> Xi_c0 Omega_c0]cc',
            '[psi(3770) -> Omega_c0 Omega_c0]cc',
            '[psi(3770) -> J/psi(1S) D0]cc',
            '[psi(3770) -> J/psi(1S) D+]cc',
            '[psi(3770) -> J/psi(1S) D_s+]cc',
            '[psi(3770) -> J/psi(1S) Lambda_c+]cc',
            '[psi(3770) -> J/psi(1S) Xi_c0]cc',
            '[psi(3770) -> J/psi(1S) Omega_c0]cc',
            '[psi(3770) -> psi(2S) D0]cc',
            '[psi(3770) -> psi(2S) D+]cc',
            '[psi(3770) -> psi(2S) D_s+]cc',
            '[psi(3770) -> psi(2S) Lambda_c+]cc',
            '[psi(3770) -> psi(2S) Xi_c0]cc',
            '[psi(3770) -> psi(2S) Omega_c0]cc',
        ])
    return line_alg


@configurable
def make_doublecharm_oppositesign(
        name='bandq_doublecharm_oppositesign_{hash}'):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmPipPip_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [
            d_to_hhh._make_kaons(),
            d_to_hhh._make_kaons(),
            d_to_hhh._make_pions()
        ],
        #name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name='Charm_DToHHH_DpDspToKmKpPip_{hash}',
        descriptor='[D_s+ -> K- K+ pi+]cc'
    )  #taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi(
    )  #taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = cbaryon_spectroscopy._make_xic0_pkkpi(
    )  #taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip(
    )  #taken from BandQ combiner
    c_hadron = ParticleContainersMerger([dz, dp, ds, lc, xicz, omegac],
                                        name='bandq_charmToHadrons_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        descriptors=[
            '[psi(3770) -> D~0 D0]cc', '[psi(3770) -> D~0 D+]cc',
            '[psi(3770) -> D~0 D_s+]cc', '[psi(3770) -> D~0 Lambda_c+]cc',
            '[psi(3770) -> D~0 Xi_c0]cc', '[psi(3770) -> D~0 Omega_c0]cc',
            '[psi(3770) -> D- D+]cc', '[psi(3770) -> D- D_s+]cc',
            '[psi(3770) -> D- Lambda_c+]cc', '[psi(3770) -> D- Xi_c0]cc',
            '[psi(3770) -> D- Omega_c0]cc', '[psi(3770) -> D_s- D_s+]cc',
            '[psi(3770) -> D_s- Lambda_c+]cc', '[psi(3770) -> D_s- Xi_c0]cc',
            '[psi(3770) -> D_s- Omega_c0]cc',
            '[psi(3770) -> Lambda_c~- Lambda_c+]cc',
            '[psi(3770) -> Lambda_c~- Xi_c0]cc',
            '[psi(3770) -> Lambda_c~- Omega_c0]cc',
            '[psi(3770) -> Xi_c~0 Xi_c0]cc',
            '[psi(3770) -> Xi_c~0 Omega_c0]cc',
            '[psi(3770) -> Omega_c~0 Omega_c0]cc'
        ])
    return line_alg


"""
Make B&Q quantum-correlated D0 anti-D0 combinations.
"""


@configurable
def make_doublecharm_D0ToHH(  # 2x2-body D0 decay modes  # note that this line contains D0 -> Kpi anti-D0 -> Kpi which is also triggered above.
        name='bandq_doublecharm_{hash}'):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi-]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi+')  #taken from Hlt2Charm_D0ToPimPip line in charm area
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        'D0 -> K- K+')  #taken from Hlt2Charm_D0ToKmKp line in charm area

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name='bandq_charmToXD0_2body_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HH, D0_meson_HH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ])
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH_D0ToHH(
        name='bandq_doublecharm_{hash}'):  # 3(LL)x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi-]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi+')  #taken from Hlt2Charm_D0ToPimPip line in charm area
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        'D0 -> K- K+')  #taken from Hlt2Charm_D0ToKmKp line in charm area
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(
        kshortsll, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name='bandq_charmToXD0_2body_{hash}')
    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name='bandq_charmToXD0_3bodyll_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_HH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToKsDDHH_D0ToHH(
        name='bandq_doublecharm_{hash}'):  # 3(DD)x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi-]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi+')  #taken from Hlt2Charm_D0ToPimPip line in charm area
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        'D0 -> K- K+')  #taken from Hlt2Charm_D0ToKmKp line in charm area
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(
        kshortsdd, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name='bandq_charmToXD0_2body_{hash}')
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name='bandq_charmToXD0_3bodydd_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsDDHH, D0_meson_HH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH(
        name='bandq_doublecharm_{hash}'):  # 3(LL)x3(LL)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(
        kshortsll, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name='bandq_charmToXD0_3bodyll_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_KsLLHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ])
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH_D0ToKsDDHH(
        name='bandq_doublecharm_{hash}'):  # 3(LL)x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(
        kshortsll, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(
        kshortsdd, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name='bandq_charmToXD0_3bodyll_{hash}')
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name='bandq_charmToXD0_3bodydd_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_KsDDHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToKsDDHH(
        name='bandq_doublecharm_{hash}'):  # 3(DD)x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(
        kshortsdd, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name='bandq_charmToXD0_3bodydd_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsDDHH, D0_meson_KsDDHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ])
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToHH(
        name='bandq_doublecharm_{hash}'):  # 4x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi+]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi-]cc')  #taken from Hlt2Charm_D0ToKmPip line in charm area
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi+')  #taken from Hlt2Charm_D0ToPimPip line in charm area
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        'D0 -> K- K+')  #taken from Hlt2Charm_D0ToKmKp line in charm area
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi- pi+ pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi- pi- pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> K- K+ pi- pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi- pi+ pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name='bandq_charmToXD0_2body_{hash}')
    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKpipipi, dzKKpipi, dzKKKpi],
        name='bandq_charmToXD0_4body_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_HH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToKsLLHH(
        name='bandq_doublecharm_{hash}'):  # 4x3(LL)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(
        kshortsll, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi- pi+ pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi- pi- pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> K- K+ pi- pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi- pi+ pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKpipipi, dzKKpipi, dzKKKpi],
        name='bandq_charmToXD0_4body_{hash}')
    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name='bandq_charmToXD0_3bodyll_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_KsLLHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToKsDDHH(
        name='bandq_doublecharm_{hash}'):  # 4x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(
        kshortsdd, pions, pions, "D0 -> KS0 pi- pi+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKKdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, kaons, "D0 -> KS0 K- K+"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi- pi+ pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi- pi- pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> K- K+ pi- pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi- pi+ pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKpipipi, dzKKpipi, dzKKKpi],
        name='bandq_charmToXD0_4body_{hash}')
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name='bandq_charmToXD0_3bodydd_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_KsDDHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ],
        allowDiffInputsForSameIDChildren_dc=True)
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH(
        name='bandq_doublecharm_{hash}'):  # 4x4-body D0 decay modes
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K- pi- pi+ pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        '[D0 -> K+ pi- pi- pi+]cc'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> K- K+ pi- pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py
    dzKKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(),
        'D0 -> pi- pi- pi+ pi+'
    )  #taken from Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/d0_to_hhhh.py

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKpipipi, dzKKpipi, dzKKKpi],
        name='bandq_charmToXD0_4body_{hash}')
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_HHHH],
        descriptors=[
            '[psi(3770) -> D0 D0]cc'  # due to HH modes use only D0 D0 combination, including D0 D~0 would be double counting
        ])
    return line_alg

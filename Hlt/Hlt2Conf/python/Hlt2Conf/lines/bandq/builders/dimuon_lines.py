###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q dimuon combinations.
"""

import Functors as F

from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Functors import require_all
from PyConf import configurable

from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_samesign_dimuon as make_samesign_dimuon
from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_dimuon as make_dimuon
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi
from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_muons as make_bandq_muons

_MASSMIN_UPSILON = 7900 * MeV
_PIDMU_UPSILON = -5.
#mass window and pidmu cuts of jpsi and psi2s defined in Hlt2Conf.lines.charmonium_to_dimuon

###############
#Specific dimuon lines
###############


@configurable
def make_detached_dimuon_geo(name='bandq_detached_dimuon_geo_{hash}',
                             minIPChi2_muon=25,
                             bpvdls_min=9.):
    """
    Make the detached dimuon, with only geometrical requirements.
    Used in Run 2 by the HLT2DiMuonB2KSMuMu lines, previously named DetachedDiMuonFilter
    """

    make_particles = make_dimuon(minIPChi2_muon=minIPChi2_muon)

    pvs = make_pvs()

    code = (F.BPVDLS(pvs) > bpvdls_min)

    return ParticleFilter(make_particles, name=name, Cut=F.FILTER(code))


@configurable
def make_tight_highpt_dimuon(name='bandq_tight_highpt_dimuon_{hash}',
                             minPt_dimuon=6000. * MeV,
                             minPt_muon=650. * MeV,
                             minMass_dimuon=3000. * MeV,
                             maxVertexChi2=20.):

    return make_dimuon(
        name=name,
        minPt_dimuon=minPt_dimuon,
        minPt_muon=minPt_muon,
        minMass_dimuon=minMass_dimuon,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_tight_highpt_samesign_dimuon(
        name='bandq_tight_highpt_samesign_dimuon_{hash}',
        minPt_dimuon=6000. * MeV,
        minPt_muon=650. * MeV,
        minMass_dimuon=3000. * MeV,
        maxVertexChi2=20.):

    return make_samesign_dimuon(
        name=name,
        minPt_dimuon=minPt_dimuon,
        minPt_muon=minPt_muon,
        minMass_dimuon=minMass_dimuon,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_jpsi_highpt(name='bandq_jpsi_highpt_{hash}'):
    return make_jpsi(minPt_Jpsi=2000 * MeV)


@configurable
def make_upsilon(name='bandq_upsilon_{hash}',
                 minMass_dimuon=_MASSMIN_UPSILON,
                 minPt_muon=300 * MeV,
                 minP_muon=0 * MeV,
                 minPt_upsilon=0 * MeV):

    code = (F.PT > minPt_upsilon)

    dimuon = make_dimuon(
        DecayDescriptor='Upsilon(1S) -> mu+ mu-',
        minPt_dimuon=minPt_upsilon,
        minP_muon=minP_muon,
        minPt_muon=minPt_muon,
        minPIDmu=_PIDMU_UPSILON,
        minMass_dimuon=minMass_dimuon)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


@configurable
def make_upsilon_tight(name='bandq_upsilon_tight_{hash}'):
    return make_upsilon(
        minPt_upsilon=3000 * MeV, minPt_muon=650 * MeV, minP_muon=10 * GeV)


@configurable
def make_z(name='bandq_z_{hash}',
           minMass_dimuon=40000 * MeV,
           minPt_muon=300 * MeV,
           minPt_Z=0 * MeV):

    code = (F.PT > minPt_Z)

    dimuon = make_dimuon(
        minPt_dimuon=minPt_Z,
        minPt_muon=minPt_muon,
        minMass_dimuon=minMass_dimuon)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


#special dimuon combiners
#Make detached soft dimuon.
#Used by the HLT2DiMuonSoft line.
#Ref: http://cds.cern.ch/record/2297352/files/LHCb-PUB-2017-023.pdf
@configurable
def make_soft_detached_dimuon(name='DiMuonDetachedSoftFilter_{hash}',
                              maxVertexChi2=25,
                              minPt_muon=0. * MeV,
                              minP_muon=0. * GeV,
                              minIPChi2_muon=9,
                              minIP_muon=0.3 * mm,
                              minPIDmu=-5,
                              maxDOCA=0.3,
                              maxIPChi2_muon=10000.0,
                              maxCosAngle=0.999998,
                              minRho=3,
                              minVDz=0.,
                              bpvdira_min=0.,
                              maxVz=650 * mm,
                              maxMass_dimuon=1000.0 * MeV,
                              maxIPdistRatio=1. / 60.):

    # get the long muons
    muons = make_bandq_muons(
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minIPChi2_muon=minIPChi2_muon,
        minIP_muon=minIP_muon,
        maxIPChi2_muon=maxIPChi2_muon,
        minPIDmu=minPIDmu)

    pvs = make_pvs()

    combination_code = require_all(
        F.MAXDOCACUT(maxDOCA),
        F.ALV(1, 2) < maxCosAngle, F.MASS < maxMass_dimuon)

    # require that the muons come from the same vertex
    vertex_code = require_all(
        F.CHI2DOF < maxVertexChi2,
        F.BPVVDZ(pvs) > minVDz,
        F.END_VZ < maxVz,
        F.END_VRHO < minRho,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVVDZ(pvs) < maxIPdistRatio,
        #                              F.MINIP(pvs)/F.BPVVDZ(pvs) < maxIPdistRatio #F.MINIP trigger a segmentation violation for some events
    )

    return ParticleCombiner(
        name=name + "combiner",
        Inputs=[muons, muons],
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_HighMass_dimuon(name='bandq_HighMass_dimuon_filter_{hash}',
                         maxDOCAChi2=30.,
                         maxVertexChi2=20.,
                         minP_muon=8000.0 * MeV,
                         minPt_muon=1000.0 * MeV,
                         minPIDmu=0.0,
                         minMass_dimuon=8500.0 * MeV):

    return make_dimuon(
        name=name,
        maxDOCAChi2=maxDOCAChi2,
        maxVertexChi2=maxVertexChi2,
        minP_muon=minP_muon,
        minPt_muon=minPt_muon,
        minPIDmu=minPIDmu,
        minMass_dimuon=minMass_dimuon)


@configurable
def make_HighMass_samesign_dimuon(
        name='bandq_HighMass_samesign_dimuon_filter_{hash}',
        maxDOCAChi2=30.,
        maxVertexChi2=20.,
        minP_muon=8000.0 * MeV,
        minPt_muon=1000.0 * MeV,
        minPIDmu=0.0,
        minMass_dimuon=8500.0 * MeV):

    return make_samesign_dimuon(
        name=name,
        maxDOCAChi2=maxDOCAChi2,
        maxVertexChi2=maxVertexChi2,
        minP_muon=minP_muon,
        minPt_muon=minPt_muon,
        minPIDmu=minPIDmu,
        minMass_dimuon=minMass_dimuon)


@configurable
def make_loose_dimuon(name='bandq_loose_dimuon_filter_{hash}',
                      maxDOCAChi2=30.,
                      maxVertexChi2=20.,
                      minMass_dimuon=3000.,
                      minPt_muon=650.):

    return make_dimuon(
        name=name,
        maxDOCAChi2=maxDOCAChi2,
        maxVertexChi2=maxVertexChi2,
        minMass_dimuon=minMass_dimuon,
        minPt_muon=minPt_muon)


@configurable
def make_loose_samesign_dimuon(
        name='bandq_loose_samesign_dimuon_filter_{hash}',
        maxDOCAChi2=30.,
        maxVertexChi2=20.,
        minMass_dimuon=3000.,
        minPt_muon=650.):

    return make_samesign_dimuon(
        name=name,
        maxDOCAChi2=maxDOCAChi2,
        maxVertexChi2=maxVertexChi2,
        minMass_dimuon=minMass_dimuon,
        minPt_muon=minPt_muon)

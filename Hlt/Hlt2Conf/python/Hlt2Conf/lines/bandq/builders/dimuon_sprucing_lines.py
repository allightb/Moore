###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q dimuon combiners for fully persisted sprucing lines.
The cut should be slightly tighter than the hlt2 line 
to control the bandwidth sent to the disk.
"""

from GaudiKernel.SystemOfUnits import MeV, GeV

from PyConf import configurable

from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_charmonium_detached_dimuon

_JPSI_PDG_MASS_ = 3096.9 * MeV
_PSI2S_PDG_MASS_ = 3686.1 * MeV
_MASSWINDOW_LOW_JPSI_ = 100 * MeV  #taken from Run2 stripping: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/dimuon/strippingfulldstdimuonjpsi2mumudetachedline.html
_MASSWINDOW_HIGH_JPSI_ = 100 * MeV
_MASSWINDOW_LOW_PSI2S_ = 100 * MeV
_MASSWINDOW_HIGH_PSI2S_ = 100 * MeV
_MASSMIN_JPSI = _JPSI_PDG_MASS_ - _MASSWINDOW_LOW_JPSI_
_MASSMAX_JPSI = _JPSI_PDG_MASS_ + _MASSWINDOW_HIGH_JPSI_
_MASSMIN_PSI2S = _PSI2S_PDG_MASS_ - _MASSWINDOW_LOW_PSI2S_
_MASSMAX_PSI2S = _PSI2S_PDG_MASS_ + _MASSWINDOW_HIGH_PSI2S_

_TIGHTMASSWINDOW_LOW_JPSI_ = 80 * MeV  #taken from Run2 stripping: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/dimuon/strippingfulldstdimuonjpsi2mumutosline.html
_TIGHTMASSWINDOW_HIGH_JPSI_ = 80 * MeV
_TIGHTMASSWINDOW_LOW_PSI2S_ = 80 * MeV
_TIGHTMASSWINDOW_HIGH_PSI2S_ = 80 * MeV
_TIGHTMASSMIN_JPSI = _JPSI_PDG_MASS_ - _TIGHTMASSWINDOW_LOW_JPSI_
_TIGHTMASSMAX_JPSI = _JPSI_PDG_MASS_ + _TIGHTMASSWINDOW_HIGH_JPSI_
_TIGHTMASSMIN_PSI2S = _PSI2S_PDG_MASS_ - _TIGHTMASSWINDOW_LOW_PSI2S_
_TIGHTMASSMAX_PSI2S = _PSI2S_PDG_MASS_ + _TIGHTMASSWINDOW_HIGH_PSI2S_

###############
#dimuon lines for sprucings
###############


@configurable
def make_detached_jpsi_sprucing(name='bandq_detached_jpsi_sprucing_{hash}',
                                minPt_muon=500 * MeV,
                                minPIDmu=-5.,
                                bpvdls_min=4.,
                                maxVertexChi2=20.,
                                minMass_dimuon=_MASSMIN_JPSI,
                                maxMass_dimuon=_MASSMAX_JPSI):

    return make_charmonium_detached_dimuon(
        name=name,
        minPt_muon=minPt_muon,
        minPIDmu=minPIDmu,
        bpvdls_min=bpvdls_min,
        maxVertexChi2=maxVertexChi2,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)


@configurable
def make_detached_psi2s_sprucing(name='bandq_detached_psi2s_sprucing_{hash}',
                                 minPt_muon=500 * MeV,
                                 minPIDmu=-5.,
                                 bpvdls_min=4.,
                                 maxVertexChi2=20.,
                                 minMass_dimuon=_MASSMIN_PSI2S,
                                 maxMass_dimuon=_MASSMAX_PSI2S):

    return make_charmonium_detached_dimuon(
        name=name,
        minPt_muon=minPt_muon,
        minPIDmu=minPIDmu,
        bpvdls_min=bpvdls_min,
        maxVertexChi2=maxVertexChi2,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)


@configurable
def make_tight_prompt_jpsi_sprucing(
        name='bandq_tight_prompt_jpsi_sprucing_{hash}',
        minMass_dimuon=_TIGHTMASSMIN_JPSI,
        maxMass_dimuon=_TIGHTMASSMAX_JPSI,
        minPt_Jpsi=3000 * MeV,
        minPt_muon=650 * MeV,
        minP_muon=10 * GeV,
        maxVertexChi2=20.):

    return make_jpsi(
        name=name,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPt_Jpsi=minPt_Jpsi,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_tight_prompt_psi2s_sprucing(
        name='bandq_tight_prompt_psi2s_sprucing_{hash}',
        minMass_dimuon=_TIGHTMASSMIN_PSI2S,
        maxMass_dimuon=_TIGHTMASSMAX_PSI2S,
        minPt_Psi2S=3000 * MeV,
        minPt_muon=650 * MeV,
        minP_muon=10 * GeV,
        maxVertexChi2=20.):

    return make_psi2s(
        name=name,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        minPt_muon=minPt_muon,
        minP_muon=minP_muon,
        minPt_Psi2S=minPt_Psi2S,
        maxVertexChi2=maxVertexChi2)

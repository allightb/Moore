###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define a set of B -> dimuon X combinations, where X is reconstructed using T-tracks
"""
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors.math import in_range
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi
from Hlt2Conf.standard_particles import make_LambdaTT_gated, make_KsTT_gated
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import GeV, MeV
import Functors as F
from PyConf import configurable


@configurable
def make_Lb2JpsiLambdaTT(make_jpsi=make_detached_jpsi,
                         make_lambda=make_LambdaTT_gated,
                         make_pvs=make_pvs,
                         mass_combination_min=3 * GeV,
                         mass_combination_max=10 * GeV,
                         bpvdira_min=0.985,
                         bpvip_max=0.2,
                         bpvipchi2_max=150.,
                         vertex_chi2_max=50.,
                         pt_min=600 * MeV,
                         apply_vertex_mass_cut=True,
                         mass_vertex_min=5 * GeV,
                         mass_vertex_max=6.5 * GeV,
                         name="make_Lb2JpsiLambdaTT"):
    '''
    Lb -> Jpsi Lambda(->p pi) combiner, where the Lambda decays after the UT.
    authors: Izaac Sanderswood, Brandon Roldan Tomei
    '''
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"

    pvs = make_pvs()
    Jpsi = make_jpsi()
    lambdas = make_lambda()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    if apply_vertex_mass_cut:
        vertex_cuts &= in_range(mass_vertex_min, F.MASS, mass_vertex_max)

    return ParticleCombiner(
        Inputs=[Jpsi, lambdas],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)


@configurable
def make_Bd2JpsiKsTT(make_jpsi=make_detached_jpsi,
                     make_ks=make_KsTT_gated,
                     make_pvs=make_pvs,
                     mass_combination_min=2.6 * GeV,
                     mass_combination_max=9.6 * GeV,
                     bpvdira_min=0.985,
                     bpvip_max=0.2,
                     bpvipchi2_max=150.,
                     vertex_chi2_max=50.,
                     pt_min=500 * MeV,
                     apply_vertex_mass_cut=True,
                     mass_vertex_min=4.5 * GeV,
                     mass_vertex_max=6.1 * GeV,
                     name="make_Bd2JpsiKsTT"):
    '''
    B0 -> Jpsi Ks0(->pi pi) combiner, where the Ks0 decays after the UT.
    authors: Izaac Sanderswood
    '''
    decay_descriptor = "B0 -> J/psi(1S) KS0"

    pvs = make_pvs()
    Jpsi = make_jpsi()
    ks = make_ks()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.),
        F.MAXSDOCACHI2CUT(30.),
    )

    vertex_cuts = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIP(pvs) < bpvip_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    if apply_vertex_mass_cut:
        vertex_cuts &= in_range(mass_vertex_min, F.MASS, mass_vertex_max)

    return ParticleCombiner(
        Inputs=[Jpsi, ks],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name)

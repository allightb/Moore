###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B-mesons/baryons in standard decay modes for consequent use in spectroscopy:

 >> B+ modes (BRs):
    - J/psi K+    (0.10%)
    - D~0 pi+     (0.47%)
    - D~0 3pi     (0.56%)
    - D- pi+ pi+  (1.1%)
    - D~0(*) mu+  (2.3+5.6=7.9%)
    ////// D- pi+ mu+  (0.44%)
    ////// - D~0(*) pi+pi- mu+ (0.16%)

 >> B0 modes (BRs):
    - J/psi K- pi+ (0.12%)
    - D- pi+ (0.25%)
    - D~0 pi+ pi- (0.27% = 0.27*0.67{D*-} + 0.09{NR})
    - D- 3pi (0.6%)
    - D- mu+ (2.2%)
    - D~0 pi- mu+ (3.8% = 5.0*0.677{D*-} + 0.4{NR})
         reconstructed in D~0 mu+

 >> Bs modes (BRs):
    - J/psi K+ K- (0.08%)
    - Ds- pi+ (0.30%)
    - Ds- 3pi (0.61%)
    - D~0 K- pi+ (0.10%)
    - Ds-(*) mu+ (2.4+5.3=7.7%)

 >> Lb modes (BRs):
    - J/psi p K (0.03%)
    - Lc+ pi- (0.49%)
    - Lc+ 3pi (0.76%)
    - Lc+ mu- (6.2%)
    - Lc+ pi+pi- mu- (5.6%)

 >> Bc modes:
    - J/psi pi+ (X)
    - J/psi 3pi (2.4*X)
    - J/psi mu+ (20*X)

 >> Xib-
    - J/psi Xi-(_>Lambda pi-)
    - J/psi p K- K- (not seen yet)
    - Xic0 pi- (seen)
    - Lc+ K- pi- (not seen yet)
    - Xic0 mu- (not seen yet)

 >> Xib0
    - J/psi X- pi+ (not seen yet)
    - J/psi p K- K- pi+ (not seen yet)
    - Xic+ pi- (not seen yet)
    - Lc+ K- pi+ pi- (not seen yet)
    - Xic+ mu- (not seen yet)

 >> Omb-
    - J/psi Omega (seen)
    - J/psi p+ K- K- K- pi+ (not seen yet)
    - Omc0 pi- (seen)
    - Xic+ K- pi- (seen)
    - Lc+ K- K- pi+ pi- (not seen yet)
    - Omc0 mu- (not seen yet)
    - Xic+ K- mu- (not seen yet)
"""

from PyConf import configurable

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

import Functors as F
from Functors.math import in_range
from Functors import require_all
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleContainersMerger
from RecoConf.reconstruction_objects import make_pvs

import Hlt2Conf.lines.bandq.builders.b_to_jpsiX_lines as b2jpsiX

from Hlt2Conf.lines.bandq.builders import charged_hadrons

############################
### B -> Jpsi lines

B_bpvltime_min = 0.3 * picosecond  # corresponds to ctau>0.09mm (tau_B ~ 1.5ps)
Bc_bpvltime_min = 0.15 * picosecond
B_vtx_chi2pdof_max = 10


def make_BuToJpsiK(process):
    return b2jpsiX.make_BuToJpsiKp_JpsiToMuMu(
        process,
        am_min=5100 * MeV,
        am_max=5550 * MeV,
        m_min=5160 * MeV,
        m_max=5500 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_BdToJpsiKPi(process):
    return b2jpsiX.make_BdToJpsiKpPim_JpsiToMuMu(
        process,
        am_min=5100 * MeV,
        am_max=5550 * MeV,
        m_min=5160 * MeV,
        m_max=5500 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_BsToJpsiKK(process):
    return b2jpsiX.make_BsToJpsiKpKm_JpsiToMuMu(
        process,
        am_min=5150 * MeV,
        am_max=5690 * MeV,
        m_min=5250 * MeV,
        m_max=5590 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_LbToJpsiPK(process):
    return b2jpsiX.make_LbToJpsiPKm_JpsiToMuMu(
        process,
        am_min=5400 * MeV,
        am_max=5940 * MeV,
        m_min=5500 * MeV,
        m_max=5840 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_BcToJpsiPi(process):
    return b2jpsiX.make_BcToJpsiPip_JpsiToMuMu(
        process,
        am_min=6000 * MeV,
        am_max=6675 * MeV,
        m_min=6075 * MeV,
        m_max=6600 * MeV,
        bpvltime_min=Bc_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_BcToJpsiPiPiPi(process):
    return b2jpsiX.make_BcToJpsiPipPipPim_JpsiToMuMu(
        process,
        am_min=6000 * MeV,
        am_max=6675 * MeV,
        m_min=6075 * MeV,
        m_max=6600 * MeV,
        bpvltime_min=Bc_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_BcToJpsiMu(process):
    return b2jpsiX.make_BcToJpsiMu_JpsiToMuMu(
        process,
        minPIDmu=0,  # tighter ID requirement
        am_min=4400 * MeV,
        am_max=6500 * MeV,
        m_min=4500 * MeV,  # to remove bkg from Buds->jpsi + X
        m_max=6400 * MeV,
        bpvltime_min=Bc_bpvltime_min,
        bpvdira_min=0.95,  # loosening cut due to missing particles
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_XibmToJpsiXi(process):
    return b2jpsiX.make_XibToJpsiXi_JpsiToMuMu(
        process,
        am_min=5780 * MeV,
        am_max=6120 * MeV,
        m_min=5680 * MeV,
        m_max=6020 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_Xib0ToJpsiXiPi(process):
    return b2jpsiX.make_XibToJpsiXiPi_JpsiToMuMu(
        process,
        am_min=5780 * MeV,
        am_max=6120 * MeV,
        m_min=5680 * MeV,
        m_max=6020 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_XibmToJpsiPKK(process):
    return b2jpsiX.make_XibToJpsiPKK_JpsiToMuMu(
        process,
        am_min=5780 * MeV,
        am_max=6120 * MeV,
        m_min=5680 * MeV,
        m_max=6020 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_Xib0ToJpsiPKKPi(process):
    return b2jpsiX.make_XibToJpsiPKKPi_JpsiToMuMu(
        process,
        am_min=5780 * MeV,
        am_max=6120 * MeV,
        m_min=5680 * MeV,
        m_max=6020 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_OmegabToJpsiOmega(process):
    return b2jpsiX.make_OmegabToJpsiOmega_JpsiToMuMu(
        process,
        am_min=5830 * MeV,
        am_max=6370 * MeV,
        m_min=5930 * MeV,
        m_max=6270 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


def make_OmegabToJpsiPKKKPi(process):
    return b2jpsiX.make_OmegabToJpsiPKKKPi_JpsiToMuMu(
        process,
        am_min=5830 * MeV,
        am_max=6370 * MeV,
        m_min=5930 * MeV,
        m_max=6270 * MeV,
        bpvltime_min=B_bpvltime_min,
        vtx_chi2pdof_max=10,  # 20->10
    )


############################
### B -> Charm +hadrons lines

#constants & preselected hadrons
pt_min_from_b = 300 * MeV
pi_p_min_from_b = 3 * GeV
k_p_min_from_b = 5 * GeV
hh_sumpt_min = 1.25 * GeV
hhh_sumpt_min = 1.5 * GeV
hhhh_sumpt_min = 1.75 * GeV


def make_pions_from_b():
    return charged_hadrons.make_detached_pions(
        pt_min=pt_min_from_b, p_min=pi_p_min_from_b)


def make_soft_pions():
    return charged_hadrons.make_detached_pions(pt_min=100 * MeV, p_min=2 * GeV)


def make_kaons_from_b():
    return charged_hadrons.make_detached_kaons(
        pt_min=pt_min_from_b, p_min=k_p_min_from_b)


def make_protons_from_b():
    return charged_hadrons.make_detached_protons(
        pt_min=150. * MeV,
        p_min=1. * GeV,
        mipchi2dvprimary_min=4,
        pid=F.require_all(F.PID_P > 5., (F.PID_P - F.PID_K) > 0.))


#------- Charm hadrons, tight versions specifically to build B-hadrons
import Hlt2Conf.lines.bandq.builders.c_to_hadrons as c_to_hadrons
make_Dz = c_to_hadrons.make_tight_Dz
make_Dp = c_to_hadrons.make_tight_DpToKmPipPip
make_Ds = c_to_hadrons.make_tight_DspToKmKpPip
make_Lc = c_to_hadrons.make_tight_LcToPpKmPip
make_Xicp = c_to_hadrons.make_tight_XicpToPpKmPip
make_Xic0 = c_to_hadrons.make_tight_XiczToPpKmKmPip
make_Omc0 = c_to_hadrons.make_tight_OmegaczToPpKmKmPip


#------- universal B builder
# can be used for B -> C + hadr and B -> C + mu X modes
@configurable
def make_b2cx_for_spectroscopy(
        particles,
        descriptor,
        name="bandq_B2CXfromB2OCCombiner_{hash}",
        am_min=5100 * MeV,
        am_max=5550 * MeV,
        m_min=5160 *
        MeV,  # assuming sigma~20MeV -> +-120MeV accounts for +-6sigma
        m_max=5500 * MeV,
        sum_pt_min=5 * GeV,
        achi2_doca_max=16,
        vtx_chi2pdof_max=10.,  # 20 -> 10
        bpvltime_min=0.4 *
        picosecond,  # 0.2ps -> 0.4ps, corresponds to ctau>0.12mm (tau_B ~ 1.5ps)
        bpvdira_min=0.995,  # [AVOID BIAS IN DIRECTION WRT TO PV], 0.995 (tan<0.1) is safe enough even for B from Tbb
        bpvipchi2_max=1.e+10,  # 25 -> (1m/10um)^2 [AVOID BIAS IN DIRECTION WRT TO PV]
        bcvtx_sep_min=0 * mm,  # z_D-z_B vtx separation
        comb_cut_add=None,
        vtx_cut_add=None):
    '''
    Default B decay maker: defines default cuts and B mass range.
    Inspired by _make_b2x from B2OC, with adding several extensions
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    pvs = make_pvs()

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    if comb_cut_add is not None:
        combination_code &= comb_cut_add

    if vtx_cut_add is not None:
        vertex_code &= vtx_cut_add

    if len(particles) == 2:
        return ParticleCombiner(
            particles,
            name=name,
            DecayDescriptor=descriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)

    if len(particles) == 3:

        combination12_code = require_all(F.MASS < am_max,
                                         F.DOCACHI2(1, 2) < achi2_doca_max)

        combination_code &= require_all(
            F.DOCACHI2(1, 3) < achi2_doca_max,
            F.DOCACHI2(2, 3) < achi2_doca_max)

        return ParticleCombiner(
            particles,
            name=name,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)

    if len(particles) >= 4:
        combination12_code = require_all(F.MASS < am_max,
                                         F.DOCACHI2(1, 2) < achi2_doca_max)

        combination123_code = require_all(F.MASS < am_max,
                                          F.DOCACHI2(1, 3) < achi2_doca_max,
                                          F.DOCACHI2(2, 3) < achi2_doca_max)

        combination_code &= require_all(
            F.DOCACHI2(1, 4) < achi2_doca_max,
            F.DOCACHI2(2, 4) < achi2_doca_max,
            F.DOCACHI2(3, 4) < achi2_doca_max)

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            Combination123Cut=combination123_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code)


@configurable
def make_bs2cx_for_spectroscopy(particles,
                                descriptor,
                                name="bandq_Bs2CXfromB2OCCombiner_{hash}",
                                am_min=5150 * MeV,
                                am_max=5690 * MeV,
                                m_min=5250 * MeV,
                                m_max=5590 * MeV,
                                **decay_arguments):
    return make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


@configurable
def make_lb2cx_for_spectroscopy(particles,
                                descriptor,
                                name="bandq_Lb2CXfromB2OCCombiner_{hash}",
                                am_min=5400 * MeV,
                                am_max=5940 * MeV,
                                m_min=5500 * MeV,
                                m_max=5840 * MeV,
                                **decay_arguments):
    return make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments)


@configurable
def make_xib2cx_for_spectroscopy(particles,
                                 descriptor,
                                 name="bandq_Xib2CXfromB2OCCombiner_{hash}",
                                 am_min=5780 * MeV,
                                 am_max=6120 * MeV,
                                 m_min=5680 * MeV,
                                 m_max=6020 * MeV,
                                 sum_pt_min=5.5 * GeV,
                                 **decay_arguments):
    return make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        sum_pt_min=sum_pt_min,
        **decay_arguments)


@configurable
def make_omegab2cx_for_spectroscopy(
        particles,
        descriptor,
        name="bandq_Omegab2CXfromB2OCCombiner_{hash}",
        am_min=5830 * MeV,
        am_max=6370 * MeV,
        m_min=5930 * MeV,
        m_max=6270 * MeV,
        sum_pt_min=5.5 * GeV,
        **decay_arguments):
    return make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        sum_pt_min=sum_pt_min,
        **decay_arguments)


#----------------------------------


@configurable
def make_BuToD0Pi(process):
    pion = make_pions_from_b()
    d = make_Dz()
    return make_b2cx_for_spectroscopy(
        particles=[d, pion], descriptor='[B+ -> D~0 pi+]cc')


@configurable
def make_BuToD0PiPiPi(process):
    pion = make_pions_from_b()
    pion_soft = make_soft_pions()
    d = make_Dz()

    Dst_14 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 4]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    tight4 = ((F.CHILD(4, F.PT) > pt_min_from_b) &
              (F.CHILD(4, F.P) > pi_p_min_from_b))

    hhh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hhh_sumpt_min)

    comb_cut_add = ((Dst_14 | tight4) & hhh_sumpt)

    return make_b2cx_for_spectroscopy(
        particles=[d, pion, pion, pion_soft],
        descriptor='[B+ -> D~0 pi+ pi+ pi-]cc',
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_BuToDmPiPi(process):
    pion = make_pions_from_b()
    d = make_Dp()

    comb_cut_add = (F.SUM(F.PT) - F.CHILD(1, F.PT) > hh_sumpt_min)

    return make_b2cx_for_spectroscopy(
        particles=[d, pion, pion],
        descriptor='[B+ -> D- pi+ pi+]cc',
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_BuToDspPPbar(process):
    protons = make_protons_from_b()
    ds = c_to_hadrons.make_DspToKmKpPip()
    return make_b2cx_for_spectroscopy(
        particles=[ds, protons, protons],
        am_max=6400 * MeV,
        m_max=6320 * MeV,
        descriptor='[B+ -> D_s+ p+ p~-]cc')


@configurable
def make_BdToDmPip(process):
    pion = make_pions_from_b()
    d = make_Dp()
    return make_b2cx_for_spectroscopy(
        particles=[d, pion], descriptor='[B0 -> D- pi+]cc')


@configurable
def make_BdToD0barPiPi(process):
    pion = make_pions_from_b()
    pion_soft = make_soft_pions()
    d = make_Dz()

    Dst_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS))
              < 150 * MeV)
    tight3 = ((F.CHILD(3, F.PT) > pt_min_from_b) &
              (F.CHILD(3, F.P) > pi_p_min_from_b))

    hh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hh_sumpt_min)

    comb_cut_add = ((Dst_13 | tight3) & hh_sumpt)

    return make_b2cx_for_spectroscopy(
        particles=[d, pion, pion_soft],
        descriptor='[B0 -> D~0 pi+ pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_BdToDmPipPipPim(process):
    pion = make_pions_from_b()
    d = make_Dp()

    comb_cut_add = (F.SUM(F.PT) - F.CHILD(1, F.PT) > hhh_sumpt_min)

    return make_b2cx_for_spectroscopy(
        particles=[d, pion, pion, pion],
        descriptor='[B0 -> D- pi+ pi+ pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_BsToDsmPip(process):
    pion = make_pions_from_b()
    d = make_Ds()
    return make_bs2cx_for_spectroscopy(
        particles=[d, pion],
        descriptor='[B_s0 -> D_s- pi+]cc',
    )


@configurable
def make_BsToDsmPipPipPim(process):
    pion = make_pions_from_b()
    d = make_Ds()

    comb_cut_add = (F.SUM(F.PT) - F.CHILD(1, F.PT) > hhh_sumpt_min)

    return make_bs2cx_for_spectroscopy(
        particles=[d, pion, pion, pion],
        descriptor='[B_s0 -> D_s- pi+ pi+ pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_BsToD0bKmPip(process):
    pion = make_pions_from_b()
    kaon = make_kaons_from_b()
    d = make_Dz()

    comb_cut_add = (F.SUM(F.PT) - F.CHILD(1, F.PT) > hh_sumpt_min)

    return make_bs2cx_for_spectroscopy(
        particles=[d, kaon, pion],
        descriptor='[B_s0 -> D~0 K- pi+]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_LbToLcPim(process):
    pion = make_pions_from_b()
    cbaryon = make_Lc()
    return make_lb2cx_for_spectroscopy(
        particles=[cbaryon, pion],
        descriptor='[Lambda_b0 -> Lambda_c+ pi-]cc',
    )


@configurable
def make_LbToLcPipPimPim(process):
    pion = make_soft_pions()
    cbaryon = make_Lc()

    Sc_12 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 2]) - F.CHILD(1, F.MASS)) <
             300 * MeV)
    Sc_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS)) <
             300 * MeV)
    Sc_14 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 4]) - F.CHILD(1, F.MASS)) <
             300 * MeV)

    tight2 = ((F.CHILD(2, F.PT) > pt_min_from_b) &
              (F.CHILD(2, F.P) > pi_p_min_from_b))
    tight3 = ((F.CHILD(3, F.PT) > pt_min_from_b) &
              (F.CHILD(3, F.P) > pi_p_min_from_b))
    tight4 = ((F.CHILD(4, F.PT) > pt_min_from_b) &
              (F.CHILD(4, F.P) > pi_p_min_from_b))

    hhh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hhh_sumpt_min)

    comb_cut_add = ((Sc_12 | tight2) & (Sc_13 | tight3) &
                    (Sc_14 | tight4) & hhh_sumpt)

    return make_lb2cx_for_spectroscopy(
        particles=[cbaryon, pion, pion, pion],
        descriptor='[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc',
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_XibmToXic0Pim(process):
    pion = make_pions_from_b()
    cbaryon = make_Xic0()
    return make_xib2cx_for_spectroscopy(
        particles=[cbaryon, pion],
        descriptor='[Xi_b- -> Xi_c0 pi-]cc',
    )


@configurable
def make_Xib0ToXicpPim(process):
    pion = make_pions_from_b()
    cbaryon = make_Xicp()
    return make_xib2cx_for_spectroscopy(
        particles=[cbaryon, pion],
        descriptor='[Xi_b0 -> Xi_c+ pi-]cc',
    )


@configurable
def make_XibmToLcKmPim(process):
    pion = make_soft_pions()
    kaon = make_kaons_from_b()
    cbaryon = make_Lc()

    Sc_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS)) <
             300 * MeV)

    tight3 = ((F.CHILD(3, F.PT) > pt_min_from_b) &
              (F.CHILD(3, F.P) > pi_p_min_from_b))

    hh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hh_sumpt_min)

    comb_cut_add = ((Sc_13 | tight3) & hh_sumpt)

    return make_xib2cx_for_spectroscopy(
        particles=[cbaryon, kaon, pion],
        descriptor='[Xi_b- -> Lambda_c+ K- pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_Xib0ToLcKmPipPim(process):
    pion = make_soft_pions()
    kaon = make_kaons_from_b()
    cbaryon = make_Lc()

    Sc_13 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS)) <
             300 * MeV)
    Sc_14 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 4]) - F.CHILD(1, F.MASS)) <
             300 * MeV)

    tight3 = ((F.CHILD(3, F.PT) > pt_min_from_b) &
              (F.CHILD(3, F.P) > pi_p_min_from_b))
    tight4 = ((F.CHILD(4, F.PT) > pt_min_from_b) &
              (F.CHILD(4, F.P) > pi_p_min_from_b))

    hhh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hhh_sumpt_min)

    comb_cut_add = ((Sc_13 | tight3) & (Sc_14 | tight4) & hhh_sumpt)

    return make_xib2cx_for_spectroscopy(
        particles=[cbaryon, kaon, pion, pion],
        descriptor='[Xi_b0 -> Lambda_c+ K- pi+ pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_OmbToOmcPim(process):
    pion = make_pions_from_b()
    cbaryon = make_Omc0()
    return make_omegab2cx_for_spectroscopy(
        particles=[cbaryon, pion],
        descriptor='[Omega_b- -> Omega_c0 pi-]cc',
    )


@configurable
def make_OmbToXicpKmPim(process):
    pion = make_soft_pions()
    kaon = make_kaons_from_b()
    cbaryon = make_Omc0()

    Xicst_13 = (
        (F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS)) <
        185 * MeV)

    tight3 = ((F.CHILD(3, F.PT) > pt_min_from_b) &
              (F.CHILD(3, F.P) > pi_p_min_from_b))

    hh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hh_sumpt_min)

    comb_cut_add = ((Xicst_13 | tight3) & hh_sumpt)

    return make_omegab2cx_for_spectroscopy(
        particles=[cbaryon, kaon, pion],
        descriptor='[Omega_b- -> Xi_c+ K- pi-]cc',
        comb_cut_add=comb_cut_add)


@configurable
def make_OmbToLcpKmKmPipPim(process):
    pion = make_soft_pions()
    kaon = make_kaons_from_b()
    cbaryon = make_Lc()

    Sc_14 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 4]) - F.CHILD(1, F.MASS)) <
             300 * MeV)
    Sc_15 = ((F.SUBCOMB(Functor=F.MASS, Indices=[1, 5]) - F.CHILD(1, F.MASS)) <
             300 * MeV)

    tight4 = ((F.CHILD(4, F.PT) > pt_min_from_b) &
              (F.CHILD(4, F.P) > pi_p_min_from_b))
    tight5 = ((F.CHILD(5, F.PT) > pt_min_from_b) &
              (F.CHILD(5, F.P) > pi_p_min_from_b))

    hhhh_sumpt = ((F.SUM(F.PT) - F.CHILD(1, F.PT)) > hhhh_sumpt_min)

    comb_cut_add = ((Sc_14 | tight4) & (Sc_15 | tight5) & hhhh_sumpt)

    return make_omegab2cx_for_spectroscopy(
        particles=[cbaryon, kaon, kaon, pion, pion],
        descriptor='[Omega_b- -> Lambda_c+ K- K- pi+ pi-]cc',
        comb_cut_add=comb_cut_add)


############################
### B -> Charm + mu nu lines
### building own lines instead of relying on SL to ensure more coherence with B->Dh(hh) ones above
from Hlt2Conf.lines.semileptonic.builders.base_builder import make_muons_from_b


@configurable
def make_b2cmu_for_spectroscopy(
        particles,
        descriptor,
        name="bandq_B2CMuCombiner_{hash}",
        am_min=2100 * MeV,
        am_max=7200 * MeV,
        m_min=2200 * MeV,
        m_max=7000 * MeV,
        bpvdira_min=0.95,  # loosening cut due to missing particles
        **decay_arguments):
    return make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        bpvdira_min=bpvdira_min,
        **decay_arguments)


@configurable
def make_BToD0mu(process, lepton="mu"):
    mu = make_muons_from_b()
    d = make_Dz()
    return make_b2cmu_for_spectroscopy(
        particles=[d, mu], descriptor='[B+ -> D~0 mu+]cc')


@configurable
def make_BToDpmu(process, lepton="mu"):
    mu = make_muons_from_b()
    d = make_Dp()
    return make_b2cmu_for_spectroscopy(
        particles=[d, mu], descriptor='[B0 -> D- mu+]cc')


@configurable
def make_BsToDsmu(process, lepton="mu"):
    mu = make_muons_from_b()
    d = make_Ds()
    return make_b2cmu_for_spectroscopy(
        particles=[d, mu], descriptor='[B_s0 -> D_s- mu+]cc')


@configurable
def make_LbToLcmu(process, lepton="mu"):
    mu = make_muons_from_b()
    cbaryon = make_Lc()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, mu], descriptor='[Lambda_b0 -> Lambda_c+ mu-]cc')


@configurable
def make_Xib0ToXicpmu(process, lepton="mu"):
    mu = make_muons_from_b()
    cbaryon = make_Xicp()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, mu], descriptor='[Xi_b0 -> Xi_c+ mu-]cc')


@configurable
def make_XibmToXic0mu(process, lepton="mu"):
    mu = make_muons_from_b()
    cbaryon = make_Xic0()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, mu], descriptor='[Xi_b- -> Xi_c0 mu-]cc')


@configurable
def make_OmbToOmcmu(process, lepton="mu"):
    mu = make_muons_from_b()
    cbaryon = make_Omc0()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, mu], descriptor='[Omega_b- -> Omega_c0 mu-]cc')


@configurable
def make_OmbToXicpKmu(process, lepton="mu"):
    mu = make_muons_from_b()
    kaon = make_kaons_from_b()
    cbaryon = make_Xicp()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, kaon, mu],
        descriptor='[Omega_b- -> Xi_c+ K- mu-]cc')


@configurable
def make_OmbToXic0Kmu(process, lepton="mu"):
    mu = make_muons_from_b()
    kaon = make_kaons_from_b()
    cbaryon = make_Xic0()
    return make_b2cmu_for_spectroscopy(
        particles=[cbaryon, kaon, mu],
        descriptor='[Omega_b- -> Xi_c0 K- mu-]cc')


############################
### Doing combinations


@configurable
def make_Bu(process, name="bandq_BuForSpectroscopy"):
    BuToJpsiK = make_BuToJpsiK(process)
    BuToD0Pi = make_BuToD0Pi(process)
    BuToD0PiPiPi = make_BuToD0PiPiPi(process)
    BuToDmPiPi = make_BuToDmPiPi(process)

    container = [BuToJpsiK, BuToD0Pi, BuToD0PiPiPi, BuToDmPiPi]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Bd(process, name="bandq_BdForSpectroscopy"):
    BdToJpsiKPi = make_BdToJpsiKPi(process)
    BdToDmPip = make_BdToDmPip(process)
    BdToD0barPiPi = make_BdToD0barPiPi(process)
    BdToDmPipPipPim = make_BdToDmPipPipPim(process)

    container = [BdToJpsiKPi, BdToDmPip, BdToD0barPiPi, BdToDmPipPipPim]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Bs(process, name="bandq_BsForSpectroscopy"):
    BsToJpsiKK = make_BsToJpsiKK(process)
    BsToDsmPip = make_BsToDsmPip(process)
    BsToDsmPipPipPim = make_BsToDsmPipPipPim(process)
    BsToD0bKmPip = make_BsToD0bKmPip(process)

    container = [BsToJpsiKK, BsToDsmPip, BsToDsmPipPipPim, BsToD0bKmPip]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Lb(process, name="bandq_LbForSpectroscopy"):
    LbToJpsiPK = make_LbToJpsiPK(process)
    LbToLcPim = make_LbToLcPim(process)
    LbToLcPipPimPim = make_LbToLcPipPimPim(process)

    container = [LbToJpsiPK, LbToLcPim, LbToLcPipPimPim]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Bc(process, name="bandq_BcForSpectroscopy"):
    BcToJpsiPi = make_BcToJpsiPi(process)
    BcToJpsiPiPiPi = make_BcToJpsiPiPiPi(process)

    container = [BcToJpsiPi, BcToJpsiPiPiPi]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Xibm(process, name="bandq_XibmForSpectroscopy"):
    XibmToJpsiXi = make_XibmToJpsiXi(process)
    XibmToJpsiPKK = make_XibmToJpsiPKK(process)

    XibmToXic0Pim = make_XibmToXic0Pim(process)
    XibmToLcKmPim = make_XibmToLcKmPim(process)

    container = [XibmToJpsiXi, XibmToJpsiPKK, XibmToXic0Pim, XibmToLcKmPim]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Xib0(process, name="bandq_Xib0ForSpectroscopy"):
    Xib0ToJpsiXiPi = make_Xib0ToJpsiXiPi(process)
    Xib0ToJpsiPKKPi = make_Xib0ToJpsiPKKPi(process)

    Xib0ToXicpPim = make_Xib0ToXicpPim(process)
    Xib0ToLcKmPipPim = make_Xib0ToLcKmPipPim(process)

    container = [
        Xib0ToJpsiXiPi, Xib0ToJpsiPKKPi, Xib0ToXicpPim, Xib0ToLcKmPipPim
    ]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_Omegab(process, name="bandq_OmegabForSpectroscopy"):
    OmegabToJpsiOmega = make_OmegabToJpsiOmega(process)
    OmegabToJpsiPKKKPi = make_OmegabToJpsiPKKKPi(process)

    OmbToOmcPim = make_OmbToOmcPim(process)
    OmbToXicpKmPim = make_OmbToXicpKmPim(process)
    OmbToLcpKmKmPipPim = make_OmbToLcpKmKmPipPim(process)

    container = [
        OmegabToJpsiOmega, OmegabToJpsiPKKKPi, OmbToOmcPim, OmbToXicpKmPim,
        OmbToLcpKmKmPipPim
    ]

    return ParticleContainersMerger(container, name=name)


@configurable
def make_AllB(process, name="bandq_AllBForSpectroscopy"):
    Bu = make_Bu(process)
    Bd = make_Bd(process)
    Bs = make_Bs(process)
    Lb = make_Lb(process)

    Bc = make_Bc(process)

    Xibm = make_Xibm(process)
    Xib0 = make_Xib0(process)
    Omegab = make_Omegab(process)

    return ParticleContainersMerger([Bu, Bd, Bs, Lb, Bc, Xibm, Xib0, Omegab],
                                    name=name)


# specifically semileptonic
@configurable
def make_Bu_SL(process, name="bandq_BuForSpectroscopySL"):
    return make_BToD0mu(process, "mu")


@configurable
def make_Bd_SL(process, name="bandq_BdForSpectroscopySL"):
    return make_BToDpmu(process, "mu")


@configurable
def make_Bud_SL(process, name="bandq_BudForSpectroscopySL"):
    D0mu = make_BToD0mu(process, "mu")
    Dpmu = make_BToDpmu(process, "mu")

    return ParticleContainersMerger([D0mu, Dpmu], name=name)


@configurable
def make_Bs_SL(process):
    return make_BsToDsmu(process, "mu")


@configurable
def make_Lb_SL(process):
    return make_LbToLcmu(process, "mu")


@configurable
def make_Bc_SL(process):
    return make_BcToJpsiMu(process)


@configurable
def make_Xibm_SL(process, name="bandq_XibmForSpectroscopySL"):
    return make_XibmToXic0mu(process, "mu")


@configurable
def make_Xib0_SL(process, name="bandq_Xib0ForSpectroscopySL"):
    return make_Xib0ToXicpmu(process, "mu")


@configurable
def make_Xib_SL(process, name="bandq_XibForSpectroscopySL"):
    Xicpmu = make_Xib0ToXicpmu(process, "mu")
    Xic0mu = make_XibmToXic0mu(process, "mu")
    return ParticleContainersMerger([Xicpmu, Xic0mu], name=name)


@configurable
def make_Omegab_SL(process):
    return make_OmbToOmcmu(process, "mu")


@configurable
def make_AllB_SL(process, name="bandq_AllBForSpectroscopySL"):
    D0mu = make_BToD0mu(process, "mu")
    Dpmu = make_BToDpmu(process, "mu")
    Dsmu = make_BsToDsmu(process, "mu")
    Lcmu = make_LbToLcmu(process, "mu")

    Jpsimu = make_BcToJpsiMu(process)

    Xicpmu = make_Xib0ToXicpmu(process, "mu")
    Xic0mu = make_XibmToXic0mu(process, "mu")
    Omcmu = make_OmbToOmcmu(process, "mu")

    return ParticleContainersMerger(
        [D0mu, Dpmu, Dsmu, Lcmu, Jpsimu, Xicpmu, Xic0mu, Omcmu], name=name)

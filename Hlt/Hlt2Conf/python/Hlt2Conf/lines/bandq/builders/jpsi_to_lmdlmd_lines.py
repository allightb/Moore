###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define the Jpsi -> Lambda anti-Lambda lines where (anti-)Lambda is reconstructed using LL/DD/TT tracks
"""
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable

import Functors as F
from Functors.math import in_range

from Hlt2Conf.standard_particles import make_down_pions, make_down_protons, _make_V0TT, make_ttrack_pions_for_V0, make_ttrack_protons_for_V0

from .charged_hadrons import make_detached_pions, make_detached_protons, make_charged_hadrons


@configurable
def make_lambdall():
    """Make Lambda -> p+ pi- from long tracks."""
    pvs = make_pvs()
    pions = make_detached_pions(
        name="bandq_deteached_pions_for_lmd_ll_{hash}",
        mipchi2dvprimary_min=20.,
        pt_min=100. * MeV,
        p_min=2. * GeV,
        pid=(F.PID_K < 5.))
    protons = make_detached_protons(
        name="bandq_deteached_protons_for_lmd_ll_{hash}",
        mipchi2dvprimary_min=10.,
        pt_min=400. * MeV,
        p_min=9. * GeV,
        pid=F.require_all(F.PID_P > 5., (F.PID_P - F.PID_K) > 0.))
    combinationcode = F.require_all(
        in_range(1095 * MeV, F.MASS, 1140 * MeV), F.MAXDOCACHI2CUT(4.),
        F.PT > 500. * MeV, F.MAXDOCACUT(0.12 * mm))
    vertexcode = F.require_all(
        in_range(1105 * MeV, F.MASS, 1130 * MeV), F.CHI2DOF < 5.,
        in_range(-100 * mm, F.END_VZ, 500 * mm),
        F.BPVVDZ(pvs) > 2 * mm,
        F.BPVLTIME(pvs) > 2 * ps)
    return ParticleCombiner(
        Inputs=[protons, pions],
        name="bandq_make_LambdaLL_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combinationcode,
        CompositeCut=vertexcode)


@configurable
def make_lambdadd():
    """Make Lambda -> p+ pi- from downstream tracks."""
    pvs = make_pvs()
    pions = make_charged_hadrons(
        name="bandq_deteached_pions_for_lmd_dd_{hash}",
        make_particles=make_down_pions,
        mipchi2dvprimary_min=6.,
        pt_min=100. * MeV,
        p_min=2. * GeV)
    protons = make_charged_hadrons(
        name="bandq_deteached_protons_for_lmd_dd_{hash}",
        make_particles=make_down_protons,
        mipchi2dvprimary_min=6.,
        pt_min=500. * MeV,
        p_min=8. * GeV,
        pid=F.require_all(F.PID_P > 5., (F.PID_P - F.PID_K) > 0.))
    combinationcode = F.require_all(
        in_range(1085 * MeV, F.MASS, 1145 * MeV), F.MAXDOCACHI2CUT(4.),
        F.PT > 600. * MeV, F.MAXDOCACUT(4.0 * mm))
    vertexcode = F.require_all(
        in_range(1105 * MeV, F.MASS, 1130 * MeV), F.CHI2DOF < 5.,
        in_range(300 * mm, F.END_VZ, 2275 * mm),
        F.BPVIPCHI2(pvs) < 10.,
        F.BPVVDZ(pvs) > 300 * mm,
        F.BPVLTIME(pvs) > 4.5 * ps)
    return ParticleCombiner(
        Inputs=[protons, pions],
        name="bandq_make_LambdaDD_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combinationcode,
        CompositeCut=vertexcode)


@configurable
def make_lambdatt():
    """Make Lambda -> p+ pi- from t-tracks."""
    ttrack_pions = make_ttrack_pions_for_V0(
        pt_min=300. * MeV,
        p_min=4. * GeV,
        minipchi2_min=60.,
    )
    ttrack_protons = make_ttrack_protons_for_V0(
        pt_min=800. * MeV,
        p_min=20. * GeV,
        minip_max=200.,
        minipchi2_max=10000.,
        minipchi2_min=10.,
    )
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[ttrack_protons, ttrack_pions],
        descriptors=descriptors,
        maxdoca=40.,
        maxdocachi2=100.,
        p_min=30 * GeV,
        max_chi2=10,
        bpv_ip_chi2_max=8.,
        name="bandq_make_Lambda0RKTTCombiner_{hash}")


@configurable
def make_jpsi_to_lmdlmd(particle1,
                        particle2,
                        name="bandq_Jpsi2LambdaLambdaCombiner_{hash}",
                        am_Min=2696.0 * MeV,
                        am_Max=3496.0 * MeV,
                        m_Min=2796.0 * MeV,
                        m_Max=3396.0 * MeV,
                        VCHI2PDOF_Max=5.0,
                        BPVIPCHI2_Max=10.0,
                        JpsiVZ_Min=-200.0 * mm,
                        JpsiVZ_Max=200.0 * mm):
    pvs = make_pvs()
    combination_code = F.require_all(in_range(am_Min, F.MASS, am_Max))
    vertex_code = F.require_all(
        in_range(m_Min, F.MASS, m_Max), F.CHI2DOF < VCHI2PDOF_Max,
        F.BPVIPCHI2(pvs) < BPVIPCHI2_Max,
        in_range(JpsiVZ_Min, F.END_VZ, JpsiVZ_Max))
    return ParticleCombiner(
        Inputs=[particle1, particle2],
        name=name,
        DecayDescriptor="J/psi(1S) -> Lambda0 Lambda~0",
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_jpsi_to_lmdlmd_llll(name="bandq_JpsiToLambdaLambda_LLLL"):
    lambdall = make_lambdall()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdall, particle2=lambdall, name=name)
    return line_alg


@configurable
def make_jpsi_to_lmdlmd_lldd(name="bandq_JpsiToLambdaLambda_LLDD"):
    lambdall = make_lambdall()
    lambdadd = make_lambdadd()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdall, particle2=lambdadd, name=name)
    return line_alg


@configurable
def make_jpsi_to_lmdlmd_dddd(name="bandq_JpsiToLambdaLambda_DDDD"):
    lambdadd = make_lambdadd()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdadd, particle2=lambdadd, name=name)
    return line_alg


@configurable
def make_jpsi_to_lmdlmd_ttll(name="bandq_JpsiToLambdaLambda_TTLL"):
    lambdatt = make_lambdatt()
    lambdall = make_lambdall()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdatt, particle2=lambdall, BPVIPCHI2_Max=20.0, name=name)
    return line_alg


@configurable
def make_jpsi_to_lmdlmd_ttdd(name="bandq_JpsiToLambdaLambda_TTDD"):
    lambdatt = make_lambdatt()
    lambdadd = make_lambdadd()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdatt, particle2=lambdadd, BPVIPCHI2_Max=30.0, name=name)
    return line_alg


@configurable
def make_jpsi_to_lmdlmd_tttt(name="bandq_JpsiToLambdaLambda_TTTT"):
    lambdatt = make_lambdatt()
    line_alg = make_jpsi_to_lmdlmd(
        particle1=lambdatt,
        particle2=lambdatt,
        VCHI2PDOF_Max=3.,
        BPVIPCHI2_Max=4.,
        name=name)
    return line_alg

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q HLT2 lines

"""

from PyConf import configurable

from Moore.config import Hlt2Line, register_line_builder

#get bandq builders
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters
from Hlt2Conf.lines.bandq.builders import b_to_jpsiX_lines, \
    doublecharm, \
    b_for_spectroscopy
from Hlt2Conf.lines.bandq.builders import dps_lines
from Hlt2Conf.lines.bandq.builders import dimuon_lines
from Hlt2Conf.lines.bandq.builders import ccbargamma_conv_lines, ccbarmumu_lines
from Hlt2Conf.lines.bandq.builders import qqbar_to_hadrons
from Hlt2Conf.lines.bandq.builders import ppmumu_lines
from Hlt2Conf.lines.bandq.builders import bbaryon_to_lcdsX_lines
from Hlt2Conf.lines.bandq.builders import jpsi_to_lmdlmd_lines
from Hlt2Conf.lines.bandq.builders import b_to_jpsiX_NoMuonID
from Hlt2Conf.lines.bandq.builders import b_to_LcX_TT_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi
from Hlt2Conf.lines.bandq.builders.b_to_jpsiX_TT_lines import make_Lb2JpsiLambdaTT, make_Bd2JpsiKsTT
from Hlt2Conf.lines.bandq.builders.jpsi_to_lmdlmd_lines import make_lambdall, make_lambdadd, make_lambdatt
from Hlt2Conf.lines.b_to_open_charm.builders.d_builder import make_dsplus_to_kpkmpip  #import this to force Lb2LcDs T-track line do the Ds sector long-track reconstruction and selection first.

PROCESS = 'hlt2'
turbo_lines = {}
full_lines = {}


@register_line_builder(turbo_lines)
@configurable
def ccbarToPpPmPrompt_line(name='Hlt2BandQ_ccbarToPpPmPrompt',
                           prescale=1,
                           persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_ccbarToPpPm()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def ccbarToPpPmHighPrompt_line(name='Hlt2BandQ_ccbarToPpPmHighPrompt',
                               prescale=1,
                               persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_ccbarToPpPmHigh()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def doubleCcbarToPpPmPrompt_line(name='Hlt2BandQ_doubleCcbarToPpPmPrompt',
                                 prescale=1,
                                 persistreco=False):
    line_alg = qqbar_to_hadrons.make_prompt_doubleCcbarToPpPm()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


#Removed. Covered by Hlt2BandQ_DiMuonUpsilon line
#@register_line_builder(turbo_lines)
#@configurable
#def DiMuonHighMass_line(name='Hlt2BandQ_DiMuonHighMass',
#                        prescale=1,
#                        persistreco=True):
#    line_alg = dimuon_lines.make_HighMass_dimuon()
#    return Hlt2Line(
#        name=name,
#        algs=make_prefilters() + [line_alg],
#        prescale=prescale,
#        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonSameSignHighMass_line(name='Hlt2BandQ_DiMuonSameSignHighMassFull',
                                prescale=0.2,
                                persistreco=True):
    line_alg = dimuon_lines.make_HighMass_samesign_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonInc_line(name='Hlt2BandQ_DiMuonIncFull',
                   prescale=0.03,
                   persistreco=True):
    line_alg = dimuon_lines.make_loose_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonSameSignInc_line(name='Hlt2BandQ_DiMuonSameSignIncFull',
                           prescale=0.1,
                           persistreco=True):
    line_alg = dimuon_lines.make_loose_samesign_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonIncHighPT_line(name='Hlt2BandQ_DiMuonIncHighPTFull',
                         prescale=1,
                         persistreco=True):
    line_alg = dimuon_lines.make_tight_highpt_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonSameSignIncHighPT_line(name='Hlt2BandQ_DiMuonSameSignIncHighPTFull',
                                 prescale=1,
                                 persistreco=True):
    line_alg = dimuon_lines.make_tight_highpt_samesign_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def DiMuonSoft_line(name='Hlt2BandQ_DiMuonSoftFull',
                    prescale=1,
                    persistreco=True):
    line_alg = dimuon_lines.make_soft_detached_dimuon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


#fully covered by Hlt2_JpsiToMuMu line.
#@register_line_builder(turbo_lines)
#@configurable
#def JpsiToMuMuHighPt_line(name='Hlt2BandQ_DiMuonJPsiHighPT', prescale=1):
#    line_alg = dimuon_lines.make_jpsi_highpt()
#    return Hlt2Line(
#        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(full_lines)
@configurable
def UpsilonToMuMu_line(name='Hlt2BandQ_DiMuonUpsilonFull',
                       prescale=1,
                       persistreco=True):
    line_alg = dimuon_lines.make_upsilon()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def ZToMuMu_line(name='Hlt2BandQ_DiMuonZ', prescale=1):
    line_alg = dimuon_lines.make_z()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BcToJpsiPip_JpsiToMuMu_line(name='Hlt2BandQ_BcToJpsiPip_JpsiToMuMu',
                                prescale=1,
                                persistreco=True):
    """Bc+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPip_JpsiToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def BcToPsi2SPip_Psi2SToMuMu_line(name='Hlt2BandQ_BcToPsi2SPip_Psi2SToMuMu',
                                  prescale=1):
    """Bc+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_line(name='Hlt2BandQ_BpToJpsiKp_JpsiToMuMu',
                               prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiKp_JpsiToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BuToPsi2SKp_Psi2SToMuMu_line(name='Hlt2BandQ_BpToPsi2SKp_Psi2SToMuMu',
                                 prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToPsi2SKp_Psi2SToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BuToJpsiPip_JpsiToMuMu_line(name='Hlt2BandQ_BpToJpsiPip_JpsiToMuMu',
                                prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiPip_JpsiToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BuToPsi2SPip_Psi2SToMuMu_line(name='Hlt2BandQ_BpToPsi2SPip_Psi2SToMuMu',
                                  prescale=1):
    """B+ --> psi(2S)(-> mu+ mu-)  pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_jpsijpsi_line(name='Hlt2BandQ_DoubleDiMuon_Jpsi_Jpsi_DPS',
                                   prescale=1,
                                   persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsijpsi()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_psi2spsi2s_line(
        name='Hlt2BandQ_DoubleDiMuon_Psi2S_Psi2S_DPS',
        prescale=1,
        persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_psi2spsi2s()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_upsilonupsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Upsilon_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_upsilonupsilon()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_jpsipsi2s_line(
        name='Hlt2BandQ_DoubleDiMuon_Jpsi_Psi2S_DPS', prescale=1,
        persistreco=True):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsipsi2s()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_jpsiupsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Jpsi_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_jpsiupsilon()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def doubledimuon_dps_psi2supsilon_line(
        name='Hlt2BandQ_DoubleDiMuon_Psi2S_Upsilon_DPS', prescale=1):
    """DoubleDiMuon DPS line"""
    line_alg = dps_lines.make_doubledimuon_dps_psi2supsilon()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def chic2jpsimumu_line(name="Hlt2BandQ_ChicToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_chic2jpsimumu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(full_lines)
@configurable
def doublecharm_samesign_line(name="Hlt2BandQ_DoubleCharmSameSignFull",
                              prescale=1,
                              persistreco=True):
    line_alg = doublecharm.make_doublecharm_samesign()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def X2jpsimumu_line(name="Hlt2BandQ_XToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_X2jpsimumu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(full_lines)
@configurable
def doublecharm_oppositesign_line(name="Hlt2BandQ_DoubleCharmOppositeSignFull",
                                  prescale=1,
                                  persistreco=True):
    line_alg = doublecharm.make_doublecharm_oppositesign()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToHH_line(name="Hlt2BandQ_DoubleCharm_D0ToHHFull",
                            prescale=1,
                            persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToKsDDHH_D0ToHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToKsDDHH_D0ToHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH_D0ToHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToKsLLHH_line(name="Hlt2BandQ_DoubleCharm_D0ToKsLLHHFull",
                                prescale=1,
                                persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToKsDDHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToKsDDHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToKsDDHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToKsDDHH_line(name="Hlt2BandQ_DoubleCharm_D0ToKsDDHHFull",
                                prescale=1,
                                persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsLLHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsLLHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsLLHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsDDHH_line(
        name="Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsDDHHFull",
        prescale=1,
        persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsDDHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def doublecharm_D0ToHHHH_line(name="Hlt2BandQ_DoubleCharm_D0ToHHHHFull",
                              prescale=1,
                              persistreco=True):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def hc2jpsimumu_line(name="Hlt2BandQ_hcToJpsiMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_hc2jpsimumu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def X2chicmumu_line(name="Hlt2BandQ_XToChicMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_X2chicmumu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def chib2upsilonmumu_line(name="Hlt2BandQ_ChibToUpsilonMuMu", prescale=1):
    line_alg = ccbarmumu_lines.make_chib2upsilonmumu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def bc2jpsimu_line(name="Hlt2BandQ_BcToJpsiMu_JpsiToMuMu",
                   prescale=1,
                   persistreco=True):
    line_alg = b_to_jpsiX_lines.make_BcToJpsiMu_JpsiToMuMu(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def ccbar2jpsigamma_convDD_line(name="Hlt2BandQ_CcbarToJpsiGamma_ConvDD",
                                prescale=1):
    line_alg = ccbargamma_conv_lines.make_ccbar2jpsigamma_convDD()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def bbbar2upsilongamma_convDD_line(name="Hlt2BandQ_BbbarToUpsilonGamma_ConvDD",
                                   prescale=1):
    line_alg = ccbargamma_conv_lines.make_bbbar2upsilongamma_convDD()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def ccbar2jpsigamma_convLL_line(name="Hlt2BandQ_CcbarToJpsiGamma_ConvLL",
                                prescale=1):
    line_alg = ccbargamma_conv_lines.make_ccbar2jpsigamma_convLL()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def bbbar2upsilongamma_convLL_line(name="Hlt2BandQ_BbbarToUpsilonGamma_ConvLL",
                                   prescale=1):
    line_alg = ccbargamma_conv_lines.make_bbbar2upsilongamma_convLL()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def BBbarToPhiPhi_line(name='Hlt2BandQ_BBbarToPhiPhi', prescale=1):
    """Upsilon(1S) -> phi(1020) phi(1020)"""
    line_alg = qqbar_to_hadrons.make_bbbar_to_phiphi()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def PPMuMu_Detached_line(name='Hlt2BandQ_PPMuMu_Detached', prescale=1):
    """detached p+ p~- mu+ mu-"""
    line_alg = ppmumu_lines.make_ppmumu_Detached()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def PPMuMu_Hc_line(name='Hlt2BandQ_PPMuMu_Hc', prescale=1):
    """p+ p~- mu+ mu- in charm mass region"""
    line_alg = ppmumu_lines.make_ppmumu_Hc()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def PPMuMu_High_line(name='Hlt2BandQ_PPMuMu_High', prescale=1):
    """p+ p~- mu+ mu- in high mass region"""
    line_alg = ppmumu_lines.make_ppmumu_High()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


###############################################
#### B for spectroscopy lines  #################
##  ------ fully reconstructed modes
@register_line_builder(full_lines)
@configurable
def Bu_for_spectroscopy_line(name="Hlt2BandQ_BuForSpectroscopyFull",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bu(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Bd_for_spectroscopy_line(name="Hlt2BandQ_BdForSpectroscopyFull",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bd(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Bs_for_spectroscopy_line(name="Hlt2BandQ_BsForSpectroscopyFull",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bs(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Lb_for_spectroscopy_line(name="Hlt2BandQ_LbForSpectroscopyFull",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Lb(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Bc_for_spectroscopy_line(name="Hlt2BandQ_BcForSpectroscopyFull",
                             prescale=1,
                             persistreco=True):
    line_alg = b_for_spectroscopy.make_Bc(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Xibm_for_spectroscopy_line(name="Hlt2BandQ_XibmForSpectroscopyFull",
                               prescale=1,
                               persistreco=True):
    line_alg = b_for_spectroscopy.make_Xibm(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Xib0_for_spectroscopy_line(name="Hlt2BandQ_Xib0ForSpectroscopyFull",
                               prescale=1,
                               persistreco=True):
    line_alg = b_for_spectroscopy.make_Xib0(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Omegab_for_spectroscopy_line(name="Hlt2BandQ_OmegabForSpectroscopyFull",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Omegab(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


##  -------- semileptonic modes


@register_line_builder(full_lines)
@configurable
def Bud_for_spectroscopy_SL_line(name="Hlt2BandQ_BudForSpectroscopySLFull",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Bud_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Bs_for_spectroscopy_SL_line(name="Hlt2BandQ_BsForSpectroscopySLFull",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Bs_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Bc_for_spectroscopy_SL_line(name="Hlt2BandQ_BcForSpectroscopySLFull",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Bc_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Lb_for_spectroscopy_SL_line(name="Hlt2BandQ_LbForSpectroscopySLFull",
                                prescale=1,
                                persistreco=True):
    line_alg = b_for_spectroscopy.make_Lb_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Xib_for_spectroscopy_SL_line(name="Hlt2BandQ_XibForSpectroscopySLFull",
                                 prescale=1,
                                 persistreco=True):
    line_alg = b_for_spectroscopy.make_Xib_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(full_lines)
@configurable
def Omegab_for_spectroscopy_SL_line(
        name="Hlt2BandQ_OmegabForSpectroscopySLFull", prescale=1,
        persistreco=True):
    line_alg = b_for_spectroscopy.make_Omegab_SL(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Lb2JpsiLambdaTTLine(name='Hlt2BandQ_Lb2JpsiLambdaTT', prescale=1.):
    return Hlt2Line(
        # Name must start with either Hlt1 or Hlt2
        name=name,
        algs=make_prefilters() +
        [make_detached_jpsi(), make_Lb2JpsiLambdaTT()],
        prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def Bd2JpsiKsTTLine(name='Hlt2BandQ_Bd2JpsiKsTT', prescale=1.):
    return Hlt2Line(
        # Name must start with either Hlt1 or Hlt2
        name=name,
        algs=make_prefilters() + [make_detached_jpsi(),
                                  make_Bd2JpsiKsTT()],
        prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def XibToJpsiPKsLLK_JpsiToMuMu_line(
        name='Hlt2BandQ_XibToJpsiPKsLLK_JpsiToMuMu', prescale=1):
    """Xib0 --> Jpsi(-> mu+ mu-)  p+ KS0LL K- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKsLLK_JpsiToMuMu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def XibToJpsiPKsDDK_JpsiToMuMu_line(
        name='Hlt2BandQ_XibToJpsiPKsDDK_JpsiToMuMu', prescale=1):
    """Xib0 --> Jpsi(-> mu+ mu-)  p+ KS0DD K- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKsDDK_JpsiToMuMu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def XibToJpsiPKPi_JpsiToMuMu_line(name='Hlt2BandQ_XibToJpsiPKPi_JpsiToMuMu',
                                  prescale=1):
    """Xib- --> Jpsi(-> mu+ mu-)  p+ K- pi- line"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiPKPi_JpsiToMuMu()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def LbToLcDsmPiPi_line(name='Hlt2BandQ_LbToLcDsmPiPi', prescale=1):
    """Lb0 --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) pi+ pi- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_LbToLcDsmPiPi()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def LbToLcDsmKK_line(name='Hlt2BandQ_LbToLcDsmKK', prescale=1):
    """Lb0 --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) K+ K- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_LbToLcDsmKK()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def XibToLcDsmK_line(name='Hlt2BandQ_XibToLcDsmK', prescale=1):
    """Xib- --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) K- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_XibToLcDsmK()
    return Hlt2Line(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_LLLL_line(name='Hlt2BandQ_Jpsi2LmdLmd_LLLL',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(LL) Lambda(LL)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_llll()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdall(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_LLDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_LLDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(LL) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_lldd()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdall(),
                                  make_lambdadd(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_DDDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_DDDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(DD) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_dddd()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdadd(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_TTLL_line(name='Hlt2BandQ_Jpsi2LmdLmd_TTLL',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(RK-TT) Lambda(LL)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_ttll()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdall(),
                                  make_lambdatt(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_TTDD_line(name='Hlt2BandQ_Jpsi2LmdLmd_TTDD',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(RK-TT) Lambda(DD)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_ttdd()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdadd(),
                                  make_lambdatt(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Jpsi2LmdLmd_TTTT_line(name='Hlt2BandQ_Jpsi2LmdLmd_TTTT',
                          prescale=1,
                          persistreco=False):
    """Jpsi -> Lambda(RK-TT) Lambda(RK-TT)"""
    line_alg = jpsi_to_lmdlmd_lines.make_jpsi_to_lmdlmd_tttt()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [make_lambdatt(), line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Bp2JpsiKp_NoMuonID_line(name='Hlt2BandQ_Bp2JpsiKp_NoMuonID',
                            prescale=1,
                            persistreco=False):
    """B+->Jpsi K+ without Muon-based PID cuts."""
    line_alg = b_to_jpsiX_NoMuonID.make_Bp2JpsiKp_NoMuonID()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Bz2JpsiKpPim_NoMuonID_line(name='Hlt2BandQ_Bz2JpsiKpPim_NoMuonID',
                               prescale=1,
                               persistreco=False):
    """B0->Jpsi K+ pi- without Muon-based PID cuts. Cover the full KPi mass spectrum"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bz2JpsiKpPim_NoMuonID()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


#This is a back-up choice if the rate of Bz2JpsiKpPim_NoMuonID_line is found to be not acceptable during rate tuning in Feb.
#@register_line_builder(turbo_lines)
#@configurable
#def Bz2JpsiKst_NoMuonID_line(name='Hlt2BandQ_Bz2JpsiKst_NoMuonID',
#                             prescale=1,
#                             persistreco=False):
#    """B0->Jpsi K*0 without Muon-based PID cuts. Focus on the K*(892) region"""
#    line_alg = b_to_jpsiX_NoMuonID.make_Bz2JpsiKst_NoMuonID()
#    return Hlt2Line(
#        name=name,
#        algs=make_prefilters() + [line_alg],
#        prescale=prescale,
#        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def Bs2JpsiKpKm_NoMuonID_line(name='Hlt2BandQ_Bs2JpsiKpKm_NoMuonID',
                              prescale=1,
                              persistreco=False):
    """Bs0->Jpsi K+ K- without Muon-based PID cuts. Cover the full KPi mass spectrum"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bs2JpsiKpKm_NoMuonID()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)


#This is a back-up choice if the rate of Bs2JpsiKpKm_NoMuonID_line is found to be not acceptable during rate tuning in Feb.
#@register_line_builder(turbo_lines)
#@configurable
#def Bs2JpsiPhi_NoMuonID_line(name='Hlt2BandQ_Bs2JpsiPhi_NoMuonID',
#                             prescale=1,
#                             persistreco=False):
#    """B0->Jpsi K*0 without Muon-based PID cuts. Focus on the phi(1020) region"""
#    line_alg = b_to_jpsiX_NoMuonID.make_Bs2JpsiPhi_NoMuonID()
#    return Hlt2Line(
#        name=name,
#        algs=make_prefilters() + [line_alg],
#        prescale=prescale,
#        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcPi_Lc2LambdaPi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcPi_Lc2LambdaPi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcPi, Lc->LambdaPi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcPi_Lc2LambdaPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DPi_D2KSPi_TT_line(name='Hlt2BandQ_EMDM_B2DPi_D2KSPi_TT',
                              prescale=0.4,
                              persistreco=False):
    """B->DPi, D->KSPi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DPi_D2KSPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcPi_Lc2Lambda3Pi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcPi_Lc2Lambda3Pi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcPi, Lc->Lambda3Pi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcPi_Lc2Lambda3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DPi_D2KS3Pi_TT_line(name='Hlt2BandQ_EMDM_B2DPi_D2KS3Pi_TT',
                               prescale=1,
                               persistreco=False):
    """B->DPi, D->KS3Pi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DPi_D2KS3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2Lc3Pi_Lc2LambdaPi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2Lc3Pi_Lc2LambdaPi_TT',
        prescale=1,
        persistreco=False):
    """Lb->Lc3Pi, Lc->LambdaPi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2Lc3Pi_Lc2LambdaPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2D3Pi_D2KSPi_TT_line(name='Hlt2BandQ_EMDM_B2D3Pi_D2KSPi_TT',
                               prescale=0.05,
                               persistreco=False):
    """B->D3Pi, D->KSPi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2D3Pi_D2KSPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2Lc3Pi_Lc2Lambda3Pi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2Lc3Pi_Lc2Lambda3Pi_TT',
        prescale=1,
        persistreco=False):
    """Lb->Lc3Pi, Lc->Lambda3Pi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2Lc3Pi_Lc2Lambda3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2D3Pi_D2KS3Pi_TT_line(name='Hlt2BandQ_EMDM_B2D3Pi_D2KS3Pi_TT',
                                prescale=1,
                                persistreco=False):
    """B->D3Pi, D->KS3Pi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2D3Pi_D2KS3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromBeauty(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcDs_Lc2LambdaPi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcDs_Lc2LambdaPi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcDs, Lc->LambdaPi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcDs_Lc2LambdaPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            make_dsplus_to_kpkmpip(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DDs_D2KSPi_TT_line(name='Hlt2BandQ_EMDM_B2DDs_D2KSPi_TT',
                              prescale=0.9,
                              persistreco=False):
    """B->DDs, D->KSPi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DDs_D2KSPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            make_dsplus_to_kpkmpip(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcDs_Lc2Lambda3Pi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcDs_Lc2Lambda3Pi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcDs, Lc->Lambda3Pi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcDs_Lc2Lambda3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            make_dsplus_to_kpkmpip(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DDs_D2KS3Pi_TT_line(name='Hlt2BandQ_EMDM_B2DDs_D2KS3Pi_TT',
                               prescale=1,
                               persistreco=False):
    """B->DDs, D->KS3Pi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DDs_D2KS3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            make_dsplus_to_kpkmpip(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcPiPiMuNu_Lc2LambdaPi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcPiPiMuNu_Lc2LambdaPi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcPiPiMuNu, Lc->LambdaPi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcPiPiMuNu_Lc2LambdaPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPimMu(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DPiPiMuNu_D2KSPi_TT_line(
        name='Hlt2BandQ_EMDM_B2DPiPiMuNu_D2KSPi_TT',
        prescale=0.7,
        persistreco=False):
    """B->DPiPiMuNu, D->KSPi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DPiPiMuNu_D2KSPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPimMu(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcPiPiMuNu_Lc2Lambda3Pi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcPiPiMuNu_Lc2Lambda3Pi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcPiPiMuNu, Lc->Lambda3Pi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcPiPiMuNu_Lc2Lambda3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPimMu(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DPiPiMuNu_D2KS3Pi_TT_line(
        name='Hlt2BandQ_EMDM_B2DPiPiMuNu_D2KS3Pi_TT',
        prescale=1,
        persistreco=False):
    """B->DPiPiMuNu, D->KS3Pi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DPiPiMuNu_D2KS3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_PipPimMu(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcMuNu_Lc2LambdaPi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcMuNu_Lc2LambdaPi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcMuNu, Lc->LambdaPi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcMuNu_Lc2LambdaPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachMuon(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DMuNu_D2KSPi_TT_line(name='Hlt2BandQ_EMDM_B2DMuNu_D2KSPi_TT',
                                prescale=0.7,
                                persistreco=False):
    """B->DMuNu, D->KSPi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DMuNu_D2KSPi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachMuon(),
            b_to_LcX_TT_lines.make_EMDM_detached_bachPion_fromCharm(), line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_Lb2LcMuNu_Lc2Lambda3Pi_TT_line(
        name='Hlt2BandQ_EMDM_Lb2LcMuNu_Lc2Lambda3Pi_TT',
        prescale=1,
        persistreco=False):
    """Lb->LcMuNu, Lc->Lambda3Pi, Lambda->PPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_Lb2LcMuNu_Lc2Lambda3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachMuon(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def EMDM_B2DMuNu_D2KS3Pi_TT_line(name='Hlt2BandQ_EMDM_B2DMuNu_D2KS3Pi_TT',
                                 prescale=1,
                                 persistreco=False):
    """B->DMuNu, D->KS3Pi, KS->PiPi (Ttracks)"""
    line_alg = b_to_LcX_TT_lines.make_EMDM_B2DMuNu_D2KS3Pi_TT()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [
            b_to_LcX_TT_lines.make_EMDM_detached_bachMuon(),
            b_to_LcX_TT_lines.make_EMDM_detached_PipPipPim_fromCharm(),
            line_alg
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(turbo_lines)
@configurable
def BuToDspPPbar_line(name='Hlt2BandQ_BuToDspPPbar',
                      prescale=1,
                      persistreco=False):
    line_alg = b_for_spectroscopy.make_BuToDspPPbar(process=PROCESS)
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=persistreco)

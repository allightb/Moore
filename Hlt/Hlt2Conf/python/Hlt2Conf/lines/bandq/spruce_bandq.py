###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Booking of B&Q sprucing lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

"""
from Moore.config import SpruceLine, register_line_builder

from PyConf import configurable
from Hlt2Conf.lines.bandq.builders import b_to_jpsiX_lines, b_to_etacX_lines, xibc_lines, Bc_lines, qqbar_to_hadrons, dimuon_sprucing_lines
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters
from Hlt2Conf.lines.bandq.builders.helper import psis_list
from Hlt2Conf.lines.bandq.builders import bx
from Hlt2Conf.lines.bandq.builders import doublecharm
from Hlt2Conf.lines.bandq.builders import b_for_spectroscopy
from Hlt2Conf.lines.bandq.builders import dimuon_lines

PROCESS = 'spruce'
sprucing_lines = {}

#################################
# dimuon sprucings, tighter cut than hlt2 lines
#################################


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuDetached_sprucing_line(name='SpruceBandQ_JpsiToMuMuDetached',
                                     prescale=1):
    """Detached Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuDetached_sprucing_line(name='SpruceBandQ_Psi2SToMuMuDetached',
                                      prescale=1):
    """Detached psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuTightPrompt_sprucing_line(
        name='SpruceBandQ_JpsiToMuMuTightPrompt', prescale=1):
    """Prompt Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuTightPrompt_sprucing_line(
        name='SpruceBandQ_Psi2SToMuMuTightPrompt', prescale=1):
    """Prompt psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True)


#######################
#Move persistReco=True Turbo dimuon lines into full stream + sprucing, to avoid overlap with other streams
######################
@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignHighMass_sprucing_line(
        name='SpruceBandQ_DiMuonSameSignHighMass', prescale=1):
    line_alg = dimuon_lines.make_HighMass_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignHighMassFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def DiMuonInc_sprucing_line(name='SpruceBandQ_DiMuonInc', prescale=1):
    line_alg = dimuon_lines.make_loose_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonIncFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignInc_sprucing_line(name='SpruceBandQ_DiMuonSameSignInc',
                                    prescale=1):
    line_alg = dimuon_lines.make_loose_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignIncFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def DiMuonIncHighPT_sprucing_line(name='SpruceBandQ_DiMuonIncHighPT',
                                  prescale=1):
    line_alg = dimuon_lines.make_tight_highpt_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonIncHighPTFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignIncHighPT_sprucing_line(
        name='SpruceBandQ_DiMuonSameSignIncHighPT', prescale=1):
    line_alg = dimuon_lines.make_tight_highpt_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignIncHighPTFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSoft_sprucing_line(name='SpruceBandQ_DiMuonSoft', prescale=1):
    line_alg = dimuon_lines.make_soft_detached_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSoftFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def UpsilonToMuMu_sprucing_line(name='SpruceBandQ_DiMuonUpsilon', prescale=1):
    line_alg = dimuon_lines.make_upsilon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonUpsilonFullDecision"])


#################################
# Book b->JpsiX sprucing lines  #
#################################

hlt2_lines_detached_jpsi = ['Hlt2_JpsiToMuMuDetachedFullDecision']
hlt2_lines_detached_psi2s = ['Hlt2_Psi2SToMuMuDetachedFullDecision']

hlt2_lines_detached_psi = [
    'Hlt2_JpsiToMuMuDetachedFullDecision',
    'Hlt2_Psi2SToMuMuDetachedFullDecision'
]


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_sprucing_line(
        name='SpruceBandQ_BpToJpsiKp_JpsiToMuMu', prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiKp_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


###################################################
# Book b -> ccbar X, ccbar->hadron sprucing lines #
###################################################
@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToHHHH', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToHHHH', prescale=0.8):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToHHHH_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToHHHH', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToHHHH()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToPpPm', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToPpPm', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPpPm_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToPpPm', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPpPm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToKpKm', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToKpKm', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToKpKm_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToKpKm', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToKpKm()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_LbToEtacPpKm_EtacToPipPim', prescale=1):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_BdToEtacKpPim_EtacToPipPim', prescale=1):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPipPim_sprucing_line(
        name='SpruceBandQ_BuToEtacKpPhi_EtacToPipPim', prescale=1):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPipPim()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# Book detached ccbar->ppbar sprucing line #
############################################
@register_line_builder(sprucing_lines)
@configurable
def ccbarToPpPmDetached_sprucing_line(name='SpruceBandQ_ccbarToPpPmDetached',
                                      prescale=1):
    """ccbar->ppbar detached line"""
    line_alg = qqbar_to_hadrons.make_detached_ccbarToPpPm_spruce()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


############################################
# Book Xibc/Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLc_sprucing_line(name='SpruceBandQ_XibcToJpsiLc', prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLc()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiXicz_sprucing_line(name='SpruceBandQ_XibcToJpsiXicz',
                                 prescale=1):
    """Xi_bc+ -> J/psi(1S) Xi_c0 line"""
    line_alg = xibc_lines.make_XibcToJpsiXicz()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKm_sprucing_line(name='SpruceBandQ_XibcToJpsiLcKm',
                                 prescale=1):
    """Xi_bc0 -> J/psi(1S) Lambda_c+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKmPip_sprucing_line(name='SpruceBandQ_XibcToJpsiLcKmPip',
                                    prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKm_sprucing_line(name='SpruceBandQ_XibcToJpsiDzPKm',
                                  prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpPKm_sprucing_line(name='SpruceBandQ_XibcToJpsiDpPKm',
                                  prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDpPKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKmPip_sprucing_line(name='SpruceBandQ_XibcToJpsiDzPKmPip',
                                     prescale=1):
    """Xi_bc+ -> J/psi(1S) D0 p+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzLam_sprucing_line(name='SpruceBandQ_XibcToJpsiDzLam',
                                  prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDzLam()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpLam_sprucing_line(name='SpruceBandQ_XibcToJpsiDpLam',
                                  prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDpLam()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


############################################
# Book Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDz_sprucing_line(name='SpruceBandQ_TbcToJpsiDz', prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDz()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDpKm_sprucing_line(name='SpruceBandQ_TbcToJpsiDpKm', prescale=1):
    """Xi_bc0 -> J/psi(1S) D+ K- line"""
    line_alg = xibc_lines.make_TbcToJpsiDpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKmPip_sprucing_line(name='SpruceBandQ_TbcToJpsiDzKmPip',
                                   prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 K- pi+ line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKs_sprucing_line(name='SpruceBandQ_TbcToJpsiDzKs', prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 KS0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKs()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


############################################
# Book Bc sprucing lines
############################################


### Bc -> Jpsi X
@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPip_sprucing_line(name='SpruceBandQ_BcToJpsiPip', prescale=1):
    """B_c+ -> J/psi(1S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPip_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPipPipPim_sprucing_line(name='SpruceBandQ_BcToJpsiPipPipPim',
                                    prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPipPipPim_JpsiToMuMu(
        process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsi5Pi_sprucing_line(name='SpruceBandQ_BcToJpsi5Pi', prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi+ pi- pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsi5Pi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi)


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPip_sprucing_line(name='SpruceBandQ_BcToPsi2SPip', prescale=1):
    """B_c+ -> psi(2S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_psi2s)


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPipPipPim_sprucing_line(name='SpruceBandQ_BcToPsi2SPipPipPim',
                                     prescale=1):
    """B_c+ -> psi(2S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPipPipPim_Psi2SToMuMu(
        process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_psi2s)


### Bc+ -> hhh


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmKp_sprucing_line(name='SpruceBandQ_BcToPpPmKp', prescale=1):
    """B_c+ -> p+ p~- K+ line"""
    line_alg = Bc_lines.make_BcToPpPmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmPip_sprucing_line(name='SpruceBandQ_BcToPpPmPip', prescale=1):
    """B_c+ -> p+ p~- pi+ line"""
    line_alg = Bc_lines.make_BcToPpPmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPimPip_sprucing_line(name='SpruceBandQ_BcToPipPimPip', prescale=1):
    """B_c+ -> pi+ pi- pi+ line"""
    line_alg = Bc_lines.make_BcToPipPimPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKmKp_sprucing_line(name='SpruceBandQ_BcToKpKmKp', prescale=1):
    """B_c+ -> K+ K- K+ line"""
    line_alg = Bc_lines.make_BcToKpKmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPimKp_sprucing_line(name='SpruceBandQ_BcToPipPimKp', prescale=1):
    """B_c+ -> pi+ pi- K+ line"""
    line_alg = Bc_lines.make_BcToPipPimKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKmPip_sprucing_line(name='SpruceBandQ_BcToKpKmPip', prescale=1):
    """B_c+ -> K+ K- pi+ line"""
    line_alg = Bc_lines.make_BcToKpKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmKp_sprucing_line(name='SpruceBandQ_BuToPpPmKp', prescale=1):
    """B_c+ -> p+ p~- K+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmPip_sprucing_line(name='SpruceBandQ_BuToPpPmPip', prescale=1):
    """B_c+ -> p+ p~- pi+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPimPip_sprucing_line(name='SpruceBandQ_BuToPipPimPip', prescale=1):
    """B_c+ -> pi+ pi- pi+ control channel"""
    line_alg = Bc_lines.make_BuToPipPimPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKmKp_sprucing_line(name='SpruceBandQ_BuToKpKmKp', prescale=1):
    """B_c+ -> K+ K- K+ control channel"""
    line_alg = Bc_lines.make_BuToKpKmKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPimKp_sprucing_line(name='SpruceBandQ_BuToPipPimKp', prescale=1):
    """B_c+ -> pi+ pi- K+ control channel"""
    line_alg = Bc_lines.make_BuToPipPimKp()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKmPip_sprucing_line(name='SpruceBandQ_BuToKpKmPip', prescale=1):
    """B_c+ -> K+ K- pi+ control channel"""
    line_alg = Bc_lines.make_BuToKpKmPip()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


######################################################
#comment out lines related to neutral particles and downstream tracks for now
######################################################


@register_line_builder(sprucing_lines)
@configurable
def XibToJpsiXi_sprucing_line(name='SpruceBandQ_XibToJpsiXi', prescale=1):
    """Xi_b- -> Xi- J/psi(1S)"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiXi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibToJpsiXiPi_sprucing_line(name='SpruceBandQ_XibToJpsiXiPi', prescale=1):
    """Xi_b0 -> Xi- J/psi(1S) pi+"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiXiPi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def OmegabToJpsiOmega_sprucing_line(name='SpruceBandQ_OmegabToJpsiOmega',
                                    prescale=1):
    """Omega_b- -> Omega- J/psi(1S)"""
    line_alg = b_to_jpsiX_lines.make_OmegabToJpsiOmega_JpsiToMuMu(
        process=PROCESS)
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToDs1Gamma_sprucing_line(name='SpruceBandQ_BcToDs1Gamma', prescale=1):
    """B_c+ -> D_s1(2536)+ gamma"""
    line_alg = Bc_lines.make_BcToDs1Gamma()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToDs1GammaWS_sprucing_line(name='SpruceBandQ_BcToDs1GammaWS',
                                 prescale=0.3):
    """B_c+ -> D_s1(2536)+ gamma wrong sign D_s1 decay"""
    line_alg = Bc_lines.make_BcToDs1GammaWS()
    return SpruceLine(
        name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


####################################################
## automatic creation of Hb -> psi + hadrons lines
###################################################


def make_spruce_lines(make_fun,
                      line_dict=sprucing_lines,
                      lines_to_add=None,
                      hlt2_filter_lines=""):

    if not lines_to_add: return
    for decay in lines_to_add:

        descriptor = lines_to_add[decay]  # or actually list of descriptors

        @register_line_builder(line_dict)
        def make_spruce_line(
                name='SpruceBandQ_%s' % decay,
                maker_name='make_%s' % decay,
                descriptor=descriptor,  # or actually list of descriptors
                make_fun=make_fun,
                prescale=1):

            try:
                line_alg = make_fun(name=maker_name, descriptor=descriptor)
            except TypeError:
                line_alg = make_fun(name=maker_name, descriptors=descriptor)

            return SpruceLine(
                name=name,
                hlt2_filter_code=hlt2_filter_lines,
                algs=make_prefilters() + [line_alg],
                prescale=prescale)


auto_PsiX_lines = {}

for psi in psis_list:
    psi_name = psis_list[psi][0]

    # ------- psiH
    auto_PsiX_lines[f"{psi_name}Pi"] = [f"[B+ -> {psi} pi+]cc"]
    auto_PsiX_lines[f"{psi_name}K"] = [f"[B+ -> {psi} K+]cc"]
    auto_PsiX_lines[f"{psi_name}P"] = [f"[B+ -> {psi} p+]cc"]
    auto_PsiX_lines[f"{psi_name}Ks"] = [f"[B0 -> {psi} KS0]cc"]
    auto_PsiX_lines[f"{psi_name}Lambda"] = [f"[Lambda_b0 -> {psi} Lambda0]cc"]
    auto_PsiX_lines[f"{psi_name}Xi"] = [f"[Xi_b- -> {psi} Xi-]cc"]
    auto_PsiX_lines[f"{psi_name}Omega"] = [f"[Omega_b- -> {psi} Omega-]cc"]

    # ------- psiHH
    auto_PsiX_lines[f"{psi_name}PiPi"] = [
        f"[B_s0 -> {psi} pi+ pi-]cc", f"[B_s0 -> {psi} pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KPi"] = [
        f"[B0 -> {psi} K+ pi-]cc", f"[B0 -> {psi} K+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KK"] = [
        f"[B_s0 -> {psi} K+ K-]cc", f"[B_s0 -> {psi} K+ K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PPi"] = [
        f"[Lambda_b0 -> {psi} p+ pi-]cc", f"[Lambda_b0 -> {psi} p+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PK"] = [
        f"[Lambda_b0 -> {psi} p+ K-]cc", f"[Lambda_b0 -> {psi} p+ K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PP"] = [
        f"[B_s0 -> {psi} p+ p~-]cc", f"[B_s0 -> {psi} p+ p+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KsPi"] = [f"[B+ -> {psi} KS0 pi+]cc"]
    auto_PsiX_lines[f"{psi_name}KsK"] = [f"[B+ -> {psi} KS0 K+]cc"]
    auto_PsiX_lines[f"{psi_name}PKs"] = [f"[B+ -> {psi} p+ KS0]cc"]
    auto_PsiX_lines[f"{psi_name}KsKs"] = [f"[B0 -> {psi} KS0 KS0]cc"]
    auto_PsiX_lines[f"{psi_name}LambdaPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 pi+]cc",
        f"[Lambda_b0 -> {psi} Lambda0 pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaK"] = [
        f"[Xi_b- -> {psi} Lambda0 K-]cc", f"[Xi_b- -> {psi} Lambda0 K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaP"] = [
        f"[B+ -> {psi} Lambda0 p+]cc", f"[B+ -> {psi} Lambda0 p~-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKs"] = [
        f"[Lambda_b0 -> {psi} Lambda0 KS0]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaLambda"] = [
        f"[Lambda_b0 -> {psi} Lambda0 Lambda0]cc",
        f"[Lambda_b0 -> {psi} Lambda0 Lambda~0]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiPi"] = [
        f"[Xi_b0 -> {psi} Xi- pi+]cc", f"[Xi_b0 -> {psi} Xi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiK"] = [
        f"[Xi_b0 -> {psi} Xi- K+]cc", f"[Xi_b0 -> {psi} Xi- K-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiP"] = [
        f"[Xi_b0 -> {psi} Xi- p+]cc", f"[Xi_b0 -> {psi} Xi- p~-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaPi"] = [
        f"[Omega_b- -> {psi} Omega- pi+]cc",
        f"[Omega_b- -> {psi} Omega- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaK"] = [
        f"[Omega_b- -> {psi} Omega- K+]cc", f"[Omega_b- -> {psi} Omega- K-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaP"] = [
        f"[Omega_b- -> {psi} Omega- p+]cc", f"[Omega_b- -> {psi} Omega- p~-]cc"
    ]
    #

    # ------- psiHHH
    auto_PsiX_lines[f"{psi_name}PiPiPi"] = [
        f"[B+ -> {psi} pi+ pi+ pi-]cc", f"[B+ -> {psi} pi+ pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KPiPi"] = [
        f"[B+ -> {psi} K+ pi+ pi-]cc", f"[B+ -> {psi} K+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKPi"] = [
        f"[B+ -> {psi} K+ K- pi+]cc", f"[B+ -> {psi} K+ K+ pi+]cc",
        f"[B+ -> {psi} K+ K+ pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKK"] = [
        f"[B+ -> {psi} K+ K+ K-]cc", f"[B+ -> {psi} K+ K+ K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PKK"] = [
        f"[Xi_b- -> {psi} p+ K+ K-]cc", f"[Xi_b- -> {psi} p+ K+ K+]cc",
        f"[Xi_b- -> {psi} p+ K- K-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PPiPi"] = [
        f"[Xi_b- -> {psi} p+ pi+ pi-]cc", f"[Xi_b- -> {psi} p+ pi+ pi+]cc",
        f"[Xi_b- -> {psi} p+ pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PKPi"] = [
        f"[Xi_b- -> {psi} p+ K+ pi-]cc", f"[Xi_b- -> {psi} p+ K+ pi+]cc",
        f"[Xi_b- -> {psi} p+ K- pi-]cc", f"[Xi_b- -> {psi} p+ K- pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}PPPi"] = [
        f"[Xi_bc+ -> {psi} p+ p+ pi-]cc", f"[Xi_bc+ -> {psi} p+ p+ pi+]cc",
        f"[Xi_bc+ -> {psi} p+ p~- pi+]cc"
    ]  # for Hexa_b
    auto_PsiX_lines[f"{psi_name}PPP"] = [
        f"[Xi_bc+ -> {psi} p+ p+ p~-]cc", f"[Xi_bc+ -> {psi} p+ p+ p+]cc"
    ]  # for Hexa_b
    auto_PsiX_lines[f"{psi_name}PPK"] = [
        f"[Xi_bc+ -> {psi} p+ p+ K-]cc", f"[Xi_bc+ -> {psi} p+ p+ K+]cc",
        f"[Xi_bc+ -> {psi} p+ p~- K+]cc"
    ]  # for Hexa_b

    # with long-lived particles
    auto_PsiX_lines[f"{psi_name}KsKK"] = [
        f"[B0 -> {psi} KS0 K+ K-]cc", f"[B0 -> {psi} KS0 K+ K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KsKPi"] = [
        f"[B0 -> {psi} KS0 K+ pi-]cc", f"[B0 -> {psi} KS0 K+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KsPiPi"] = [
        f"[B0 -> {psi} KS0 pi+ pi-]cc", f"[B0 -> {psi} KS0 pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKK"] = [
        f"[Lambda_b0 -> {psi} Lambda0 K+ K-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 K+ K+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 K+ pi-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 K+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}LambdaPiPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 pi+ pi-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiPiPi"] = [
        f"[Xi_b- -> {psi} Xi- pi+ pi-]cc", f"[Xi_b- -> {psi} Xi- pi+ pi+]cc",
        f"[Xi_b- -> {psi} Xi- pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiKPi"] = [
        f"[Xi_b- -> {psi} Xi- K+ pi-]cc", f"[Xi_b- -> {psi} Xi- K+ pi+]cc",
        f"[Xi_b- -> {psi} Xi- K- pi+]cc", f"[Xi_b- -> {psi} Xi- K- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}XiKK"] = [
        f"[Xi_b- -> {psi} Xi- K+ K-]cc", f"[Xi_b- -> {psi} Xi- K+ K+]cc",
        f"[Xi_b- -> {psi} Xi- K- K-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaPiPi"] = [
        f"[Omega_b- -> {psi} Omega- pi+ pi-]cc",
        f"[Omega_b- -> {psi} Omega- pi+ pi+]cc",
        f"[Omega_b- -> {psi} Omega- pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaKPi"] = [
        f"[Omega_b- -> {psi} Omega- K+ pi-]cc",
        f"[Omega_b- -> {psi} Omega- K+ pi+]cc",
        f"[Omega_b- -> {psi} Omega- K- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}OmegaKK"] = [
        f"[Omega_b- -> {psi} Omega- K+ K-]cc",
        f"[Omega_b- -> {psi} Omega- K+ K+]cc",
        f"[Omega_b- -> {psi} Omega- K- K-]cc"
    ]

    #

    # psiHHHH
    auto_PsiX_lines[f"{psi_name}PiPiPiPi"] = [
        f"[B0 -> {psi} pi+ pi+ pi- pi-]cc", f"[B0 -> {psi} pi+ pi+ pi+ pi-]cc",
        f"[B0 -> {psi} pi+ pi+ pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KPiPiPi"] = [
        f"[B0 -> {psi} K+ pi+ pi- pi-]cc", f"[B0 -> {psi} K+ pi+ pi+ pi-]cc",
        f"[B0 -> {psi} K+ pi- pi- pi-]cc", f"[B0 -> {psi} K+ pi+ pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKPiPi"] = [
        f"[B0 -> {psi} K+ K- pi+ pi-]cc", f"[B0 -> {psi} K+ K- pi+ pi+]cc",
        f"[B0 -> {psi} K+ K- pi- pi-]cc", f"[B0 -> {psi} K+ K+ pi+ pi-]cc",
        f"[B0 -> {psi} K+ K+ pi+ pi+]cc", f"[B0 -> {psi} K+ K+ pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKKPi"] = [
        f"[B0 -> {psi} K+ K+ K- pi-]cc", f"[B0 -> {psi} K+ K+ K- pi+]cc",
        f"[B0 -> {psi} K+ K+ K+ pi-]cc", f"[B0 -> {psi} K+ K+ K+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKKK"] = [
        f"[B0 -> {psi} K+ K+ K- K-]cc", f"[B0 -> {psi} K+ K+ K+ K-]cc",
        f"[B0 -> {psi} K+ K+ K+ K+]cc"
    ]

    auto_PsiX_lines[f"{psi_name}PPKPi"] = [
        f"[Xi_bc0 -> {psi} p+ p+ K- pi-]cc",  # for Hexa_b
        f"[Xi_bc0 -> {psi} p+ p+ K+ pi-]cc",
        f"[Xi_bc0 -> {psi} p+ p+ K- pi+]cc",
        f"[Xi_bc0 -> {psi} p+ p~- K- pi-]cc",
        f"[Xi_bc0 -> {psi} p+ p~- K- pi+]cc"
    ]

    # psiHHHHH
    auto_PsiX_lines[f"{psi_name}PiPiPiPiPi"] = [
        f"[B+ -> {psi} pi+ pi+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} pi+ pi+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} pi+ pi+ pi+ pi+ pi+]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KPiPiPiPi"] = [
        f"[B+ -> {psi} K+ pi+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ pi+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ pi+ pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ pi+ pi- pi- pi-]cc",
        f"[B+ -> {psi} K+ pi- pi- pi- pi-]cc"
    ]
    auto_PsiX_lines[f"{psi_name}KKPiPiPi"] = [
        f"[B+ -> {psi} K+ K- pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ K- pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ K- pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ K- pi- pi- pi-]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ K+ pi- pi- pi-]cc"
    ]

make_spruce_lines(
    make_fun=b_to_jpsiX_lines.make_HbToPsiX_PsiToMuMu_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_PsiX_lines,
    hlt2_filter_lines=hlt2_lines_detached_psi,
)

####################################################
## automatic creation of prompt X->B+hadrons lines
###################################################

BX_mother_id = "Upsilon(4S)"

auto_BX_lines = {}

auto_BX_lines["BeautyPi"] = [
    f"[ {BX_mother_id} -> B+ pi+]cc",
    f"[ {BX_mother_id} -> B+ pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+]cc",
    f"[ {BX_mother_id} -> B0 pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi-]cc",
    f"[ {BX_mother_id} -> B_c+ pi+]cc",
    f"[ {BX_mother_id} -> B_c+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+]cc",
    f"[ {BX_mother_id} -> Omega_b- pi-]cc",
]
auto_BX_lines["BeautyK"] = [
    f"[ {BX_mother_id} -> B+ K+]cc",
    f"[ {BX_mother_id} -> B+ K-]cc",
    f"[ {BX_mother_id} -> B0 K+]cc",
    f"[ {BX_mother_id} -> B0 K-]cc",
    f"[ {BX_mother_id} -> B_s0 K+]cc",
    f"[ {BX_mother_id} -> B_s0 K-]cc",
    f"[ {BX_mother_id} -> B_c+ K+]cc",
    f"[ {BX_mother_id} -> B_c+ K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K-]cc",
    f"[ {BX_mother_id} -> Xi_b0 K+]cc",
    f"[ {BX_mother_id} -> Xi_b0 K-]cc",
    f"[ {BX_mother_id} -> Xi_b- K+]cc",
    f"[ {BX_mother_id} -> Xi_b- K-]cc",
    f"[ {BX_mother_id} -> Omega_b- K+]cc",
    f"[ {BX_mother_id} -> Omega_b- K-]cc",
]
auto_BX_lines["BeautyP"] = [
    f"[ {BX_mother_id} -> B+ p+]cc",
    f"[ {BX_mother_id} -> B+ p~-]cc",
    f"[ {BX_mother_id} -> B0 p+]cc",
    f"[ {BX_mother_id} -> B0 p~-]cc",
    f"[ {BX_mother_id} -> B_s0 p+]cc",
    f"[ {BX_mother_id} -> B_s0 p~-]cc",
    f"[ {BX_mother_id} -> B_c+ p+]cc",
    f"[ {BX_mother_id} -> B_c+ p~-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p~-]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p~-]cc",
    f"[ {BX_mother_id} -> Xi_b- p+]cc",
    f"[ {BX_mother_id} -> Xi_b- p~-]cc",
    f"[ {BX_mother_id} -> Omega_b- p+]cc",
    f"[ {BX_mother_id} -> Omega_b- p~-]cc",
]
auto_BX_lines["BeautyKs"] = [
    f"[ {BX_mother_id} -> B+ KS0]cc",
    f"[ {BX_mother_id} -> B0 KS0]cc",
    f"[ {BX_mother_id} -> B_s0 KS0]cc",
    f"[ {BX_mother_id} -> B_c+ KS0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 KS0]cc",
    f"[ {BX_mother_id} -> Xi_b0 KS0]cc",
    f"[ {BX_mother_id} -> Xi_b- KS0]cc",
    f"[ {BX_mother_id} -> Omega_b- KS0]cc",
]
auto_BX_lines["BeautyLambda"] = [
    f"[ {BX_mother_id} -> B+ Lambda0]cc",
    f"[ {BX_mother_id} -> B+ Lambda~0]cc",
    f"[ {BX_mother_id} -> B0 Lambda0]cc",
    f"[ {BX_mother_id} -> B0 Lambda~0]cc",
    f"[ {BX_mother_id} -> B_s0 Lambda0]cc",
    f"[ {BX_mother_id} -> B_s0 Lambda~0]cc",
    f"[ {BX_mother_id} -> B_c+ Lambda0]cc",
    f"[ {BX_mother_id} -> B_c+ Lambda~0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Lambda0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Lambda~0]cc",
    f"[ {BX_mother_id} -> Xi_b0 Lambda0]cc",
    f"[ {BX_mother_id} -> Xi_b0 Lambda~0]cc",
    f"[ {BX_mother_id} -> Xi_b- Lambda0]cc",
    f"[ {BX_mother_id} -> Xi_b- Lambda~0]cc",
    f"[ {BX_mother_id} -> Omega_b- Lambda0]cc",
    f"[ {BX_mother_id} -> Omega_b- Lambda~0]cc",
]
auto_BX_lines["BeautyXi"] = [
    f"[ {BX_mother_id} -> B+ Xi-]cc",
    f"[ {BX_mother_id} -> B+ Xi~+]cc",
    f"[ {BX_mother_id} -> B0 Xi-]cc",
    f"[ {BX_mother_id} -> B0 Xi~+]cc",
    f"[ {BX_mother_id} -> B_s0 Xi-]cc",
    f"[ {BX_mother_id} -> B_s0 Xi~+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Xi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Xi~+]cc",
]
auto_BX_lines["BeautyOmega"] = [
    f"[ {BX_mother_id} -> B+ Omega-]cc",
    f"[ {BX_mother_id} -> B+ Omega~+]cc",
    f"[ {BX_mother_id} -> B0 Omega-]cc",
    f"[ {BX_mother_id} -> B0 Omega~+]cc",
    f"[ {BX_mother_id} -> B_s0 Omega-]cc",
    f"[ {BX_mother_id} -> B_s0 Omega~+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Omega-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Omega~+]cc",
]

auto_BX_lines["BeautyPiPi"] = [
    f"[ {BX_mother_id} -> B+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> B0 pi- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi- pi-]cc",
    f"[ {BX_mother_id} -> B_c+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_c+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_c- pi- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+ pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- pi- pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+ pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+ pi+]cc",
    f"[ {BX_mother_id} -> Omega_b- pi- pi-]cc",
]
auto_BX_lines["BeautyKK"] = [
    f"[ {BX_mother_id} -> B+ K+ K-]cc",
    f"[ {BX_mother_id} -> B+ K+ K+]cc",
    f"[ {BX_mother_id} -> B+ K- K-]cc",
    f"[ {BX_mother_id} -> B+ K- K+]cc",
    f"[ {BX_mother_id} -> B0 K+ K-]cc",
    f"[ {BX_mother_id} -> B0 K+ K+]cc",
    f"[ {BX_mother_id} -> B0 K- K-]cc",
    f"[ {BX_mother_id} -> B0 K- K+]cc",
    f"[ {BX_mother_id} -> B_s0 K+ K-]cc",
    f"[ {BX_mother_id} -> B_s0 K+ K+]cc",
    f"[ {BX_mother_id} -> B_s0 K- K-]cc",
    f"[ {BX_mother_id} -> B_s0 K- K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- K+]cc",
]
auto_BX_lines["BeautyKPi"] = [
    f"[ {BX_mother_id} -> B+ K- pi+]cc", f"[ {BX_mother_id} -> B+ K+ pi-]cc",
    f"[ {BX_mother_id} -> B+ K- pi-]cc", f"[ {BX_mother_id} -> B+ K+ pi+]cc",
    f"[ {BX_mother_id} -> B0 K- pi+]cc", f"[ {BX_mother_id} -> B0 K+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi-]cc", f"[ {BX_mother_id} -> B0 K+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 K- pi+]cc",
    f"[ {BX_mother_id} -> B_s0 K+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 K- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 K+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ pi+]cc"
]

auto_BX_lines["BeautyPPi"] = [
    f"[ {BX_mother_id} -> B+ p+ pi-]cc",  # for Pb [b~udud]
    f"[ {BX_mother_id} -> B+ p~- pi+]cc",
    f"[ {BX_mother_id} -> B+ p+ pi+]cc",
    f"[ {BX_mother_id} -> B+ p~- pi-]cc",
    f"[ {BX_mother_id} -> B0 p+ pi-]cc",
    f"[ {BX_mother_id} -> B0 p~- pi+]cc",
    f"[ {BX_mother_id} -> B0 p+ pi+]cc",
    f"[ {BX_mother_id} -> B0 p~- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 p+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 p~- pi+]cc",
    f"[ {BX_mother_id} -> B_s0 p+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+ pi-]cc",  # for Hb [bqudud]
    f"[ {BX_mother_id} -> Lambda_b0 p~- pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+ pi-]cc",  # for Hbs [bsudud]
    f"[ {BX_mother_id} -> Xi_b0 p~- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- p+ pi-]cc",  # for Hbs [bsudud]
    f"[ {BX_mother_id} -> Xi_b- p~- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- p+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- p~- pi-]cc",
]

## for strongly-decaying Tbc [bcu~d~] or Tbcs [bcs~u~]/[bcs~d~]
auto_BX_lines["B0D0"] = [
    f"[ {BX_mother_id} -> B~0 D0]cc", f"[ {BX_mother_id} -> B~0 D~0]cc"
]
auto_BX_lines["BmDp"] = [
    f"[ {BX_mother_id} -> B- D+]cc", f"[ {BX_mother_id} -> B- D-]cc"
]
auto_BX_lines["B0D0Pi"] = [
    f"[ {BX_mother_id} -> B~0 D0 pi+]cc", f"[ {BX_mother_id} -> B~0 D0 pi-]cc",
    f"[ {BX_mother_id} -> B~0 D~0 pi+]cc",
    f"[ {BX_mother_id} -> B~0 D~0 pi-]cc"
]
auto_BX_lines["BmD0Pi"] = [
    f"[ {BX_mother_id} -> B- D0 pi+]cc", f"[ {BX_mother_id} -> B- D0 pi-]cc",
    f"[ {BX_mother_id} -> B- D~0 pi+]cc", f"[ {BX_mother_id} -> B- D~0 pi-]cc"
]
auto_BX_lines["B0DpPi"] = [
    f"[ {BX_mother_id} -> B~0 D+ pi- ]cc",
    f"[ {BX_mother_id} -> B~0 D- pi+]cc", f"[ {BX_mother_id} -> B~0 D+ pi+]cc",
    f"[ {BX_mother_id} -> B~0 D- pi-]cc"
]

auto_BX_lines["B0Ds"] = [
    f"[ {BX_mother_id} -> B~0 D_s+]cc", f"[ {BX_mother_id} -> B~0 D_s-]cc"
]
auto_BX_lines["BmDs"] = [
    f"[ {BX_mother_id} -> B- D_s+]cc", f"[ {BX_mother_id} -> B- D_s-]cc"
]
auto_BX_lines["BsD0"] = [
    f"[ {BX_mother_id} -> B_s~0 D0]cc", f"[ {BX_mother_id} -> B_s~0 D~0]cc"
]
auto_BX_lines["BsDp"] = [
    f"[ {BX_mother_id} -> B_s~0 D+]cc", f"[ {BX_mother_id} -> B_s~0 D-]cc"
]
auto_BX_lines["BsD0Pi"] = [
    f"[ {BX_mother_id} -> B_s~0 D0 pi+]cc",
    f"[ {BX_mother_id} -> B_s~0 D0 pi-]cc",
    f"[ {BX_mother_id} -> B_s~0 D~0 pi+]cc",
    f"[ {BX_mother_id} -> B_s~0 D~0 pi-]cc"
]

## for strongly-decaying Pb [b~udud] / Pbs [b~suud] / [b~sudd]
# covered by BeautyP, BeautyLambda, BeautyPPi lines

hlt2_lines_b_for_spectroscopy = [
    "Hlt2BandQ_BuForSpectroscopyFullDecision",
    "Hlt2BandQ_BdForSpectroscopyFullDecision",
    "Hlt2BandQ_BsForSpectroscopyFullDecision",
    "Hlt2BandQ_LbForSpectroscopyFullDecision",
    "Hlt2BandQ_BcForSpectroscopyFullDecision",
    "Hlt2BandQ_XibmForSpectroscopyFullDecision",
    "Hlt2BandQ_Xib0ForSpectroscopyFullDecision",
    "Hlt2BandQ_OmegabForSpectroscopyFullDecision"
]

make_spruce_lines(
    make_fun=bx.make_BX_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy,
)

## SemiLeptonic modes for B
auto_BX_SL_lines = {}
for line in auto_BX_lines:
    auto_BX_SL_lines[line + "_SL"] = auto_BX_lines[line]

hlt2_lines_b_for_spectroscopy_SL = [
    "Hlt2BandQ_BudForSpectroscopySLFullDecision",
    "Hlt2BandQ_BsForSpectroscopySLFullDecision",
    "Hlt2BandQ_LbForSpectroscopySLFullDecision",
    "Hlt2BandQ_BcForSpectroscopySLFullDecision",
    "Hlt2BandQ_XibForSpectroscopySLFullDecision",
    "Hlt2BandQ_OmegabForSpectroscopySLFullDecision"
]

make_spruce_lines(
    make_fun=bx.make_BX_SL_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_SL_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy_SL,
)

####################### Detached BX
BX_mother_id = "B_c+"
auto_BX_detached_lines = {}

## Bc decays for study of B* excited states
auto_BX_detached_lines["BcToBpKPi"] = [
    f"[ {BX_mother_id} -> B+ K+ pi-]cc", f"[ {BX_mother_id} -> B+ K- pi+]cc",
    f"[ {BX_mother_id} -> B+ K- pi-]cc", f"[ {BX_mother_id} -> B+ K+ pi+]cc"
]
auto_BX_detached_lines["BcToBpKs"] = [f"[ {BX_mother_id} -> B+ KS0]cc"]
auto_BX_detached_lines["BcToB0KPiPi"] = [
    f"[ {BX_mother_id} -> B0 K+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi+ pi+]cc"
]
auto_BX_detached_lines["BcToB0KsPi"] = [
    f"[ {BX_mother_id} -> B0 KS0 pi+]cc", f"[ {BX_mother_id} -> B0 KS0 pi-]cc"
]
auto_BX_detached_lines["BcToBsPi"] = [
    f"[ {BX_mother_id} -> B_s0 pi+]cc", f"[ {BX_mother_id} -> B_s0 pi-]cc"
]
auto_BX_detached_lines["BcToBsPiPiPi"] = [
    f"[ {BX_mother_id} -> B_s0 pi+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi- pi- pi-]cc"
]

## searches for Xi_bc
BX_mother_id = "Xi_bc0"
auto_BX_detached_lines["XibcToLbPi"] = [
    "[ Xi_bc+ -> Lambda_b0 pi+]cc", "[ Xi_bc+ -> Lambda_b0 pi-]cc"
]
auto_BX_detached_lines["XibcToXibzPi"] = [
    "[ Xi_bc+ -> Xi_b0 pi+]cc", "[ Xi_bc+ -> Xi_b0 pi-]cc"
]
auto_BX_detached_lines["XibcToXibmPi"] = [
    "[ Xi_bc0 -> Xi_b- pi+]cc", "[ Xi_bc0 -> Xi_b- pi-]cc"
]
auto_BX_detached_lines["XibcToLbKPi"] = [
    "[ Xi_bc0 -> Lambda_b0 K- pi+]cc", "[ Xi_bc0 -> Lambda_b0 K+ pi-]cc",
    "[ Xi_bc0 -> Lambda_b0 K- pi-]cc", "[ Xi_bc0 -> Lambda_b0 K+ pi+]cc"
]
auto_BX_detached_lines["XibcToLbKPiPiPi"] = [
    "[ Xi_bc0 -> Lambda_b0 K- pi+ pi+ pi-]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ pi+ pi- pi-]cc"
]
auto_BX_detached_lines["XibcToB0PKPi"] = [
    "[ Xi_bc0 -> B0 p+ K- pi+]cc", "[ Xi_bc0 -> B0 p+ K+ pi-]cc",
    "[ Xi_bc0 -> B0 p~- K- pi+]cc", "[ Xi_bc0 -> B0 p~- K+ pi-]cc"
]
auto_BX_detached_lines["XibcToB0LambdaPi"] = [
    "[ Xi_bc0 -> B0 Lambda0 pi+]cc", "[ Xi_bc0 -> B0 Lambda0 pi-]cc",
    "[ Xi_bc0 -> B0 Lambda~0 pi+]cc", "[ Xi_bc0 -> B0 Lambda~0 pi-]cc"
]
auto_BX_detached_lines["XibcToXibzMu"] = [
    "[ Xi_bc+ -> Xi_b0 mu+]cc", "[ Xi_bc+ -> Xi_b0 mu-]cc"
]
auto_BX_detached_lines["XibcToXibmMu"] = [
    "[ Xi_bc0 -> Xi_b- mu+]cc", "[ Xi_bc0 -> Xi_b- mu-]cc"
]
auto_BX_detached_lines["XibcToLbMu"] = [
    "[ Xi_bc0 -> Lambda_b0 mu+]cc", "[ Xi_bc0 -> Lambda_b0 mu-]cc"
]
auto_BX_detached_lines["XibcToLbKMu"] = [
    "[ Xi_bc0 -> Lambda_b0 K- mu+]cc", "[ Xi_bc0 -> Lambda_b0 K+ mu-]cc",
    "[ Xi_bc0 -> Lambda_b0 K- mu-]cc", "[ Xi_bc0 -> Lambda_b0 K+ mu+]cc"
]

## searches for T_bc
auto_BX_detached_lines["TbcToB0bKPi"] = [
    "[ Xi_bc0 -> B~0 K- pi+]cc", "[ Xi_bc0 -> B~0 K+ pi-]cc",
    "[ Xi_bc0 -> B~0 K+ pi+]cc", "[ Xi_bc0 -> B~0 K- pi-]cc"
]
auto_BX_detached_lines["TbcToBmKPiPi"] = [
    "[ Xi_bc0 -> B- K- pi+ pi+]cc", "[ Xi_bc0 -> B- K- pi+ pi-]cc",
    "[ Xi_bc0 -> B- K+ pi- pi-]cc", "[ Xi_bc0 -> B- K+ pi+ pi-]cc"
]
auto_BX_detached_lines["TbcToB0bKPiPiPi"] = [
    "[ Xi_bc0 -> B~0 K- pi+ pi+ pi-]cc", "[ Xi_bc0 -> B~0 K+ pi+ pi+ pi-]cc",
    "[ Xi_bc0 -> B~0 K- pi+ pi- pi-]cc", "[ Xi_bc0 -> B~0 K+ pi+ pi- pi-]cc"
]
auto_BX_detached_lines["TbcToB0bKMu"] = [
    "[ Xi_bc0 -> B~0 K- mu+]cc", "[ Xi_bc0 -> B~0 K+ mu-]cc",
    "[ Xi_bc0 -> B~0 K+ mu+]cc", "[ Xi_bc0 -> B~0 K- mu-]cc"
]
auto_BX_detached_lines["TbcToBmKPiMu"] = [
    "[ Xi_bc0 -> B- K- pi+ mu+]cc", "[ Xi_bc0 -> B- K- pi+ mu-]cc",
    "[ Xi_bc0 -> B- K+ pi- mu+]cc", "[ Xi_bc0 -> B- K+ pi+ mu-]cc"
]

make_spruce_lines(
    make_fun=bx.make_BX_detached_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_detached_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy,
)

## SemiLeptonic modes for B
auto_BX_detached_SL_lines = {}
for line in auto_BX_detached_lines:
    auto_BX_detached_SL_lines[line + "_SL"] = auto_BX_detached_lines[line]

make_spruce_lines(
    make_fun=bx.make_BX_detached_SL_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_detached_SL_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy_SL,
)


#################################################################################
## PersistReco=True bSpectroscopy sprucing lines to cover physics channels not considered above
################################################################################
@register_line_builder(sprucing_lines)
@configurable
def Bu_for_spectroscopy_sprucing_line(name='SpruceBandQ_BuForSpectroscopy',
                                      prescale=1):
    line_alg = b_for_spectroscopy.make_Bu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BuForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bd_for_spectroscopy_sprucing_line(name='SpruceBandQ_BdForSpectroscopy',
                                      prescale=1):
    line_alg = b_for_spectroscopy.make_Bd(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BdForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bs_for_spectroscopy_sprucing_line(name='SpruceBandQ_BsForSpectroscopy',
                                      prescale=1):
    line_alg = b_for_spectroscopy.make_Bs(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BsForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Lb_for_spectroscopy_sprucing_line(name='SpruceBandQ_LbForSpectroscopy',
                                      prescale=1):
    line_alg = b_for_spectroscopy.make_Lb(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_LbForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bc_for_spectroscopy_sprucing_line(name='SpruceBandQ_BcForSpectroscopy',
                                      prescale=1):
    line_alg = b_for_spectroscopy.make_Bc(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BcForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Xibm_for_spectroscopy_sprucing_line(name='SpruceBandQ_XibmForSpectroscopy',
                                        prescale=1):
    line_alg = b_for_spectroscopy.make_Xibm(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_XibmForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Xib0_for_spectroscopy_sprucing_line(name='SpruceBandQ_Xib0ForSpectroscopy',
                                        prescale=1):
    line_alg = b_for_spectroscopy.make_Xib0(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_Xib0ForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Omegab_for_spectroscopy_sprucing_line(
        name='SpruceBandQ_OmegabForSpectroscopy', prescale=1):
    line_alg = b_for_spectroscopy.make_Omegab(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_OmegabForSpectroscopyFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bud_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_BudForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Bud_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BudForSpectroscopySLFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bs_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_BsForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Bs_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BsForSpectroscopySLFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Lb_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_LbForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Lb_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_LbForSpectroscopySLFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Bc_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_BcForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Bc_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BcForSpectroscopySLFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Xib_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_XibForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Xib_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_XibForSpectroscopySLFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def Omegab_for_spectroscopy_SL_sprucing_line(
        name='SpruceBandQ_OmegabForSpectroscopySL', prescale=1):
    line_alg = b_for_spectroscopy.make_Omegab_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_OmegabForSpectroscopySLFullDecision"])


########################################################
## Create sprucing lines to read doubleCharm hlt2 output
########################################################
@register_line_builder(sprucing_lines)
@configurable
def doublecharm_samesign_sprucing_line(name='SpruceBandQ_DoubleCharmSameSign',
                                       prescale=1):
    line_alg = doublecharm.make_doublecharm_samesign()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharmSameSignFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_oppositesign_sprucing_line(
        name='SpruceBandQ_DoubleCharmOppositeSign', prescale=1):
    line_alg = doublecharm.make_doublecharm_oppositesign()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharmOppositeSignFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHH_sprucing_line(name='SpruceBandQ_DoubleCharm_D0ToHH',
                                     prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToKsLLHH_D0ToHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToHHFullDecision"
        ])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsDDHH_D0ToHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToKsDDHH_D0ToHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH_D0ToHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2BandQ_DoubleCharm_D0ToKsDDHH_D0ToHHFullDecision"
        ])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToKsLLHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsLLHHFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToKsDDHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToKsLLHH_D0ToKsDDHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToKsDDHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToKsDDHHFullDecision"
        ])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsDDHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToKsDDHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsDDHHFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFullDecision"])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsLLHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToKsLLHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsLLHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsLLHHFullDecision"
        ])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsDDHH_sprucing_line(
        name='SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToKsDDHH', prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsDDHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsDDHHFullDecision"
        ])


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_sprucing_line(name='SpruceBandQ_DoubleCharm_D0ToHHHH',
                                       prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHHFullDecision"])

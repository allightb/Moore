###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf.tonic import configurable
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import make_long_pions

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.probe_muons import make_velo_muons
from Hlt2Conf.algorithms_thor import ParticleContainersMerger
from Hlt2Conf.hlt1_tistos import hlt1_tis_on_any_filter
from PyConf.Algorithms import KSLongVeloFilter
from PyConf.Algorithms import KSVelo2LongEfficiencyMonitor, VeloIDOverlapRelationTable, FlattenDecayTree, Monitor__ParticleRange as Monitor, VoidFilter

turbo_lines = {}
turcal_lines = {}
monitoring_lines = {}

hlt1_tis_lines = [
    "Hlt1TrackMuonMVADecision", "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision", "Hlt1OneMuonTrackLineDecision",
    "Hlt1DiMuonHighMassDecision", "Hlt1DiMuonNoIPDecision",
    "Hlt1PassthroughDecision", "Hlt1GECPassthroughDecision"
]


@configurable
def filter_pions(particles, pvs=None, pt_min=0.4 * GeV):
    if pvs is not None:
        cut = F.require_all(
            F.P > 5000,
            F.PT > pt_min,
            F.ETA < 5.0,
            F.ETA > 1.5,
            F.PID_P < 5,
            F.MINIP(pvs) > 0.4 * mm,
        )
    else:
        cut = F.require_all(F.P > 5000, F.PT > pt_min, F.ETA < 5.0,
                            F.ETA > 1.5, F.PID_P < 5)

    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_velo(particles, pvs):
    cut = F.require_all(
        F.ETA < 4.7,
        F.ETA > 1.5,
        F.MINIP(pvs) > 0.5 * mm,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_tis_velo(particles, pvs):
    pre_filtered_particles = filter_particles_velo(particles, pvs)

    return hlt1_tis_on_any_filter(
        hlt1_trigger_lines=hlt1_tis_lines,
        data=pre_filtered_particles,
        name="Hlt1TISFilter_Velo2Long_{hash}",
        advanced_tistos_arguments={
            "TOSFracFT": 0.0,
            "TISFracUT": 0.0,
            "TOSFracVP": 1.0
        })


@configurable
def make_kshort_pi_muplus_specific_charge(pions,
                                          muons,
                                          decay_descriptor,
                                          pvs,
                                          comb_m_min=0 * MeV,
                                          comb_m_max=2650 * MeV,
                                          comb_maxdoca=0.15 * mm,
                                          name="KShortMuPiCombiner_{hash}"):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = F.require_all(F.END_VRHO < 100 * mm, F.END_VZ > 18 * mm,
                                F.END_VZ < 600 * mm,
                                F.BPVFD(pvs) > 5 * mm)
    return ParticleCombiner(
        [pions, muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_kshort_pi_muplus(pions, muons, **kwargs):
    ks_particle_combinations = [
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi+ mu+",
                                              **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi+ mu-",
                                              **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi- mu-",
                                              **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi- mu+",
                                              **kwargs),
    ]

    return ParticleContainersMerger(
        ks_particle_combinations, name="KS_combinations_{hash}")


def construct_hlt2_line(*,
                        name,
                        prescale,
                        pt_min,
                        pt_max,
                        p_min=5000.,
                        filter_tis=False,
                        apply_lambda_veto=False):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)

    if filter_tis:
        muonsVelo = filter_particles_tis_velo(make_velo_muons(), pvs)
    else:
        muonsVelo = filter_particles_velo(make_velo_muons(), pvs)

    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, pvs=pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=pvs,
        PVConstrainedMassMin=365.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=pt_min,
        PVConstrainedProbePtMax=pt_max,
        PVConstrainedProbePMin=p_min,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        ApplyLambdaVeto=apply_lambda_veto,
        name=f"TrackEffFilter_{name}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        persistreco=True)


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line(name="Hlt2TurCalVelo2Long_KshortVSoft",
                          prescale=0.0005):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=250., pt_max=800., p_min=3000.)


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_15(name="Hlt2TurCalVelo2Long_KshortSoft",
                             prescale=0.005):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=800., pt_max=1500.)


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_20(name="Hlt2TurCalVelo2Long_Kshort",
                             prescale=0.007):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=1500., pt_max=2000.)


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_high(name="Hlt2TurCalVelo2Long_KshortHard",
                               prescale=0.01):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=2000., pt_max=3500.)


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_vhigh(name="Hlt2TurCalVelo2Long_KshortVHard",
                                prescale=0.07):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=3500., pt_max=10000.)


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_vhard(name="Hlt2TurboVelo2Long_KshortVHard",
                                      prescale=0.4):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=3500.,
        pt_max=10000.,
        filter_tis=True,
        apply_lambda_veto=True)


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_hard(name="Hlt2TurboVelo2Long_KshortHard",
                                     prescale=0.12):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=2000.,
        pt_max=3500.,
        filter_tis=True,
        apply_lambda_veto=True)


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line(name="Hlt2TurboVelo2Long_Kshort",
                                prescale=0.05):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=1500.,
        pt_max=2000.,
        filter_tis=True,
        apply_lambda_veto=True)


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_soft(name="Hlt2TurboVelo2Long_KshortSoft",
                                     prescale=0.013):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=800.,
        pt_max=1500.,
        filter_tis=True)


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_vsoft(name="Hlt2TurboVelo2Long_KshortVSoft",
                                      prescale=0.0015):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=250.,
        pt_max=800.,
        p_min=3000.,
        filter_tis=True)


@configurable
def construct_hlt2_monitoring_line(*, name, prescale, no_bias=False):
    pvs = make_pvs()
    all_long_pions = make_long_pions()
    pions = filter_pions(all_long_pions, pvs, pt_min=0.5 * GeV)
    if not no_bias:
        muonsVelo = filter_particles_tis_velo(make_velo_muons(), pvs)
        hlt1_filter_code = None
    else:
        muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
        hlt1_filter_code = "Hlt1ODINLumiDecision"

    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, pvs=pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1
    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=pvs,
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePMin=2000.,
        PVConstrainedProbePtMin=250.,
        IPperpendicularMax=0.007,
        ApplyLambdaVeto=True,
        IPMax=0.8,
        name="TrackEffFilterMonitoring_{hash}",
    ).OutputParticles
    muon_filter = VoidFilter(Cut=F.SIZE(muonsVelo) > 0)

    flattened_decay_tree = FlattenDecayTree(InputParticles=filtered_kshorts)
    basic_particles = ParticleFilter(
        flattened_decay_tree.OutputParticles,
        F.FILTER(F.ISBASICPARTICLE),
        name="MyParticleFilterThing_{hash}")

    relation_table_match_to_long = VeloIDOverlapRelationTable(
        MatchFrom=basic_particles,
        MatchTo=all_long_pions,
        MinMatchFraction=0.5)
    plot_efficiency = KSVelo2LongEfficiencyMonitor(
        name=f"PlotKsEfficiencies_{name}",
        Particles=filtered_kshorts,
        Table=relation_table_match_to_long,
        MinMatchFraction=0.7)
    mass_monitor = Monitor(
        name="Monitor_ks_m_{hash}",
        Input=filtered_kshorts,
        Variable=F.MASS,
        HistogramName="/Hlt2KshortVeloLong/m",
        Bins=200,
        Range=(300, 700))

    algs = [
        require_pvs(make_pvs()), muon_filter, filtered_kshorts, mass_monitor,
        plot_efficiency
    ]
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=algs,
        hlt1_filter_code=hlt1_filter_code)


@register_line_builder(monitoring_lines)
@configurable
def kshort_velo_long_all_hlt1_monitoring_line(
        name="Hlt2CalibMon_KshortVeloLongAllHlt1", prescale=1.0,
        no_bias=False):
    return construct_hlt2_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias)


@register_line_builder(monitoring_lines)
@configurable
def kshort_velo_long_nobias_monitoring_line(
        name="Hlt2CalibMon_KshortVeloLongNoBias", prescale=1.0, no_bias=True):
    return construct_hlt2_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias)


from RecoConf.hlt1_allen import make_allen_seed_and_match_tracks, allen_gaudi_config
from PyConf.Algorithms import FunctionalChargedProtoParticleMaker
from Hlt2Conf.standard_particles import (_make_particles,
                                         get_all_track_selector)

from Moore.config import Reconstruction


def make_allen_tracks():
    seed_and_match_tracks = make_allen_seed_and_match_tracks()["v1keyed"]
    return seed_and_match_tracks


@configurable
def construct_hlt1_monitoring_line(*, name, prescale, no_bias=False):
    pvs = make_pvs()
    all_long_pions = make_long_pions()
    pions = filter_pions(all_long_pions, pvs, pt_min=0.5 * GeV)
    if not no_bias:
        muonsVelo = filter_particles_tis_velo(make_velo_muons(), pvs)
        hlt1_filter_code = None
    else:
        muonsVelo = filter_particles_velo(make_velo_muons(), pvs)
        hlt1_filter_code = "Hlt1ODINLumiDecision"

    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, pvs=pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1
    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=pvs,
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePMin=2000.,
        PVConstrainedProbePtMin=250.,
        IPperpendicularMax=0.007,
        ApplyLambdaVeto=True,
        IPMax=0.8,
        name="TrackEffFilterMonitoring_{hash}",
    ).OutputParticles
    muon_filter = VoidFilter(Cut=F.SIZE(muonsVelo) > 0)

    flattened_decay_tree = FlattenDecayTree(InputParticles=filtered_kshorts)
    basic_particles = ParticleFilter(
        flattened_decay_tree.OutputParticles,
        F.FILTER(F.ISBASICPARTICLE),
        name="MyParticleFilterThing_{hash}")
    mass_monitor = Monitor(
        name="Monitor_ks_m_{hash}",
        Input=filtered_kshorts,
        Variable=F.MASS,
        HistogramName="/Hlt2KshortVeloLong/m",
        Bins=200,
        Range=(300, 700))
    from Allen.config import allen_detectors, setup_allen_non_event_data_service
    from PyConf.control_flow import CompositeNode, NodeLogic

    with allen_gaudi_config.bind(sequence='hlt1_pp_matching_no_ut'):
        allen_long_tracks = make_allen_tracks()
        wrapper = Reconstruction("allen_wrapper_for_config",
                                 [allen_long_tracks])
        charged_long_protos_callable = lambda: FunctionalChargedProtoParticleMaker(Inputs=[allen_long_tracks], Code=F.require_all(F.ALL))
        allen_long_pions = _make_particles(
            species="pion",
            get_track_selector=get_all_track_selector,
            make_protoparticles=charged_long_protos_callable)
        detectors = allen_detectors(wrapper.node)
        non_event_data_node = setup_allen_non_event_data_service(
            bank_types=detectors)
        relation_table_match_to_long = VeloIDOverlapRelationTable(
            MatchFrom=basic_particles,
            MatchTo=allen_long_pions,
            MinMatchFraction=0.5)
        plot_efficiency = KSVelo2LongEfficiencyMonitor(
            name=f"PlotKsEfficiencies_{name}",
            Particles=filtered_kshorts,
            Table=relation_table_match_to_long,
            MinMatchFraction=0.7)

        allen_node = CompositeNode(
            'allen_reconstruction',
            combine_logic=NodeLogic.NONLAZY_OR,
            children=[non_event_data_node, plot_efficiency],
            force_order=True)

    algs = [
        require_pvs(make_pvs()), muon_filter, filtered_kshorts, mass_monitor,
        allen_node
    ]
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=algs,
        hlt1_filter_code=hlt1_filter_code)


# Commented out for now as the line would break running with DD4HEP
# @register_line_builder(monitoring_lines)
@configurable
def kshort_velo_long_allen_monitoring_line(
        name="Hlt2KshortVeloLongAllenMonitoring", prescale=1.0, no_bias=False):
    return construct_hlt1_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias)

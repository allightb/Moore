###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of the following lines:

  1. D(s)+ -> eta(') pi/K/mu
  2. Lambda_c+ -> eta(') p

with the subsequent decays

  1. eta -> pi+ pi- gamma/pi0
  2. eta' -> (rho0 gamma)/(pi+pi-eta)

When the eta is coming from the eta' decay, it is reconstructed also in the
gamma gamma final state (resolved eta).

TODO: add vertex cuts when the combiner with neutrals becomes available

TODO: Because of an issue with the vertex fitter when neutrals are involved
(see Rec#208), we need to use the "ParticleAdder" option in the particle
combiner for D(s)+/Lambda_c+. This also implies that no cut can be applied on
the D(s)+/Lambdac+ vertex at the moment (cuts on the rho0 -> pi+ pi- vertex
have been tightened w.r.t Run 2 selections for this reason). eta/eta(')
candidates are not made separately to be able to apply a cut on the doca
between the rho and the pion/kaon/proton coming from the D(s)+/Lambdac+ decay
(as a replacement for the vtxchi2 cut). This apporach can be changed when the
vertex fit will properly work also with neutrals.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV, ps, mm
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_photons, make_has_rich_long_kaons,
    make_merged_pi0s, make_has_rich_long_protons, make_ismuon_long_muon)
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from RecoConf.reconstruction_objects import make_pvs
from .prefilters import charm_prefilters
from .particle_properties import _PI0_M, _ETA_M


# re-definitions of functors
def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


#####################################################################
### Shortcuts for filters and builders used throughout the module ###
#####################################################################


def _make_photons_from_pi0s():
    return make_photons(PtCut=200 * MeV)


def _make_resolved_pi0s():
    photons = _make_photons_from_pi0s()
    combination_code = in_range(_PI0_M - 30 * MeV, F.MASS, _PI0_M + 30 * MeV)
    composite_code = F.PT > 300 * MeV
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[photons, photons],
        name='Charm_DToEtaH_Resolved_Pi0_{hash}',
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def _make_resolved_etas():
    photons = _make_photons_from_pi0s()
    combination_code = in_range(_ETA_M - 30 * MeV, F.MASS, _ETA_M + 30 * MeV)
    composite_code = F.PT > 300 * MeV
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[photons, photons],
        name='Charm_DToEtaH_Resolved_Eta_{hash}',
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def _filter_pions_from_eta():
    cut = F.require_all(
        _MIPCHI2_MIN(16.),
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PT > 1 * GeV,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def _filter_pions_from_eta_loose():
    cut = F.require_all(
        _MIPCHI2_MIN(16.),
        F.PT > 350. * MeV,
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def _filter_pions_from_etaprime():
    cut = F.require_all(
        _MIPCHI2_MIN(25.),
        F.PT > 1 * GeV,
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


#This function is used to filter pions and kaons coming directly from the D(s)+ decay [D(s)+->eta(')pi+/K+]
def _filter_odd_particles_from_ds(particles):
    cut = F.require_all(_MIPCHI2_MIN(16.), F.PT > 600 * MeV, F.P > 1 * GeV)
    return ParticleFilter(particles, F.FILTER(cut))
    #TODO Selections commented out for the start of Run 3
    #trghostprob_max=0.4,
    #trchi2dof_max=3)


def _filter_muons_from_ds():
    cut = F.require_all(_MIPCHI2_MIN(16.), F.PT > 500 * MeV)
    return ParticleFilter(make_ismuon_long_muon(), F.FILTER(cut))
    #TODO Selections commented out for the start of Run 3
    #trghostprob_max=0.4,
    #trchi2dof_max=3)


def _filter_protons_from_lambdas():
    cut = F.require_all(_MIPCHI2_MIN(16.), F.PT > 600 * MeV, F.P > 1 * GeV)
    return ParticleFilter(make_has_rich_long_protons(), F.FILTER(cut))
    #TODO Selections commented out for the start of Run 3
    #trghostprob_max=0.4,
    #trchi2dof_max=3)


def _filter_neutrals_from_eta(particles):
    """Particles might be photons or pi0s."""
    cut = F.require_all(F.PT > 1200 * MeV, F.P > 1500 * MeV)
    return ParticleFilter(particles, F.FILTER(cut))


def _filter_neutrals_from_eta_loose(particles):
    """Particles might be photons or pi0s."""
    return ParticleFilter(particles, F.FILTER(F.PT > 350 * MeV))


def _filter_resolvedetas_from_etaprime():
    cut = F.require_all(F.PT > 1500 * MeV, F.P > 2 * GeV)
    return ParticleFilter(_make_resolved_etas(), F.FILTER(cut))


#TODO: Add request on the rho(770)0/(pi+pi-)pair be TOS on Hlt1TrackMVA or Hlt1TwoTrackMVA (when the corresponding ThOr functor will be available)
def _make_pion_pairs_from_eta():
    pions = _filter_pions_from_eta()
    combination_code = F.require_all(
        in_range(180 * MeV, F.MASS, 770 * MeV),
        F.MAXDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(200 * MeV, F.MASS, 750 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name='Charm_DToEtaH_PionPairsFromEta_{hash}',
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_pion_pairs_from_eta_loose():
    pions = _filter_pions_from_eta_loose()
    combination_code = F.require_all(
        in_range(180 * MeV, F.MASS, 770 * MeV),
        F.MAXDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(200 * MeV, F.MASS, 750 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name='Charm_DToEtaH_PionPairsFromEtaLoose_{hash}',
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_pion_pairs_from_etaprime():
    pions = _filter_pions_from_etaprime()
    combination_code = F.require_all(
        in_range(180 * MeV, F.MASS, 1070 * MeV),
        F.MAXDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(200 * MeV, F.MASS, 1050 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name='Charm_DToEtaH_PionPairsFromEtaPrime_{hash}',
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_rho_from_etaprime():
    """Since eta' -> rho0 gamma dominates the eta' -> pi+pi- gamma rate,
    the cut m(pi+ pi-) > 600 MeV is effective in reducing the background,
    which has a substantial contribution at m(pi+ pi-) < 600 MeV.
    This is why we have two different combiners for pi+ pi- pairs coming from
    the eta' decay. The same cannot be done for eta' -> pi+ pi- eta.
    """
    pions = _filter_pions_from_etaprime()
    combination_code = F.require_all(
        in_range(580 * MeV, F.MASS, 1020 * MeV),
        F.MAXDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(600 * MeV, F.MASS, 1000 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name='Charm_DToEtaH_RhoFromEtaPrime_{hash}',
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_dplus_to_etah(pion_pairs, neutrals, odd_particles, descriptor):
    combination_12_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    combination_code = F.require_all(
        in_range(1600 * MeV, F.MASS, 2200 * MeV),
        F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm,
    )
    vertex_code = F.require_all(in_range(1650 * MeV, F.MASS, 2150 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_DplusToEtaH_{hash}')


def _make_dplus_to_etaprimeh(pion_pairs, neutrals, odd_particles, descriptor):
    combination_12_code = F.require_all(
        in_range(850 * MeV, F.MASS, 1060 * MeV))
    combination_code = F.require_all(
        in_range(1600 * MeV, F.MASS, 2200 * MeV),
        F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm,
    )
    vertex_code = F.require_all(in_range(1650 * MeV, F.MASS, 2150 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_DplusToEtaPrimeH_{hash}')


def _make_lambdas_to_etap(pion_pairs, neutrals, odd_particles, descriptor):
    combination_12_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    combination_code = F.require_all(
        in_range(2100 * MeV, F.MASS, 2500 * MeV),
        F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm,
    )
    vertex_code = F.require_all(in_range(2150 * MeV, F.MASS, 2450 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_LambdasToEtaP_{hash}')


def _make_lambdas_to_etaprimep(pion_pairs, neutrals, odd_particles,
                               descriptor):
    combination_12_code = F.require_all(
        in_range(850 * MeV, F.MASS, 1060 * MeV))
    combination_code = F.require_all(
        in_range(2100 * MeV, F.MASS, 2500 * MeV),
        F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm,
    )
    vertex_code = F.require_all(in_range(2150 * MeV, F.MASS, 2450 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_LambdasToEtaPrimeP_{hash}')


def _make_dplus_to_etamu(pion_pairs, neutrals, odd_particles, descriptor):
    combination_12_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    combination_code = F.require_all(
        in_range(1500 * MeV, F.MASS, 2200 * MeV),
        F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm,
    )
    vertex_code = F.require_all(in_range(1550 * MeV, F.MASS, 2150 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_DplusToEtaMu_{hash}')


def _make_dplus_to_etaprimemu(pion_pairs, neutrals, odd_particles, descriptor):
    combination_12_code = F.require_all(
        in_range(850 * MeV, F.MASS, 1060 * MeV))
    combination_code = F.require_all(
        in_range(1500 * MeV, F.MASS, 2200 * MeV), F.PT > 3000 * MeV,
        F.DOCA(1, 3) < 0.05 * mm)
    vertex_code = F.require_all(in_range(1550 * MeV, F.MASS, 2150 * MeV))
    return ParticleCombiner([pion_pairs, neutrals, odd_particles],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            Combination12Cut=combination_12_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_DplusToEtaPrimeMu_{hash}')


def _make_eta_from_etaprime(pion_pairs, neutrals, descriptor):
    """This combiner is used to select eta' -> pi+ pi- eta,
    with eta -> pi+ pi- gamma or eta -> pi+ pi- pi0
    """
    combination_code = F.require_all(
        in_range(420 * MeV, F.MASS, 680 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    return ParticleCombiner([pion_pairs, neutrals],
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name='Charm_DToEtaH_EtaFromEtaPrime_{hash}')


#########################
### Lines definition  ###
#########################

all_lines = {}


@register_line_builder(all_lines)
def dp2etapip_eta2pimpipG_line(name="Hlt2Charm_DpDspToEtaPip_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        photons,
        odd_pions,
        descriptor="[D+ -> rho(770)0 gamma pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etakp_eta2pimpipG_line(name="Hlt2Charm_DpDspToEtaKp_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        photons,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 gamma K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etappip_etap2pimpipG_line(
        name="Hlt2Charm_DpDspToEtaprPip_EtaprToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    pion_pairs = _make_rho_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs,
        photons,
        odd_pions,
        descriptor="[D+ -> rho(770)0 gamma pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapkp_etap2pimpipG_line(
        name="Hlt2Charm_DpDspToEtaprKp_EtaprToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    pion_pairs = _make_rho_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs,
        photons,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 gamma K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapip_eta2pimpipi0R_line(
        name="Hlt2Charm_DpDspToEtaPip_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta(_make_resolved_pi0s())
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        resolved_pi0s,
        odd_pions,
        descriptor="[D+ -> rho(770)0 pi0 pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etakp_eta2pimpipi0R_line(
        name="Hlt2Charm_DpDspToEtaKp_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta(_make_resolved_pi0s())
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        resolved_pi0s,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 pi0 K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapip_eta2pimpipi0M_line(
        name="Hlt2Charm_DpDspToEtaPip_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta(make_merged_pi0s())
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        merged_pi0s,
        odd_pions,
        descriptor="[D+ -> rho(770)0 pi0 pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etakp_eta2pimpipi0M_line(
        name="Hlt2Charm_DpDspToEtaKp_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta(make_merged_pi0s())
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        merged_pi0s,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 pi0 K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etappip_etap2pimpipetaR_line(
        name="Hlt2Charm_DpDspToEtaprPip_EtaprToEtaPimPip_R"):

    resolved_etas = _filter_resolvedetas_from_etaprime()
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    pion_pairs = _make_pion_pairs_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs,
        resolved_etas,
        odd_pions,
        descriptor="[D+ -> rho(770)0 eta pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapkp_etap2pimpipetaR_line(
        name="Hlt2Charm_DpDspToEtaprKp_EtaprToEtaPimPip_R"):

    resolved_etas = _filter_resolvedetas_from_etaprime()
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    pion_pairs = _make_pion_pairs_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs,
        resolved_etas,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 eta K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etappip_etap2pimpipeta_eta2pippimgamma_line(
        name="Hlt2Charm_DpDspToEtaprPip_EtaprToEtaPimPip_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta_loose(make_photons())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, photons, descriptor="eta -> rho(770)0 gamma ")
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_pions,
        descriptor="[D+ -> rho(770)0 eta pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etapkp_etap2pimpipeta_eta2pippimgamma_line(
        name="Hlt2Charm_DpDspToEtaprKp_EtaprToEtaPimPip_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta_loose(make_photons())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, photons, descriptor="eta -> rho(770)0 gamma ")
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 eta K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etappip_etap2pimpipeta_eta2pippimpi0R_line(
        name="Hlt2Charm_DpDspToEtaprPip_EtaprToEtaPimPip_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, resolved_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_pions,
        descriptor="[D+ -> rho(770)0 eta pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etapkp_etap2pimpipeta_eta2pippimpi0R_line(
        name="Hlt2Charm_DpDspToEtaprKp_EtaprToEtaPimPip_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, resolved_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 eta K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etappip_etap2pimpipeta_eta2pippimpi0M_line(
        name="Hlt2Charm_DpDspToEtaprPip_EtaprToEtaPimPip_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, merged_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    odd_pions = _filter_odd_particles_from_ds(make_has_rich_long_pions())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_pions,
        descriptor="[D+ -> rho(770)0 eta pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etapkp_etap2pimpipeta_eta2pippimpi0M_line(
        name="Hlt2Charm_DpDspToEtaprKp_EtaprToEtaPimPip_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, merged_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    odd_kaons = _filter_odd_particles_from_ds(make_has_rich_long_kaons())
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        odd_kaons,
        descriptor="[D+ -> rho(770)0 eta K+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def lambdac2etap_eta2pimpipG_line(name="Hlt2Charm_LcpToEtaPp_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    pion_pairs = _make_pion_pairs_from_eta()
    lambdas = _make_lambdas_to_etap(
        pion_pairs,
        photons,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 gamma p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etap_eta2pimpippi0R_line(
        name="Hlt2Charm_LcpToEtaPp_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta(_make_resolved_pi0s())
    pion_pairs = _make_pion_pairs_from_eta()
    lambdas = _make_lambdas_to_etap(
        pion_pairs,
        resolved_pi0s,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 pi0 p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etap_eta2pimpippi0M_line(
        name="Hlt2Charm_LcpToEtaPp_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta(make_merged_pi0s())
    pion_pairs = _make_pion_pairs_from_eta()
    lambdas = _make_lambdas_to_etap(
        pion_pairs,
        merged_pi0s,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 pi0 p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etapp_etap2pimpipetaR_line(
        name="Hlt2Charm_LcpToEtaprPp_EtaprToEtaPimPip_R"):

    resolved_etas = _filter_resolvedetas_from_etaprime()
    pion_pairs = _make_pion_pairs_from_etaprime()
    lambdas = _make_lambdas_to_etaprimep(
        pion_pairs,
        resolved_etas,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 eta p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etapp_etap2pimpipG_line(
        name="Hlt2Charm_LcpToEtaprPp_EtaprToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    pion_pairs = _make_rho_from_etaprime()
    lambdas = _make_lambdas_to_etaprimep(
        pion_pairs,
        photons,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 gamma p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etapp_etap2pimpipeta_eta2pippimgamma_line(
        name="Hlt2Charm_LcpToEtaprPp_EtaprToEtaPimPip_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta_loose(make_photons())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, photons, descriptor="eta -> rho(770)0 gamma ")
    lambdas = _make_lambdas_to_etaprimep(
        etapr_pion_pairs,
        etas,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 eta p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etapp_etap2pimpipeta_eta2pippimpi0R_line(
        name="Hlt2Charm_LcpToEtaprPp_EtaprToEtaPimPip_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, resolved_pi0s, descriptor="eta -> rho(770)0 pi0")
    lambdas = _make_lambdas_to_etaprimep(
        etapr_pion_pairs,
        etas,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 eta p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, lambdas],
    )


@register_line_builder(all_lines)
def lambdac2etapp_etap2pimpipeta_eta2pippimpi0M_line(
        name="Hlt2Charm_LcpToEtaprPp_EtaprToEtaPimPip_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, merged_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    lambdas = _make_lambdas_to_etaprimep(
        etapr_pion_pairs,
        etas,
        _filter_protons_from_lambdas(),
        descriptor="[Lambda_c+ -> rho(770)0 eta p+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, lambdas],
    )


@register_line_builder(all_lines)
def dp2etamu_eta2pimpipG_line(name="Hlt2Charm_DpDspToEtaMup_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    pion_pairs = _make_pion_pairs_from_eta()
    muons = _filter_muons_from_ds()
    dplus = _make_dplus_to_etamu(
        pion_pairs, photons, muons, descriptor="[D+ -> rho(770)0 gamma mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etamu_eta2pimpipi0R_line(
        name="Hlt2Charm_DpDspToEtaMup_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta(_make_resolved_pi0s())
    muons = _filter_muons_from_ds()
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        resolved_pi0s,
        muons,
        descriptor="[D+ -> rho(770)0 pi0 mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etamu_eta2pimpipi0M_line(
        name="Hlt2Charm_DpDspToEtaMup_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta(make_merged_pi0s())
    muons = _filter_muons_from_ds()
    pion_pairs = _make_pion_pairs_from_eta()
    dplus = _make_dplus_to_etah(
        pion_pairs,
        merged_pi0s,
        muons,
        descriptor="[D+ -> rho(770)0 pi0 mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapmu_etap2pimpipG_line(
        name="Hlt2Charm_DpDspToEtaprMup_EtaprToPimPipG"):

    photons = _filter_neutrals_from_eta(make_photons())
    muons = _filter_muons_from_ds()
    pion_pairs = _make_rho_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs, photons, muons, descriptor="[D+ -> rho(770)0 gamma mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapmu_etap2pimpipetaR_line(
        name="Hlt2Charm_DpDspToEtaprMup_EtaprToPimPipEta_R"):

    resolved_etas = _filter_resolvedetas_from_etaprime()
    muons = _filter_muons_from_ds()
    pion_pairs = _make_pion_pairs_from_etaprime()
    dplus = _make_dplus_to_etaprimeh(
        pion_pairs,
        resolved_etas,
        muons,
        descriptor="[D+ -> rho(770)0 eta mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [pion_pairs, dplus],
    )


@register_line_builder(all_lines)
def dp2etapmu_etap2pimpipeta_eta2pippimgamma_line(
        name="Hlt2Charm_DpDspToEtaprMup_EtaprToEtaPimPip_EtaToPimPipG"):

    photons = _filter_neutrals_from_eta_loose(make_photons())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, photons, descriptor="eta -> rho(770)0 gamma ")
    muons = _filter_muons_from_ds()
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        muons,
        descriptor="[D+ -> rho(770)0 eta mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etapmu_etap2pimpipeta_eta2pippimpi0R_line(
        name="Hlt2Charm_DpDspToEtaprMup_EtaprToEtaPimPip_EtaToPimPipPi0_R"):

    resolved_pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, resolved_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    muons = _filter_muons_from_ds()
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        muons,
        descriptor="[D+ -> rho(770)0 eta mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )


@register_line_builder(all_lines)
def dp2etapmu_etap2pimpipeta_eta2pippimpi0M_line(
        name="Hlt2Charm_DpDspToEtaprMup_EtaprToEtaPimPip_EtaToPimPipPi0_M"):

    merged_pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    etapr_pion_pairs = _make_pion_pairs_from_etaprime()
    eta_pion_pairs = _make_pion_pairs_from_eta_loose()
    etas = _make_eta_from_etaprime(
        eta_pion_pairs, merged_pi0s, descriptor="eta -> rho(770)0 pi0 ")
    muons = _filter_muons_from_ds()
    dplus = _make_dplus_to_etaprimeh(
        etapr_pion_pairs,
        etas,
        muons,
        descriptor="[D+ -> rho(770)0 eta mu+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() +
        [etapr_pion_pairs, eta_pion_pairs, etas, dplus],
    )

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D0 -> h- h+ HLT2 lines.

Prompt lines
============

From D0 mesons produced in the PV. Final states built are:

  1. D0 -> K-  pi+ and its charge conjugate
  2. D0 -> K-  K+
  3. D0 -> pi- pi+
  4. D0 -> K-  pi+ and its charge conjugate, with low bias
  5. D0 -> K-  K+, with low bias
  6. D0 -> pi- pi+, with low bias
  7. D*+ -> (D0 -> K-  pi+) pi+ and its charge conjugate
  8. D*+ -> (D0 -> pi- K+ ) pi+ and its charge conjugate
  9. D*+ -> (D0 -> K-  K+ ) pi+ and D*- -> (D0 -> K-  K+ ) pi-
 10. D*+ -> (D0 -> pi- pi+) pi+ and D*- -> (D0 -> pi- pi+) pi-
 11. D*+ -> (D0 -> K-  pi+) pi+ and its charge conjugate, with low bias
 12. D*+ -> (D0 -> pi- K+ ) pi+ and its charge conjugate, with low bias
 13. D*+ -> (D0 -> K-  K+ ) pi+ and D*- -> (D0 -> K-  K+ ) pi-, with low bias
 14. D*+ -> (D0 -> pi- pi+) pi+ and D*- -> (D0 -> pi- pi+) pi-, lwith low bias

Muon-tagged lines
=================

B -> (D0-> h- h+) mu X HLT2 lines (plus control lines). Final states built are:

 1.  D0  -> K-  pi+ and its charge conjugate
 2.  D0  -> K-  K+
 3.  D0  -> pi- pi+
 4.  D*+ -> (D0 -> K-  pi+) pi+ and its charge conjugate
 5.  D*- -> (D0 -> K-  pi+) pi- and its charge conjugate
 6.  D*+ -> (D0 -> K-  K+ ) pi+ and D*- -> (D0 -> K-  K+ ) pi-
 7.  D*+ -> (D0 -> pi- pi+) pi+ and D*- -> (D0 -> pi- pi+) pi-
 8.  B-  -> (D0 -> K-  pi+) mu- and its charge conjugate
 9.  B+  -> (D0 -> K-  pi+) mu+ and its charge conjugate
 10. B-  -> (D0 -> K-  K+ ) mu- and B+  -> (D0 -> K-  K+ ) mu+
 11. B-  -> (D0 -> pi- pi+) mu- and B+  -> (D0 -> pi- pi+) mu+
 12. B-  -> (D*(2010)+ -> (D0 -> K-  pi+) pi+) mu- and its charge conjugate
 13. B+  -> (D*(2010)- -> (D0 -> K-  pi+) pi-) mu+ and its charge conjugate
 14. B-  -> (D*(2010)+ -> (D0 -> K-  K+ ) pi+) mu- and B+  -> (D*(2010)- -> (D0 -> K-  K+ ) pi-) mu+
 15. B-  -> (D*(2010)+ -> (D0 -> pi- pi+) pi+) mu- and B+  -> (D*(2010)- -> (D0 -> pi- pi+) pi-) mu+
 16. B+  -> (D*(2010)+ -> (D0 -> K-  pi+) pi+) mu+ and its charge conjugate (combinatorial proxy)
 17. B-  -> (D*(2010)- -> (D0 -> K-  pi+) pi-) mu- and its charge conjugate (combinatorial proxy)
 18. B+  -> (D*(2010)+ -> (D0 -> K-  K+ ) pi+) mu+ and B-  -> (D*(2010)- -> (D0 -> K-  K+ ) pi-) mu- (combinatorial proxy)
 20. B+  -> (D*(2010)+ -> (D0 -> pi- pi+) pi+) mu+ and B-  -> (D*(2010)- -> (D0 -> pi- pi+) pi-) mu- (combinatorial proxy)


The D0 -> h- h+, with identical h species, do not create the charge conjugate
D~0 objects because it would have identical properties and hence would be
a waste of CPU (duplicating the selection and vertexing). This is annoying when
D* tagging is eventually added, however, and one cannot create the 'physical'
charge combinations of `D*+ -> D0 pi+` and `D*- -> D~0 pi-`. It can be
considered a flaw in the selection framework that we cannot currently express
these sorts of combinations cleanly. For the moment the 'unphisical'
`D*- -> D0 pi-` decay is reconstructed instead.
See https://gitlab.cern.ch/lhcb/Moore/issues/64

TODO add Hlt1 selections for lowbias lines
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, micrometer as um, picosecond as ps
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons)
from .prefilters import charm_prefilters
from .taggers import make_dstars, make_tagging_muons, make_sl_bs, make_dt_bs
#from . import charm_isolation as isolation

all_lines = {}

################################################################################
# Prompt lines
################################################################################


def make_charm_pions():
    """Return pions maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
            ), ),
    )


def make_charm_kaons():
    """Return kaons maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
            ), ),
    )


def make_charm_pions_lowbias():
    """Return pions maker for D0 decay reconstructed with low-bias selection.
    """
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCUT(IPCut=60 * um, Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
            ), ),
    )


def make_charm_kaons_lowbias():
    """Return kaons maker for D0 decay reconstructed with low-bias selection.
    """
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCUT(IPCut=60 * um, Vertices=make_pvs()),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
            ), ),
    )


def make_dzeros(particle1, particle2, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    if descriptor == "[D0 -> K- pi+]cc":
        name = 'Charm_D0ToHH_BuilderD0ToKmPip'
    elif descriptor == "D0 -> K- K+":
        name = 'Charm_D0ToHH_BuilderD0ToKmKp'
    elif descriptor == "D0 -> pi- pi+":
        name = 'Charm_D0ToHH_BuilderD0ToPimPip'
    elif descriptor == "[D0 -> K+ pi-]cc":
        name = 'Charm_D0ToHH_BuilderD0ToKpPim'
    else:
        raise RuntimeError(
            "charm.b_to_d0mu_d0_to_hh.make_dzeros_for_b called with unsupported decay descriptor."
        )
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 25.,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )


def make_dzeros_lowbias(particle1, particle2, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states, selected with low-biasing requirements.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    if descriptor == "[D0 -> K- pi+]cc":
        name = 'Charm_D0ToHH_BuilderD0ToKmPip_LowBias'
    elif descriptor == "D0 -> K- K+":
        name = 'Charm_D0ToHH_BuilderD0ToKmKp_LowBias'
    elif descriptor == "D0 -> pi- pi+":
        name = 'Charm_D0ToHH_BuilderD0ToPimPip_LowBias'
    elif descriptor == "[D0 -> K+ pi-]cc":
        name = 'Charm_D0ToHH_BuilderD0ToKpPim_LowBias'
    else:
        raise RuntimeError(
            "charm.b_to_d0mu_d0_to_hh.make_dzeros_for_b called with unsupported decay descriptor."
        )
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1714 * MeV, F.MASS, 2014 * MeV),
            F.MAX(F.PT) > 1200 * MeV,
            F.PT > 2 * GeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1764 * MeV, F.MASS, 1964 * MeV),
            F.CHI2DOF < 10.,
            F.BPVLTIME(make_pvs()) > 0.204 * ps,
        ),
    )


@register_line_builder(all_lines)
def dzero2kpi_line(name='Hlt2Charm_D0ToKmPip', prescale=0.2):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    dzeros = make_dzeros(kaons, pions, '[D0 -> K- pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dzero2pipi_line(name='Hlt2Charm_D0ToPimPip', prescale=0.2):
    pions = make_charm_pions()
    dzeros = make_dzeros(pions, pions, 'D0 -> pi- pi+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dzero2kk_line(name='Hlt2Charm_D0ToKmKp', prescale=0.2):
    kaons = make_charm_kaons()
    dzeros = make_dzeros(kaons, kaons, 'D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dzero2kpi_lowbias_line(name='Hlt2Charm_D0ToKmPip_LowBias', prescale=0.2):
    kaons = make_charm_kaons_lowbias()
    pions = make_charm_pions_lowbias()
    dzeros = make_dzeros_lowbias(kaons, pions, '[D0 -> K- pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2KPiDecision") # TODO


@register_line_builder(all_lines)
def dzero2kk_lowbias_line(name='Hlt2Charm_D0ToKmKp_LowBias', prescale=0.2):
    kaons = make_charm_kaons_lowbias()
    dzeros = make_dzeros_lowbias(kaons, kaons, 'D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2KKDecision") # TODO


@register_line_builder(all_lines)
def dzero2pipi_lowbias_line(name='Hlt2Charm_D0ToPimPip_LowBias', prescale=0.2):
    pions = make_charm_pions_lowbias()
    dzeros = make_dzeros_lowbias(pions, pions, 'D0 -> pi- pi+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2PiPiDecision") # TODO


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kmpip_line(name='Hlt2Charm_DstpToD0Pip_D0ToKmPip',
                                     prescale=1):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    dzeros = make_dzeros(kaons, pions, '[D0 -> K- pi+]cc')
    dstars = make_dstars(
        dzeros, self_conjugate_d0_decay=False, d0_name="D0ToHH_D0ToKmPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kppim_line(name='Hlt2Charm_DstpToD0Pip_D0ToKpPim',
                                     prescale=1):
    kaons = make_charm_kaons()
    pions = make_charm_pions()
    dzeros = make_dzeros(kaons, pions, '[D0 -> K+ pi-]cc')
    dstars = make_dstars(
        dzeros, self_conjugate_d0_decay=False, d0_name="D0ToHH_D0ToKpPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kmkp_line(name='Hlt2Charm_DstpToD0Pip_D0ToKmKp',
                                    prescale=1):
    kaons = make_charm_kaons()
    dzeros = make_dzeros(kaons, kaons, 'D0 -> K- K+')
    dstars = make_dstars(
        dzeros, self_conjugate_d0_decay=True, d0_name="D0ToHH_D0ToKmKp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimpip_line(name='Hlt2Charm_DstpToD0Pip_D0ToPimPip',
                                      prescale=1):
    pions = make_charm_pions()
    dzeros = make_dzeros(pions, pions, 'D0 -> pi- pi+')
    dstars = make_dstars(
        dzeros, self_conjugate_d0_decay=True, d0_name="D0ToHH_D0ToPimPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kmpip_lowbias_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPip_LowBias', prescale=1):
    kaons = make_charm_kaons_lowbias()
    pions = make_charm_pions_lowbias()
    dzeros = make_dzeros_lowbias(kaons, pions, '[D0 -> K- pi+]cc')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=False,
        d0_name="D0ToHH_D0ToKmPip_LowBias")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2KPiDecision") # TODO


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kppim_lowbias_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPim_LowBias', prescale=1):
    kaons = make_charm_kaons_lowbias()
    pions = make_charm_pions_lowbias()
    dzeros = make_dzeros_lowbias(kaons, pions, '[D0 -> K+ pi-]cc')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=False,
        d0_name="D0ToHH_D0ToKpPim_LowBias")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2KPiDecision") # TODO


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2kmkp_lowbias_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKp_LowBias', prescale=1):
    kaons = make_charm_kaons_lowbias()
    dzeros = make_dzeros_lowbias(kaons, kaons, 'D0 -> K- K+')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=True,
        d0_name="D0ToHH_D0ToKmKp_LowBias")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2KKDecision") # TODO


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pimpip_lowbias_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPip_LowBias', prescale=1):
    pions = make_charm_pions_lowbias()
    dzeros = make_dzeros_lowbias(pions, pions, 'D0 -> pi- pi+')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=True,
        d0_name="D0ToHH_D0ToPimPip_LowBias")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )
    # hlt1_filter_code="Hlt1D2PiPiDecision") # TODO


################################################################################
# Semi-leptonic lines
################################################################################


def make_charm_pions_for_b():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
                F.PID_K < 0.,
                F.PT > 400 * MeV,
                F.P > 4 * GeV,
            ), ),
    )


def make_charm_kaons_for_b():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=9., Vertices=make_pvs()),
                F.PID_K > 0.,
                F.PT > 400 * MeV,
                F.P > 4 * GeV,
            ), ),
    )


def make_dzeros_for_b(particle1, particle2, descriptor):
    if descriptor == "[D0 -> K- pi+]cc":
        name = 'Charm_D0ToHH_SL_BuilderD0ToKmPip'
    elif descriptor == "D0 -> K- K+":
        name = 'Charm_D0ToHH_SL_BuilderD0ToKmKp'
    elif descriptor == "D0 -> pi- pi+":
        name = 'Charm_D0ToHH_SL_BuilderD0ToPimPip'
    elif descriptor == "[D0 -> K+ pi-]cc":
        name = 'Charm_D0ToHH_SL_BuilderD0ToKpPim'
    else:
        raise RuntimeError(
            "charm.b_to_d0mu_d0_to_hh.make_dzeros_for_b called with unsupported decay descriptor."
        )
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.MAX(F.PT) > 1200 * MeV,
            F.PT > 1 * GeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 200.,
        ),
    )


### Untagged D0 lines


@register_line_builder(all_lines)
@configurable
def dzero2kpi_for_b2d0mu_line(name='Hlt2Charm_D0ToKmPip_ForBToD0MuX',
                              prescale=0.1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K- pi+]cc')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def dzero2kk_for_b2d0mu_line(name='Hlt2Charm_D0ToKmKp_ForBToD0MuX',
                             prescale=0.1):
    kaons = make_charm_kaons_for_b()
    dzeros = make_dzeros_for_b(kaons, kaons, 'D0 -> K- K+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def dzero2pipi_for_b2d0mu_line(name='Hlt2Charm_D0ToPimPip_ForBToD0MuX',
                               prescale=0.1):
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(pions, pions, 'D0 -> pi- pi+')
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


### Tagged D*(2010)+ -> D0 pi+ lines


@register_line_builder(all_lines)
@configurable
def dstar2d0pi_dzero2kpi_for_b2d0mu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPip_ForBToD0MuX', prescale=0.1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K- pi+]cc')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKmPip',
        for_b=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def dstar2d0pi_dzero2kpi_ws_for_b2d0mu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKpPim_ForBToD0MuX', prescale=0.1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K+ pi-]cc')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKpPim',
        for_b=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def dstar2d0pi_dzero2kk_for_b2d0mu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKp_ForBToD0MuX', prescale=0.1):
    kaons = make_charm_kaons_for_b()
    dzeros = make_dzeros_for_b(kaons, kaons, 'D0 -> K- K+')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToKmKp',
        for_b=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def dstar2d0pi_dzero2pipi_for_b2d0mu_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPip_ForBToD0MuX', prescale=0.1):
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(pions, pions, 'D0 -> pi- pi+')
    dstars = make_dstars(
        dzeros,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToPimPip',
        for_b=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstars],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dstars, coneangle=0.5)
    )


### Tagged B -> D mu lines


@register_line_builder(all_lines)
@configurable
def b2d0mu_dzero2kpi_line(name='Hlt2Charm_BToD0MumX_D0ToKmPip', prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K- pi+]cc')
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2d0mu_dzero2kpi_ws_line(name='Hlt2Charm_BToD0MumX_D0ToKpPim', prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K+ pi-]cc')
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2d0mu_dzero2kk_line(name='Hlt2Charm_BToD0MumX_D0ToKmKp', prescale=1):
    kaons = make_charm_kaons_for_b()
    dzeros = make_dzeros_for_b(kaons, kaons, 'D0 -> K- K+')
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2d0mu_dzero2pipi_line(name='Hlt2Charm_BToD0MumX_D0ToPimPip', prescale=1):
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(pions, pions, descriptor='D0 -> pi- pi+')
    muons = make_tagging_muons()
    bs = make_sl_bs(dzeros, muons, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


### Tagged B -> D*(2010)+ mu- lines


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpi_line(name='Hlt2Charm_BToDstpMumX_D0ToKmPip',
                             prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K- pi+]cc')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKmPip',
        same_sign=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kpi_ws_line(name='Hlt2Charm_BToDstpMumX_D0ToKpPim',
                                prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K+ pi-]cc')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKpPim',
        same_sign=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2kk_line(name='Hlt2Charm_BToDstpMumX_D0ToKmKp', prescale=1):
    kaons = make_charm_kaons_for_b()
    dzeros = make_dzeros_for_b(kaons, kaons, 'D0 -> K- K+')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToKmKp',
        same_sign=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmu_dzero2pipi_line(name='Hlt2Charm_BToDstpMumX_D0ToPimPip',
                              prescale=1):
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(pions, pions, 'D0 -> pi- pi+')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToPimPip',
        same_sign=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


### Tagged B -> D*(2010)+ mu+ lines (for combinatorial bkg)


@register_line_builder(all_lines)
@configurable
def b2dstarmuWS_dzero2kpi_line(name='Hlt2Charm_BToDstpMupX_D0ToKmPip',
                               prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K- pi+]cc')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKmPip',
        same_sign=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmuWS_dzero2kpi_ws_line(name='Hlt2Charm_BToDstpMupX_D0ToKpPim',
                                  prescale=1):
    kaons = make_charm_kaons_for_b()
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(kaons, pions, '[D0 -> K+ pi-]cc')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=False,
        d0_name='D02HH_D0ToKpPim',
        same_sign=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmuWS_dzero2kk_line(name='Hlt2Charm_BToDstpMupX_D0ToKmKp',
                              prescale=1):
    kaons = make_charm_kaons_for_b()
    dzeros = make_dzeros_for_b(kaons, kaons, 'D0 -> K- K+')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToKmKp',
        same_sign=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )


@register_line_builder(all_lines)
@configurable
def b2dstarmuWS_dzero2pipi_line(name='Hlt2Charm_BToDstpMupX_D0ToPimPip',
                                prescale=1):
    pions = make_charm_pions_for_b()
    dzeros = make_dzeros_for_b(pions, pions, 'D0 -> pi- pi+')
    muons = make_tagging_muons()
    bs = make_dt_bs(
        dzeros,
        muons,
        self_conjugate_d0_decay=True,
        d0_name='D02HH_D0ToPimPip',
        same_sign=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [muons, dzeros, bs],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dzeros, coneangle=0.5)
    )

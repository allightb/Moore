###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Lines for Lambda_c decays to p h0, h0 = pi0, eta, eta', and L0 eta pi. Lambda_c is reconstructed from Lambda_b0 -> Lambda_c+ pi- decay.

    1. Lambda_b0 -> Lambda_c+ pi-, Lambda_c+ -> Lambda0 pi+ eta, eta -> pi+ pi- gamma(pi0):
        Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_DD and  Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_LL
        Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_DDR and Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_LLR

    2. Lambda_b0 -> Lambda_c+ pi-, Lambda_c+ -> p pi0
        Hlt2Charm_Lb0ToLcpPim_LcpToPPi0_R and Hlt2Charm_Lb0ToLcpPim_LcpToPPi0_M 

    3. Lambda_b0 -> Lambda_c+ pi-, Lambda_c+ -> p eta, eta -> pi+ pi- gamma(pi0):
        Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimG
        Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimPi0_R and Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimPi0_M

    4. Lambda_b0 -> Lambda_c+ pi-, Lambda_c+ -> p eta', eta' -> pi+ pi- eta(gamma)
        Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_R and Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_M 
        Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimG

where the daughter hadron is reconstructed via:
  Lambda0 -> p pi-  (LL/DD)
  Eta     -> pi+ pi- gamma(pi0)
  Eta'     -> pi+ pi- gamma(eta) 

Proponents: Xiao-Rui Lyu, Yangjie Su
TODO:
    add requirements on tracks chi2/ndf
    add HLT1 filtering
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps

import Functors as F
from Functors.math import in_range
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_down_pions, make_down_protons,
    make_has_rich_long_kaons, make_has_rich_long_pions, make_long_protons,
    make_has_rich_long_protons, make_ismuon_long_muon, make_photons,
    make_merged_pi0s)
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from .prefilters import charm_prefilters
from .particle_properties import _PI0_M

all_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


###############################################################################
# Track filters
###############################################################################


def _filter_long_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )


def _filter_long_pions_forlambdacppimu(pt=500):
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > pt * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )


def _filter_long_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K > 5.), ),
    )


def _filter_long_protons():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(4.),
                F.PID_P > 5.,
                F.PID_P - F.PID_K > 0.,
            ), ),
    )


def _filter_long_muons_loose():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(4.),
                F.PID_MU > 0.,
            ), ),
    )


def _filter_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(4.),
                F.PID_MU > 0.,
            ), ),
    )


def _filter_tight_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 1 * GeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_MU > 3.,
                #trghostprob_max=None, # TODO
            ), ),
    )


def _filter_long_pions_from_lambda():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            _MIPCHI2_MIN(36.),
        ), ),
    )


def _filter_long_protons_from_lambda():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                _MIPCHI2_MIN(6.),
                F.PID_P > 5.,
                F.PID_P - F.PID_K > 0.,
            ), ),
    )


def _filter_down_pions_from_lambda():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(F.require_all(
            F.PT > 100 * MeV,
            F.P > 1 * GeV,
        ), ),
    )


def _filter_down_protons_from_lambda():
    return ParticleFilter(
        make_down_protons(),
        F.FILTER(F.require_all(
            F.PT > 300 * MeV,
            F.P > 5 * GeV,
        ), ),
    )


def _filter_pions_from_etaprime():
    cut = F.require_all(
        _MIPCHI2_MIN(25.),
        F.PT > 1 * GeV,
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


###############################################################################
# Basic combiners
###############################################################################


def make_lambdall():
    """Make Lambda -> p+ pi- from long tracks."""
    protons = _filter_long_protons_from_lambda()
    pions = _filter_long_pions_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_CBaryonToPh_make_lambdall_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1160 * MeV,
            F.SUM(F.PT) > 450 * MeV,
            F.MAXSDOCACUT(0.2 * mm),
            F.MAXDOCACHI2CUT(16.),
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.END_VZ > -100 * mm,
            F.END_VZ < 500 * mm,
        ),
    )


def make_lambdadd():
    """Make Lambda -> p+ pi- from downstream tracks."""
    pions = _filter_down_pions_from_lambda()
    protons = _filter_down_protons_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_CBaryonToPh_make_lambdadd_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.SUM(F.PT) > 450 * MeV,
            F.MAXSDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(16.),
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.,
            F.END_VZ > 300 * mm,
            F.END_VZ < 2275 * mm,
        ),
    )


# Credit to authors of charm/d_to_etah.py
def _filter_pions_from_eta_loose():
    cut = F.require_all(
        _MIPCHI2_MIN(16.),
        F.PT > 350. * MeV,
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def _filter_neutrals_from_eta_loose(particles):
    """Particles might be photons or pi0s."""
    return ParticleFilter(particles, F.FILTER(F.PT > 350 * MeV))


def _make_photons_from_pi0s():
    return make_photons(PtCut=200 * MeV)


def _make_resolved_pi0s():
    photons = _make_photons_from_pi0s()
    combination_code = F.require_all(
        F.PT > 200 * MeV, in_range(_PI0_M - 30 * MeV, F.MASS,
                                   _PI0_M + 30 * MeV))
    composite_code = F.PT > 250 * MeV
    return ParticleCombiner(
        Inputs=[photons, photons],
        name="Charm_CBaryonToPh_Resolved_Pi0_{hash}",
        DecayDescriptor="pi0 -> gamma gamma",
        ParticleCombiner="ParticleAdder",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


# Credit to authors of charm/d_to_etah.py
def _make_pion_pairs_from_eta_loose():
    pions = _filter_pions_from_eta_loose()
    combination_code = F.require_all(
        in_range(180 * MeV, F.MASS, 770 * MeV),
        F.MAXSDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(210 * MeV, F.MASS, 750 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner(
        [pions, pions],
        DecayDescriptor="rho(770)0 -> pi+ pi-",
        name="Charm_CBaryonToPh_PionPairsFromEtaLoose_{hash}",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def _make_eta_to_pipigamma():
    combination_code = F.require_all(
        in_range(410 * MeV, F.MASS, 690 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    pion_pairs = _make_pion_pairs_from_eta_loose()
    photons = _filter_neutrals_from_eta_loose(make_photons())
    return ParticleCombiner([pion_pairs, photons],
                            DecayDescriptor="eta -> rho(770)0 gamma ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="Charm_CBayronToPh_EtaToPipPimG_{hash}")


def _make_eta_to_pipipi0_r():
    combination_code = F.require_all(
        in_range(420 * MeV, F.MASS, 680 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    pion_pairs = _make_pion_pairs_from_eta_loose()
    pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    return ParticleCombiner([pion_pairs, pi0s],
                            DecayDescriptor="eta -> rho(770)0 pi0 ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="Charm_CBaryonToPh_EtaToPipPimPi0_R_{hash}")


def _make_eta_to_pipipi0_m():
    combination_code = F.require_all(
        in_range(420 * MeV, F.MASS, 680 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    pion_pairs = _make_pion_pairs_from_eta_loose()
    pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    return ParticleCombiner([pion_pairs, pi0s],
                            DecayDescriptor="eta -> rho(770)0 pi0 ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="Charm_CBaryonToPh_EtaToPipPimPi0_M_{hash}")


def _make_pion_pairs_from_etaprime():
    pions = _filter_pions_from_etaprime()
    combination_code = F.require_all(
        in_range(170 * MeV, F.MASS, 1080 * MeV),
        F.MAXSDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(200 * MeV, F.MASS, 1050 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner(
        [pions, pions],
        DecayDescriptor="rho(770)0 -> pi+ pi-",
        name='CBaryonToPh_DToEtaH_PionPairsFromEtaPrime_{hash}',
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def _make_rho_from_etaprime():
    """Since eta' -> rho0 gamma dominates the eta' -> pi+pi- gamma rate,
    the cut m(pi+ pi-) > 600 MeV is effective in reducing the background,
    which has a substantial contribution at m(pi+ pi-) < 600 MeV.
    This is why we have two different combiners for pi+ pi- pairs coming from
    the eta' decay. The same cannot be done for eta' -> pi+ pi- eta.
    """
    pions = _filter_pions_from_etaprime()
    combination_code = F.require_all(
        in_range(570 * MeV, F.MASS, 1030 * MeV),
        F.MAXSDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(600 * MeV, F.MASS, 1000 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name='CBaryonToPh_DToEtaH_RhoFromEtaPrime_{hash}',
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_etaprime_to_pipieta_r():
    combination_code = F.require_all(
        in_range(750 * MeV, F.MASS, 1170 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(760 * MeV, F.MASS, 1160 * MeV))
    pion_pairs = _make_pion_pairs_from_etaprime()
    etas = _make_eta_to_pipipi0_r()
    return ParticleCombiner([pion_pairs, etas],
                            DecayDescriptor="eta_prime -> rho(770)0 eta ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="CBaryonToPh_EtaPrimeToPipPimEta_R_{hash}")


def _make_etaprime_to_pipieta_m():
    combination_code = F.require_all(
        in_range(750 * MeV, F.MASS, 1170 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(760 * MeV, F.MASS, 1160 * MeV))
    pion_pairs = _make_pion_pairs_from_etaprime()
    etas = _make_eta_to_pipipi0_m()
    return ParticleCombiner([pion_pairs, etas],
                            DecayDescriptor="eta_prime -> rho(770)0 eta ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="CBaryonToPh_EtaPrimeToPipPimEta_M_{hash}")


def _make_etaprime_to_pipigamma():
    combination_code = F.require_all(
        in_range(750 * MeV, F.MASS, 1170 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(760 * MeV, F.MASS, 1160 * MeV))
    pion_pairs = _make_rho_from_etaprime()
    photons = _filter_neutrals_from_eta_loose(make_photons())
    return ParticleCombiner([pion_pairs, photons],
                            DecayDescriptor="eta_prime -> rho(770)0 gamma ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="CBaryonToPh_EtaPrimeToPipPimEtaGamma_{hash}")


###############################################################################
# charm baryon combiners
###############################################################################
def make_lc_to_lambdaetapi(lambdas,
                           etas,
                           pions,
                           pvs,
                           name,
                           comb_pt_min=1.2 * GeV,
                           comb_sum_pt_min=1.8 * GeV,
                           comb_p_min=15 * GeV,
                           comb_m_min=1800 * MeV,
                           comb_m_max=2500 * MeV,
                           doca_max=2 * mm,
                           p_min=16 * GeV,
                           vdz_min=-1.0 * mm,
                           vchi2dof_max=15.,
                           dira_min=0.9):
    return ParticleCombiner([lambdas, etas, pions],
                            DecayDescriptor="[Lambda_c+ -> Lambda0 eta pi+]cc",
                            name=name,
                            CombinationCut=F.require_all(
                                F.PT > comb_pt_min,
                                F.SUM(F.PT) > comb_sum_pt_min,
                                F.P > comb_p_min,
                                in_range(comb_m_min, F.MASS, comb_m_max),
                                F.MAXDOCACUT(doca_max),
                            ),
                            CompositeCut=F.require_all(
                                F.P > p_min,
                                F.BPVVDZ(pvs) > vdz_min,
                                F.CHI2DOF < vchi2dof_max,
                                F.BPVDIRA(pvs) > dira_min,
                            ))


def make_lc_to_ph(protons,
                  hadrons,
                  pvs,
                  decay_descriptor,
                  name,
                  comb_pt_min=1.0 * GeV,
                  comb_sum_pt_min=1.8 * GeV,
                  comb_p_min=15 * GeV,
                  comb_m_min=1800 * MeV,
                  comb_m_max=2500 * MeV,
                  pt_min=1.1 * GeV,
                  Combiner="ParticleVertexFitter"):
    return ParticleCombiner([protons, hadrons],
                            DecayDescriptor=decay_descriptor,
                            name=name,
                            CombinationCut=F.require_all(
                                F.PT > comb_pt_min,
                                F.SUM(F.PT) > comb_sum_pt_min,
                                F.P > comb_p_min,
                                in_range(comb_m_min, F.MASS, comb_m_max),
                            ),
                            CompositeCut=F.require_all(F.PT > pt_min, ),
                            ParticleCombiner=Combiner)


def make_lb_to_lcpi(lcs,
                    pions,
                    pvs,
                    name,
                    comb_pt_min=1.2 * GeV,
                    comb_sum_pt_min=2.0 * GeV,
                    comb_p_min=20 * GeV,
                    comb_m_min=5200 * MeV,
                    comb_m_max=6200 * MeV,
                    doca_max=2 * mm,
                    pt_min=2 * GeV,
                    sum_pt_min=2.5 * GeV,
                    p_min=22 * GeV,
                    vchi2dof_max=10.,
                    dira_min=0.95):
    return ParticleCombiner([lcs, pions],
                            DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi-]cc",
                            name=name,
                            CombinationCut=F.require_all(
                                F.PT > comb_pt_min,
                                F.P > comb_p_min,
                                F.SUM(F.PT) > comb_sum_pt_min,
                                in_range(comb_m_min, F.MASS, comb_m_max),
                                F.MAXDOCACUT(doca_max),
                            ),
                            CompositeCut=F.require_all(
                                F.PT > pt_min,
                                F.P > p_min,
                                F.SUM(F.PT) > sum_pt_min,
                                F.CHI2DOF < vchi2dof_max,
                                F.BPVDIRA(pvs) > dira_min,
                            ))


###############################################################################
# Lines definitions
###############################################################################
@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimg_dd_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_DD",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    etas = _make_eta_to_pipigamma()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_DD_EtaToPipPimG_DD_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_DD_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimg_ll_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_LL",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    etas = _make_eta_to_pipigamma()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_LL_EtaToPipPimG_LL_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimG_LL_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimpi0_ddr_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_DDR",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    etas = _make_eta_to_pipipi0_r()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_DD_EtaToPipPimPi0_DDR_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_DDR_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimpi0_llr_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_LLR",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    etas = _make_eta_to_pipipi0_r()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_LL_EtaToPipPimPi0_LLR_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_LLR_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimpi0_ddm_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_DDM",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    etas = _make_eta_to_pipipi0_m()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_DD_EtaToPipPimPi0_DDM_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_DDM_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptol0etapip_etatopippimpi0_llm_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_LLM",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    etas = _make_eta_to_pipipi0_m()
    lcs = make_lc_to_lambdaetapi(
        lambdas,
        etas,
        pions,
        pvs,
        name="Charm_CBaryonToPh_LcpToLmdEtaPip_LL_EtaToPipPimPi0_LLM_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToL0EtaPip_EtaToPipPimPi0_LLM_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptoppi0_r_line(name="Hlt2Charm_Lb0ToLcpPim_LcpToPPi0_R",
                                 prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    lcs = make_lc_to_ph(
        protons,
        pi0s,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ pi0]cc",
        name="Charm_CBaryonToPh_LcpToPPi0_R_{hash}",
        Combiner="ParticleAdder")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPPi0_R_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptoppi0_m_line(name="Hlt2Charm_Lb0ToLcpPim_LcpToPPi0_M",
                                 prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    pi0s = _filter_neutrals_from_eta_loose(make_merged_pi0s())
    lcs = make_lc_to_ph(
        protons,
        pi0s,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ pi0]cc",
        name="Charm_CBaryonToPh_LcpToPPi0_M_{hash}",
        Combiner="ParticleAdder")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPPi0_M_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopeta_etatopippimg_r_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimG", prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etas = _make_eta_to_pipigamma()
    lcs = make_lc_to_ph(
        protons,
        etas,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta]cc",
        name="Charm_CBaryonToPh_LcpToPEta_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEta_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopeta_etatopippimpi0_r_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimPi0_R", prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etas = _make_eta_to_pipipi0_r()
    lcs = make_lc_to_ph(
        protons,
        etas,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta]cc",
        name="Charm_CBaryonToPh_LcpToPEta_R_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEta_R_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopeta_etatopippimpi0_m_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEta_EtaToPipPimPi0_M", prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etas = _make_eta_to_pipipi0_m()
    lcs = make_lc_to_ph(
        protons,
        etas,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta]cc",
        name="Charm_CBaryonToPh_LcpToPEta_M_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEta_M_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopetapr_etaprtopippimeta_etatopippimpi0_r_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_R",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etaprs = _make_etaprime_to_pipieta_r()
    lcs = make_lc_to_ph(
        protons,
        etaprs,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta_prime]cc",
        name=
        "Charm_CBaryonToPh_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_R_{hash}"
    )
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_R_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopetapr_etaprtopippimeta_etatopippimpi0_m_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_M",
        prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etaprs = _make_etaprime_to_pipieta_m()
    lcs = make_lc_to_ph(
        protons,
        etaprs,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta_prime]cc",
        name=
        "Charm_CBaryonToPh_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_M_{hash}"
    )
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name=
        "Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimEta_EtaToPipPimPi0_M_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)


@register_line_builder(all_lines)
def lb0tolcppim_lcptopetapr_etaprtopippimg_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimG", prescale=1):
    pvs = make_pvs()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    etaprs = _make_etaprime_to_pipigamma()
    lcs = make_lc_to_ph(
        protons,
        etaprs,
        pvs,
        decay_descriptor="[Lambda_c+ -> p+ eta_prime]cc",
        name="Charm_CBaryonToPh_LcpToPEtapr_EtaprToPipPimG_{hash}")
    lb0 = make_lb_to_lcpi(
        lcs,
        pions,
        pvs,
        name="Charm_CBaryonToPh_Lb0ToLcpPim_LcpToPEtapr_EtaprToPipPimG_{hash}",
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, lb0], prescale=prescale)

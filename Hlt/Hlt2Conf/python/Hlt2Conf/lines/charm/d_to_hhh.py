###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D+(s) -> h- h+ h+ HLT2 lines.

Final states built are (charge conjugates are implied):

 1. D(s)+ -> K- pi+ pi+:                        Hlt2Charm_DpDspToKmPipPip
 2. D(s)+ -> K- K+ pi+:                         Hlt2Charm_DpDspToKmKpPip
 3. D(s)+ -> pi- pi+ pi+:                       Hlt2Charm_DpDspToPimPipPip
 4. D(s)+ -> pi- pi+ K+:                        Hlt2Charm_DpDspToKpPimPip
 5. D(s)+ -> pi- K+ K+:                         Hlt2Charm_DpDspToKpKpPim
 6. D(s)+ -> K- K+ K+:                          Hlt2Charm_DpDspToKmKpKp
 7. B0 -> D- pi+, D- -> K- K+ pi-:              Hlt2Charm_B0ToDmPip_DmToKmKpPim
 8. B0 -> D- pi+, D- -> pi- pi- pi+:            Hlt2Charm_B0ToDmPip_DmToPimPimPip
 9. Bs0 -> D_s- pi+, D_s- -> K- K+ pi-:         Hlt2Charm_Bs0ToDsmPip_DsmToKmKpPim
10. Bs0 -> D_s- pi+, D_s- -> pi- pi- pi+:       Hlt2Charm_Bs0ToDsmPip_DsmToPimPimPip

TODO
----
Functor Wishlist:
  - ProbNN (only after data arrives)
  - CHI2DOF (to be tuned after data arrives)
Other:
  - Include Hlt1 TOS
  - Tune line for secondaries
  - Possibly implement MVA for D(s)+ -> 3pi line (to be discussed)
"""

import math

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mrad, picosecond, mm
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable
from Hlt2Conf.lines.charm.prefilters import charm_prefilters
#from . import charm_isolation as isolation

all_lines = {}


def _make_pions():
    """Return maker for pions filtered by thresholds common to charm decay
    product selections."""

    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.CHI2DOF < 99.,
                          F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                          F.PID_K < 5)))


def _make_tight_pions():
    """Return maker for pions filtered by thresholds common to charm decay
    product selections, but tighter pid requirement."""

    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.CHI2DOF < 99.,
                          F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                          F.PID_K < 1)))


def _make_kaons():
    """Return maker for kaons filtered by thresholds common to charm decay
    product selections."""

    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.CHI2DOF < 99.,
                          F.MINIPCHI2CUT(IPChi2Cut=4., Vertices=make_pvs()),
                          F.PID_K > 5)))


def _make_d_or_ds_to_hhh(particles, name, descriptor, with_cuts=True):
    """Return D(s)+- maker with selection tailored for three-body hadronic final states.

    Parameters
    ----------
    particles:
        List with maker algorithm instances for input particles.
    name:
        Name to help identify the combiner for given decay in the log.
    descriptors:
        String with the list of decay descriptors to be reconstructed.
    witht_cuts:
        Boolean to set if the selection includes IPCHI2 and DIRA requirements."""

    pvs = make_pvs()

    # Combination12Cut
    doca12_max = 0.1 * mm
    # CombinationCut
    comb_m_min = 1779 * MeV
    comb_m_max = 2059 * MeV
    max_pt_min = 1 * GeV
    trk_2of3_pt_min = 400 * MeV
    sum_pt_min = 3 * GeV
    max_mipchi2_min = 50.
    trk_2of3_mipchi2_min = 10.
    doca13_max = 0.1 * mm
    doca23_max = 0.1 * mm
    # CompositeCut
    m_min = 1789 * MeV
    m_max = 2049 * MeV
    vchi2dof_max = 9.
    bpvltime_min = 0.2 * picosecond
    bpvdira_min = math.cos(14.1 * mrad)
    bpvipchi2_max = 20.
    pt_min = 2.5 * GeV

    if 'K- K+ K+' in descriptor or 'K- K- K+' in descriptor:
        max_mipchi2_min = 10.
        trk_2of3_mipchi2_min = 4.

    if 'pi- pi+ pi+' in descriptor or 'pi- pi- pi+' in descriptor:
        sum_pt_min = 3.2 * GeV

    vertex_cuts = [
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2dof_max,
        F.BPVLTIME(pvs) > bpvltime_min, F.PT > pt_min
    ]

    if with_cuts:
        vertex_cuts.append(F.BPVDIRA(pvs) > bpvdira_min)
        vertex_cuts.append(F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=F.require_all(F.MAXDOCACUT(doca12_max)),
        CombinationCut=F.require_all(
            in_range(comb_m_min, F.MASS, comb_m_max),
            F.MAX(F.PT) > max_pt_min,
            F.SUM(F.PT > trk_2of3_pt_min) > 1,
            F.SUM(F.PT) > sum_pt_min,
            F.MAX(F.MINIPCHI2(pvs)) > max_mipchi2_min,
            F.SUM(F.MINIPCHI2(pvs) > trk_2of3_mipchi2_min) > 1,
            F.DOCA(1, 3) < doca13_max,
            F.DOCA(2, 3) < doca23_max),
        CompositeCut=F.require_all(*vertex_cuts))


def _make_d_to_hhh_for_secondaries(particles, name, descriptor):
    """Return D(s)+- maker with selection tailored for D(s)+- secondary decays into three-body hadronic final states.

    Parameters
    ----------
    particles:
        List with maker algorithm instances for input particles.
    name:
        Name to help identify the combiner for given decay in the log.
    descriptors:
        String with the list of decay descriptors to be reconstructed."""

    pvs = make_pvs()

    # CombinationCut
    comb_m_min = 1779 * MeV
    comb_m_max = 1959 * MeV
    max_pt_min = 1 * GeV
    trk_2of3_pt_min = 400 * MeV
    sum_pt_min = 3 * GeV
    max_mipchi2_min = 50.
    trk_2of3_mipchi2_min = 10.
    doca12_max = 0.1 * mm
    doca13_max = 0.1 * mm
    doca23_max = 0.1 * mm
    # CompositeCut
    m_min = 1789 * MeV
    m_max = 1949 * MeV
    vchi2dof_max = 9.
    bpvltime_min = 0.4 * picosecond
    pt_min = 2.5 * GeV

    if 'D_s' in descriptor:
        comb_m_min = 1879 * MeV
        comb_m_max = 2059 * MeV
        m_min = 1889 * MeV
        m_max = 2049 * MeV
        bpvltime_min = 0.2 * picosecond

    if 'K- K+ K+' in descriptor or 'K- K- K+' in descriptor:
        max_mipchi2_min = 10.
        trk_2of3_mipchi2_min = 4.

    if 'pi- pi+ pi+' in descriptor or 'pi- pi- pi+' in descriptor:
        sum_pt_min = 3.2 * GeV

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=F.require_all(F.MAXDOCACUT(doca12_max)),
        CombinationCut=F.require_all(
            in_range(comb_m_min, F.MASS, comb_m_max),
            F.MAX(F.PT) > max_pt_min,
            F.SUM(F.PT > trk_2of3_pt_min) > 1,
            F.SUM(F.PT) > sum_pt_min,
            F.MAX(F.MINIPCHI2(pvs)) > max_mipchi2_min,
            F.SUM(F.MINIPCHI2(pvs) > trk_2of3_mipchi2_min) > 1,
            F.DOCA(1, 3) < doca13_max,
            F.DOCA(2, 3) < doca23_max),
        CompositeCut=F.require_all(
            in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2dof_max,
            F.BPVLTIME(pvs) > bpvltime_min, F.PT > pt_min))


@register_line_builder(all_lines)
@configurable
def dplus2Kpipi_line(name='Hlt2Charm_DpDspToKmPipPip', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_pions(),
         _make_pions()],
        name='Charm_DToHHH_DpDspToKmPipPip_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2Kpipi_nocuts_line(name='Hlt2Charm_DpDspToKmPipPip_NoCuts',
                            prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_pions(),
         _make_pions()],
        name='Charm_DToHHH_DpDspToKmPipPip_NoCuts_{hash}',
        descriptor='[D+ -> K- pi+ pi+]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2KKpi_line(name='Hlt2Charm_DpDspToKmKpPip', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DpDspToKmKpPip_{hash}',
        descriptor='[D+ -> K- K+ pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2KKpi_nocuts_line(name='Hlt2Charm_DpDspToKmKpPip_NoCuts',
                           prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DpDspToKmKpPip_NoCuts_{hash}',
        descriptor='[D+ -> K- K+ pi+]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipipi_line(name='Hlt2Charm_DpDspToPimPipPip', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_tight_pions(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DpDspToPimPipPip_{hash}',
        descriptor='[D+ -> pi- pi+ pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipipi_nocuts_line(name='Hlt2Charm_DpDspToPimPipPip_NoCuts',
                             prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_tight_pions(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DpDspToPimPipPip_NoCuts_{hash}',
        descriptor='[D+ -> pi- pi+ pi+]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipiK_line(name='Hlt2Charm_DpDspToKpPimPip', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DpDspToKpPimPip_{hash}',
        descriptor='[D+ -> K+ pi- pi+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipiK_nocuts_line(name='Hlt2Charm_DpDspToKpPimPip_NoCuts',
                            prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DpDspToKpPimPip_NoCuts_{hash}',
        descriptor='[D+ -> K+ pi- pi+]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2KKK_line(name='Hlt2Charm_DpDspToKmKpKp', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_kaons()],
        name='Charm_DToHHH_DpDspToKmKpKp_{hash}',
        descriptor='[D+ -> K- K+ K+]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2KKK_nocuts_line(name='Hlt2Charm_DpDspToKmKpKp_NoCuts',
                          prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_kaons()],
        name='Charm_DToHHH_DpDspToKmKpKp_NoCuts_{hash}',
        descriptor='[D+ -> K- K+ K+]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2piKK_line(name='Hlt2Charm_DpDspToKpKpPim', prescale=1):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DpDspToKpKpPim_{hash}',
        descriptor='[D+ -> K+ K+ pi-]cc')

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def dplus2piKK_nocuts_line(name='Hlt2Charm_DpDspToKpKpPim_NoCuts',
                           prescale=0.05):

    dplus = _make_d_or_ds_to_hhh(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DpToKpKpPim_NoCuts_{hash}',
        descriptor='[D+ -> K+ K+ pi-]cc',
        with_cuts=False)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dplus, coneangle=0.5),
    )


"""Lines to study secondary decays: B0 -> D- pi+, B_s0 -> Ds- pi+"""


def _make_xb_to_xct(xc, t, descriptor):

    pvs = make_pvs()

    #Combination Cut
    comb_m_min = 5219 * MeV
    comb_m_max = 5339 * MeV
    m_min = 5229 * MeV
    m_max = 5329 * MeV
    if ("B_s0" in descriptor):
        comb_m_min = 5306 * MeV
        comb_m_max = 5426 * MeV
        m_min = 5316 * MeV
        m_max = 5416 * MeV

    return ParticleCombiner([xc, t],
                            name='Charm_DToHHH_XbToXcTrack_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=F.require_all(
                                in_range(comb_m_min, F.MASS, comb_m_max),
                                F.SUM(F.PT) > 6 * GeV,
                            ),
                            CompositeCut=F.require_all(
                                in_range(m_min, F.MASS, m_max),
                                F.BPVLTIME(pvs) > 0.4 * picosecond))


@register_line_builder(all_lines)
@configurable
def b02dminuspi_dminus2KKpi_line(name="Hlt2Charm_B0ToDmPip_DmToKmKpPim",
                                 prescale=1):

    dminus = _make_d_to_hhh_for_secondaries(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DmToKpKmPim_{hash}',
        descriptor='[D- -> K- K+ pi-]cc')

    b_tracks = _make_pions()
    b_to_dh = _make_xb_to_xct(dminus, b_tracks, "[B0 -> D- pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dminus, b_to_dh],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dminus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def b02dminuspi_dminus2pipipi_line(name="Hlt2Charm_B0ToDmPip_DmToPimPimPip",
                                   prescale=1):

    dminus = _make_d_to_hhh_for_secondaries(
        [_make_tight_pions(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DmToPimPimPip_{hash}',
        descriptor='[D- -> pi- pi- pi+]cc')

    b_tracks = _make_pions()
    b_to_dh = _make_xb_to_xct(dminus, b_tracks, "[B0 -> D- pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dminus, b_to_dh],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dminus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def bs02dsminuspi_dsminus2KKpi_line(name="Hlt2Charm_Bs0ToDsmPip_DsmToKmKpPim",
                                    prescale=1):

    dsminus = _make_d_to_hhh_for_secondaries(
        [_make_kaons(), _make_kaons(),
         _make_pions()],
        name='Charm_DToHHH_DsmToKpKmPim_{hash}',
        descriptor='[D_s- -> K- K+ pi-]cc')

    b_tracks = _make_pions()
    b_to_dh = _make_xb_to_xct(dsminus, b_tracks, "[B_s0 -> D_s- pi+]cc")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dsminus, b_to_dh],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dsminus, coneangle=0.5),
    )


@register_line_builder(all_lines)
@configurable
def bs02dsminuspi_dsminus2pipipi_line(
        name="Hlt2Charm_Bs0ToDsmPip_DsmToPimPimPip", prescale=1):

    dsminus = _make_d_to_hhh_for_secondaries(
        [_make_tight_pions(),
         _make_tight_pions(),
         _make_tight_pions()],
        name='Charm_DToHHH_DsmToPimPimPip_{hash}',
        descriptor='[D_s- -> pi- pi- pi+]cc')

    b_tracks = _make_pions()
    b_to_dh = _make_xb_to_xct(dsminus, b_tracks, "[B_s0 -> D_s- pi+]cc")

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dsminus, b_to_dh],
        prescale=prescale,
        #extra_outputs=isolation.make_iso_particles(dsminus, coneangle=0.5),
    )

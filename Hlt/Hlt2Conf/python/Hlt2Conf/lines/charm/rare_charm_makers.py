###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Rare Charm Makers that are used for the Rare charm HLT2 lines.
For the definition of the lines see rare_charm_lines.py

These are makers for HLT2 lines of rare charm decays with two leptons in the
final state and their (prescaled) hadronic control channels. The lines have
been transported from the Run1/2 to the new upgrade framework.

*******************************
Remarks:

- Dedicated Dz/Dp/Lc combiner exists for the modes with at least one electron in the final state. In principle, cuts on modes with electrons can be adjusted (for example, in Run1/2 there was a cut FDCHI2>49 in the dielectron modes wrt to FDCHI2>16 in the dimuon modes). At the moment, the cuts are aligned between electon and muon modes. In light of future LU tests, equal cuts are beneficial.

- Four-body and baryonic decays share the same preselection for the final state particles via make_selected_rarecharm_particles. The three-body decays have a tighter IP cut (tightIPCut) versions of the maker, but they had a slightly looser momentum daughter cut (p>2GeV) during Run1/2, which is now set to 3GeV. PID cuts in the selection may be revised (also for two-body decays).

- In order to avoid duplication of combinatorics of the charge-symmetric final state of the D0, only combinations labelled D0 are made. This leads to the second 'unphysical' decay descriptor for the D*-. See https://gitlab.cern.ch/lhcb/Moore/issues/64.

- before the four-body candidates are formed, intermediate two-particle objects are created. These are generically denoted as Jpsi, phi, rho. These have nothing to do with the physical meson states and there is no requirement on the dilepton mass. For same-sign lepton cominations, the two hadron-lepton combinations are allowed to be displaced. For this reason, the intermediate states are labelled with KS0 and K0L, ie. intermediate states with a finite lifetime (otherwise the vertex fit of the combiner requires the particles to come from the same vertex).
*******************************

TODO:
- add Track_Chi2 cuts
- move to ProbNN
"""
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, micrometer as um

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

from PyConf import configurable
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_down_pions, make_long_pions, make_has_rich_down_pions, make_has_rich_long_protons, make_has_rich_long_kaons, make_ismuon_long_muon, make_long_electrons_no_brem, make_long_protons, make_has_rich_down_protons, make_merged_pi0s, make_long_electrons_with_brem, make_photons
from Hlt2Conf.standard_particles import FunctionalDiElectronMaker
from Functors.math import in_range
import Functors as F
from .particle_properties import _PI0_M

_MONITORING_VARIABLES = ("pt", "eta", "vchi2", "ipchi2", "n_candidates")
_MONITORING_VARIABLES_MASS = ("pt", "eta", "vchi2", "ipchi2", "n_candidates",
                              "m", "dectime")
"""****************************************"""
"""Common cuts"""
"""****************************************"""
"""Cuts for all rare charm particles for multibody and baryonic decays. At the moment, p,pt cuts are shared among three and four body (+baryonic) decays. The two body lines have tighter cuts. IPCuts are tighter for three body and baryonic decays to contro, the rares. D*-tagged decays have a looser cut."""
"""****************************************"""
_TRACK_PT_MIN_XLL = 300.0 * MeV
_TRACK_P_MIN_XLL = 3000.0 * MeV
_TRACK_IPCHI2_MIN_XLL = 3.0  # D* tagged four-body decays
_TRACK_IPCHI2_MIN_XLL_TIGHT = 6.0  #!#6.0  ## (Run 2 cut was 5) three body decays (including lambda_c)
_TRACK_IPCHI2_MIN_PROTON_TIGHT = 9.0
_TRACK_CHI2_MAX = 4.0  #TODO Cut value is reasonable for Run 2, removed for the moment
_TRACK_GHOSTPROB_MAX = 0.25  #TODO Cut value has to be added for Run3
"""****************************************"""
"""lambda decay from Xic -> lambda mumu/ee"""
"""****************************************"""
_TRACK_PT_MAX_LONG_PI_FROMLAMBDA = 100 * MeV
_TRACK_IPCHI2_MIN_LONG_PI_FROMLAMBDA = 32.
_TRACK_PT_MIN_DOWN_PI_FROMLAMBDA = 180 * MeV
_TRACK_PT_MIN_DOWN_P_FROMLAMBDA = 700 * MeV
_TRACK_P_MIN_DOWN_P_FROMLAMBDA = 10 * GeV
_TRACK_PT_MIN_LONG_P_FROMLAMBDA = 400 * MeV
_TRACK_P_MIN_LONG_P_FROMLAMBDA = 9 * GeV
"""****************************************"""
"""Two body lines"""
"""****************************************"""
_TRACK_P_MIN_LL = 4000 * MeV
_TRACK_PT_MIN_LL = 750 * MeV
_TRACK_IPCHI2_MIN_LL = 3.0
_TRACK_IPCHI2_MIN_D2PL = 49.0  # tighter IP for D->pl
_TRACK_P_MIN_P_D2PL = 10 * GeV  # tighter P for protons from D->pl
_TRACK_IPCHI2_MIN_PI_FROMTAU = 49.0
"""****************************************"""
"""PID cuts"""
"""****************************************"""
_PID_MU = F.PID_MU > 0.0
_PID_E = F.PID_E > 3.0
_PID_K = F.PID_K > 0.0
_PID_PI = F.PID_K < 0.0
_PID_P = F.PID_P > 0.0
_PID_PI_TIGHT = F.PID_K < -10.0  #pi from tau decays
_PID_P_TIGHT = F.PID_P > 10.0  #for two body D->pl decays
_PID_E_TIGHT = F.PID_E > 8.0  #for two body D->pl decays
_PID_PI_FROMKS = F.PID_K < 0.0  #pions from Ks decays
_PID_P_FROMLAMBDA = F.PID_P > -5.0  #protons from lambda decays
"""****************************************"""
"""D->LL combiner"""
"""****************************************"""
_D_MAXCHILD_PT_MIN_LL = 1100 * MeV
_D_MAXCHILD_IPCHI2_MIN_LL = 8.0
_D_MASS_MIN_LL = 1565 * MeV
_D_MASS_MAX_LL = 2165 * MeV
_D_COMB_PT_MIN_LL = 1800. * MeV
_D_DOCA_MAX_LL = 0.1 * mm  #previous 1.0mm seems huge! 0.1 seems reasonable and was in stripping http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/charm/strippingdstard02xxdst2pid02mumubox.html, see also https://twiki.cern.ch/twiki/pub/LHCbPhysics/Dtomumu/main_v1.1.pdf (p100)
_D_VCHI2PDOF_MAX_LL = 9  # was 10 before
_D_DIRA_LL = 0.99996  ## was 0.9997 could be tighened, see below
_D_FDCHI2_MIN_LL = 20
_D_IPCHI2_MAX_LL = 10.0  ##15 before, in https://twiki.cern.ch/twiki/pub/LHCbPhysics/Dtomumu/main_v1.1.pdf looks like there was a cut at 10
"""****************************************"""
"""Pi0 and Ks combiner"""
"""****************************************"""
_RESOLVED_PI0_MASS_WINDOW = 30. * MeV
_RESOLVED_PI0_PT_MIN = 400. * MeV  # taken from https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2HH0/LHCb-ANA-2020-030_v3r0.pdf (p19)
_MERGED_PI0_MASS_WINDOW = 60. * MeV
_MERGED_PI0_PT_MIN = 2000. * MeV
_GAMMA_PT_MIN = 500 * MeV  # taken from https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2HH0/LHCb-ANA-2020-030_v3r0.pdf
_GAMMA_CL_MIN = 0.3
"""filter pi for LL KS"""
_TRACK_IPCHI2_MIN_PI_FROMKSLL = 36.0
"""filter pi for DD KS"""
_TRACK_PT_MIN_PI_FROMKSDD = 175 * MeV
_TRACK_P_MIN_PI_FROMKSDD = 3000 * MeV
"""LL KS"""
_KSLL_MASS_MIN = 460 * MeV
_KSLL_MASS_MAX = 535 * MeV
_KSLL_COMB_PT_MIN = 450 * MeV
_KSLL_VCHI2PDOF_MAX = 16.0
_KsLL_PT_MIN = 500 * MeV
_KSLL_DOCA_MAX = 150 * um
_KSLL_VZ_MIN = -100 * mm
_KSLL_VZ_MAX = 500 * mm
"""DD KS"""
_KSDD_MASS_MIN = 430 * MeV
_KSDD_MASS_MAX = 565 * MeV
_KSDD_VCHI2PDOF_MAX = 30.0
_KSDD_PT_MIN = 500 * MeV
_KSDD_COMB_PT_MIN = 450 * MeV
_KSDD_FDZ_MIN = 400 * mm
_KSDD_DOCA_MAX = 2 * mm
_KSDD_VZ_MIN = 300 * mm
_KSDD_VZ_MAX = 2275 * mm
"""****************************************"""
"""tau combiner"""
"""****************************************"""
_TAU_MAXCHILD_PT_MIN = 800 * MeV
_TAU_COMB_MASS_MIN = 400 * MeV
_TAU_COMB_MASS_MAX = 1900 * MeV
_TAU_DOCA_MAX = 0.05 * mm  #0.2 * mm
_TAU_MASS_MIN = 500 * MeV
_TAU_MASS_MAX = 2000 * MeV
_TAU_PT_MIN = 1200 * MeV
_TAU_DIRA_MIN = 0.999  #0.99
_TAU_FDCHI2_MIN = 120.0  #16
_TAU_VCHI2PDOF_MAX = 5  #16
_TAU_FDRHO_MIN = 0.1 * mm
_TAU_FDDRHO_MAX = 7.0 * mm
_TAU_FDZ_MIN = 5.0 * mm
_TAU_MAXCHILD_IPCHI2_MIN = 64.0
"""****************************************"""
"""D->taue combiner"""
"""****************************************"""
_D_MASS_MAX_TAUE = 1950 * MeV
_D_COMB_P_MIN_TAUE = 20000.0 * MeV
_D_DOCA_MAX_TAUE = 0.07 * mm
_D_VCHI2PDOF_MAX_TAUE = 3.0
_D_FD_MIN_TAUE = 4.0 * mm  #inspired by http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping21/charm/strippingcharmforvubprescaledhmunuline.html
_D_FDCHI2_MIN_TAUE = 100  #new
_D_FDZ_MIN_TAUE = 0.0 * mm
_D_PT_MIN_TAUE = 2000.0 * MeV
_TAU_VTXDISTCHI2_MIN_TAUE = 25.0
_D_CORRMASS_MIN_TAUE = 1400.0 * MeV
_D_CORRMASS_MAX_TAUE = 2700.0 * MeV
"""****************************************"""
"""Two muon combinations"""
"""****************************************"""
_PAIR_COMB_MASS_MAX_MUMU = 1900.0 * MeV
_PAIR_VCHI2PDOF_MAX_MUMU = 9.0
"""****************************************"""
"""Two electron combination"""
"""****************************************"""
_PAIR_COMB_MASS_MAX_EE = 2000.0 * MeV
_PAIR_VCHI2PDOF_MAX_EE = 9.0
"""****************************************"""
"""electron-muon combination"""
"""****************************************"""
_PAIR_COMB_MASS_MAX_EMU = 2000.0 * MeV
_PAIR_VCHI2PDOF_MAX_EMU = 9.0
"""****************************************"""
"""hadron-lepton combination"""
"""****************************************"""
_PAIR_DOCA_MAX_HL = 0.07 * mm  # tighter cut on the dimuon objects, as no DOCA cut on the D is placed
_PAIR_COMB_MASS_MAX_HL = 2000.0 * MeV
"""****************************************"""
"""Dzero combiner cuts for D0->HHLL"""
"""****************************************"""
_D_MAXCHILD_IPCHI2_MIN_HHLL = 9.0  # !!In stripping, one of the daughters has to fullfill MIPCHI2>9. Added this cut for now.
_D_MAXCHILD_PT_MIN_HHLL = 600.0 * MeV
_D_DOCA_MAX_HHLL = 0.15 * mm  #0.3 * mm might also be tighened to 0.15 https://twiki.cern.ch/twiki/pub/LHCbPhysics/D02HHMUMUANGULARANALYSIS/LHCb-ANA-2021-015_v1_1.pdf
_D_VCHI2PDOF_MAX_HHLL = 9.0  #was 15 might be possibly tightened. 6 would be fine in Run2 https://twiki.cern.ch/twiki/pub/LHCbPhysics/D02HHMUMUANGULARANALYSIS/LHCb-ANA-2021-015_v1_1.pdf
_D_IPCHI2_MAX_HHLL = 25.0
_D_IPCHI2_MAX_HHLL_SSLEPTONS = 9.0  ##tigher IP cut for forbidden mode
_D_COMB_PT_MIN_HHLL = 1800 * MeV  #In stripping Dpt>2000. Move to 2000 and apply to D object
_D_DIRA_MIN_HHLL = 0.99996  #was 0.9999, possible to tighten maybe cos(0.006) = 0.99998, see three body decays
_D_PT_MIN_HHLL = 2000 * MeV  #newly added, was present in the stripping
_D_MASS_MIN_HHLL = 1600.0 * MeV
_D_MASS_MAX_HHLL = 2150.0 * MeV
"""cuts that can be different between muon und electron modes"""
_D_FDCHI2_HHLL = 25.0  #was 16.0
_D_FDCHI2_HHEL = 25.0  #looks ok for the moment, was 49.0 in Run2
_D_FDCHI2_HHLL_FROMB = 9.0  #doubly tagged
"""****************************************"""
"""D combiner cuts for D+->HLL"""
"""****************************************"""
_D_DOCA_MAX_HLL = 0.1 * mm  #was 0.15, tightened to 0.1 https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2hll2016/LHCb-ANA-2018-027_v4.3_appendices.pdf.pdf (p52)
_D_MASS_MIN_HLL = 1500 * MeV  # changed from 1763.0 * MeV
_D_MASS_MAX_HLL = 2300 * MeV  # replace the window cut
_D_VCHI2PDOF_MAX_HLL = 5  #maybe also here <3 see https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2hll2016/LHCb-ANA-2018-027_v4.3_appendices.pdf.pdf (p56)
_D_IPCHI2_MAX_HLL = 25.0
_D_FDCHI2_MIN_HLL = 36.0
_D_FDCHI2_MIN_HLL_FROMB = 49.0  #changed from 100
_D_DIRA_MIN_HLL = 0.99996  # was 0.9999 possible to tighten maybe cos(0.006) = 0.99998, see three body decays   see https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2hll2016/LHCb-ANA-2018-027_v4.3_appendices.pdf.pdf (p56)
_D_DIRA_MIN_HLL_FROMB = 0.99
"""****************************************"""
"""D combiner cuts for D+->H0HLL"""
"""****************************************"""
_D_DOCA_MAX_HH0LL = 0.3 * mm  # TODO IMPORTANT: CHECK IF MAXDOCA makes sense for final states with neutrals! Removed for now
_D_MASS_MIN_HH0LL = 1500 * MeV
_D_MASS_MAX_HH0LL = 2300 * MeV
_D_VCHI2PDOF_MAX_HH0LL = 5  #maybe also here <3 see three body decays above
_D_IPCHI2_MAX_HH0LL = 25.0
_D_FDCHI2_MIN__HH0LL = 36.0
_D_DIRA_MIN_HH0LL = 0.9999  # possible to tighten maybe cos(0.006) = 0.99998, https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2hll2016/LHCb-ANA-2018-027_v4.3_appendices.pdf.pdf (p56)
"""****************************************"""
""" combiner cuts for lambdac"""
"""****************************************"""
_LAMBDAC_MAXCHILD_PT_MIN = 600.0 * MeV  #0.0 * MeV
_LAMBDAC_DOCA_MAX = 0.10 * mm  #0.25 was in trigger, 0.15 in stripping
_LAMBDAC_FDCHI2_MIN_FROMB = 25.0  #49.0
_LAMBDAC_VCHI2PDOF_MAX = 5  #20.0
_LAMBDAC_IPCHI2_MAX = 25.0  # maybe can be tighened see https://twiki.cern.ch/twiki/pub/LHCbPhysics/Lc2PMuMu/LHCb-ANA-2016-15_v15.pdf (p48), but careful since secondaries might be needed to be estimated later
_LAMBDAC_COMB_PT_MIN = 500.0 * MeV
_LAMBDAC_DIRA_MIN = 0.99996  #was 0.9999 possible to tighten maybe cos(0.006) = 0.99998, see three body decays   see https://twiki.cern.ch/twiki/pub/LHCbPhysics/D2hll2016/LHCb-ANA-2018-027_v4.3_appendices.pdf.pdf (p56)
_LAMBDAC_DIRA_MIN_FROMB = 0.99
_LAMBDAC_MASS_MIN = 1800.0 * MeV
_LAMBDAC_MASS_MAX = 2900.0 * MeV
'''lambdac -> Ksp'''
_LAMBDAC_DOCA_MAX_KSP = 0.2 * mm
_LAMBDAC_MASS_MIN_KSP = 2100.0 * MeV
_LAMBDAC_MASS_MAX_KSP = 2500.0 * MeV
"""cuts that can be different between muon und electron modes"""
_LAMBDAC_FDCHI2_MIN_MUMU = 6
_LAMBDAC_FDCHI2_MIN_EE = 6
"""****************************************"""
"""lambda to ppi"""
"""****************************************"""
_LAMBDA_MASS_MAX_PPI = 1165 * MeV
'''Long track Lambda '''
_LAMBDA_COMB_PT_MIN_PPI_LL = 500 * MeV
_LAMBDA_PT_MIN_PPI_LL = 600 * MeV
_LAMBDA_DOCA_MAX_PPI_LL = 100 * um
_LAMBDA_VCHI2PDOF_MAX_PPI_LL = 6.
_LAMBDA_FDZ_MIN_PPI_LL = 8 * mm
_LAMBDA_FDCHI2_MIN_PPI_LL = 120.
'''Downsteam track Lambda '''
_LAMBDA_COMB_PT_MIN_PPI_DD = 0.9 * GeV
_LAMBDA_PT_MIN_PPI_DD = 1 * GeV
_LAMBDA_SUMCHILD_PT_MIN_PPI_DD = 1 * GeV
_LAMBDA_COMB_P_MIN_PPI_DD = 13 * GeV
_LAMBDA_P_MIN_PPI_DD = 14 * GeV
_LAMBDA_DOCA_MAX_PPI_DD = 2 * mm
_LAMBDA_DOCACHI2_MAX_PPI_DD = 12.
_LAMBDA_VCHI2PDOF_MAX_PPI_DD = 9.
"""****************************************"""
"""Xic combiner cuts"""
"""****************************************"""
_XIC_MASS_MAX = 2570 * MeV
_XIC_MASS_MIN = 2380 * MeV
_XIC_COMB_PT_MIN = 0.9 * GeV
_XIC_PT_MIN = 1 * GeV
_XIC_SUMCHILD_PT_MIN = 2 * GeV
_XIC_COMB_P_MIN = 15 * GeV
_XIC_P_MIN = 16 * GeV
_XIC_FDZ_MIN = -0.5 * mm
_XIC_VCHI2PDOF_MAX = 6.
_XIC_FDCHI2_MIN = 4.
_XIC_IPCHI2_MAX = 9.
'''cuts different for LL and DD Lambda candidates'''
_XIC_LL_DOCA_MAX = 0.2 * mm
_XIC_DD_DOCA_MAX = 1.0 * mm
_XIC_LL_DIRA_MIN = 0.998
_XIC_DD_DIRA_MIN = 0.997
"""****************************************"""
"""Dstar combiner cuts"""
"""****************************************"""
_DSTAG_ADM_MIN = 120.0 * MeV
_DSTAG_DM_MIN = 120.0 * MeV
_DSTAG_ADM_MAX = 180.0 * MeV
_DSTAG_DM_MAX = 170.0 * MeV
_DSTAG_VCHI2PDOF_MAX = 16.0  #25.0  # probably could also be tightened, 10 or even 6
_DSTAG_SLOWPI_TRCHI2DOF_MAX = 5.0  #cuts on slowpi are not yet included
_DSTAG_SLOWPI_PT_MIN = 200 * MeV  # changed from 120 to 200
_DSTAG_SLOWPI_GOSTPROB_MAX = 0.25
"""****************************************"""
"""B-tag combiner cuts"""
"""****************************************"""
_BTAG_VCHI2PDOF_MAX = 9.0
_BTAG_MASS_MIN = 2300 * MeV
_BTAG_MASS_MAX = 10000 * MeV
_BTAG_DOCACHI2_MAX = 10.
_BTAG_CORRMASS_MIN = 2800 * MeV
_BTAG_CORRMASS_MAX = 8500 * MeV
_BTAG_DIRA_MIN = 0.999
_BTAG_DZ_MIN = -3 * mm
_BTAG_MUON_IPCHI2_MIN = 9.0
_BTAG_MUON_PT_MIN = 1000 * MeV
"""****************************************"""


@configurable
def make_selected_rarecharm_particles(make_particles=make_has_rich_long_pions,
                                      trchi2_max=_TRACK_CHI2_MAX,
                                      trghostprob=_TRACK_GHOSTPROB_MAX,
                                      mipchi2_min=_TRACK_IPCHI2_MIN_XLL,
                                      pt_min=_TRACK_PT_MIN_XLL,
                                      p_min=_TRACK_P_MIN_XLL,
                                      pid_cut=None,
                                      particleID=None,
                                      CloneFilteredParticles=False):
    """Return maker for particles filtered by thresholds common to rare decay charm decay product selections. Some cuts for the multibudy decays have to be looser compared to the "standard two-body charm selection" """

    pvs = make_pvs()
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        # TODO
        #F.GHOSTPROB < trghostprob,
        #F.CHI2 < trchi2_max,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs))

    if pid_cut is not None:
        code = F.require_all(code, pid_cut)

    if particleID is not None:
        code = F.require_all(code, particleID)

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_selected_rarecharm_slowpions(make_particles=make_long_pions,
                                      trchi2_max=_DSTAG_SLOWPI_TRCHI2DOF_MAX,
                                      trghostprob=_DSTAG_SLOWPI_GOSTPROB_MAX,
                                      pt_min=_DSTAG_SLOWPI_PT_MIN):
    """Return slow pions for rare charm decays"""
    code = F.require_all(
        F.PT > pt_min
        # TODO
        #F.CHI2 < trchi2_max,
        #F.GHOSTPROB < trghostprob
    )

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_rarecharm_tagging_muons(pid_cut=_PID_MU):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        mipchi2_min=_BTAG_MUON_IPCHI2_MIN,
        pt_min=_BTAG_MUON_PT_MIN)


@configurable
def make_rarecharm_pions(pid_cut=_PID_PI):
    """Kept the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return pions filtered by thresholds common to rare charm decay product selections."""
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_rarecharm_kaons(pid_cut=_PID_K):
    """left the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return kaons filtered by thresholds common to rare charm decay product selections."""
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_rarecharm_muons(pid_cut=_PID_MU):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon, pid_cut=pid_cut)


@configurable
def make_rarecharm_electrons_no_brem(pid_cut=_PID_E,
                                     particleID=None,
                                     CloneFilteredParticles=False):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem,
        pid_cut=pid_cut,
        particleID=particleID,
        CloneFilteredParticles=CloneFilteredParticles)


@configurable
def make_rarecharm_electrons_with_brem(pid_cut=_PID_E):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_with_brem, pid_cut=pid_cut)


@configurable
def make_rarecharm_protons(pid_cut=_PID_P):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_protons, pid_cut=pid_cut)


def get_positive_particles(particles):
    return ParticleFilter(particles, F.FILTER(F.CHARGE > 0))


def get_negative_particles(particles):
    return ParticleFilter(particles, F.FILTER(F.CHARGE < 0))


@configurable
def make_rarecharm_pions_tightIPCut(pid_cut=_PID_PI):
    """Kept the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return pions filtered by thresholds common to rare charm decay product selections."""
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


@configurable
def make_rarecharm_pions_from_tau(pid_cut=_PID_PI_TIGHT):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_PI_FROMTAU)


@configurable
def make_rarecharm_kaons_tightIPCut(pid_cut=_PID_K):
    """left the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return kaons filtered by thresholds common to rare charm decay product selections."""
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


@configurable
def make_rarecharm_muons_tightIPCut(pid_cut=_PID_MU):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


def make_rarecharm_muons_forD2pl(pid_cut=_PID_MU):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_D2PL)


@configurable
def make_rarecharm_electrons_no_brem_tightIPCut(pid_cut=_PID_E,
                                                particleID=None,
                                                CloneFilteredParticles=False):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT,
        particleID=particleID,
        CloneFilteredParticles=CloneFilteredParticles)


@configurable
def make_rarecharm_electrons_with_brem_tightIPCut(pid_cut=_PID_E):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_with_brem,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


@configurable
def make_rarecharm_electrons_with_brem_forD2pl(pid_cut=_PID_E_TIGHT):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_with_brem,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_D2PL)


@configurable
def make_rarecharm_protons_tightIPCut(pid_cut=_PID_P):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_protons,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_PROTON_TIGHT)


@configurable
def make_rarecharm_noPID_pions(pid_cut=None):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_rarecharm_noPID_pions_tightIPCut(pid_cut=None):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


@configurable
def make_rarecharm_noPID_kaons(pid_cut=None):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_rarecharm_down_pions_fromLambda(
        make_particles=make_down_pions,
        pt_min=_TRACK_PT_MIN_DOWN_PI_FROMLAMBDA):

    code = F.require_all(F.PT > pt_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_rarecharm_long_pions_fromLambda(
        make_particles=make_long_pions,
        pt_min=_TRACK_PT_MAX_LONG_PI_FROMLAMBDA,
        mipchi2_min=_TRACK_IPCHI2_MIN_LONG_PI_FROMLAMBDA):

    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min,
                         F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs))
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_rarecharm_down_protons_fromLambda(
        make_particles=make_has_rich_down_protons,
        pt_min=_TRACK_PT_MIN_DOWN_P_FROMLAMBDA,
        p_min=_TRACK_P_MIN_DOWN_P_FROMLAMBDA,
        pid_cut=_PID_P_FROMLAMBDA):

    code = F.require_all(F.PT > pt_min, F.P > p_min, pid_cut)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_rarecharm_long_protons_fromLambda(
        make_particles=make_has_rich_long_protons,
        pt_min=_TRACK_PT_MIN_LONG_P_FROMLAMBDA,
        p_min=_TRACK_P_MIN_LONG_P_FROMLAMBDA,
        pid_cut=_PID_P_FROMLAMBDA):

    code = F.require_all(F.PT > pt_min, F.P > p_min, pid_cut)
    return ParticleFilter(make_particles(), F.FILTER(code))


""" Below are filter for two body decays, which are tighter"""


@configurable
def make_rarecharm_pions_forTwoBodyDecays(pid_cut=_PID_PI):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_LL)


@configurable
def make_rarecharm_protons_forD2pl(pid_cut=_PID_P_TIGHT):
    return make_selected_rarecharm_particles(
        make_particles=make_long_protons,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_P_D2PL,
        mipchi2_min=_TRACK_IPCHI2_MIN_D2PL)


@configurable
def make_rarecharm_kaons_forTwoBodyDecays(pid_cut=_PID_K):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_LL)


@configurable
def make_rarecharm_muons_forTwoBodyDecays(pid_cut=_PID_MU):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_LL)


@configurable
def make_rarecharm_electrons_no_brem_forTwoBodyDecays(
        pid_cut=_PID_E, particleID=None, CloneFilteredParticles=False):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_LL,
        particleID=particleID,
        CloneFilteredParticles=CloneFilteredParticles)


def make_rarecharm_electrons_with_brem_forTwoBodyDecays(pid_cut=_PID_E):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_with_brem,
        pid_cut=pid_cut,
        pt_min=_TRACK_PT_MIN_LL,
        p_min=_TRACK_P_MIN_LL,
        mipchi2_min=_TRACK_IPCHI2_MIN_XLL_TIGHT)


""" neutral particles (Ks, pi0)"""


@configurable
def make_rarecharm_photons(photon_min_pt=_GAMMA_PT_MIN,
                           photon_min_cl=_GAMMA_CL_MIN):
    return make_photons(PtCut=photon_min_pt, ConfLevelCut=photon_min_cl)


@configurable
def make_rarecharm_resolved_pi0s(mass_window=_RESOLVED_PI0_MASS_WINDOW,
                                 pt_min=_RESOLVED_PI0_PT_MIN):

    photons = make_rarecharm_photons()
    combination_code = F.require_all(
        in_range(_PI0_M - _RESOLVED_PI0_MASS_WINDOW, F.MASS,
                 _PI0_M + _RESOLVED_PI0_MASS_WINDOW))
    composite_code = F.require_all(F.PT > pt_min)

    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[photons, photons],
        name='Charm_RareCharm_Resolved_Pi0_{hash}',
        DecayDescriptor='pi0 -> gamma gamma',
        CombinationCut=combination_code,
        CompositeCut=composite_code)


@configurable
def make_rarecharm_merged_pi0s(mass_window=_MERGED_PI0_MASS_WINDOW,
                               _PtCut=_MERGED_PI0_PT_MIN):
    return make_merged_pi0s(mass_window=mass_window, PtCut=_PtCut)


@configurable
def filter_long_pions_forKs(make_particles=make_has_rich_long_pions,
                            bpvipchi2_min=_TRACK_IPCHI2_MIN_PI_FROMKSLL,
                            pid_cut=_PID_PI_FROMKS):

    pvs = make_pvs()
    code = F.require_all(F.BPVIPCHI2(pvs) > bpvipchi2_min)

    if pid_cut is not None:
        code = F.require_all(code, pid_cut)

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def filter_down_pions_forKs(make_particles=make_has_rich_down_pions,
                            pt_min=_TRACK_PT_MIN_PI_FROMKSDD,
                            p_min=_TRACK_P_MIN_PI_FROMKSDD,
                            pid_cut=_PID_PI_FROMKS):

    code = F.require_all(F.PT > pt_min, F.P > p_min)

    if pid_cut is not None:
        code = F.require_all(code, pid_cut)

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_ll_ks(pion1,
               pion2,
               mmax=_KSLL_MASS_MAX,
               mmin=_KSLL_MASS_MIN,
               comb_pt_min=_KSLL_COMB_PT_MIN,
               doca_max=_KSLL_DOCA_MAX,
               vchi2pdof_max=_KSLL_VCHI2PDOF_MAX,
               vz_min=_KSLL_VZ_MIN,
               vz_max=_KSLL_VZ_MAX,
               pt_min=_KsLL_PT_MIN):
    """Returns maker for KS0 -> pi+ pi- constructed with two long pions."""
    combination_code = F.require_all(
        in_range(mmin - 15 * MeV, F.MASS, mmax + 15 * MeV),
        F.MAXDOCACUT(doca_max), F.PT > comb_pt_min)

    vertex_code = F.require_all(
        in_range(mmin, F.MASS, mmax), F.CHI2DOF < vchi2pdof_max, F.PT > pt_min,
        in_range(vz_min, F.END_VZ, vz_max))

    return ParticleCombiner([pion1, pion2],
                            name='Charm_RareCharm_KsLL_{hash}',
                            DecayDescriptor='KS0 -> pi+ pi-',
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_dd_ks(pion1,
               pion2,
               mmax=_KSDD_MASS_MAX,
               mmin=_KSDD_MASS_MIN,
               comb_pt_min=_KSDD_COMB_PT_MIN,
               doca_max=_KSDD_DOCA_MAX,
               vchi2pdof_max=_KSDD_VCHI2PDOF_MAX,
               vz_min=_KSDD_VZ_MIN,
               vz_max=_KSDD_VZ_MAX,
               pt_min=_KSDD_PT_MIN,
               bpvvdz_min=_KSDD_FDZ_MIN):
    """Returns maker for KS0 -> pi+ pi- constructed with two downstream pions."""

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(mmin - 15 * MeV, F.MASS, mmax + 15 * MeV),
        F.MAXDOCACUT(doca_max), F.PT > comb_pt_min)

    vertex_code = F.require_all(
        in_range(mmin, F.MASS, mmax), F.CHI2DOF < vchi2pdof_max, F.PT > pt_min,
        in_range(vz_min, F.END_VZ, vz_max),
        F.BPVVDZ(pvs) > bpvvdz_min)

    return ParticleCombiner([pion1, pion2],
                            name='Charm_RareCharm_KsDD_{hash}',
                            DecayDescriptor='KS0 -> pi+ pi-',
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


"""
Make two lepton combinations with very loose selection. Same cuts for ee and mumu combinations
"""


@configurable
def make_rarecharm_TwoMuons(particles,
                            descriptor,
                            amass=_PAIR_COMB_MASS_MAX_MUMU,
                            maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_MUMU):
    """Add a dimuon object (generically denoted as JPsi, however obviously no cut on the dimuon mass)
    to allow for cuts on the dimoun combination;
    PID and pt cuts could directly be applied in make_rarecharm_muons if needed. Set them to the same values at the moment.
    """
    combination_code = F.require_all(F.MASS < amass)
    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([particles, particles],
                            name='Charm_RareCharm_MuMuPair_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_TwoElectrons(particles,
                                DecayDescriptor,
                                amass=_PAIR_COMB_MASS_MAX_EE,
                                maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_EE):
    """Add a dielectron object (generically denoted as JPsi,Phi,Rho, however obviously no cut on the dielectron mass)
    to allow for cuts on the dielectron combination;
    PID and pt cuts could directly be applied in make_rarecharm_electrons if needed.
    """

    combination_code = F.require_all(F.MASS < amass)
    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([particles, particles],
                            name='Charm_RareCharm_EEPair_{hash}',
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_TwoElectrons_with_brem(
        electrons,
        diElectonID="J/psi(1S)",
        isOS=True,
        m_diE_min=0 * MeV,
        m_diE_max=_PAIR_COMB_MASS_MAX_EE,
        vfaspfchi2ndof=_PAIR_VCHI2PDOF_MAX_EE):
    """Make detached Jpsi -> e+ e- candidates adding bremsstrahlung correction to the
    electrons and avoid havinf the same brem added to both electrons. Select OS side charge electrain pairs by
    setting isOS = False. To have a specific charge combination filter on the input electrons."""

    dielectron_with_brem = FunctionalDiElectronMaker(
        InputParticles=electrons,
        MinDiElecMass=m_diE_min,
        MaxDiElecMass=m_diE_max,
        DiElecID=diElectonID,
        OppositeSign=isOS).Particles

    vertex_code = F.require_all(F.CHI2DOF < vfaspfchi2ndof)

    return ParticleFilter(dielectron_with_brem, F.FILTER(vertex_code))


@configurable
def make_rarecharm_ElectronMuon(electrons,
                                muons,
                                descriptor,
                                amass=_PAIR_COMB_MASS_MAX_EMU,
                                maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_EMU):
    """Add a muon-electron object (generically denoted as JPsi, however, obviously no cut on the dilepton mass)
    to allow for cuts on the dilepton combination;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_electrons if needed.
    """
    combination_code = F.require_all(F.MASS < amass)
    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([electrons, muons],
                            name='Charm_RareCharm_EMuPair_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_TwoPions(pion1,
                            pion2,
                            descriptor,
                            amass=_PAIR_COMB_MASS_MAX_MUMU,
                            maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_MUMU):
    """Add a dipion object (generically denoted as JPsi,Phi,Rho, however obviously no cut on the mass) for the HHpipi control modes
    to allow for cuts on the dimoun combination;
    PID and pt cuts could directly be applied in make_rarecharm_muons if needed. Cuts are set to the default values of the mumu combinations.
    """
    combination_code = F.require_all(F.MASS < amass)

    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([pion1, pion2],
                            name='Charm_RareCharm_PiPiPair_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_TwoPions_forThreeBodyDecay(
        particles,
        descriptor,
        amass=_PAIR_COMB_MASS_MAX_MUMU,
        maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_MUMU):
    """Add a dipion object (generically denoted as JPsi,Phi,Rho, however obviously no cut on the mass) for the Hpipi control modes
    to allow for cuts on the dimoun combination;
    PID and pt cuts could directly be applied in make_rarecharm_muons if needed. Cuts are set to the default values of the mumu combinations.
    """
    combination_code = F.require_all(F.MASS < amass)

    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([particles, particles],
                            name='Charm_RareCharm_PiPiPair_NoPID_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_displaced_HadronLepton(  # for LNV D->hhmumu with two seperated h-l vertices
        leptons,
        hadrons,
        descriptor,
        amass=_PAIR_COMB_MASS_MAX_HL,
        amaxdoca_max=_PAIR_DOCA_MAX_HL,
        maxVCHI2PDOF=_PAIR_VCHI2PDOF_MAX_MUMU):
    """Add a combined pi-muon pbject object (generically denoted as JPsi and Phi, depending on the charge combination, however, obviously no cut on the combination mass)
    to allow for cuts on the diparticle combination;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_pions if needed.
    """
    combination_code = F.require_all(F.MASS < amass,
                                     F.MAXDOCACUT(amaxdoca_max))
    vertex_code = F.require_all(F.CHI2DOF < maxVCHI2PDOF)

    return ParticleCombiner([leptons, hadrons],
                            name='Charm_RareCharm_Detached_HadLep_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_tau(particles,
                       descriptor,
                       trkipchi2_min=_TAU_MAXCHILD_IPCHI2_MIN,
                       amaxdoca_max=_TAU_DOCA_MAX,
                       am_min=_TAU_COMB_MASS_MIN,
                       am_max=_TAU_COMB_MASS_MAX,
                       mmin=_TAU_MASS_MIN,
                       mmax=_TAU_MASS_MAX,
                       apt_min=_TAU_PT_MIN,
                       pipt_min=_TAU_MAXCHILD_PT_MIN,
                       bpvvdrho_min=_TAU_FDRHO_MIN,
                       bpvvdrho_max=_TAU_FDDRHO_MAX,
                       bpvvdz=_TAU_FDZ_MIN,
                       vchi2pdof_max=_TAU_VCHI2PDOF_MAX,
                       bpvvdchi2_min=_TAU_FDCHI2_MIN,
                       bpvdira_min=_TAU_DIRA_MIN):
    """Return tau maker with selection."""

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS > am_min, F.MASS < am_max,
                                     F.MAXDOCACUT(amaxdoca_max),
                                     F.PT > apt_min,
                                     F.MAX(F.MINIPCHI2(pvs)) > trkipchi2_min,
                                     F.MAX(F.PT) > pipt_min)
    vertex_code = F.require_all(F.MASS > mmin, F.MASS < mmax,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min, F.PT > apt_min,
                                F.BPVVDZ(pvs) > bpvvdz,
                                F.BPVVDRHO(pvs) > bpvvdrho_min,
                                F.BPVVDRHO(pvs) < bpvvdrho_max)
    return ParticleCombiner([particles, particles, particles],
                            name='Charm_RareCharm_TauTo3Pi_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d02hhmumu_dzeros(dileptons,
                                    hadron1,
                                    hadron2,
                                    descriptor,
                                    m_min=_D_MASS_MIN_HHLL,
                                    m_max=_D_MASS_MAX_HHLL,
                                    amaxchild_pt_min=_D_MAXCHILD_PT_MIN_HHLL,
                                    apt_min=_D_COMB_PT_MIN_HHLL,
                                    amaxdoca_max=_D_DOCA_MAX_HHLL,
                                    vchi2pdof_max=_D_VCHI2PDOF_MAX_HHLL,
                                    bpvvdchi2_min=_D_FDCHI2_HHLL,
                                    bpvdira_min=_D_DIRA_MIN_HHLL,
                                    bpvipchi2_max=_D_IPCHI2_MAX_HHLL,
                                    trkipchi2_min=_D_MAXCHILD_IPCHI2_MIN_HHLL,
                                    dpt_min=_D_PT_MIN_HHLL):
    """Return D0 maker with selection tailored for four-body dimuon final states."""

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS > m_min - 100 * MeV, F.MASS < m_max + 100 * MeV,
        F.MAX(F.PT) > amaxchild_pt_min, F.PT > apt_min,
        F.MAXDOCACUT(amaxdoca_max),
        F.MAX(F.MINIPCHI2(pvs)) > trkipchi2_min)

    vertex_code = F.require_all(
        F.MASS > m_min, F.MASS < m_max, F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.PT > dpt_min)

    return ParticleCombiner([dileptons, hadron1, hadron2],
                            name='Charm_RareCharm_D0ToHHLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d02hhel_dzeros(dileptons, hadron1, hadron2, descriptor):
    """Return D0 maker with selection tailored for four-body semileptonic decays with at least one electron in the final state."""

    return make_rarecharm_d02hhmumu_dzeros(
        dileptons, hadron1, hadron2, descriptor, bpvvdchi2_min=_D_FDCHI2_HHEL)


@configurable
def make_rarecharm_d02hhmumu_dzeros_fromB(
        dileptons,
        hadron1,
        hadron2,
        descriptor,
        am_min=_D_MASS_MIN_HHLL,
        am_max=_D_MASS_MAX_HHLL,
        amaxdoca_max=_D_DOCA_MAX_HHLL,
        trkipchi2_min=_D_MAXCHILD_IPCHI2_MIN_HHLL,
        vchi2pdof_max=_D_VCHI2PDOF_MAX_HHLL,
        bpvvdchi2_min=_D_FDCHI2_HHLL_FROMB):
    """Return D0 maker with selection tailored for double tagged four body D decays from B"""

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS > am_min - 100 * MeV,
                                     F.MASS < am_max + 100 * MeV,
                                     F.MAXDOCACUT(amaxdoca_max),
                                     F.MAX(F.MINIPCHI2(pvs)) > trkipchi2_min)

    composite_code = F.require_all(F.MASS > am_min, F.MASS < am_max,
                                   F.CHI2DOF < vchi2pdof_max,
                                   F.BPVFDCHI2(pvs) > bpvvdchi2_min)

    return ParticleCombiner([dileptons, hadron1, hadron2],
                            name='Charm_RareCharm_D0ToHHLL_fromB_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


@configurable
def make_rarecharm_d02hhll_sslepton_dzeros(
        hadron_lepton1,
        hadron_lepton2,
        descriptor,
        am_min=_D_MASS_MIN_HHLL,
        am_max=_D_MASS_MAX_HHLL,
        amaxchild_pt_min=_D_MAXCHILD_PT_MIN_HHLL,
        apt_min=_D_COMB_PT_MIN_HHLL,
        vchi2pdof_max=_D_VCHI2PDOF_MAX_HHLL,
        bpvvdchi2_min=_D_FDCHI2_HHLL,
        bpvdira_min=_D_DIRA_MIN_HHLL,
        BPVIPCHI2=_D_IPCHI2_MAX_HHLL,
        bpvipchi2_max=_D_IPCHI2_MAX_HHLL_SSLEPTONS,
        trkipchi2_min=_D_MAXCHILD_IPCHI2_MIN_HHLL,
        dpt_min=_D_PT_MIN_HHLL):
    """Return D0 maker with selection tailored for four body D decays with SS lepton final states with no DOCA cut, ie possible displaced hadron-lepton pairs."""

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS > am_min - 100 * MeV, F.MASS < am_max + 100 * MeV,
        F.MAX(F.PT) > amaxchild_pt_min, F.PT > apt_min,
        F.MAX(F.MINIPCHI2(pvs)) > trkipchi2_min)

    vertex_code = F.require_all(
        F.MASS > am_min, F.MASS < am_max, F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max, F.PT > dpt_min)

    return ParticleCombiner([hadron_lepton1, hadron_lepton2],
                            name='Charm_RareCharm_D0ToHHLL_SSLeptons_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d02ll_dzeros(lepton1,
                                lepton2,
                                descriptor,
                                m_min=_D_MASS_MIN_LL,
                                m_max=_D_MASS_MAX_LL,
                                amaxchild_pt_min=_D_MAXCHILD_PT_MIN_LL,
                                apt_min=_D_COMB_PT_MIN_LL,
                                amaxdoca_max=_D_DOCA_MAX_LL,
                                vchi2pdof_max=_D_VCHI2PDOF_MAX_LL,
                                bpvvdchi2_min=_D_FDCHI2_MIN_LL,
                                bpvdira_min=_D_DIRA_LL,
                                bpvipchi2_max=_D_IPCHI2_MAX_LL,
                                maxIPChi2_min=_D_MAXCHILD_IPCHI2_MIN_LL):

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS > m_min - 50 * MeV, F.MASS < m_max + 50 * MeV,
        F.MAX(F.PT) > amaxchild_pt_min, F.PT > apt_min,
        F.MAXDOCACUT(amaxdoca_max),
        F.MAX(F.BPVIPCHI2(pvs)) > maxIPChi2_min)

    vertex_code = F.require_all(F.MASS > m_min, F.MASS < m_max,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner([lepton1, lepton2],
                            name='Charm_RareCharm_D0ToLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d02ee_dzeros(particles,
                                diElectonID="D0",
                                amass_min=_D_MASS_MIN_LL,
                                amass_max=_D_MASS_MAX_LL,
                                apt_min=_D_COMB_PT_MIN_LL,
                                vchi2pdof_max=_D_VCHI2PDOF_MAX_LL,
                                bpvvdchi2_min=_D_FDCHI2_MIN_LL,
                                bpvdira_min=_D_DIRA_LL,
                                bpvipchi2_max=_D_IPCHI2_MAX_LL,
                                maxIPChi2_min=_D_MAXCHILD_IPCHI2_MIN_LL):

    pvs = make_pvs()

    dielectron_with_brem = FunctionalDiElectronMaker(
        InputParticles=particles,
        MinDiElecMass=amass_min,
        MaxDiElecMass=amass_max,
        MinDiElecPT=apt_min,
        DiElecID=diElectonID,
    ).Particles
    #"INGENERATION(BPVIPCHI2() > {maxIPChi2_min}, 1)" used in make_rarecharm_d02ll_dzeros cannot be applied here

    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleFilter(dielectron_with_brem, F.FILTER(vertex_code))


@configurable
def make_rarecharm_d02taue_dzeros(taus,
                                  electrons,
                                  descriptor,
                                  amass_max=_D_MASS_MAX_TAUE,
                                  ap_min=_D_COMB_P_MIN_TAUE,
                                  amaxdoca_max=_D_DOCA_MAX_TAUE,
                                  vchi2pdof_max=_D_VCHI2PDOF_MAX_TAUE,
                                  bpvvd_min=_D_FD_MIN_TAUE,
                                  bpvvdchi2_min=_D_FDCHI2_MIN_TAUE,
                                  bpvvdz=_D_FDZ_MIN_TAUE,
                                  bpvcorrm_min=_D_CORRMASS_MIN_TAUE,
                                  bpvcorrm_max=_D_CORRMASS_MAX_TAUE,
                                  pt_min=_D_PT_MIN_TAUE,
                                  d2dvvdchi2_min=_TAU_VTXDISTCHI2_MIN_TAUE):

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS < amass_max + 100 * MeV,
                                     F.P > ap_min, F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(
        F.MASS < amass_max,
        F.CHI2DOF < vchi2pdof_max,
        #F.BPVFD(pvs) > bpvvd_min, #TODO missing
        F.BPVVDZ(pvs) > bpvvdz,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVCORRM(pvs) < bpvcorrm_max,
        F.BPVCORRM(pvs) > bpvcorrm_min,
        F.PT > pt_min,
        #"D2DVVDCHI2(2) > {d2dvvdchi2_min} TODO missing
    )

    return ParticleCombiner([taus, electrons],
                            name='Charm_RareCharm_D2TauE_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d2hll_ds(dilepton,
                            hadron,
                            descriptor,
                            m_min=_D_MASS_MIN_HLL,
                            m_max=_D_MASS_MAX_HLL,
                            amaxdoca_max=_D_DOCA_MAX_HLL,
                            vfaspf=_D_VCHI2PDOF_MAX_HLL,
                            bpvipchi2_max=_D_IPCHI2_MAX_HLL,
                            bpvvdchi2_min=_D_FDCHI2_MIN_HLL,
                            bpvdira_min=_D_DIRA_MIN_HLL):
    """Return D+/Ds maker with selection tailored for SL three-body final states."""

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MAXDOCACUT(amaxdoca_max),
        in_range(m_min - 100 * MeV, F.MASS, m_max + 100 * MeV))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vfaspf,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([dilepton, hadron],
                            name='Charm_RareCharm_DToHLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d2hll_ds_fromB(dilepton,
                                  hadron,
                                  descriptor,
                                  m_min=_D_MASS_MIN_HLL,
                                  m_max=_D_MASS_MAX_HLL,
                                  amaxdoca_max=_D_DOCA_MAX_HLL,
                                  vfaspf=_D_VCHI2PDOF_MAX_HLL,
                                  bpvvdchi2_min=_D_FDCHI2_MIN_HLL_FROMB,
                                  bpvdira_min=_D_DIRA_MIN_HLL_FROMB):
    """Return D+/Ds maker with selection tailored for SL three-body final states coming from B decays (no ID, large FDCHI2, loose Dira)"""

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MAXDOCACUT(amaxdoca_max),
        in_range(m_min - 100 * MeV, F.MASS, m_max + 100 * MeV))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vfaspf,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([dilepton, hadron],
                            name='Charm_RareCharm_D0ToHLL_fromB_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_d2hh0ll_ds(dilepton,
                              neutral,
                              hadron,
                              descriptor,
                              amaxdoca_max=_D_DOCA_MAX_HH0LL,
                              m_min=_D_MASS_MIN_HH0LL,
                              m_max=_D_MASS_MAX_HH0LL,
                              vfaspf=_D_VCHI2PDOF_MAX_HH0LL,
                              bpvipchi2_max=_D_IPCHI2_MAX_HH0LL,
                              bpvvdchi2_min=_D_FDCHI2_MIN__HH0LL,
                              bpvdira_min=_D_DIRA_MIN_HH0LL):
    """Return D+/Ds maker with selection tailored for final states with neutral hadrons."""

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(m_min - 50 * MeV, F.MASS,
                 m_max + 50 * MeV))  #, F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vfaspf,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([dilepton, neutral, hadron],
                            name='Charm_RareCharm_D0ToHH0LL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pmumu(dileptons,
                                  hadrons,
                                  descriptor,
                                  m_min=_LAMBDAC_MASS_MIN,
                                  m_max=_LAMBDAC_MASS_MAX,
                                  amaxchild_pt_min=_LAMBDAC_MAXCHILD_PT_MIN,
                                  apt_min=_LAMBDAC_COMB_PT_MIN,
                                  amaxdoca_max=_LAMBDAC_DOCA_MAX,
                                  vchi2pdof_max=_LAMBDAC_VCHI2PDOF_MAX,
                                  bpvvdchi2_min=_LAMBDAC_FDCHI2_MIN_MUMU,
                                  bpvdira_min=_LAMBDAC_DIRA_MIN,
                                  bpvipchi2_max=_LAMBDAC_IPCHI2_MAX):
    """Return lamdba maker decaying to proton and two leptons."""

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS > m_min - 50 * MeV,
        F.MASS < m_max + 50 * MeV,
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT) > apt_min,
        F.MAXDOCACUT(amaxdoca_max),
    )

    vertex_code = F.require_all(F.MASS > m_min, F.MASS < m_max,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner([dileptons, hadrons],
                            name='Charm_RareCharm_LcTopLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pll_fromB(
        dileptons,
        hadrons,
        descriptor,
        m_min=_LAMBDAC_MASS_MIN,
        m_max=_LAMBDAC_MASS_MAX,
        amaxchild_pt_min=_LAMBDAC_MAXCHILD_PT_MIN,
        apt_min=_LAMBDAC_COMB_PT_MIN,
        amaxdoca_max=_LAMBDAC_DOCA_MAX,
        vchi2pdof_max=_LAMBDAC_VCHI2PDOF_MAX,
        bpvvdchi2_min=_LAMBDAC_FDCHI2_MIN_FROMB,
        bpvdira_min=_LAMBDAC_DIRA_MIN_FROMB):
    """Return lamdba maker decaying to proton and two leptons from B (no IP, large FDCHI2, loose Dira)
    """

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS > m_min - 50 * MeV,
                                     F.MASS < m_max + 50 * MeV,
                                     F.MAX(F.PT) > amaxchild_pt_min,
                                     F.SUM(F.PT) > apt_min,
                                     F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(F.MASS > m_min, F.MASS < m_max,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([dileptons, hadrons],
                            name='Charm_RareCharm_LcTopLL_fromB_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pel(dileptons, hadrons, descriptor):
    """Return Lc maker with selection tailored for four-body semileptonic decays with at least one electron in the final state."""

    return make_rarecharm_lambdacs2pmumu(
        dileptons, hadrons, descriptor, bpvvdchi2_min=_LAMBDAC_FDCHI2_MIN_EE)


@configurable
def make_rarecharm_lambdacs2pks(protons,
                                Ks,
                                descriptor,
                                m_min=_LAMBDAC_MASS_MIN_KSP,
                                m_max=_LAMBDAC_MASS_MAX_KSP,
                                amaxchild_pt_min=_LAMBDAC_MAXCHILD_PT_MIN,
                                apt_min=_LAMBDAC_COMB_PT_MIN,
                                vchi2pdof_max=_LAMBDAC_VCHI2PDOF_MAX,
                                amaxdoca_max=_LAMBDAC_DOCA_MAX_KSP,
                                bpvvdchi2_min=_LAMBDAC_FDCHI2_MIN_MUMU,
                                bpvdira_min=_LAMBDAC_DIRA_MIN,
                                bpvipchi2_max=_LAMBDAC_IPCHI2_MAX):
    """Return lamdba maker decaying to proton and Ks. Remove the DOCA cuts wrt to the dimuon combiner  """

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS > m_min - 50 * MeV,
        F.MASS < m_max + 50 * MeV,
        F.MAX(F.PT) > amaxchild_pt_min,
        F.SUM(F.PT) > apt_min,
        F.MAXDOCACUT(amaxdoca_max),
    )

    vertex_code = F.require_all(F.MASS > m_min, F.MASS < m_max,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max)

    return ParticleCombiner([protons, Ks],
                            name='Charm_RareCharm_LcTopKs_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pks_fromB(
        protons,
        Ks,
        descriptor,
        m_min=_LAMBDAC_MASS_MIN_KSP,
        m_max=_LAMBDAC_MASS_MAX_KSP,
        amaxchild_pt_min=_LAMBDAC_MAXCHILD_PT_MIN,
        apt_min=_LAMBDAC_COMB_PT_MIN,
        vchi2pdof_max=_LAMBDAC_VCHI2PDOF_MAX,
        amaxdoca_max=_LAMBDAC_DOCA_MAX_KSP,
        bpvvdchi2_min=_LAMBDAC_FDCHI2_MIN_FROMB,
        bpvdira_min=_LAMBDAC_DIRA_MIN_FROMB,
        bpvipchi2_max=_LAMBDAC_IPCHI2_MAX):
    """Return lamdba maker decaying to proton and Ks. Relax the DOCA cuts wrt to the dimuon combiner. No IPCHI2 cut, tighter FDCHI2  """

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS > m_min - 50 * MeV,
                                     F.MASS < m_max + 50 * MeV,
                                     F.MAX(F.PT) > amaxchild_pt_min,
                                     F.SUM(F.PT) > apt_min,
                                     F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(F.MASS > m_min, F.MASS < m_max,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([protons, Ks],
                            name='Charm_RareCharm_LcTopKs_fromB_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_ll_xic2lambdall(lambdas,
                                   dilepton,
                                   descriptor,
                                   m_max=_XIC_MASS_MAX,
                                   m_min=_XIC_MASS_MIN,
                                   comb_pt_min=_XIC_COMB_PT_MIN,
                                   pt_min=_XIC_PT_MIN,
                                   sum_pt_min=_XIC_SUMCHILD_PT_MIN,
                                   comb_p_min=_XIC_COMB_P_MIN,
                                   p_min=_XIC_P_MIN,
                                   amaxdoca_max=_XIC_LL_DOCA_MAX,
                                   vchi2pdof_max=_XIC_VCHI2PDOF_MAX,
                                   bpvipchi2_max=_XIC_IPCHI2_MAX,
                                   bpvvdz_min=_XIC_FDZ_MIN,
                                   bpvvdchi2_min=_XIC_FDCHI2_MIN,
                                   bpvdira_min=_XIC_LL_DIRA_MIN):
    """Return Xic maker decaying to LL lambda and two leptons. """

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS < m_max + 35 * MeV, F.MASS > m_min - 35 * MeV,
        F.PT > comb_pt_min,
        F.SUM(F.PT) > sum_pt_min, F.P > comb_p_min, F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(F.MASS < m_max, F.MASS > m_min, F.PT > pt_min,
                                F.P > p_min, F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVVDZ(pvs) > bpvvdz_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max,
                                F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([lambdas, dilepton],
                            name='Charm_RareCharm_XicToLLLambdaLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_dd_xic2lambdall(lambdas,
                                   dilepton,
                                   descriptor,
                                   m_max=_XIC_MASS_MAX,
                                   m_min=_XIC_MASS_MIN,
                                   comb_pt_min=_XIC_COMB_PT_MIN,
                                   pt_min=_XIC_PT_MIN,
                                   sum_pt_min=_XIC_SUMCHILD_PT_MIN,
                                   comb_p_min=_XIC_COMB_P_MIN,
                                   p_min=_XIC_P_MIN,
                                   amaxdoca_max=_XIC_DD_DOCA_MAX,
                                   vchi2pdof_max=_XIC_VCHI2PDOF_MAX,
                                   bpvipchi2_max=_XIC_IPCHI2_MAX,
                                   bpvvdz_min=_XIC_FDZ_MIN,
                                   bpvvdchi2_min=_XIC_FDCHI2_MIN,
                                   bpvdira_min=_XIC_DD_DIRA_MIN):
    """Return Xic maker decaying to DD lambda and two leptons. """

    pvs = make_pvs()

    combination_code = F.require_all(
        F.MASS < m_max + 35 * MeV, F.MASS > m_min - 35 * MeV,
        F.PT > comb_pt_min,
        F.SUM(F.PT) > sum_pt_min, F.P > comb_p_min, F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(F.MASS < m_max, F.MASS > m_min, F.PT > pt_min,
                                F.P > p_min, F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVVDZ(pvs) > bpvvdz_min,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max,
                                F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner([lambdas, dilepton],
                            name='Charm_RareCharm_XicToDDLambdaLL_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_dd_lambda2ppi(protons,
                                 pions,
                                 descriptor,
                                 m_max=_LAMBDA_MASS_MAX_PPI,
                                 comb_pt_min=_LAMBDA_COMB_PT_MIN_PPI_DD,
                                 pt_min=_LAMBDA_PT_MIN_PPI_DD,
                                 sum_pt_min=_LAMBDA_SUMCHILD_PT_MIN_PPI_DD,
                                 comb_p_min=_LAMBDA_COMB_P_MIN_PPI_DD,
                                 p_min=_LAMBDA_P_MIN_PPI_DD,
                                 amaxdoca_max=_LAMBDA_DOCA_MAX_PPI_DD,
                                 docachi2_max=_LAMBDA_DOCACHI2_MAX_PPI_DD,
                                 vchi2pdof_max=_LAMBDA_VCHI2PDOF_MAX_PPI_DD):
    """Return lamdba maker decaying to DD proton and pion. """

    combination_code = F.require_all(
        F.MASS < m_max + 35 * MeV, F.PT > comb_pt_min,
        F.SUM(F.PT) > sum_pt_min, F.P > comb_p_min, F.MAXDOCACUT(amaxdoca_max),
        F.MAXDOCACHI2CUT(docachi2_max))

    vertex_code = F.require_all(F.MASS < m_max, F.PT > pt_min, F.P > p_min,
                                F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner([protons, pions],
                            name='Charm_RareCharm_LambdaDDTopPi_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_ll_lambda2ppi(protons,
                                 pions,
                                 descriptor,
                                 m_max=_LAMBDA_MASS_MAX_PPI,
                                 comb_pt_min=_LAMBDA_COMB_PT_MIN_PPI_LL,
                                 pt_min=_LAMBDA_PT_MIN_PPI_LL,
                                 amaxdoca_max=_LAMBDA_DOCA_MAX_PPI_LL,
                                 vchi2pdof_max=_LAMBDA_VCHI2PDOF_MAX_PPI_LL,
                                 bpvvdz_min=_LAMBDA_FDZ_MIN_PPI_LL,
                                 bpvvdchi2_min=_LAMBDA_FDCHI2_MIN_PPI_LL):
    """Return lamdba maker decaying to LL proton and pion. """

    pvs = make_pvs()

    combination_code = F.require_all(F.MASS < m_max + 35 * MeV,
                                     F.PT > comb_pt_min,
                                     F.MAXDOCACUT(amaxdoca_max))

    vertex_code = F.require_all(F.MASS < m_max, F.PT > pt_min,
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min,
                                F.BPVVDZ(pvs) > bpvvdz_min)

    return ParticleCombiner([protons, pions],
                            name='Charm_RareCharm_LambdaLLTopPi_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_dstars(dzeros,
                soft_pions,
                descriptor,
                adm_min=_DSTAG_ADM_MIN,
                adm_max=_DSTAG_ADM_MAX,
                dm_min=_DSTAG_DM_MIN,
                dm_max=_DSTAG_DM_MAX,
                vchi2pdof_max=_DSTAG_VCHI2PDOF_MAX):
    """Return D*+ maker for tagging a D0 with a soft pion."""

    combination_code = F.require_all(
        in_range(adm_min, (F.MASS - F.CHILD(1, F.MASS)), adm_max))
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        in_range(dm_min, (F.MASS - F.CHILD(1, F.MASS)), dm_max))
    return ParticleCombiner([dzeros, soft_pions],
                            name='Charm_RareCharm_DstToD0Pi_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_rarecharm_bs(dstar,
                      muons,
                      descriptor,
                      vchi2pdof_max=_BTAG_VCHI2PDOF_MAX,
                      comb_m_min=_BTAG_MASS_MIN,
                      comb_m_max=_BTAG_MASS_MAX,
                      adocachi2_max=_BTAG_DOCACHI2_MAX,
                      mcorr_min=_BTAG_CORRMASS_MIN,
                      mcorr_max=_BTAG_CORRMASS_MAX,
                      cos_bpvdira_min=_BTAG_DIRA_MIN,
                      b_d_dz_min=_BTAG_DZ_MIN):

    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(adocachi2_max))

    composite_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        in_range(mcorr_min, F.BPVCORRM(pvs), mcorr_max),
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > cos_bpvdira_min,
        F.CHILD(1, F.END_VZ) - F.END_VZ > b_d_dz_min)

    return ParticleCombiner([dstar, muons],
                            name='Charm_RareCharm_BToCharmMuX_{hash}',
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import ThOrParticleSelection
from Hlt2Conf.standard_particles import make_long_pions, make_up_pions, make_down_pions, make_ttrack_pions, make_photons, make_merged_pi0s
from Hlt2Conf.isolation import extra_outputs_for_isolation

import Functors as F
from Functors.math import in_range


@configurable
def select_parts_for_isolation(
        candidates=[],
        cut=F.require_all(in_range(0., F.DR2, 1.), ~F.SHARE_TRACKS()),
        LongTrackIso=True,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PizIso=False,
):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag 
    Args:
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. Be default: cone geometry with max dr2=1 and extra particles not signal
        LongTrackIso: Boolean value to make isolation with long tracks
        TTrackIso: Boolean value to make isolation with tt tracks
        DownstreamTrackIso: Boolean value to make isolation with downstream tracks
        UpstreamTrackIso: Boolean value to make isolation with upstream tracks
        NeutralIso: Boolean value to make isolation with neutral particles
        PizIso: Boolean value to make isolation with merged pi0 -> gamma gamma
    """
    extra_outputs = []
    for cand in candidates:
        if LongTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name="LongTrackIso",
                extra_particles=make_long_pions(),
                ref_particles=cand,
                selection=cut)
        if TTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name="TTrackIso",
                extra_particles=make_ttrack_pions(),
                ref_particles=cand,
                selection=cut)
        if DownstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name="DownstreamTrackIso",
                extra_particles=make_down_pions(),
                ref_particles=cand,
                selection=cut)
        if UpstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name="UpstreamTrackIso",
                extra_particles=make_up_pions(),
                ref_particles=cand,
                selection=cut)
        if NeutralIso:
            extra_outputs += extra_outputs_for_isolation(
                name="NeutralIso",
                extra_particles=make_photons(),
                ref_particles=cand,
                selection=cut)
        if PizIso:
            extra_outputs += extra_outputs_for_isolation(
                name="PizIso",
                extra_particles=make_merged_pi0s(),
                ref_particles=cand,
                selection=cut)

    return extra_outputs


@configurable
def make_iso_particles(input,
                       coneangle=2.,
                       LongTrackIso=True,
                       TTrackIso=False,
                       DownstreamTrackIso=False,
                       UpstreamTrackIso=False,
                       NeutralIso=True,
                       PizIso=False,
                       cone_for_each_track=False):

    candidate = input
    cut = F.require_all(in_range(0., F.DR2, coneangle), ~F.FIND_IN_TREE())

    if cone_for_each_track:
        code = F.GET_ALL_BASICS()
        candidate = ThOrParticleSelection(
            InputParticles=input, Functor=code).OutputSelection
        cut = F.require_all(in_range(0., F.DR2, coneangle), ~F.SHARE_TRACKS())

    iso_parts = select_parts_for_isolation(
        candidates=[candidate],
        cut=cut,
        LongTrackIso=LongTrackIso,
        TTrackIso=TTrackIso,
        DownstreamTrackIso=DownstreamTrackIso,
        UpstreamTrackIso=UpstreamTrackIso,
        NeutralIso=NeutralIso,
        PizIso=PizIso,
    )

    return iso_parts

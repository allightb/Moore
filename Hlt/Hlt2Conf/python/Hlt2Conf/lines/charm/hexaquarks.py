###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
# code inspired by cbaryon_spectroscopy module #

Set of HLT2 lines to search for both longlived (with detached vertex) and short-lived (decaying in PV) hexaquarks (+ selected pentaquarks).
Particles to search for with corresponding decay channels

==== Long-lived (weakly-decaying) ====

- Hs+[uds uud] : p+p+pi-
upper mass limit = 2053.95 MeV

- Hss0[uds uds] : p+Lambda0pi-
upper mass limit = 2231.36 MeV

- Hc+/++ [cud udd(u)] : p+p+K-pi+, p+p+K-pi+pi-, p+p+K0, p+p+K0pi-,
upper mass limit = 3224.73 MeV

- Hcs+ [csd uud] : p+p+K-K-pi+, p+Lambda0K-pi+, p+p+K0pi+
upper mass limit = 3402-3406 MeV

- Hcc++ [ccd uud] : Lc+p+K-pi+, Xic0p+pi+, Xic+p+, Lc+pK0, (D+p+p+K-, D0p+p+K-pi+, D+p+Lambda0) 
upper mass limit = 4560 MeV

- Ps+ [s~d uud] : ppi+pi-
upper mass limit = 1433.25 MeV

- Pc0 [c~d uud] : p+K+pi-pi-, p+K0pi-
upper mass limit = 2804.4 MeV

     + include various WS and similar samples for variety


=  + additional lines with PR for  
      * two protons
      * or Lambda0 + proton
    of same sign with detached vertex

==== Short-lived (strongly-decaying) ====

- Hs+[uds uud] : Lambda0 p

- Hss0[uds uds] : Lambda0 Lambda0

- Hc+/++ [cud udd(u)] : Lc+ p

- Hcs+ [csd uud] : Lc+ Lambda0, Xic0 p

- Hcc++ [ccd uud] : Lc+ Lc+, Xicc+ p

- Ps+ [s~d uud] : p K0

- Pc0 [c~d uud] : D- p

     + include various WS and similar samples for variety

[... have been put into ift/hlt2_ift_femtoscopy.py]

"""

import Functors as F
from Functors.math import in_range

from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond as ps, mm
from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter,
                                      ParticleContainersMerger)

from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)

from Hlt2Conf.lines.charm.prefilters import charm_prefilters
from Hlt2Conf.lines.charm.particle_properties import _PION_M

all_lines = {}

from Hlt2Conf.standard_particles import make_KsLL, make_KsDD, make_LambdaLL, make_LambdaDD

#### individual particle builders
# copy from  cbaryon_spectroscopy.py


@configurable
def _make_protons_detached(
        pt_min=200 * MeV,
        p_min=10 * GeV,
        mipchi2_min=4.0,
        dllp_min=5.0,  # not pi with ~3sigma
        #                           dllp_m_dllk_min=5.0): # initial
        dllp_m_dllk_min=0.):  # not K with ~2sigma
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min,
                                         Vertices=pvs), F.PID_P > dllp_min,
                          (F.PID_P - F.PID_K) > dllp_m_dllk_min)),
    )


@configurable
def _make_kaons_detached(
        pt_min=200 * MeV,
        p_min=5 * GeV,
        mipchi2_min=4.0,
        dllk_min=0.,
):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                          F.PID_K > dllk_min), ),
    )


@configurable
def _make_pions_detached(pt_min=200 * MeV,
                         p_min=2 * GeV,
                         mipchi2_min=4.0,
                         dllk_max=5.0):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                          F.PID_K < dllk_max), ),
    )


### builder for c-hadrons
_bpvdira_min = 0.995  # corresponds to cos(0.1)

from Hlt2Conf.lines.charm.prod_xsec import _xsec_make_dzeros_Kpi, _xsec_make_dplus_Kpipi, \
    _xsec_make_lc_or_xicp_pKpi, _xsec_make_xic0_pkkpi


@configurable
def make_Dz(no_pvdira_bias=True):
    if no_pvdira_bias: return _xsec_make_dzeros_Kpi(bpvdira_min=_bpvdira_min)
    else: return _xsec_make_dzeros_Kpi


@configurable
def make_Dp(no_pvdira_bias=True):
    if no_pvdira_bias: return _xsec_make_dplus_Kpipi(bpvdira_min=_bpvdira_min)
    else: return _xsec_make_dplus_Kpipi()


@configurable
def make_Lc(no_pvdira_bias=True):
    if no_pvdira_bias:
        return _xsec_make_lc_or_xicp_pKpi(
            m_min=2211.0 * MeV, m_max=2362.0 * MeV, bpvdira_min=_bpvdira_min)
    else:
        return _xsec_make_lc_or_xicp_pKpi(
            m_min=2211.0 * MeV, m_max=2362.0 * MeV)


@configurable
def make_Xicp(no_pvdira_bias=True):
    if no_pvdira_bias:
        return _xsec_make_lc_or_xicp_pKpi(
            m_min=2392.0 * MeV, m_max=2543.0 * MeV, bpvdira_min=_bpvdira_min)
    else:
        return _xsec_make_lc_or_xicp_pKpi(
            m_min=2392.0 * MeV, m_max=2543.0 * MeV)


@configurable
def make_Xicz(no_pvdira_bias=True):
    if no_pvdira_bias: return _xsec_make_xic0_pkkpi(bpvdira_min=_bpvdira_min)
    else: return _xsec_make_xic0_pkkpi()


###

from Hlt2Conf.lines.charm.ccbaryon_hadronic import _make_xicc_detached_child_kaons, \
    _make_xicc_detached_child_pions, \
    _make_xicc_prompt_child_kaons, \
    _make_xicc_prompt_child_pions, \
    _make_xicc_fourbody, \
    _make_xicc_threebody, \
    _make_lc


# direct copy from ccbaryon_hadronic
@configurable
def make_xiccpptolcpkmpippip_lcptoppkmpip():
    lcs = _make_lc()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    return _make_xicc_fourbody(
        [lcs, kaons, pions, pions],
        descriptor="[Xi_cc++ -> Lambda_c+ K- pi+ pi+]cc",
        name="XiccppToLcpKmPipPip")


@configurable
def make_xiccptolcpkmpip_lcptoppkmpip_line():
    lcs = _make_lc()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    return _make_xicc_threebody([lcs, kaons, pions],
                                descriptor="[Xi_cc+ -> Lambda_c+ K- pi+]cc",
                                name="XiccpToLcpKmPip")


##############################################################
########### template builders


@configurable
def _make_combination(
        particles,
        descriptor,
        name,
        #
        am_min=2201 * MeV,
        am_max=2372 * MeV,
        m_min=2211 * MeV,
        m_max=2362 * MeV,  # for Lambda_c+
        #
        sum_pt_min=1 * GeV,  # needs to be varied for different decays?
        doca_max=0.2 * mm,
        vxchi2ndof_max=9,
        comb_cut_add=None,
        pv_requirement=None,
):

    combination_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXDOCACUT(doca_max),
        F.SUM(F.PT) > sum_pt_min,
    )

    composite_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vxchi2ndof_max,
    )

    if comb_cut_add is not None:
        combination_cut &= comb_cut_add

    if pv_requirement is not None:
        composite_cut &= pv_requirement

    if len(particles) > 2:
        combination12_cut = F.MASS < am_max - (len(particles) - 2) * _PION_M

    if len(particles) > 3:
        combination123_cut = F.MASS < am_max - (len(particles) - 3) * _PION_M

    if len(particles) > 4:
        combination1234_cut = F.MASS < am_max - (len(particles) - 4) * _PION_M

    if len(particles) == 2:
        return ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=descriptor,
            name=name,
            AllowDiffInputsForSameIDChildren=True,
            CombinationCut=combination_cut,
            CompositeCut=composite_cut)

    elif len(particles) == 3:
        return ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=descriptor,
            name=name,
            CombinationCut=combination_cut,
            Combination12Cut=combination12_cut,
            CompositeCut=composite_cut)

    elif len(particles) == 4:
        return ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=descriptor,
            name=name,
            CombinationCut=combination_cut,
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            CompositeCut=composite_cut)

    elif len(particles) >= 5:
        return ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=descriptor,
            name=name,
            CombinationCut=combination_cut,
            Combination12Cut=combination12_cut,
            Combination123Cut=combination123_cut,
            Combination1234Cut=combination1234_cut,
            CompositeCut=composite_cut)


def make_combination(particles, descriptors, name, **decay_arguments):
    assert len(descriptors) > 0
    container = []
    for descriptor in descriptors:
        container.append(
            _make_combination(
                particles=particles,
                descriptor=descriptor,
                name=name,
                **decay_arguments))
    return ParticleContainersMerger(container, name=name)


@configurable
def make_combination_detached(
        name,
        particles,
        descriptors,
        #
        mipchi2_min_at_least_one=16.0,  # can be loosened in specific cases
        mipchi2_min_at_least_two=9.0,
        bpvipchi2_max=25,
        bpvltime_min=0.25 * ps,
        bpvfdchi2_min=49,
        comb_cut_add=None,
        **decay_arguments):

    pvs = make_pvs()
    pv_requirement = F.require_all(
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
    )

    _comb_cut_add = F.require_all(
        F.SUM(
            F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min_at_least_one,
                           Vertices=pvs)) >= 1,
        F.SUM(
            F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min_at_least_two, Vertices=pvs))
        >= 2)
    if comb_cut_add is not None:
        comb_cut_add &= _comb_cut_add
    else:
        comb_cut_add = _comb_cut_add

    return make_combination(
        particles,
        descriptors,
        name,
        comb_cut_add=comb_cut_add,
        pv_requirement=pv_requirement,
        **decay_arguments)


#######################################################################################
#######################################################################################
############################   Long-lived  channels  ##################################
#######################################################################################
#######################################################################################

prescale_ws = 1  # while testing, then 0.01-1

## Line builders
#------------ Hs

_Hs_instance = "Sigma(2030)+"
_Hs_m = 2054 * MeV


@configurable
def make_hs_to_pppi(name, descriptors):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, pions],
        am_min=0 * MeV,  # 2016 MeV is natural lower limit
        am_max=2090 * MeV,
        m_min=0 * MeV,
        m_max=2070 * MeV,
    )


@register_line_builder(all_lines)
def HsToPPPi_line(name="Hlt2Charm_HspToPpPpPim_RSWS", prescale=0.1):
    alg = make_hs_to_pppi(
        name="Charm_Hexa_HsToPPPi_{hash}",
        descriptors=[
            f"[{_Hs_instance} -> p+ p+ pi-]cc",
            f"[{_Hs_instance} -> p+ p~- pi-]cc",
            f"[{_Hs_instance} -> p+ p+ pi+]cc",
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Hss

_Hss_instance = "Xi(1950)0"
_Hss_m = 2231 * MeV


@configurable
def make_hss_to_lamppi(name, descriptors, make_lam):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    lam = make_lam()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lam, protons, pions],
        am_min=0 * MeV,  # 2193.5 MeV is natural lower limit
        am_max=2270 * MeV,
        m_min=0 * MeV,
        m_max=2250 * MeV,
    )


@register_line_builder(all_lines)
def HssToL0PPi_LL_line(name="Hlt2Charm_Hss0ToL0PpPim_LL_RSWS", prescale=1.0):
    alg = make_hss_to_lamppi(
        name="Charm_Hexa_HssToLamPPi_LL_{hash}",
        descriptors=[
            f"[{_Hss_instance} -> Lambda0 p+ pi-]cc",
            f"[{_Hss_instance} -> Lambda0 p+ pi+]cc",
            f"[{_Hss_instance} -> Lambda0 p~- pi-]cc"
        ],
        make_lam=make_LambdaLL,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HssToL0PPi_DD_line(name="Hlt2Charm_Hss0ToL0PpPim_DD_RSWS", prescale=1.0):
    alg = make_hss_to_lamppi(
        name="Charm_Hexa_HssToLamPPi_DD_{hash}",
        descriptors=[
            f"[{_Hss_instance} -> Lambda0 p+ pi-]cc",
            f"[{_Hss_instance} -> Lambda0 p+ pi+]cc",
            f"[{_Hss_instance} -> Lambda0 p~- pi-]cc"
        ],
        make_lam=make_LambdaDD,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Hc

_Hc_instance = "Sigma_c*+"
_Hc_m = 3225 * MeV
_Hc_m_min = _Hc_m - 300 * MeV
_Hc_m_max = _Hc_m + 100 * MeV
_Hc_am_min = _Hc_m - 320 * MeV
_Hc_am_max = _Hc_m + 120 * MeV


@configurable
def make_hc_to_ppKpi(name, descriptors):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, kaons, pions],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@register_line_builder(all_lines)
def HcToPPKPi_line(name="Hlt2Charm_HcppToPpPpKmPip_RSWS", prescale=0.1):
    alg = make_hc_to_ppKpi(
        name="Charm_Hexa_HcToPPKPi_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ K- pi+]cc",
            f"[{_Hc_instance} -> p+ p+ K- pi-]cc",
            f"[{_Hc_instance} -> p+ p~- K- pi+]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hc_to_ppKpipi(name, descriptors):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, kaons, pions, pions],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@register_line_builder(all_lines)
def HcToPPKPiPi_line(name="Hlt2Charm_HcpToPpPpKmPipPim_RSWS", prescale=0.1):
    alg = make_hc_to_ppKpipi(
        name="Charm_Hexa_HcToPPKPiPi_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ K- pi+ pi-]cc",
            f"[{_Hc_instance} -> p+ p+ K- pi+ pi+]cc",
            f"[{_Hc_instance} -> p+ p+ K- pi- pi-]cc",
            f"[{_Hc_instance} -> p+ p~- K- pi+ pi-]cc",
            f"[{_Hc_instance} -> p+ p~- K- pi+ pi+]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hc_to_ppKs(name, descriptors, make_ks):
    protons = _make_protons_detached()
    ks = make_ks()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, ks],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@configurable
def make_hc_to_ppKspi(name, descriptors, make_ks):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    ks = make_ks()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, ks, pions],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@register_line_builder(all_lines)
def HcToPPKs_LL_line(name="Hlt2Charm_HcppToPpPpKs_LL_RSWS", prescale=1.0):
    alg = make_hc_to_ppKs(
        name="Charm_Hexa_HcToPPKs_LL_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ KS0]cc",
            f"[{_Hc_instance} -> p+ p~- KS0]cc"
        ],
        make_ks=make_KsLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToPPKs_DD_line(name="Hlt2Charm_HcppToPpPpKs_DD_RSWS", prescale=1.0):
    alg = make_hc_to_ppKs(
        name="Charm_Hexa_HcToPPKs_DD_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ KS0]cc",
            f"[{_Hc_instance} -> p+ p~- KS0]cc"
        ],
        make_ks=make_KsDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToPPKsPi_LL_line(name="Hlt2Charm_HcpToPpPpKsPim_LL_RSWS", prescale=1.0):
    alg = make_hc_to_ppKspi(
        name="Charm_Hexa_HcToPPKspi_LL_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ KS0 pi-]cc",
            f"[{_Hc_instance} -> p+ p~- KS0 pi-]cc"
        ],
        make_ks=make_KsLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToPPKsPi_DD_line(name="Hlt2Charm_HcpToPpPpKsPim_DD_RSWS", prescale=1.0):
    alg = make_hc_to_ppKspi(
        name="Charm_Hexa_HcToPPKspi_DD_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> p+ p+ KS0 pi-]cc",
            f"[{_Hc_instance} -> p+ p~- KS0 pi-]cc"
        ],
        make_ks=make_KsDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hc_to_lamppi(name, descriptors, make_lam):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    lam = make_lam()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lam, protons, pions],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@configurable
def make_hc_to_lamppipi(name, descriptors, make_lam):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    lam = make_lam()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lam, protons, pions, pions],
        am_min=_Hc_am_min,
        am_max=_Hc_am_max,
        m_min=_Hc_m_min,
        m_max=_Hc_m_max,
    )


@register_line_builder(all_lines)
def HcToLamPPi_LL_line(name="Hlt2Charm_HcppToL0PpPip_LL_RSWS", prescale=1.0):
    alg = make_hc_to_lamppi(
        name="Charm_Hexa_HcToLamPPi_LL_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> Lambda0 p+ pi+]cc",
            f"[{_Hc_instance} -> Lambda0 p~- pi+]cc"
        ],
        make_lam=make_LambdaLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToLamPPi_DD_line(name="Hlt2Charm_HcppToL0PpPip_DD_RSWS", prescale=1.0):
    alg = make_hc_to_lamppi(
        name="Charm_Hexa_HcToLamPPi_DD_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> Lambda0 p+ pi+]cc",
            f"[{_Hc_instance} -> Lambda0 p~- pi+]cc"
        ],
        make_lam=make_LambdaDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToLamPPiPi_LL_line(name="Hlt2Charm_HcpToL0PpPipPim_LL_RSWS",
                         prescale=1.0):
    alg = make_hc_to_lamppipi(
        name="Charm_Hexa_HcToLamPPiPi_LL_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> Lambda0 p+ pi+ pi-]cc",
            f"[{_Hc_instance} -> Lambda0 p+ pi- pi-]cc",
            f"[{_Hc_instance} -> Lambda0 p~- pi+ pi-]cc"
        ],
        make_lam=make_LambdaLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcToLamPPiPi_DD_line(name="Hlt2Charm_HcpToL0PpPipPim_DD_RSWS",
                         prescale=1.0):
    alg = make_hc_to_lamppipi(
        name="Charm_Hexa_HcToLamPPiPi_DD_{hash}",
        descriptors=[
            f"[{_Hc_instance} -> Lambda0 p+ pi+ pi-]cc",
            f"[{_Hc_instance} -> Lambda0 p+ pi- pi-]cc",
            f"[{_Hc_instance} -> Lambda0 p~- pi+ pi-]cc"
        ],
        make_lam=make_LambdaDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Hcs

_Hcs_instance = "Sigma_c*+"
_Hcs_m = 3405 * MeV
_Hcs_m_min = _Hcs_m - 300 * MeV
_Hcs_m_max = _Hcs_m + 100 * MeV
_Hcs_am_min = _Hcs_m - 320 * MeV
_Hcs_am_max = _Hcs_m + 120 * MeV


@configurable
def make_hcs_to_ppKKpi(name, descriptors):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, kaons, kaons, pions],
        am_min=_Hcs_am_min,
        am_max=_Hcs_am_max,
        m_min=_Hcs_m_min,
        m_max=_Hcs_m_max,
    )


@register_line_builder(all_lines)
def HcsToPPKKPi_line(name="Hlt2Charm_HcspToPpPpKmKmPip_RSWS", prescale=1.0):
    alg = make_hcs_to_ppKKpi(
        name="Charm_Hexa_HcsToPPKKPi_{hash}",
        descriptors=[
            f"[{_Hcs_instance} -> p+ p+ K- K- pi+]cc",
            f"[{_Hcs_instance} -> p+ p+ K+ K- pi+]cc",
            f"[{_Hcs_instance} -> p+ p~- K- K- pi+]cc",
            f"[{_Hcs_instance} -> p+ p~- K+ K- pi+]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hcs_to_ppKKs(name, descriptors, make_ks):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    ks = make_ks()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons, kaons, ks],
        am_min=_Hcs_am_min,
        am_max=_Hcs_am_max,
        m_min=_Hcs_m_min,
        m_max=_Hcs_m_max,
    )


@register_line_builder(all_lines)
def HcsToPPKKs_LL_line(name="Hlt2Charm_HcspToPpPpKmKs_LL_RSWS", prescale=1.0):
    alg = make_hcs_to_ppKKs(
        name="Charm_Hexa_HcsToPPKKs_LL_{hash}",
        descriptors=[
            f"[{_Hcs_instance} -> p+ p+ K- KS0]cc",
            f"[{_Hcs_instance} -> p+ p~- K- KS0]cc"
        ],
        make_ks=make_KsLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcsToPPKKs_DD_line(name="Hlt2Charm_HcspToPpPpKmKs_DD_RSWS", prescale=1.0):
    alg = make_hcs_to_ppKKs(
        name="Charm_Hexa_HcsToPPKKs_DD_{hash}",
        descriptors=[
            f"[{_Hcs_instance} -> p+ p+ K- KS0]cc",
            f"[{_Hcs_instance} -> p+ p~- K- KS0]cc"
        ],
        make_ks=make_KsDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hcs_to_lampKpi(name, descriptors, make_lam):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    lam = make_lam()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lam, protons, kaons, pions],
        am_min=_Hcs_am_min,
        am_max=_Hcs_am_max,
        m_min=_Hcs_m_min,
        m_max=_Hcs_m_max,
    )


@register_line_builder(all_lines)
def HcsToLamPKPi_LL_line(name="Hlt2Charm_HcspToL0PpKmPip_LL_RSWS",
                         prescale=1.0):
    alg = make_hcs_to_lampKpi(
        name="Charm_Hexa_HcsToLamPKPi_LL_{hash}",
        descriptors=[
            f"[{_Hcs_instance} -> Lambda0 p+ K- pi+]cc",
            f"[{_Hcs_instance} -> Lambda0 p+ K+ pi-]cc",
            f"[{_Hcs_instance} -> Lambda0 p~- K- pi+]cc",
            f"[{_Hcs_instance} -> Lambda0 p~- K+ pi-]cc"
        ],
        make_lam=make_LambdaLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HcsToLamPKPi_DD_line(name="Hlt2Charm_HcspToL0PpKmPip_DD_RSWS",
                         prescale=1.0):
    alg = make_hcs_to_lampKpi(
        name="Charm_Hexa_HcsToLamPKPi_DD_{hash}",
        descriptors=[
            f"[{_Hcs_instance} -> Lambda0 p+ K- pi+]cc",
            f"[{_Hcs_instance} -> Lambda0 p+ K+ pi-]cc",
            f"[{_Hcs_instance} -> Lambda0 p~- K- pi+]cc",
            f"[{_Hcs_instance} -> Lambda0 p~- K+ pi-]cc"
        ],
        make_lam=make_LambdaDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Hcc

_Hcc_instance = "Xi*_cc+"
_Hcc_m = 4560 * MeV
_Hcc_m_min = _Hcc_m - 500 * MeV
_Hcc_m_max = _Hcc_m + 100 * MeV
_Hcc_am_min = _Hcc_m - 520 * MeV
_Hcc_am_max = _Hcc_m + 120 * MeV


@configurable
def make_hcc_to_LcpKpi(name, descriptors):
    lc = make_Lc()
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lc, protons, kaons, pions],
        am_min=_Hcc_am_min,
        am_max=_Hcc_am_max,
        m_min=_Hcc_m_min,
        m_max=_Hcc_m_max,
    )


@register_line_builder(all_lines)
def HccToLcPKPi_line(name="Hlt2Charm_HccppToLcpPpKmPip_RSWS", prescale=1.0):
    alg = make_hcc_to_LcpKpi(
        name="Charm_Hexa_HccToLcPKPi_{hash}",
        descriptors=[
            f"[{_Hcc_instance} -> Lambda_c+ p+ K- pi+]cc",
            f"[{_Hcc_instance} -> Lambda_c+ p+ K+ pi-]cc",
            f"[{_Hcc_instance} -> Lambda_c+ p~- K- pi+]cc",
            f"[{_Hcc_instance} -> Lambda_c+ p~- K+ pi-]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hcc_to_Xic0ppi(name, descriptors):
    xic = make_Xicz()
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[xic, protons, pions],
        am_min=_Hcc_am_min,
        am_max=_Hcc_am_max,
        m_min=_Hcc_m_min,
        m_max=_Hcc_m_max,
    )


@register_line_builder(all_lines)
def HccToXic0PPi_line(name="Hlt2Charm_HccppToXic0PpPip_RSWS", prescale=1.0):
    alg = make_hcc_to_Xic0ppi(
        name="Charm_Hexa_HccToXic0PPi_{hash}",
        descriptors=[
            f"[{_Hcc_instance} -> Xi_c0 p+ pi+]cc",
            f"[{_Hcc_instance} -> Xi_c0 p+ pi-]cc",
            f"[{_Hcc_instance} -> Xi_c0 p~- pi+]cc",
            f"[{_Hcc_instance} -> Xi_c0 p~- pi-]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hcc_to_Xicpp(name, descriptors):
    xic = make_Xicp()
    protons = _make_protons_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[xic, protons],
        am_min=_Hcc_am_min,
        am_max=_Hcc_am_max,
        m_min=_Hcc_m_min,
        m_max=_Hcc_m_max,
    )


@register_line_builder(all_lines)
def HccToXicpP_line(name="Hlt2Charm_HccppToXicpPp_RSWS", prescale=1.0):
    alg = make_hcc_to_Xicpp(
        name="Charm_Hexa_HccToXicpP_{hash}",
        descriptors=[
            f"[{_Hcc_instance} -> Xi_c+ p+]cc",
            f"[{_Hcc_instance} -> Xi_c+ p~-]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_hcc_to_dppK(name, descriptors):
    d = make_Dp()
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[d, protons, protons, kaons],
        am_min=_Hcc_am_min,
        am_max=_Hcc_am_max,
        m_min=_Hcc_m_min,
        m_max=_Hcc_m_max,
    )


@configurable
def make_hcc_to_dppKpi(name, descriptors):
    d = make_Dz()
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[d, protons, protons, kaons, pions],
        am_min=_Hcc_am_min,
        am_max=_Hcc_am_max,
        m_min=_Hcc_m_min,
        m_max=_Hcc_m_max,
    )


@register_line_builder(all_lines)
def HccToDpPPK_line(name="Hlt2Charm_HccppToDpPpPpKm_RSWS", prescale=1.0):
    alg = make_hcc_to_dppK(
        name="Charm_Hexa_HccToDpPPK_{hash}",
        descriptors=[
            f"[{_Hcc_instance} -> D+ p+ p+ K-]cc",
            f"[{_Hcc_instance} -> D+ p+ p~- K-]cc",
            f"[{_Hcc_instance} -> D+ p+ p~- K+]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def HccToD0PPKPi_line(name="Hlt2Charm_HccppToD0PpPpKmPip_RSWS", prescale=1.0):
    alg = make_hcc_to_dppKpi(
        name="Charm_Hexa_HccToD0PPKpi_{hash}",
        descriptors=[
            f"[{_Hcc_instance} -> D0 p+ p+ K- pi+]cc",
            f"[{_Hcc_instance} -> D0 p+ p+ K+ pi-]cc",
            f"[{_Hcc_instance} -> D0 p+ p~- K- pi+]cc",
            f"[{_Hcc_instance} -> D0 p+ p~- K+ pi+]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Ps [s~ udud]

_Ps_instance = "Sigma(2030)+"
_Ps_m = 1435 * MeV


@configurable
def make_ps_to_ppipi(name, descriptors):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, pions, pions],
        am_min=0 * MeV,  # 1216 MeV is natural lower limit
        am_max=1520 * MeV,
        m_min=0 * MeV,
        m_max=1500 * MeV,
    )


@register_line_builder(all_lines)
def PsToPPiPi_line(name="Hlt2Charm_PspToPpPipPim_RSWS", prescale=1.0):
    alg = make_ps_to_ppipi(
        name="Charm_Hexa_PsToPPiPi_{hash}",
        descriptors=[
            f"[{_Ps_instance} -> p+ pi+ pi-]cc",
            f"[{_Ps_instance} -> p+ pi+ pi+]cc",
            f"[{_Ps_instance} -> p+ pi- pi-]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#------------ Pc [c~ udud]

_Pc_instance = "Sigma_c*0"
_Pc_m = 2805 * MeV
_Pc_m_min = _Pc_m - 300 * MeV
_Pc_m_max = _Pc_m + 100 * MeV
_Pc_am_min = _Pc_m - 320 * MeV
_Pc_am_max = _Pc_m + 120 * MeV


@configurable
def make_pc_to_pKpipi(name, descriptors):
    protons = _make_protons_detached()
    kaons = _make_kaons_detached()
    pions = _make_pions_detached()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, kaons, pions, pions],
        am_min=_Pc_am_min,
        am_max=_Pc_am_max,
        m_min=_Pc_m_min,
        m_max=_Pc_m_max,
        sum_pt_min=2.5 * GeV,
    )


@register_line_builder(all_lines)
def PcToPKPiPi_line(name="Hlt2Charm_Pc0ToPpKpPimPim_RSWS", prescale=0.01):
    alg = make_pc_to_pKpipi(
        name="Charm_Hexa_PcToPKPiPi_{hash}",
        descriptors=[
            f"[{_Pc_instance} -> p+ K+ pi- pi-]cc",
            f"[{_Pc_instance} -> p+ K+ pi+ pi-]cc",
            f"[{_Pc_instance} -> p+ K- pi+ pi-]cc"
        ])
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@configurable
def make_pc_to_pKspi(name, descriptors, make_ks):
    protons = _make_protons_detached()
    pions = _make_pions_detached()
    ks = make_ks()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, ks, pions],
        am_min=_Pc_am_min,
        am_max=_Pc_am_max,
        m_min=_Pc_m_min,
        m_max=_Pc_m_max,
        sum_pt_min=2.5 * GeV,
    )


@register_line_builder(all_lines)
def PcToPKsPi_LL_line(name="Hlt2Charm_Pc0ToPpKsPim_LL_RSWS", prescale=0.1):
    alg = make_pc_to_pKspi(
        name="Charm_Hexa_PcToPKsPi_LL_{hash}",
        descriptors=[
            f"[{_Pc_instance} -> p+ KS0 pi-]cc",
            f"[{_Pc_instance} -> p+ KS0 pi+]cc"
        ],
        make_ks=make_KsLL)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


@register_line_builder(all_lines)
def PcToPKsPi_DD_line(name="Hlt2Charm_Pc0ToPpKsPim_DD_RSWS", prescale=0.1):
    alg = make_pc_to_pKspi(
        name="Charm_Hexa_PcToPKsPi_DD_{hash}",
        descriptors=[
            f"[{_Pc_instance} -> p+ KS0 pi-]cc",
            f"[{_Pc_instance} -> p+ KS0 pi+]cc"
        ],
        make_ks=make_KsDD)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [alg], prescale=prescale)


#################################
##  channels for PersistReco


@configurable
def make_detached_pp_loosePt(name, descriptors):
    protons = _make_protons_detached()
    comb_cut_add = F.SUM(F.PID_P - F.PID_K > 5.0) >= 1

    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons],
        am_min=0 * MeV,
        am_max=2320 * MeV,
        m_min=0 * MeV,
        m_max=2300 * MeV,  # +520 MeV from phasespace threshold
        sum_pt_min=1.0 * GeV,  # not changing
        bpvfdchi2_min=64,  # tighten
        vxchi2ndof_max=5,  # tighten
        bpvipchi2_max=100,  # loosen
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_detached_pp_tightPt(name, descriptors):
    protons = _make_protons_detached()
    comb_cut_add = F.SUM(F.PID_P - F.PID_K > 5.0) >= 1

    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[protons, protons],
        am_min=0 * MeV,
        am_max=2320 * MeV,
        m_min=0 * MeV,
        m_max=2300 * MeV,  # +520 MeV from phasespace threshold
        sum_pt_min=2.0 * GeV,  # tighten
        bpvfdchi2_min=64,  # tighten
        vxchi2ndof_max=5,  # tighten
        bpvipchi2_max=100,  # loosen
        comb_cut_add=comb_cut_add,
    )


@register_line_builder(all_lines)
def detachedPP_loosePT_PR_line(name="Hlt2Charm_DetachedPpPp_LoosePT_RSWS_PR",
                               prescale=0.001):
    alg = make_detached_pp_loosePt(
        name="Charm_Hexa_DetachedPP_LoosePT_PR_{hash}",
        descriptors=[
            f"[{_Hs_instance} -> p+ p+]cc", f"[{_Hs_instance} -> p+ p~-]cc"
        ])
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def detachedPP_tightPT_PR_line(name="Hlt2Charm_DetachedPpPp_TightPT_RSWS_PR",
                               prescale=0.01):
    alg = make_detached_pp_tightPt(
        name="Charm_Hexa_DetachedPP_TightPT_PR_{hash}",
        descriptors=[
            f"[{_Hs_instance} -> p+ p+]cc", f"[{_Hs_instance} -> p+ p~-]cc"
        ])
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [alg],
        prescale=prescale,
        persistreco=True)


##############


@configurable
def make_detached_lamp(name, descriptors, make_lam):
    protons = _make_protons_detached()
    lam = make_lam()
    return make_combination_detached(
        name=name,
        descriptors=descriptors,
        particles=[lam, protons],
        am_min=0 * MeV,
        am_max=2570 * MeV,
        m_min=0 * MeV,
        m_max=2550 * MeV,  # +500 MeV from phasespace threshold
        sum_pt_min=1.5 * GeV,
        bpvfdchi2_min=64,  # tighten
        vxchi2ndof_max=5,  # tighten
        bpvipchi2_max=100,  # loosen
    )


@register_line_builder(all_lines)
def detachedLamP_LL_PR_line(name="Hlt2Charm_DetachedL0Pp_LL_RSWS_PR",
                            prescale=0.01):
    alg = make_detached_lamp(
        name="Charm_Hexa_DetachedLamP_LL_PR_{hash}",
        descriptors=[
            f"[{_Hs_instance} -> Lambda0 p+]cc",
            f"[{_Hs_instance} -> Lambda0 p~-]cc"
        ],
        make_lam=make_LambdaLL)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [alg],
        prescale=prescale,
        persistreco=True)


@register_line_builder(all_lines)
def detachedLamP_DD_PR_line(name="Hlt2Charm_DetachedL0Pp_DD_RSWS_PR",
                            prescale=0.01):
    alg = make_detached_lamp(
        name="Charm_Hexa_DetachedLamP_DD_PR_{hash}",
        descriptors=[
            f"[{_Hs_instance} -> Lambda0 p+]cc",
            f"[{_Hs_instance} -> Lambda0 p~-]cc"
        ],
        make_lam=make_LambdaDD)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [alg],
        prescale=prescale,
        persistreco=True)


#######################################################################################
#######################################################################################
############################   Short-lived  channels  #################################
#######################################################################################
#######################################################################################
# ... continued in hexaquarks_prompt.py / i.e. in ift/hlt2_ift_femtoscopy.py

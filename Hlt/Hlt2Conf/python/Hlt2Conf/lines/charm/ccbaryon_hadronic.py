###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 lines for hadronic doubly charmed baryon decays.

Signal decay modes:
   "XiccpToLcpKmPip_Lcp2PpKmPip"             : Xi_cc+ -> Lambda_c+ K- pi+
   "XiccpToLcpPimPip_Lcp2PpKmPip"            : Xi_cc+ -> Lambda_c+ pi- pi+
   "XiccpToXicpPimPip_Xicp2PpKmPip"          : Xi_cc+ -> Lambda_c+ pi- pi+
   "XiccpToXicpKpPim_Xicp2PpKmPip"           : Xi_cc+ -> Lambda_c+ K+ pi-
   "XiccpToXic0Pip_Xic0ToPpKmKmPip"          : Xi_cc+ -> Xi_c0 pi+
   "XiccpToXic0Kp_Xic0ToPpKmKmPip"           : Xi_cc+ -> Xi_c0 K+
   "XiccpToDpPpKm_Dp2KmPipPip"               : Xi_cc+ -> D+ p+ K-
   "XiccpToDpPpPim_Dp2KmPipPip"              : Xi_cc+ -> D+ p+ pi-
   "XiccpToD0PpKmPip_D02KmPip"               : Xi_cc+ -> D0 p+ K- pi+
   "XiccpToD0Pp_D02KmPip"                    : Xi_cc+ -> D0 p+
   "XiccpToDspPpKm_Dsp2KmKpPip"              : Xi_cc+ -> D_s+ p+ K-
   "XiccppToLcpKmPipPip_Lcp2PpKmPip"         : Xi_cc++ -> Lambda_c+ K- pi+ pi+
   "XiccppToLcpPip_Lcp2PpKmPip"              : Xi_cc++ -> Lambda_c+ pi+
   "XiccppToXicpPip_Xicp2PpKmPip"            : Xi_cc++ -> Lambda_c+ pi+
   "XiccppToXicpKp_Xicp2PpKmPip"             : Xi_cc++ -> Lambda_c+ K+
   "XiccppToXic0PipPip_Xic0ToPpKmKmPip"      : Xi_cc++ -> Xi_c0 pi+ pi+
   "XiccppToXic0KpPip_Xic0ToPpKmKmPip"       : Xi_cc++ -> Xi_c0 K+ pi+
   "XiccppToOmegacKpKp_Omegac0ToPpKmKmPip"  : Xi_cc++ -> Xi_c0 K+ K+
   "XiccppToDpPpKmPip_Dp2KmPipPip"           : Xi_cc++ -> D+ p+ K- pi+
   "XiccppToDpPp_Dp2KmPipPip"                : Xi_cc++ -> D+ p+
   "XiccppToD0PpKmPipPip_D02KmPip"           : Xi_cc++ -> D0 p+ K- pi+ pi+
   "XiccppToD0PpPip_D02KmPip"                : Xi_cc++ -> D0 p+ pi+
   "XiccppToDspPpKmPip_Dsp2KmKpPip"          : Xi_cc++ -> D_s+ p+ K- pi+
   "OmegaccToLcpKmKmPipPip_Lcp2PpKmPip"      : Omega_cc+ -> Lambda_c+ K- K- pi+ pi+
   "OmegaccToXicpKmPip_Xicp2PpKmPip"         : Omega_cc+ -> Lambda_c+ K- pi+
   "OmegaccToXic0KmPipPip_Xic0ToPpKmKmPip"   : Omega_cc+ -> Xi_c0 K- pi+ pi+
   "OmegaccToOmegacPip_OmegacToPpKmKmPip"    : Omega_cc+ -> Xi_c0 pi+
   "OmegaccToDpPpKmKmPip_Dp2KmPipPip"        : Omega_cc+ -> D+ p+ K- K- pi+

Wrong-sign combinations for background studies:
   "Xiccp2LcpKmPim_Lcp2PpKmPip"             : Xi_cc+ -> Lambda_c+ K- pi-
   "Xiccp2XicpPimPim_Xicp2PpKmPip"          : Xi_cc+ -> Lambda_c+ pi- pi-
   "Xiccp2Xic0Pim_Xic0ToPpKmKmPip"          : Xi_cc+ -> Xi_c0 pi-
   "Xiccp2DpPpKp_Dp2KmPipPip"               : Xi_cc+ -> D+ p+ K+
   "Xiccp2D0PpKmPim_D02KmPip"               : Xi_cc+ -> D0 p+ K- pi-
   "Xiccpp2LcpKmPimPip_Lcp2PpKmPip"         : Xi_cc++ -> Lambda_c+ K- pi- pi+
   "Xiccpp2LcpPim_Lcp2PpKmPip"              : Xi_cc++ -> Lambda_c+ pi-
   "Xiccpp2XicpPim_Xicp2PpKmPip"            : Xi_cc++ -> Lambda_c+ pi-
   "Xiccpp2Xic0PimPip_Xic0ToPpKmKmPip"      : Xi_cc++ -> Xi_c0 pi- pi+
   "Xiccpp2DpPpKmPim_Dp2KmPipPip"           : Xi_cc++ -> D+ p+ K- pi-
   "Xiccpp2DpPpKpPip_Dp2KmPipPip"           : Xi_cc++ -> D+ p+ K+ pi+
   "Xiccpp2D0PpKmPimPip_D02KmPip"           : Xi_cc++ -> D0 p+ K- pi- pi+
   "Omegacc2XicpKmPim_Xicp2PpKmPip"         : Omega_cc+ -> Lambda_c+ K- pi-
   "OmegaccToOmegacPim_OmegacToPpKmKmPip"   : Omega_cc+ -> Xi_c0 pi-

Doubly-Cabibbo-suppressed decay modes for background studies:
   "Xiccp2LcpKpPim_Lcp2PpKmPip"             : Xi_cc+ -> Lambda_c+ K+ pi-
   "Xiccp2D0PpKpPim_D02KmPip"               : Xi_cc+ -> D0 p+ K+ pi-
   "Xiccpp2LcpKpPimPip_Lcp2PpKmPip"         : Xi_cc++ -> Lambda_c+ K+ pi- pi+
   "Xiccpp2D0PpKpPimPip_D02KmPip"           : Xi_cc++ -> D0 p+ K+ pi- pi+
"""
import math

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, mrad, picosecond)

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)

from .prefilters import charm_prefilters
from .particle_properties import _D0_M, _DP_M, _DS_M, _LC_M, _XICP_M, _XICZ_M, _OMEGAC_M


# -------------------
# final-state filters
# -------------------
def _filter_particles(
        input_particles,
        chi2dof_max=1e6,  # placeholder
        ghostprob_max=1e6,  # placeholder
        pt_min=200 * MeV,
        p_min=1 * GeV):
    """Basic track filters."""
    cut = F.require_all(F.CHI2DOF < chi2dof_max, F.GHOSTPROB < ghostprob_max,
                        F.PT > pt_min, F.P > p_min)
    return ParticleFilter(input_particles, F.FILTER(cut))


def _filter_detached_particles(input_particles, ipchi2_min=6.0):
    """Filter detached tracks."""
    pvs = make_pvs()
    cut = F.MINIPCHI2(pvs) > ipchi2_min
    return ParticleFilter(input_particles, F.FILTER(cut))


def _filter_protons(input_particles, dllpi_min=5.0, dllk_min=None):
    """Filter particles consistent with a proton."""
    cut = F.PID_P > dllpi_min
    if dllk_min is not None:
        cut = F.require_all(cut, F.PID_P - F.PID_K > dllk_min)
    return ParticleFilter(input_particles, F.FILTER(cut))


def _filter_kaons(input_particles, dllpi_min=5.0):
    """Filter particles consistent with a Kaon."""
    return ParticleFilter(input_particles, F.FILTER(F.PID_K > dllpi_min))


def _filter_pions(input_particles, dllk_min=-5):
    """Filter particles consistent with a Pion."""
    return ParticleFilter(input_particles, F.FILTER(F.PID_K < -dllk_min))


def _make_xicc_prompt_child_protons():
    """Make protons directly from Xicc, assuming a short Xicc lifetime."""
    basic_protons = make_has_rich_long_protons()
    return _filter_protons(_filter_particles(basic_protons))


def _make_xicc_detached_child_protons():
    """Make protons directly from Xicc, assuming a long Xicc lifetime."""
    basic_protons = make_has_rich_long_protons()
    return _filter_protons(
        _filter_particles(basic_protons, pt_min=500 * MeV), dllk_min=10.0)


def _make_xicc_prompt_child_kaons():
    """Make Kaons directly from Xicc, assuming a short Xicc lifetime."""
    basic_kaons = make_has_rich_long_kaons()
    return _filter_kaons(_filter_particles(basic_kaons))


def _make_xicc_detached_child_kaons():
    """Make Kaons directly from Xicc, assuming a long Xicc lifetime."""
    basic_kaons = make_has_rich_long_kaons()
    return _filter_kaons(
        _filter_particles(basic_kaons, pt_min=500 * MeV), dllpi_min=10.0)


def _make_xicc_prompt_child_pions():
    """Make Pions directly from Xicc, assuming a short Xicc lifetime."""
    basic_pions = make_has_rich_long_pions()
    return _filter_pions(_filter_particles(basic_pions))


def _make_xicc_detached_child_pions():
    """Make Pions directly from Xicc, assuming a long Xicc lifetime."""
    basic_pions = make_has_rich_long_pions()
    return _filter_pions(
        _filter_detached_particles(
            _filter_particles(basic_pions), ipchi2_min=1.0),
        dllk_min=0.0)


# -------------------------------
# singly-charmed-hadron combiners
# -------------------------------
def _make_hc_twobody(particles,
                     descriptor,
                     name,
                     am_min=1715.0 * MeV,
                     am_max=2015.0 * MeV,
                     m_min=1735.0 * MeV,
                     m_max=1995.0 * MeV,
                     pt_min=1000 * MeV,
                     doca_max=0.1 * mm,
                     chi2dof_max=10,
                     bpvfdchi2_min=25.0,
                     bpvacosdira_max=17.3 * mrad):
    """
    Make two-body charmed hadron decays.
    """
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.MAXDOCACUT(doca_max))
    pvs = make_pvs()
    vtx_cut = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < chi2dof_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > math.cos(bpvacosdira_max))
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "DCBHcTwoBody", name]),
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


def _make_hc_threebody(particles,
                       descriptor,
                       name,
                       am_min=2211.0 * MeV,
                       am_max=2543.0 * MeV,
                       m_min=2221.0 * MeV,
                       m_max=2533.0 * MeV,
                       sumpt_min=3000 * MeV,
                       trk_1of3_pt_min=1000.0 * MeV,
                       trk_2of3_pt_min=400.0 * MeV,
                       trk_1of3_ipchi2_min=16.0,
                       trk_2of3_ipchi2_min=9.0,
                       doca12_max=0.1 * mm,
                       doca13_max=0.1 * mm,
                       doca23_max=0.1 * mm,
                       maxdoca_max=0.1 * mm,
                       chi2dof_max=10.0,
                       bpvltime_min=0.15 * picosecond,
                       bpvfdchi2_min=None,
                       bpvacosdira_max=math.pi / 2.0):
    """
    Make three-body charmed hadron decays.
    """
    comb12_cut = F.require_all(F.MASS < am_max, F.DOCA(1, 2) < doca12_max)
    pvs = make_pvs()
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sumpt_min,
        F.SUM(F.PT > trk_1of3_pt_min) >= 1,
        F.SUM(F.PT > trk_2of3_pt_min) >= 2,
        F.SUM(F.MINIPCHI2(pvs) > trk_1of3_ipchi2_min) >= 1,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of3_ipchi2_min) >= 2,
        F.DOCA(1, 3) < doca13_max,
        F.DOCA(2, 3) < doca23_max)
    vtx_cut = F.require_all(*[
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < chi2dof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
    ] + ([
        # Only configure this cut if it has a threshold that is not None.
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
    ] if bpvfdchi2_min else []) + [F.BPVDIRA(pvs) > math.cos(bpvacosdira_max)])
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "CCBHcThreeBody", name]),
        Combination12Cut=comb12_cut,
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


def _make_hc_fourbody(particles,
                      descriptor,
                      name,
                      am_min=2386.0 * MeV,
                      am_max=2780.0 * MeV,
                      m_min=2396.0 * MeV,
                      m_max=2770.0 * MeV,
                      sumpt_min=3000 * MeV,
                      trk_1of4_pt_min=1000.0 * MeV,
                      trk_2of4_pt_min=500.0 * MeV,
                      trk_1of4_ipchi2_min=8.0,
                      trk_2of4_ipchi2_min=6.0,
                      doca12_max=0.2 * mm,
                      doca13_max=0.2 * mm,
                      doca14_max=0.2 * mm,
                      doca23_max=0.2 * mm,
                      doca24_max=0.2 * mm,
                      doca34_max=0.2 * mm,
                      maxdoca_max=0.2 * mm,
                      chi2dof_max=10.0,
                      bpvltime_min=0.1 * picosecond,
                      bpvfdchi2_min=10.0,
                      bpvacosdira_max=10.0 * mrad):
    """
    Make four-body charmed hadron decays.
    """
    comb12_cut = F.require_all(F.MASS < am_max, F.DOCA(1, 2) < doca12_max)
    comb123_cut = F.require_all(F.MASS < am_max,
                                F.DOCA(1, 3) < doca13_max,
                                F.DOCA(2, 3) < doca23_max)
    pvs = make_pvs()
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sumpt_min,
        F.SUM(F.PT > trk_1of4_pt_min) >= 1,
        F.SUM(F.PT > trk_2of4_pt_min) >= 2,
        F.SUM(F.MINIPCHI2(pvs) > trk_1of4_ipchi2_min) >= 1,
        F.SUM(F.MINIPCHI2(pvs) > trk_2of4_ipchi2_min) >= 2,
        F.DOCA(1, 4) < doca14_max,
        F.DOCA(2, 4) < doca24_max,
        F.DOCA(3, 4) < doca34_max,
    )
    vtx_cut = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < chi2dof_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > math.cos(bpvacosdira_max))
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "CCBHcFourBody", name]),
        Combination12Cut=comb12_cut,
        Combination123Cut=comb123_cut,
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


def _make_lcxicp():
    """Make common Lc and Xicp particles."""
    basic_protons = make_has_rich_long_protons()
    basic_kaons = make_has_rich_long_kaons()
    basic_pions = make_has_rich_long_pions()
    protons = _filter_protons(
        _filter_detached_particles(
            _filter_particles(basic_protons, p_min=10 * GeV)),
        dllk_min=5.0)
    kaons = _filter_kaons(
        _filter_detached_particles(_filter_particles(basic_kaons)))
    pions = _filter_pions(
        _filter_detached_particles(_filter_particles(basic_pions)))
    return _make_hc_threebody(
        [protons, kaons, pions],
        descriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="_".join(["Charm", "CCBLcOrXicpToPpKmPip"]),
        am_min=_LC_M - 85 * MeV,
        am_max=_XICP_M + 85 * MeV,
        m_min=_LC_M - 75 * MeV,
        m_max=_XICP_M + 75 * MeV,
    )


def _make_lc():
    """Make Lc particles."""
    return ParticleFilter(
        _make_lcxicp(),
        F.FILTER(in_range(_LC_M - 75 * MeV, F.MASS, _LC_M + 75 * MeV)))


def _make_xicp():
    """Make Xicp particles."""
    return ParticleFilter(
        _make_lcxicp(),
        F.FILTER(in_range(_XICP_M - 75 * MeV, F.MASS, _XICP_M + 75 * MeV)))


def _make_xiczomegac():
    """Make common Xicz and Omegac particles."""
    basic_protons = make_has_rich_long_protons()
    basic_kaons = make_has_rich_long_kaons()
    basic_pions = make_has_rich_long_pions()
    protons = _filter_protons(
        _filter_detached_particles(
            _filter_particles(basic_protons, pt_min=500 * MeV, p_min=10 * GeV),
            ipchi2_min=4.0),
        dllk_min=5.0)
    kaons = _filter_kaons(
        _filter_detached_particles(
            _filter_particles(basic_kaons, pt_min=500 * MeV), ipchi2_min=4.0))
    pions = _filter_pions(
        _filter_detached_particles(
            _filter_particles(basic_pions, pt_min=500 * MeV), ipchi2_min=4.0))
    return _make_hc_fourbody(
        [protons, kaons, kaons, pions],
        descriptor="[Xi_c0 -> p+ K- K- pi+]cc",
        name="CharmXiczOmegacCombiners",
        am_min=_XICZ_M - 85 * MeV,
        am_max=_OMEGAC_M + 85 * MeV,
        m_min=_XICZ_M - 75 * MeV,
        m_max=_OMEGAC_M + 75 * MeV,
    )


def _make_xicz():
    """Make Xicz particles."""
    return ParticleFilter(
        _make_xiczomegac(),
        F.FILTER(in_range(_XICZ_M - 75 * MeV, F.MASS, _XICZ_M + 75 * MeV)))


def _make_omegac():
    """Make Omegac particles."""
    return ParticleFilter(
        _make_xiczomegac(),
        F.FILTER(in_range(_OMEGAC_M - 75 * MeV, F.MASS, _OMEGAC_M + 75 * MeV)))


def _make_dz():
    """Make D0 particles."""
    basic_kaons = make_has_rich_long_kaons()
    basic_pions = make_has_rich_long_pions()
    kaons = _filter_kaons(
        _filter_detached_particles(
            _filter_particles(basic_kaons, pt_min=500 * MeV, p_min=5 * GeV),
            ipchi2_min=4.0))
    pions = _filter_pions(
        _filter_detached_particles(
            _filter_particles(basic_pions, pt_min=500 * MeV, p_min=5 * GeV),
            ipchi2_min=4.0))
    return _make_hc_twobody(
        [kaons, pions],
        descriptor="[D0 -> K- pi+]cc",
        name="DzToKmPip",
        am_min=_D0_M - 85 * MeV,
        am_max=_D0_M + 85 * MeV,
        m_min=_D0_M - 75 * MeV,
        m_max=_D0_M + 75 * MeV,
    )


def _make_dp():
    """Make D+ particles."""
    basic_kaons = make_has_rich_long_kaons()
    basic_pions = make_has_rich_long_pions()
    kaons = _filter_kaons(
        _filter_detached_particles(_filter_particles(basic_kaons)))
    pions = _filter_pions(
        _filter_detached_particles(_filter_particles(basic_pions)))
    return _make_hc_threebody([kaons, pions, pions],
                              descriptor="[D+ -> K- pi+ pi+]cc",
                              name="DpToKmPipPip",
                              am_min=_DP_M - 85 * MeV,
                              am_max=_DP_M + 85 * MeV,
                              m_min=_DP_M - 75 * MeV,
                              m_max=_DP_M + 75 * MeV,
                              sumpt_min=3000 * MeV,
                              trk_1of3_pt_min=1000.0 * MeV,
                              trk_2of3_pt_min=400.0 * MeV,
                              trk_1of3_ipchi2_min=50.0,
                              trk_2of3_ipchi2_min=10.0,
                              chi2dof_max=6.0,
                              bpvfdchi2_min=150.0,
                              bpvltime_min=0.4 * picosecond,
                              bpvacosdira_max=10.0 * mrad)


def _make_ds():
    """Make Ds particles."""
    basic_kaons = make_has_rich_long_kaons()
    basic_pions = make_has_rich_long_pions()
    kaons = _filter_kaons(
        _filter_detached_particles(_filter_particles(basic_kaons)))
    pions = _filter_pions(
        _filter_detached_particles(_filter_particles(basic_pions)))
    return _make_hc_threebody([kaons, kaons, pions],
                              descriptor="[D_s+ -> K- K+ pi+]cc",
                              name="DspToKmKpPip",
                              am_min=_DS_M - 85 * MeV,
                              am_max=_DS_M + 85 * MeV,
                              m_min=_DS_M - 75 * MeV,
                              m_max=_DS_M + 75 * MeV,
                              sumpt_min=3000 * MeV,
                              trk_1of3_pt_min=1000.0 * MeV,
                              trk_2of3_pt_min=400.0 * MeV,
                              trk_1of3_ipchi2_min=50.0,
                              trk_2of3_ipchi2_min=10.0,
                              chi2dof_max=6.0,
                              bpvfdchi2_min=100.0,
                              bpvltime_min=0.2 * picosecond,
                              bpvacosdira_max=14.1 * mrad)


# -------------------------------
# doubly-charmed baryon combiners
# -------------------------------
def _make_xicc_twobody(
        particles,
        descriptor,
        name,
        am_min=3090.0 * MeV,
        am_max=3810.0 * MeV,
        m_min=3100.0 * MeV,
        m_max=3800.0 * MeV,
        pt_min=2 * GeV,
        doca_max=0.1 * mm,
        docachi2_max=10.0,
        chi2dof_max=10,
        vz1vzdiff_min=0.01 * mm,
        #bpvfdchi2_min=-1.0,
        bpvacosdira_max=math.pi / 2.0):
    """
    Make two-body doubly charmed hadron decays.
    """
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.MAXDOCACUT(doca_max), F.MAXDOCACHI2CUT(docachi2_max))
    pvs = make_pvs()
    vtx_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < chi2dof_max,
        F.CHILD(1, F.END_VZ) - F.END_VZ > vz1vzdiff_min,
        #F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > math.cos(bpvacosdira_max))
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "DCBTwoBody", name]),
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


def _make_xicc_threebody(
        particles,
        descriptor,
        name,
        am_min=3090.0 * MeV,
        am_max=3810.0 * MeV,
        m_min=3100.0 * MeV,
        m_max=3800.0 * MeV,
        pt_min=2 * GeV,
        trk_1of2_pt_min=250 * MeV,
        doca_12_max=0.1 * mm,
        doca_13_max=0.1 * mm,
        doca_23_max=0.1 * mm,
        docachi2_12_max=10.0,
        docachi2_13_max=10.0,
        docachi2_23_max=10.0,
        chi2dof_max=15.0,
        vz1vzdiff_min=0.01 * mm,
        #bpvfdchi2_min=-1.0,
        bpvacosdira_max=math.pi / 2.0):
    """
    Make three-body doubly charmed hadron decays.
    """
    comb12_cut = F.require_all(F.MASS < am_max,
                               F.DOCA(1, 2) < doca_12_max,
                               F.DOCACHI2(1, 2) < docachi2_12_max)
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.require_any(
            F.CHILD(2, F.PT) > trk_1of2_pt_min,
            F.CHILD(3, F.PT) > trk_1of2_pt_min),
        F.DOCA(1, 3) < doca_13_max,
        F.DOCA(1, 3) < doca_13_max,
        F.DOCA(2, 3) < doca_23_max,
        F.DOCACHI2(1, 3) < docachi2_13_max,
        F.DOCACHI2(1, 3) < docachi2_13_max,
        F.DOCACHI2(2, 3) < docachi2_23_max)
    pvs = make_pvs()
    vtx_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < chi2dof_max,
        F.CHILD(1, F.END_VZ) - F.END_VZ > vz1vzdiff_min,
        #F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > math.cos(bpvacosdira_max))
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "DCBThreeBody", name]),
        Combination12Cut=comb12_cut,
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


def _make_xicc_fourbody(
        particles,
        descriptor,
        name,
        am_min=3090.0 * MeV,
        am_max=3810.0 * MeV,
        m_min=3100.0 * MeV,
        m_max=3800.0 * MeV,
        pt_min=2 * GeV,
        trk_1of3_pt_min=250 * MeV,
        trk_2of3_pt_min=250 * MeV,
        doca_12_max=0.2 * mm,
        doca_13_max=0.2 * mm,
        doca_14_max=0.2 * mm,
        doca_23_max=0.2 * mm,
        doca_24_max=0.2 * mm,
        doca_34_max=0.2 * mm,
        docachi2_12_max=10.0,
        docachi2_13_max=10.0,
        docachi2_14_max=10.0,
        docachi2_23_max=10.0,
        docachi2_24_max=10.0,
        docachi2_34_max=10.0,
        chi2dof_max=30.0,
        vz1vzdiff_min=0.01 * mm,
        #bpvfdchi2_min=-1.0,
        bpvacosdira_max=math.pi / 2.0):
    """
    Make four-body doubly charmed hadron decays.
    """
    comb12_cut = F.require_all(F.MASS < am_max,
                               F.DOCA(1, 2) < doca_12_max,
                               F.DOCACHI2(1, 2) < docachi2_12_max)
    comb123_cut = F.require_all(
        F.MASS < am_max,
        F.DOCA(1, 3) < doca_13_max,
        F.DOCA(2, 3) < doca_23_max,
        F.DOCACHI2(1, 3) < docachi2_13_max,
        F.DOCACHI2(2, 3) < docachi2_23_max,
        F.require_any(
            F.CHILD(2, F.PT) > trk_2of3_pt_min,
            F.CHILD(3, F.PT) > trk_2of3_pt_min))
    comb_cut = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min,
        F.require_any(
            F.CHILD(2, F.PT) > trk_1of3_pt_min,
            F.CHILD(3, F.PT) > trk_1of3_pt_min,
            F.CHILD(4, F.PT) > trk_1of3_pt_min),
        F.require_any(
            F.CHILD(2, F.PT) > trk_2of3_pt_min,
            F.CHILD(4, F.PT) > trk_2of3_pt_min),
        F.require_any(
            F.CHILD(3, F.PT) > trk_2of3_pt_min,
            F.CHILD(4, F.PT) > trk_2of3_pt_min),
        F.DOCA(1, 4) < doca_14_max,
        F.DOCA(2, 4) < doca_24_max,
        F.DOCA(3, 4) < doca_34_max,
        F.DOCACHI2(1, 4) < docachi2_14_max,
        F.DOCACHI2(2, 4) < docachi2_24_max,
        F.DOCACHI2(3, 4) < docachi2_34_max)
    pvs = make_pvs()
    vtx_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < chi2dof_max,
        F.CHILD(1, F.END_VZ) - F.END_VZ > vz1vzdiff_min,
        #F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > math.cos(bpvacosdira_max))
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "DCBFourBody", name]),
        Combination12Cut=comb12_cut,
        Combination123Cut=comb123_cut,
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


# --------------------------
# Pseudo four-body combiners
# --------------------------
def _make_pseudo_fourbody(particles,
                          descriptor,
                          name="",
                          am_max=3630.0 * MeV,
                          trk_1of4_pt_min=250 * MeV,
                          trk_2of4_pt_min=250 * MeV,
                          trk_3of4_pt_min=250 * MeV,
                          doca_12_max=0.2 * mm,
                          doca_13_max=0.2 * mm,
                          doca_14_max=0.2 * mm,
                          doca_23_max=0.2 * mm,
                          doca_24_max=0.2 * mm,
                          doca_34_max=0.2 * mm,
                          docachi2_12_max=10.0,
                          docachi2_13_max=10.0,
                          docachi2_14_max=10.0,
                          docachi2_23_max=10.0,
                          docachi2_24_max=10.0,
                          docachi2_34_max=10.0,
                          chi2dof_max=60.0):
    """Make pseudo particles from Xicc decay."""
    comb12_cut = F.require_all(F.MASS < am_max,
                               F.DOCA(1, 2) < doca_12_max,
                               F.DOCACHI2(1, 2) < docachi2_12_max)
    comb123_cut = F.require_all(
        F.MASS < am_max,
        F.DOCA(1, 3) < doca_13_max,
        F.DOCA(2, 3) < doca_23_max,
        F.DOCACHI2(1, 3) < docachi2_13_max,
        F.DOCACHI2(2, 3) < docachi2_23_max,
        F.require_any(
            F.CHILD(2, F.PT) > trk_3of4_pt_min,
            F.CHILD(3, F.PT) > trk_3of4_pt_min))
    comb_cut = F.require_all(F.MASS < am_max,
                             F.SUM(F.PT > trk_1of4_pt_min) >= 1,
                             F.SUM(F.PT > trk_2of4_pt_min) >= 2,
                             F.SUM(F.PT > trk_3of4_pt_min) >= 3,
                             F.DOCA(1, 4) < doca_14_max,
                             F.DOCA(2, 4) < doca_24_max,
                             F.DOCA(3, 4) < doca_34_max,
                             F.DOCACHI2(1, 4) < docachi2_14_max,
                             F.DOCACHI2(2, 4) < docachi2_24_max,
                             F.DOCACHI2(3, 4) < docachi2_34_max)
    vtx_cut = F.CHI2DOF < chi2dof_max
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name="_".join(["Charm", "DCBPseudoFourBody", name]),
        Combination12Cut=comb12_cut,
        Combination123Cut=comb123_cut,
        CombinationCut=comb_cut,
        CompositeCut=vtx_cut)


# ---------------------------
# doubly charmed baryon lines
# ---------------------------
all_lines = {}


# --------------------
# decay to Lambda_c+ X
# --------------------
# Signal modes
@register_line_builder(all_lines)
def xiccptolcpkmpip_lcptoppkmpip_line(
        name="Hlt2Charm_XiccpToLcpKmPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([lcs, kaons, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ K- pi+]cc",
                                  name="XiccpToLcpKmPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [xiccps], prescale=prescale)


@register_line_builder(all_lines)
def xiccptolcppimpip_lcptoppkmpip_line(
        name="Hlt2Charm_XiccpToLcpPimPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([lcs, pions, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ pi- pi+]cc",
                                  name="XiccpToLcpPimPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccps], prescale=prescale)


@register_line_builder(all_lines)
def xiccpptolcpkmpippip_lcptoppkmpip_line(
        name="Hlt2Charm_XiccppToLcpKmPipPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody(
        [lcs, kaons, pions, pions],
        descriptor="[Xi_cc++ -> Lambda_c+ K- pi+ pi+]cc",
        name="XiccppToLcpKmPipPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccpps], prescale=prescale)


@register_line_builder(all_lines)
def xiccpptolcppip_lcptoppkmpip_line(
        name="Hlt2Charm_XiccppToLcpPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_twobody([lcs, pions],
                                 descriptor="[Xi_cc++ -> Lambda_c+ pi+]cc",
                                 name="XiccppToLcpPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccpps], prescale=prescale)


@register_line_builder(all_lines)
def omegacctolcpkmkmpippip_lcptoppkmpip_line(
        name="Hlt2Charm_OmegaccToLcpKmKmPipPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    # placeholder for pseudo intermediate state
    bas = _make_pseudo_fourbody([kaons, kaons, pions, pions],
                                descriptor="[K_1(1400)0 -> K- K- pi+ pi+]cc",
                                name="K114000ToKmKmPipPip",
                                am_max=3.91 * GeV - _LC_M + 75 * MeV)
    omegaccs = _make_xicc_twobody(
        [lcs, bas],
        descriptor="[Omega_cc+ -> Lambda_c+ K_1(1400)0]cc",
        name="OmegaccToLcpKmKmPipPip",
        am_min=3.19 * GeV,
        am_max=3.91 * GeV,
        m_min=3.2 * GeV,
        m_max=3.9 * GeV)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lcs, omegaccs],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def xiccptolcpkmpim_lcptoppkmpip_line(
        name="Hlt2Charm_XiccpToLcpKmPim_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([lcs, kaons, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ K- pi-]cc",
                                  name="XiccpToLcpKmPim")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccps], prescale=prescale)


@register_line_builder(all_lines)
def xiccpptolcpkmpippim_lcptoppkmpip_line(
        name="Hlt2Charm_XiccppToLcpKmPimPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody(
        [lcs, kaons, pions, pions],
        descriptor="[Xi_cc++ -> Lambda_c+ K- pi- pi+]cc",
        name="XiccppToLcpKmPimPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccpps], prescale=prescale)


@register_line_builder(all_lines)
def xiccpptolcppim_lcptoppkmpip_line(
        name="Hlt2Charm_XiccppToLcpPim_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_twobody([lcs, pions],
                                 descriptor="[Xi_cc++ -> Lambda_c+ pi-]cc",
                                 name="XiccppToLcpPim")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccpps], prescale=prescale)


# Doubly Cabibbo Suppressed combinations
@register_line_builder(all_lines)
def xiccptolcpkppim_lcptoppkmpip_line(
        name="Hlt2Charm_XiccpToLcpKpPim_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([lcs, kaons, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ K+ pi-]cc",
                                  name="XiccpToLcpKpPim")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccps], prescale=prescale)


@register_line_builder(all_lines)
def xiccpptolcpkppimpip_lcptoppkmpip_line(
        name="Hlt2Charm_XiccppToLcpKpPimPip_LcpToPpKmPip", prescale=1):
    lcs = _make_lc()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody(
        [lcs, kaons, pions, pions],
        descriptor="[Xi_cc++ -> Lambda_c+ K+ pi- pi+]cc",
        name="XiccppToLcpKpPimPip")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, xiccpps], prescale=prescale)


# ----------------
# decay to Xi_c+ X
# ----------------
# Signal modes
@register_line_builder(all_lines)
def xiccptoxicppimpip_xiptoppkmpip_line(
        name="Hlt2Charm_XiccpToXipPimPip_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([xicps, pions, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ pi- pi+]cc",
                                  name="XiccpToXipPimPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccptoxicpkppim_xiptoppkmpip_line(
        name="Hlt2Charm_XiccpToXipKpPim_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([xicps, kaons, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ K+ pi-]cc",
                                  name="XiccpToXipKpPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxicppip_xiptoppkmpip_line(
        name="Hlt2Charm_XiccppToXipPip_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_twobody([xicps, pions],
                                 descriptor="[Xi_cc++ -> Lambda_c+ pi+]cc",
                                 name="XiccppToXipPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxicpkp_xiptoppkmpip_line(
        name="Hlt2Charm_XiccppToXipKp_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    kaons = _make_xicc_detached_child_kaons()
    xiccpps = _make_xicc_twobody([xicps, kaons],
                                 descriptor="[Xi_cc++ -> Lambda_c+ K+]cc",
                                 name="XiccppToXipKp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def omegacctoxicpkmpip_xiptoppkmpip_line(
        name="Hlt2Charm_OmegaccToXipKmPip_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    omegaccs = _make_xicc_threebody(
        [xicps, kaons, pions],
        descriptor="[Omega_cc+ -> Lambda_c+ K- pi+]cc",
        name="OmegaccToXipKmPip",
        am_min=3.19 * GeV,
        am_max=3.91 * GeV,
        m_min=3.2 * GeV,
        m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, omegaccs],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def xiccptoxicppimpim_xiptoppkmpip_line(
        name="Hlt2Charm_XiccpToXipPimPim_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([xicps, pions, pions],
                                  descriptor="[Xi_cc+ -> Lambda_c+ pi- pi-]cc",
                                  name="XiccpToXipPimPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxicppim_xiptoppkmpip_line(
        name="Hlt2Charm_XiccppToXipPim_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_twobody([xicps, pions],
                                 descriptor="[Xi_cc++ -> Lambda_c+ pi-]cc",
                                 name="XiccppToXipPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def omegacctoxicpkmpim_xiptoppkmpip_line(
        name="Hlt2Charm_OmegaccToXipKmPim_XipToPpKmPip", prescale=1):
    xicps = _make_xicp()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    omegaccs = _make_xicc_threebody(
        [xicps, kaons, pions],
        descriptor="[Omega_cc+ -> Lambda_c+ K- pi-]cc",
        name="OmegaccToXipKmPim",
        am_min=3.19 * GeV,
        am_max=3.91 * GeV,
        m_min=3.2 * GeV,
        m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, omegaccs],
        prescale=prescale)


# ----------------
# decay to Xi_c0 X
# ----------------
# Signal modes
@register_line_builder(all_lines)
def xiccptoxic0pip_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccpToXic0Pip_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_twobody([xiczs, pions],
                                descriptor="[Xi_cc+ -> Xi_c0 pi+]cc",
                                name="XiccpToXic0Pip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccptoxic0kp_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccpToXic0Kp_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    kaons = _make_xicc_prompt_child_kaons()
    xiccps = _make_xicc_twobody([xiczs, kaons],
                                descriptor="[Xi_cc+ -> Xi_c0 K+]cc",
                                name="XiccpToXic0Kp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxic0pippip_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccppToXic0PipPip_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_threebody([xiczs, pions, pions],
                                   descriptor="[Xi_cc++ -> Xi_c0 pi+ pi+]cc",
                                   name="XiccppToXic0PipPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxic0kppip_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccppToXic0KpPip_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_threebody([xiczs, kaons, pions],
                                   descriptor="[Xi_cc++ -> Xi_c0 K+ pi+]cc",
                                   name="XiccppToXic0KpPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def omegacctoxic0kmpippip_xic0toppkmkmpip_line(
        name="Hlt2Charm_OmegaccToXic0KmPipPip_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    omegaccs = _make_xicc_fourbody(
        [xiczs, kaons, pions, pions],
        descriptor="[Omega_cc+ -> Xi_c0 K- pi+ pi+]cc",
        name="OmegaccToXic0KmPipPip",
        am_min=3.19 * GeV,
        am_max=3.91 * GeV,
        m_min=3.2 * GeV,
        m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, omegaccs],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def xiccptoxic0pim_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccpToXic0Pim_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_twobody([xiczs, pions],
                                descriptor="[Xi_cc+ -> Xi_c0 pi-]cc",
                                name="XiccpToXic0Pim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptoxic0pippim_xic0toppkmkmpip_line(
        name="Hlt2Charm_XiccppToXic0PimPip_Xic0ToPpKmKmPip", prescale=1):
    xiczs = _make_xicz()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_threebody([xiczs, pions, pions],
                                   descriptor="[Xi_cc++ -> Xi_c0 pi- pi+]cc",
                                   name="XiccppToXic0PimPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xiczs, xiccpps],
        prescale=prescale)


# -------------------
# decay to Omega_c0 X
# -------------------
# Signal modes
@register_line_builder(all_lines)
def xiccpptoomegackpkp_omegactoppkmkmpip_line(
        name="Hlt2Charm_XiccppToOmegacKpKp_OmegacToPpKmKmPip", prescale=1):
    omegacs = _make_omegac()
    kaons = _make_xicc_detached_child_kaons()
    xiccpps = _make_xicc_threebody([omegacs, kaons, kaons],
                                   descriptor="[Xi_cc++ -> Xi_c0 K+ K+]cc",
                                   name="XiccppToOmegacKpKp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [omegacs, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def omegacctoomegacpip_omegactoppkmkmpip_line(
        name="Hlt2Charm_OmegaccToOmegacPip_OmegacToPpKmKmPip", prescale=1):
    omegacs = _make_omegac()
    pions = _make_xicc_prompt_child_pions()
    omegaccs = _make_xicc_twobody([omegacs, pions],
                                  descriptor="[Omega_cc+ -> Xi_c0 pi+]cc",
                                  name="OmegaccToOmegacPip",
                                  am_min=3.19 * GeV,
                                  am_max=3.91 * GeV,
                                  m_min=3.2 * GeV,
                                  m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [omegacs, omegaccs],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def omegacctoomegacpim_omegactoppkmkmpip_line(
        name="Hlt2Charm_OmegaccToOmegacPim_OmegacToPpKmKmPip", prescale=1):
    omegacs = _make_omegac()
    pions = _make_xicc_prompt_child_pions()
    omegaccs = _make_xicc_twobody([omegacs, pions],
                                  descriptor="[Omega_cc+ -> Xi_c0 pi-]cc",
                                  name="OmegaccToOmegacPim",
                                  am_min=3.19 * GeV,
                                  am_max=3.91 * GeV,
                                  m_min=3.2 * GeV,
                                  m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [omegacs, omegaccs],
        prescale=prescale)


# -------------
# decay to D+ X
# -------------
# Signal modes
@register_line_builder(all_lines)
def xiccptodpppkm_dp2kmpippip_line(name="Hlt2Charm_XiccpToDpPpKm_Dp2KmPipPip",
                                   prescale=1):
    dps = _make_dp()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    xiccps = _make_xicc_threebody([dps, protons, kaons],
                                  descriptor="[Xi_cc+ -> D+ p+ K-]cc",
                                  name="XiccpToDpPpKm")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccptodppppim_dp2kmpippip_line(
        name="Hlt2Charm_XiccpToDpPpPim_Dp2KmPipPip", prescale=1):
    dps = _make_dp()
    protons = _make_xicc_prompt_child_protons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_threebody([dps, protons, pions],
                                  descriptor="[Xi_cc+ -> D+ p+ pi-]cc",
                                  name="XiccpToDpPpPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptodpppkmpip_dp2kmpippip_line(
        name="Hlt2Charm_XiccppToDpPpKmPip_Dp2KmPipPip", prescale=1):
    dps = _make_dp()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody([dps, protons, kaons, pions],
                                  descriptor="[Xi_cc++ -> D+ p+ K- pi+]cc",
                                  name="XiccppToDpPpKmPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptodppp_dp2kmpippip_line(name="Hlt2Charm_XiccppToDpPp_Dp2KmPipPip",
                                  prescale=1):
    dps = _make_dp()
    protons = _make_xicc_detached_child_protons()
    xiccpps = _make_xicc_twobody([dps, protons],
                                 descriptor="[Xi_cc++ -> D+ p+]cc",
                                 name="XiccppToDpPp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def omegacctodpppkmkmpip_dp2kmpippip_line(
        name="Hlt2Charm_OmegaccToDpPpKmKmPip_Dp2KmPipPip", prescale=1):
    dps = _make_dp()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    # placeholder for pseudo intermediate state
    bas = _make_pseudo_fourbody([protons, kaons, kaons, pions],
                                descriptor="[K_1(1400)0 -> p+ K- K- pi+]cc",
                                name="K114000ToPpKmKmPip",
                                am_max=3.91 * GeV - _DP_M + 75 * MeV)
    omegaccs = _make_xicc_twobody([dps, bas],
                                  descriptor="[Omega_cc+ -> D+ K_1(1400)0]cc",
                                  name="OmegaccToDpPpKmKmPip",
                                  am_min=3.19 * GeV,
                                  am_max=3.91 * GeV,
                                  m_min=3.2 * GeV,
                                  m_max=3.9 * GeV)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, omegaccs],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def xiccptodpppkp_dp2kmpippip_line(name="Hlt2Charm_XiccpToDpPpKp_Dp2KmPipPip",
                                   prescale=1):
    dps = _make_dp()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    xiccps = _make_xicc_threebody([dps, protons, kaons],
                                  descriptor="[Xi_cc+ -> D+ p+ K+]cc",
                                  name="XiccpToDpPpKp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptodpppkmpim_dp2kmpippip_line(
        name="Hlt2Charm_XiccppToDpPpKmPim_Dp2KmPipPip", prescale=1):
    dps = _make_dp()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody([dps, protons, kaons, pions],
                                  descriptor="[Xi_cc++ -> D+ p+ K- pi-]cc",
                                  name="XiccppToDpPpKmPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptodpppkppip_dp2kmpippip_line(
        name="Hlt2Charm_XiccppToDpPpKpPip_Dp2KmPipPip", prescale=1):
    dps = _make_dp()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody([dps, protons, kaons, pions],
                                  descriptor="[Xi_cc++ -> D+ p+ K+ pi+]cc",
                                  name="XiccppToDpPpKpPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dps, xiccpps],
        prescale=prescale)


# -------------
# decay to D0 X
# -------------
# Signal modes
@register_line_builder(all_lines)
def xiccptod0ppkmpip_d02kmpip_line(name="Hlt2Charm_XiccpToD0PpKmPip_D02KmPip",
                                   prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_fourbody([dzs, protons, kaons, pions],
                                 descriptor="[Xi_cc+ -> D0 p+ K- pi+]cc",
                                 name="XiccpToD0PpKmPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccptod0pp_d02kmpip_line(name="Hlt2Charm_XiccpToD0Pp_D02KmPip",
                              prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_prompt_child_protons()
    xiccps = _make_xicc_twobody([dzs, protons],
                                descriptor="[Xi_cc+ -> D0 p+]cc",
                                name="XiccpToD0Pp")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptod0ppkmpippip_d02kmpip_line(
        name="Hlt2Charm_XiccppToD0PpKmPipPip_D02KmPip", prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    # placeholder for pseudo intermediate state
    bas = _make_pseudo_fourbody(
        [protons, kaons, pions, pions],
        name="Delta1950ppToPpKmPipPip",
        descriptor="[Delta(1950)++ -> p+ K- pi+ pi+]cc",
        am_max=3.81 * GeV - _D0_M + 75 * MeV)
    xiccpps = _make_xicc_twobody([dzs, bas],
                                 descriptor="[Xi_cc++ -> D0 Delta(1950)++]cc",
                                 name="XiccppToD0PpKmPipPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccpps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptod0pppip_d02kmpip_line(name="Hlt2Charm_XiccppToD0PpPip_D02KmPip",
                                  prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_detached_child_protons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_threebody([dzs, protons, pions],
                                   descriptor="[Xi_cc++ -> D0 p+ pi+]cc",
                                   name="XiccppToD0PpPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccpps],
        prescale=prescale)


# Wrong-sign combinations
@register_line_builder(all_lines)
def xiccptod0ppkmpim_d02kmpip_line(name="Hlt2Charm_XiccpToD0PpKmPim_D02KmPip",
                                   prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_fourbody([dzs, protons, kaons, pions],
                                 descriptor="[Xi_cc+ -> D0 p+ K- pi-]cc",
                                 name="XiccpToD0PpKmPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptod0ppkmpippim_d02kmpip_line(
        name="Hlt2Charm_XiccppToD0PpKmPimPip_D02KmPip", prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    # placeholder for pseudo intermediate state
    bas = _make_pseudo_fourbody([protons, kaons, pions, pions],
                                descriptor="[K_1(1400)0 -> p+ K- pi- pi+]cc",
                                name="K114000ToPpKmPimPip",
                                am_max=3.81 * GeV - _D0_M + 75 * MeV)
    xiccpps = _make_xicc_twobody([dzs, bas],
                                 descriptor="[Xi_cc++ -> D0 K_1(1400)0]cc",
                                 name="XiccppToD0PpKmPimPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccpps],
        prescale=prescale)


# Doubly Cabibbo Suppressed combinations
@register_line_builder(all_lines)
def xiccptod0ppkppim_d02kmpip_line(name="Hlt2Charm_XiccpToD0PpKpPim_D02KmPip",
                                   prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    pions = _make_xicc_prompt_child_pions()
    xiccps = _make_xicc_fourbody([dzs, protons, kaons, pions],
                                 descriptor="[Xi_cc+ -> D0 p+ K+ pi-]cc",
                                 name="XiccpToD0PpKpPim")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptod0ppkppippim_d02kmpip_line(
        name="Hlt2Charm_XiccppToD0PpKpPimPip_D02KmPip", prescale=1):
    dzs = _make_dz()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    # placeholder for pseudo intermediate state
    bas = _make_pseudo_fourbody(
        [protons, kaons, pions, pions],
        descriptor="[Delta(1950)++ -> p+ K+ pi- pi+]cc",
        name="Delta1950ppToPpKmPimPip",
        am_max=3.81 * GeV - _D0_M + 75 * MeV)
    xiccpps = _make_xicc_twobody([dzs, bas],
                                 descriptor="[Xi_cc++ -> D0 Delta(1950)++]cc",
                                 name="XiccppToD0PpKpPimPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dzs, xiccpps],
        prescale=prescale)


# ---------------
# decay to D_s+ X
# ---------------
# Signal modes
@register_line_builder(all_lines)
def xiccptodspppkm_dsp2kmkppip_line(
        name="Hlt2Charm_XiccpToDspPpKm_Dsp2KmKpPip", prescale=1):
    dss = _make_ds()
    protons = _make_xicc_prompt_child_protons()
    kaons = _make_xicc_prompt_child_kaons()
    xiccps = _make_xicc_threebody([dss, protons, kaons],
                                  descriptor="[Xi_cc+ -> D_s+ p+ K-]cc",
                                  name="XiccpToDspPpKm")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dss, xiccps],
        prescale=prescale)


@register_line_builder(all_lines)
def xiccpptodspppkmpip_dsp2kmkppip_line(
        name="Hlt2Charm_XiccppToDspPpKmPip_Dsp2KmKpPip", prescale=1):
    dss = _make_ds()
    protons = _make_xicc_detached_child_protons()
    kaons = _make_xicc_detached_child_kaons()
    pions = _make_xicc_detached_child_pions()
    xiccpps = _make_xicc_fourbody([dss, protons, kaons, pions],
                                  descriptor="[Xi_cc++ -> D_s+ p+ K- pi+]cc",
                                  name="XiccppToDspPpKmPip")
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [protons, dss, xiccpps],
        prescale=prescale)

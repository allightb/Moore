###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Ds1 -> Ds+ pi+ pi- decays
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm, GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons)
from .prefilters import charm_prefilters

all_lines = {}


def make_pions_from_ds_decay():
    """Return pions maker for Ds decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4.0, Vertices=make_pvs()),
                F.P > 1.2 * GeV,
                F.PT > 245 * MeV,
                F.PID_K < 5.,
            ), ),
    )


def make_pions_from_dsstar_decay():
    """Return pions maker for Dsstar decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.BPVIPCHI2(make_pvs()) < 3.5,
                F.P > 1.3 * GeV,
                F.PT > 295 * MeV,
                # p_min=5 * GeV,
                # trchi2dof_max=4,
                # trg_ghost_prob_max=0.5,
                F.PID_K < 5.,
            ), ),
    )


def make_kaons():
    """Return kaons maker for Ds decay."""
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=4.0, Vertices=make_pvs()),
                F.PT > 290 * MeV,
                F.P > 3.2 * GeV,
                # p_min=5 * GeV,
                # trchi2dof_max=4,
                # trg_ghost_prob_max=0.5,
                F.PID_K > 5.,
            ), ),
    )


def make_dss(particle1, particle2, particle3):
    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor="[D_s+ -> K- K+ pi+]cc",
        name='Charm_Dsstar2dspipi_BuilderDsToKmKpPip',
        CombinationCut=F.require_all(
            in_range(1885 * MeV, F.MASS, 2045 * MeV),
            F.PT > 4.0 * GeV,
            F.MAXDOCACHI2CUT(18.0),
            F.MAXDOCACUT(0.27 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1890 * MeV, F.MASS, 2040 * MeV),
            F.CHI2DOF < 10.,
            in_range(-130 * mm, F.END_VZ, 200 * mm),
            F.BPVFDCHI2(make_pvs()) > 75.0,
            F.BPVDIRA(make_pvs()) > 0.999,
        ),
    )


def make_dsstars2536(particle1, particle2, particle3):
    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor="[D_s1(2536)+ -> D_s+ pi- pi+]cc",
        name='Charm_Dsstar2dspipi_BuilderDsstar2536ToDspPimPip_{hash}',
        CombinationCut=F.require_all(
            in_range(2500 * MeV, F.MASS, 2615 * MeV),
            F.PT > 4.5 * GeV,
            F.MAXDOCACHI2CUT(15.0),
            F.DOCA(1, 2) < 0.30 * mm,
            F.DOCA(1, 3) < 0.30 * mm,
            F.DOCA(2, 3) < 0.65 * mm,
        ),
        CompositeCut=F.require_all(
            in_range(2410 * MeV, F.MASS, 2670 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 1.0,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )


def make_dsstars2460(particle1, particle2, particle3):
    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor="[D_s1(2460)+ -> D_s+ pi- pi+]cc",
        name='Charm_Dsstar2dspipi_BuilderDsstar2460ToDspPimPip_{hash}',
        CombinationCut=F.require_all(
            in_range(2370 * MeV, F.MASS, 2500 * MeV),
            F.PT > 4.5 * GeV,
            F.MAXDOCACHI2CUT(15.0),
            F.DOCA(1, 2) < 0.30 * mm,
            F.DOCA(1, 3) < 0.30 * mm,
            F.DOCA(2, 3) < 0.65 * mm,
        ),
        CompositeCut=F.require_all(
            in_range(2375 * MeV, F.MASS, 2500 * MeV),
            F.CHI2DOF < 10.,
            in_range(-130 * mm, F.END_VZ, 190 * mm),
            F.BPVDIRA(make_pvs()) > 0.99,
        ),
    )


@register_line_builder(all_lines)
def dstar2536p2dsppimpip_dsp2kmkppip_line(
        name='Hlt2Charm_Dsstar2536pToDspPimPip_DspToKmKpPip', prescale=1):
    kaons = make_kaons()
    pions_from_ds = make_pions_from_ds_decay()
    pions_from_dsstar = make_pions_from_dsstar_decay()
    dss = make_dss(kaons, kaons, pions_from_ds)
    dsstars = make_dsstars2536(dss, pions_from_dsstar, pions_from_dsstar)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dss, dsstars], prescale=prescale)


@register_line_builder(all_lines)
def dstar2460p2dsppimpip_dsp2kmkppip_line(
        name='Hlt2Charm_Dsstar2460pToDspPimPip_DspToKmKpPip', prescale=1):
    kaons = make_kaons()
    pions_from_ds = make_pions_from_ds_decay()
    pions_from_dsstar = make_pions_from_dsstar_decay()
    dss = make_dss(kaons, kaons, pions_from_ds)
    dsstars = make_dsstars2460(dss, pions_from_dsstar, pions_from_dsstar)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dss, dsstars], prescale=prescale)

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Lambda_c+/Xi_c+/Xi_c0/Omega_c0 -> Lambda/Xi-/Omega- X HLT2 lines.
Most decays are prompt, some have a dedicated from-b selection.

(quasi) 2-body lines:
---------------------
- Lc -> L pi (+ S0 pi)
  Hlt2Charm_LcpToL0Pip_LL
  Hlt2Charm_LcpToL0Pip_DD
  Hlt2Charm_LcpToL0Pip_LL_Inclb_PR
  Hlt2Charm_LcpToL0Pip_DD_Inclb_PR
  Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_LL
  Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_DD
  Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_LL
  Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_DD
- Lc -> L K (+ S0 K)  (author: Miroslav Saur)
  Hlt2Charm_LcpToL0Kp_LL
  Hlt2Charm_LcpToL0Kp_DD
  Hlt2Charm_LcpToL0Kp_LL_Inclb_PR
  Hlt2Charm_LcpToL0Kp_DD_Inclb_PR
- Xic+ -> L pi (+ S0 pi)
  Hlt2Charm_XicpToL0Pip_LL
  Hlt2Charm_XicpToL0Pip_DD
  Hlt2Charm_XicpToL0Pip_LL_Inclb_PR
  Hlt2Charm_XicpToL0Pip_DD_Inclb_PR
  Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_LL
  Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_DD
  Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_LL
  Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_DD
- Xic0 -> Xi pi
  Hlt2Charm_Xic0ToXimPip_LLL
  Hlt2Charm_Xic0ToXimPip_DDL
  Hlt2Charm_Xic0ToXimPip_LLL_Inclb_PR
  Hlt2Charm_Xic0ToXimPip_DDL_Inclb_PR
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_LLL
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_DDL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_DDL
- Xic0 -> Xi K
  Hlt2Charm_Xic0ToXimKp_LLL
  Hlt2Charm_Xic0ToXimKp_DDL
  Hlt2Charm_Xic0ToXimKp_LLL_Inclb_PR
  Hlt2Charm_Xic0ToXimKp_DDL_Inclb_PR
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_LLL
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_DDL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_LLL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_DDL
- Xic0 -> O K
  Hlt2Charm_Xic0ToOmKp_LLL
  Hlt2Charm_Xic0ToOmKp_DDL
- Xic0 -> L0 KS0  (author: Miroslav Saur)
  Hlt2Charm_Xic0ToL0Ks_LLLL
  Hlt2Charm_Xic0ToL0Ks_DDLL
  Hlt2Charm_Xic0ToL0Ks_LLDD
  Hlt2Charm_Xic0ToL0Ks_DDDD
- Oc -> Xi pi
  Hlt2Charm_Oc0ToXimPip_LLL
  Hlt2Charm_Oc0ToXimPip_DDL
- Oc -> Xi K
  Hlt2Charm_Oc0ToXimKp_LLL
  Hlt2Charm_Oc0ToXimKp_DDL
  Hlt2Charm_Oc0ToXimKp_LLL_Inclb_PR
  Hlt2Charm_Oc0ToXimKp_DDL_Inclb_PR
  Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_LLL
  Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_DDL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_LLL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_DDL
- Oc -> O pi
  Hlt2Charm_Oc0ToOmPip_LLL
  Hlt2Charm_Oc0ToOmPip_DDL
  Hlt2Charm_Oc0ToOmPip_LLL_Inclb_PR
  Hlt2Charm_Oc0ToOmPip_DDL_Inclb_PR
  Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_LLL
  Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_DDL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_DDL
- Oc -> O K
  Hlt2Charm_Oc0ToOmKp_LLL
  Hlt2Charm_Oc0ToOmKp_DDL
- Oc -> L0 KS0  (author: Miroslav Saur)
  Hlt2Charm_Oc0ToL0Ks_LLLL
  Hlt2Charm_Oc0ToL0Ks_DDLL
  Hlt2Charm_Oc0ToL0Ks_LLDD
  Hlt2Charm_Oc0ToL0Ks_DDDD

(quasi) 3-body lines:
---------------------
- Omega- -> Xi- pi- pi+ (long Xi-)
  Hlt2Charm_OmToXimPimPip_LongXim_SP
- Lc -> L Ks K (+ S0 Ks K)
  Hlt2Charm_LcpToL0KsKp_LLLL
  Hlt2Charm_LcpToL0KsKp_DDLL
  Hlt2Charm_LcpToL0KsKp_LLDD
- Lc -> Sigma- pi+ pi+ (long Sigma-, from b)
  Hlt2Charm_Lb0ToLcpPim_LcpToSmPipPip_LongSm_SP
- Lc -> Xi K pi
  Hlt2Charm_LcpToXimKpPip_LLL
  Hlt2Charm_LcpToXimKpPip_DDL
- Xic+ -> L Ks pi (+ S0 Ks pi)
  Hlt2Charm_XicpToL0KsPip_LLLL
  Hlt2Charm_XicpToL0KsPip_DDLL
  Hlt2Charm_XicpToL0KsPip_LLDD
- Xic+ -> L Ks K (+ S0 Ks K)
  Hlt2Charm_XicpToL0KsKp_LLLL
  Hlt2Charm_XicpToL0KsKp_DDLL
  Hlt2Charm_XicpToL0KsKp_LLDD
- Xic+ -> Sigma+ K- pi+ (long Sigma+, from b)
  Hlt2Charm_Xib0ToXicpPim_XicpToSpKmPip_LongSp_SP
- Xic+ -> Xi pi pi
  Hlt2Charm_XicpToXimPipPip_LLL
  Hlt2Charm_XicpToXimPipPip_DDL
  Hlt2Charm_XicpToXimPipPip_LLL_Inclb_PR
  Hlt2Charm_XicpToXimPipPip_DDL_Inclb_PR
  Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_LLL
  Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_DDL
  Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_DDL
  Hlt2Charm_Xib0ToXicpPim_XicpToXimPipPip_LongXi_SP
- Xic+ -> Xi K pi
  Hlt2Charm_XicpToXimKpPip_LLL
  Hlt2Charm_XicpToXimKpPip_DDL
- Xic+ -> O K pi
  Hlt2Charm_XicpToOmKpPip_LLL
  Hlt2Charm_XicpToOmKpPip_DDL
  Hlt2Charm_Xib0ToXicpPim_XicpToOmKpPip_LongOm_SP
- Xic0 -> L pi pi (+ S0 pi pi)
  Hlt2Charm_Xic0ToL0PimPip_LL
  Hlt2Charm_Xic0ToL0PimPip_DD
- Xic0 -> L K- pi+ (+ S0 K- pi+)
  Hlt2Charm_Xic0ToL0KmPip_LL
  Hlt2Charm_Xic0ToL0KmPip_DD
  Hlt2Charm_Xic0ToL0KmPip_LL_Inclb_PR
  Hlt2Charm_Xic0ToL0KmPip_DD_Inclb_PR
  Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_DD
  Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_LL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_DD
  Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_LL
- Xic0 -> L K K (+ S0 K K)
  Hlt2Charm_Xic0ToL0KmKp_LL
  Hlt2Charm_Xic0ToL0KmKp_DD
- Xic0 -> Xi Ks pi
  Hlt2Charm_Xic0ToXimKsPip_LLLLL
  Hlt2Charm_Xic0ToXimKsPip_LLLDD
  Hlt2Charm_Xic0ToXimKsPip_DDLLL
  Hlt2Charm_Xic0ToXimKsPip_DDLDD
- Xic0 -> Xi Ks K
  Hlt2Charm_Xic0ToXimKsKp_LLLLL
  Hlt2Charm_Xic0ToXimKsKp_LLLDD
  Hlt2Charm_Xic0ToXimKsKp_DDLLL
  Hlt2Charm_Xic0ToXimKsKp_DDLDD
- Oc -> L K pi (+ S0 K pi)
  Hlt2Charm_Oc0ToL0KmPip_LL
  Hlt2Charm_Oc0ToL0KmPip_DD
- Oc -> L K K (+ S0 K K)
  Hlt2Charm_Oc0ToL0KmKp_LL
  Hlt2Charm_Oc0ToL0KmKp_DD
- Oc -> Xi Ks pi
  Hlt2Charm_Oc0ToXimKsPip_LLLLL
  Hlt2Charm_Oc0ToXimKsPip_LLLDD
  Hlt2Charm_Oc0ToXimKsPip_DDLLL
  Hlt2Charm_Oc0ToXimKsPip_DDLDD

(quasi) 4-body lines:
---------------------
- Lc -> L pi- pi+ pi+
  Hlt2Charm_LcpToL0PimPipPip_LL  (author: Miguel Rebollo De Miguel)
  Hlt2Charm_LcpToL0PimPipPip_DD  (author: Miguel Rebollo De Miguel)
- Lc -> L K- K+ pi+
  Hlt2Charm_LcpToL0KmKpPip_LL
  Hlt2Charm_LcpToL0KmKpPip_DD
- Lc -> Xi Ks pi pi
  Hlt2Charm_LcpToXimKsPipPip_LLLLL
  Hlt2Charm_LcpToXimKsPipPip_LLLDD
  Hlt2Charm_LcpToXimKsPipPip_DDLLL
  Hlt2Charm_LcpToXimKsPipPip_DDLDD
- Xic+ -> L K- K+ pi+ (+ S0 K- K+ pi+)
  Hlt2Charm_XicpToL0KmKpPip_LL
  Hlt2Charm_XicpToL0KmKpPip_DD
- Xic+ -> L K pi+ pi+ (+ S0 K pi+ pi+)
  Hlt2Charm_XicpToL0KmPipPip_LL
  Hlt2Charm_XicpToL0KmPipPip_DD
- Xic0 -> L Ks pi pi (+ S0 Ks pi pi)
  Hlt2Charm_Xic0ToL0KsPimPip_LLLL
  Hlt2Charm_Xic0ToL0KsPimPip_LLDD
  Hlt2Charm_Xic0ToL0KsPimPip_DDLL
- Xic0 -> Xi 3pi
  Hlt2Charm_Xic0ToXimPimPipPip_LLL
  Hlt2Charm_Xic0ToXimPimPipPip_DDL
- Xic0 -> O K+ pi+ pi-
  Hlt2Charm_Xic0ToOmKpPimPip_LLL
  Hlt2Charm_Xic0ToOmKpPimPip_DDL
- Oc -> L Ks K- pi+ (+ S0 Ks K- pi+)
  Hlt2Charm_Oc0ToL0KsKmPip_LLLL
  Hlt2Charm_Oc0ToL0KsKmPip_LLDD
  Hlt2Charm_Oc0ToL0KsKmPip_DDLL
- Oc -> Xi K- pi+ pi+
  Hlt2Charm_Oc0ToXimKmPipPip_LLL
  Hlt2Charm_Oc0ToXimKmPipPip_DDL
- Oc -> O 3pi
  Hlt2Charm_Oc0ToOmPimPipPip_LLL
  Hlt2Charm_Oc0ToOmPimPipPip_DDL
- Oc -> O K+ pi+ pi-
  Hlt2Charm_Oc0ToOmKpPimPip_LLL
  Hlt2Charm_Oc0ToOmKpPimPip_DDL

TODO
----
Functor wishlist:
  - ProbNN (only after data arrives)
Other:
  - Revisit with isolation as RelatedInfo available
"""
import Functors as F
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um, ps)

from PyConf.Algorithms import (MatchVeloTrackToVertex, TrackListRefiner,
                               TrackSelectionToContainer)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.legacy_rec_hlt1_tracking import all_velo_track_types
from ...standard_particles import (
    make_has_rich_long_pions, make_long_pions, make_has_rich_long_kaons,
    make_has_rich_down_kaons, make_has_rich_long_protons,
    make_has_rich_down_pions, make_has_rich_down_protons, make_long_sigmaps,
    make_long_sigmams, make_long_xis, make_long_omegas, masses as mPDG)
from ...algorithms_thor import ParticleFilter, ParticleCombiner
from .prefilters import charm_prefilters


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIP_MIN(cut, pvs=make_pvs):
    return F.MINIPCUT(IPCut=cut, Vertices=pvs())


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _match_ddd_hyperon_to_velo_track(hyperon,
                                     velo_tracks=all_velo_track_types):
    filtered_velo_tracks = TrackListRefiner(
        inputLocation=velo_tracks()["v1"],
        Code=F.require_all(_MIPCHI2_MIN(1.),
                           _MIP_MIN(20. * um))).outputLocation
    filtered_velo_container = TrackSelectionToContainer(
        InputLocation=filtered_velo_tracks).OutputLocation
    return MatchVeloTrackToVertex(
        SeedParticles=hyperon,
        VeloTracks=filtered_velo_container,
        VeloMatchIP=2.5 * mm,
        EtaTolerance=0.4).OutputParticles


########################################################################
## Local builders for (combined) particles used throughout the module ##
########################################################################
# Some selections are used multiple times throughout the module.
# We define their builders upfront here. These builders are local
# and follow the PEP 8 Style Guide for Python Code
# https://www.python.org/dev/peps/pep-0008/#descriptive-naming-styles
#  >  _single_leading_underscore: weak "internal use" indicator.
#     E.g. from M import * does not import objects whose names
#     start with an underscore.
#
# The cut values are set in the body of the builders,
# such that they are immutable.
# This ensures that:
# - Only one filter or combiner instance is created and called.
#   This speeds up the selection.
# - Cut values are set only once and cannot be overwritten.
#   The code is easier to read and less error-prone.
#
# Some builders are standalone and not part of the "configuration-flow"
# mentioned in the style-guidelines used for Moore/RecoConf:
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/recoconf/recoconf.html
# The idea behind this is to re-define basic particles as starting point
# for selections in the module. These builders use proto-particle-makers
# and pvs directly in the body of the function. Other common selections,
# like the Xi- builders consume "rare" input particles.
# These rare input particles are intended to be used to configure the
# control flow and speed up selections.
########################################################################


def _make_long_pions_for_lambda():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(32.))))


def _make_long_pions_for_kshort():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(16.), F.PID_K < 5.)))


def _make_long_pions_for_xi():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(8.))))


def _make_long_pions_for_d():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 150 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(12.),
                          F.PID_K < 5.)))


def _make_down_pions_for_kshort():
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 200 * MeV, F.P > 2 * GeV, F.PID_K < 5.)))


def _make_down_pions_for_lambda():
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, F.PID_K < 5.)))


def _make_long_kaons_for_omega():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 180 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(9.),
                          F.PID_K > -2.)))


def _make_down_kaons_for_omega():
    return ParticleFilter(
        make_has_rich_down_kaons(),
        F.FILTER(
            F.require_all(F.PT > 180 * MeV, F.P > 3 * GeV, F.PID_K > -2.)))


def _make_long_kaons_for_d():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 200 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(12.),
                          F.PID_K > 0.)))


def _make_long_protons_for_lambda():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 9 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_P > -5.)))


def _make_down_protons_for_lambda():
    return ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.P > 9 * GeV, F.PID_P > -2.)))


def _make_ll_kshorts():
    return ParticleCombiner(
        [_make_long_pions_for_kshort(),
         _make_long_pions_for_kshort()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Charm_Hyperons_KS0_LL_{hash}',
        CombinationCut=F.require_all(
            F.MASS > 435 * MeV,
            F.MAXDOCACUT(150 * um),
            F.MASS < 560 * MeV,
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(455 * MeV, F.MASS, 540 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.CHI2DOF < 9.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.,
        ),
    )


def _make_dd_kshorts():
    return ParticleCombiner(
        [_make_down_pions_for_kshort(),
         _make_down_pions_for_kshort()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Charm_Hyperons_KS0_DD_{hash}',
        CombinationCut=F.require_all(
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.),
            F.math.in_range(425 * MeV, F.MASS, 570 * MeV),
            F.PT > 400 * MeV,
            F.P > 4.5 * GeV,
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(455 * MeV, F.MASS, 540 * MeV),
            F.PT > 450 * MeV,
            F.P > 5 * GeV,
            F.CHI2DOF < 12.,
        ),
    )


def _make_ll_lambdas_for_charm():
    return ParticleCombiner(
        [_make_long_protons_for_lambda(),
         _make_long_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_Hyperons_LambdaFromC_LL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(make_pvs()) > 8 * mm,
            F.BPVFDCHI2(make_pvs()) > 120.,
        ),
    )


def _make_dd_lambdas():
    return ParticleCombiner(
        [_make_down_protons_for_lambda(),
         _make_down_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_Hyperons_Lambda_DD_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1150 * MeV,
            F.MAXDOCACUT(3 * mm),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1140 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 24.,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
        ),
    )


# TODO: Look at data to see if a displacement cut like BPVIP(CHI2) makes sense.
def _make_ll_lambdas_for_hyperon():
    return ParticleCombiner(
        [_make_long_protons_for_lambda(),
         _make_long_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name='Charm_Hyperons_LambdaFromHyperon_LL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 550 * MeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(make_pvs()) > 8 * mm,
            F.BPVFDCHI2(make_pvs()) > 480.,
        ),
    )


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon.
def _make_lll_xis(ll_lambdas, pions, pvs):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name='Charm_Hyperons_Xim_LLL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 9.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 4 * mm,
            F.BPVFDCHI2(pvs) > 16.,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddl_xis(dd_lambdas, pions, pvs):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name='Charm_Hyperons_Xim_DDL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(750 * um),
            F.PT > 1 * GeV,
            F.P > 14 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 1.1 * GeV,
            F.P > 15 * GeV,
            F.CHI2DOF < 9.,
            F.BPVVDZ(pvs) > 4 * mm,
            F.BPVFDCHI2(pvs) > 16.,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddd_xis(dd_lambdas, down_pions, pvs):
    return ParticleCombiner(
        [dd_lambdas, down_pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name='Charm_Hyperons_Xim_DDD_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1390 * MeV,
            F.PT > 600 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1290 * MeV,
                            F.MASS - F.CHILD(1, F.MASS) + mPDG["Lambda0"],
                            1350 * MeV),
            F.PT > 700 * MeV,
            F.CHI2DOF < 24.,
            _DZ_CHILD(1) > 4 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.BPVVDRHO(pvs) > 5 * mm,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon.
def _make_lll_omegas(ll_lambdas, kaons, pvs):
    return ParticleCombiner(
        [ll_lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name='Charm_Hyperons_Omegam_LLL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1750 * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 550 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1730 * MeV,
            F.PT > 650 * MeV,
            F.CHI2DOF < 9.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 2 * mm,
            F.BPVFDCHI2(pvs) > 12.,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddl_omegas(dd_lambdas, kaons, pvs):
    return ParticleCombiner(
        [dd_lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name='Charm_Hyperons_Omegam_DDL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1750 * MeV,
            F.MAXDOCACUT(750 * um),
            F.PT > 1 * GeV,
            F.P > 14 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1730 * MeV,
            F.PT > 1.1 * GeV,
            F.P > 15 * GeV,
            F.CHI2DOF < 9.,
            F.BPVVDZ(pvs) > 2 * mm,
            F.BPVFDCHI2(pvs) > 12.,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddd_omegas(dd_lambdas, down_kaons, pvs):
    return ParticleCombiner(
        [dd_lambdas, down_kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name='Charm_Hyperons_Omegam_DDD_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1750 * MeV,
            F.PT > 650 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1640 * MeV,
                            F.MASS - F.CHILD(1, F.MASS) + mPDG["Lambda0"],
                            1705 * MeV),
            F.PT > 750 * MeV,
            F.CHI2DOF < 24.,
            _DZ_CHILD(1) > 4 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.BPVVDRHO(pvs) > 5 * mm,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )


def _make_d_to_kpipi():
    pions = _make_long_pions_for_d()
    pvs = make_pvs()
    return ParticleCombiner(
        [_make_long_kaons_for_d(), pions, pions],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name='Charm_Hyperons_DpToKmPipPip_{hash}',
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(150 * um),
            F.MASS < 1850 * MeV,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 1990 * MeV),
            F.MAXSDOCACUT(200 * um),
            F.PT > 1.1 * GeV,
            F.P > 11 * GeV,
            F.SUM(F.PT) > 1.4 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 1970 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_ds_to_kkpi():
    kaons = _make_long_kaons_for_d()
    pvs = make_pvs()
    return ParticleCombiner(
        [kaons, kaons, _make_long_pions_for_d()],
        DecayDescriptor="[D_s+ -> K- K+ pi+]cc",
        name='Charm_Hyperons_DspToKmKpPip_{hash}',
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(150 * um),
            F.MASS < 1950 * MeV,
        ),
        CombinationCut=F.require_all(
            F.MAXSDOCACUT(200 * um),
            F.math.in_range(1750 * MeV, F.MASS, 2090 * MeV),
            F.PT > 1.1 * GeV,
            F.P > 11 * GeV,
            F.SUM(F.PT) > 1.4 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 2070 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


#########################################################
## Local bachelor particle filters used at least twice ##
#########################################################
def _make_loose_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K < 5.), ),
    )


def _make_std_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(4.),
                          F.PID_K < 5.), ),
    )


def _make_tight_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.),
                          F.PID_K < 5.), ),
    )


def _make_verytight_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(9.),
                          F.PID_K < 5.), ),
    )


def _make_std_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.P > 5 * GeV, _MIPCHI2_MIN(3.),
                          F.PID_K > 0.), ),
    )


def _make_tight_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 450 * MeV, F.P > 6 * GeV, _MIPCHI2_MIN(5.),
                          F.PID_K > 5.), ),
    )


def _make_verytight_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.P > 6 * GeV, _MIPCHI2_MIN(7.),
                          F.PID_K > 5.), ),
    )


def _make_long_tracks_for_beauty():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(9.),
            ), ),
    )


#################################################
## Local "for-b" combiners used at least twice ##
#################################################
def _make_bbaryon_to_cbaryonttrack(cbaryon, track, pvs, descriptor, suffix):
    return ParticleCombiner(
        [cbaryon, track],
        DecayDescriptor=descriptor,
        name='Charm_Hyperons_XbToXcTrack_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 6.4 * GeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 6.3 * GeV,
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.P > 25 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > -1 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_xib0_to_xicppim(xicp, pim, pvs, suffix):
    return ParticleCombiner(
        [xicp, pim],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi-]cc",
        name="Charm_Hyperons_Xib0ToXicpPim" + suffix + "_{hash}",
        CombinationCut=F.require_all(
            F.MASS > 5.3 * GeV,
            F.MASS < 6.1 * GeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 5.4 * GeV,
            F.MASS < 6.0 * GeV,
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.P > 25 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > -0.5 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_bbaryon_to_cbaryond(xc, d, pvs, descriptor, suffix):
    return ParticleCombiner(
        [xc, d],
        DecayDescriptor=descriptor,
        name='Charm_Hyperons_XbToXcD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(4.9 * GeV, F.MASS, 6.4 * GeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 3.8 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 4.5 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(5 * GeV, F.MASS, 6.3 * GeV),
            F.PT > 4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 9.,
            _DZ_CHILD(1) > -1 * mm,
            _DZ_CHILD(2) > 0 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 12.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )


def _make_detached_lc_to_lpi_ll(ll_lambdas, pions, pvs):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_DetachedLcpToL0Pip_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_lc_to_lpi_dd(dd_lambdas, pions, pvs):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_DetachedLcpToL0Pip_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
        ),
        CompositeCut=F.require_all(
            F.SUM(F.PT) > 2 * GeV,
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicp_to_lpi_ll(ll_lambdas, pions, pvs):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_DetachedXicpToL0Pip_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicp_to_lpi_dd(dd_lambdas, pions, pvs):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_DetachedXicpToL0Pip_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_ximpi_lll(lll_xis, pions, pvs):
    return ParticleCombiner(
        [lll_xis, pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_DetachedXic0ToXimPip_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_ximpi_ddl(ddl_xis, pions, pvs):
    return ParticleCombiner(
        [ddl_xis, pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_DetachedXic0ToXimPip_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_ximk_lll(lll_xis, kaons, pvs):
    return ParticleCombiner(
        [lll_xis, kaons],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_DetachedXic0ToXimKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_ximk_ddl(ddl_xis, kaons, pvs):
    return ParticleCombiner(
        [ddl_xis, kaons],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_DetachedXic0ToXimKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_oc_to_ximk_lll(lll_xis, kaons, pvs):
    return ParticleCombiner(
        [lll_xis, kaons],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_DetachedOc0ToXimKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_oc_to_ximk_ddl(ddl_xis, kaons, pvs):
    return ParticleCombiner(
        [ddl_xis, kaons],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_DetachedOc0ToXimKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_oc_to_ompi_lll(lll_oms, pions, pvs):
    return ParticleCombiner(
        [lll_oms, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name='Charm_Hyperons_DetachedOc0ToOmPip_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_oc_to_ompi_ddl(ddl_oms, pions, pvs):
    return ParticleCombiner(
        [ddl_oms, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name='Charm_Hyperons_DetachedOc0ToOmPip_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 1.3 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicp_to_ximpipi_lll(lll_xis, pions, pvs):
    return ParticleCombiner(
        [lll_xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name='Charm_Hyperons_DetachedXicpToXimPipPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicp_to_ximpipi_ddl(ddl_xis, pions, pvs):
    return ParticleCombiner(
        [ddl_xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name='Charm_Hyperons_DetachedXicpToXimPipPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1.2 * mm,
            F.DOCA(2, 3) < 400 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.9 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_lkpi_ll(ll_lambdas, kaons, pions, pvs):
    return ParticleCombiner(
        [ll_lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_DetachedXic0ToL0KmPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


def _make_detached_xicz_to_lkpi_dd(dd_lambdas, kaons, pions, pvs):
    return ParticleCombiner(
        [dd_lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_DetachedXic0ToL0KmPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1 * mm,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )


###################
## trigger lines ##
###################
# Expected rates are simple estimates using as inst. lumi 2*10^33 /(cm^2 s) measured or estimated branching fractions,
# and the following cross-section estimates: Lc 300 mub, Xic 100 mub, Omegac 40 mub (arxiv:2105.05187 and arxiv:2012.12001).
# The cross-sections should be within the acceptance, so we only have to correct for what is reconstructible as LL(L) or DDL.
# For Lambda from charm this is about 60%, Xi- (-> Lambda pi) from c 20%, Omega- (-> Lambda K) from c 40%.
all_lines = {}


###############
## inclusive ##
###############
# Xi- -> L pi- with matched Velo track
@register_line_builder(all_lines)
def xi_to_lpi_match(name="Hlt2Charm_XimToL0Pim_VeloMatch_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddd_xis = _make_ddd_xis(dd_lambdas, _make_down_pions_for_lambda(), pvs)
    matched_xis = _match_ddd_hyperon_to_velo_track(ddd_xis)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddd_xis, matched_xis],
        persistreco=True,
        raw_banks=["VP"])


# Omega- -> L K- with matched Velo track
@register_line_builder(all_lines)
def om_to_lk_match(name="Hlt2Charm_OmToL0Km_VeloMatch_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddd_omegas = _make_ddd_omegas(dd_lambdas, _make_down_kaons_for_omega(),
                                  pvs)
    matched_omegas = _match_ddd_hyperon_to_velo_track(ddd_omegas)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddd_omegas, matched_omegas],
        persistreco=True,
        raw_banks=["VP"])


#######################
## (quasi-) two-body ##
#######################
# Lc -> L pi and partially reconstructed Lc -> Sigma0 pi. Expected rate 6.2 kHz
@register_line_builder(all_lines)
def lc_to_lpi_ll_line(name="Hlt2Charm_LcpToL0Pip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_tight_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_LcpToL0Pip_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lpi_dd_line(name="Hlt2Charm_LcpToL0Pip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_tight_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_LcpToL0Pip_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lpi_llinclb_line(name="Hlt2Charm_LcpToL0Pip_LL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas,
                                      _make_verytight_pions_for_charm(), pvs)
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs, _make_long_tracks_for_beauty(), pvs,
        "[Lambda_b0 -> Lambda_c+ pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch],
        persistreco=True)


@register_line_builder(all_lines)
def lc_to_lpi_ddinclb_line(name="Hlt2Charm_LcpToL0Pip_DD_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas,
                                        _make_verytight_pions_for_charm(), pvs)
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lc_dd, _make_long_tracks_for_beauty(), pvs,
        "[Lambda_b0 -> Lambda_c+ pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch],
        persistreco=True)


@register_line_builder(all_lines)
def lb_to_lcdm_lc_to_lpi_ll_line(
        name="Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas,
                                      _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lcs, dm, pvs, "[Lambda_b0 -> Lambda_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdm_lc_to_lpi_dd_line(
        name="Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas,
                                        _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lc_dd, dm, pvs, "[Lambda_b0 -> Lambda_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdsm_lc_to_lpi_ll_line(
        name="Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas,
                                      _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lcs, dsm, pvs, "[Lambda_b0 -> Lambda_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdsm_lc_to_lpi_dd_line(
        name="Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas,
                                        _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lc_dd, dsm, pvs, "[Lambda_b0 -> Lambda_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch])


@register_line_builder(all_lines)
def lc_to_lk_ll_line(name="Hlt2Charm_LcpToL0Kp_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name='Charm_Hyperons_LcpToL0Kp_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lk_dd_line(name="Hlt2Charm_LcpToL0Kp_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name='Charm_Hyperons_LcpToL0Kp_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lk_llinclb_line(name="Hlt2Charm_LcpToL0Kp_LL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name='Charm_Hyperons_DetachedLcpToL0Kp_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs, _make_long_tracks_for_beauty(), pvs,
        "[Lambda_b0 -> Lambda_c+ pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch],
        persistreco=True)


@register_line_builder(all_lines)
def lc_to_lk_ddinclb_line(name="Hlt2Charm_LcpToL0Kp_DD_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name='Charm_Hyperons_DetachedLcpToL0Kp_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs, _make_long_tracks_for_beauty(), pvs,
        "[Lambda_b0 -> Lambda_c+ pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lcs, b_to_lch], persistreco=True)


# Xic+ -> L pi and partially reconstructed Xic -> Sigma0 pi. Expected rate 320 Hz + a bit of Lc -> L pi
@register_line_builder(all_lines)
def xicp_to_lpi_ll_line(name="Hlt2Charm_XicpToL0Pip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_XicpToL0Pip_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0.1 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lpi_dd_line(name="Hlt2Charm_XicpToL0Pip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicp_pions = _make_verytight_pions_for_charm()
    xicps = ParticleCombiner(
        [dd_lambdas, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name='Charm_Hyperons_XicpToL0Pip_DD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(1 * mm),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > 0.1 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lpi_llbinc_line(name="Hlt2Charm_XicpToL0Pip_LL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xicps,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b0 -> Xi_c+ pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xicp_to_lpi_ddbinc_line(name="Hlt2Charm_XicpToL0Pip_DD_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xicps,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b0 -> Xi_c+ pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_lpi_ll_line(
        name="Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, pvs, "[Xi_b0 -> Xi_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_lpi_dd_line(
        name="Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, pvs, "[Xi_b0 -> Xi_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_lpi_ll_line(
        name="Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, pvs, "[Xi_b0 -> Xi_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_lpi_dd_line(
        name="Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas,
                                          _make_verytight_pions_for_charm(),
                                          pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, pvs, "[Xi_b0 -> Xi_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich])


# Xic0 -> Xi- pi+.  Expected rate 380 Hz
@register_line_builder(all_lines)
def xicz_to_ximpi_lll_line(name="Hlt2Charm_Xic0ToXimPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0s = ParticleCombiner(
        [lll_xis, _make_loose_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_Xic0ToXimPip_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximpi_ddl_line(name="Hlt2Charm_Xic0ToXimPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_Xic0ToXimPip_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximpi_lllinclb_line(name="Hlt2Charm_Xic0ToXimPip_LLL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0sl,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xicz_to_ximpi_ddlinclb_line(name="Hlt2Charm_Xic0ToXimPip_DDL_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0sl,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximpi_lll_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximpi_ddl_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximpi_lll_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximpi_ddl_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich])


# Xic0 -> Xi- K+.  Expected rate 10 Hz
@register_line_builder(all_lines)
def xicz_to_ximk_lll_line(name="Hlt2Charm_Xic0ToXimKp_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0s = ParticleCombiner(
        [lll_xis, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_Xic0ToXimKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximk_ddl_line(name="Hlt2Charm_Xic0ToXimKp_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0s = ParticleCombiner(
        [ddl_xis, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_Xic0ToXimKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximk_lllinclb_line(name="Hlt2Charm_Xic0ToXimKp_LLL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0sl,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xicz_to_ximk_ddlinclb_line(name="Hlt2Charm_Xic0ToXimKp_DDL_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0sl,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximkm_lll_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximkm_ddl_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximkm_lll_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximkm_ddl_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis,
                                             _make_verytight_kaons_for_charm(),
                                             pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich])


# Xic0 -> Omega- K+. Expected rate 150 Hz
@register_line_builder(all_lines)
def xicz_to_omk_lll_line(name="Hlt2Charm_Xic0ToOmKp_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    xic0s = ParticleCombiner(
        [lll_oms, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Omega- K+]cc",
        name='Charm_Hyperons_Xic0ToOmKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xic0s])


@register_line_builder(all_lines)
def xicz_to_omk_ddl_line(name="Hlt2Charm_Xic0ToOmKp_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    xic0s = ParticleCombiner(
        [ddl_oms, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Omega- K+]cc",
        name='Charm_Hyperons_Xic0ToOmKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 3.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xic0s])


# Xic0 -> L0 KS0 ; LLLL + DDLL + LLDD + DDDD combinations
@register_line_builder(all_lines)
def xicz_to_l0ll_kshortsll_line(name="Hlt2Charm_Xic0ToL0Ks_LLLL", prescale=1):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Xic0ToL0Ks_LLLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 1 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, xic0s],
        prescale=prescale)


@register_line_builder(all_lines)
def xicz_to_l0dd_kshortsll_line(name="Hlt2Charm_Xic0ToL0Ks_DDLL", prescale=1):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xic0s = ParticleCombiner(
        [dd_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Xic0ToL0Ks_DDLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.MAXDOCACUT(2 * mm),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(2) > 1 * mm,
            F.BPVVDZ(pvs) > -5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xic0s],
        prescale=prescale)


@register_line_builder(all_lines)
def xicz_to_l0ll_kshortsdd_line(name="Hlt2Charm_Xic0ToL0Ks_LLDD", prescale=1):
    pvs = make_pvs()
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [_make_ll_lambdas_for_charm(), dd_kshorts],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Xic0ToL0Ks_LLDD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MAXDOCACUT(2 * mm),
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_kshorts, xic0s],
        prescale=prescale)


@register_line_builder(all_lines)
def xicz_to_l0dd_kshortsdd_line(name="Hlt2Charm_Xic0ToL0Ks_DDDD", prescale=1):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xic0s = ParticleCombiner(
        [dd_lambdas, _make_dd_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Xic0ToL0Ks_DDDD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.MAXDOCACUT(5 * mm),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > -10 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xic0s],
        prescale=prescale)


# Omegac0 -> Xi- pi+. Expected rate 75 Hz
@register_line_builder(all_lines)
def oc_to_ximpi_lll_line(name="Hlt2Charm_Oc0ToXimPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = ParticleCombiner(
        [lll_xis, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_Oc0ToXimPip_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximpi_ddl_line(name="Hlt2Charm_Oc0ToXimPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = ParticleCombiner(
        [ddl_xis, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- pi+]cc",
        name='Charm_Hyperons_Oc0ToXimPip_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


# Omegac0 -> Xi- K+.  Expected rate 7.5 Hz
@register_line_builder(all_lines)
def oc_to_ximk_lll_line(name="Hlt2Charm_Oc0ToXimKp_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = ParticleCombiner(
        [lll_xis, _make_std_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_Oc0ToXimKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximk_ddl_line(name="Hlt2Charm_Oc0ToXimKp_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = ParticleCombiner(
        [ddl_xis, _make_std_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name='Charm_Hyperons_Oc0ToXimKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximk_lllinclb_line(name="Hlt2Charm_Oc0ToXimKp_LLL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis,
                                           _make_verytight_kaons_for_charm(),
                                           pvs)
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc_lll, _make_long_tracks_for_beauty(), pvs,
        "[Omega_b- -> Omega_c0 pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och],
        persistreco=True)


@register_line_builder(all_lines)
def oc_to_ximk_ddlinclb_line(name="Hlt2Charm_Oc0ToXimKp_DDL_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis,
                                         _make_verytight_kaons_for_charm(),
                                         pvs)
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc0s, _make_long_tracks_for_beauty(), pvs,
        "[Omega_b- -> Omega_c0 pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och],
        persistreco=True)


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_ximkm_lll_line(
        name="Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis,
                                           _make_verytight_kaons_for_charm(),
                                           pvs)
    b_to_och = _make_bbaryon_to_cbaryond(oc_lll, _make_d_to_kpipi(), pvs,
                                         "[Omega_b- -> Omega_c0 D-]cc",
                                         "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_ximkm_ddl_line(
        name="Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis,
                                         _make_verytight_kaons_for_charm(),
                                         pvs)
    b_to_och = _make_bbaryon_to_cbaryond(oc0s, _make_d_to_kpipi(), pvs,
                                         "[Omega_b- -> Omega_c0 D-]cc",
                                         "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_ximkm_lll_line(
        name="Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis,
                                           _make_verytight_kaons_for_charm(),
                                           pvs)
    b_to_och = _make_bbaryon_to_cbaryond(oc_lll, _make_ds_to_kkpi(), pvs,
                                         "[Omega_b- -> Omega_c0 D_s-]cc",
                                         "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_ximkm_ddl_line(
        name="Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis,
                                         _make_verytight_kaons_for_charm(),
                                         pvs)
    b_to_och = _make_bbaryon_to_cbaryond(oc0s, _make_ds_to_kkpi(), pvs,
                                         "[Omega_b- -> Omega_c0 D_s-]cc",
                                         "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och])


# Omegac0 -> Omega- pi+. Expected rate 140 Hz
@register_line_builder(all_lines)
def oc_to_ompi_lll_line(name="Hlt2Charm_Oc0ToOmPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_oms, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name='Charm_Hyperons_Oc0ToOmPip_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 16 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_ompi_ddl_line(name="Hlt2Charm_Oc0ToOmPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = ParticleCombiner(
        [ddl_oms, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name='Charm_Hyperons_Oc0ToOmPip_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_ompi_lllinclb_line(name="Hlt2Charm_Oc0ToOmPip_LLL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms,
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc_lll, _make_long_tracks_for_beauty(), pvs,
        "[Omega_b- -> Omega_c0 pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och],
        persistreco=True)


@register_line_builder(all_lines)
def oc_to_ompi_ddlinclb_line(name="Hlt2Charm_Oc0ToOmPip_DDL_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms,
                                         _make_verytight_pions_for_charm(),
                                         pvs)
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc0s, _make_long_tracks_for_beauty(), pvs,
        "[Omega_b- -> Omega_c0 pi-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och],
        persistreco=True)


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_opi_lll_line(
        name="Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms,
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dm = _make_d_to_kpipi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll, dm, pvs, "[Omega_b- -> Omega_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_opi_ddl_line(
        name="Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms,
                                         _make_verytight_pions_for_charm(),
                                         pvs)
    dm = _make_d_to_kpipi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, dm, pvs, "[Omega_b- -> Omega_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_opi_lll_line(
        name="Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms,
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dsm = _make_ds_to_kkpi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll, dsm, pvs, "[Omega_b- -> Omega_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och])


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_opi_ddl_line(
        name="Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms,
                                         _make_verytight_pions_for_charm(),
                                         pvs)
    dsm = _make_ds_to_kkpi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, dsm, pvs, "[Omega_b- -> Omega_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och])


# Omegac0 -> Omega- K+. Expected rate 7 Hz
@register_line_builder(all_lines)
def oc_to_omk_lll_line(name="Hlt2Charm_Oc0ToOmKp_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = ParticleCombiner(
        [lll_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- K+]cc",
        name='Charm_Hyperons_Oc0ToOmKp_LLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(100 * um),
            F.PT > 0.9 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_omk_ddl_line(name="Hlt2Charm_Oc0ToOmKp_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    oc0s = ParticleCombiner(
        [ddl_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- K+]cc",
        name='Charm_Hyperons_Oc0ToOmKp_DDL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])


# Omegac0 -> L0 KS0 ; LLLL + DDLL + LLDD + DDDD combinations
@register_line_builder(all_lines)
def oc_to_l0ll_kshortsll_line(name="Hlt2Charm_Oc0ToL0Ks_LLLL", prescale=1):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Oc0ToL0Ks_LLLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 1 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, oc0s],
        prescale=prescale)


@register_line_builder(all_lines)
def oc_to_l0dd_kshortsll_line(name="Hlt2Charm_Oc0ToL0Ks_DDLL", prescale=1):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    oc0s = ParticleCombiner(
        [dd_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Oc0ToL0Ks_DDLL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(2 * mm),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(2) > 1 * mm,
            F.BPVVDZ(pvs) > -5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, oc0s],
        prescale=prescale)


@register_line_builder(all_lines)
def oc_to_l0ll_kshortsdd_line(name="Hlt2Charm_Oc0ToL0Ks_LLDD", prescale=1):
    pvs = make_pvs()
    dd_kshorts = _make_ll_kshorts()
    oc0s = ParticleCombiner(
        [_make_ll_lambdas_for_charm(), dd_kshorts],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Oc0ToL0Ks_LLDD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(2 * mm),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_kshorts, oc0s],
        prescale=prescale)


@register_line_builder(all_lines)
def oc_to_l0dd_kshortsdd_line(name="Hlt2Charm_Oc0ToL0Ks_DDDD", prescale=1):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    oc0s = ParticleCombiner(
        [dd_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name='Charm_Hyperons_Oc0ToL0Ks_DDDD_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(5 * mm),
            F.PT > 0.9 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 6.,
            F.BPVVDZ(pvs) > -10 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, oc0s],
        prescale=prescale)


#########################
## (quasi-) three-body ##
#########################
# Omega -> Xi pi pi. Expected rate ?
@register_line_builder(all_lines)
def o_to_xipipi_line(name="Hlt2Charm_OmToXimPimPip_LongXim_SP"):
    pvs = make_pvs()
    xis = ParticleFilter(
        make_long_xis(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV,
                F.P > 25 * GeV,
                _MIPCHI2_MIN(2.),
            ), ),
    )
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1.5 * GeV,
                _MIPCHI2_MIN(48.),
            ), ),
    )
    omegas = ParticleCombiner(
        [xis, pions, pions],
        DecayDescriptor="[Omega- -> Xi- pi- pi+]cc",
        name='Charm_Hyperons_OmToXimPimPip_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1631 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(F.MASS < 1770 * MeV, F.MAXDOCACUT(
            250 * um), F.PT > 1 * GeV, F.P > 16 * GeV),
        CompositeCut=F.require_all(
            F.MASS < 1750 * MeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 200 * mm,
            F.BPVFDCHI2(pvs) > 1000.,
            F.BPVIPCHI2(pvs) < 24.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [omegas],
        #extra_outputs=[("RichRawBanks", default_raw_event(["Rich"]))]
    )


# Lc -> L KS K and partially reconstructed Lc -> Sigma0 KS K. Expected rate 1.2 kHz
@register_line_builder(all_lines)
def lc_to_lksk_ll_line(name="Hlt2Charm_LcpToL0KsKp_LLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    lcs = ParticleCombiner(
        [ll_lambdas, ll_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_LcpToL0KsKp_LLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS < 2405 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_lksk_dl_line(name="Hlt2Charm_LcpToL0KsKp_DDLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    lcs = ParticleCombiner(
        [dd_lambdas, ll_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_LcpToL0KsKp_DDLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 600 * um,
            F.DOCA(2, 3) < 250 * um,
            F.MASS < 2405 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_lksk_ld_line(name="Hlt2Charm_LcpToL0KsKp_LLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    lcs = ParticleCombiner(
        [ll_lambdas, dd_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_LcpToL0KsKp_LLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 800 * um,
            F.MASS < 2405 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, lcs])


# Lb0 -> Lc+ pi- with Lc+ -> S- pi+ pi+. Expected rate negligible
@register_line_builder(all_lines)
def lcp_to_smkpi_longsm_line(
        name="Hlt2Charm_Lb0ToLcpPim_LcpToSmPipPip_LongSm_SP"):
    pvs = make_pvs()
    pions = _make_verytight_pions_for_charm()
    sigmas = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 22 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_P > 7.,
            ), ),
    )
    lcs = ParticleCombiner(
        [sigmas, pions, pions],
        DecayDescriptor="[Lambda_c+ -> Sigma- pi+ pi+]cc",
        name='Charm_Hyperons_LcpToSmPipPip_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2165 * MeV,
            F.MASS < 2405 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 2185 * MeV,
            F.MASS < 2385 * MeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 48.,
        ),
    )
    lbs = ParticleCombiner(
        [lcs, _make_long_tracks_for_beauty()],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi-]cc",
        name='Charm_Hyperons_Lb0ToLcpPim_{hash}',
        CombinationCut=F.require_all(
            F.MASS > 5.2 * GeV,
            F.MASS < 6.0 * GeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 5.3 * GeV,
            F.MASS < 5.9 * GeV,
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.P > 25 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > -0.5 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 24.,
        ),
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lcs, lbs],
        #extra_outputs=[("RichRawBanks", default_raw_event(["Rich"]))]
    )


# Lc -> Xi K pi. Expected rate 500 Hz
@register_line_builder(all_lines)
def lc_to_ximkpi_lll_line(name="Hlt2Charm_LcpToXimKpPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    lcs = ParticleCombiner(
        [lll_xis,
         _make_std_kaons_for_charm(),
         _make_std_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Xi- K+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKpPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, lcs])


@register_line_builder(all_lines)
def lc_to_ximkpi_ddl_line(name="Hlt2Charm_LcpToXimKpPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    lcs = ParticleCombiner(
        [ddl_xis,
         _make_std_kaons_for_charm(),
         _make_std_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Xi- K+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKpPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1.2 * mm,
            F.DOCA(2, 3) < 400 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, lcs])


# Xic+ -> L KS pi and partially reconstructed Xic+ -> Sigma0 KS pi. Expected rate 2.9 kHz
@register_line_builder(all_lines)
def xicp_to_lkspi_ll_line(name="Hlt2Charm_XicpToL0KsPip_LLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts,
         _make_tight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name='Charm_Hyperons_XicpToL0KsPip_LLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicp_to_lkspi_dl_line(name="Hlt2Charm_XicpToL0KsPip_DDLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts,
         _make_tight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name='Charm_Hyperons_XicpToL0KsPip_DDLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 600 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicp_to_lkspi_ld_line(name="Hlt2Charm_XicpToL0KsPip_LLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts,
         _make_tight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name='Charm_Hyperons_XicpToL0KsPip_LLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 800 * um,
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps])


# Xic+ -> L KS K and partially reconstructed Xic+ -> Sigma0 KS K. Expected rate 80 Hz
@register_line_builder(all_lines)
def xicp_to_lksk_ll_line(name="Hlt2Charm_XicpToL0KsKp_LLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_XicpToL0KsKp_LLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicp_to_lksk_dl_line(name="Hlt2Charm_XicpToL0KsKp_DDLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_XicpToL0KsKp_DDLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 500 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicp_to_lksk_ld_line(name="Hlt2Charm_XicpToL0KsKp_LLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts,
         _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name='Charm_Hyperons_XicpToL0KsKp_LLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 500 * um,
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps])


# Xib0 -> Xic+ pi- with Xic+ -> S+ K- pi+. Expected rate negligible
@register_line_builder(all_lines)
def xicp_to_spkpi_longsp_line(
        name="Hlt2Charm_Xib0ToXicpPim_XicpToSpKmPip_LongSp_SP"):
    pvs = make_pvs()
    kaons = _make_tight_kaons_for_charm()
    pions = _make_verytight_pions_for_charm()
    sigmas = ParticleFilter(
        make_long_sigmaps(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 22 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_P > 7.,
            ), ),
    )
    xicps = ParticleCombiner(
        [
            sigmas,
            kaons,
            pions,
        ],
        DecayDescriptor="[Xi_c+ -> Sigma+ K- pi+]cc",
        name='Charm_Hyperons_XicpToSpKmPip_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2350 * MeV,
            F.MASS < 2590 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 2370 * MeV,
            F.MASS < 2570 * MeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 48.,
        ),
    )
    xib0s = _make_xib0_to_xicppim(xicps, _make_long_tracks_for_beauty(), pvs,
                                  "_for_" + name)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xib0s],
        #extra_outputs=[("RichRawBanks", default_raw_event(["Rich"]))]
    )


# Xic+ -> Xi- pi+ pi+. Expected rate 800 Hz
@register_line_builder(all_lines)
def xicp_to_ximpipi_lll_line(name="Hlt2Charm_XicpToXimPipPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [lll_xis, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name='Charm_Hyperons_XicpToXimPipPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximpipi_ddl_line(name="Hlt2Charm_XicpToXimPipPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ddl_xis, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name='Charm_Hyperons_XicpToXimPipPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1.2 * mm,
            F.DOCA(2, 3) < 400 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.9 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximpipi_lllinclb_line(
        name="Hlt2Charm_XicpToXimPipPip_LLL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xicps,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b0 -> Xi_c+ pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xicp_to_ximpipi_ddlinclb_line(
        name="Hlt2Charm_XicpToXimPipPip_DDL_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xicps,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b0 -> Xi_c+ pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_ximpipi_lll_line(
        name="Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, pvs, "[Xi_b0 -> Xi_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_ximpipi_ddl_line(
        name="Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, pvs, "[Xi_b0 -> Xi_c+ D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_ximpipi_lll_line(
        name="Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_lll(
        lll_xis, _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, pvs, "[Xi_b0 -> Xi_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_ximpipi_ddl_line(
        name="Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = _make_detached_xicp_to_ximpipi_ddl(
        ddl_xis, _make_verytight_pions_for_charm(), pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, pvs, "[Xi_b0 -> Xi_c+ D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich])


@register_line_builder(all_lines)
def xicp_to_ximpipi_longxim_line(
        name="Hlt2Charm_Xib0ToXicpPim_XicpToXimPipPip_LongXi_SP"):
    pvs = make_pvs()
    pions = _make_verytight_pions_for_charm()
    xis = ParticleFilter(
        make_long_xis(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 25 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_P > 7.,
            ), ),
    )
    xicps = ParticleCombiner(
        [xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXiPipPip_XiLong",
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2350 * MeV,
            F.MASS < 2590 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 2370 * MeV,
            F.MASS < 2570 * MeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 48.,
        ),
    )
    xib0s = _make_xib0_to_xicppim(xicps, _make_long_tracks_for_beauty(), pvs,
                                  "_for_" + name)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xib0s],
        #extra_outputs=[("RichRawBanks", default_raw_event(["Rich"]))]
    )


# Xic+ -> Xi- K+ pi+. Expected rate 15 Hz
@register_line_builder(all_lines)
def xicp_to_ximkpi_lll_line(name="Hlt2Charm_XicpToXimKpPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = ParticleCombiner(
        [
            lll_xis,
            _make_tight_kaons_for_charm(),
            _make_verytight_pions_for_charm()
        ],
        DecayDescriptor="[Xi_c+ -> Xi- K+ pi+]cc",
        name='Charm_Hyperons_XicpToXimKpPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximkpi_ddl_line(name="Hlt2Charm_XicpToXimKpPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    xicps = ParticleCombiner(
        [
            ddl_xis,
            _make_tight_kaons_for_charm(),
            _make_verytight_pions_for_charm()
        ],
        DecayDescriptor="[Xi_c+ -> Xi- K+ pi+]cc",
        name='Charm_Hyperons_XicpToXimKpPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1.2 * mm,
            F.DOCA(2, 3) < 400 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 12.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps])


# Xic+ -> Omega- K+ pi+. Expected rate 75 Hz
@register_line_builder(all_lines)
def xicp_to_omkpi_lll_line(name="Hlt2Charm_XicpToOmKpPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    xicps = ParticleCombiner(
        [
            lll_oms,
            _make_tight_kaons_for_charm(),
            _make_tight_pions_for_charm()
        ],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name='Charm_Hyperons_XicpToOmKpPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xicps])


@register_line_builder(all_lines)
def xicp_to_omkpi_ddl_line(name="Hlt2Charm_XicpToOmKpPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    xicps = ParticleCombiner(
        [
            ddl_oms,
            _make_tight_kaons_for_charm(),
            _make_tight_pions_for_charm()
        ],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name='Charm_Hyperons_XicpToOmKpPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 1.2 * mm,
            F.DOCA(2, 3) < 400 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 6.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 9.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xicps])


@register_line_builder(all_lines)
def xicp_to_omkpi_longom_line(
        name="Hlt2Charm_Xib0ToXicpPim_XicpToOmKpPip_LongOm_SP"):
    pvs = make_pvs()
    kaons = _make_tight_kaons_for_charm()
    pions = _make_verytight_pions_for_charm()
    omegas = ParticleFilter(
        make_long_omegas(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 32 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_P > 7.,
            ), ),
    )
    xicps = ParticleCombiner(
        [omegas, kaons, pions],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name='Charm_Hyperons_XicpToOmKpPip_OmLong_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2350 * MeV,
            F.MASS < 2590 * MeV,
            F.MAXDOCACUT(100 * um),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 2370 * MeV,
            F.MASS < 2570 * MeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 1 * mm,
            F.BPVFDCHI2(pvs) > 48.,
        ),
    )
    xib0s = _make_xib0_to_xicppim(xicps, _make_long_tracks_for_beauty(), pvs,
                                  "_for_" + name)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicps, xib0s],
        #extra_outputs=[("RichRawBanks", default_raw_event(["Rich"]))]
    )


# Xic0 -> L pi- pi+ and partially reconstructed Xic0 -> Sigma0 pi- pi+. Expected rate 1.6 kHz
@register_line_builder(all_lines)
def xicz_to_lpipi_ll_line(name="Hlt2Charm_Xic0ToL0PimPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicz_pions = _make_tight_pions_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0PimPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 100 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lpipi_dd_line(name="Hlt2Charm_Xic0ToL0PimPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicz_pions = _make_tight_pions_for_charm()
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0PimPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 150 * um,
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


# Xic0 -> L K- pi+ and partially reconstructed Xic0 -> Sigma0 K- pi+. Expected rate 2.3 kHz
@register_line_builder(all_lines)
def xicz_to_lkpi_ll_line(name="Hlt2Charm_Xic0ToL0KmPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicz_pions = _make_std_pions_for_charm()
    long_xicz_kaons = _make_tight_kaons_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, long_xicz_kaons, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0KmPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lkpi_dd_line(name="Hlt2Charm_Xic0ToL0KmPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicz_pions = _make_std_pions_for_charm()
    long_xicz_kaons = _make_tight_kaons_for_charm()
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_kaons, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0KmPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(700 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.MAX(F.MINIPCHI2(pvs)) > 9.,
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lkpi_llinclb_line(name="Hlt2Charm_Xic0ToL0KmPip_LL_Inclb_PR"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(ll_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0s,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xicz_to_lkpi_ddinclb_line(name="Hlt2Charm_Xic0ToL0KmPip_DD_Inclb_PR"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(dd_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    b_to_xich = _make_bbaryon_to_cbaryonttrack(xic0s,
                                               _make_long_tracks_for_beauty(),
                                               pvs, "[Xi_b- -> Xi_c0 pi-]cc",
                                               "_for_" + name)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich],
        persistreco=True)


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_lkpi_ll_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(ll_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_lkpi_dd_line(
        name="Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(dd_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dm, pvs, "[Xi_b- -> Xi_c0 D-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_lkpi_ll_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(ll_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_lkpi_dd_line(
        name="Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(dd_lambdas,
                                           _make_verytight_kaons_for_charm(),
                                           _make_verytight_pions_for_charm(),
                                           pvs)
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dsm, pvs, "[Xi_b- -> Xi_c0 D_s-]cc", "_for_" + name)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich])


# Xic0 -> L K K and partially reconstructed Xic0 -> Sigma0 K- K+. Expected rate 128 Hz
@register_line_builder(all_lines)
def xicz_to_lkk_ll_line(name="Hlt2Charm_Xic0ToL0KmKp_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicz_kaons = _make_tight_kaons_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, long_xicz_kaons, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- K+]cc",
        name='Charm_Hyperons_Xic0ToL0KmKp_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 100 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2280 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lkk_dd_line(name="Hlt2Charm_Xic0ToL0KmKp_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicz_kaons = _make_tight_kaons_for_charm()
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_kaons, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- K+]cc",
        name='Charm_Hyperons_Xic0ToL0KmKp_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2280 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


# Xic0 -> Xi KS pi. Expected rate 10 Hz
@register_line_builder(all_lines)
def xicz_to_ximkspi_ll_line(name="Hlt2Charm_Xic0ToXimKsPip_LLLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Xic0ToXimKsPip_LLLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximkspi_ld_line(name="Hlt2Charm_Xic0ToXimKsPip_LLLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    long_xicz_pions = _make_std_pions_for_charm()
    xic0s = ParticleCombiner(
        [lll_xis, dd_kshorts, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Xic0ToXimKsPip_LLLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximkspi_dl_line(name="Hlt2Charm_Xic0ToXimKsPip_DDLLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Xic0ToXimKsPip_DDLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximkspi_dd_line(name="Hlt2Charm_Xic0ToXimKsPip_DDLDD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, dd_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Xic0ToXimKsPip_DDLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, xic0s])


# Xic0 -> Xi KS K. Expected rate 15 Hz
@register_line_builder(all_lines)
def xicz_to_ximksk_ll_line(name="Hlt2Charm_Xic0ToXimKsKp_LLLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name='Charm_Hyperons_Xic0ToXimKsKp_LLLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximksk_ld_line(name="Hlt2Charm_Xic0ToXimKsKp_LLLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, dd_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name='Charm_Hyperons_Xic0ToXimKsKp_LLLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximksk_dl_line(name="Hlt2Charm_Xic0ToXimKsKp_DDLLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    long_xicz_kaons = _make_std_kaons_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, ll_kshorts, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name='Charm_Hyperons_Xic0ToXimKsKp_DDLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.993,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximksk_dd_line(name="Hlt2Charm_Xic0ToXimKsKp_DDLDD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, dd_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name='Charm_Hyperons_Xic0ToXimKsKp_DDLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.99,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, xic0s])


# Oc -> L K- pi+ and partially reconstructed Oc -> Sigma0 K- pi+. Expected rate 450 Hz
@register_line_builder(all_lines)
def oc_to_lkpi_ll_line(name="Hlt2Charm_Oc0ToL0KmPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    oc0s = ParticleCombiner(
        [
            ll_lambdas,
            _make_tight_kaons_for_charm(),
            _make_tight_pions_for_charm()
        ],
        DecayDescriptor="[Omega_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_Oc0ToL0KmPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )

    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, oc0s])


@register_line_builder(all_lines)
def oc_to_lkpi_dd_line(name="Hlt2Charm_Oc0ToL0KmPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    oc0s = ParticleCombiner(
        [
            dd_lambdas,
            _make_tight_kaons_for_charm(),
            _make_tight_pions_for_charm()
        ],
        DecayDescriptor="[Omega_c0 -> Lambda0 K- pi+]cc",
        name='Charm_Hyperons_Oc0ToL0KmPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(700 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, oc0s])


# Oc -> L K K and partially reconstructed Oc -> Sigma0 K- K+. Expected rate 45 Hz
@register_line_builder(all_lines)
def oc_to_lkk_ll_line(name="Hlt2Charm_Oc0ToL0KmKp_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_oc_kaons = _make_tight_kaons_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, long_oc_kaons, long_oc_kaons],
        DecayDescriptor="[Omega_c0 -> Lambda0 K- K+]cc",
        name='Charm_Hyperons_Oc0ToL0KmKp_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2320 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 100 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2490 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2510 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, oc0s])


@register_line_builder(all_lines)
def oc_to_lkk_dd_line(name="Hlt2Charm_Oc0ToL0KmKp_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_oc_kaons = _make_tight_kaons_for_charm()
    oc0s = ParticleCombiner(
        [dd_lambdas, long_oc_kaons, long_oc_kaons],
        DecayDescriptor="[Omega_c0 -> Lambda0 K- K+]cc",
        name='Charm_Hyperons_Oc0ToL0KmKp_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2320 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 250 * um,
            F.math.in_range(2490 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2510 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, oc0s])


# Oc -> Xi KS pi. Expected rate 140 Hz
@register_line_builder(all_lines)
def oc_to_ximkspi_ll_line(name="Hlt2Charm_Oc0ToXimKsPip_LLLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    oc0s = ParticleCombiner(
        [lll_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKsPip_LLLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 17 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 18 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_ximkspi_ld_line(name="Hlt2Charm_Oc0ToXimKsPip_LLLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    oc0s = ParticleCombiner(
        [lll_xis, dd_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKsPip_LLLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.994,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, dd_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_ximkspi_dl_line(name="Hlt2Charm_Oc0ToXimKsPip_DDLLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    oc0s = ParticleCombiner(
        [ddl_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKsPip_DDLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.994,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_ximkspi_dd_line(name="Hlt2Charm_Oc0ToXimKsPip_DDLDD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    oc0s = ParticleCombiner(
        [ddl_xis, dd_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- KS0 pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKsPip_DDLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 1 * mm,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.992,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, dd_kshorts, oc0s])


########################
## (quasi-) four-body ##
########################
# Lc -> L pi+ pi+ pi-. Expected rate 15 kHz
#TO-DO: Maybe include D2DVVDCHI2_MIN = 60.0 cut present at Run2
@register_line_builder(all_lines)
def lc_to_l0pipipi_ll_line(name="Hlt2Charm_LcpToL0PimPipPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, long_lc_pions, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name='Charm_Hyperons_LcpToL0PimPipPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2240 * MeV,
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2201 * MeV, F.MASS, 2371 * MeV),
            # F.PT > None, # 1.3 * GeV # Possible cuts for future tunning
            # F.P > None, # 28 * GeV # Possible cuts for future tunning
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(250. * um),
            F.MAXDOCACHI2CUT(12.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2211 * MeV, F.MASS, 2361 * MeV),
            # F.PT > None, # 1.4 * GeV # Possible cuts for future tunning
            # F.P > None, # 30 * GeV # Possible cuts for future tunning
            F.CHI2DOF < 12.,
            _DZ_CHILD(1) > 4 * mm,
            # F.BPVVDZ(pvs) > None, # 0.25 * mm # Possible cuts for future tunning
            F.BPVFDCHI2(pvs) > 4.,
            # F.BPVIPCHI2(pvs) < None, # 10. # Possible cuts for future tunning
            F.BPVDIRA(pvs) > 0.998,
            F.BPVLTIME(pvs) > 0.1 * ps,
        ),
    )

    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_l0pipipi_dd_line(name="Hlt2Charm_LcpToL0PimPipPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [dd_lambdas, long_lc_pions, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name='Charm_Hyperons_LcpToL0PimPipPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2240 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2201 * MeV, F.MASS, 2371 * MeV),
            # F.PT > None, # 1.3 * GeV # Possible cuts for future tunning
            # F.P > None, # 28 * GeV # Possible cuts for future tunning
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2211 * MeV, F.MASS, 2361 * MeV),
            # F.PT > None, # 1.4 * GeV # Possible cuts for future tunning
            # F.P > None, # 30 * GeV # Possible cuts for future tunning
            F.CHI2DOF < 12,
            # F.BPVVDZ(pvs) > None, # 0.25 * mm # Possible cuts for future tunning
            F.BPVFDCHI2(pvs) > 4.,
            # F.BPVIPCHI2(pvs) < None, # 10. # Possible cuts for future tunning
            F.BPVDIRA(pvs) > 0.996,
            F.BPVLTIME(pvs) > 0.1 * ps,
        ),
    )

    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


# Lc -> L K- K+ pi+. Expected rate 480 Hz
@register_line_builder(all_lines)
def lc_to_lkkpi_ll_line(name="Hlt2Charm_LcpToL0KmKpPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_lc_kaons = _make_std_kaons_for_charm()
    lcs = ParticleCombiner(
        [
            ll_lambdas, long_lc_kaons, long_lc_kaons,
            _make_loose_pions_for_charm()
        ],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K- K+ pi+]cc",
        name='Charm_Hyperons_LcpToL0KmKpPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1775 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.MASS < 2405 * MeV,
            F.PT > 1.1 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 1.2 * GeV,
            F.P > 25 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lkkpi_dd_line(name="Hlt2Charm_LcpToL0KmKpPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_lc_kaons = _make_std_kaons_for_charm()
    lcs = ParticleCombiner(
        [
            dd_lambdas, long_lc_kaons, long_lc_kaons,
            _make_loose_pions_for_charm()
        ],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K- K+ pi+]cc",
        name='Charm_Hyperons_LcpToL0KmKpPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1775 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.MASS < 2405 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 8.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


# Lc -> Xi KS pi pi. Expected rate 40 Hz
@register_line_builder(all_lines)
def lc_to_ximkspipi_ll_line(name="Hlt2Charm_LcpToXimKsPipPip_LLLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [lll_xis, ll_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKsPipPip_LLLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 250 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.1 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_ximkspipi_ld_line(name="Hlt2Charm_LcpToXimKsPipPip_LLLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [lll_xis, dd_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKsPipPip_LLLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 1 * mm,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 1 * mm,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 24 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 26 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.994,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_ximkspipi_dl_line(name="Hlt2Charm_LcpToXimKsPipPip_DDLLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    ll_kshorts = _make_ll_kshorts()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [ddl_xis, ll_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKsPipPip_DDLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 24 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 26 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.994,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_ximkspipi_dd_line(name="Hlt2Charm_LcpToXimKsPipPip_DDLDD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    dd_kshorts = _make_dd_kshorts()
    long_lc_pions = _make_std_pions_for_charm()
    lcs = ParticleCombiner(
        [ddl_xis, dd_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name='Charm_Hyperons_LcpToXimKsPipPip_DDLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 1 * mm,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 750 * um,
            F.DOCA(2, 4) < 1 * mm,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.992,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, lcs])


# Xic+ -> L K- K+ pi+ and partially reconstructed Xic+ -> Sigma0 K- K+ pi+. Expected rate 30 Hz
@register_line_builder(all_lines)
def xicp_to_lkkpi_ll_line(name="Hlt2Charm_XicpToL0KmKpPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicp_kaons = _make_tight_kaons_for_charm()
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, long_xicp_kaons, long_xicp_kaons, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- K+ pi+]cc",
        name='Charm_Hyperons_XicpToL0KmKpPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1957 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2300 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2320 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lkkpi_dd_line(name="Hlt2Charm_XicpToL0KmKpPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicp_kaons = _make_tight_kaons_for_charm()
    long_xicp_pions = _make_std_pions_for_charm()
    xicp_DD = ParticleCombiner(
        [dd_lambdas, long_xicp_kaons, long_xicp_kaons, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- K+ pi+]cc",
        name='Charm_Hyperons_XicpToL0KmKpPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 1957 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2300 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2320 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 8.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 8.,
            F.BPVIPCHI2(pvs) < 6.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicp_DD])


# Xic+ -> L K- pi+ pi+ and partially reconstructed Xic+ -> Sigma0 K- pi+ pi+. Expected rate 1.5 kHz
@register_line_builder(all_lines)
def xicp_to_lkpipi_ll_line(name="Hlt2Charm_XicpToL0KmPipPip_LL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicp_kaons = _make_std_kaons_for_charm()
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, long_xicp_kaons, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name='Charm_Hyperons_XicpToL0KmPipPip_LL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lkpipi_dd_line(name="Hlt2Charm_XicpToL0KmPipPip_DD"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    long_xicp_kaons = _make_std_kaons_for_charm()
    long_xicp_pions = _make_std_pions_for_charm()
    xicp_DD = ParticleCombiner(
        [dd_lambdas, long_xicp_kaons, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name='Charm_Hyperons_XicpToL0KmPipPip_DD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 750 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 23 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 24 * GeV,
            F.CHI2DOF < 8.,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 6.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicp_DD])


# Xic0 -> L KS pi pi and partially reconstructed Xic0 -> Sigma0 KS pi pi. Expected rate 300 Hz
@register_line_builder(all_lines)
def xicz_to_lkspipi_ll_line(name="Hlt2Charm_Xic0ToL0KsPimPip_LLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0KsPimPip_LLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicz_to_lkspipi_dl_line(name="Hlt2Charm_Xic0ToL0KsPimPip_DDLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0KsPimPip_DDLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 600 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 600 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps])


@register_line_builder(all_lines)
def xicz_to_lkspipi_ld_line(name="Hlt2Charm_Xic0ToL0KsPimPip_LLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToL0KsPimPip_LLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 800 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 800 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps])


# Xic0 -> Xi- pi- pi+ pi+.  Expected rate 1.3 kHz
@register_line_builder(all_lines)
def xicz_to_xim3pi_lll_line(name="Hlt2Charm_Xic0ToXimPimPipPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0_LLL = ParticleCombiner(
        [lll_xis, long_xicz_pions, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name='Charm_Hyperons_Xic0ToXimPimPipPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0_LLL])


@register_line_builder(all_lines)
def xicz_to_xim3pi_ddl_line(name="Hlt2Charm_Xic0ToXimPimPipPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, long_xicz_pions, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name='Charm_Hyperons_Xic0ToXimPimPipPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


# Xic0 -> Omega- K+ pi- pi+. Expected rate 35 Hz
@register_line_builder(all_lines)
def xicz_to_omkppippim_lll_line(name="Hlt2Charm_Xic0ToOmKpPimPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    long_xicz_kaons = _make_std_kaons_for_charm()
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0_LLL = ParticleCombiner(
        [lll_oms, long_xicz_kaons, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Omega- K+ pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToOmKpPimPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.MASS < 2590 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2570 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xic0_LLL])


@register_line_builder(all_lines)
def xicz_to_omkppippim_ddl_line(name="Hlt2Charm_Xic0ToOmKpPimPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    long_xicz_kaons = _make_std_kaons_for_charm()
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_oms, long_xicz_kaons, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Omega- K+ pi- pi+]cc",
        name='Charm_Hyperons_Xic0ToOmKpPimPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 800 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 800 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.MASS < 2590 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2570 * MeV,
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xic0s])


# Oc -> L KS K- pi+ and partially reconstructed Oc -> Sigma0 KS K- pi+. Expected rate 400 Hz
@register_line_builder(all_lines)
def oc_to_lkskpi_ll_line(name="Hlt2Charm_Oc0ToL0KsKmPip_LLLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    long_oc_kaons = _make_std_kaons_for_charm()
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, ll_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name='Charm_Hyperons_Oc0ToL0KsKmPip_LLLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_lkskpi_dl_line(name="Hlt2Charm_Oc0ToL0KsKmPip_DDLL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    long_oc_kaons = _make_std_kaons_for_charm()
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [dd_lambdas, ll_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name='Charm_Hyperons_Oc0ToL0KsKmPip_DDLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 600 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 600 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(2) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.992,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_lkskpi_ld_line(name="Hlt2Charm_Oc0ToL0KsKmPip_LLDD"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    long_oc_kaons = _make_std_kaons_for_charm()
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, dd_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name='Charm_Hyperons_Oc0ToL0KsKmPip_LLDD_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(1 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 800 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 800 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.992,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, oc0s])


# Omegac0 -> Xi- K- pi+ pi+.  Expected rate 64 Hz
@register_line_builder(all_lines)
def oc_to_ximkpipi_lll_line(name="Hlt2Charm_Oc0ToXimKmPipPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi(), pvs)
    long_oc_kaons = _make_std_kaons_for_charm()
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_xis, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKmPipPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximkpipi_ddl_line(name="Hlt2Charm_Oc0ToXimKmPipPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi(), pvs)
    long_oc_kaons = _make_std_kaons_for_charm()
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [ddl_xis, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name='Charm_Hyperons_Oc0ToXimKmPipPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 1 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 1 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


# Oc -> Omega- pi- pi+ pi+.  Expected rate 42 Hz
@register_line_builder(all_lines)
def oc_to_om3pi_lll_line(name="Hlt2Charm_Oc0ToOmPimPipPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_oms, long_oc_pions, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi- pi+ pi+]cc",
        name='Charm_Hyperons_Oc0ToOmPimPipPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_om3pi_ddl_line(name="Hlt2Charm_Oc0ToOmPimPipPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [ddl_oms, long_oc_pions, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi- pi+ pi+]cc",
        name='Charm_Hyperons_Oc0ToOmPimPipPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 1 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 1 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])


# Oc -> Omega- K+ pi- pi+.  Expected rate 7 Hz
@register_line_builder(all_lines)
def oc_to_omkppipi_lll_line(name="Hlt2Charm_Oc0ToOmKpPimPip_LLL"):
    pvs = make_pvs()
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega(), pvs)
    long_oc_pions = _make_loose_pions_for_charm()
    long_oc_kaons = _make_std_kaons_for_charm()
    oc0s = ParticleCombiner(
        [lll_oms, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- K+ pi- pi+ ]cc",
        name='Charm_Hyperons_Oc0ToOmKpPimPip_LLL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_omkppipi_ddl_line(name="Hlt2Charm_Oc0ToOmKpPimPip_DDL"):
    pvs = make_pvs()
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega(), pvs)
    long_oc_pions = _make_loose_pions_for_charm()
    long_oc_kaons = _make_std_kaons_for_charm()
    oc0s = ParticleCombiner(
        [ddl_oms, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- K+ pi- pi+]cc",
        name='Charm_Hyperons_Oc0ToOmKpPimPip_DDL_{hash}',
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(800 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 1 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 1 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.,
            _DZ_CHILD(1) > 2 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.,
            F.BPVIPCHI2(pvs) < 9.,
            F.BPVDIRA(pvs) > 0.995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])

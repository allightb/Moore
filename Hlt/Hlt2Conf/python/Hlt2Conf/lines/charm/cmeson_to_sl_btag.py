###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Charmed mesons from beauty decays (Btag) along with WS lines and normalization lines:
   Note: 
   The mass window for B~0 and D+ are wide enough to include B_s~0 and Ds+

    1. B~0 ->  D+ pi- pi- pi+, D+ ->  K+ K- mu+ nu : 
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMupNu
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMumNu_WS
        Hlt2Charm_B0bToDpPimPimPim_DpToKpKmMupNu_WS
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmPip

    2. B~0 ->  D+ pi- pi- pi+, D+ ->   pi+ pi- mu+ nu :
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMupNu
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMumNu_WS
        Hlt2Charm_B0bToDpPimPimPim_DpToKpKmMupNu_WS
        Hlt2Charm_B0bToDpPipPimPim_DpToKpKmPip

    3. B- ->  D0 pi- pi- pi+, D0 ->   eta pi- mu+ nu, eta -> pi+ pi- pi0(gamma):
        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R
        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimPi0_R_WS
        Hlt2Charm_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R_WS
        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimPi0_R

        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimG
        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimG_WS
        Hlt2Charm_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimG_WS
        Hlt2Charm_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimG

    4. B- ->  D0 pi- pi- pi+, D0 ->   pi- pi+ pi- mu+ nu:
        Hlt2Charm_BmToD0PipPimPim_D0ToPipPimPimMupNu
        Hlt2Charm_BmToD0PimPimPim_D0ToPipPimPimMupNu_WS
        Hlt2Charm_BmToD0PipPimPim_D0ToPimPimPipMumNu_WS
        Hlt2Charm_BmToD0PipPimPim_D0ToPipPipPimPim

    5. B- ->  D0 pi- pi- pi+, D0 ->   pi- K+ K- mu+ nu :
        Hlt2Charm_BmToD0PipPimPim_D0ToKpKmPimMupNu
        Hlt2Charm_BmToD0PipPimPim_D0ToKmKmPipMumNu_WS
        Hlt2Charm_BmToD0PimPimPim_D0ToKpKmPimMupNu_WS
        Hlt2Charm_BmToD0PipPimPim_D0ToKpKmPipPim


Proponents: Xiao-Rui Lyu, Yangjie Su
TODO: refine eta selection 
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps

import Functors as F
from Functors.math import in_range
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.standard_particles import (make_has_rich_long_kaons,
                                         make_has_rich_long_pions,
                                         make_ismuon_long_muon, make_photons)
from Hlt2Conf.algorithms_thor import (ParticleFilter, ParticleCombiner)
from .prefilters import charm_prefilters
from .particle_properties import _PI0_M

all_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


###############################################################################
# Track filters
###############################################################################
def _filter_long_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 250 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(8.),
                          F.PID_K < 5.), ))


def _filter_long_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 350 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(8.),
                          F.PID_K > 5.), ))


def _filter_long_muons_loose():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(8.),
                F.PID_MU > 0.,
            ), ),
    )


def _filter_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(8.),
                F.PID_MU > 0.,
            ), ),
    )


def _filter_tight_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 1 * GeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(9.),
                F.PID_MU > 3.,
                #trghostprob_max=None, # TODO
            ), ),
    )


# Credit to authors of charm/d_to_etah.py
def _filter_pions_from_eta_loose():
    cut = F.require_all(
        _MIPCHI2_MIN(16.),
        F.PT > 350. * MeV,
        #TODO Selections commented out for the start of Run 3
        #trghostprob_max=0.4,
        #trchi2dof_max=3,
        F.PID_K < 5)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def _filter_neutrals_from_eta_loose(particles):
    """Particles might be photons or pi0s."""
    return ParticleFilter(particles, F.FILTER(F.PT > 350 * MeV))


def _make_photons_from_pi0s():
    return make_photons(PtCut=200 * MeV)


def _make_resolved_pi0s():
    photons = _make_photons_from_pi0s()
    combination_code = in_range(_PI0_M - 30 * MeV, F.MASS, _PI0_M + 30 * MeV)
    composite_code = F.PT > 350 * MeV
    return ParticleCombiner(
        Inputs=[photons, photons],
        name="CMesonToSl_Resolved_Pi0_{hash}",
        DecayDescriptor="pi0 -> gamma gamma",
        ParticleCombiner="ParticleAdder",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


# Credit to authors of charm/d_to_etah.py
def _make_pion_pairs_from_eta_loose():
    pions = _filter_pions_from_eta_loose()
    combination_code = F.require_all(
        in_range(190 * MeV, F.MASS, 770 * MeV),
        F.MAXSDOCACUT(0.1 * mm),
    )
    composite_code = F.require_all(
        in_range(200 * MeV, F.MASS, 750 * MeV),
        F.CHI2DOF < 4,
        F.BPVLTIME(make_pvs()) > 0.25 * ps,
    )
    return ParticleCombiner([pions, pions],
                            DecayDescriptor="rho(770)0 -> pi+ pi-",
                            name="CMesonToSl_PionPairsFromEtaLoose_{hash}",
                            CombinationCut=combination_code,
                            CompositeCut=composite_code)


def _make_eta_to_pipigamma():
    combination_code = F.require_all(
        in_range(420 * MeV, F.MASS, 680 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    pion_pairs = _make_pion_pairs_from_eta_loose()
    photons = _filter_neutrals_from_eta_loose(make_photons())
    return ParticleCombiner([pion_pairs, photons],
                            DecayDescriptor="eta -> rho(770)0 gamma ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="CMesonToSl_EtaToPipPimG_{hash}")


def _make_eta_to_pipipi0():
    combination_code = F.require_all(
        in_range(410 * MeV, F.MASS, 690 * MeV), F.PT > 500 * MeV)
    vertex_code = F.require_all(in_range(450 * MeV, F.MASS, 650 * MeV))
    pion_pairs = _make_pion_pairs_from_eta_loose()
    resolved_pi0s = _filter_neutrals_from_eta_loose(_make_resolved_pi0s())
    return ParticleCombiner([pion_pairs, resolved_pi0s],
                            DecayDescriptor="eta -> rho(770)0 pi0 ",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name="CMesonToSl_EtaToPipPimPi0_{hash}")


###############################################################################
# Basic combiners for charm mesons from beauty
###############################################################################


def make_dptokpkmmum_ws():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    kaons = _filter_long_kaons()

    return ParticleCombiner(
        [muons, kaons, kaons],
        DecayDescriptor="[D+ -> mu- K+ K- ]cc",  # less mouns than pions/kaons
        name="Charm_CMesonToSl_Btag_DpToKpKmMumNu_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.1 * GeV, F.MASS, 2.168 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_dptokpkmmup_rs():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    kaons = _filter_long_kaons()

    return ParticleCombiner(
        [muons, kaons, kaons],
        DecayDescriptor="[D+ -> mu+ K+ K- ]cc",  # less mouns than pions/kaons
        name="Charm_CMesonToSl_Btag_DpToKpKmMupNu_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.1 * GeV, F.MASS, 2.168 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_dptokpkmpip_rs():
    pvs = make_pvs()
    pions = _filter_long_pions()
    kaons = _filter_long_kaons()

    return ParticleCombiner([kaons, kaons, pions],
                            DecayDescriptor="[D+ -> K+ K- pi+ ]cc",
                            name="Charm_CMesonToSl_Btag_DpToKpKmPip_{hash}",
                            Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
                            CombinationCut=F.require_all(
                                F.PT > 1.2 * GeV,
                                F.P > 15 * GeV,
                                F.SUM(F.PT) > 1.8 * GeV,
                                in_range(1.770 * GeV, F.MASS, 2.068 * GeV),
                                F.MAXSDOCACUT(0.15 * mm),
                            ),
                            CompositeCut=F.require_all(
                                F.P > 20 * GeV,
                                F.BPVVDZ(pvs) > 1.5 * mm,
                                F.CHI2DOF < 6.,
                                F.BPVFDCHI2(pvs) > 15.,
                                F.BPVDIRA(pvs) > 0.95,
                            ))


def make_dptopippimmum_ws():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, pions, pions],
        DecayDescriptor="[D+ -> mu- pi+ pi- ]cc",  # less mouns than pions/kaons
        name="Charm_CMesonToSl_Btag_DpToPipPimMumNu_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.4 * GeV, F.MASS, 2.168 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_dptopippippim_rs():
    pvs = make_pvs()
    pions = _filter_long_pions()

    return ParticleCombiner([pions, pions, pions],
                            DecayDescriptor="[D+ -> pi+ pi+ pi- ]cc",
                            name="Charm_CMesonToSl_Btag_DpToPipPipPim_{hash}",
                            Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
                            CombinationCut=F.require_all(
                                F.PT > 1.2 * GeV,
                                F.P > 15 * GeV,
                                F.SUM(F.PT) > 1.8 * GeV,
                                in_range(1.770 * GeV, F.MASS, 2.068 * GeV),
                                F.MAXSDOCACUT(0.15 * mm),
                            ),
                            CompositeCut=F.require_all(
                                F.P > 20 * GeV,
                                F.BPVVDZ(pvs) > 1.5 * mm,
                                F.CHI2DOF < 6.,
                                F.BPVFDCHI2(pvs) > 15.,
                                F.BPVDIRA(pvs) > 0.95,
                            ))


def make_dptopippimmup_rs():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, pions, pions],
        DecayDescriptor="[D+ -> mu+ pi+ pi- ]cc",  # less mouns than pions/kaons
        name="Charm_CMesonToSl_Btag_DpToPipPimMupNu_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.4 * GeV, F.MASS, 2.168 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0topimpimpipmum_ws():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, pions, pions, pions],
        DecayDescriptor=
        "[D0 -> mu- pi- pi- pi+ ]cc",  # less mouns than pions/kaons 
        name="Charm_CMesonToSl_Btag_D0ToPimPimPipMumNu_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.5 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0toetapippim_etatopippimpi0_r_rs():
    pvs = make_pvs()
    etas = _make_eta_to_pipipi0()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, pions, pions],
        DecayDescriptor="[D0 -> eta pi+ pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPipPim_EtaToPipPimPi0_R_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.765 * GeV, F.MASS, 1.965 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0toetapimmup_etatopippimg_rs():
    pvs = make_pvs()
    etas = _make_eta_to_pipigamma()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, muons, pions],
        DecayDescriptor="[D0 -> eta mu+ pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPimMupNu_EtaToPipPimG_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.8 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0topippimpimmup_rs():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, pions, pions, pions],
        DecayDescriptor="[D0 -> mu+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToPipPimPimMupNu_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MASS < 2.065 * GeV,
            in_range(0.5 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0topippippimpim_rs():
    pvs = make_pvs()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [pions, pions, pions, pions],
        DecayDescriptor="[D0 -> pi+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToPipPipPimPim_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.765 * GeV, F.MASS, 1.965 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0toetapimmum_etatopippimpi0_r_ws():
    pvs = make_pvs()
    etas = _make_eta_to_pipipi0()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, muons, pions],
        DecayDescriptor="[D0 -> eta mu- pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPimMumNu_EtaToPipPimPi0_R_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.8 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0tokpkmpimmup_rs():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    kaons = _filter_long_kaons()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, kaons, kaons, pions],
        DecayDescriptor=
        "[D0 -> mu+ K+ K- pi- ]cc",  # less mouns than pions/kaons
        name="Charm_CMesonToSl_Btag_D0ToKpKmPimMupNu_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.25 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0toetapippim_etatopippimg_rs():
    pvs = make_pvs()
    etas = _make_eta_to_pipigamma()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, pions, pions],
        DecayDescriptor="[D0 -> eta pi+ pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPipPim_EtaToPipPimG_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.765 * GeV, F.MASS, 1.965 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0tokpkmpimmum_ws():
    pvs = make_pvs()
    muons = _filter_long_muons_loose()
    kaons = _filter_long_kaons()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [muons, kaons, kaons, pions],
        DecayDescriptor="[D0 -> mu- K+ K- pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToKpKmPimMumNu_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1.25 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0tokpkmpippim_rs():
    pvs = make_pvs()
    pions = _filter_long_pions()
    kaons = _filter_long_kaons()

    return ParticleCombiner([kaons, kaons, pions, pions],
                            DecayDescriptor="[D0 ->  K+ K- pi+ pi- ]cc",
                            name="Charm_CMesonToSl_Btag_D0ToKpKmPipPim_{hash}",
                            Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
                            CombinationCut=F.require_all(
                                F.PT > 1.2 * GeV,
                                F.P > 15 * GeV,
                                F.SUM(F.PT) > 1.8 * GeV,
                                in_range(1.765 * GeV, F.MASS, 1.965 * GeV),
                                F.MAXSDOCACUT(0.15 * mm),
                            ),
                            CompositeCut=F.require_all(
                                F.P > 20 * GeV,
                                F.BPVVDZ(pvs) > 1.5 * mm,
                                F.CHI2DOF < 6.,
                                F.BPVFDCHI2(pvs) > 15.,
                                F.BPVDIRA(pvs) > 0.95,
                            ))


def make_d0toetapimmup_etatopippimpi0_r_rs():
    pvs = make_pvs()
    etas = _make_eta_to_pipipi0()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, muons, pions],
        DecayDescriptor="[D0 -> eta mu+ pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPimMupNu_EtaToPipPimPi0_R_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.8 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


def make_d0toetapimmum_etatopippimg_ws():
    pvs = make_pvs()
    etas = _make_eta_to_pipigamma()
    muons = _filter_long_muons_loose()
    pions = _filter_long_pions()

    return ParticleCombiner(
        [etas, muons, pions],
        DecayDescriptor="[D0 -> eta mu- pi- ]cc",
        name="Charm_CMesonToSl_Btag_D0ToEtaPimMumNu_EtaToPipPimG_WS_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(0.8 * GeV, F.MASS, 2.065 * GeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.BPVVDZ(pvs) > 1.5 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 15.,
            F.BPVDIRA(pvs) > 0.95,
        ))


###############################################################################
# Definition of HLT2 lines
###############################################################################
@register_line_builder(all_lines)
def b0btodppippimpim_dptopippimmum_ws_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToPipPimMumNu_WS", prescale=1):
    pvs = make_pvs()
    dps = make_dptopippimmum_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToPipPimMumNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV, in_range(0.938 * GeV, F.MASS, 5.767 * GeV),
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.P > 22 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppippimpim_dptopippippim_rs_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToPipPipPim", prescale=1):
    pvs = make_pvs()
    dps = make_dptopippippim_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToPipPipPim",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(5.080 * GeV, F.MASS, 5.480 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.P > 22 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppippimpim_dptokpkmpip_rs_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToKpKmPip", prescale=1):
    pvs = make_pvs()
    dps = make_dptokpkmpip_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToKpKmPip",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(5.080 * GeV, F.MASS, 5.480 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppimpimpim_dptokpkmmup_ws_line(
        name="Hlt2Charm_B0bToDpPimPimPim_DpToKpKmMupNu_WS", prescale=1):
    pvs = make_pvs()
    dps = make_dptokpkmmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi- pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPimPimPim_DpToKpKmMupNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.804 * GeV, F.MASS, 5.767 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppippimpim_dptokpkmmup_rs_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMupNu", prescale=1):
    pvs = make_pvs()
    dps = make_dptokpkmmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToKpKmMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.804 * GeV, F.MASS, 5.767 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppippimpim_dptokpkmmum_ws_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToKpKmMumNu_WS", prescale=1):
    pvs = make_pvs()
    dps = make_dptokpkmmum_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToKpKmMumNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.804 * GeV, F.MASS, 5.767 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppimpimpim_dptopippimmup_ws_line(
        name="Hlt2Charm_B0bToDpPimPimPim_DpToPipPimMupNu_WS", prescale=1):
    pvs = make_pvs()
    dps = make_dptopippimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi- pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPimPimPim_DpToPipPimMupNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(0.938 * GeV, F.MASS, 5.767 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def b0btodppippimpim_dptopippimmup_rs_line(
        name="Hlt2Charm_B0bToDpPipPimPim_DpToPipPimMupNu", prescale=1):
    pvs = make_pvs()
    dps = make_dptopippimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [dps, pions, pions, pions],
        DecayDescriptor="[B~0 -> D+ pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_B0bToDpPipPimPim_DpToPipPimMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(0.938 * GeV, F.MASS, 5.767 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dps, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapippim_etatopippimpi0_r_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimPi0_R",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapippim_etatopippimpi0_r_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimPi0_R",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(4.879 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapimmup_etatopippimpi0_r_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmup_etatopippimpi0_r_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pimpimpim_d0topippimpimmup_ws_line(
        name="Hlt2Charm_BmToD0PimPimPim_D0ToPipPimPimMupNu_WS", prescale=1):
    pvs = make_pvs()
    d0s = make_d0topippimpimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi- pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PimPimPim_D0ToPipPimPimMupNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(1.345 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0tokpkmpimmup_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToKpKmPimMupNu", prescale=1):
    pvs = make_pvs()
    d0s = make_d0tokpkmpimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToKpKmPimMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(3.306 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pimpimpim_d0toetapimmup_etatopippimg_ws_line(
        name="Hlt2Charm_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimG_WS",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmup_etatopippimg_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi- pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimG_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pimpimpim_d0toetapimmup_etatopippimpi0_r_ws_line(
        name="Hlt2Charm_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R_WS",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmup_etatopippimpi0_r_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi- pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PimPimPim_D0ToEtaPimMupNu_EtaToPipPimPi0_R_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapimmum_etatopippimg_ws_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimG_WS",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmum_etatopippimg_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimG_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0tokmkmpipmum_ws_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToKmKmPipMumNu_WS", prescale=1):
    pvs = make_pvs()
    d0s = make_d0tokpkmpimmum_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToKmKmPipMumNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(3.306 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0topippimpimmup_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToPipPimPimMupNu", prescale=1):
    pvs = make_pvs()
    d0s = make_d0topippimpimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToPipPimPimMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(1.345 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapimmup_etatopippimg_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimG",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmup_etatopippimg_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPimMupNu_EtaToPipPimG",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pimpimpim_d0tokpkmpimmup_ws_line(
        name="Hlt2Charm_BmToD0PimPimPim_D0ToKpKmPimMupNu_WS", prescale=1):
    pvs = make_pvs()
    d0s = make_d0tokpkmpimmup_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi- pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PimPimPim_D0ToKpKmPimMupNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(3.306 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0topippippimpim_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToPipPipPimPim", prescale=1):
    pvs = make_pvs()
    d0s = make_d0topippippimpim_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToPipPipPimPim",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(4.879 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0topimpimpipmum_ws_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToPimPimPipMumNu_WS", prescale=1):
    pvs = make_pvs()
    d0s = make_d0topimpimpipmum_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToPimPimPipMumNu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(1.345 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0tokpkmpippim_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToKpKmPipPim", prescale=1):
    pvs = make_pvs()
    d0s = make_d0tokpkmpippim_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToKpKmPipPim",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(4.879 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapippim_etatopippimg_rs_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimG",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapippim_etatopippimg_rs()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name="Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPipPim_EtaToPipPimG",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(4.879 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)


@register_line_builder(all_lines)
def bmtod0pippimpim_d0toetapimmum_etatopippimpi0_r_ws_line(
        name="Hlt2Charm_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimPi0_R_WS",
        prescale=1):
    pvs = make_pvs()
    d0s = make_d0toetapimmum_etatopippimpi0_r_ws()
    pions = _filter_long_pions()

    line_alg = ParticleCombiner(
        [d0s, pions, pions, pions],
        DecayDescriptor="[B- -> D0 pi+ pi- pi- ]cc",
        name=
        "Charm_CMesonToSl_Btag_BmToD0PipPimPim_D0ToEtaPimMumNu_EtaToPipPimPi0_R_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.SUM(F.PT) > 1.5 * GeV, F.P > 20 * GeV,
            in_range(2.082 * GeV, F.MASS, 5.679 * GeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm, F.MAXSDOCACUT(2 * mm)),
        CompositeCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.BPVVDZ(pvs) > 0.7 * mm,
            F.CHI2DOF < 6.,
            F.BPVFDCHI2(pvs) > 50.,
            F.BPVDIRA(pvs) > 0.95,
        ))
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [d0s, line_alg],
        prescale=prescale)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D0 -> h- h+ pi0 HLT2 lines.

All lines are promptly tagged with D*(2010)+ -> D0 pi+ decays.
The pi0's can be resolved or merged depending on how they are reconstructed in
the ECAL.

  1. D0 -> phi(1020)(-> K- K+)  pi0 (Hlt2Charm_DstpToD0Pip_D0ToKmKpPi0_[R,M])
  2. D0 -> rho(770)(-> pi- pi+) pi0 (Hlt2Charm_DstpToD0Pip_D0ToPimPipPi0_[R,M])
  3. D0 -> K*(892)(-> K- pi+)   pi0 (Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_[R,M])
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, micrometer as um
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter,
                                      ParticleContainersMerger)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from .prefilters import charm_prefilters
from .particle_properties import _D0_M, _PI0_M

from ...standard_particles import (make_has_rich_long_pions,
                                   make_has_rich_long_kaons, make_merged_pi0s)

from RecoConf.reconstruction_objects import (
    make_pvs, make_neutral_protoparticles as _make_neutral_protoparticles)
from PyConf.Algorithms import (LHCb__Phys__ParticleMakers__PhotonMaker as
                               PhotonMaker)
from .taggers import _make_dstar_from_descriptor
from . import charm_isolation as isolation

###############################################################################
# Basic particles builders.
#   - kaon and pion cuts from Stripping29r2p1
#   - photon cuts from photons tool in standard particles (PT(gamma) > 2 GeV)
###############################################################################


def _make_charm_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.PID_K < 0,
                          F.MINIPCHI2CUT(IPChi2Cut=3., Vertices=make_pvs()))))


def _make_charm_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 500 * MeV, F.PID_K > 7,
                          F.MINIPCHI2CUT(IPChi2Cut=3., Vertices=make_pvs()))))


###############################################################################
# Particles combiners
###############################################################################


def _make_hh_pair(particle1,
                  particle2,
                  descriptor,
                  name,
                  comb_pt_min=1700 * MeV,
                  bpvipchi2_min=36.,
                  comb_m_max=1850 * MeV,
                  docachi2_max=15.,
                  doca_max=200 * um,
                  vchi2dof_max=3.,
                  bpvfdchi2_min=100.):
    """Make the intermediate 2-body resonances `res -> h h` (h=K, pi)."""

    pvs = make_pvs()

    # Require that at least one of the hadrons has high PT and high IPChi2
    child1 = F.require_all(
        F.CHILD(1, F.PT) > comb_pt_min,
        F.CHILD(1, F.BPVIPCHI2(pvs)) > bpvipchi2_min)
    child2 = F.require_all(
        F.CHILD(2, F.PT) > comb_pt_min,
        F.CHILD(2, F.BPVIPCHI2(pvs)) > bpvipchi2_min)
    combination_cut_list = [child1 | child2]

    combination_code = F.require_all(*combination_cut_list,
                                     F.MASS < comb_m_max,
                                     F.MAXDOCACHI2CUT(docachi2_max),
                                     F.DOCA(1, 2) < doca_max)

    vertex_code = F.require_all(F.CHI2DOF < vchi2dof_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


###############################################################################
# Make Resolved Pi0's since the one in standard particles uses LoKi
###############################################################################


def _make_photons(make_neutral_protoparticles=_make_neutral_protoparticles,
                  pvs=make_pvs):
    """Create photon LHCb::Particles from LHCb::ProtoParticles."""
    return PhotonMaker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=pvs(),
        ConfLevelCut=0.5,
        PtCut=150 * MeV).Particles


def _make_resolved_pi0s(photon=_make_photons):
    g = photon()
    combination_code = in_range(_PI0_M - 15 * MeV, F.MASS, _PI0_M + 15 * MeV)
    composite_code = F.PT > 500 * MeV
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[g, g],
        name="Charm_D0Tohhpi0_Pi0_R",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code)


def _make_dzeros(particle1,
                 particle2,
                 descriptor,
                 name,
                 comb_m_min=_D0_M - 160 * MeV,
                 comb_m_max=_D0_M + 160 * MeV,
                 m_min=_D0_M - 150 * MeV,
                 m_max=_D0_M + 150 * MeV,
                 sum_pt_min=1400 * MeV,
                 vchi2dof_max=20.):
    """Combine the 2-body resonance and the pi0 to make the D0."""

    assert particle1 is not None, 'particles must be specified'
    assert particle2 is not None, 'particles must be specified'

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > sum_pt_min)
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2dof_max)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


def _make_dstars(dzeros, self_conjugate_d0_decay):
    """Make a D*(2010)+ -> D0 pi+ combination. Both self-conjugate and non
    self-conjugate D0 decays are supported.

    Args:
        dzeros (DataHandle): Input D0 and anti-D0 candidates.
        self_conjugate_d0_decay (bool): True if the D0 final state is
                                        self-conjugate and anti-D0 are not
                                        reconstructed.

    Returns:
        Output of particle filtering algorithm as particle container
        DataHandle.
    """
    if self_conjugate_d0_decay:
        dstarp = _make_dstar_from_descriptor(
            dzeros, "D*(2010)+ -> D0 pi+",
            "Charm_D0Tohhpi0_DstpToD0Pip_{hash}")
        dstarm = _make_dstar_from_descriptor(
            dzeros, "D*(2010)- -> D0 pi-",
            "Charm_D0Tohhpi0_DstmToD0Pim_{hash}")
        return ParticleContainersMerger([dstarp, dstarm])
    else:
        return _make_dstar_from_descriptor(
            dzeros, "[D*(2010)+ -> D0 pi+]cc",
            "Charm_D0Tohhpi0_DstpToD0Pip_{hash}")


###############################################################################
# Lines definition
###############################################################################

all_lines = {}


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipipi0R_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipPi0_R', prescale=1):
    pions = _make_charm_pions()
    rho = _make_hh_pair(pions, pions, 'rho(770)0 -> pi- pi+',
                        "Charm_D0Tohhpi0_Rho0ToPimPip")
    pi0R = _make_resolved_pi0s()
    dzeros = _make_dzeros(rho, pi0R, 'D0 -> rho(770)0 pi0',
                          "Charm_D0Tohhpi0_D0ToRho0Pi0_R")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [rho, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2pipipi0M_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToPimPipPi0_M', prescale=1):
    pions = _make_charm_pions()
    rho = _make_hh_pair(pions, pions, 'rho(770)0 -> pi- pi+',
                        "Charm_D0Tohhpi0_Rho0ToPimPip")
    pi0M = make_merged_pi0s(PtCut=500 * MeV)
    dzeros = _make_dzeros(rho, pi0M, 'D0 -> rho(770)0 pi0',
                          "Charm_D0Tohhpi0_D0ToRho0Pi0_M")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [rho, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2KKpi0R_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKpPi0_R', prescale=1):
    kaons = _make_charm_kaons()
    phi = _make_hh_pair(kaons, kaons, 'phi(1020) -> K- K+',
                        "Charm_D0Tohhpi0_PhiToKmKp")
    pi0R = _make_resolved_pi0s()
    dzeros = _make_dzeros(phi, pi0R, 'D0 -> phi(1020) pi0',
                          "Charm_D0Tohhpi0_D0ToPhiPi0_R")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [phi, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2KKpi0M_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmKpPi0_M', prescale=1):
    kaons = _make_charm_kaons()
    phi = _make_hh_pair(kaons, kaons, 'phi(1020) -> K- K+',
                        "Charm_D0Tohhpi0_PhiToKmKp")
    pi0M = make_merged_pi0s(PtCut=500 * MeV)
    dzeros = _make_dzeros(phi, pi0M, 'D0 -> phi(1020) pi0',
                          "Charm_D0Tohhpi0_D0ToPhiPi0_M")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=True)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [phi, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2Kpipi0R_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_R', prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    kstar = _make_hh_pair(kaons, pions, '[K*(892)~0 -> K- pi+]cc',
                          "Charm_D0Tohhpi0_Kst0bToKmPip")
    pi0R = _make_resolved_pi0s()
    dzeros = _make_dzeros(kstar, pi0R, '[D0 -> K*(892)~0 pi0]cc',
                          "Charm_D0Tohhpi0_D0ToKst0bPi0_R")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kstar, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )


@register_line_builder(all_lines)
def dstarp2dzeropip_dzero2Kpipi0M_line(
        name='Hlt2Charm_DstpToD0Pip_D0ToKmPipPi0_M', prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    kstar = _make_hh_pair(kaons, pions, '[K*(892)~0 -> K- pi+]cc',
                          "Charm_D0Tohhpi0_Kst0bToKmPip")
    pi0M = make_merged_pi0s(PtCut=500 * MeV)
    dzeros = _make_dzeros(kstar, pi0M, '[D0 -> K*(892)~0 pi0]cc',
                          "Charm_D0Tohhpi0_D0ToKst0bPi0_M")
    dstars = _make_dstars(dzeros, self_conjugate_d0_decay=False)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [kstar, dzeros, dstars],
        prescale=prescale,
        extra_outputs=isolation.make_iso_particles(
            dstars, coneangle=0.5, PizIso=True),
    )

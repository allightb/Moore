###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from . import cutbased_dilepton_trigger, inclusive_detached_dilepton_trigger

all_lines = {}

all_lines.update(cutbased_dilepton_trigger.all_lines)
all_lines.update(inclusive_detached_dilepton_trigger.all_lines)

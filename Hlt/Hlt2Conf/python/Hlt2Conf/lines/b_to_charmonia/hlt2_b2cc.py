###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B -> J/psi X HLT2 lines.
"""
from Moore.config import Hlt2Line, register_line_builder

from Hlt2Conf.lines.b_to_charmonia.prefilters import b2cc_prefilters
from Hlt2Conf.lines.b_to_charmonia import b_to_jpsix, b_to_jpsix0

PROCESS = 'hlt2'
all_lines = {}


@register_line_builder(all_lines)
def BsToJpsiPhi_detachedline(name='Hlt2B2CC_BsToJpsiPhi_Detached',
                             prescale=1,
                             persistreco=True):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_detached_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'],
        pv_tracks=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsiPhi_extramuonline(name='Hlt2B2CC_BsToJpsiPhi_ExtraMuon',
                              prescale=1,
                              persistreco=True):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_extramuonline(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        raw_banks=['VP', 'UT', 'FT', 'Rich', 'Muon', 'Calo'],
        pv_tracks=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
def BsToJpsiPhi_line(name='Hlt2B2CC_BsToJpsiPhi',
                     prescale=0.1,
                     persistreco=False):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsieePhi_detached_line(name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE_Detached',
                                prescale=1,
                                persistreco=False):
    line_alg = b_to_jpsix.make_BsToJpsieePhi_detached_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsieePhi_line(name='Hlt2B2CC_BsToJpsiPhi_JpsiToEE',
                       prescale=0.1,
                       persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsieePhi_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKstar_detached_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE_Detached',
        prescale=1,
        persistreco=False):

    line_alg = b_to_jpsix.make_BdToJpsieeKstar_detached_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKstar_line(name='Hlt2B2CC_BdToJpsiKstar_JpsiToEE',
                         prescale=0.05,
                         persistreco=False):

    line_alg = b_to_jpsix.make_BdToJpsieeKstar_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsif0_line(name='Hlt2B2CC_BsToJpsif0', prescale=1, persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        tagging_particles=True,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsif0kaon_line(name='Hlt2B2CC_BsToJpsif0Kaon',
                        prescale=1,
                        persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0kaon_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        tagging_particles=True,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsif0ws_line(name='Hlt2B2CC_BsToJpsif0ws',
                      prescale=1,
                      persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0ws_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        tagging_particles=True,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsif0Prescaled_line(name='Hlt2B2CC_BsToJpsif0Prescaled',
                             prescale=0.03,
                             persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsif0Prescaled_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BsToJpsikstar_line(name='Hlt2B2CC_BsToJpsiKstar',
                       prescale=1,
                       persistreco=False):

    line_alg = b_to_jpsix.make_BsToJpsiKstar_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        tagging_particles=True,
        pv_tracks=True)


@register_line_builder(all_lines)
def LbToJpsipH_line(name='Hlt2B2CC_LbToJpsipH', prescale=1, persistreco=False):

    line_alg = b_to_jpsix.make_LbToJpsipH_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKshort_LL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_LL_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_LL_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKshort_DD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_DD_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_DD_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKshort_LL_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_LL_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_LL_tight_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsieeKshort_DD_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToEE_DD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsieeKshort_DD_tight_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BuToJpsieeKplus_detached_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToEE_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BuToJpsieeKplus_detached_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LL_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LL_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LL_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_DD_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_DD_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_DD_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LD_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LD_Prompt',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LD_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_UL_prompt_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_UL_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_UL_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LL_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LL_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_DD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_DD_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_DD_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_LD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_LD_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_LD_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshort_UL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshort_JpsiToMuMu_UL_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshort_UL_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_LL_prompt_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_LL_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_LL_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_DD_prompt_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_DD_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_DD_tight_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_LL_detached_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_LL_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_LL_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdTopsitwosmumuKshort_DD_detached_line(
        name='Hlt2B2CC_BdToPsitwosKshort_PsitwosToMuMu_DD_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdTopsitwosmumuKshort_DD_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BuToJpsimumuKplus_prompt_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BuToJpsimumuKplus_tight_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BuToJpsimumuKplus_detached_line(
        name='Hlt2B2CC_BuToJpsiKplus_JpsiToMuMu_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BuToJpsimumuKplus_detached_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKstar_detached_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToMuMu_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKstar_detached_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKstar_prompt_line(
        name='Hlt2B2CC_BdToJpsiKstar_JpsiToMuMu_Prompt',
        prescale=0.1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKstar_tight_line(process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BuToJpsiKstPlus(name='Hlt2B2CC_BuToJpsiKstPlus',
                    prescale=1.0,
                    persistreco=False):
    line_alg = b_to_jpsix0.make_B2JpsiKst2KPi0R_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BuToChicKPlus(name='Hlt2B2CC_BuToChicKPlus',
                  prescale=1.0,
                  persistreco=False):
    line_alg = b_to_jpsix0.make_B2Chic2JpsiGK_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BToJpsiEtaGG(name='Hlt2B2CC_BToJpsiEtaGG', prescale=1.0,
                 persistreco=False):
    line_alg = b_to_jpsix0.make_B2JpsiEtaGG_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BToJpsiPi0GG(name='Hlt2B2CC_BToJpsiPi0GG', prescale=1.0,
                 persistreco=False):
    line_alg = b_to_jpsix0.make_B2JpsiPi0GG_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BcToJpsiPip(name='Hlt2B2CC_BcToJpsiPip', prescale=1.0, persistreco=False):
    line_alg = b_to_jpsix.make_BcToJpsimumuPiplus_line(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshortKshort_LL_detached_line(
        name='Hlt2B2CC_BdToJpsiKshortKshort_JpsiToMuMu_LL_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshortKshort_LL_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsimumuKshortKshort_DD_detached_line(
        name='Hlt2B2CC_BdToJpsiKshortKshort_JpsiToMuMu_DD_Detached',
        prescale=1,
        persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsimumuKshortKshort_DD_detached_line(
        process=PROCESS)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def BdToJpsippKstar(name='Hlt2B2CC_BdToJpsiKstar_JpsiToPP',
                    prescale=1.0,
                    persistreco=False):
    line_alg = b_to_jpsix.make_BdToJpsippKstar(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)


@register_line_builder(all_lines)
def Bs0ToJpsippPhi(name='Hlt2B2CC_Bs0ToJpsiPhi_JpsiToPP',
                   prescale=1.0,
                   persistreco=False):
    line_alg = b_to_jpsix.make_Bs0ToJpsippPhi(process=PROCESS)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        persistreco=persistreco,
        pv_tracks=True)

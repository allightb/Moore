###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of special descriptor for non-default Bs meson decays 
that can be shared in the B2CC selections, and therefore are defined centrally.
"""
from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond
import Functors as F


@configurable
def make_X2BsLep(particles,
                 descriptor,
                 name='B2CC_X2Bsmu_Combiner_{hash}',
                 pvs=make_pvs):
    """
    A X->BLep decay maker, where X
    is a dummy particle
    """
    combination_code = F.ALL
    vertex_code = F.ALL
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_JpsiPP_X(particles,
                  descriptor,
                  name='B2CC_JpsiPP_X_Combiner_{hash}',
                  am_min_mass=(5279.66 - 300.) * MeV,
                  am_max_mass=(5279.66 + 300.) * MeV,
                  vtx_min_mass=(5279.66 - 290.) * MeV,
                  vtx_max_mass=(5279.66 + 290.) * MeV,
                  pp_max_mass=4700. * MeV,
                  pp_min_sumpt=900.0 * MeV,
                  pp_min_sump=8400.0 * MeV,
                  pp_max_p=4.8 * GeV,
                  pp_max_pt=480.0 * MeV,
                  vtx_min_sumpt=3600.0 * MeV,
                  vtx_min_pt=3000.0 * MeV,
                  vtx_max_chi2=9.,
                  vtx_min_dira=0.9999,
                  doca_max=0.15 * mm,
                  ipchi2_min=0.2 * mm,
                  b_min_lft=0.2 * picosecond,
                  b_min_fdchi2=100):

    combination12_code = F.require_all(  # Jpsi->ppbar combination
        F.DOCA(1, 2) < doca_max, F.MASS < pp_max_mass,
        F.SUM(F.PT) > pp_min_sumpt,
        F.SUM(F.P) > pp_min_sump,
        F.MAX(F.P) > pp_max_p,
        F.MAX(F.PT) > pp_max_pt)

    combination123_code = F.require_all(  # ppbarK+ combination
        F.MASS < am_max_mass,
        F.DOCA(1, 3) < doca_max,
        F.DOCA(2, 3) < doca_max,
    )

    combination_code = F.require_all(  ## ppbarK+pi-(K-) combination
        F.math.in_range(am_min_mass, F.MASS, am_max_mass),
        F.DOCA(1, 4) < doca_max,
        F.DOCA(2, 4) < doca_max,
        F.DOCA(3, 4) < doca_max,
    )

    pvs = make_pvs()

    vertex_code = F.require_all(  ## fit to the B0(s)->ppbarhh norm channerls
        F.math.in_range(vtx_min_mass, F.MASS, vtx_max_mass),
        F.CHI2DOF < vtx_max_chi2,
        F.MINIPCHI2(pvs) < ipchi2_min,
        F.BPVDIRA(pvs) > vtx_min_dira, F.PT > vtx_min_pt,
        F.SUM(F.PT) > vtx_min_sumpt,
        F.BPVFDCHI2(pvs) > b_min_fdchi2,
        F.BPVLTIME(pvs) > b_min_lft)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import (GeV, MeV, mm, micrometer as um)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_pions, make_has_rich_long_kaons,
    make_long_protons, make_photons)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Moore.lines import Hlt2Line
import Functors as F
from PyConf.Algorithms import Monitor__ParticleRange
from Functors.math import log


def _MIP_MIN(cut, pvs=make_pvs):
    return F.MINIPCUT(IPCut=cut, Vertices=pvs())


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _filter_long_pions_for_strange():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 80 * MeV, _MIP_MIN(1.2 * mm),
                          _MIPCHI2_MIN(12))))


# take the charm pi/K filter from d0_to_hh
def _filter_long_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K < 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


def _filter_long_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.,
                _MIPCHI2_MIN(4),
            ), ),
    )


def _filter_long_protons_for_strange():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.PID_P > -5., _MIP_MIN(80. * um),
                          _MIPCHI2_MIN(9))))


def _filter_long_protons_for_charm():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(F.PT > 600 * MeV, F.PID_P > 5., _MIP_MIN(20. * um),
                          _MIPCHI2_MIN(4))))


def _filter_photons():
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(F.PT > 300 * MeV,
                          F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.9,
                          F.IS_PHOTON > 0.7, F.IS_NOT_H > 0.5)))


def _make_ll_kshorts():
    return ParticleCombiner(
        [_filter_long_pions_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name='Comissioning_KS0_LL_{hash}',
        CombinationCut=F.require_all(
            F.math.in_range(420 * MeV, F.MASS, 570 * MeV),
            F.PT > 300 * MeV,
            F.P > 3.5 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(450 * MeV, F.MASS, 550 * MeV),
            F.PT > 350 * MeV,
            F.P > 4 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.,
            F.BPVDIRA(make_pvs()) < 0.9995,
        ),
    )


def _make_ll_lambdas():
    return ParticleCombiner(
        [_filter_long_protons_for_strange(),
         _filter_long_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Commissioning_Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.PT > 450 * MeV,
            F.MAXDOCACUT(300 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1080 * MeV, F.MASS, 1140 * MeV),
            F.PT > 500 * MeV,
            F.CHI2DOF < 16.,
            F.BPVVDZ(make_pvs()) > 12 * mm,
            F.math.in_range(-100 * mm, F.END_VZ, 700 * mm),
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 100,
        ),
    )


def _make_dp_kpipi():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="Commissioning_DpToKmPipPip",
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 1990 * MeV),
            F.PT > 1.2 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 1.2 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 1970 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 12 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(make_pvs()) > 1.5 * mm,
            F.BPVDIRA(make_pvs()) > 0.999,
            F.BPVFDCHI2(make_pvs()) > 32.,
        ),
    )


def _diphoton_line(name='Hlt2Commissioning_DiPhoton'):
    diphoton = ParticleCombiner(
        name="Commissioning_DiPhoton_Combiner",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(0 * MeV, F.MASS, 1200 * MeV), F.PT > 1.6 * GeV),
        CompositeCut=F.ALL,
    )
    return Hlt2Line(
        name=name,
        algs=[diphoton],
        postscale=0.,
        persistreco=True,
    )


def _kshort_ll_tos_line(name="Hlt2Commissioning_KsToPimPip_LL_Hlt1KsTOS"):
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*Ks.*Decision",
        algs=[require_pvs(make_pvs()),
              _make_ll_kshorts()],
        postscale=0.,
        persistreco=True,
    )


def _kshort_ll_line(name="Hlt2Commissioning_KsToPimPip_LL"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_ll_kshorts()],
        postscale=0.,
        persistreco=True,
    )


def _phi_to_kk_detached_line(name="Hlt2Commissioning_PhiToKmKp"):
    """
      Detached inclusive phi -> KK
    """
    phis = ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_kaons_for_charm()],
        DecayDescriptor="phi(1020) -> K- K+",
        name="Commissioning_Phi",
        CombinationCut=F.require_all(
            F.MASS < 1080 * MeV,
            F.MAXSDOCACUT(100 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(985 * MeV, F.MASS, 1065 * MeV),
            F.PT > 600 * MeV,
            F.CHI2DOF < 10.,
            F.BPVVDZ(make_pvs()) > 0.8 * mm,
            F.BPVVDRHO(make_pvs()) < 2 * mm,
        ),
    )
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=[require_pvs(make_pvs()), phis],
        postscale=0.,
        persistreco=True,
    )


def _lambda_ll_tos_line(name="Hlt2Commissioning_L0ToPpPim_LL_Hlt1L02PPiTOS"):
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1L02PPiDecision",
        algs=[require_pvs(make_pvs()),
              _make_ll_lambdas()],
        postscale=0.,
        persistreco=True,
    )


def _lambda_ll_line(name="Hlt2Commissioning_L0ToPpPim_LL"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_ll_lambdas()],
        postscale=0.,
        persistreco=True,
    )


# the non-TOS version lives in charm/d0_to_hh
def _d0_to_kpi_line(name="Hlt2Commissioning_D0ToKmPip_Hlt1D2KPiTOS"):
    d0 = ParticleCombiner(
        [_filter_long_kaons_for_charm(),
         _filter_long_pions_for_charm()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name="Commissioning_D0ToKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.,
            F.BPVFDCHI2(make_pvs()) > 25.,
            F.BPVDIRA(make_pvs()) > 0.99985,
        ),
    )

    d_ipchi2_mon = Monitor__ParticleRange(
        Input=d0,
        Variable=log(F.MINIPCHI2(make_pvs())),
        HistogramName=f"/{name}/D0_logIPCHI2",
        Bins=60,
        Range=(-6 * MeV, 12 * MeV),
    )
    d_pasy_mon = Monitor__ParticleRange(
        Input=d0,
        Variable=(F.CHILD(1, F.P) - F.CHILD(2, F.P)) /
        (F.CHILD(1, F.P) + F.CHILD(2, F.P)),
        HistogramName=f"/{name}/D0_Daughters_Pasy",
        Bins=60,
        Range=(-1, 1),
    )
    d_ptasy_mon = Monitor__ParticleRange(
        Input=d0,
        Variable=(F.CHILD(1, F.PT) - F.CHILD(2, F.PT)) /
        (F.CHILD(1, F.PT) + F.CHILD(2, F.PT)),
        HistogramName=f"/{name}/D0_Daughters_PTasy",
        Bins=60,
        Range=(-1, 1),
    )

    pi_pidk_mon = Monitor__ParticleRange(
        Input=d0,
        Variable=F.CHILD(2, F.PID_K),
        #Variable=F.Mass,
        HistogramName=f"/{name}/pi_PIDk",
        Bins=60,
        Range=(-150, 5),
    )

    K_pidk_mon = Monitor__ParticleRange(
        Input=d0,
        Variable=F.CHILD(1, F.PID_K),
        #Variable=F.Mass,
        HistogramName=f"/{name}/K_PIDk",
        Bins=60,
        Range=(5, 150),
    )
    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1D2KPiDecision",
        algs=[
            require_pvs(make_pvs()), d0, d_ipchi2_mon, d_pasy_mon, d_ptasy_mon,
            pi_pidk_mon, K_pidk_mon
        ],
        postscale=0.,
        persistreco=True,
    )


def _dp_to_kpipi_line(name="Hlt2Commissioning_DpToKmPipPip"):
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()),
              _make_dp_kpipi()],
        postscale=0.,
        persistreco=True,
    )


def _dp_to_kpipi_tos_line(
        name="Hlt2Commissioning_DpToKmPipPip_Hlt1TrackMVATOS"):

    d_m_plus_mon = Monitor__ParticleRange(
        Input=_make_dp_kpipi(),
        Variable=F.MASS * F.CHARGE,
        #Variable=F.Mass,
        HistogramName=f"/{name}/D0_M_plus",
        Bins=60,
        Range=(1779 * MeV, 1959 * MeV),
    )

    d_m_minus_mon = Monitor__ParticleRange(
        Input=_make_dp_kpipi(),
        Variable=F.MASS * F.CHARGE * (-1),
        #Variable=F.Mass,
        HistogramName=f"/{name}/D0_M_minus",
        Bins=60,
        Range=(1779 * MeV, 1959 * MeV),
    )

    return Hlt2Line(
        name=name,
        hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=[
            require_pvs(make_pvs()),
            _make_dp_kpipi(), d_m_minus_mon, d_m_plus_mon
        ],
        postscale=0.,
        persistreco=True,
    )


def _lc_to_pkpi_line(name="Hlt2Commissioning_LcpToPpKmPip"):
    pvs = make_pvs()
    lcs = ParticleCombiner(
        [
            _filter_long_protons_for_charm(),
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm()
        ],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="Commissioning_LcpToPpKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(2200 * MeV, F.MASS, 2560 * MeV),
            F.PT > 1.7 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2230 * MeV, F.MASS, 2550 * MeV),
            F.PT > 2 * GeV,
            F.P > 16 * GeV,
            F.CHI2DOF < 12.,
            F.BPVVDZ(pvs) > 0.5 * mm,
            F.BPVDIRA(pvs) > 0.999,
            F.BPVFDCHI2(pvs) > 12.,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs()), lcs],
        postscale=0.,
        persistreco=True,
    )

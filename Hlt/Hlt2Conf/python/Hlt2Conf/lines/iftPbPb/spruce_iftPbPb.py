###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import SpruceLine, register_line_builder

from RecoConf.reconstruction_objects import upfront_reconstruction
from PyConf import configurable
from RecoConf.event_filters import require_gec

sprucing_lines = {}
PROCESS = "spruce"

hlt2_ion_triggers = [
    'Hlt2IFT_PassThroughDecision', 'Hlt2IFT_PbPbMicroBiasDecision'
]


@register_line_builder(sprucing_lines)
@configurable
def passthrough_PbPb(name='SpruceIFT_PbPbPassThrough',
                     prescale=1,
                     skipUT=True,
                     persistreco=True,
                     cutGEC=30000,
                     hlt2_filter=hlt2_ion_triggers):
    algs = upfront_reconstruction() + [require_gec(cut=cutGEC, skipUT=skipUT)]
    return SpruceLine(
        name=name,
        algs=algs,
        hlt2_filter_code=hlt2_filter,
        prescale=prescale,
        persistreco=persistreco)

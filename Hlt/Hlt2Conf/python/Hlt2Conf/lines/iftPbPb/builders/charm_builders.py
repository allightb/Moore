###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import (
    ParticleFilter,
    ParticleCombiner,
)
from Hlt2Conf.standard_particles import (make_has_rich_long_pions,
                                         make_has_rich_long_kaons,
                                         make_has_rich_long_protons)

all_lines = {}

################################################################################
# Prompt lines
################################################################################


@configurable
def make_charm_pions(bpvipchi2=9.0,
                     maxPIDKaon=None,
                     min_pt=250 * MeV,
                     min_p=3000.0 * MeV,
                     max_trchi2dof=3):
    code = F.require_all(F.PT > min_pt, F.P > min_p, F.CHI2DOF < max_trchi2dof,
                         F.BPVIPCHI2(make_pvs()) > bpvipchi2)
    if maxPIDKaon is not None:
        code &= (F.PID_K < maxPIDKaon)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(code))


@configurable
def make_charm_kaons(bpvipchi2=9.0,
                     minPIDKaon=None,
                     min_pt=250 * MeV,
                     min_p=3000.0 * MeV,
                     max_trchi2dof=3):
    code = F.require_all(F.PT > min_pt, F.P > min_p, F.CHI2DOF < max_trchi2dof,
                         F.BPVIPCHI2(make_pvs()) > bpvipchi2)
    if minPIDKaon is not None:
        code &= (F.PID_K > minPIDKaon)
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


@configurable
def make_charm_protons_loose(bpvipchi2=9.0,
                             minPIDProton=None,
                             min_pt=250 * MeV,
                             min_p=3000.0 * MeV,
                             max_trchi2dof=3):
    code = F.require_all(F.PT > min_pt, F.P > min_p, F.CHI2DOF < max_trchi2dof,
                         F.BPVIPCHI2(make_pvs()) > bpvipchi2)
    if minPIDProton is not None:
        code &= (F.PID_P > minPIDProton)
    return ParticleFilter(make_has_rich_long_protons(), F.FILTER(code))


@configurable
def make_charm_protons_tight(bpvipchi2=9.0,
                             minPIDProton=None,
                             minPIDProtonKaon=None,
                             pt_min=250 * MeV,
                             p_min=3000.0 * MeV,
                             max_trchi2dof=3):
    code = F.require_all(F.PT > pt_min, F.P > p_min, F.CHI2DOF < max_trchi2dof,
                         F.BPVIPCHI2(make_pvs()) > bpvipchi2)
    if (minPIDProton and minPIDProtonKaon) is not None:
        code &= (F.PID_P > minPIDProton)
        code &= ((F.PID_P - F.PID_K) < minPIDProtonKaon)
    return ParticleFilter(make_has_rich_long_protons(), F.FILTER(code))


@configurable
def define_child(particle, Criteria):

    if particle == "kaon":
        dau = make_charm_kaons(
            bpvipchi2=Criteria["bpvipchi2"],
            minPIDKaon=Criteria["minPIDK"],
            min_pt=Criteria["minpT"],
            min_p=Criteria["minp"],
            max_trchi2dof=Criteria["maxtrchi2dof"])
    elif particle == "pion":
        dau = make_charm_pions(
            bpvipchi2=Criteria["bpvipchi2"],
            maxPIDKaon=Criteria["maxPIDK"],
            min_pt=Criteria["minpT"],
            min_p=Criteria["minp"],
            max_trchi2dof=Criteria["maxtrchi2dof"])
    elif particle == "proton":
        if (Criteria["minPIDpK"] is None):
            dau = make_charm_protons_loose(
                bpvipchi2=Criteria["bpvipchi2"],
                minPIDProton=Criteria["minPIDp"],
                min_pt=Criteria["minpT"],
                min_p=Criteria["minp"],
                max_trchi2dof=Criteria["maxtrchi2dof"])
        else:
            dau = make_charm_protons_tight(
                bpvipchi2=Criteria["bpvipchi2"],
                minPIDProton=Criteria["minPIDp"],
                minPIDProtonKaon=Criteria["minPIDpK"],
                min_pt=Criteria["minpT"],
                min_p=Criteria["minp"],
                max_trchi2dof=Criteria["maxtrchi2dof"])
    else:
        print(particle + "is not correctly defined")

    return dau


@configurable
def make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,  #PIDp - PIDK > minPIDpK
        minpT=0,
        minp=1 * GeV,
        maxtrchi2dof=5):

    criteria = {
        "bpvipchi2": bpvipchi2,
        "minPIDK": minPIDK,
        "maxPIDK": maxPIDK,
        "minPIDp": minPIDp,
        "minPIDpK": minPIDpK,
        "minpT": minpT,
        "minp": minp,
        "maxtrchi2dof": maxtrchi2dof
    }

    return criteria


@configurable
def make_comb_criteria(mmin=None,
                       mmax=None,
                       min_child_pt=0,
                       max_doca=None,
                       vchi2pdof_max=25):

    criteria_comb = {
        "mmin": mmin,
        "mmax": mmax,
        "min_child_pt": min_child_pt,
        "max_doca": max_doca,
        "vchi2pdof_max": vchi2pdof_max
    }

    return criteria_comb


@configurable
def make_charm2hadrons(name="",
                       process="",
                       decay="",
                       particle="",
                       CriteriaParticle="",
                       CriteriaCombination=""):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    dau1 = define_child(particle[0], CriteriaParticle[0])
    dau2 = define_child(particle[1], CriteriaParticle[1])

    combination_code = F.require_all(
        in_range(CriteriaCombination["mmin"], F.MASS,
                 CriteriaCombination["mmin"]),
        F.SUM(F.PT > CriteriaCombination["min_child_pt"]) > 0)

    if CriteriaCombination["max_doca"] is not None:
        combination_code &= F.MAXDOCACHI2CUT(CriteriaCombination["max_doca"])

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"])

    return ParticleCombiner(
        name=name,
        Inputs=[dau1, dau2],
        DecayDescriptor=decay,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_charm3hadrons(name="",
                       process="",
                       decay="",
                       particle="",
                       CriteriaParticle="",
                       CriteriaCombination=""):
    """Return a decay maker for either of the decays:
    1. D-  -> K- pi+ pi+
    2. Ds- -> K- K+  pi+
    3. Lc  -> K pi p
    """

    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    dau1 = define_child(particle[0], CriteriaParticle[0])
    dau2 = define_child(particle[1], CriteriaParticle[1])
    dau3 = define_child(particle[2], CriteriaParticle[2])

    combination_code = F.require_all(
        in_range(CriteriaCombination["mmin"], F.MASS,
                 CriteriaCombination["mmin"]),
        F.SUM(F.PT > CriteriaCombination["min_child_pt"]) > 0)

    if CriteriaCombination["max_doca"] is not None:
        combination_code &= F.MAXDOCACHI2CUT(CriteriaCombination["max_doca"])

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"])
    combination12_code = F.require_all(F.MAXDOCACUT(0.5 * mm))

    return ParticleCombiner(
        name=name,
        Inputs=[dau1, dau2, dau3],
        DecayDescriptor=decay,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_charm4hadrons(name="",
                       process="",
                       decay="",
                       particle="",
                       CriteriaParticle="",
                       CriteriaCombination="",
                       allchild_ptcut=0.25 * GeV,
                       twochild_ptcut=0.25 * GeV,
                       onechild_ptcut=0.25 * GeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    daus = [
        define_child(particle[0], CriteriaParticle[0]),
        define_child(particle[1], CriteriaParticle[1]),
        define_child(particle[2], CriteriaParticle[2]),
        define_child(particle[3], CriteriaParticle[3])
    ]

    allhaschild_pt = F.SUM(F.PT > allchild_ptcut) > 3
    twohaschild_pt = F.SUM(F.PT > twochild_ptcut) > 1
    ahaschild_pt = F.SUM(F.PT > onechild_ptcut) > 0

    combination_code = F.require_all(
        in_range(CriteriaCombination["mmin"], F.MASS,
                 CriteriaCombination["mmin"]),
        F.MAXDOCACHI2CUT(CriteriaCombination["max_doca"]), allhaschild_pt,
        twohaschild_pt, ahaschild_pt)

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"])
    combination12_code = F.require_all(F.MAXDOCACUT(0.5 * mm))
    return ParticleCombiner(
        name=name,
        Inputs=daus,
        DecayDescriptor=decay,
        CombinationCut=combination_code,
        Combination12Cut=combination12_code,
        CompositeCut=vertex_code)

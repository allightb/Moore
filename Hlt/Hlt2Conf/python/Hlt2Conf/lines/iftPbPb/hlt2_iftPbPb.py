###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs
from RecoConf.event_filters import require_pvs
from PyConf import configurable

from RecoConf.event_filters import require_gec

PbSMOG_hlt1 = [
    "Hlt1BeamGasDecision", "Hlt1PbSMOGMicroBiasDecision",
    "Hlt1PbSMOGMBOneTrackDecision"
]

PbPb_hlt1 = [
    "Hlt1HeavyIonPbPbMicroBias", "Hlt1HeavyIonPbPbPeripheral",
    "Hlt1HeavyIonPbPbCentral", "Hlt1HeavyIonPbPbUPCMB", "Hlt1PbPbMBOneTrack"
]

all_lines = {}
PROCESS = "hlt2"

CUTGEC = 30000


## Mass ranges
## simple prefilters that requires at least one PV ##
def prefilters():
    return [require_pvs(make_pvs())]  ## might be adapted in case of PbPb


@register_line_builder(all_lines)
@configurable
def passthrough(name='Hlt2IFT_PassThrough',
                prescale=1,
                persistreco=True,
                cutGeC=CUTGEC):
    algs = [require_gec(cut=cutGeC)]
    return Hlt2Line(
        name=name, algs=algs, prescale=prescale, persistreco=persistreco)

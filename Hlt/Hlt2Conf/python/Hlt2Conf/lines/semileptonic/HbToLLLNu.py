###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders specific for the B->3 leptons neutrino lines.
This includes the following:

Physics lines:

Hlt2SLB_B2MuMuMuNu
Hlt2SLB_B2EMuMuNu
Hlt2SLB_B2MuEENu
Hlt2SLB_B2EEENu
Hlt2SLB_B2TauMuMuNu_3Pi
Hlt2SLB_B2TauEENu_3Pi

Same-sign leptons lines:

Hlt2SLB_B2MuMuMuNu_SS
Hlt2SLB_B2EMuMuNu_SS
Hlt2SLB_B2MuEENu_SS
Hlt2SLB_B2EEENu_SS
Hlt2SLB_B2TauMuMuNu_3Pi_SS
Hlt2SLB_B2TauEENu_3Pi_SS

Unphysical prescaled lines (loose PID):

Hlt2SLB_B2MuMuMuNu_TriFake
Hlt2SLB_B2MuMuMuNu_OneFake
Hlt2SLB_B2EMuMuNu_FakeE
Hlt2SLB_B2MuEENu_FakeMu
Hlt2SLB_B2EEENu_TriFake
Hlt2SLB_B2EEENu_OneFake

The cuts in the leptonic lines are loose enough to comprise also leptonic tau decays
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, GeV
from .builders.base_builder import make_muons_from_b, make_fake_muons_from_b_notIsMuon, make_electrons_from_b, make_tauons_hadronic_decay
from .builders.b_builder import make_b2lllnu
from .builders.dilepton_builder import make_detached_dimuon_for_b2lllnu, make_detached_dielectron_for_b2lllnu

all_lines = {}

## Physics lines first ##


def make_b2mumumunu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+mu-mu+nu decays
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_muons_from_b(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9., pid=(F.PID_MU > 0.0))

    bps = make_b2lllnu(
        particles=[muons, muons, muons],
        descriptor="[B+ -> mu+ mu+ mu-]cc",
        name=name + "Combiner")

    return bps


def make_b2emumunu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+mu-mu+nu decays
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV, p_min=2. * GeV, mipchi2_min=9., pid=(F.PID_E > 2.0))

    dimuons = make_detached_dimuon_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dimuons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2mueenu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+e-e+nu decays
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_muons_from_b(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9., pid=(F.PID_MU > 2.0))
    dielectrons = make_detached_dielectron_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


def make_b2eeenu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+e-e+nu decays
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV, p_min=2. * GeV, mipchi2_min=9., pid=(F.PID_E > 2.0))
    dielectrons = make_detached_dielectron_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2taumumunu_3pi(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+mu-mu+nu decays with hadronic tau->3pi
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dimuons = make_detached_dimuon_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dimuons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner")

    return bps


def make_b2taueenu_3pi(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+e-e+nu decays with hadronic tau->3pi
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dielectrons = make_detached_dielectron_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dielectrons, tauons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


## Unphysical lines: same-sign ##


def make_b2mumumunu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+mu+mu+nu decays: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_muons_from_b(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9., pid=(F.PID_MU > 0.0))

    bps = make_b2lllnu(
        particles=[muons, muons, muons],
        descriptor="[B+ -> mu+ mu+ mu+]cc",
        name=name + "Combiner")

    return bps


def make_b2emumunu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+mu+mu+nu decays: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV, p_min=2. * GeV, mipchi2_min=9., pid=(F.PID_E > 2.0))

    dimuons = make_detached_dimuon_for_b2lllnu(same_sign=True)
    bps = make_b2lllnu(
        particles=[dimuons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2mueenu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+e+e+nu decays: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_muons_from_b(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9., pid=(F.PID_MU > 2.0))
    dielectrons = make_detached_dielectron_for_b2lllnu(opposite_sign=False)
    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


def make_b2eeenu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+e+e+nu decays: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV, p_min=2. * GeV, mipchi2_min=9., pid=(F.PID_E > 2.0))
    dielectrons = make_detached_dielectron_for_b2lllnu(opposite_sign=False)
    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2taumumunu_3pi_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+mu+mu+nu decays with hadronic tau->3pi: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dimuons = make_detached_dimuon_for_b2lllnu(same_sign=True)
    bps = make_b2lllnu(
        particles=[dimuons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner")

    return bps


def make_b2taueenu_3pi_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+e+e+nu decays with hadronic tau->3pi: same-sign combinations
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    tauons = make_tauons_hadronic_decay()
    dielectrons = make_detached_dielectron_for_b2lllnu(opposite_sign=False)
    bps = make_b2lllnu(
        particles=[dielectrons, tauons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


## Fake lepton lines ##


def make_b2mumumunu_trifakemuon(name, process, prescale=0.01):
    """
    SL line for the B(c)+ -> mu+mu-mu+nu decays: three fake muons
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_fake_muons_from_b_notIsMuon(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9.)

    bps = make_b2lllnu(
        particles=[muons, muons, muons],
        descriptor="[B+ -> mu+ mu+ mu-]cc",
        name=name + "Combiner")

    return bps


def make_b2mumumunu_onefakemuon(name, process, prescale=0.1):
    """
    SL line for the B(c)+ -> mu+mu-mu+nu decays: one fake muon
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_fake_muons_from_b_notIsMuon(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9.)
    dimuons = make_detached_dimuon_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[muons, dimuons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


def make_b2emumunu_fakeelectron(name, process, prescale=0.1):
    """
    SL line for the B(c)+ -> e+mu-mu+nu decays: fake electron
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV,
        p_min=2. * GeV,
        mipchi2_min=9.,
        pid=(F.PID_E > -10.0))

    dimuons = make_detached_dimuon_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dimuons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2mueenu_fakemuon(name, process, prescale=0.05):
    """
    SL line for the B(c)+ -> mu+e-e+nu decays: fake muon
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    muons = make_fake_muons_from_b_notIsMuon(
        pt_min=0. * MeV, p_min=3. * GeV, mipchi2_min=9.)
    dielectrons = make_detached_dielectron_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner")

    return bps


def make_b2eeenu_trifakeelectron(name, process, prescale=0.01):
    """
    SL line for the B(c)+ -> e+e-e+nu decays: three fake electrons
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV,
        p_min=2. * GeV,
        mipchi2_min=9.,
        pid=(F.PID_E > -8.0))
    dielectrons = make_detached_dielectron_for_b2lllnu(pid_e_min=-10.)
    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps


def make_b2eeenu_onefakeelectron(name, process, prescale=0.05):
    """
    SL line for the B(c)+ -> e+e-e+nu decays: one fake electron
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    electrons = make_electrons_from_b(
        pt_min=250. * MeV,
        p_min=2. * GeV,
        mipchi2_min=9.,
        pid=(F.PID_E > -10.0))
    dielectrons = make_detached_dielectron_for_b2lllnu()
    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner")

    return bps

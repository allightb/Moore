###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .builders.base_builder import make_tauons_muonic_decay, make_fake_tauons_muonic_decay, make_kaons_from_b
from .builders.b_builder import make_b2xulnu


def make_bstoktaunu_muonic(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu ) nu.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons = make_kaons_from_b()
    taus = make_tauons_muonic_decay()
    line_alg = make_b2xulnu(
        particles=[kaons, taus], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg


def make_bstoktaunu_muonic_fakemu(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu) nu, with a fake muon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons = make_kaons_from_b()
    taus_nopid = make_fake_tauons_muonic_decay()
    line_alg = make_b2xulnu(
        particles=[kaons, taus_nopid], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg


def make_bstoktaunu_muonic_fakek(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu) , with a fake kaon.
    """
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    kaons_nopid = make_kaons_from_b(pid=None)
    taus = make_tauons_muonic_decay()
    line_alg = make_b2xulnu(
        particles=[kaons_nopid, taus], descriptor="[B_s~0 -> K+ mu-]cc")

    return line_alg

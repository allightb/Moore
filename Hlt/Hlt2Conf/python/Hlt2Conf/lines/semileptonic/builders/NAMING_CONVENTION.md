# Naming convention for builder development

It's worth establishing a naming convention for the builders committed to Moore and made available
to the WG analysts. This is to prevent the erroneous adoption of a builder when developing lines
and to clearly distinguish potentially ambiguous builders, _e.g._ whether a fake muon is identified by failing a DLLmu
cut or by `!PP_ISMUON`. The list below may not be exhaustive; if you find the need to add another
item, please contant the MiCo team before doing so to discuss the need and naming.

Last revision: [blaise.delaney@cern.ch](mailto:blaise.delaney@cern.ch), 09/02/2021

### General naming conventions
A set of naming conventions for the lines has been established centrally; these are listed in [Moore#60](https://gitlab.cern.ch/lhcb/Moore/-/issues/60). Please consult these guidelines before committing a line. In addition, builder-specific conventions must be followed when naming the configurables needed to implement the various lines. These are listed in the following sections.
### Leptons
- **Fake leptons**:  general format of builder: `make_fake{leptons}_{fakerequirement}()`, where `{leptons}=[electrons, muons, tauons]` and `{fakerequirement}` can be one of the following: 
    -   `make_fake{leptons}_notismuon()`: configurable returning a lepton builder using `with standard_protoparticle_filter.bind(Code='!PP_ISMUON')`; currently implemented for `{leptons}=muons`;
    -   `make_fake{leptons}_antidll()`: configurable returning a lepton builder with `pid='PIDmu<0.0'`; currently implemented for `{leptons}=muons`;
    
- **Taus**: depending on whether you wish to build a muonic or hadronic tau decay, please choose between `make_tauons_muonic_decay()` and `make_tauons_hadronic_decay()`;

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of dilepton builders, used by the B->3lnu lines
"""
import Functors as F
from PyConf import configurable
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_detached_dielectron, make_detached_dielectron_with_brem
from .base_builder import make_muons_from_b


@configurable
def make_detached_dimuon_for_b2lllnu(name='detached_dimuon_for_b2lllnu_{hash}',
                                     parent_id='J/psi(1S)',
                                     pt_dimuon_min=0. * MeV,
                                     pt_muon_min=300. * MeV,
                                     p_muon_min=3000. * MeV,
                                     ipchi2_muon_min=9.,
                                     pidmu_muon_min=2.,
                                     adocachi2cut_max=30.,
                                     bpvvdchi2_min=20.,
                                     vchi2pdof_max=30.,
                                     am_min=0. * MeV,
                                     am_max=7000. * MeV,
                                     same_sign=False):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """
    muons = make_muons_from_b(
        pt_min=pt_muon_min,
        p_min=p_muon_min,
        mipchi2_min=ipchi2_muon_min,
        pid=(F.PID_MU > pidmu_muon_min))

    DecayDescriptor = f'{parent_id} -> mu+ mu-'
    if same_sign: DecayDescriptor = f'[{parent_id} -> mu+ mu+]cc'

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_dimuon_min,
        F.MAXDOCACHI2CUT(adocachi2cut_max))
    pvs = make_pvs()
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVFDCHI2(pvs) > bpvvdchi2_min)
    return ParticleCombiner([muons, muons],
                            name=name,
                            DecayDescriptor=DecayDescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_detached_dielectron_for_b2lllnu(
        name='detached_dielectron_for_b2lllnu_{hash}',
        pid_e_min=2.,
        pt_e_min=0.25 * GeV,
        ipchi2_e_min=9.,
        parent_id="J/psi(1S)",
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=20.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """
    dielectrons = make_detached_dielectron_with_brem(
        opposite_sign=opposite_sign,
        PIDe_min=pid_e_min,
        pt_e=pt_e_min,
        minipchi2=ipchi2_e_min,
        dielectron_ID=parent_id,
        pt_diE=pt_diE_min,
        m_diE_min=am_min,
        m_diE_max=am_max,
        adocachi2cut=adocachi2cut_min,
        bpvvdchi2=bpvvdchi2_min,
        vfaspfchi2ndof=vfaspfchi2ndof_max)
    code = F.require_all(F.MASS > am_min, F.MASS < am_max)
    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


@configurable
def make_detached_dielectron_for_b2dlllnu(
        name='detached_dielectron_for_b2dlllnu_{hash}',
        pid_e_min=2.,
        pt_e_min=50 * MeV,
        p_e_min=0. * MeV,
        ipchi2_e_min=4.,
        parent_id="J/psi(1S)",
        opposite_sign=True,
        pt_diE_min=0. * MeV,
        adocachi2cut_min=30.,
        bpvvdchi2_min=30.,
        vfaspfchi2ndof_max=10.,
        am_min=0. * MeV,
        am_max=6000. * MeV,
        with_brem=True):
    """
    Make the detached e+e- pair with/without the proper bremsstrahlung correction handling.
    """
    if with_brem:
        return make_detached_dielectron_with_brem(
            opposite_sign=opposite_sign,
            PIDe_min=pid_e_min,
            pt_e=pt_e_min,
            p_e=p_e_min,
            minipchi2=ipchi2_e_min,
            dielectron_ID=parent_id,
            pt_diE=pt_diE_min,
            m_diE_min=am_min,
            m_diE_max=am_max,
            adocachi2cut=adocachi2cut_min,
            bpvvdchi2=bpvvdchi2_min,
            vfaspfchi2ndof=vfaspfchi2ndof_max)
    else:
        dielectrons = make_detached_dielectron(
            opposite_sign=opposite_sign,
            dilepton_ID=parent_id,
            pid_e=pid_e_min,
            pt_e=pt_e_min,
            minipchi2=ipchi2_e_min,
            adocachi2cut=adocachi2cut_min,
            bpvvdchi2=bpvvdchi2_min,
            vfaspfchi2ndof=vfaspfchi2ndof_max)

        code = F.require_all(F.PT > pt_diE_min, F.MASS > am_min,
                             F.MASS < am_max)
        return ParticleFilter(dielectrons, F.FILTER(code), name=name)

###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Semileptonic HH builder
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.algorithms_thor import ParticleCombiner
from PyConf import configurable


@configurable
def make_hh(particles,
            descriptors,
            pvs,
            name='HHCombiner_{hash}',
            m_min=1800 * MeV,
            m_max=5300 * MeV,
            pt_min=None,
            fdchi2_min=25.0,
            vtx_chi2pdof_max=9.0,
            bpvdira_min=0.999):

    combination_code = in_range(m_min, F.MASS, m_max)
    vertex_code = F.require_all(
        F.BPVDIRA(pvs) > bpvdira_min, F.CHI2DOF < vtx_chi2pdof_max)
    if pt_min is not None:
        vertex_code = F.require_all(vertex_code, F.PT > pt_min)
    if fdchi2_min is not None:
        vertex_code = F.require_all(vertex_code, F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

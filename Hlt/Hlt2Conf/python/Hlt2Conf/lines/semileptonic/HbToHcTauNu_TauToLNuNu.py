###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Definition of semileptonic selections for the Hb -> Hc tau nu topology,
with tau -> l nu nu

Non trivial imports:
basic builders, prefilters

Returns:
every function returns a line_alg
"""

from Hlt2Conf.algorithms_thor import ParticleContainersMerger

from .builders.base_builder import (
    make_tauons_muonic_decay, make_fake_tauons_muonic_decay,
    make_tauons_electronic_decay, make_fake_tauons_electronic_decay,
    make_candidate)
from .builders.charm_hadron_builder import (
    make_dplus_tokpipi, make_ds_tokkpi, make_d0_tokpi, make_d0_tok3pi,
    make_lambdac_topkpi, make_omegac_topkkpi, make_lambdac_topks,
    make_lambdac_tolambda0pi, make_Hc_to_nbody)
from .builders.b_builder import make_b2xtaunu
from .builders.charm_hadron_builder import make_xicplus_topkpi, make_xic0_topkkpi, make_jpsi_tomumu
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
import Functors as F

from PyConf import ConfigurationError


def make_bctojpsitaunu_jpsitomumu_tautolnunu(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu ) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV, comb_m_max=3217 * MeV, vchi2pdof_max=4)
    bc_jpsimum = make_b2xtaunu(
        [jpsi, tauons],
        descriptor=descriptor_rs,
        name=f"Bc2JpsiTauNu_Tau2{lepton}NuNu_Jpsi2MuMu_combiner")
    return bc_jpsimum


def make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu ) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV, comb_m_max=3217 * MeV, vchi2pdof_max=4)
    bc_jpsimum = make_b2xtaunu(
        [jpsi, tauons],
        descriptor=descriptor_rs,
        name=f"Bc2JpsiTauNu_Tau2{lepton}NuNu_Jpsi2MuMu_fakeL_combiner")
    return bc_jpsimum


def make_butod0taunu_d0tokpi_tautolnunu(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        taus = make_tauons_muonic_decay()
        decay_descriptor = "[B- -> D0 mu-]cc"
        decay_descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        taus = make_tauons_electronic_decay()
        decay_descriptor = "[B- -> D0 e-]cc"
        decay_descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    extraCuts = {
        'comb_docachi2_max': None,
        'daughter_pt_min': 300.0 * MeV,
    }
    if lepton == "e":
        extraCuts = {
            'comb_docachi2_max': 5.,
            'daughter_pt_min': 750.0 * MeV,
        }
    p_min = 15. * GeV if lepton == "e" else 5.0 * GeV  # Tighter D0 cut
    with make_candidate.bind(p_min=p_min):
        d0s = make_d0_tokpi(
            vchi2pdof_max=4,
            bpvdira_min=0.999,
            comb_pt_min=2000 * MeV,
            comb_pt_any_min=800 * MeV,
            comb_pt_sum_min=2.5 * GeV,
            comb_doca_max=0.1 * mm,
            daughter_mipchi2_min=9.0,
            kaon_pid=(F.PID_K > 4.0),
            pion_pid=(F.PID_K < 2.0),
            **extraCuts)

    b0s_rightsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor,
        comb_m_max=10000.0 * MeV,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_combiner")

    b0s_wrongsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor_ws,
        comb_m_max=10000.0 * MeV,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_WS_combiner")

    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0taunu_d0tokpi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        taus = make_fake_tauons_muonic_decay()
        decay_descriptor = "[B- -> D0 mu-]cc"
        decay_descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        taus = make_fake_tauons_electronic_decay()
        decay_descriptor = "[B- -> D0 e-]cc"
        decay_descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    extraCuts = {
        'comb_docachi2_max': None,
        'daughter_pt_min': 300.0 * MeV,
    }
    if lepton == "e":
        extraCuts = {
            'comb_docachi2_max': 5.,
            'daughter_pt_min': 750.0 * MeV,
        }
    p_min = 15. * GeV if lepton == "e" else 5.0 * GeV  # Tighter D0 cut
    with make_candidate.bind(p_min=p_min):
        d0s = make_d0_tokpi(
            vchi2pdof_max=4,
            bpvdira_min=0.999,
            comb_pt_min=2000 * MeV,
            comb_pt_any_min=800 * MeV,
            comb_pt_sum_min=2.5 * GeV,
            comb_doca_max=0.1 * mm,
            daughter_mipchi2_min=9.0,
            kaon_pid=(F.PID_K > 4.0),
            pion_pid=(F.PID_K < 2.0),
            **extraCuts)

    b0s_rightsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor,
        comb_m_max=10000.0 * MeV,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_fakeL_combiner")

    b0s_wrongsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor_ws,
        comb_m_max=10000.0 * MeV,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_fakeL_WS_combiner")

    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0taunu_d0tok3pi_tautolnunu(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    extraCuts = {
        'comb_docachi2_max': None,
        'daughter_pt_min': 300.0 * MeV,
    }
    if lepton == "e":
        extraCuts = {
            'comb_docachi2_max': 5.,
            'daughter_pt_min': 750.0 * MeV,
        }
    p_min = 15. * GeV if lepton == "e" else 5.0 * GeV  # Tighter D0 cut
    with make_candidate.bind(p_min=p_min):
        dzs = make_d0_tok3pi(**extraCuts)
    bus_dzmum = make_b2xtaunu(
        [dzs, tauons],
        descriptor=descriptor_rs,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_combiner")
    bus_dzmup_wrongsign_D = make_b2xtaunu(
        [dzs, tauons],
        descriptor=descriptor_ws,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_WS_combiner")
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup_wrongsign_D])

    return line_alg


def make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    extraCuts = {
        'comb_docachi2_max': None,
        'daughter_pt_min': 300.0 * MeV,
    }
    if lepton == "e":
        extraCuts = {
            'comb_docachi2_max': 5.,
            'daughter_pt_min': 750.0 * MeV,
        }
    p_min = 15. * GeV if lepton == "e" else 5.0 * GeV  # Tighter D0 cut
    with make_candidate.bind(p_min=p_min):
        dzs = make_d0_tok3pi(**extraCuts)
    bus_dzmum = make_b2xtaunu(
        [dzs, fake_tauons],
        descriptor=descriptor_rs,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_fakeL_combiner")
    bus_dzmup_wrongsign_D = make_b2xtaunu(
        [dzs, fake_tauons],
        descriptor=descriptor_ws,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup_wrongsign_D])

    return line_alg


def make_b0todptaunu_dptokpipi_tautolnunu(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor=descriptor_rs,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_combiner")
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor=descriptor_ws,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_WS_combiner")
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, fake_tauons],
        descriptor=descriptor_rs,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_fakeL_combiner")
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, fake_tauons],
        descriptor=descriptor_ws,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_fakeL_WS_combiner")
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_bstodstaunu_dstokkpi_tautolnunu(process, lepton):
    """
    Selection for the decay Bs0 -> Ds-(-> K K pi) tau+(-> l nu nu) nu, and combinatorial (same sign).
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    #kaons = make_kaons() #maybe put in line instantiation for extra output?
    #pions = make_pions()
    dss = make_ds_tokkpi()
    bss_rightsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor=descriptor_rs,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_combiner")
    bss_wrongsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor=descriptor_ws,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_WS_combiner")
    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay Bs0 -> Ds-(-> K K pi) tau+(-> l nu nu) nu and combinatorial (same sign), with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    #kaons = make_kaons() maybe put in line instantiation for extra output?
    #pions = make_pions()
    dss = make_ds_tokkpi()
    bss_rightsign = make_b2xtaunu(
        particles=[dss, fake_tauons],
        descriptor=descriptor_rs,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_fakeL_combiner")
    bss_wrongsign = make_b2xtaunu(
        particles=[dss, fake_tauons],
        descriptor=descriptor_ws,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_fakeL_WS_combiner")

    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautolnu(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) tau-(-> l nu nu) nu]cc.
    """
    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    lcs = make_lambdac_topkpi(
        mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
        mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
        mother_pt_min=2500 * MeV,
        comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
        comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        comb_pt_min=None,
        comb_pt_any_min=None,
        comb_pt_sum_min=2500 * MeV,
        daughter_p_min=2000 * MeV,
        daughter_pt_min=300 * MeV,
        daughter_mipchi2_min=9,
        kaon_pid=(F.PID_K > 4.),
        pion_pid=(F.PID_K < 2.),
        proton_pid=(F.PID_P > 0))

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_rs,
        comb_m_max=10200. * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_combiner")

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_ws,
        comb_m_max=10200. * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_WS_combiner")

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautolnu_fakelepton(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) tau-(-> l nu nu) nu]cc and combinatorial (same sign) with a fake muon.
    """
    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if lepton == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must either mu or e")

    lcs = make_lambdac_topkpi(
        mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
        mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
        mother_pt_min=2500. *
        MeV,  # in place of (SUMTREE( PT, ISBASIC )> 2500.0 * MeV)
        comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
        comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        comb_pt_min=None,
        comb_pt_any_min=None,
        comb_pt_sum_min=2500. * MeV,
        daughter_p_min=2000. * MeV,
        daughter_pt_min=300. * MeV,
        daughter_mipchi2_min=9.,
        kaon_pid=(F.PID_K > 4.),
        pion_pid=(F.PID_K < 2.),
        proton_pid=(F.PID_P > 0.))

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_rs,
        comb_m_max=10200. * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_fakeL_combiner")

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_ws,
        comb_m_max=10200. * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_fakeL_WS_combiner")

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctoV0h_tautolnu(process, V0_name, V0_type, lepton_name):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'K0S' or a 'Lambda0'.
    When 'V0'='K0S', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(
            vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV)

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(process, V0_name, V0_type,
                                                 lepton_name):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'K0S' or a 'Lambda0'.
    When 'V0'='K0S', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(
            vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV)

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_xib0toxicplustaunu_xicplustopkpi_tautomununu(process):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    #make tau candidates
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    taus = make_tauons_muonic_decay()

    #make Xi_c+ candidates
    xicplus_mass = 2467.71  # units MeV (PDG 2020)
    delta_xicplus_comb = 90.0  # units MeV
    comb_m_min = (xicplus_mass - delta_xicplus_comb)
    comb_m_max = (xicplus_mass + delta_xicplus_comb)
    delta_xicplus_mother = 80.0  # units MeV
    mother_m_min = (xicplus_mass - delta_xicplus_mother)
    mother_m_max = (xicplus_mass + delta_xicplus_mother)
    kaon_pid = (F.PID_K > 2.)
    pion_pid = (F.PID_K < 4.)
    proton_pid = F.require_all(F.PID_P > 1., (F.PID_P - F.PID_K) > 0.)
    xicplus = make_xicplus_topkpi(
        comb_m_min=comb_m_min * MeV,  #default is hardcoded
        comb_m_max=comb_m_max * MeV,  #default is hardcoded
        mother_m_min=mother_m_min * MeV,  #default is hardcoded
        mother_m_max=mother_m_max * MeV,  #default is hardcoded
        kaon_pid=kaon_pid,
        pion_pid=pion_pid,
        proton_pid=proton_pid)

    #make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, taus])
    xibs_ws = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, taus])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakelepton(process):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    #make tau candidates
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    fake_taus = make_fake_tauons_muonic_decay()

    #make Xi_c+ candidates
    xicplus_mass = 2467.71  # units MeV (PDG 2020)
    delta_xicplus_comb = 90.0  # units MeV
    comb_m_min = (xicplus_mass - delta_xicplus_comb)
    comb_m_max = (xicplus_mass + delta_xicplus_comb)
    delta_xicplus_mother = 80.0  # units MeV
    mother_m_min = (xicplus_mass - delta_xicplus_mother)
    mother_m_max = (xicplus_mass + delta_xicplus_mother)
    kaon_pid = (F.PID_K > 2.)
    pion_pid = (F.PID_K < 4.)
    proton_pid = F.require_all(F.PID_P > 1., (F.PID_P - F.PID_K) > 0.)
    xicplus = make_xicplus_topkpi(
        comb_m_min=comb_m_min * MeV,  #default is hardcoded
        comb_m_max=comb_m_max * MeV,  #default is hardcoded
        mother_m_min=mother_m_min * MeV,  #default is hardcoded
        mother_m_max=mother_m_max * MeV,  #default is hardcoded
        kaon_pid=kaon_pid,
        pion_pid=pion_pid,
        proton_pid=proton_pid)

    #make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, fake_taus])
    xibs_ws = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, fake_taus])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0taunu_xic0topkkpi_tautomununu(process):
    """
    SL line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu+ nu nu) nu (Wrong sign)
    """
    #make light lepton candidates
    taus = make_tauons_muonic_decay()
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    #make Xi_c0 candidates
    xic0_mass = 2470.44  # units MeV (PDG 2020)
    delta_xic0_mother = 80.0  # units MeV
    mother_m_min = (xic0_mass - delta_xic0_mother)
    mother_m_max = (xic0_mass + delta_xic0_mother)
    delta_xic0_comb = 90.0  # units MeV
    comb_m_min = (xic0_mass - delta_xic0_comb)
    comb_m_max = (xic0_mass + delta_xic0_comb)
    xic0 = make_xic0_topkkpi(
        comb_m_min=comb_m_min * MeV,
        comb_m_max=comb_m_max * MeV,
        mother_m_min=mother_m_min * MeV,
        mother_m_max=mother_m_max * MeV)

    #make Xib- candidates
    xibs_rs = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_RS",
        descriptor=descriptor_rs,
        particles=[xic0, taus])
    xibs_ws = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_WS",
        descriptor=descriptor_ws,
        particles=[xic0, taus])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakelepton(process):
    """
    SL line for the decays with a fake lepton:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu+ nu nu) nu (Wrong sign)
    """
    #make light lepton candidates
    fake_taus = make_fake_tauons_muonic_decay()
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    #make Xi_c0 candidates
    xic0_mass = 2470.44  # units MeV (PDG 2020)
    delta_xic0_mother = 80.0  # units MeV
    mother_m_min = (xic0_mass - delta_xic0_mother)
    mother_m_max = (xic0_mass + delta_xic0_mother)
    delta_xic0_comb = 90.0  # units MeV
    comb_m_min = (xic0_mass - delta_xic0_comb)
    comb_m_max = (xic0_mass + delta_xic0_comb)
    xic0 = make_xic0_topkkpi(
        comb_m_min=comb_m_min * MeV,
        comb_m_max=comb_m_max * MeV,
        mother_m_min=mother_m_min * MeV,
        mother_m_max=mother_m_max * MeV)

    #make Xib- candidates
    xibs_rs = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xic0, fake_taus])
    xibs_ws = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xic0, fake_taus])
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_omegabtoomegactaunu_omegactopkkpi_tautomununu(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu]cc.
    """

    Omegac_mass = 2695.2  # units MeV
    delta_Omegac_comb = 90.0  # units MeV, Delta combination
    delta_Omegac_mother = 60.0  # units MeV, Delta composite

    tauons = make_tauons_muonic_decay()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi(
        mother_m_min=(Omegac_mass - delta_Omegac_mother) * MeV,
        mother_m_max=(Omegac_mass + delta_Omegac_mother) * MeV,
        comb_m_min=(Omegac_mass - delta_Omegac_comb) * MeV,
        comb_m_max=(Omegac_mass + delta_Omegac_comb) * MeV)

    omegab_rightsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_combiner")
    omegab_wrongsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_WS_combiner")
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg


def make_omegabtoomegactaunu_omegactopkkpi_tautomununu_fakelepton(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu]cc with a fake lepton.
    """

    Omegac_mass = 2695.2  # units MeV
    delta_Omegac_comb = 90.0  # units MeV, Delta combination
    delta_Omegac_mother = 60.0  # units MeV, Delta composite

    # Fake muons
    tauons = make_fake_tauons_muonic_decay()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi(
        mother_m_min=(Omegac_mass - delta_Omegac_mother) * MeV,
        mother_m_max=(Omegac_mass + delta_Omegac_mother) * MeV,
        comb_m_min=(Omegac_mass - delta_Omegac_comb) * MeV,
        comb_m_max=(Omegac_mass + delta_Omegac_comb) * MeV)

    omegab_rightsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_fakeL_combiner")
    omegab_wrongsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_fakeL_WS_combiner")
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg

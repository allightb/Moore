###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""These lines define high-mass dimuon selections used for general calibration
purposes (mostly related to tracking detectors). They overlap mostly
with the lines from the QEE WG.

Cuts applied are inspired by the Run-3 Drell-Yan HLT2 lines (in QEE).

If there are default function arguments, the cut values applied
are set/configured there. If they are absent, they are intended
to be set downstream.

Contact: Laurent Dufour <laurent.dufour@cern.ch>
"""
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, mm

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction, make_pvs
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon

from Moore.streams import DETECTORS

turbo_lines = {}

### Line configuration
mass_ranges = [[
    8.0 * GeV, 11.5 * GeV, "Upsilon", 1.0, "Upsilon(1S) -> mu+ mu-"
], [60. * GeV, 125 * GeV, "Z0", 1.0, "Z0 -> mu+ mu-"]]
# third argument:  name
# fourth argument: prescale
# fifth argument:  decay descriptor


@configurable
def filter_muons(
        particles,
        pvs,
        min_probnnmu,
        min_ip=-1 * mm,
        max_ip=0.3 * mm,
        min_pt=1.8 * GeV,
        min_eta=1.9,
        max_eta=5.1,
        min_p=20. * GeV,
):
    muon_requirements = F.require_all(
        in_range(min_eta, F.ETA, max_eta), F.PT >= min_pt, F.P >= min_p,
        F.PROBNN_MU >= min_probnnmu, in_range(min_ip, F.MINIP(pvs), max_ip))

    return ParticleFilter(particles, F.FILTER(muon_requirements))


@configurable
def combine_muons_for_decay_descriptor(decay_descriptor,
                                       pvs,
                                       name,
                                       min_mass,
                                       max_mass,
                                       min_probnnmu,
                                       max_doca=0.15 * mm,
                                       vchi2pdof_max=16):
    all_muons = make_ismuon_long_muon()

    filtered_muons = filter_muons(all_muons, pvs, min_probnnmu=min_probnnmu)

    combination_cuts = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.DOCA(1, 2) < max_doca)
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner([filtered_muons, filtered_muons],
                            name=name,
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_cuts,
                            CompositeCut=vertex_code)


@configurable
def make_dimuon_candidates(name, **kwargs):
    return combine_muons_for_decay_descriptor(name=name, **kwargs)


for mass_range in mass_ranges:
    prescale = mass_range[3]
    decay_descriptor = mass_range[4]
    name_suffix = f"QEE_Calibration_{mass_range[2]}_prompt"
    hlt2_name = f"Hlt2{name_suffix}"

    min_probnnmu = 0.0  # disabled option for now.

    # HLT2 lines
    @register_line_builder(turbo_lines)
    def hlt2_dimuon_calibration_line(name=hlt2_name,
                                     min_mass=mass_range[0],
                                     max_mass=mass_range[1],
                                     prescale=prescale,
                                     min_probnnmu=min_probnnmu):
        pvs = make_pvs()
        dimuons = make_dimuon_candidates(
            "Hlt2_" + name + "_combiner",
            decay_descriptor=decay_descriptor,
            pvs=pvs,
            min_mass=min_mass,
            max_mass=max_mass,
            min_probnnmu=min_probnnmu)

        return Hlt2Line(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(pvs), dimuons],
            persistreco=False,
            raw_banks=DETECTORS,
            prescale=prescale)

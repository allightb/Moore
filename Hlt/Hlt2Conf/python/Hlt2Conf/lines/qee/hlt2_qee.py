###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of all the QEE HLT2 lines.
"""

from Hlt2Conf.lines.qee import single_high_pt_muon
from Hlt2Conf.lines.qee import single_high_pt_electron
from Hlt2Conf.lines.qee import jets
from Hlt2Conf.lines.qee import high_mass_dimuon
from Hlt2Conf.lines.qee import high_mass_dielec
from Hlt2Conf.lines.qee import diphoton
from Hlt2Conf.lines.qee import b_to_majolep_majo_to_leplep
from Hlt2Conf.lines.qee import b_to_majolep_majo_to_leppi
from Hlt2Conf.lines.qee import wz_boson_rare_decays
from Hlt2Conf.lines.qee import drellyan
from Hlt2Conf.lines.qee import quarkonia
from Hlt2Conf.lines.qee import dielectron_persist_photons
from Hlt2Conf.lines.qee import dimuon_no_ip
from Hlt2Conf.lines.qee import calibration

full_lines = {}
full_lines.update(single_high_pt_muon.all_lines)
full_lines.update(single_high_pt_electron.all_lines)
full_lines.update(jets.all_lines)
full_lines.update(high_mass_dimuon.all_lines)
full_lines.update(high_mass_dielec.all_lines)
full_lines.update(dimuon_no_ip.full_lines)
full_lines.update(dielectron_persist_photons.full_lines)

turbo_lines = {}
turbo_lines.update(quarkonia.all_lines)
turbo_lines.update(b_to_majolep_majo_to_leplep.all_lines)
turbo_lines.update(b_to_majolep_majo_to_leppi.all_lines)
turbo_lines.update(diphoton.all_lines)
turbo_lines.update(dimuon_no_ip.turbo_lines)
turbo_lines.update(drellyan.all_lines)
turbo_lines.update(dielectron_persist_photons.turbo_lines)
turbo_lines.update(calibration.turbo_lines)
turbo_lines.update(wz_boson_rare_decays.all_lines)

###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Z->MuMu and same-sign dimuon (control sample) lines.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons

all_lines = {}


@configurable
def muon_filter_for_Z(particles, min_pt=0. * GeV):
    """ A muon filter: PT
    """
    return ParticleFilter(particles, F.FILTER(F.PT > min_pt))


@configurable
def make_Z_cand():
    muons_for_Z = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3. * GeV)
    return make_dimuon_novxt(muons_for_Z, muons_for_Z, "Z0 -> mu+ mu-",
                             40 * GeV)


@configurable
def make_Z_cand_SingleNoMuID():
    muon_ID, muon_noID = make_ismuon_long_muon(), make_long_muons()
    muons_for_Z_ID = muon_filter_for_Z(muon_ID, min_pt=3. * GeV)
    muons_for_Z_noID = muon_filter_for_Z(muon_noID, min_pt=3. * GeV)
    return make_dimuon_novxt(muons_for_Z_ID, muons_for_Z_noID,
                             "[Z0 -> mu+ mu-]cc", 40 * GeV)


@configurable
def make_Z_cand_DoubleNoMuID():
    muons_for_Z = muon_filter_for_Z(make_long_muons(), min_pt=3. * GeV)
    return make_dimuon_novxt(muons_for_Z, muons_for_Z, "Z0 -> mu+ mu-",
                             40 * GeV)


@configurable
def make_Zss_cand():
    muons = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3. * GeV)
    return make_dimuon_novxt(muons, muons, "[Z0 -> mu- mu-]cc", 16. * GeV)


@configurable
def make_dimuon_novxt(input_muon1,
                      input_muon2,
                      decaydescriptor,
                      min_mass=40. * GeV):
    """Dimuon combination with only a mass cut
    """

    # mass region of dimuons
    combination_code = (F.MASS > min_mass)

    return ParticleCombiner([input_muon1, input_muon2],
                            DecayDescriptor=decaydescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=F.ALL)


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_line(name='Hlt2QEE_ZToMuMuFull', prescale=1, persistreco=True):
    """Z0 boson decay to two muons line, both requiring ismuon"""

    z02mumu = make_Z_cand()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_single_nomuid_line(name='Hlt2QEE_ZToMuMu_SingleNoMuIDFull',
                                  prescale=1,
                                  persistreco=True):
    """Z0 boson decay to two muons line, where one requires ismuon, for efficiency studies"""

    z02mumu = make_Z_cand_SingleNoMuID()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_double_nomuid_line(name='Hlt2QEE_ZToMuMu_DoubleNoMuIDFull',
                                  prescale=0.01,
                                  persistreco=True):
    """Z0 boson decay to two muons line, where neither requires ismuon, for background studies"""

    z02mumu = make_Z_cand_DoubleNoMuID()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def same_sign_dimuon_line(name='Hlt2QEE_DiMuonSameSignFull',
                          prescale=1,
                          persistreco=True):
    """Z0 boson decay to two same-sign muons line"""

    mumuss = make_Zss_cand()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [mumuss],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )

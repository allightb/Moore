###############################################################################
# Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration          #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of useful QEE filters and builders
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_ismuon_long_muon, make_long_muons
from Hlt2Conf.standard_particles import make_has_rich_long_pions
from Hlt2Conf.standard_particles import make_long_electrons_with_brem, make_down_electrons_no_brem
from Hlt2Conf.standard_jets import make_particleflow, build_jets, tag_jets
from Hlt2Conf.lines.jets.topobits import make_topo_2body
from Hlt2Conf.standard_particles import make_photons
from Hlt2Conf.lines.qee.high_mass_dimuon import make_dimuon_novxt
from Hlt2Conf.lines.qee.high_mass_dielec import make_dielec_novxt

from RecoConf.reconstruction_objects import make_pvs


@configurable
def make_filter_tracks(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="qee_has_rich_long_pions",
        pt_min=0.25 * GeV,
        p_min=2. * GeV,
        trchi2dof_max=3,  #TBC with Reco
        trghostprob_max=0.4,  #TBC with Reco
        mipchi2dvprimary_min=None,
        pid=None):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
    )

    if pid is not None:
        code &= pid

    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def muon_filter(min_pt=0. * GeV, require_muID=True):
    #A muon filter: PT

    code = (F.PT > min_pt)
    particles = make_ismuon_long_muon() if require_muID else make_long_muons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter(min_pt=0. * GeV, min_electron_id=-1):
    #An electron filter: PT
    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_long_electrons_with_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter_down(min_pt=0. * GeV, min_electron_id=-1):
    #An down type electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_down_electrons_no_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_jets(name='SimpleJets_{hash}',
              min_pt=10 * GeV,
              JetsByVtx=True,
              tags=None):
    #Build and tag jets

    pflow = make_particleflow()
    svtags = make_topo_2body()
    jets = build_jets(pflow, JetsByVtx, name='JetBuilder' + name)

    if tags is not None:
        taggedjets = tag_jets(jets, svtags, name="Tags" + name)
        jets = taggedjets

    code = (F.PT > min_pt)

    return ParticleFilter(jets, F.FILTER(code))


@configurable
def make_qee_photons(name="qee_photons",
                     make_particles=make_photons,
                     pt_min=5. * GeV):
    code = (F.PT > pt_min)
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_qee_gamma_DD(name="qee_gamma_DD",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter_down(
        min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_qee_gamma_LL(name="qee_gamma_LL",
                      descriptor="gamma -> e+ e-",
                      am_max=0.5 * GeV,
                      m_max=0.1 * GeV,
                      pt_min=5. * GeV,
                      min_elec_pt=0. * GeV,
                      min_elec_id=0.,
                      maxVertexChi2=16):

    electrons = elec_filter(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = (F.MASS < am_max)
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max,
                                F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_photons(photon_type="DD", pt_min=10. * GeV):

    if photon_type == "LL":
        gamma = make_qee_gamma_LL()
    elif photon_type == "DD":
        gamma = make_qee_gamma_DD()
    else:
        gamma = make_qee_photons()

    code = F.require_all(F.PT > pt_min)

    return ParticleFilter(gamma, F.FILTER(code))


@configurable
def lepton_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        lepts = muon_filter(min_pt=min_pt)
    else:
        lepts = elec_filter(min_pt=min_pt)

    return lepts


@configurable
def Z_To_lepts_filter(lepton_type="mu", min_pt=10. * GeV):

    if lepton_type == "mu":
        muons_for_Z = muon_filter(min_pt=min_pt)
        zcand = make_dimuon_novxt(
            input_muon1=muons_for_Z,
            input_muon2=muons_for_Z,
            decaydescriptor="Z0 -> mu+ mu-",
            min_mass=40 * GeV)
    else:
        electrons_for_Z = elec_filter(min_pt=min_pt)
        zcand = make_dielec_novxt(
            input_elecs=electrons_for_Z,
            decaydescriptor="Z0 -> e+ e-",
            min_mass=40. * GeV)

    return zcand


# For HNL lines


@configurable
def filter_neutral_hadrons(particles,
                           pvs,
                           pt_min=0.5 * GeV,
                           ipchi2_min=0.0,
                           name='qee_rad_incl_neutral_hadrons_{hash}_{hash}'):
    """Returns extra neutral hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


def hnl_prefilter(require_GEC=False):
    from RecoConf.event_filters import require_pvs, require_gec
    from RecoConf.reconstruction_objects import (make_pvs,
                                                 upfront_reconstruction)
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


@configurable
def make_majorana_lepton(leptons,
                         pvs,
                         name="standard_lepton_for_majorana",
                         pt_min=0.5 * GeV,
                         p_min=0 * GeV,
                         mipchi2dvprimary_min=25.0,
                         pid=None):
    """
    Filter default leptons for HNL
    """
    return make_filter_tracks(
        make_particles=leptons,
        make_pvs=pvs,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_majorana(child1,
                  child2,
                  name='Generic_Majorana',
                  descriptor='',
                  am_min=0.2 * GeV,
                  am_max=7. * GeV,
                  adocachi2=16.,
                  pt_min=0.7 * GeV,
                  vtxchi2_max=9.):
    """
    Make HNL -> lep +  pi. 
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2))
    majorana_code = F.require_all(F.PT > pt_min, F.CHI2 < vtxchi2_max)

    return ParticleCombiner([child1, child2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=majorana_code)


@configurable
def make_bhadron_majorana(majoranas,
                          bachelor,
                          pvs,
                          name='Generic_B_2_Majorana',
                          descriptor='',
                          am_min=4.3 * GeV,
                          am_max=7.2 * GeV,
                          m_min=4.5 * GeV,
                          m_max=6.8 * GeV,
                          adocachi2=25.,
                          vtxchi2_max=9.,
                          mipchi2_max=16.,
                          bpvdls_min=4.,
                          dira_min=0.):
    """
    Make B-> lep + HNL
    """
    #majoranas = make_majorana()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXDOCACHI2CUT(adocachi2))
    b_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.BPVDLS(pvs()) > bpvdls_min, F.CHI2 < vtxchi2_max,
        F.BPVDIRA(pvs()) > dira_min,
        F.MINIPCHI2(pvs()) < mipchi2_max)
    return ParticleCombiner([majoranas, bachelor],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=b_code)

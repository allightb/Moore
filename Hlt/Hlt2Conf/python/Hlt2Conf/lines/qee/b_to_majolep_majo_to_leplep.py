###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This defines HLT2 lines for B+(-)/B_c+(-) -> (X) mu N, N-> l l' nu where N is a neutral lepton
N has no lifetime hypothesis applied. Combination of the neutral lepton daugthers is limited between 200(to cut conversion)-7000 MeV
All leptons from B are Long. Daughters of the neutral lepton can be both Long and Downstream.
Loose and Tight lines have a different PID cut looseness on pions and electrons. Note, PID_E is applied to pions too, so there is a need for a no_brem added calibration.
Tight lines will hopefully have raw event information saved, specifically tracking infor.

List of lines (LL - Long, DD - downstream):

Hlt2QEE_BpToMajoMu_MajoToMuMu_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu mu, LL)
Hlt2QEE_BpToMajoMu_MajoToMuMu_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu mu, DD)
Hlt2QEE_BpToMajoMu_MajoToMuE_SS_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu+ e, LL)
Hlt2QEE_BpToMajoMu_MajoToMuE_SS_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu+ e, DD)
Hlt2QEE_BpToMajoMu_MajoToMuE_OS_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu- e, LL)
Hlt2QEE_BpToMajoMu_MajoToMuE_OS_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu- e, DD)

Hlt2QEE_BpToMajoE_MajoToMuMu_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu mu, LL)
Hlt2QEE_BpToMajoE_MajoToMuMu_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu mu, DD)
Hlt2QEE_BpToMajoE_MajoToMuE_SS_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu+ e, LL)
Hlt2QEE_BpToMajoE_MajoToMuE_SS_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu+ e, DD)
Hlt2QEE_BpToMajoE_MajoToMuE_OS_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu- e, LL)
Hlt2QEE_BpToMajoE_MajoToMuE_OS_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu- e, DD)

Contact: Louis Henry, louis.henry@cern.ch

"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.standard_particles import (
    make_long_muons, make_long_electrons_with_brem, make_down_muons,
    make_down_electrons_no_brem)

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.qee.qee_builders import make_majorana_lepton, make_bhadron_majorana, hnl_prefilter, make_majorana
import Functors as F

all_lines = {}
_HNL_MONITORING_VARIABLES = ("pt", "eta", "vchi2", "ipchi2", "n_candidates")

_defaultCutDict = {
    "Bachelor_MIN_PT": 700 * MeV,
    "Child1_MIN_PT": 700 * MeV,
    "Child2_MIN_PT": 700 * MeV,
}


def builder_BToMajoL_line(name,
                          cutDict=_defaultCutDict,
                          prescale=1,
                          persistreco=True):
    pvs = make_pvs
    # Interpret name into a B decay
    bachelorName = "mu" if name.find("MajoMu") != -1 else "e"
    bachelorContainer = {
        "mu": make_long_muons,
        "e": make_long_electrons_with_brem
    }[bachelorName]
    bachelorAddCuts = {"mu": {"pid": F.ISMUON}, "e": {}}[bachelorName]
    bachelorContName = {
        "mu": "majo_long_muons_{hash}",
        "e": "majo_long_electrons_with_brem_{hash}"
    }[bachelorName]
    #Interpret name into a majorana constructor

    recoMode = "LL" if name.find("LL") != -1 else "DD"
    recoName1 = {"L": "long", "D": "down"}[recoMode[0]]
    recoName2 = {"L": "long", "D": "down"}[recoMode[1]]
    children = ("mu", "mu") if name.find("ToMuMu") != -1 else (
        "e", "e") if name.find("ToEE") != -1 else (
            "mu", "e") if name.find("SS") != -1 else ("e", "mu")
    descr = "[KS0 -> " + children[0] + "+ " + children[1] + "-]cc"
    constructors = {
        "LL": {
            "mu": make_long_muons,
            "e": make_long_electrons_with_brem
        },
        "DD": {
            "mu": make_down_muons,
            "e": make_down_electrons_no_brem
        }
    }
    childrenNames = {
        "mu_long": "long_muons",
        "mu_down": "down_muons",
        "e_long": "long_electrons_with_brem",
        "e_down": "down_electrons_without_brem",
    }
    childName1 = childrenNames[children[0] + "_" + recoName1]
    childName2 = childrenNames[children[1] + "_" + recoName2]
    addCut1 = {}
    addCut2 = {}
    if recoMode == "DD":
        addCut1["mipchi2dvprimary_min"] = None
        addCut2["mipchi2dvprimary_min"] = None

    if children[0] == "muons":
        addCut1["pid"] = F.ISMUON
    if children[1] == "muons":
        addCut2["pid"] = F.ISMUON
    firstChild = make_majorana_lepton(
        leptons=constructors[recoMode][children[0]],
        name="majo_" + childName1 + "_{hash}",
        pvs=pvs,
        pt_min=cutDict["Child1_MIN_PT"],
        **(addCut1))
    secondChild = make_majorana_lepton(
        leptons=constructors[recoMode][children[1]],
        name="majo_" + childName2 + "_{hash}",
        pvs=pvs,
        pt_min=cutDict["Child2_MIN_PT"],
        **(addCut2))
    majo = make_majorana(
        name="Majo2" + children[0] + children[1] + "_" + recoMode + "_{hash}",
        child1=firstChild,
        child2=secondChild,
        descriptor=descr)
    #Bachelor
    bachelor = make_majorana_lepton(
        leptons=bachelorContainer,
        name=bachelorContName,
        pvs=pvs,
        pt_min=cutDict["Bachelor_MIN_PT"],
        **bachelorAddCuts)
    #Decay
    b2LN = make_bhadron_majorana(
        name=name + '_{hash}',
        majoranas=majo,
        bachelor=bachelor,
        pvs=pvs,
        descriptor='[B+ -> KS0 ' + bachelorName + '+]cc')

    return Hlt2Line(
        name=name,
        algs=hnl_prefilter() + [majo, b2LN],
        prescale=prescale,
        monitoring_variables=_HNL_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToMuMu_LL_Tight_line(
        name="Hlt2QEE_BpToMajoMu_MajoToMuMu_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToMuE_LL_Tight_line(
        name="Hlt2QEE_BpToMajoMu_MajoToMuE_SS_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEMu_LL_Tight_line(
        name="Hlt2QEE_BpToMajoMu_MajoToEMu_OS_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoMu_MajoToEE_LL_Tight_line(
        name="Hlt2QEE_BpToMajoMu_MajoToEE_LL_Tight",
        cutDict={
            "Bachelor_MIN_PT": 1000 * MeV,
            "Child1_MIN_PT": 1000 * MeV,
            "Child2_MIN_PT": 1000 * MeV,
        }):
    return builder_BToMajoL_line(name, cutDict)


# B-> eN
@register_line_builder(all_lines)
def BpToMajoE_MajoToMuMu_LL_Tight_line(
        name="Hlt2QEE_BpToMajoE_MajoToMuMu_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoE_MajoToMuE_LL_Tight_line(
        name="Hlt2QEE_BpToMajoE_MajoToMuE_SS_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoE_MajoToEMu_LL_Tight_line(
        name="Hlt2QEE_BpToMajoE_MajoToEMu_OS_LL_Tight"):
    return builder_BToMajoL_line(name)


@register_line_builder(all_lines)
def BpToMajoE_MajoToEE_LL_Tight_line(
        name="Hlt2QEE_BpToMajoE_MajoToEE_LL_Tight",
        cutDict={
            "Bachelor_MIN_PT": 1000 * MeV,
            "Child1_MIN_PT": 1000 * MeV,
            "Child2_MIN_PT": 1000 * MeV,
        }):
    return builder_BToMajoL_line(name, cutDict)

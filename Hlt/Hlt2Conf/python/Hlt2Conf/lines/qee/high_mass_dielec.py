###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Z->ee and same-sign dielectron (control sample) line.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import upfront_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.standard_particles import make_long_electrons_with_brem

all_lines = {}


@configurable
def elec_filter_for_Z(particles, min_pt=0. * GeV):
    """ An electron filter: PT
    """

    return ParticleFilter(particles, F.FILTER(F.PT > min_pt))


@configurable
def make_Zee_cand():
    elecs = elec_filter_for_Z(
        make_long_electrons_with_brem(), min_pt=15. * GeV)
    line_alg = make_dielec_novxt(elecs, "Z0 -> e+ e-", 40. * GeV)
    return line_alg


@configurable
def make_Zeess_cand():
    elecs = elec_filter_for_Z(
        make_long_electrons_with_brem(), min_pt=15. * GeV)
    line_alg = make_dielec_novxt(elecs, "[Z0 -> e- e-]cc", 40. * GeV)
    return line_alg


@configurable
def make_dielec_novxt(input_elecs, decaydescriptor, min_mass=40. * GeV):
    """DiElec without any requirements but mass
    """

    # mass region of dielecs
    combination_code = (F.MASS > min_mass)

    return ParticleCombiner([input_elecs, input_elecs],
                            DecayDescriptor=decaydescriptor,
                            CombinationCut=combination_code,
                            CompositeCut=F.ALL)


@register_line_builder(all_lines)
@configurable
def z_to_e_e_line(name='Hlt2QEE_ZToEEFull', prescale=1, persistreco=True):
    """Z0 boson decay to two elecs line"""

    z02ee = make_Zee_cand()

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02ee],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def same_sign_dielec_line(name='Hlt2QEE_DiElectronSameSignFull',
                          prescale=1,
                          persistreco=True):
    """Z0 boson decay to two same-sign elecs line"""

    eess = make_Zeess_cand()

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [eess],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("m", "pt", "eta", "n_candidates"),
    )

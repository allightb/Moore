###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of an HLT2 line to preserve dielectron candidates and nearby photons
in order to reconstruct, later, {pi0, eta} -> e+ e- gamma.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV

from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg, SelectionFromWeightedRelationTable

from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter
from Hlt2Conf.standard_particles import make_long_electrons_no_brem, make_prompt_dielectron_with_brem, make_detached_dielectron_with_brem, make_photons, make_has_rich_long_pions

full_lines = {}
turbo_lines = {}


@configurable
def photon_filter(name='inputphotons_{hash}',
                  make_particles=make_photons,
                  et_min=1000 * MeV,
                  e_min=0 * MeV,
                  is_photon_min=0.0,
                  is_not_h_min=0.2,
                  shower_shape_min=0):

    code = F.require_all(F.PT > et_min, F.P > e_min,
                         F.IS_PHOTON > is_photon_min,
                         F.IS_NOT_H > is_not_h_min,
                         F.CALO_NEUTRAL_SHOWER_SHAPE > shower_shape_min)

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def charged_pion_filter(pi_minpt):
    code = F.require_all(F.PT > pi_minpt)
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(code))


@configurable
def dielectron_maker_prompt(e_minpt, isOS):
    return make_prompt_dielectron_with_brem(
        electron_maker=make_long_electrons_no_brem,
        opposite_sign=isOS,
        PIDe_min=0.,
        pt_e=e_minpt,
        maxipchi2=2.,
        trghostprob=0.25,
        dielectron_ID="J/psi(1S)",
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=None,
        vfaspfchi2ndof=10)


@configurable
def dielectron_maker_displaced(e_minpt, isOS):
    return make_detached_dielectron_with_brem(
        electron_maker=make_long_electrons_no_brem,
        opposite_sign=isOS,
        PIDe_min=0.,
        pt_e=e_minpt,
        minipchi2=2.,
        trghostprob=0.25,
        dielectron_ID="J/psi(1S)",
        pt_diE=0 * MeV,
        m_diE_min=5 * MeV,
        m_diE_max=300 * MeV,
        adocachi2cut=30,
        bpvvdchi2=0,
        vfaspfchi2ndof=10)


# OS Prompt lines (one to turbo, prescaled to full)
@register_line_builder(turbo_lines)
@configurable
def dielectron_sp_prompt_line(name='Hlt2QEE_DiElectronPrompt_PersistPhotons',
                              prescale=1,
                              persistreco=False,
                              e_minpt=0.,
                              make_pvs=make_pvs):
    """Aiming for prompt pi0/eta -> gamma e+ e- decays. Label the dielectron 
    as J/psi(1S) [during brem-recovery-reconstruction of dielectron pair] and 
    save photons if Pi0/eta in mass range of [5, 600]MeV."""
    pvs = make_pvs()

    dielectrons_prompt = dielectron_maker_prompt(e_minpt, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        persistreco=persistreco,
        extra_outputs=[('DiElectronPrompt_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations))],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_massSlice.*_promptDecision"
        ])


@register_line_builder(full_lines)
@configurable
def dielectron_sp_prompt_line_full(
        name='Hlt2QEE_DiElectronPrompt_PersistPhotons_Full',
        prescale=0.05,
        persistreco=True,
        e_minpt=0.,
        make_pvs=make_pvs):
    """Copy of turbo line, but strongly prescaled, which allows full-event 
    studies e.g. of brem correction and background template for e+e- pairs 
    combined with a random photon (4-vector deduced from e+e- in same event 
    and charged pion momentum)"""
    pvs = make_pvs()

    dielectrons_prompt = dielectron_maker_prompt(e_minpt, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    extra_pions = charged_pion_filter(pi_minpt=75. * MeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        persistreco=persistreco,
        extra_outputs=[('DiElectronPrompt_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations)),
                       ('ChargedPions', extra_pions)],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_massSlice.*_promptDecision"
        ])


# OS displaced lines (one to turbo, prescaled to full)
@register_line_builder(turbo_lines)
@configurable
def dielectron_sp_displaced_line(
        name='Hlt2QEE_DiElectronDisplaced_PersistPhotons',
        prescale=1,
        persistreco=False,
        e_minpt=0.,
        make_pvs=make_pvs):
    """Aiming for displaced pi0/eta -> gamma e+ e- decays. Label the 
    dielectron as J/psi(1S) [during brem-recovery-reconstruction of dielectron 
    pair] and save photons if Pi0/eta in mass range of [5, 600]MeV."""
    pvs = make_pvs()

    dielectrons_displaced = dielectron_maker_displaced(e_minpt, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), dielectrons_displaced],
        persistreco=persistreco,
        extra_outputs=[('DiElectronDisplaced_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations))],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_massSlice.*_displacedDecision"
        ])


@register_line_builder(full_lines)
@configurable
def dielectron_sp_displaced_line_full(
        name='Hlt2QEE_DiElectronDisplaced_PersistPhotons_Full',
        prescale=0.05,
        persistreco=True,
        e_minpt=0.,
        make_pvs=make_pvs):
    """Copy of turbo line, but strongly prescaled, which allows full-event 
    studies e.g. of brem correction and background template for e+e- pairs 
    combined with a random photon (4-vector deduced from e+e- in same event 
    and charged pion momentum)"""
    pvs = make_pvs()

    dielectrons_displaced = dielectron_maker_displaced(e_minpt, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    extra_pions = charged_pion_filter(pi_minpt=75. * MeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), dielectrons_displaced],
        persistreco=persistreco,
        extra_outputs=[('DiElectronDisplaced_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations)),
                       ('ChargedPions', extra_pions)],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_massSlice.*_displacedDecision"
        ])


@register_line_builder(turbo_lines)
@configurable
def dielectron_sp_prompt_same_sign_line(
        name='Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
        prescale=0.1,
        persistreco=False,
        e_minpt=0.,
        make_pvs=make_pvs):
    """Aiming for prompt pi0/eta -> gamma e+ e+ or gamma e- e- decays. Label 
    the doubly-charged dielectron as J/psi(1S) and save photons if Pi0/eta in 
    mass range of [5, 600]MeV."""
    pvs = make_pvs()

    dielectrons_prompt = dielectron_maker_prompt(e_minpt, isOS=False)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        persistreco=persistreco,
        extra_outputs=[('DiElectronPrompt_ss_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations))],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_SS_massSlice.*_promptDecision"
        ])


@register_line_builder(turbo_lines)
@configurable
def dielectron_sp_displaced_same_sign_line(
        name='Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS',
        prescale=0.1,
        persistreco=False,
        e_minpt=0.,
        make_pvs=make_pvs):
    """Aiming for displaced pi0/eta -> gamma e+ e+ or gamma e- e- decays. 
    Label the doubly-charged dielectron as J/psi(1S) and save photons if 
    Pi0/eta in mass range of [5, 600]MeV."""
    pvs = make_pvs()

    dielectrons_displaced = dielectron_maker_displaced(e_minpt, isOS=False)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5. * MeV, F.COMB_MASS(), 600. * MeV))

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() +
        [require_pvs(pvs), dielectrons_displaced],
        persistreco=persistreco,
        extra_outputs=[('DiElectronDisplaced_ss_Photons',
                        SelectionFromWeightedRelationTable(
                            InputRelations=photons_table.OutputRelations))],
        prescale=prescale,
        hlt1_filter_code=[
            "Hlt1LowMass(NoipDie|DiE)lectron_SS_massSlice.*_displacedDecision"
        ])

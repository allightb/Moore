###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Common prefilters used by all lines
"""
from RecoConf.event_filters import require_pvs, require_gec

from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from Hlt2Conf.lines.topological_b import require_topo_candidate


def bnoc_prefilters(require_GEC=False,
                    require_topo=False,
                    min_twobody_mva=0.1,
                    min_threebody_mva=0.1):

    filters = list(upfront_reconstruction())  # create a copy
    if require_GEC:
        filters.append(require_gec())
    filters.append(require_pvs(make_pvs()))
    if require_topo:
        filters.append(
            require_topo_candidate(
                min_twobody_mva=min_twobody_mva,
                min_threebody_mva=min_threebody_mva))
    return filters

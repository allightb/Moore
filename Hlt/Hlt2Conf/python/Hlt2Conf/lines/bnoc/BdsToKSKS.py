###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to select the decay B(s)0 -> KS0 KS0 and KS0 -> pi+ pi-.
The KS0 can be reconstructed either from long or downstream tracks, resulting
in the combinations LLLL, LLDD, DDDD.

"""

from GaudiKernel.SystemOfUnits import MeV, mm
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_KS_LL, make_KS_DD
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2ksks
from PyConf import configurable


@configurable
def make_BdsToKSKS_LLLL(process):
    ll_kshorts = make_KS_LL()
    line_alg = make_b2ksks(
        particles=[ll_kshorts, ll_kshorts],
        descriptor="B_s0 -> KS0 KS0",
        am_min=4400 * MeV,
        am_max=6100 * MeV,
        adoca_max=1 * mm,
        vtx_am_min=4600 * MeV,
        vtx_am_max=5900 * MeV,
        vchi2pdof_max=20)
    return [ll_kshorts, line_alg]


@configurable
def make_BdsToKSKS_LLDD(process):
    ll_kshorts = make_KS_LL()
    dd_kshorts = make_KS_DD()
    line_alg = make_b2ksks(
        particles=[dd_kshorts, ll_kshorts],
        descriptor="B_s0 -> KS0 KS0",
        vchi2pdof_max=30,
        AllowDiffInputsForSameIDChildren=True)
    return [ll_kshorts, dd_kshorts, line_alg]


@configurable
def make_BdsToKSKS_DDDD(process):
    dd_kshorts = make_KS_DD()
    line_alg = make_b2ksks(
        particles=[dd_kshorts, dd_kshorts], descriptor="B_s0 -> KS0 KS0")
    return [dd_kshorts, line_alg]

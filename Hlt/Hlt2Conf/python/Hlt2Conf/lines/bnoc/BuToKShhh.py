###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B+ -> KShhh inclusive HLT2 lines, with LL and DD Ks
"""

from PyConf import configurable
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_soft_pions, make_soft_kaons
from Hlt2Conf.lines.b_to_open_charm.builders.basic_builder import make_ks_LL, make_ks_DD
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2x
from GaudiKernel.SystemOfUnits import MeV, picosecond

#aprox masses of particles: lower than true value because of detector resolution
m_K = 480. * MeV
m_pi = 120. * MeV

Bu_kwargs = {
    "MassWindow": True,
    "dira_min": 0.99995,
    "ltime_min": 0.1 * picosecond,
    "charged": True
}  # Particle ordering matters when chraged is enabled! (Ordering by particle's charge)


@check_process
@configurable
def make_BuToKSPipPipPim_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions(pt_min=300 * MeV)
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, pions, pions, pions],
        descriptor='[B+ -> KS0 pi+ pi+ pi-]cc',
        **Bu_kwargs)
    return Bu


@check_process
@configurable
def make_BuToKSPipPipPim_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, pions, pions, pions],
        descriptor='[B+ -> KS0 pi+ pi+ pi-]cc',
        **Bu_kwargs)
    return Bu


@check_process
@configurable
def make_BuToKSKpPipPim_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions(pt_min=300 * MeV)
        kaons = make_soft_kaons(pt_min=300 * MeV)
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, kaons, pions, pions],
        descriptor='[B+ -> KS0 K+ pi+ pi-]cc',
        **Bu_kwargs,
        am123_min=2 * m_pi + m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpPipPim_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, kaons, pions, pions],
        descriptor='[B+ -> KS0 K+ pi+ pi-]cc',
        **Bu_kwargs,
        am123_min=2 * m_pi + m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKmPipPip_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, pions, pions, kaons],  # Suboptimal ordering!
        descriptor='[B+ -> KS0 pi+ pi+ K-]cc',
        **Bu_kwargs,
        am123_min=2 * m_pi + m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKmPipPip_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, pions, pions, kaons],  # Suboptimal ordering!
        descriptor='[B+ -> KS0 pi+ pi+ K-]cc',
        **Bu_kwargs,
        am123_min=2 * m_pi + m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKmPip_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, kaons, pions, kaons],  # Suboptimal ordering!
        descriptor='[B+ -> KS0 K+ pi+ K-]cc',
        **Bu_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKmPip_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, kaons, pions, kaons],  # Suboptimal ordering!
        descriptor='[B+ -> KS0 K+ pi+ K-]cc',
        **Bu_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKpPim_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, kaons, kaons, pions],
        descriptor='[B+ -> KS0 K+ K+ pi-]cc',
        **Bu_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKpPim_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
        kaons = make_soft_kaons()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, kaons, kaons, pions],
        descriptor='[B+ -> KS0 K+ K+ pi-]cc',
        **Bu_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKpKm_LL(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    KsLL = make_ks_LL()
    Bu = make_b2x(
        particles=[KsLL, kaons, kaons, kaons],
        descriptor='[B+ -> KS0 K+ K+ K-]cc',
        **Bu_kwargs,
        am12_min=2 * m_K,
        am123_min=3 * m_K)
    return Bu


@check_process
@configurable
def make_BuToKSKpKpKm_DD(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    KsDD = make_ks_DD()
    Bu = make_b2x(
        particles=[KsDD, kaons, kaons, kaons],
        descriptor='[B+ -> KS0 K+ K+ K-]cc',
        **Bu_kwargs,
        am12_min=2 * m_K,
        am123_min=3 * m_K)
    return Bu

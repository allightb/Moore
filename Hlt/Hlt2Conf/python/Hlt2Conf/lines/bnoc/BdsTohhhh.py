###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> hhhh inclusive HLT2 lines.
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_pions, make_tight_kaons
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2x

#aprox masses of particles: lower than true value because of detector resolution
m_K = 480. * MeV
m_pi = 120. * MeV

Bds_kwargs = {
    "am_min": 4900 * MeV,
    "am_max": 5900 * MeV,
    "am123_max": 2015.0 * MeV,
    "am12_max": 1915.0 * MeV,
    "MassWindow": True,
    "mothermass_min": 5000 * MeV,
    "mothermass_max": 5800 * MeV,
    "asumpt_min": 2000 * MeV,
    "motherpt_min": 500 * MeV,
    "daughter_mipchi2_min": 6,
    "vtxchi2pdof_max": 16,
    "adoca_max": .5 * mm,
    "dira_min": 0.999,
    #"ltime_min": 0.1 * picosecond
}


@configurable
def make_BdsToPipPimPipPim(process):
    pions = make_pions(pt_min=500 * MeV, p_min=1.5 * GeV, pi_pidk_max=0)
    builder_kwargs = {k: v for k, v in Bds_kwargs.items()}
    builder_kwargs['vtxchi2pdof_max'] = 14
    builder_kwargs['adoca_max'] = .3 * mm
    builder_kwargs['motherpt_min'] = 5000 * MeV
    line_alg = make_b2x(
        particles=[pions, pions, pions, pions],
        descriptor='B0 -> pi+ pi+ pi- pi-',
        **builder_kwargs,
        am12_min=2 * m_pi,
        am123_min=3 * m_pi)
    return line_alg


@configurable
def make_BdsToKpPimPipPim(process):
    pions = make_pions(pt_min=500 * MeV, p_min=1.5 * GeV, pi_pidk_max=0)
    kaons = make_tight_kaons(k_pidk_min=0, p_min=2 * GeV, pt_min=500 * MeV)
    builder_kwargs = {k: v for k, v in Bds_kwargs.items()}
    builder_kwargs['vtxchi2pdof_max'] = 14
    builder_kwargs['adoca_max'] = .3 * mm
    builder_kwargs['motherpt_min'] = 5000 * MeV
    line_alg = make_b2x(
        particles=[kaons, pions, pions, pions],
        descriptor='[B0 -> K+ pi+ pi- pi-]cc',
        **builder_kwargs,
        am12_min=2 * m_pi,
        am123_min=3 * m_pi)
    return line_alg


@configurable
def make_BdsToKpKmPipPim(process):
    pions = make_pions(pt_min=500 * MeV, p_min=1.5 * GeV, pi_pidk_max=0)
    kaons = make_tight_kaons(k_pidk_min=0, p_min=2 * GeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[kaons, kaons, pions, pions],
        descriptor='B0 -> K+ K- pi+ pi-',
        **Bds_kwargs,
        am12_min=2 * m_pi,
        am123_min=2 * m_pi + m_K)
    return line_alg


@configurable
def make_BdsToKpKmKpPim(process):
    pions = make_pions(pt_min=500 * MeV, p_min=1.5 * GeV, pi_pidk_max=0)
    kaons = make_tight_kaons(k_pidk_min=0, p_min=1.5 * GeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[kaons, kaons, kaons, pions],
        descriptor='[B0 -> K+ K+ K- pi-]cc',
        **Bds_kwargs,
        am12_min=m_pi + m_K,
        am123_min=m_pi + 2 * m_K)
    return line_alg


@configurable
def make_BdsToKpKmKpKm(process):
    kaons = make_tight_kaons(k_pidk_min=-2, p_min=2 * GeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[kaons, kaons, kaons, kaons],
        descriptor='B0 -> K+ K+ K- K-',
        **Bds_kwargs,
        am12_min=2 * m_K,
        am123_min=3 * m_K)
    return line_alg

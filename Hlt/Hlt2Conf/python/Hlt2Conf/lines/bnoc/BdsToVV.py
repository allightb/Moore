###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B0(s) -> VV, V -> hh HLT2 lines.
* V: phi, kstar, rho
"""

from GaudiKernel.SystemOfUnits import MeV, mm, GeV, picosecond

from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_KS_LL, make_wide_kstar0, make_phi, make_phi_fake, make_rho0, make_rho0_fake, make_omega0, make_omega0_fake, make_tight_kaons, make_tight_pions, make_soft_kaons, make_merged_pi0s, make_soft_pions
from Hlt2Conf.lines.bnoc.builders.b_builder import make_btovv, make_b2Kpi, make_BdsToVKpKm
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
@configurable
def make_BdsToPhiPhi(process):
    phi = make_phi()
    line_alg = make_btovv(
        particles=[phi, phi],
        descriptor="B_s0 -> phi(1020) phi(1020)",
        asumpt_min=None,
        ptproduct_min=1.5 * GeV * GeV,
        adoca12_max=None,
        ltime_min=0.2 * picosecond,
        bpvfdchi2_min=None,
        bpvdira_min=None)
    return [phi, line_alg]


@check_process
@configurable
def make_BdsToKstzKstzb(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        vchi2pdof_max=13,
        adoca12_max=0.15 * mm,
        motherpt_min=900 * MeV,
        mipchi2_min=20.)
    line_alg = make_btovv(
        particles=[kstar, kstar],
        descriptor='B_s0 -> K*(892)0 K*(892)~0',
        asumpt_min=2500. * MeV,
        adoca12_max=0.08 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=81)
    return [kstar, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV,
        invert_pi_pid=True)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV,
        invert_k_pid=True)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV,
        invert_pi_pid=True,
        invert_k_pid=True)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakePhiK(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    phi = make_phi_fake(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakePhiDouble(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    phi = make_phi(
        make_kaons=make_tight_kaons,
        k_pidk_min=-5.,
        k_p_min=1.5 * GeV,
        k_pt_min=500 * MeV,
        asumpt_min=500 * MeV,
        vchi2pdof_max=9,
        invert_pid=True)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor='[B0 -> phi(1020) K*(892)0]cc',
        asumpt_min=1500 * MeV,
        adoca12_max=0.3 * mm,
        vchi2pdof_max=15,
        mipchi2_max=25)
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzRho(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV,
        invert_pi_pid=True)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV,
        invert_k_pid=True)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV,
        invert_pi_pid=True,
        invert_k_pid=True)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeRhoPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV)
    rho = make_rho0_fake(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeRhoDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=2.,
        pi_p_min=1.5 * GeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1300 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=12,
        asumpt_min=1250 * MeV,
        motherpt_min=1250 * MeV,
        mipchi2_min=32.,
        invert_pid=True)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor='[B0 -> K*(892)~0 rho(770)0]cc',
        asumpt_min=5000. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=2.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        am_min=590 * MeV,
        am_max=1200 * MeV)
    omega = make_omega0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_pi_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_k_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_pi_pid=True,
        invert_k_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeOmegaPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    omega = make_omega0_fake(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeOmegaDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    omega = make_omega0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.,
        invert_pid=True)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_pi_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_k_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.,
        invert_pi_pid=True,
        invert_k_pid=True)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeOmegaPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    omega = make_omega0_fake(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeOmegaDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        pi_pidk=3.,
        pi_pt_min=500 * MeV,
        k_pidk_min=-3.,
        k_pt_min=500 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=9,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        pi_pidk_max=0.,
        pi_p_min=1. * GeV,
        pi_pt_min=250. * MeV,
        am_min=700 * MeV,
        am_max=860 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.,
        invert_pid=True)
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor='[B0 -> K*(892)~0 omega(782)]cc',
        asumpt_min=2500. * MeV,
        adoca12_max=0.15 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiRho(process):
    phi = make_phi(
        make_kaons=make_soft_kaons,
        k_pidk_min=-5.,
        adoca12_max=0.12 * mm,
        asumpt_min=1100 * MeV,
        bpvfdchi2_min=11,
        vchi2pdof_max=9,
        mipchi2_min=32.)
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=5.,
        adoca12_max=0.12 * mm,
        bpvfdchi2_min=20,
        vchi2pdof_max=15,
        mipchi2_min=32.)
    line_alg = make_btovv(
        particles=[phi, rho],
        descriptor='B_s0 -> phi(1020) rho(770)0',
        asumpt_min=2000. * MeV,
        ptproduct_min=2.5 * GeV * GeV,
        adoca12_max=0.09 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=90)
    return [phi, rho, line_alg]


@check_process
@configurable
def make_BdsToRhoRho(process):
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.,
        pi_p_min=800 * MeV,
        pi_pt_min=250. * MeV,
        am_min=280 * MeV,
        am_max=1600 * MeV,
        adoca12_max=0.3 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asump_min=800 * MeV,
        asumpt_min=500 * MeV,
        motherpt_min=500 * MeV,
        mipchi2_min=36.)
    line_alg = make_btovv(
        particles=[rho, rho],
        descriptor='B0 -> rho(770)0 rho(770)0',
        asumpt_min=2500. * MeV,
        adoca12_max=0.2 * mm,
        vchi2pdof_max=12,
        mipchi2_max=25,
        bpvfdchi2_min=36)
    return [rho, line_alg]


@check_process
@configurable
def make_BdsToKSPi0_LL(process):
    ks = make_KS_LL(
        pi_p_min=0 * GeV,
        p_min=8000.0 * MeV,
        pt_min=500.0 * MeV,
        vchi2_max=10.0,
        bpvfdchi2_min=25,
        mipchi2_min=10.0)
    pions = make_merged_pi0s(p_min=5000 * MeV, pt_min=3500 * MeV)

    line_alg = make_b2Kpi(
        particles=[ks, pions],
        descriptor='B0 -> KS0 pi0',
        comb_m_min=4000 * MeV,
        comb_m_max=6200 * MeV,
        comb_pt_min=5000 * MeV,
        mtdocachi2_max=10.0,
        pt_min=4000 * MeV)
    return [ks, line_alg]


@check_process
@configurable
def make_BdsToPhiKpKm(process):
    phi = make_phi(am_min=995 * MeV, am_max=1045 * MeV, k_pt_min=500 * MeV)
    kaons = make_soft_kaons(k_pidk_min=0., pt_min=500 * MeV)
    line_alg = make_BdsToVKpKm(
        particles=[phi, kaons, kaons], descriptor="B_s0 -> phi(1020) K+ K-")
    return [phi, line_alg]


@check_process
@configurable
def make_BdsToKstzKpKm(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        k_pidk_min=0.,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV)
    kaons = make_soft_kaons(k_pidk_min=0., pt_min=500 * MeV)
    line_alg = make_BdsToVKpKm(
        particles=[kstar, kaons, kaons], descriptor='[B0 -> K*(892)0 K+ K-]cc')
    return [kstar, line_alg]

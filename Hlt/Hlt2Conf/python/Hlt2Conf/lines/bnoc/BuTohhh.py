###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BuTohhh and BcTohhh lines
"""
from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import b_builder

##############################################
# BuTohhh lines
##############################################


@check_process
def make_BuToKpKpKm_NoPID(process):
    if process == 'spruce':
        kaons = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_tight_kaons(k_pidk_min=None, mipchi2_min=9)
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, kaons, kaons], descriptor='[B+ -> K+ K+ K-]cc')
    return line_alg


##############################################
# BcTohhh lines
##############################################


@check_process
def make_BcToKpKpKm_NoPID(process):
    if process == 'spruce':
        kaons = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = basic_builder.make_tight_kaons(k_pidk_min=None, mipchi2_min=9)
    line_alg = b_builder.make_b2hhh(
        particles=[kaons, kaons, kaons],
        descriptor='[B_c+ -> K+ K+ K-]cc',
        am_min=5998 * MeV,
        am_max=6502 * MeV,
        am_min_vtx=5998 * MeV,
        am_max_vtx=6502 * MeV,
        vtx_chi2pdof_max=40,
    )
    return line_alg

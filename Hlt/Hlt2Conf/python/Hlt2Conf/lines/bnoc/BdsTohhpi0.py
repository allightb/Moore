###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> hhpi0 HLT2 lines.
"""

from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_tight_pions, make_resolved_pi0s, make_merged_pi0s
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2hhh


@configurable
def make_BdsToPipPimPi0_merged(process):
    pions_charged = make_tight_pions(mipchi2_min=15)
    pi0_merged = make_merged_pi0s()
    line_alg = make_b2hhh(
        particles=[pions_charged, pions_charged, pi0_merged],
        descriptor='B0 -> pi+ pi- pi0')
    return line_alg


@configurable
def make_BdsToPipPimPi0_resolved(process):
    pions_charged = make_tight_pions(mipchi2_min=15)
    pi0_resolved = make_resolved_pi0s()
    line_alg = make_b2hhh(
        particles=[pions_charged, pions_charged, pi0_resolved],
        descriptor='B0 -> pi+ pi- pi0')
    return line_alg

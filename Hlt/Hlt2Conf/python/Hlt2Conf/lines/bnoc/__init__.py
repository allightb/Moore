# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the BnoC selection lines
"""
from . import hlt2_bnoc
from . import spruce_bnoc

# provide "all_lines" for correct registration by the overall HLT2 lines module
hlt2_lines = {}
hlt2_lines.update(hlt2_bnoc.hlt2_lines)

full_lines = {}
full_lines.update(hlt2_bnoc.full_lines)

all_lines = {}
all_lines.update(hlt2_lines)
all_lines.update(full_lines)

sprucing_lines = {}
sprucing_lines.update(spruce_bnoc.sprucing_lines)

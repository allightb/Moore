###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC bTohh lines
"""

from GaudiKernel.SystemOfUnits import MeV  # will be needed once the full set of lines is back

from Hlt2Conf.lines.bnoc.utils import check_process

from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.builders import b_builder

##############################################
# Definition of BNOC bTohh lines:            #
#   B+ -> K+ pi0                             #
#   B+ -> Lambda~0 p+(LL,DD)                 #
#   Bc+ -> Lambda~0 p+(LL,DD)                #
##############################################


@check_process
def make_BuToKpPi0(process):
    kaons = basic_builder.make_soft_kaons(
        k_pidk_min=-0.5, p_min=12000 * MeV, pt_min=1200 * MeV, mipchi2_min=50)
    pions = basic_builder.make_merged_pi0s(p_min=5000 * MeV, pt_min=3500 * MeV)

    line_alg = b_builder.make_b2Kpi(
        particles=[kaons, pions],
        descriptor='[B+ -> K+ pi0]cc',
        comb_m_min=4000 * MeV,
        comb_m_max=6200 * MeV,
        comb_pt_min=5000 * MeV,
        mtdocachi2_max=8.0,
        pt_min=5000 * MeV)
    return line_alg


@check_process
def make_BuToPipPi0(process):
    pips = basic_builder.make_soft_pions(
        p_min=12000 * MeV, pt_min=1200 * MeV, mipchi2_min=50)
    pi0s = basic_builder.make_merged_pi0s(p_min=5000 * MeV, pt_min=3500 * MeV)

    line_alg = b_builder.make_b2Kpi(
        particles=[pips, pi0s],
        descriptor='[B+ -> pi+ pi0]cc',
        comb_m_min=4000 * MeV,
        comb_m_max=6200 * MeV,
        comb_pt_min=5000 * MeV,
        mtdocachi2_max=8.0,
        pt_min=5000 * MeV)
    return line_alg


@check_process
def make_BuToL0barPp_LL(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(p_pidp_min=None)
        lambdas = basic_builder.make_veryloose_lambda_LL()
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            p_pidp_min=None, pt_min=250 * MeV)
        lambdas = basic_builder.make_veryloose_lambda_LL()

    line_alg = b_builder.make_b2Lambdah(
        particles=[lambdas, protons],
        descriptor='[B+ -> Lambda~0 p+]cc',
        am_min=4800. * MeV,
        am_max=6000. * MeV)
    return [lambdas, line_alg]


@check_process
def make_BuToL0barPp_DD(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(p_pidp_min=None)
        lambdas = basic_builder.make_loose_lambda_DD()
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            p_pidp_min=None, pt_min=250 * MeV)
        lambdas = basic_builder.make_loose_lambda_DD()

    line_alg = b_builder.make_b2Lambdah(
        particles=[lambdas, protons],
        descriptor='[B+ -> Lambda~0 p+]cc',
        am_min=4800. * MeV,
        am_max=6000. * MeV)
    return [lambdas, line_alg]


@check_process
def make_BcToL0barPp_LL(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(p_pidp_min=None)
        lambdas = basic_builder.make_veryloose_lambda_LL()
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            p_pidp_min=None, pt_min=250 * MeV)
        lambdas = basic_builder.make_veryloose_lambda_LL()

    line_alg = b_builder.make_b2Lambdah(
        particles=[lambdas, protons],
        descriptor='[B_c+ -> Lambda~0 p+ ]cc',
        am_min=5800. * MeV,
        am_max=7000. * MeV)
    return [lambdas, line_alg]


@check_process
def make_BcToL0barPp_DD(process):
    if process == 'spruce':
        protons = basic_builder.make_soft_protons(p_pidp_min=None)
        lambdas = basic_builder.make_loose_lambda_DD()
    elif process == 'hlt2':
        protons = basic_builder.make_soft_protons(
            p_pidp_min=None, pt_min=250 * MeV)
        lambdas = basic_builder.make_loose_lambda_DD()

    line_alg = b_builder.make_b2Lambdah(
        particles=[lambdas, protons],
        descriptor='[B_c+ -> Lambda~0 p+]cc',
        am_min=5800. * MeV,
        am_max=7000. * MeV)
    return [lambdas, line_alg]

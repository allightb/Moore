###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC b-baryon -> proton 2h lines
Implemented:
Xib- -> pKK
Xib- -> pKpi
Xib- -> ppipi
Xib- -> L0pi (LL / DD)
Xib- -> L0K (LL / DD)
"""

from GaudiKernel.SystemOfUnits import mm, MeV

from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_bbaryon_detached_pions, make_bbaryon_detached_kaons, make_bbaryon_soft_pions, make_bbaryon_soft_kaons, make_bbaryon_soft_protons, make_veryloose_lambda_LL, make_loose_lambda_DD
from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_2body, make_bbaryon_3body

all_lines = {}

###################
### Global cuts ###
###################

xibm_3body_no_sep_cut = {
    "mass_min": 5500 * MeV,
    "mass_max": 6600 * MeV,
    "pt_sum_min": 4100 * MeV,
    "adoca12_max": 0.1 * mm,
    "adoca13_max": 0.1 * mm,
    "adoca23_max": 0.1 * mm,
    "ipchi2_max": 12.,
    "bpvfdchi2_min": 50.,
    "dira_min": 0.9997,
    "med_pt_min": 700 * MeV,
    "med_bpvipchi2_min": 30.,
    "bpvipchi2_sum_min": 150.,
    "bcvtx_sep_min": None
}

##################
### HLT2 Lines ###
##################


# b-baryon -> proton 2h
@check_process
def make_XibmToPpKmKm(process):
    if process == 'spruce':
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_kaons(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron1],
        name="BNOC_XibmToPpKmKm_Combiner",
        descriptor='[Xi_b- -> p+ K- K-]cc',
        **xibm_3body_no_sep_cut)
    return line_alg


@check_process
def make_XibmToPpKmPim(process):
    if process == 'spruce':
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        name="BNOC_XibmToPpKmPim_Combiner",
        descriptor='[Xi_b- -> p+ K- pi-]cc',
        **xibm_3body_no_sep_cut)
    return line_alg


@check_process
def make_XibmToPpPimPim(process):
    if process == 'spruce':
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_pions(pid=None)
    elif process == 'hlt2':
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron1],
        name="BNOC_XibmToPpPimPim_Combiner",
        descriptor='[Xi_b- -> p+ pi- pi-]cc',
        **xibm_3body_no_sep_cut)
    return line_alg


# b-baryon -> Lambda0 h LL / DD


@check_process
def make_XibmToL0Pim_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="BNOC_XibmToL0Pim_LL_Combiner",
        descriptor='[Xi_b- -> Lambda0 pi-]cc')
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Pim_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="BNOC_XibmToL0Pim_DD_Combiner",
        descriptor='[Xi_b- -> Lambda0 pi-]cc',
        adoca12_max=1. * mm)
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Km_LL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="BNOC_XibmToL0Km_LL_Combiner",
        descriptor='[Xi_b- -> Lambda0 K-]cc')
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Km_DD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        name="BNOC_XibmToL0Km_DD_Combiner",
        descriptor='[Xi_b- -> Lambda0 K-]cc',
        adoca12_max=1. * mm)
    return [lambda0s, line_alg]

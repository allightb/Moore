###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC b-baryon -> hyperon h lines
"""
from GaudiKernel.SystemOfUnits import MeV, mm
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_bbaryon_detached_pions, make_bbaryon_detached_kaons, make_detached_down_pions, make_bbaryon_detached_down_kaons, make_veryloose_lambda_LL, make_loose_lambda_DD, make_xim_to_lambda_pi_lll, make_xim_to_lambda_pi_ddl, make_xim_to_lambda_pi_ddd, make_omegam_to_lambda_k_lll, make_omegam_to_lambda_k_ddl, make_omegam_to_lambda_k_ddd
from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_2body

all_lines = {}


#############################################################################
# Form the Lambda_b0 / Xi_b0> Xi- K+, Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-
# Xi- decays into Lambda0(LL or DD) and pi-(L)
#############################################################################
@check_process
def make_Lb0ToXimKp_LLL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Lb0ToXimKp_LLL_Combiner",
        descriptor='[Lambda_b0 -> Xi- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, xims, line_alg]


@check_process
def make_Lb0ToXimKp_DDL(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Lb0ToXimKp_DDL_Combiner",
        descriptor='[Lambda_b0 -> Xi- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, xims, line_alg]


@check_process
def make_Lb0ToXimKp_DDD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Lb0ToXimKp_DDD_Combiner",
        descriptor='[Lambda_b0 -> Xi- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, xims, line_alg]


###############################################################################
# Form the Lambda_b0 / Xi_b0 -> Xi- pi+, Xi- -> Lambda0 pi-, Lambda0 -> p+ pi-
# Xi- decays into Lambda0(LL or DD) and pi-(L)
###############################################################################
@check_process
def make_Xib0ToXimPip_LLL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Xib0ToXimPip_LLL_Combiner",
        descriptor='[Xi_b0 -> Xi- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, xims, line_alg]


@check_process
def make_Xib0ToXimPip_DDL(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Xib0ToXimPip_DDL_Combiner",
        descriptor='[Xi_b0 -> Xi- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, xims, line_alg]


@check_process
def make_Xib0ToXimPip_DDD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        name="BNOC_Xib0ToXimPip_DDD_Combiner",
        descriptor='[Xi_b0 -> Xi- pi+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, xims, line_alg]


###########################################################
# Form the Xi_b0 -> Omega- K+, Omega- -> Lambda0 K-, Lambda0 -> p+ pi-
# Omega- decays into Lambda0(LL or DD) and pi-(L)
##########################################################
@check_process
def make_Xib0ToOmmKp_LLL(process):
    if process == 'spruce':
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        name="BNOC_Xib0ToOmmKpLLL_Combiner",
        descriptor='[Xi_b0 -> Omega- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV)
    return [lambda0s, omms, line_alg]


@check_process
def make_Xib0ToOmmKp_DDL(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        name="BNOC_Xib0ToOmmKp_DDL_Combiner",
        descriptor='[Xi_b0 -> Omega- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, omms, line_alg]


@check_process
def make_Xib0ToOmmKp_DDD(process):
    if process == 'spruce':
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == 'hlt2':
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        name="BNOC_Xib0ToOmmKp_DDD_Combiner",
        descriptor='[Xi_b0 -> Omega- K+]cc',
        mass_min=5195 * MeV,
        mass_max=6105 * MeV,
        adoca12_max=0.5 * mm)
    return [lambda0s, omms, line_alg]

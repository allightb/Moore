# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This tightware is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of BnoC B+ -> KS0 h lines 
"""

from Hlt2Conf.lines.bnoc.builders.basic_builder import make_soft_pions, make_soft_kaons
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_KS_LL, make_KS_DD
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2ksh


@check_process
def make_BuToKSPip_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_KS_LL()
    line_alg = make_b2ksh(
        particles=[ks, pions], descriptor='[B+ -> KS0 pi+]cc')
    return [ks, line_alg]


@check_process
def make_BcToKSPip_LL(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_KS_LL()
    line_alg = make_b2ksh(
        particles=[ks, pions], descriptor='[B_c+ -> KS0 pi+]cc')
    return [ks, line_alg]


@check_process
def make_BuToKSPip_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_KS_DD()
    line_alg = make_b2ksh(
        particles=[ks, pions], descriptor='[B+ -> KS0 pi+]cc')
    return [ks, line_alg]


@check_process
def make_BcToKSPip_DD(process):
    if process == 'spruce':
        pions = make_soft_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        pions = make_soft_pions()
    ks = make_KS_DD()
    line_alg = make_b2ksh(
        particles=[ks, pions], descriptor='[B_c+ -> KS0 pi+]cc')
    return [ks, line_alg]


@check_process
def make_BuToKSKp_LL(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_KS_LL()
    line_alg = make_b2ksh(particles=[ks, kaons], descriptor='[B+ -> KS0 K+]cc')
    return [ks, line_alg]


@check_process
def make_BuToKSKp_DD(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_KS_DD()
    line_alg = make_b2ksh(particles=[ks, kaons], descriptor='[B+ -> KS0 K+]cc')
    return [ks, line_alg]


@check_process
def make_BcToKSKp_LL(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_KS_LL()
    line_alg = make_b2ksh(
        particles=[ks, kaons], descriptor='[B_c+ -> KS0 K+]cc')
    return [ks, line_alg]


@check_process
def make_BcToKSKp_DD(process):
    if process == 'spruce':
        kaons = make_soft_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        kaons = make_soft_kaons()
    ks = make_KS_DD()
    line_alg = make_b2ksh(
        particles=[ks, kaons], descriptor='[B_c+ -> KS0 K+]cc')
    return [ks, line_alg]

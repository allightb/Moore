###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 sprucing lines for Xi->Lambda0pi
"""

# TODO: remove unique names when calling builder when builder and lines switch to Thor-Functors masses

from __future__ import absolute_import
from PyConf import configurable
from Moore.config import SpruceLine, register_line_builder

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import (
    make_smog2_xi2L0pi_lll_line,
    make_smog2_xi2L0pi_ddl_line,
    make_smog2_xi2L0pi_ddd_line,
)

from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ns
import Functors as F

PROCESS = "spruce"
sprucing_lines = {}

_MASSWINDOW_LAMBDA0 = [(F.PDG_MASS("Lambda0") - 25) * MeV,
                       (F.PDG_MASS("Lambda0") + 25) * MeV]
_MASSWINDOW_XI = [(F.PDG_MASS("Xi-") - 25) * MeV,
                  (F.PDG_MASS("Xi-") + 25) * MeV]

#################################################################
#################################################################
###################  CHARGED PID LINES  #########################
#################################################################
#################################################################


@register_line_builder(sprucing_lines)
@configurable
def smog2_xi2lambda0pi_lll_spruceline(
        name="SpruceIFT_SMOG2Xi2Lambda0pi_lll",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        end_vz_max=2200 * mm,
        apt_min=0 * MeV,
        vchi2pdof_max=25.0,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
):
    """
    SMOG2 Xi -> Lambda0 pi HLT2 trigger line for PID calibration (no PID requirements)
    All children reconstructed as long tracks
    """
    pvs = make_pvs

    xi2Lambda0pi = make_smog2_xi2L0pi_lll_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Xi=mmin_Xi,
        mmax_Xi=mmax_Xi,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        name_Xi="smog2_Xi2L0pi_lll_{hash}",
        name_L0="smog2_lambda2ppi_ll_loose_no_ks_veto_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [xi2Lambda0pi],
        hlt2_filter_code="Hlt2IFT_SMOG2Xi2Lambda0pi_lllDecision",
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_xi2lambda0pi_ddl_spruceline(
        name="SpruceIFT_SMOG2Xi2Lambda0pi_ddl",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        end_vz_max=2200 * mm,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdchi2_min=5,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
):
    """
    SMOG2 Xi -> Lambda0 pi HLT2 trigger line for PID calibration (no PID requirements)
    Lambda0 -> p pi reconstructed as long tracks, pi reconstructed as downstream tracks
    """
    pvs = make_pvs

    xi2Lambda0pi = make_smog2_xi2L0pi_ddl_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Xi=mmin_Xi,
        mmax_Xi=mmax_Xi,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        name_Xi="smog2_Xi2L0pi_ddl_{hash}",
        name_L0="smog2_lambda2ppi_dd_loose_no_ks_veto_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [xi2Lambda0pi],
        hlt2_filter_code="Hlt2IFT_SMOG2Xi2Lambda0pi_ddlDecision",
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_xi2lambda0pi_ddd_spruceline(
        name="SpruceIFT_SMOG2Xi2Lambda0pi_ddd",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=100 * MeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin_Xi=_MASSWINDOW_XI[0],
        mmax_Xi=_MASSWINDOW_XI[1],
        mmin_L0=_MASSWINDOW_LAMBDA0[0],
        mmax_L0=_MASSWINDOW_LAMBDA0[1],
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdchi2_min=5,
        parent_bpvipchi2_max=1000,
        ks_veto_window=None,
):
    """
    SMOG2 Xi -> Lambda0 pi HLT2 trigger line for PID calibration (no PID requirements)
    All children reconstructed as downstream tracks
    """
    pvs = make_pvs

    xi2Lambda0pi = make_smog2_xi2L0pi_ddd_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Xi=mmin_Xi,
        mmax_Xi=mmax_Xi,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        name_Xi="smog2_Xi2L0pi_ddd_{hash}",
        name_L0="smog2_lambda2ppi_dd_loose_no_ks_veto_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [xi2Lambda0pi],
        hlt2_filter_code="Hlt2IFT_SMOG2Xi2Lambda0pi_dddDecision",
        prescale=prescale,
        persistreco=persistreco,
    )

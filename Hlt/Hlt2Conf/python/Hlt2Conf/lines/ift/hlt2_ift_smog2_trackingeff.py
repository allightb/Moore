###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 tracking efficiency HLT2 lines - following pp tracking efficiency method: 
 - Combination of long 'tag' track and partially reconstructed 'probe' track
 - Matching of probe with long track
 - running without UT: VeloMuon and SeedMuon lines
"""
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from Hlt2Conf.probe_muons import make_velomuon_muons, make_seed_muons
from PyConf.Algorithms import MuonProbeToLongMatcher
import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.lines.ift.builders.smog2_builders import sv_in_smog2, bpv_in_smog2
from Hlt2Conf.lines.trackeff.DiMuonTrackEfficiency import make_LongFilter

all_lines = {}

#use Velo+Muon track for efficiency of SciFi(+UT)
VeloMuon_cuts = {
    'max_ProbeTrChi2': 5,
    'min_ProbeP': 2 * GeV,
    'min_ProbePT': 0.2 * GeV,
    'max_TagTrChi2': 5,
    'min_TagP': 2 * GeV,
    'min_TagPT': 0 * GeV,
    'min_TagMuonID': -1,
    'max_TagGhostProb': 1,
    'max_JPsiDOCA': 3 * mm,
    'max_JPsiVtxChi2': 32,
    'min_JPsiPT': 0 * GeV,
    'min_JPsimass': 2600 * MeV,
    'max_JPsimass': 3600 * MeV,
    'checkVP': True,
    'checkFT': False,
    'checkUT': False,
    'checkMuon': True,
    'require_ismuon': False,
}

#use SciFi+Muon track for efficiency of Velo(+UT)
SeedMuon_cuts = {
    'max_ProbeTrChi2': 5,
    'min_ProbeP': 2 * GeV,
    'min_ProbePT': 0.2 * GeV,
    'max_TagTrChi2': 5,
    'min_TagP': 2 * GeV,
    'min_TagPT': 0 * GeV,
    'min_TagMuonID': -1,
    'max_TagGhostProb': 1,
    'max_JPsiDOCA': 3 * mm,
    'max_JPsiVtxChi2': 32,
    'min_JPsiPT': 0 * GeV,
    'min_JPsimass': 2600 * MeV,
    'max_JPsimass': 3600 * MeV,
    'checkVP': False,
    'checkFT': True,
    'checkUT': False,
    'checkMuon': True,
    'require_ismuon': False,
}


@configurable
# Combination of Tag and Probe to make the JPsi candidate
def make_JPsiCombiner(particles,
                      pvs,
                      probe_charge,
                      get_cuts,
                      name='Hlt2IFT_SMOG2TrackEff_Jpsi_{hash}'):
    #upper JPsi DOCA
    combination_cut = F.require_all(
        in_range(get_cuts['min_JPsimass'], F.MASS, get_cuts['max_JPsimass']),
        F.SUM(F.PT) > get_cuts['min_JPsiPT'],
    )
    if get_cuts['max_JPsiDOCA'] is not None:
        combination_cut = F.require_all(combination_cut,
                                        F.MAXDOCACUT(get_cuts['max_JPsiDOCA']))
    #additional cuts
    mother_cut = F.require_all(
        in_range(get_cuts['min_JPsimass'], F.MASS, get_cuts['max_JPsimass']),
        F.PT > get_cuts['min_JPsiPT'],
    )
    if get_cuts['max_JPsiVtxChi2'] is not None:
        mother_cut = F.require_all(mother_cut,
                                   F.CHI2DOF < get_cuts['max_JPsiVtxChi2'],
                                   sv_in_smog2(), bpv_in_smog2(pvs))

    decay_descriptor = "J/psi(1S) -> mu- mu+" if probe_charge == 1 else "J/psi(1S) -> mu+ mu-"
    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut,
        CompositeCut=mother_cut,
        name=name)


@configurable
# Tag filter
def make_TagFilter(particles,
                   pvs,
                   probe_charge,
                   get_cuts,
                   name='Hlt2IFT_SMOG2TrackEff_TagTrack_{hash}'):
    code = F.require_all(
        (F.CHARGE < 0) if probe_charge == 1 else (F.CHARGE > 0),
        F.CHI2DOF < get_cuts['max_TagTrChi2'],
        F.P > get_cuts['min_TagP'],
        F.PT > get_cuts['min_TagPT'],
        F.ISMUON(),
        F.PID_MU > get_cuts['min_TagMuonID'],
        F.GHOSTPROB() < get_cuts['max_TagGhostProb'],
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
# Probe filter
def make_ProbeFilter(particles,
                     pvs,
                     probe_charge,
                     get_cuts,
                     name='Hlt2IFT_SMOG2TrackEff_ProbeTrack_{hash}'):
    code = F.require_all(
        (F.CHARGE > 0) if probe_charge == 1 else (F.CHARGE < 0),
        F.CHI2DOF < get_cuts['max_ProbeTrChi2'],
        F.P > get_cuts['min_ProbeP'],
        F.PT > get_cuts['min_ProbePT'],
    )
    if get_cuts['require_ismuon']:
        code = F.require_all(code, F.ISMUON())
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def create_TagLine(name, prescale, persistreco, probe_charge, get_cuts,
                   particles):
    pvs = make_pvs
    probe_muons = make_ProbeFilter(particles, pvs, probe_charge, get_cuts)
    tag_muons = make_TagFilter(make_ismuon_long_muon(), pvs, probe_charge,
                               get_cuts)
    JPsi = make_JPsiCombiner([tag_muons, probe_muons], pvs, probe_charge,
                             get_cuts)

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [JPsi],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=persistreco)


@configurable
def create_MatchLine(name, prescale, persistreco, probe_charge, get_cuts,
                     particles):
    pvs = make_pvs
    probe_muons = make_ProbeFilter(particles, pvs, probe_charge, get_cuts)
    tag_muons = make_TagFilter(make_ismuon_long_muon(), pvs, probe_charge,
                               get_cuts)
    long_track = make_LongFilter(make_ismuon_long_muon(), probe_charge)
    JPsi = make_JPsiCombiner([tag_muons, probe_muons], pvs, probe_charge,
                             get_cuts)

    matcher = MuonProbeToLongMatcher(
        TwoBodyComposites=JPsi,
        LongTracks=long_track,
        checkVP=get_cuts['checkVP'],
        checkFT=get_cuts['checkFT'],
        checkUT=get_cuts['checkUT'],
        checkMuon=get_cuts['checkMuon'])
    JPsi_matched = matcher.MatchedComposites
    long_matched = matcher.MatchedLongTracks
    long_save = make_LongFilter(long_matched, probe_charge)
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [JPsi, JPsi_matched],
        extra_outputs=[("LongMatched", long_save)],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=persistreco)


for line, get_cuts, myparticles in zip(['VeloMuon', 'SeedMuon'],
                                       [VeloMuon_cuts, SeedMuon_cuts],
                                       [make_velomuon_muons, make_seed_muons]):
    for charge, mu in zip([1, 0], ['mup', 'mum']):

        @register_line_builder(all_lines)
        @configurable
        def LineWithTagging(name="Hlt2IFT_SMOG2TrackEff_DiMuon_" + line + "_" +
                            mu + "_Tag",
                            prescale=1,
                            persistreco=True,
                            probe_charge=charge,
                            get_cuts=get_cuts,
                            get_particles=myparticles):
            return create_TagLine(name, prescale, persistreco, probe_charge,
                                  get_cuts, get_particles())

        @register_line_builder(all_lines)
        @configurable
        def LineWithMatching(name="Hlt2IFT_SMOG2TrackEff_DiMuon_" + line + "_"
                             + mu + "_Match",
                             prescale=1,
                             persistreco=True,
                             probe_charge=charge,
                             get_cuts=get_cuts,
                             get_particles=myparticles):
            return create_MatchLine(name, prescale, persistreco, probe_charge,
                                    get_cuts, get_particles())

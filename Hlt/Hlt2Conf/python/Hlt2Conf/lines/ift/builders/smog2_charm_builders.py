###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, GeV

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons, make_has_rich_long_protons

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_common_particles, sv_in_smog2, bpv_in_smog2

all_lines = {}


@configurable
def make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,  #PIDp - PIDK > minPIDpK
        minpT=0 * MeV,
        minp=1 * GeV,
        maxtrchi2dof=5):

    criteria = {
        "bpvipchi2": bpvipchi2,
        "minPIDK": minPIDK,
        "maxPIDK": maxPIDK,
        "minPIDp": minPIDp,
        "minPIDpK": minPIDpK,
        "minpT": minpT,
        "minp": minp,
        "maxtrchi2dof": maxtrchi2dof
    }

    return criteria


@configurable
def make_comb_criteria(mass=None,
                       masswin=None,
                       min_child_pt=0 * MeV,
                       max_sdoca=None,
                       vchi2pdof_max=25,
                       min_bpvltime=None,
                       min_mother_pt=None):

    criteria_comb = {
        "mass": mass,
        "masswin": masswin,
        "min_child_pt": min_child_pt,
        "max_sdoca": max_sdoca,
        "vchi2pdof_max": vchi2pdof_max,
        "min_bpvltime": min_bpvltime,
        "min_mother_pt": min_mother_pt,
    }

    return criteria_comb


#====================================================
#  Make common particles : pions, kaons, proton
#====================================================


@configurable
def make_charm_pions(bpvipchi2=None,
                     maxPIDKaon=None,
                     min_pt=0 * MeV,
                     min_p=0 * GeV,
                     max_trchi2dof=5):

    return make_smog2_common_particles(
        make_particles=make_has_rich_long_pions,
        max_trchi2dof=max_trchi2dof,
        min_pt=min_pt,
        min_p=min_p,
        pid_cut=maxPIDKaon,
        particle='Pion',
        min_bpvipchi2=bpvipchi2)


@configurable
def make_charm_kaons(bpvipchi2=None,
                     minPIDKaon=None,
                     min_pt=0 * MeV,
                     min_p=0 * GeV,
                     max_trchi2dof=5):

    return make_smog2_common_particles(
        make_particles=make_has_rich_long_kaons,
        max_trchi2dof=max_trchi2dof,
        min_pt=min_pt,
        min_p=min_p,
        pid_cut=minPIDKaon,
        particle='Kaon',
        min_bpvipchi2=bpvipchi2)


@configurable
def make_charm_protons_loose(bpvipchi2=None,
                             minPIDProton=None,
                             min_pt=0 * MeV,
                             min_p=0 * GeV,
                             max_trchi2dof=5):

    return make_smog2_common_particles(
        make_particles=make_has_rich_long_protons,
        max_trchi2dof=max_trchi2dof,
        min_pt=min_pt,
        min_p=min_p,
        pid_cut=minPIDProton,
        particle='Proton',
        min_bpvipchi2=bpvipchi2)


#Tight protons: PIDp > minPIDp and PIDp - PIDK > minPIDpK
@configurable
def make_charm_protons_tight(bpvipchi2=None,
                             minPIDProton=None,
                             minPIDProtonKaon=None,
                             min_pt=0 * MeV,
                             min_p=0 * GeV,
                             max_trchi2dof=5):

    protons = make_smog2_common_particles(
        make_particles=make_has_rich_long_protons,
        max_trchi2dof=max_trchi2dof,
        min_pt=min_pt,
        min_p=min_p,
        pid_cut=minPIDProton,
        particle='Proton',
        min_bpvipchi2=bpvipchi2)

    code = (F.PID_P - F.PID_K) > minPIDProtonKaon

    return ParticleFilter(
        protons, Cut=F.FILTER(code), name='TightProtons_{hash}')


#======================================================
#         Make combination of particles
#======================================================


@configurable
def define_child(particle, Criteria):
    #  [bpvipchi2, minPIDk, maxPIDK, minPIDp, minPIDpK, minpT, minp, maxtrchi2dof]

    if particle == "kaon":
        dau = make_charm_kaons(
            bpvipchi2=Criteria["bpvipchi2"],
            minPIDKaon=Criteria["minPIDK"],
            min_pt=Criteria["minpT"],
            min_p=Criteria["minp"],
            max_trchi2dof=Criteria["maxtrchi2dof"])
    elif particle == "pion":
        dau = make_charm_pions(
            bpvipchi2=Criteria["bpvipchi2"],
            maxPIDKaon=Criteria["maxPIDK"],
            min_pt=Criteria["minpT"],
            min_p=Criteria["minp"],
            max_trchi2dof=Criteria["maxtrchi2dof"])
    elif particle == "proton":
        if (Criteria["minPIDpK"] is None):
            dau = make_charm_protons_loose(
                bpvipchi2=Criteria["bpvipchi2"],
                minPIDProton=Criteria["minPIDp"],
                min_pt=Criteria["minpT"],
                min_p=Criteria["minp"],
                max_trchi2dof=Criteria["maxtrchi2dof"])
        else:
            dau = make_charm_protons_tight(
                bpvipchi2=Criteria["bpvipchi2"],
                minPIDProton=Criteria["minPIDp"],
                minPIDProtonKaon=Criteria["minPIDpK"],
                min_pt=Criteria["minpT"],
                min_p=Criteria["minp"],
                max_trchi2dof=Criteria["maxtrchi2dof"])
    else:
        print(particle + "is not correctly defined")

    return dau


@configurable
def make_charm2hadrons(name, process, pvs, decay, particle, CriteriaParticle,
                       CriteriaCombination):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(
        F.MASS >
        (CriteriaCombination["mass"] - CriteriaCombination["masswin"]),
        F.MASS <
        (CriteriaCombination["mass"] + CriteriaCombination["masswin"]))
    dau1 = define_child(particle[0], CriteriaParticle[0])
    dau2 = define_child(particle[1], CriteriaParticle[1])

    combination_code = F.require_all(
        in_range(CriteriaCombination["mass"] - CriteriaCombination["masswin"],
                 F.MASS,
                 CriteriaCombination["mass"] + CriteriaCombination["masswin"]),
        F.SUM(F.PT > CriteriaCombination["min_child_pt"]) > 0)

    if CriteriaCombination["max_sdoca"] is not None:
        combination_code &= F.MAXSDOCACUT(CriteriaCombination["max_sdoca"])

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"], sv_in_smog2(),
        bpv_in_smog2(pvs))

    if CriteriaCombination["min_bpvltime"] is not None:
        vertex_code &= F.BPVLTIME(pvs()) > CriteriaCombination["min_bpvltime"]

    dihadron = ParticleCombiner(
        [dau1, dau2],
        DecayDescriptor=decay,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )

    return ParticleFilter(dihadron, Cut=F.FILTER(code), name=name)


@configurable
def make_D0forDst(
        process,
        pvs,
        kaons,
        pions,
        CriteriaCombination,
        double_misid_veto=25 * MeV,
        single_misid_veto=25 * MeV,
):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    combination_code = F.require_all(
        in_range(CriteriaCombination["mass"] - CriteriaCombination["masswin"],
                 F.MASS,
                 CriteriaCombination["mass"] + CriteriaCombination["masswin"]),
        F.SUM(F.PT > CriteriaCombination["min_child_pt"]) > 0)

    if CriteriaCombination["max_sdoca"] is not None:
        combination_code &= F.MAXSDOCACUT(CriteriaCombination["max_sdoca"])

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"],
        (F.MASSWITHHYPOTHESES(
            ("pi-", "K+")) < CriteriaCombination["mass"] - double_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "K+")) > CriteriaCombination["mass"] + double_misid_veto),
        (F.MASSWITHHYPOTHESES(
            ("pi-", "pi+")) < CriteriaCombination["mass"] - single_misid_veto)
        | (F.MASSWITHHYPOTHESES(
            ("pi-", "pi+")) > CriteriaCombination["mass"] + single_misid_veto),
        (F.CHILD(1, F.PT) > CriteriaCombination["min_child_pt"]) |
        (F.CHILD(2, F.PT) > CriteriaCombination["min_child_pt"]),
    )
    if CriteriaCombination["min_bpvltime"] is not None:
        vertex_code &= F.BPVLTIME(pvs()) > CriteriaCombination["min_bpvltime"]

    if CriteriaCombination["min_mother_pt"] is not None:
        vertex_code &= F.PT > CriteriaCombination["min_mother_pt"]

    return ParticleCombiner(
        [kaons, pions],
        name='smog2_PID_D0ToKPi_Combiner_{hash}',
        DecayDescriptor="[D0 -> K- pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Dst(name, process, pvs, CriteriaParticleD0, CriteriaCombinationD0,
             CriteriaSlowPion, CriteriaCombinationDstar):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    kaons = define_child("kaon", CriteriaParticleD0[0])
    pions = define_child("pion", CriteriaParticleD0[1])

    makeD0 = make_D0forDst(process, pvs, kaons, pions, CriteriaCombinationD0)
    slowpion = define_child("pion", CriteriaSlowPion)

    combination_code = F.require_all(
        in_range(
            CriteriaCombinationDstar["mass"] -
            CriteriaCombinationDstar["masswin"], F.MASS,
            CriteriaCombinationDstar["mass"] +
            CriteriaCombinationDstar["masswin"]),
        F.SUM(F.PT > CriteriaCombinationDstar["min_child_pt"]) > 0,
        F.MASS - F.CHILD(1, F.MASS) > 135 * MeV,
        F.MASS - F.CHILD(1, F.MASS) < 160 * MeV,
    )

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombinationDstar["vchi2pdof_max"], sv_in_smog2(),
        bpv_in_smog2(pvs))

    Dstar = ParticleCombiner(
        [makeD0, slowpion],
        name=name,
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return Dstar


@configurable
def make_charm3hadrons(name, process, pvs, decay, particle, CriteriaParticle,
                       CriteriaCombination):
    """Return a decay maker for either of the decays:
    1. D-  -> K- pi+ pi+
    2. Ds- -> K- K+  pi+
    3. Lc  -> K pi p
    """

    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(
        F.MASS >
        (CriteriaCombination["mass"] - CriteriaCombination["masswin"]),
        F.MASS <
        (CriteriaCombination["mass"] + CriteriaCombination["masswin"]))
    dau1 = define_child(particle[0], CriteriaParticle[0])
    dau2 = define_child(particle[1], CriteriaParticle[1])
    dau3 = define_child(particle[2], CriteriaParticle[2])

    combination_code = F.require_all(
        in_range(CriteriaCombination["mass"] - CriteriaCombination["masswin"],
                 F.MASS,
                 CriteriaCombination["mass"] + CriteriaCombination["masswin"]),
        F.SUM(F.PT > CriteriaCombination["min_child_pt"]) > 0)

    if CriteriaCombination["max_sdoca"] is not None:
        combination_code &= F.MAXSDOCACUT(CriteriaCombination["max_sdoca"])

    vertex_code = F.require_all(
        F.CHI2DOF < CriteriaCombination["vchi2pdof_max"], sv_in_smog2(),
        bpv_in_smog2(pvs))

    if CriteriaCombination["min_bpvltime"] is not None:
        vertex_code &= F.BPVLTIME(pvs()) > CriteriaCombination["min_bpvltime"]

    trihadron = ParticleCombiner(
        Inputs=[dau1, dau2, dau3],
        DecayDescriptor=decay,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

    return ParticleFilter(trihadron, F.FILTER(code), name=name)


@configurable
def make_charm4hadrons(name,
                       process,
                       pvs,
                       decay,
                       particle,
                       CriteriaParticle,
                       max_sdoca,
                       max_vchi2pdof,
                       mass,
                       masswindow,
                       allchild_ptcut,
                       twochild_ptcut,
                       onechild_ptcut,
                       min_bpvltime=None):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    code = F.require_all(F.MASS > (mass - masswindow),
                         F.MASS < (mass + masswindow))
    daus = [
        define_child(particle[0], CriteriaParticle[0]),
        define_child(particle[1], CriteriaParticle[1]),
        define_child(particle[2], CriteriaParticle[2]),
        define_child(particle[3], CriteriaParticle[3])
    ]

    allhaschild_pt = F.SUM(F.PT > allchild_ptcut) > 3
    twohaschild_pt = F.SUM(F.PT > twochild_ptcut) > 1
    ahaschild_pt = F.SUM(F.PT > onechild_ptcut) > 0

    combination_code = F.require_all(
        in_range(mass - masswindow, F.MASS, mass + masswindow),
        F.MAXSDOCACUT(max_sdoca), allhaschild_pt, twohaschild_pt, ahaschild_pt)

    vertex_code = F.require_all(F.CHI2DOF < max_vchi2pdof, sv_in_smog2(),
                                bpv_in_smog2(pvs))

    if min_bpvltime is not None:
        vertex_code &= F.BPVLTIME(pvs()) > min_bpvltime

    quadrihadron = ParticleCombiner(
        Inputs=daus,
        DecayDescriptor=decay,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

    return ParticleFilter(quadrihadron, F.FILTER(code), name=name)

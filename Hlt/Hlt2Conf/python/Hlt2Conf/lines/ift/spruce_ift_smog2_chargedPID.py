###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 sprucing lines for charged PID calibration
"""

from __future__ import absolute_import
from PyConf import configurable
from Moore.config import SpruceLine, register_line_builder

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import make_smog2_L02ppi_ll_line, make_smog2_ks2pipi_ll_line, make_smog2_phi2kk, make_smog2_phi2kk_calib, make_smog2_kstar2kpi_line
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import make_smog2_L02ppi_dd_line, make_smog2_ks2pipi_dd_line
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ns

PROCESS = "spruce"
sprucing_lines = {}

_MASSWINDOW_KS = [(497.7 - 50) * MeV, (497.7 + 50) * MeV]
_MASSWINDOW_KStar = [(891.67 - 300) * MeV, (891.67 + 300) * MeV]
_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]

_MASSWINDOW_COMB_PHI = [(1019.445 - 40) * MeV, (1019.445 + 40) * MeV]
_MASSWINDOW_VERTEX_PHI = [(1019.445 - 20) * MeV, (1019.445 + 20) * MeV]

_MASS_KS = 497.7 * MeV
_MASS_Lambda0 = 1115.683 * MeV
_MASS_PHI = 1019.445 * MeV

#################################################################
#################################################################
###################  CHARGED PID LINES  #########################
#################################################################
#################################################################


@register_line_builder(sprucing_lines)
@configurable
def smog2_ks2pipi_ll_spruceline(
        name="SpruceIFT_SMOG2KS2PiPiLL",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=25.,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=75,
        lambda_veto_window=9 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        name='smog2_ks2pipi_ll',
        process="spruce")
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        hlt2_filter_code='Hlt2IFT_SMOG2KS2PiPiLLDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_ks2pipi_dd_spruceline(
        name="SpruceIFT_SMOG2KS2PiPiDD",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=16.,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=200,
        lambda_veto_window=18 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as downstream tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        process="spruce",
        name='smog2_ks2pipi_dd')
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        hlt2_filter_code='Hlt2IFT_SMOG2KS2PiPiDDDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_Kstar2Kpi_spruceline(
        name="SpruceIFT_SMOG2Kstar2KPi",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        minPIDKaon=0,
        maxPIDKaon=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KStar[0],
        mmax=_MASSWINDOW_KStar[1],
        mminver=_MASSWINDOW_KStar[0],
        mmaxver=_MASSWINDOW_KStar[1],
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25.,
):
    """
    SMOG2 K* -> K pi sprucing line
    """
    pvs = make_pvs

    kstar2kpi = make_smog2_kstar2kpi_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        minPIDKaon=minPIDKaon,
        maxPIDKaon=maxPIDKaon,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        mminver=_MASSWINDOW_KStar[0],
        mmaxver=_MASSWINDOW_KStar[1],
        apt_min=apt_min,
        maxsdoca=10 * mm,
        vchi2pdof_max=vchi2pdof_max,
        process="spruce",
        name='smog2_kstar2kpi')

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [kstar2kpi],
        hlt2_filter_code=[
            'Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2Decision',
            'Hlt2IFT_SMOG2MBPassthroughDecision'
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_L02ppi_ll_spruceline(
        name="SpruceIFT_SMOG2Lambda02PPiLL",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=25.,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=200,
        ks_veto_window=20 * MeV,
):
    """
    SMOG2 Lambda0 -> p pi sprucing trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="spruce",
        name='smog2_lambda2ppi_ll')

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        hlt2_filter_code='Hlt2IFT_SMOG2Lambda02PPiLLDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_L02ppi_dd_spruceline(
        name="SpruceIFT_SMOG2Lambda02PPiDD",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=16.,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=None,
        ks_veto_window=40 * MeV,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as downstream tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="spruce",
        name='smog2_lambda2ppi_dd')

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        hlt2_filter_code='Hlt2IFT_SMOG2Lambda02PPiDDDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_phi2kk_spruceline(
        name="SpruceIFT_SMOG2Phi2kk",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=380 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=0,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=16.,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) sprucing trigger line for physics analysis
    """
    name = "SpruceIFT_SMOG2Phi2kk"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="spruce")
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        hlt2_filter_code=[
            'Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2Decision',
            'Hlt2IFT_SMOG2MBPassthroughDecision'
        ],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_phi2kk_kmProbe_spruceline(
        name="SpruceIFT_SMOG2Phi2kk_Kmprobe",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=380 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=16.,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) sprucing trigger line for PID calibration
    """
    name = "SpruceIFT_SMOG2Phi2kk_Kmprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="spruce")
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        hlt2_filter_code='Hlt2IFT_SMOG2Phi2kk_KmprobeDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def smog2_phi2kk_kpProbe_spruceline(name="SpruceIFT_SMOG2Phi2kk_Kpprobe",
                                    prescale=1,
                                    persistreco=True,
                                    min_p=2 * GeV,
                                    min_pt=380 * MeV,
                                    max_trchi2dof=5,
                                    max_ghostprob=0.25,
                                    min_pidk=15,
                                    mmincomb=_MASSWINDOW_COMB_PHI[0],
                                    mmaxcomb=_MASSWINDOW_COMB_PHI[1],
                                    mminver=_MASSWINDOW_VERTEX_PHI[0],
                                    mmaxver=_MASSWINDOW_VERTEX_PHI[1],
                                    apt_min=0 * MeV,
                                    maxsdoca=10 * mm,
                                    vchi2pdof_max=16.):
    """
    SMOG2 phi(1020) -> K K (Kp probe) sprucing trigger line for PID calibration
    """
    name = "SpruceIFT_SMOG2Phi2kk_Kpprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="spruce")
    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        hlt2_filter_code='Hlt2IFT_SMOG2Phi2kk_KpprobeDecision',
        prescale=prescale,
        persistreco=persistreco)

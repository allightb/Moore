###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import absolute_import, division, print_function
from Moore.config import register_line_builder, SpruceLine
from RecoConf.reconstruction_objects import make_pvs
from PyConf import configurable

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_charm_builders import make_particle_criteria, make_comb_criteria
from Hlt2Conf.lines.ift.builders.smog2_charm_builders import make_charm2hadrons, make_charm3hadrons, make_charm4hadrons

sprucing_lines = {}
PROCESS = "spruce"

#===================
#    PDG masses
#===================

__MASS_D0 = 1864.83 * MeV
__MASS_DP = 1869.62 * MeV
__MASS_DS = 1968.49 * MeV
__MASS_Lc = 2286.46 * MeV
__MASS_Xicp = 2467.95 * MeV
__MASS_Xic0 = 2470.99 * MeV
__MASS_Omegac0 = 2695.2 * MeV
__MASS_ETAC = 2983.9 * MeV
__MASS_JPSI = 3096.9 * MeV
__MASS_CHIC0 = 3414.7 * MeV
__MASS_CHIC2 = 3556.17 * MeV
__MASS_PSI2S = 3686.1 * MeV

__MASSWin_D0 = 150 * MeV
__MASSWin_ETAC = 300 * MeV
__MASSWin_DP = 150 * MeV
__MASSWin_DS = 150 * MeV
__MASSWin_Lc = 150 * MeV
__MASSWin_Xicp = 150 * MeV
__MASSWin_Xic0 = 150 * MeV
__MASSWin_Omegac0 = 150 * MeV

#========================================
#            2-body decays
#========================================


@register_line_builder(sprucing_lines)
@configurable
def smog2_D02kpi_spruceline(name='SpruceIFT_SMOG2D02KPi',
                            prescale=1,
                            persistreco=True):

    # Define bpvipchi2, minPIDk, maxPIDK, minPIDp, minpT, minp, maxtrchi2dof
    # if the line retention rate is low : no PID cut
    # otherwise :
    ## for kaon : PIDK > 0
    ## for pion : PIDK < 5
    ## for proton : PIDp > 0

    # non prompt particle : bpipchi2 > 4
    # prompt particle : bpipchi2 > -1000

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=2,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=2,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    D02kpi = make_charm2hadrons(
        name="D02kpi",
        process="spruce",
        pvs=pvs,
        decay="[D0 -> K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [D02kpi],
        hlt2_filter_code='Hlt2IFT_SMOG2D02KPiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def D02kk_spruceline(name='SpruceIFT_SMOG2D02KK', prescale=1,
                     persistreco=True):

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "kaon"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    D02kk = make_charm2hadrons(
        name="D02kk",
        process="spruce",
        pvs=pvs,
        decay="D0 -> K- K+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [D02kk],
        hlt2_filter_code='Hlt2IFT_SMOG2D02KKDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def D02pipi_spruceline(name='SpruceIFT_SMOG2D02pipi',
                       prescale=1,
                       persistreco=True):

    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["pion", "pion"]
    CriteriaParticles = [CriteriaPion, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    D02pipi = make_charm2hadrons(
        name="D02pipi",
        process="spruce",
        pvs=pvs,
        decay="D0 -> pi- pi+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [D02pipi],
        hlt2_filter_code='Hlt2IFT_SMOG2D02pipiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Etac2ppbar_spruceline(name='SpruceIFT_SMOG2Etac2ppbar',
                          prescale=1,
                          persistreco=True):

    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["proton", "proton"]
    CriteriaParticles = [CriteriaProton, CriteriaProton]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_ETAC,
        masswin=__MASSWin_ETAC,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    etac2ppbar = make_charm2hadrons(
        name="etac2ppbar",
        process="spruce",
        pvs=pvs,
        decay='eta_c(1S) -> p+ p~-',
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [etac2ppbar],
        hlt2_filter_code='Hlt2IFT_SMOG2Etac2ppbarDecision',
        prescale=prescale,
        persistreco=persistreco)


#========================================
#            3-body decays
#========================================


@register_line_builder(sprucing_lines)
@configurable
def Dpm2kpipi_spruceline(name='SpruceIFT_SMOG2Dpm2kpipi',
                         prescale=1,
                         persistreco=True):

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "pion", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaPion, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_DP,
        masswin=__MASSWin_DP,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    Dpm2kpipi = make_charm3hadrons(
        name="Dpm2kpipi",
        process="spruce",
        pvs=pvs,
        decay="[D+ -> K- pi+ pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Dpm2kpipi],
        hlt2_filter_code='Hlt2IFT_SMOG2Dpm2kpipiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Ds2kkpi_spruceline(name='SpruceIFT_SMOG2DsToKKPi',
                       prescale=1,
                       persistreco=True):

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "kaon", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_DS,
        masswin=__MASSWin_DS,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    Ds2kkpi = make_charm3hadrons(
        name="Ds2kkpi",
        process="spruce",
        pvs=pvs,
        decay="[D_s+ -> K- K+ pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Ds2kkpi],
        hlt2_filter_code='Hlt2IFT_SMOG2DsToKKPiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Lc2pkpi_spruceline(name='SpruceIFT_SMOG2LcTopKPi',
                       prescale=1,
                       persistreco=True):

    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["proton", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_Lc,
        masswin=__MASSWin_Lc,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    Lc2pkpi = make_charm3hadrons(
        name="Lc2pkpi",
        process="spruce",
        pvs=pvs,
        decay="[Lambda_c+ -> p+ K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Lc2pkpi],
        hlt2_filter_code='Hlt2IFT_SMOG2LcTopKPiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Xicp2pkpi_spruceline(name='SpruceIFT_SMOG2XicpTopKPi',
                         prescale=1,
                         persistreco=True):

    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["proton", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_Xicp,
        masswin=__MASSWin_Xicp,
        min_child_pt=0 * MeV,
        max_sdoca=2 * mm,
        vchi2pdof_max=25)

    Xicp2pkpi = make_charm3hadrons(
        name="Xicp2pkpi",
        process="spruce",
        pvs=pvs,
        decay="[Xi_c+ -> p+ K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Xicp2pkpi],
        hlt2_filter_code='Hlt2IFT_SMOG2XicpTopKPiDecision',
        prescale=prescale,
        persistreco=persistreco)


#========================================
#            4-body decays
#========================================


@register_line_builder(sprucing_lines)
@configurable
def Xic02pkkpi_spruceline(name='SpruceIFT_SMOG2Xic0TopKKPi',
                          prescale=1,
                          persistreco=True):

    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["proton", "kaon", "kaon", "pion"]
    CriteriaParticles = [
        CriteriaProton, CriteriaKaon, CriteriaKaon, CriteriaPion
    ]

    Xic02pkkpi = make_charm4hadrons(
        name="Xic02pkkpi",
        process="spruce",
        pvs=pvs,
        decay="[Xi_c0 -> p+ K- K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        mass=__MASS_Xic0,
        masswindow=__MASSWin_Xic0,
        max_sdoca=2 * mm,
        max_vchi2pdof=25,
        allchild_ptcut=0 * GeV,
        twochild_ptcut=0 * GeV,
        onechild_ptcut=0 * GeV)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Xic02pkkpi],
        hlt2_filter_code='Hlt2IFT_SMOG2Xic0TopKKPiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Omegac02pkkpi_spruceline(name='SpruceIFT_SMOG2Omegac0TopKKPi',
                             prescale=1,
                             persistreco=True):

    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["proton", "kaon", "kaon", "pion"]
    CriteriaParticles = [
        CriteriaProton, CriteriaKaon, CriteriaKaon, CriteriaPion
    ]

    Omegac02pkkpi = make_charm4hadrons(
        name="Omegac02pkkpi",
        process="spruce",
        pvs=pvs,
        decay="[Omega_c0 -> p+ K- K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        mass=__MASS_Omegac0,
        masswindow=__MASSWin_Omegac0,
        max_sdoca=2 * mm,
        max_vchi2pdof=25,
        allchild_ptcut=0 * GeV,
        twochild_ptcut=0 * GeV,
        onechild_ptcut=0 * GeV)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Omegac02pkkpi],
        hlt2_filter_code='Hlt2IFT_SMOG2Omegac0TopKKPiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Hiddencharm4pi_spruceline(name='SpruceIFT_SMOG2ccTo4Pi',
                              prescale=1,
                              persistreco=True):

    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["pion", "pion", "pion", "pion"]
    CriteriaParticles = [
        CriteriaPion, CriteriaPion, CriteriaPion, CriteriaPion
    ]

    cc2pipipipi = make_charm4hadrons(
        name="cc2pipipipi",
        process="spruce",
        pvs=pvs,
        decay='eta_c(1S) -> pi- pi- pi+ pi+',
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=2.0 * mm,
        max_vchi2pdof=25.,
        mass=3300 * MeV,
        masswindow=500 * MeV,
        allchild_ptcut=0.25 * GeV,
        twochild_ptcut=0.25 * GeV,
        onechild_ptcut=0.25 * GeV)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [cc2pipipipi],
        hlt2_filter_code='Hlt2IFT_SMOG2ccTo4PiDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Hiddencharm2pi2k_spruceline(name="SpruceIFT_SMOG2ccTo2Pi2K",
                                prescale=1,
                                persistreco=True):

    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "kaon", "pion", "pion"]
    CriteriaParticles = [
        CriteriaKaon, CriteriaKaon, CriteriaPion, CriteriaPion
    ]

    cc2kkpipi = make_charm4hadrons(
        name="cc2kkpipi",
        process="spruce",
        pvs=pvs,
        decay='chi_c1(1P) -> K- K+ pi- pi+',
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=2.0 * mm,
        max_vchi2pdof=25.,
        mass=__MASS_CHIC0,
        masswindow=600 * MeV,
        allchild_ptcut=0.25 * GeV,
        twochild_ptcut=0.25 * GeV,
        onechild_ptcut=0.25 * GeV)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [cc2kkpipi],
        hlt2_filter_code='Hlt2IFT_SMOG2ccTo2Pi2KDecision',
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(sprucing_lines)
@configurable
def Hiddencharm4k_spruceline(name="SpruceIFT_SMOG2ccTo4K",
                             prescale=1,
                             persistreco=True):

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=10,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5)
    Particles = ["kaon", "kaon", "kaon", "kaon"]
    CriteriaParticles = [
        CriteriaKaon, CriteriaKaon, CriteriaKaon, CriteriaKaon
    ]

    cc2kkkk = make_charm4hadrons(
        name="cc2kkkk",
        process="spruce",
        pvs=pvs,
        decay='chi_c1(1P) -> K- K- K+ K+',
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=2.0 * mm,
        max_vchi2pdof=25,
        mass=__MASS_CHIC0,
        masswindow=600 * MeV,
        allchild_ptcut=0.25 * GeV,
        twochild_ptcut=0.25 * GeV,
        onechild_ptcut=0.25 * GeV)

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [cc2kkkk],
        hlt2_filter_code='Hlt2IFT_SMOG2ccTo4KDecision',
        prescale=prescale,
        persistreco=persistreco)

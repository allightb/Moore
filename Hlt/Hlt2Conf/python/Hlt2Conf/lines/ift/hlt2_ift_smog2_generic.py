###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Generic SMOG2 HLT2 high pt lines: high pt single hadron, generic two body, 2-body V0
"""

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from Hlt2Conf.standard_particles import make_has_rich_long_pions, make_long_pions
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_common_particles
from Hlt2Conf.lines.ift.builders.smog2_generic_builders import make_smog2_generic_2body, make_smog2_generic_3body
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import (
    make_smog2_L02ppi_ll_line, make_smog2_L02ppi_dd_line,
    make_smog2_ks2pipi_ll_line, make_smog2_ks2pipi_dd_line)
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ns, picosecond

PROCESS = "hlt2"
all_lines = {}


@register_line_builder(all_lines)
@configurable
def smog2_singletrack_highpt_line(
        name="Hlt2IFT_SMOG2SingleTrackHighPT",
        prescale=0.1,
        persistreco=True,
        min_pt=4 * GeV,
):
    """
    Line requiring one single track with pt>2.GeV comming from the SMOG2 region
    """
    pvs = make_pvs

    highp_track = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=5,
        min_p=0 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
        pvs=make_pvs,
        min_bpvipchi2=None,
        min_bpvip=None,
        max_bpvipchi2=50,
        max_bpvip=None,
        pvinsmog2=True,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [highp_track],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_singletrack_veryhighpt_line(
        name="Hlt2IFT_SMOG2SingleTrackVeryHighPT",
        prescale=1,
        persistreco=True,
        min_pt=6 * GeV,
):
    """
    Line requiring one single track with pt>4GeV comming from the SMOG2 region
    """
    pvs = make_pvs

    veryhighp_track = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=5,
        min_p=0 * GeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
        pvs=make_pvs,
        min_bpvipchi2=None,
        min_bpvip=None,
        max_bpvipchi2=50,
        max_bpvip=None,
        pvinsmog2=True,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [veryhighp_track],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_generic2bodydetached_line(
        name="Hlt2IFT_SMOG2Detached2Body",
        prescale=1,
        persistreco=True,
):
    """
    Detached generic 2-body decay coming from the SMOG2 region
    """
    pvs = make_pvs
    generic_particle = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=3,
        min_p=5000 * MeV,
        min_pt=1 * GeV,
        pvs=make_pvs,
    )

    generic_2body = make_smog2_generic_2body(
        pvs,
        generic_particle,
        generic_particle,
        name="generic_2body_smog2",
        descriptor="B0 -> pi- pi+",
        apt_min=0 * GeV,
        maxsdoca=0.2 * mm,
        vchi2pdof_max=25,
        bpvvdchi2_min=40,
        single_pt_min=2 * GeV,
        mipchi2_min=20,
        sv_insmog=True,
        minmass=3.5 * GeV,
        min_bpvltime=1 * picosecond,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [generic_2body],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_generic3bodydetached_line(
        name="Hlt2IFT_SMOG2Detached3Body",
        prescale=1,
        persistreco=True,
):
    """
    Detached generic 2-body decay coming from the SMOG2 region
    """
    pvs = make_pvs

    generic_particle = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=3,
        min_p=5000 * MeV,
        min_pt=800 * MeV,
        pvs=make_pvs,
    )

    generic_3body = make_smog2_generic_3body(
        pvs,
        generic_particle,
        generic_particle,
        generic_particle,
        name="generic_3body_smog2",
        descriptor="B0 -> pi+ pi+ pi-",
        apt_min=0 * GeV,
        maxsdoca=0.2 * mm,
        vchi2pdof_max=25,
        bpvvdchi2_min=50,
        single_pt_min=2 * GeV,
        mipchi2_min=20,
        sv_insmog=True,
        minmass=3.5 * GeV,
        min_bpvltime=1 * picosecond,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [generic_3body],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_diks_llll_line(
        name="Hlt2IFT_SMOG2DiKsLLLL",
        prescale=1,
        persistreco=True,
):
    """
    Two LL KS line
    """
    pvs = make_pvs
    ks1 = make_smog2_ks2pipi_ll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=800 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_ll_{hash}",
    )
    ks2 = make_smog2_ks2pipi_ll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=800 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_ll_{hash}",
    )

    diks = make_smog2_generic_2body(
        pvs,
        ks1,
        ks2,
        name="smog2_diks_llll_{hash}",
        descriptor="J/psi(1S) -> KS0 KS0",
        apt_min=0 * MeV,
        maxsdoca=0.5 * mm,
        vchi2pdof_max=25,
        sv_insmog=True,
        minmass=1000 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [diks],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_diks_lldd_line(
        name="Hlt2IFT_SMOG2DiKsLLDD",
        prescale=1,
        persistreco=True,
):
    """
    One LL KS + one DD KS line
    """
    pvs = make_pvs
    ks1 = make_smog2_ks2pipi_ll_line(
        pvs=make_pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_ll_{hash}",
    )
    ks2 = make_smog2_ks2pipi_dd_line(
        pvs=pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_dd_{hash}",
    )

    diks = make_smog2_generic_2body(
        pvs,
        ks1,
        ks2,
        name="smog2_diks_llll_{hash}",
        descriptor="J/psi(1S) -> KS0 KS0",
        apt_min=0 * MeV,
        maxsdoca=2 * mm,
        vchi2pdof_max=25,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [diks],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_diks_dddd_line(
        name="Hlt2IFT_SMOG2DiKsDDDD",
        prescale=1,
        persistreco=True,
):
    """
    Two DD KS  line
    """
    pvs = make_pvs
    ks1 = make_smog2_ks2pipi_dd_line(
        pvs=pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_dd_{hash}",
    )
    ks2 = make_smog2_ks2pipi_dd_line(
        pvs=pvs,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=20,
        apt_min=0 * MeV,
        vchi2pdof_max=30.0,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=None,
        lambda_veto_window=9 * MeV,
        bestpv_in_smog2=False,
        process="hlt2",
        name="smog2_ks2pipi_dd_{hash}",
    )

    diks = make_smog2_generic_2body(
        pvs,
        ks1,
        ks2,
        name="smog2_diks_llll_{hash}",
        descriptor="J/psi(1S) -> KS0 KS0",
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [diks],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_dilambda_llll_line(
        name="Hlt2IFT_SMOG2DiLambdaLLLL",
        prescale=1,
        persistreco=True,
):
    """
    Two LL Lambda line
    """
    pvs = make_pvs
    lambda1 = make_smog2_L02ppi_ll_line(
        name='smog2_L02ppi_LL_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        pvs=pvs,
        bestpv_in_smog2=False,
    )
    lambda2 = make_smog2_L02ppi_ll_line(
        name='smog2_L02ppi_LL_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        pvs=pvs,
        bestpv_in_smog2=False,
    )

    dilambda = make_smog2_generic_2body(
        pvs,
        lambda1,
        lambda2,
        name="smog2_dilambda_llll_{hash}",
        descriptor="J/psi(1S) -> Lambda0 Lambda~0",
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [dilambda],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_dilambda_lldd_line(
        name="Hlt2IFT_SMOG2DiLambdaLLDD",
        prescale=1,
        persistreco=True,
):
    """
    Two LL-DD Lambda line
    """
    pvs = make_pvs
    lambda1 = make_smog2_L02ppi_ll_line(
        name='smog2_L02ppi_LL_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        pvs=pvs,
        bestpv_in_smog2=False,
    )
    lambda2 = make_smog2_L02ppi_dd_line(
        name='smog2_L02ppi_DD_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        pvs=pvs,
        bestpv_in_smog2=False,
    )

    dilambda = make_smog2_generic_2body(
        pvs,
        lambda1,
        lambda2,
        name="smog2_dilambda_lldd_{hash}",
        descriptor="J/psi(1S) -> Lambda0 Lambda~0",
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [dilambda],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_dilambda_dddd_line(
        name="Hlt2IFT_SMOG2DiLambdaDDDD",
        prescale=1,
        persistreco=True,
):
    """
    Two DD Lambda line
    """
    pvs = make_pvs
    lambda1 = make_smog2_L02ppi_dd_line(
        name='smog2_L02ppi_DD_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        pvs=pvs,
        bestpv_in_smog2=False,
    )
    lambda2 = make_smog2_L02ppi_dd_line(
        name='smog2_L02ppi_DD_{hash}',
        parent_bpvipchi2_max=None,
        vchi2pdof_max=30,
        min_bpvipchi2=20,
        ks_veto_window=20 * MeV,
        max_trchi2dof=5,
        bpvvdz_min=400,
        bpvvdz_max=2800,
        pvs=pvs,
        bestpv_in_smog2=False,
    )

    dilambda = make_smog2_generic_2body(
        pvs,
        lambda1,
        lambda2,
        name="smog2_dilambda_dddd_{hash}",
        descriptor="J/psi(1S) -> Lambda0 Lambda~0",
        apt_min=0 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [dilambda],
        prescale=prescale,
        persistreco=persistreco,
    )

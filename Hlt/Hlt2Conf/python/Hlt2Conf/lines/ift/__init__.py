###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all ift HLT2 and sprucing lines
"""

from . import hlt2_ift_femtoscopy
from . import hlt2_ift_smog2
from . import hlt2_ift_smog2_generic
from . import hlt2_ift_smog2_charm
from . import hlt2_ift_smog2_chargedPID
from . import hlt2_ift_smog2_muons
from . import hlt2_ift_smog2_omegas
from . import hlt2_ift_smog2_xis
from . import hlt2_ift_smog2_trackingeff
from . import hlt2_ift_smog2_trackingeff_Ks

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(hlt2_ift_femtoscopy.all_lines)
all_lines.update(hlt2_ift_smog2.all_lines)
all_lines.update(hlt2_ift_smog2_generic.all_lines)
all_lines.update(hlt2_ift_smog2_charm.all_lines)
all_lines.update(hlt2_ift_smog2_chargedPID.all_lines)
all_lines.update(hlt2_ift_smog2_muons.all_lines)
all_lines.update(hlt2_ift_smog2_omegas.all_lines)
all_lines.update(hlt2_ift_smog2_xis.all_lines)
all_lines.update(hlt2_ift_smog2_trackingeff.all_lines)
all_lines.update(hlt2_ift_smog2_trackingeff_Ks.all_lines)

ift_full_lines = {}
ift_full_lines.update(hlt2_ift_smog2.all_lines)
ift_full_lines.update(hlt2_ift_smog2_generic.all_lines)
ift_full_lines.update(hlt2_ift_smog2_charm.all_lines)
ift_full_lines.update(hlt2_ift_smog2_chargedPID.all_lines)
ift_full_lines.update(hlt2_ift_smog2_muons.all_lines)
ift_full_lines.update(hlt2_ift_smog2_omegas.all_lines)
ift_full_lines.update(hlt2_ift_smog2_xis.all_lines)
ift_full_lines.update(hlt2_ift_smog2_trackingeff.all_lines)
ift_full_lines.update(hlt2_ift_smog2_trackingeff_Ks.all_lines)

ift_turbo_lines = {}
ift_turbo_lines.update(hlt2_ift_femtoscopy.all_lines)

##### Sprucing lines

from . import spruce_ift_smog2
#from . import spruce_ift_generic # to be added
from . import spruce_ift_smog2_charm
from . import spruce_ift_smog2_chargedPID
from . import spruce_ift_smog2_muons
from . import spruce_ift_smog2_xis
from . import spruce_ift_smog2_omegas

# provide "sprucing_lines" for correct registration by the overall sprucing lines module
sprucing_lines = {}
sprucing_lines.update(spruce_ift_smog2.sprucing_lines)
#sprucing_lines.update(spruce_ift_smog2_generic.sprucing_lines) # to be added
sprucing_lines.update(spruce_ift_smog2_charm.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_chargedPID.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_muons.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_xis.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_omegas.sprucing_lines)

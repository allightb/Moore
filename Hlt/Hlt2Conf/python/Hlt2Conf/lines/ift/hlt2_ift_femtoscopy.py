###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 lines for studying femtoscopic correlations of protons and hyperons.
"""

import Functors as F

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, micrometer as um
from PyConf import configurable

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs, require_gec

from Hlt2Conf.standard_particles import (
    make_long_pions, make_has_rich_long_kaons, make_has_rich_long_protons,
    make_LambdaLL)
from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter,
                                      ParticleContainersMerger)

from math import sqrt

_LAMBDA_M = 1115.683 * MeV
_XIM_M = 1321.71 * MeV
_OMEGAM_M = 1672.45 * MeV
_PROTON_M = 938.272 * MeV

# Note that this will cause an error when used with old HLT1-filtered MC.
_hlt1_filter_lambda = [  #"Hlt1L02PPiDecision",
    "Hlt1SMOG2L02PPiDecision"
]
# Filter for lines including Xis and Omegas.
_hlt1_filter_xiomega = ["Hlt1XiOmegaLLLDecision"]


# Our goal is to study the highest multiplicity interactions, so we'd like to
# have the GEC off.
def femtoscopy_prefilters(require_GEC=False):
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


@configurable
def _make_kaons_from_omega(pt_min=180 * MeV,
                           p_min=3 * GeV,
                           mipchi2_min=9.,
                           dllk_min=-2.):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                          F.PID_K > dllk_min)))


@configurable
def _make_pions_from_xi(pt_min=100 * MeV, p_min=1 * GeV, mipchi2_min=8.0):
    pvs = make_pvs()
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min,
                                         Vertices=pvs))))


@configurable
def _make_prompt_protons(pt_min=200 * MeV,
                         p_min=5 * GeV,
                         mipchi2_max=4.0,
                         dllp_min=5.,
                         dllp_m_dllk_min=5.):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(F.PT > pt_min, F.P > p_min,
                          F.BPVIPCHI2(Vertices=pvs) < mipchi2_max,
                          F.PID_P > dllp_min,
                          (F.PID_P - F.PID_K) > dllp_m_dllk_min)))


def _make_ll_lambdas_from_hyperons():
    pvs = make_pvs()
    code = F.require_all(F.MASS < 1165 * MeV, F.PT > 550 * MeV, F.CHI2DOF < 6.,
                         F.BPVVDZ(pvs) > 8. * mm,
                         F.BPVFDCHI2(pvs) > 480.)
    return ParticleFilter(make_LambdaLL(), F.FILTER(code))


def _make_lll_xis():
    pvs = make_pvs()
    return ParticleCombiner(
        [_make_ll_lambdas_from_hyperons(),
         _make_pions_from_xi()],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name='Femtoscopy_Xim_LLL_{hash}',
        CombinationCut=F.require_all(F.MASS < 1400. * MeV,
                                     F.MAXDOCACUT(150. * um),
                                     F.PT > 500. * MeV),
        CompositeCut=F.require_all(F.MASS < 1380. * MeV, F.PT > 600. * MeV,
                                   F.CHI2DOF < 9.,
                                   F.CHILD(1, F.END_VZ) - F.END_VZ > 8. * mm,
                                   F.BPVVDZ(pvs) > 4. * mm,
                                   F.BPVFDCHI2(pvs) > 16.))


def _make_lll_omegas():
    pvs = make_pvs()
    return ParticleCombiner(
        [_make_ll_lambdas_from_hyperons(),
         _make_kaons_from_omega()],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name='Femtoscopy_Omegam_LLL_{hash}',
        CombinationCut=F.require_all(
            F.MASS < 1750. * MeV,
            F.MAXDOCACUT(150 * um),
            F.PT > 550. * MeV,
        ),
        CompositeCut=F.require_all(F.MASS < 1730. * MeV, F.PT > 650. * MeV,
                                   F.CHI2DOF < 9.,
                                   F.CHILD(1, F.END_VZ) - F.END_VZ > 8. * mm,
                                   F.BPVVDZ(pvs) > 2. * mm,
                                   F.BPVFDCHI2(pvs) > 12.))


def _make_correlated_pair(
        name,
        decay_descriptor,
        inputs,
        m1,
        m2,
        sumpt_min=1 * GeV,  # 2 * GeV, 
        pt_min=1 * GeV,  # 2 * GeV,
        doca_max=1 * mm,
        vtx_chi2ndof_max=10.,
        fdchi2_max=10.,
        k_max=1 * GeV):
    pvs = make_pvs()
    msq_max = 2 * k_max**2 + m1**2 + m2**2 + 2 * sqrt(
        (k_max**2 + m1**2) * (k_max**2 + m2**2))
    m_max = sqrt(msq_max)
    combo_cut = F.require_all(
        F.SUM(F.PT) > sumpt_min, F.MASS < m_max + 50. * MeV,
        F.MAXDOCACUT(doca_max))
    composite_cut = F.require_all(F.PT > pt_min, F.MASS < m_max,
                                  F.CHI2DOF < vtx_chi2ndof_max,
                                  F.BPVFDCHI2(pvs) < fdchi2_max)
    return ParticleCombiner(
        name=name,
        Inputs=inputs,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combo_cut,
        CompositeCut=composite_cut)


def make_correlated_pair(name, descriptors, inputs, **decay_arguments):
    assert len(descriptors) > 0
    container = []
    for descriptor in descriptors:
        container.append(
            _make_correlated_pair(
                name=name,
                decay_descriptor=descriptor,
                inputs=inputs,
                **decay_arguments))
    return ParticleContainersMerger(container, name=name)


all_lines = {}

# Inclusive single particle lines for producing mixed-event backgrounds. Expect
# to be heavily prescaled.

# TODO: For now we assume we can get enough protons from minimum bias.


# TODO: Can this be accomplished in minimum bias?
@register_line_builder(all_lines)
@configurable
def femto_inclusive_lambda_line(name="Hlt2IFT_Femtoscopy_InclLambdaLL",
                                prescale=1.e-4,
                                persistreco=True):
    lambdas = _make_ll_lambdas_from_hyperons()
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lambdas],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_inclusive_xi_lll_line(name="Hlt2IFT_Femtoscopy_InclXiLLL",
                                prescale=1.e-3,
                                persistreco=True):
    xis = _make_lll_xis()
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xis],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_inclusive_omega_lll_line(name="Hlt2IFT_Femtoscopy_InclOmegaLLL",
                                   prescale=1.e-2,
                                   persistreco=True):
    omegas = _make_lll_omegas()
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omegas],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


# Femtoscopic pair lines. These should hopefully be only minimally prescaled.
# TODO: For now we assume we can get enough p-p correlated pairs from minimum bias.

###################################################################
# light baryon/hyperon pairs with p, Lambda, Xi, Omega
###################################################################
_LOW_K_MAX = 100 * MeV


def make_lambda_p_pairs(k_max):
    lambdas = _make_ll_lambdas_from_hyperons()
    protons = _make_prompt_protons()
    return make_correlated_pair(
        name='Femtoscopy_LambdaP_{hash}',
        descriptors=[
            "[J/psi(1S) -> Lambda0 p+]cc", "[J/psi(1S) -> Lambda0 p~-]cc"
        ],
        inputs=[lambdas, protons],
        m1=_LAMBDA_M,
        m2=_PROTON_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_lambda_p_line(name="Hlt2IFT_Femtoscopy_LambdaP",
                        k_max=1. * GeV,
                        prescale=1.e-3,
                        persistreco=True):
    lambda_p_pairs = make_lambda_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lambda_p_pairs],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_lambda_p_lowk_line(name="Hlt2IFT_Femtoscopy_LambdaP_lowK",
                             k_max=_LOW_K_MAX,
                             prescale=1.,
                             persistreco=True):
    lambda_p_pairs = make_lambda_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lambda_p_pairs],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


def make_lambda_lambda_pairs(k_max):
    lambdas = _make_ll_lambdas_from_hyperons()
    return make_correlated_pair(
        name='Femtoscopy_LambdaLambda_{hash}',
        descriptors=[
            "[J/psi(1S) -> Lambda0 Lambda0]cc",
            "[J/psi(1S) -> Lambda0 Lambda~0]cc"
        ],
        inputs=[lambdas, lambdas],
        m1=_LAMBDA_M,
        m2=_LAMBDA_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_lambda_lambda_line(name="Hlt2IFT_Femtoscopy_LambdaLambda",
                             k_max=1. * GeV,
                             prescale=0.1,
                             persistreco=True):
    lambda_lambda_pairs = make_lambda_lambda_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lambda_lambda_pairs],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_lambda_lambda_lowk_line(name="Hlt2IFT_Femtoscopy_LambdaLambda_lowK",
                                  k_max=_LOW_K_MAX,
                                  prescale=1.,
                                  persistreco=True):
    lambda_lambda_pairs = make_lambda_lambda_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lambda_lambda_pairs],
        hlt1_filter_code=_hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


def make_xi_p_pairs(k_max):
    xis = _make_lll_xis()
    protons = _make_prompt_protons()
    return make_correlated_pair(
        name='Femtoscopy_XiP_{hash}',
        descriptors=["[J/psi(1S) -> Xi- p+]cc", "[J/psi(1S) -> Xi- p~-]cc"],
        inputs=[xis, protons],
        m1=_XIM_M,
        m2=_PROTON_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_xim_p_line(name="Hlt2IFT_Femtoscopy_XiP",
                     k_max=1. * GeV,
                     prescale=0.1,
                     persistreco=True):
    xi_p_pairs = make_xi_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xi_p_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xim_p_lowk_line(name="Hlt2IFT_Femtoscopy_XiP_lowK",
                          k_max=_LOW_K_MAX,
                          prescale=1.,
                          persistreco=True):
    xi_p_pairs = make_xi_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xi_p_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


def make_xi_lam_pairs(k_max):
    xis = _make_lll_xis()
    lambdas = _make_ll_lambdas_from_hyperons()
    return make_correlated_pair(
        name='Femtoscopy_XiLambda_{hash}',
        descriptors=[
            "[J/psi(1S) -> Xi- Lambda0]cc", "[J/psi(1S) -> Xi- Lambda~0]cc"
        ],
        inputs=[xis, lambdas],
        m1=_XIM_M,
        m2=_LAMBDA_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_xim_lambda_line(name="Hlt2IFT_Femtoscopy_XiLambda",
                          k_max=1. * GeV,
                          prescale=1.,
                          persistreco=True):
    xi_lam_pairs = make_xi_lam_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xi_lam_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xim_lambda_lowk_line(name="Hlt2IFT_Femtoscopy_XiLambda_lowK",
                               k_max=_LOW_K_MAX,
                               prescale=1.,
                               persistreco=True):
    xi_lam_pairs = make_xi_lam_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xi_lam_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xim_xim_line(name="Hlt2IFT_Femtoscopy_XiXi",
                       k_max=1. * GeV,
                       prescale=1.,
                       persistreco=True):
    xis = _make_lll_xis()
    xi_pairs = make_correlated_pair(
        name='Femtoscopy_XiXi_{hash}',
        descriptors=["[J/psi(1S) -> Xi- Xi-]cc", "[J/psi(1S) -> Xi- Xi~+]cc"],
        inputs=[xis, xis],
        m1=_XIM_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xis, xi_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


def make_omega_p_pairs(k_max):
    omegas = _make_lll_omegas()
    protons = _make_prompt_protons()
    return make_correlated_pair(
        name='Femtoscopy_OmegaP_{hash}',
        descriptors=[
            "[J/psi(1S) -> Omega- p+]cc", "[J/psi(1S) -> Omega- p~-]cc"
        ],
        inputs=[omegas, protons],
        m1=_OMEGAM_M,
        m2=_PROTON_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_omegam_p_line(name="Hlt2IFT_Femtoscopy_OmegaP",
                        k_max=1. * GeV,
                        prescale=1.,
                        persistreco=True):
    omega_p_pairs = make_omega_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omega_p_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_omegam_p_lowk_line(name="Hlt2IFT_Femtoscopy_OmegaP_lowK",
                             k_max=_LOW_K_MAX,
                             prescale=1.,
                             persistreco=True):
    omega_p_pairs = make_omega_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omega_p_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_omegam_lambda_line(name="Hlt2IFT_Femtoscopy_OmegaLambda",
                             k_max=1. * GeV,
                             prescale=1.,
                             persistreco=True):
    omegas = _make_lll_omegas()
    lambdas = _make_ll_lambdas_from_hyperons()
    omega_lam_pairs = make_correlated_pair(
        name='Femtoscopy_OmegaLambda_{hash}',
        descriptors=[
            "[J/psi(1S) -> Omega- Lambda0]cc",
            "[J/psi(1S) -> Omega- Lambda~0]cc"
        ],
        inputs=[omegas, lambdas],
        m1=_OMEGAM_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omegas, lambdas, omega_lam_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_omegam_xim_line(name="Hlt2IFT_Femtoscopy_OmegaXi",
                          k_max=1. * GeV,
                          prescale=1.,
                          persistreco=True):
    omegas = _make_lll_omegas()
    xis = _make_lll_xis()
    omega_xi_pairs = make_correlated_pair(
        name='Femtoscopy_OmegaXi_{hash}',
        descriptors=[
            "[J/psi(1S) -> Omega- Xi-]cc", "[J/psi(1S) -> Omega- Xi~+]cc"
        ],
        inputs=[omegas, xis],
        m1=_OMEGAM_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omegas, xis, omega_xi_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_omegam_omegam_line(name="Hlt2IFT_Femtoscopy_OmegaOmega",
                             k_max=1. * GeV,
                             prescale=1.,
                             persistreco=True):
    omegas = _make_lll_omegas()
    omega_pairs = make_correlated_pair(
        name='Femtoscopy_OmegaOmega_{hash}',
        descriptors=[
            "[J/psi(1S) -> Omega- Omega-]cc", "[J/psi(1S) -> Omega- Omega~+]cc"
        ],
        inputs=[omegas, omegas],
        m1=_OMEGAM_M,
        m2=_OMEGAM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [omegas, omega_pairs],
        hlt1_filter_code=_hlt1_filter_xiomega + _hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco)


#################################################################################################
# charm - light baryon/hyperon pairs
# charm = Lambda_c, Xi_c, (D0,D+,Ds ?)
# h = p, Lambda, Xi, Omega
###################################################################
from Hlt2Conf.lines.charm.prod_xsec import _xsec_make_dzeros_Kpi as _make_dz, \
    _xsec_make_dplus_Kpipi as _make_dp, \
    _xsec_make_lc_or_xicp_pKpi as _make_lc_or_xicp, \
    _xsec_make_xic0_pkkpi as _make_xicz


def _make_lc():
    return _make_lc_or_xicp(m_min=2211.0 * MeV, m_max=2362.0 * MeV)


def _make_xicp():
    return _make_lc_or_xicp(m_min=2392.0 * MeV, m_max=2543.0 * MeV)


_LAMBDAC_M = 2286.46 * MeV
_XICP_M = 2467.71 * MeV
_XICZ_M = 2470.44 * MeV
_DZ_M = 1864.84 * MeV
_DP_M = 1869.66 * MeV
_DS_M = 1968.35 * MeV


@register_line_builder(all_lines)
@configurable
def femto_lc_p_line(name="Hlt2IFT_Femtoscopy_LcP",
                    k_max=1. * GeV,
                    prescale=1.,
                    persistreco=True):
    lc = _make_lc()
    protons = _make_prompt_protons()
    pairs = make_correlated_pair(
        name='Femtoscopy_LcP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Lambda_c+ p+]cc", "[Sigma_c*+ -> Lambda_c+ p~-]cc"
        ],
        inputs=[lc, protons],
        m1=_LAMBDAC_M,
        m2=_PROTON_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lc, protons, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_lc_lambda_line(name="Hlt2IFT_Femtoscopy_LcLambda",
                         k_max=1. * GeV,
                         prescale=1.,
                         persistreco=True):
    lc = _make_lc()
    lambdas = _make_ll_lambdas_from_hyperons()
    pairs = make_correlated_pair(
        name='Femtoscopy_LcP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Lambda_c+ Lambda0]cc",
            "[Sigma_c*+ -> Lambda_c+ Lambda~0]cc"
        ],
        inputs=[lc, lambdas],
        m1=_LAMBDAC_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lc, lambdas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_lc_xi_line(name="Hlt2IFT_Femtoscopy_LcXi",
                     k_max=1. * GeV,
                     prescale=1.,
                     persistreco=True):
    lc = _make_lc()
    xis = _make_lll_xis()
    pairs = make_correlated_pair(
        name='Femtoscopy_LcXi_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Lambda_c+ Xi-]cc", "[Sigma_c*+ -> Lambda_c+ Xi~+]cc"
        ],
        inputs=[lc, xis],
        m1=_LAMBDAC_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lc, xis, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_lc_omega_line(name="Hlt2IFT_Femtoscopy_LcOmega",
                        k_max=1. * GeV,
                        prescale=1.,
                        persistreco=True):
    lc = _make_lc()
    omegas = _make_lll_omegas()
    pairs = make_correlated_pair(
        name='Femtoscopy_LcOmega_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Lambda_c+ Omega-]cc",
            "[Sigma_c*+ -> Lambda_c+ Omega~+]cc"
        ],
        inputs=[lc, omegas],
        m1=_LAMBDAC_M,
        m2=_OMEGAM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [lc, omegas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicp_p_line(name="Hlt2IFT_Femtoscopy_XicpP",
                      k_max=1. * GeV,
                      prescale=1.,
                      persistreco=True):
    xicp = _make_xicp()
    protons = _make_prompt_protons()
    pairs = make_correlated_pair(
        name='Femtoscopy_XicpP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c+ p+]cc", "[Sigma_c*+ -> Xi_c+ p~-]cc"
        ],
        inputs=[xicp, protons],
        m1=_XICP_M,
        m2=_PROTON_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicp, protons, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicp_lambda_line(name="Hlt2IFT_Femtoscopy_XicpLambda",
                           k_max=1. * GeV,
                           prescale=1.,
                           persistreco=True):
    xicp = _make_xicp()
    lambdas = _make_ll_lambdas_from_hyperons()
    pairs = make_correlated_pair(
        name='Femtoscopy_XicpP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c+ Lambda0]cc", "[Sigma_c*+ -> Xi_c+ Lambda~0]cc"
        ],
        inputs=[xicp, lambdas],
        m1=_XICP_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicp, lambdas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicp_xi_line(name="Hlt2IFT_Femtoscopy_XicpXi",
                       k_max=1. * GeV,
                       prescale=1.,
                       persistreco=True):
    xicp = _make_xicp()
    xis = _make_lll_xis()
    pairs = make_correlated_pair(
        name='Femtoscopy_XicpXi_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c+ Xi-]cc", "[Sigma_c*+ -> Xi_c+ Xi~+]cc"
        ],
        inputs=[xicp, xis],
        m1=_XICP_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicp, xis, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicz_p_line(name="Hlt2IFT_Femtoscopy_XiczP",
                      k_max=1. * GeV,
                      prescale=1.,
                      persistreco=True):
    xicz = _make_xicz()
    protons = _make_prompt_protons()
    pairs = make_correlated_pair(
        name='Femtoscopy_XiczP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c0 p+]cc", "[Sigma_c*+ -> Xi_c0 p~-]cc"
        ],
        inputs=[xicz, protons],
        m1=_XICZ_M,
        m2=_PROTON_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicz, protons, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicz_lambda_line(name="Hlt2IFT_Femtoscopy_XiczLambda",
                           k_max=1. * GeV,
                           prescale=1.,
                           persistreco=True):
    xicz = _make_xicz()
    lambdas = _make_ll_lambdas_from_hyperons()
    pairs = make_correlated_pair(
        name='Femtoscopy_XiczP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c0 Lambda0]cc", "[Sigma_c*+ -> Xi_c0 Lambda~0]cc"
        ],
        inputs=[xicz, lambdas],
        m1=_XICZ_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicz, lambdas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_xicz_xi_line(name="Hlt2IFT_Femtoscopy_XiczXi",
                       k_max=1. * GeV,
                       prescale=1.,
                       persistreco=True):
    xicz = _make_xicz()
    xis = _make_lll_xis()
    pairs = make_correlated_pair(
        name='Femtoscopy_XiczXi_{hash}',
        descriptors=[
            "[Sigma_c*+ -> Xi_c0 Xi-]cc", "[Sigma_c*+ -> Xi_c0 Xi~+]cc"
        ],
        inputs=[xicz, xis],
        m1=_XICZ_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [xicz, xis, pairs],
        prescale=prescale,
        persistreco=persistreco)


def make_dz_p_pairs(k_max):
    dz = _make_dz()
    protons = _make_prompt_protons()
    return make_correlated_pair(
        name='Femtoscopy_DzP_{hash}',
        descriptors=["[Sigma_c*+ -> D0 p+]cc", "[Sigma_c*+ -> D0 p~-]cc"],
        inputs=[dz, protons],
        m1=_DZ_M,
        m2=_PROTON_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_dz_p_line(name="Hlt2IFT_Femtoscopy_DzP",
                    k_max=1. * GeV,
                    prescale=0.1,
                    persistreco=True):
    pairs = make_dz_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dz_p_lowk_line(name="Hlt2IFT_Femtoscopy_DzP_lowK",
                         k_max=_LOW_K_MAX,
                         prescale=1.,
                         persistreco=True):
    pairs = make_dz_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dz_lambda_line(name="Hlt2IFT_Femtoscopy_DzLambda",
                         k_max=1. * GeV,
                         prescale=1.,
                         persistreco=True):
    dz = _make_dz()
    lambdas = _make_ll_lambdas_from_hyperons()
    pairs = make_correlated_pair(
        name='Femtoscopy_DzP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> D0 Lambda0]cc", "[Sigma_c*+ -> D0 Lambda~0]cc"
        ],
        inputs=[dz, lambdas],
        m1=_DZ_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dz, lambdas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dz_xi_line(name="Hlt2IFT_Femtoscopy_DzXi",
                     k_max=1. * GeV,
                     prescale=1.,
                     persistreco=True):
    dz = _make_dz()
    xis = _make_lll_xis()
    pairs = make_correlated_pair(
        name='Femtoscopy_DzXi_{hash}',
        descriptors=["[Sigma_c*+ -> D0 Xi-]cc", "[Sigma_c*+ -> D0 Xi~+]cc"],
        inputs=[dz, xis],
        m1=_DZ_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dz, xis, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dz_omega_line(name="Hlt2IFT_Femtoscopy_DzOmega",
                        k_max=1. * GeV,
                        prescale=1.,
                        persistreco=True):
    dz = _make_dz()
    omegas = _make_lll_omegas()
    pairs = make_correlated_pair(
        name='Femtoscopy_DzOmega_{hash}',
        descriptors=[
            "[Sigma_c*+ -> D0 Omega-]cc", "[Sigma_c*+ -> D0 Omega~+]cc"
        ],
        inputs=[dz, omegas],
        m1=_DZ_M,
        m2=_OMEGAM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dz, omegas, pairs],
        prescale=prescale,
        persistreco=persistreco)


def make_dp_p_pairs(k_max):
    dp = _make_dp()
    protons = _make_prompt_protons()
    return make_correlated_pair(
        name='Femtoscopy_DpP_{hash}',
        descriptors=["[Sigma_c*+ -> D+ p+]cc", "[Sigma_c*+ -> D+ p~-]cc"],
        inputs=[dp, protons],
        m1=_DP_M,
        m2=_PROTON_M,
        k_max=k_max)


@register_line_builder(all_lines)
@configurable
def femto_dp_p_line(name="Hlt2IFT_Femtoscopy_DpP",
                    k_max=1. * GeV,
                    prescale=0.1,
                    persistreco=True):
    pairs = make_dp_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dp_p_lowk_line(name="Hlt2IFT_Femtoscopy_DpP_lowK",
                         k_max=_LOW_K_MAX,
                         prescale=1.,
                         persistreco=True):
    pairs = make_dp_p_pairs(k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dp_lambda_line(name="Hlt2IFT_Femtoscopy_DpLambda",
                         k_max=1. * GeV,
                         prescale=1.,
                         persistreco=True):
    dp = _make_dp()
    lambdas = _make_ll_lambdas_from_hyperons()
    pairs = make_correlated_pair(
        name='Femtoscopy_DpP_{hash}',
        descriptors=[
            "[Sigma_c*+ -> D+ Lambda0]cc", "[Sigma_c*+ -> D+ Lambda~0]cc"
        ],
        inputs=[dp, lambdas],
        m1=_DP_M,
        m2=_LAMBDA_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dp, lambdas, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dp_xi_line(name="Hlt2IFT_Femtoscopy_DpXi",
                     k_max=1. * GeV,
                     prescale=1.,
                     persistreco=True):
    dp = _make_dp()
    xis = _make_lll_xis()
    pairs = make_correlated_pair(
        name='Femtoscopy_DpXi_{hash}',
        descriptors=["[Sigma_c*+ -> D+ Xi-]cc", "[Sigma_c*+ -> D+ Xi~+]cc"],
        inputs=[dp, xis],
        m1=_DP_M,
        m2=_XIM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dp, xis, pairs],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def femto_dp_omega_line(name="Hlt2IFT_Femtoscopy_DpOmega",
                        k_max=1. * GeV,
                        prescale=1.,
                        persistreco=True):
    dp = _make_dp()
    omegas = _make_lll_omegas()
    pairs = make_correlated_pair(
        name='Femtoscopy_DpOmega_{hash}',
        descriptors=[
            "[Sigma_c*+ -> D+ Omega-]cc", "[Sigma_c*+ -> D+ Omega~+]cc"
        ],
        inputs=[dp, omegas],
        m1=_DP_M,
        m2=_OMEGAM_M,
        k_max=k_max)
    return Hlt2Line(
        name,
        algs=femtoscopy_prefilters() + [dp, omegas, pairs],
        prescale=prescale,
        persistreco=persistreco)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines for charged PID calibration
"""

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import make_smog2_L02ppi_ll_line, make_smog2_ks2pipi_ll_line, make_smog2_phi2kk, make_smog2_phi2kk_calib
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import make_smog2_ks2pipi_dd_line, make_smog2_L02ppi_dd_line
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ns

PROCESS = "hlt2"
all_lines = {}

_MASSWINDOW_KS = [(497.7 - 50) * MeV, (497.7 + 50) * MeV]
_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]

_MASSWINDOW_COMB_PHI = [(1019.445 - 40) * MeV, (1019.445 + 40) * MeV]
_MASSWINDOW_VERTEX_PHI = [(1019.445 - 20) * MeV, (1019.445 + 20) * MeV]

_MASS_KS = 497.7 * MeV
_MASS_Lambda0 = 1115.683 * MeV
_MASS_PHI = 1019.445 * MeV

#################################################################
#################################################################
###################  CHARGED PID LINES  #########################
#################################################################
#################################################################


@register_line_builder(all_lines)
@configurable
def smog2_ks2pipi_ll_line(
        name="Hlt2IFT_SMOG2KS2PiPiLL",
        prescale=0.002,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=75,
        lambda_veto_window=9 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements).
    Prescaled, use low and high pT lines
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        process="hlt2",
        name="smog2_ks2pipi_ll",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_ks2pipi_ll_lowpt_line(
        name="Hlt2IFT_SMOG2KS2PiPiLLLowPT",
        prescale=0.005,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * GeV,
        apt_max=2 * GeV,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=75,
        lambda_veto_window=9 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        apt_max=apt_max,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        process="hlt2",
        name="smog2_ks2pipi_ll_lowpt",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_ks2pipi_ll_highpt_line(
        name="Hlt2IFT_SMOG2KS2PiPiLLHighPT",
        prescale=0.5,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=2 * GeV,
        apt_max=None,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=75,
        lambda_veto_window=9 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        apt_max=apt_max,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        process="hlt2",
        name="smog2_ks2pipi_ll_highpt",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_ks2pipi_dd_line(
        name="Hlt2IFT_SMOG2KS2PiPiDD",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_KS[0],
        mmax=_MASSWINDOW_KS[1],
        apt_min=0 * MeV,
        vchi2pdof_max=16.0,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=200,
        lambda_veto_window=18 * MeV,
):
    """
    SMOG2 KS -> pi pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as downstream tracks
    """
    pvs = make_pvs

    ks2pipi = make_smog2_ks2pipi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        lambda_veto_window=lambda_veto_window,
        process="hlt2",
        name="smog2_ks2pipi_dd",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [ks2pipi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_L02ppi_ll_line(
        name="Hlt2IFT_SMOG2Lambda02PPiLL",
        prescale=0.02,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=200,
        ks_veto_window=20 * MeV,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="hlt2",
        name="smog2_lambda2ppi_ll",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_L02ppi_ll_highpt_line(
        name="Hlt2IFT_SMOG2Lambda02PPiLLHighPT",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=2 * GeV,
        vchi2pdof_max=25.0,
        end_vz_max=2200 * mm,
        bpvvdchi2_min=0,
        bpvltime_min=0 * ns,
        parent_bpvipchi2_max=200,
        ks_veto_window=20 * MeV,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements). High pT line without presale for pT>1.5
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="hlt2",
        name="smog2_lambda2ppi_ll_highpt",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_L02ppi_dd_line(
        name="Hlt2IFT_SMOG2Lambda02PPiDD",
        prescale=1,
        persistreco=True,
        min_p=2 * GeV,
        max_trchi2dof=5,
        min_bpvipchi2=25,
        mmin=_MASSWINDOW_LAMBDA0[0],
        mmax=_MASSWINDOW_LAMBDA0[1],
        apt_min=0 * MeV,
        vchi2pdof_max=16.0,
        bpvvdz_min=400 * mm,
        bpvvdz_max=2800 * mm,
        bpvvdchi2_min=0,
        parent_bpvipchi2_max=None,
        ks_veto_window=40 * MeV,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as downstream tracks
    """
    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_dd_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="hlt2",
        name="smog2_lambda2ppi_dd",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_line(
        name="Hlt2IFT_SMOG2Phi2kk",
        prescale=0.01,
        persistreco=True,
        min_p=3 * GeV,
        min_pt=600 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=10,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxsdoca=0.2 * mm,
        vchi2pdof_max=16.,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) sprucing trigger line for physics analysis
    """
    name = "Hlt2IFT_SMOG2Phi2kk"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2")
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kmProbe_line(
        name="Hlt2IFT_SMOG2Phi2kk_Kmprobe",
        prescale=0.01,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=380 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxsdoca=2 * mm,
        vchi2pdof_max=16.0,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) HLT2 trigger line for PID calibration
    """
    name = "Hlt2IFT_SMOG2Phi2kk_Kmprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kpProbe_line(
        name="Hlt2IFT_SMOG2Phi2kk_Kpprobe",
        prescale=0.01,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=380 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=0 * MeV,
        maxsdoca=2 * mm,
        vchi2pdof_max=16.0,
):
    """
    SMOG2 phi(1020) -> K K (Kp probe) HLT2 trigger line for PID calibration
    """
    name = "Hlt2IFT_SMOG2Phi2kk_Kpprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kmProbe_highpt_line(
        name="Hlt2IFT_SMOG2Phi2kkHighPT_Kmprobe",
        prescale=0.5,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=600 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=2.5 * GeV,
        maxsdoca=2 * mm,
        vchi2pdof_max=16.0,
):
    """
    SMOG2 phi(1020) -> K K (Km probe) HLT2 trigger line for PID calibration. Unprescaled High PT line.
    """
    name = "Hlt2IFT_SMOG2Phi2kkHighPT_Kmprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_phi2kk_kpProbe_highpt_line(
        name="Hlt2IFT_SMOG2Phi2kkHighPT_Kpprobe",
        prescale=0.5,
        persistreco=True,
        min_p=2 * GeV,
        min_pt=600 * MeV,
        max_trchi2dof=5,
        max_ghostprob=0.25,
        min_pidk=15,
        mmincomb=_MASSWINDOW_COMB_PHI[0],
        mmaxcomb=_MASSWINDOW_COMB_PHI[1],
        mminver=_MASSWINDOW_VERTEX_PHI[0],
        mmaxver=_MASSWINDOW_VERTEX_PHI[1],
        apt_min=2.5 * GeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=16.0,
):
    """
    SMOG2 phi(1020) -> K K (Kp probe) HLT2 trigger line for PID calibration. Unprescaled high PT line
    """
    name = "Hlt2IFT_SMOG2Phi2kkHighPT_Kpprobe"
    pvs = make_pvs

    phi2kk = make_smog2_phi2kk_calib(
        name,
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        max_ghostprob=max_ghostprob,
        min_pidk=min_pidk,
        mmincomb=mmincomb,
        mmaxcomb=mmaxcomb,
        mminver=mminver,
        mmaxver=mmaxver,
        apt_min=apt_min,
        maxsdoca=maxsdoca,
        vchi2pdof_max=vchi2pdof_max,
        process="hlt2",
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [phi2kk],
        prescale=prescale,
        persistreco=persistreco,
    )

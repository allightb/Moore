###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from Hlt2Conf.lines.ift.builders.smog2_builders import bpv_in_smog2
from Hlt2Conf.standard_particles import make_long_pions
from Hlt2Conf.probe_muons import make_velo_muons

all_lines = {}


@configurable
def filter_pions(particles, pvs=None, pt_min=0.3 * GeV):
    cut = F.require_all(
        F.P > 2000,
        F.PT > pt_min,
        F.ETA < 5.0,
        F.ETA > 1.5,
        F.BPVIPCHI2(pvs()) >= 25,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_velo(particles, pvs):
    cut = F.require_all(
        F.ETA < 4.5,
        F.ETA > 1.5,
        F.BPVIPCHI2(pvs()) >= 20,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_kshort_pi_muplus_specific_charge(pions,
                                          muons,
                                          pvs,
                                          decay_descriptor,
                                          comb_m_min=0 * MeV,
                                          comb_m_max=2650 * MeV,
                                          comb_maxdoca=0.15 * mm,
                                          name="KShortMuPiCombiner_{hash}"):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = F.require_all(
        F.END_VRHO < 100 * mm,
        F.END_VZ < 2200 * mm,
        bpv_in_smog2(pvs),
    )
    return ParticleCombiner(
        [pions, muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


from Hlt2Conf.algorithms_thor import ParticleContainersMerger


def make_kshort_pi_muplus(pions, muons, pvs, **kwargs):
    ks_particle_combinations = [
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi+ mu+",
            name="KShortMuPiCombiner_{hash}",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi+ mu-",
            name="KShortMuPiCombiner_{hash}",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi- mu-",
            name="KShortMuPiCombiner_{hash}",
            **kwargs),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi- mu+",
            name="KShortMuPiCombiner_{hash}",
            **kwargs),
    ]

    return ParticleContainersMerger(
        ks_particle_combinations, name="KS_combinations_{hash}")


from PyConf.Algorithms import KSLongVeloFilter


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_low(name="Hlt2IFT_SMOG2KshortVeloLong_LowPT",
                              prescale=0.01):

    pions = filter_pions(make_long_pions(), make_pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), make_pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, make_pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMax=1500.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_{hash}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=make_pvs) + [filtered_kshorts],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=True)


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_high(name="Hlt2IFT_SMOG2KshortVeloLong_HighPT",
                               prescale=0.1):
    pions = filter_pions(make_long_pions(), make_pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), make_pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, make_pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=1500.,
        PVConstrainedProbePtMax=3000.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_{hash}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=make_pvs) + [filtered_kshorts],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=True)


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_veryhigh(
        name="Hlt2IFT_SMOG2KshortVeloLong_VeryHighPT", prescale=1):
    pions = filter_pions(make_long_pions(), make_pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), make_pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, make_pvs, comb_m_max=1500,
        comb_m_min=0)  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs(),
        PVConstrainedMassMin=350.,
        PVConstrainedMassMax=625.,
        PVConstrainedProbePtMin=3000.,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_{hash}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=make_pvs) + [filtered_kshorts],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=True)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines
"""
from PyConf import configurable
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.event_filters import require_gec

PROCESS = "hlt2"
all_lines = {}

_hlt1_SMOG2_lines = [
    "Hlt1SMOG2MinimumBiasDecision",
    "Hlt1SMOG2SingleTrackHighPtDecision",
    "Hlt1SMOG2SingleTrackVeryHighPtDecision",
    "Hlt1SMOG2SingleMuonDecision",
    "Hlt1SMOG2eta2ppDecision",
    "Hlt1SMOG2D2KpiDecision",
    "Hlt1SMOG22BodyGenericDecision",
    "Hlt1SMOG22BodyGenericLowPtDecision",
    "Hlt1SMOG2DiMuonHighMassDecision",
    "Hlt1GECPassThroughLowMult5Decision",
    "Hlt1SMOG2BENoBiasDecision",
    "Hlt1SMOG2BELowMult10Decision",
    "Hlt1SMOG2MinimumBiasDecision",
    "Hlt1PassthroughPVinSMOG2Decision",
    "Hlt1SMOG2KsToPiPiDecision",
    "Hlt1SMOG2L02PPiDecision",
]

_hlt1_lowMult_SMOG2_lines = [
    "Hlt1SMOG2GECPassThroughLowMult5Decision",
    "Hlt1SMOG2BELowMultElectronsDecision"
]  # For luminosity measurement with p-e scattering
_hlt1_minbias_SMOG2_lines = [
    "Hlt1SMOG2MinimumBiasDecision", "Hlt1PassthroughPVinSMOG2Decision",
    "Hlt1SMOG2BENoBiasDecision"
]


@register_line_builder(all_lines)
@configurable
def smog2_passthrough_PV(name='Hlt2IFT_SMOG2Passthrough_PV_in_SMOG2',
                         prescale=0.0001,
                         persistreco=True):
    pvs = make_pvs

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs),
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_passthrough(name='Hlt2IFT_SMOG2GECPassthrough',
                      prescale=0.001,
                      persistreco=True):
    gec = require_gec()
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [gec],
        hlt1_filter_code=_hlt1_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_lumi_passthrough(name='Hlt2IFT_SMOG2LumiPassthrough',
                           prescale=1,
                           persistreco=True):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_lowMult_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_minbias_passthrough(name='Hlt2IFT_SMOG2MBPassthrough',
                              prescale=1,
                              persistreco=True):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_minbias_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )

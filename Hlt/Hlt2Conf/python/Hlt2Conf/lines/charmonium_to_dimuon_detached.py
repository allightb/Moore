###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make dimuon lines for charmonium decays. Shared by B&Q and B2CC.
"""

from PyConf import configurable

import Functors as F

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.algorithms_thor import ParticleFilter

from Moore.config import Hlt2Line, register_line_builder

from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters

from Hlt2Conf.lines.charmonium_to_dimuon import make_charmonium_dimuon

# old: mass window [MeV] around the Jpsi and Psi2s,
# old: applied to Jpsi and Psi2s lines [2018: 120 MeV]
# new: exactly define the mass upper, lower limit of Jpsi.
# new: Easier for users to define asymmetric mass window considering the FSR.
_JPSI_PDG_MASS_ = 3096.9 * MeV
_PSI2S_PDG_MASS_ = 3686.1 * MeV
_MASSWINDOW_LOW_JPSI_ = 150 * MeV
_MASSWINDOW_HIGH_JPSI_ = 150 * MeV
_MASSWINDOW_LOW_PSI2S_ = 120 * MeV
_MASSWINDOW_HIGH_PSI2S_ = 120 * MeV
_MASSMIN_JPSI = _JPSI_PDG_MASS_ - _MASSWINDOW_LOW_JPSI_
_MASSMAX_JPSI = _JPSI_PDG_MASS_ + _MASSWINDOW_HIGH_JPSI_
_MASSMIN_PSI2S = _PSI2S_PDG_MASS_ - _MASSWINDOW_LOW_PSI2S_
_MASSMAX_PSI2S = _PSI2S_PDG_MASS_ + _MASSWINDOW_HIGH_PSI2S_

_PIDMU_JPSI = -5.
_PIDMU_PSI2S = -5.

all_lines = {}


@configurable
def make_charmonium_detached_dimuon(name='charmonium_detached_dimuon_{hash}',
                                    DecayDescriptor='J/psi(1S) -> mu+ mu-',
                                    minPt_dimuon=0 * MeV,
                                    minPt_muon=300 * MeV,
                                    minPIDmu=-5,
                                    bpvdls_min=3.,
                                    minMass_dimuon=0. * MeV,
                                    maxMass_dimuon=100000. * MeV,
                                    maxVertexChi2=25.):
    """
    Make the detached dimuon.
    """

    return make_charmonium_dimuon(
        name=name,
        DecayDescriptor=DecayDescriptor,
        minPt_dimuon=minPt_dimuon,
        minPt_muon=minPt_muon,
        minPIDmu=minPIDmu,
        bpvdls_min=bpvdls_min,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon,
        maxVertexChi2=maxVertexChi2)


@configurable
def make_detached_jpsi(name='detached_jpsi_{hash}',
                       minMass_dimuon=_MASSMIN_JPSI,
                       maxMass_dimuon=_MASSMAX_JPSI,
                       minPt_muon=300 * MeV,
                       minPt_Jpsi=0 * MeV):

    code = (F.PT > minPt_Jpsi)

    dimuon = make_charmonium_detached_dimuon(
        DecayDescriptor='J/psi(1S) -> mu+ mu-',
        minPt_dimuon=minPt_Jpsi,
        minPt_muon=minPt_muon,
        minPIDmu=_PIDMU_JPSI,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


@configurable
def make_detached_psi2s(name='charmonium_detached_psi2s_{hash}',
                        minMass_dimuon=_MASSMIN_PSI2S,
                        maxMass_dimuon=_MASSMAX_PSI2S,
                        minPt_muon=300 * MeV,
                        minPt_Psi2S=0 * MeV):

    code = (F.PT > minPt_Psi2S)

    dimuon = make_charmonium_detached_dimuon(
        DecayDescriptor='psi(2S) -> mu+ mu-',
        minPt_dimuon=minPt_Psi2S,
        minPt_muon=minPt_muon,
        minPIDmu=_PIDMU_PSI2S,
        minMass_dimuon=minMass_dimuon,
        maxMass_dimuon=maxMass_dimuon)

    return ParticleFilter(dimuon, name=name, Cut=F.FILTER(code))


#############
#define lines
##############


@register_line_builder(all_lines)
@configurable
def JpsiToMuMuDetached_line(name='Hlt2_JpsiToMuMuDetachedFull',
                            prescale=1,
                            persistreco=True):
    line_alg = make_detached_jpsi()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        pv_tracks=True,
        persistreco=persistreco)


@register_line_builder(all_lines)
@configurable
def Psi2SToMuMuDetached_line(name='Hlt2_Psi2SToMuMuDetachedFull',
                             prescale=1,
                             persistreco=True):
    line_alg = make_detached_psi2s()
    return Hlt2Line(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        pv_tracks=True,
        persistreco=persistreco)

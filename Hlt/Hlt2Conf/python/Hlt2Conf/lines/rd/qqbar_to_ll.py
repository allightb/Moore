##############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definiton of LFV lines of qqbar -> emu
- Phi(1020) -> E Mu + SS prompt
- Phi(1020) -> E Mu + SS detached
- J/Psi(1S) -> E Mu + SS prompt
- J/Psi(1S) -> E Mu + SS detached
- Upsilon(1S) -> E Mu + SS prompt only

Control channels:
- Phi(1020) -> E E prompt
- J/Psi(1S) -> E E prompt
- Upsilon(1S) -> E E prompt

author: Miroslav Saur
date: 27.12.2021

- Upsilon(1S) -> tau+ tau-
- Upsilon(1S) -> e+ tau-
- Upsilon(1S) -> mu+ tau-
- Upsilon(1S) -> mu+ mu-
- Upsilon(2S) -> (Upsilon(1S) ->tau+ tau-) pi+ pi-
- Upsilon(2S) -> (Upsilon(1S) ->mu+ mu-) pi+ pi-
- Upsilon(2S) -> (Upsilon(1S) ->e+ e-) pi+ pi-

author: Raja Nandakumar
date: 27.05.2023
"""

from GaudiKernel.SystemOfUnits import MeV, GeV
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_mue,
    make_rd_prompt_mue,
    make_rd_prompt_dielectrons,
    make_rd_prompt_dimuons,
)
from Hlt2Conf.lines.rd.builders.qqbar_to_ll_builders import (
    make_prompt_tautau,
    make_prompt_etau,
    make_prompt_mutau,
    make_upsilons_to_upsilons,
)
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    rd_prefilter,
    _VRD_MONITORING_VARIABLES,
)
import Functors as F
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}

###### PROMPT LINES #####


@register_line_builder(all_lines)
@configurable
def phi_to_mue_line(name="Hlt2RD_PhiToMuE", prescale=0.01, persistreco=False):
    """
    Definiton of [phi(1020) -> mu- e+]CC
    """
    emu = make_rd_prompt_mue(
        parent_id="phi(1020)",
        am_min=850.0 * MeV,  # 850
        am_max=1170.0 * MeV,  # 1220
        pt_dilepton_min=0.5 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
        same_sign=False,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def phi_to_mue_ss_line(name="Hlt2RD_PhiToMuE_SS",
                       prescale=0.001,
                       persistreco=False):
    emu_SS = make_rd_prompt_mue(
        parent_id="phi(1020)",
        am_min=850.0 * MeV,
        am_max=1170.0 * MeV,
        pt_dilepton_min=0.5 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
        same_sign=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu_SS],
        extra_outputs=parent_isolation_output("EMu", emu_SS),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_mue_line(name="Hlt2RD_JpsiToMuE", prescale=1, persistreco=False):
    emu = make_rd_prompt_mue(
        parent_id="J/psi(1S)",
        am_min=2700.0 * MeV,
        am_max=3400.0 * MeV,
        pt_dilepton_min=2.5 * GeV,
        pt_electron_min=1.0 * GeV,
        pt_muon_min=1.0 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_mue_ss_line(name="Hlt2RD_JpsiToMuE_SS",
                        prescale=0.1,
                        persistreco=False):
    emu = make_rd_prompt_mue(
        parent_id="J/psi(1S)",
        am_min=2700.0 * MeV,
        am_max=3400.0 * MeV,
        pt_dilepton_min=2.5 * GeV,
        pt_electron_min=1.0 * GeV,
        pt_muon_min=1.0 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
        same_sign=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def upsilon_to_mue_line(name="Hlt2RD_UpsilonToMuE",
                        prescale=1,
                        persistreco=False):
    emu = make_rd_prompt_mue(
        parent_id="Upsilon(1S)",
        am_min=8000.0 * MeV,
        am_max=12000.0 * MeV,
        pt_dilepton_min=2.0 * GeV,
        pt_electron_min=1.0 * GeV,
        pt_muon_min=1.0 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
        same_sign=False,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def upsilon_to_mue_ss_line(name="Hlt2RD_UpsilonToMuE_SS",
                           prescale=1,
                           persistreco=False):
    emu = make_rd_prompt_mue(
        parent_id="Upsilon(1S)",
        am_min=8000.0 * MeV,
        am_max=12000.0 * MeV,
        pt_dilepton_min=2.0 * GeV,
        pt_electron_min=1.0 * GeV,
        pt_muon_min=1.0 * GeV,
        pid_muon=F.require_all(F.ISMUON, F.PID_MU > 3.0),
        same_sign=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def upsilonToTauTau_Line(name="Hlt2RD_UpsilonToTauTau",
                         prescale=1,
                         persistreco=False):
    """
    Definiton of [Upsilon(1S) -> tau- tau+]CC
    """

    tautau = make_prompt_tautau(
        parent_id="Upsilon(1S)",
        min_dilepton_mass=8000.0 * MeV,
        max_dilepton_mass=12000.0 * MeV,
        min_dilepton_pt=1.0 * GeV,
        # pi_pt_min=150 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tautau],
        extra_outputs=parent_isolation_output("TauTau", tautau),
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def upsilonToeTau_Line(name="Hlt2RD_UpsilonToTauE",
                       prescale=1,
                       persistreco=False):
    etau = make_prompt_etau(
        # name="Hlt2RD_UpsilonToTauE_Builder",
        parent_id="Upsilon(1S)",
        min_dilepton_mass=8000.0 * MeV,
        max_dilepton_mass=12000.0 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [etau],
        extra_outputs=parent_isolation_output("ETau", etau),
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def upsilonTomuTau_Line(name="Hlt2RD_UpsilonToTauMu",
                        prescale=1,
                        persistreco=False):
    mutau = make_prompt_mutau(
        # name="Hlt2RD_UpsilonToTauMu_Builder",
        parent_id="Upsilon(1S)",
        min_dilepton_mass=7000.0 * MeV,
        max_dilepton_mass=12000.0 * MeV,
        min_dilepton_pt=1.0 * GeV,
        min_pt_mu=1000 * MeV,
        # pi_pt_min=150 * MeV,
        IsMuon=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [mutau],
        extra_outputs=parent_isolation_output("MuTau", mutau),
        prescale=prescale,
        persistreco=persistreco,
    )


##### DETACHED LINES #####


@register_line_builder(all_lines)
@configurable
def phi_to_mue_detached_line(name="Hlt2RD_PhiToMuE_Detached",
                             prescale=0.1,
                             persistreco=False):
    """
    Definiton of [phi(1020) -> mu- e+]CC
    """
    emu = make_rd_detached_mue(
        parent_id="phi(1020)",
        min_dilepton_mass=800.0 * MeV,
        max_dilepton_mass=1170.0 * MeV,
        min_probnn_mu=None,
        min_pt_e=0.4 * GeV,
        min_pt_mu=0.4 * GeV,
        min_bpvvdchi2=30.0,
        max_vchi2ndof=4.0,
        min_PIDmu=2,
        IsMuon=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def phi_to_mue_ss_detached_line(name="Hlt2RD_PhiToMuE_SS_Detached",
                                prescale=0.05,
                                persistreco=False):
    """
    Definiton of [phi(1020) -> mu+ e+]CC
    """
    emu = make_rd_detached_mue(
        parent_id="phi(1020)",
        min_dilepton_mass=800.0 * MeV,
        max_dilepton_mass=1170.0 * MeV,
        min_probnn_mu=None,
        min_pt_e=0.4 * GeV,
        min_pt_mu=0.4 * GeV,
        min_bpvvdchi2=30.0,
        max_vchi2ndof=4.0,
        min_PIDmu=2,
        IsMuon=True,
        same_sign=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_mue_detached_line(name="Hlt2RD_JpsiToMuE_Detached",
                              prescale=1,
                              persistreco=False):
    """
    Definiton of [J/psi(1S) -> mu- e+]CC
    """
    emu = make_rd_detached_mue(
        parent_id="J/psi(1S)",
        min_dilepton_mass=2700.0 * MeV,
        max_dilepton_mass=3400.0 * MeV,
        min_probnn_mu=None,
        min_pt_e=1.0 * GeV,
        min_pt_mu=1.0 * GeV,
        min_bpvvdchi2=30.0,
        max_vchi2ndof=4.0,
        min_PIDmu=2,
        IsMuon=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_mue_ss_detached_line(name="Hlt2RD_JpsiToMuE_SS_Detached",
                                 prescale=1,
                                 persistreco=False):
    """
    Definiton of [J/psi(1S) -> mu+ e+]CC
    """
    emu = make_rd_detached_mue(
        parent_id="J/psi(1S)",
        min_dilepton_mass=2700.0 * MeV,
        max_dilepton_mass=3400.0 * MeV,
        min_probnn_mu=None,
        min_pt_e=0.5 * GeV,
        min_pt_mu=0.5 * GeV,
        min_bpvvdchi2=30.0,
        max_vchi2ndof=4.0,
        min_PIDmu=2,
        IsMuon=True,
        same_sign=True,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


##### CONTROL ee LINES #####


@register_line_builder(all_lines)
@configurable
def phi_to_ee_line(name="Hlt2RD_PhiToEE", prescale=0.01, persistreco=False):
    emu = make_rd_prompt_dielectrons(
        parent_id="phi(1020)",
        same_sign=False,
        PIDe_min=2.0,
        pt_e_min=1.5 * GeV,
        min_dilepton_pt=2.0 * GeV,
        min_dilepton_mass=850 * MeV,
        max_dilepton_mass=1170 * MeV,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def jpsi_to_ee_line(name="Hlt2RD_JpsiToEE", prescale=0.001, persistreco=False):
    emu = make_rd_prompt_dielectrons(
        parent_id="J/psi(1S)",
        same_sign=False,
        PIDe_min=2.0,
        pt_e_min=0.5 * GeV,
        min_dilepton_pt=1.0 * GeV,
        min_dilepton_mass=2700 * MeV,
        max_dilepton_mass=3400 * MeV,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [emu],
        extra_outputs=parent_isolation_output("EMu", emu),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def upsilon_to_ee_line(name="Hlt2RD_UpsilonToEE",
                       prescale=0.1,
                       persistreco=False):
    ee = make_rd_prompt_dielectrons(
        parent_id="Upsilon(1S)",
        same_sign=False,
        PIDe_min=2.0,
        pt_e_min=1.0 * GeV,
        min_dilepton_pt=1.0 * GeV,
        min_dilepton_mass=8000 * MeV,
        max_dilepton_mass=12000 * MeV,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ee],
        extra_outputs=parent_isolation_output("EE", ee),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def upsilon_to_mumu_line(name="Hlt2RD_UpsilonToMuMu",
                         prescale=1,
                         persistreco=False):
    # From rdbuilder_thor - promptness is defined there. The variable mipchi2dvprimary_min is cut on for detached muons. So, if we want detached dimuons use
    # make_rd_detached_dimuons instead of make_rd_prompt_dimuons
    # in the line just below
    mumu = make_rd_prompt_dimuons(
        name="Hlt2RD_UpsilonToMuMu_Builder",
        parent_id="Upsilon(1S)",
        same_sign=False,
        pid=F.require_all(F.PID_MU > 3.0, F.ISMUON),
        pt_dimuon_min=1000.0 * MeV,
        pt_muon_min=1000.0 * MeV,
        p_muon_min=1000.0 * MeV,
        adocachi2cut_max=30.0,
        vchi2pdof_max=16.0,
        am_min=7000.0 * MeV,
        am_max=12000.0 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [mumu],
        extra_outputs=parent_isolation_output("MuMu", mumu),
        prescale=prescale,
        persistreco=persistreco,
    )


##### Cascade upsilon decays #####


@register_line_builder(all_lines)
@configurable
def upsilon2_to_upsilon1pipi_tautau_line(
        name="Hlt2RD_Upsilon2ToUpsilon1PiPi_Upsilon1ToTauTau",
        prescale=1,
        persistreco=False):
    upsilons1s = make_prompt_tautau()
    upsilons2s = make_upsilons_to_upsilons(upsilons1s)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [upsilons1s, upsilons2s
                               ],  # this helps the control flow
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def upsilon2_to_upsilon1pipi_mumu_line(
        name="Hlt2RD_Upsilon2ToUpsilon1PiPi_Upsilon1ToMuMu",
        prescale=1,
        persistreco=False):
    upsilons1s = make_rd_prompt_dimuons()
    upsilons2s = make_upsilons_to_upsilons(upsilons1s)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [upsilons1s, upsilons2s
                               ],  # this helps the control flow
        extra_outputs=parent_isolation_output("Upsilon2s", upsilons2s),
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def upsilon2_to_upsilon1pipi_ee_line(
        name="Hlt2RD_Upsilon2ToUpsilon1PiPi_Upsilon1ToEE",
        prescale=1,
        persistreco=False):
    upsilons1s = make_rd_prompt_dielectrons()
    upsilons2s = make_upsilons_to_upsilons(upsilons1s)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [upsilons1s, upsilons2s
                               ],  # this helps the control flow
        extra_outputs=parent_isolation_output("Upsilon2s", upsilons2s),
        prescale=prescale,
        persistreco=persistreco,
    )


#################
## END OF FILE ##
#################

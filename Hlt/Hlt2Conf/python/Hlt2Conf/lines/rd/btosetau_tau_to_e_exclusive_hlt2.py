###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s e tau HLT2 lines.
Final states built are (all taus decay to electrons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) e+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) e+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) e- tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake kaon and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake electron from the Bs and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake electron from the tau and its charge conjugate
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) e+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K+) e+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) e- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau- with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau- with a fake proton
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake proton
     - Lb -> Lambda(-> K+ K-) e+ tau- with a fake electron
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake electron
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) e+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi+) e+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi+) e- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake electron and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake electron and its charge conjugate
Note a: fake particles are obtained through reversing the PID requirements
Note b: particles stemming directly from the b-hadron are combined with a ParticleCombiner. This is not intended to represent a given particle (hence the loose mass requirements)
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV

import Functors as F

from Hlt2Conf.lines.rd.builders import rdbuilder_thor
from Hlt2Conf.lines.rd.builders import btosetau_exclusive
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_and_children_isolation

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}

kwargs_protons = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_P > 4, F.PID_P - F.PID_K > 0, ~F.ISMUON),
}
kwargs_protons_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_P < 4),
}

kwargs_kaons = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON),
}

kwargs_kaons_reverse_pid = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_bu = {
    "pt_min": 500 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 4, F.PID_K - F.PID_P > 0, ~F.ISMUON),
}

kwargs_kaons_for_bu_reverse_pid = {
    "pt_min": 500 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_kstar = {
    "p_min": 5000 * MeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_K > 4, F.PID_K - F.PID_P > 0, ~F.ISMUON)
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 5000 * MeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_K < 4)
}

kwargs_pions = {
    "p_min": 5000 * MeV,
    "pt_min": 750 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K < 0, ~F.ISMUON)
}
kwargs_pions_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 750 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K > 0)
}
kwargs_electrons = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_E > 5, ~F.ISMUON),
    "pt_min": 500 * MeV
}

kwargs_electrons_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_E < 5),
    "pt_min": 500 * MeV
}

kwargs_electrons_from_tau = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_E > 5, ~F.ISMUON)
}
kwargs_electrons_from_tau_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_E < 5)
}

kwargs_electrons_for_bu = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_E > 3, ~F.ISMUON),
    "pt_min": 500 * MeV
}

kwargs_electrons_for_bu_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_E < 3),
    "pt_min": 500 * MeV
}


@register_line_builder(all_lines)
def bstophietau_tautoe_line(name='Hlt2RD_BsToKKTauE_TauToE', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(kaons, kaons, electrons, pvs)
    bs = btosetau_exclusive.make_bs(phies, electrons_from_tau, pvs)
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_same_sign_kaons_sske_line(
        name='Hlt2RD_BsToKKTauE_TauToE_SSK_SSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ K+ e+]cc",
        name="rd_same_sign_dikaon_electron_for_btosetau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phies,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_same_sign_kaons_{hash}")
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_same_sign_kaons_oske_line(
        name='Hlt2RD_BsToKKTauE_TauToE_SSK_OSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K- K- e+]cc",
        name="rd_same_sign_dikaon_electron_for_btosetau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phies,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_same_sign_kaons_{hash}")
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_same_sign_electrons_line(
        name='Hlt2RD_BsToKKTauE_TauToE_SSe', prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons, kaons, electrons, pvs, decay_descriptor="[B0 -> K+ K- e+]cc")
    bs = btosetau_exclusive.make_bs(
        phies,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bs_to_kktaue_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_fakeelectron_from_tau_line(
        name='Hlt2RD_BsToKKTauE_TauToE_FakeelectronFromTau', prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    phies = btosetau_exclusive.make_phie(kaons, kaons, electrons, pvs)
    bs = btosetau_exclusive.make_bs(
        phies,
        fake_electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_fakeelectron_from_b_line(
        name='Hlt2RD_BsToKKTauE_TauToE_FakeelectronFromB', prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    phies_fake_electron = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        fake_electrons,
        pvs,
        name="rd_dikaon_fake_electron_for_btosetau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phies_fake_electron,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [phies_fake_electron, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophietau_tautoe_fakekaon_line(name='Hlt2RD_BsToKKTauE_TauToE_FakeKaon',
                                     prescale=0.03):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        fake_kaons,
        electrons,
        pvs,
        name="rd_fake_dikaon_electron_for_btosetau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phies,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_fake_kaons_{hash}")
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phie': phies
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_sspe_line(name='Hlt2RD_LbToPKTauE_TauToE_SSpe',
                                prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_ospe_line(name='Hlt2RD_LbToPKTauE_TauToE_OSpe',
                                prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_same_sign_pk_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_SSpK_SSpe', prescale=0.1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ e+]cc",
        name="rd_same_sign_pk_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_same_sign_pk_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_same_sign_pk_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_SSpK_OSpe', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ e-]cc",
        name="rd_same_sign_pk_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_same_sign_pk_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_same_sign_electrons_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_SSe_SSpe', prescale=0.3):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_same_sign_electrons_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_SSe_OSpe', prescale=0.5):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeelectron_from_tau_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeelectronFromTau_SSpe',
        prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeelectron_from_tau_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeelectronFromTau_OSpe',
        prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc")
    lb = btosetau_exclusive.make_lb(
        pkes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeelectron_from_b_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeelectronFromB_SSpe',
        prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    pkes_fake_electron = btosetau_exclusive.make_pke(
        kaons,
        protons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [pkes_fake_electron, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeelectron_from_b_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeelectronFromB_OSpe',
        prescale=0.005):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    pkes_fake_electron = btosetau_exclusive.make_pke(
        kaons,
        protons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [pkes_fake_electron, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeproton_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeProton_SSpe', prescale=0.05):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_proton_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_fake_protons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakeproton_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeProton_OSpe', prescale=0.05):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_proton_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_fake_protons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakekaon_sspe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeKaon_SSpe', prescale=0.15):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_kaon_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pketau_fake_kaons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopketau_tautoe_fakekaon_ospe_line(
        name='Hlt2RD_LbToPKTauE_TauToE_FakeKaon_OSpe', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_kaon_electron_for_btosetau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pketau_fake_kaons_{hash}")
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKe': pkes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_sske_line(name='Hlt2RD_BdToKPiTauE_TauToE_SSKe',
                                 prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc")
    bd = btosetau_exclusive.make_bd(
        kstes, electrons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 e-]cc")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_oske_line(name='Hlt2RD_BdToKPiTauE_TauToE_OSKe',
                                 prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc")
    bd = btosetau_exclusive.make_bd(
        kstes, electrons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 e+]cc")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_same_sign_kpi_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_SSKpi_SSKe', prescale=0.3):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ e+]cc",
        name="rd_same_sign_kpi_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_same_sign_kpi_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_same_sign_kpi_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_SSKpi_OSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ e-]cc",
        name="rd_same_sign_kpi_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_same_sign_kpi_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_same_sign_electrons_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_SSe_SSKe', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_same_sign_electrons_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_SSe_OSKe', prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakeelectron_from_tau_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeelectronFromTau_SSKe',
        prescale=0.01):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc")
    bd = btosetau_exclusive.make_bd(
        kstes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakeelectron_from_tau_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeelectronFromTau_OSKe',
        prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc")
    bd = btosetau_exclusive.make_bd(
        kstes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakeelectron_from_b_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeelectronFromB_SSKe',
        prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    kstes_fake_electron = btosetau_exclusive.make_kste(
        kaons,
        pions,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [kstes_fake_electron, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakeelectron_from_b_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeelectronFromB_OSKe',
        prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    kstes_fake_electron = btosetau_exclusive.make_kste(
        kaons,
        pions,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [kstes_fake_electron, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakekaon_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeKaon_SSKe', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_kaon_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_fake_kaons_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakekaon_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakeKaon_OSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_kaon_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_fake_kaons_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakepion_sske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakePion_SSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_pion_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpietau_fake_pions_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstetau_tautoe_fakepion_oske_line(
        name='Hlt2RD_BdToKPiTauE_TauToE_FakePion_OSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_pion_electron_for_btosetau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpietau_fake_pions_{hash}")
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstes': kstes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_oske_line(name='Hlt2RD_BuToKTauE_TauToE_OSKe',
                               prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kes, electrons_from_tau, pvs, decay_descriptor="[B+ -> B0 e+]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_sske_line(name='Hlt2RD_BuToKTauE_TauToE_SSKe',
                               prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e+ K+]cc")
    bu = btosetau_exclusive.make_bu(
        kes, electrons_from_tau, pvs, decay_descriptor="[B+ -> B0 e-]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_same_sign_electrons_oske_line(
        name='Hlt2RD_BuToKTauE_TauToE_SSe_OSKe', prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_ketau_same_sign_electrons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_same_sign_electrons_sske_line(
        name='Hlt2RD_BuToKTauE_TauToE_SSe_SSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K-]cc")
    bu = btosetau_exclusive.make_bu(
        kes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_ketau_same_sign_electrons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakeelectron_from_tau_oske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeelectronFromTau_OSKe',
        prescale=0.01):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_tau_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    algs = rd_prefilter() + [kes, bu]
    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakeelectron_from_tau_sske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeelectronFromTau_SSKe',
        prescale=0.003):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e+ K+]cc")
    bu = btosetau_exclusive.make_bu(
        kes,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [kes, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakeelectron_from_b_oske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeelectronFromB_OSKe', prescale=0.005):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu_reverse_pid)
    kes_fake_electron = btosetau_exclusive.make_ke(
        kaons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> e- K+]cc",
        name="rd_kaon_fake_electron_for_btosetau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [kes_fake_electron, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakeelectron_from_b_sske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeelectronFromB_SSKe', prescale=0.01):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu_reverse_pid)
    kes_fake_electron = btosetau_exclusive.make_ke(
        kaons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> e+ K+]cc",
        name="rd_kaon_fake_electron_for_btosetau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kes_fake_electron,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_b_{hash}")
    algs = rd_prefilter() + [kes_fake_electron, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes_fake_electron
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakekaon_oske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeKaon_OSKe', prescale=0.01):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        fake_kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> e- K+]cc",
        name="rd_fake_kaon_electron_for_btosetau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_ketau_fake_kaons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoketau_tautoe_fakekaon_sske_line(
        name='Hlt2RD_BuToKTauE_TauToE_FakeKaon_SSKe', prescale=0.05):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        fake_kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> e+ K+]cc",
        name="rd_fake_kaon_electron_for_btosetau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kes,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_ketau_fake_kaons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kes': kes
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)

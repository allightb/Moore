# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines HLT2 lines for studies of rare decays.
"""

from . import hlt2_rd
from . import spruce_rd

# provide "all_lines" for correct registration by the overall HLT2 lines module
turbo_lines = {}
turbo_lines.update(hlt2_rd.turbo_lines)

full_lines = {}
full_lines.update(hlt2_rd.full_lines)

all_lines = {}

all_lines.update(turbo_lines)
all_lines.update(full_lines)

sprucing_lines = {}
sprucing_lines.update(spruce_rd.sprucing_lines)

# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines HLT2 lines for studies of rare decays.
"""

from . import b_to_ll_hlt2
from . import b_to_multilepton_hlt2
from . import b_to_Xdilepton_detached
from . import b_to_xtaul_hlt2
from . import prompt_multilepton
from . import rare_tau_decay_lines
from . import strange
from . import RpK_lines
from . import b_to_hemu
from . import b_to_hemu_control_modes
from . import baryonic
from . import qqbar_to_ll
from . import b_to_ll_LFV
from . import b_to_kstarmumu
from . import btosetau_exclusive_hlt2
from . import btosmutau_tau_to_e_exclusive_hlt2
from . import btosetau_tau_to_e_exclusive_hlt2
from . import btosmutau_exclusive_hlt2
from . import btostautau_exclusive_hlt2
from . import btostautau_mue_exclusive_hlt2
from . import btostautau_ee_exclusive_hlt2
from . import b_to_xgamma_exclusive_hlt2
from . import b_to_xll_hlt2
from . import rad_incl
from . import b_to_v0ll_hlt2
from . import omega_rare_decay_lines
from . import baryonic_radiative
from . import bnv_lines_hlt2
from . import lfv_lines_hlt2
from . import b_to_tautau_hlt2

# full lines
full_lines = {}
full_lines.update(rad_incl.all_lines)

# turbo lines
turbo_lines = {}
turbo_lines.update(b_to_ll_hlt2.all_lines)
turbo_lines.update(b_to_multilepton_hlt2.all_lines)
turbo_lines.update(b_to_Xdilepton_detached.all_lines)
turbo_lines.update(b_to_xtaul_hlt2.all_lines)
turbo_lines.update(prompt_multilepton.all_lines)
turbo_lines.update(rare_tau_decay_lines.all_lines)
turbo_lines.update(RpK_lines.all_lines)
turbo_lines.update(b_to_hemu.all_lines)
turbo_lines.update(b_to_hemu_control_modes.all_lines)
turbo_lines.update(baryonic.all_lines)
turbo_lines.update(qqbar_to_ll.all_lines)
turbo_lines.update(b_to_ll_LFV.hlt2_lines)
turbo_lines.update(b_to_kstarmumu.all_lines)
turbo_lines.update(btosetau_exclusive_hlt2.all_lines)
turbo_lines.update(btosmutau_tau_to_e_exclusive_hlt2.all_lines)
turbo_lines.update(btosetau_tau_to_e_exclusive_hlt2.all_lines)
turbo_lines.update(btosmutau_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_mue_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_ee_exclusive_hlt2.all_lines)
turbo_lines.update(b_to_xgamma_exclusive_hlt2.all_lines)
turbo_lines.update(b_to_xll_hlt2.all_lines)
turbo_lines.update(strange.all_lines)
turbo_lines.update(b_to_v0ll_hlt2.all_lines)
turbo_lines.update(omega_rare_decay_lines.all_lines)
turbo_lines.update(baryonic_radiative.all_lines)
turbo_lines.update(bnv_lines_hlt2.all_lines)
turbo_lines.update(lfv_lines_hlt2.all_lines)
turbo_lines.update(b_to_tautau_hlt2.hlt2_lines)

###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s mu tau HLT2 lines.
Final states built are (all taus decay to muons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) mu+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) mu+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) mu- tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake kaon and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake muon from the Bs and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake muon from the tau and its charge conjugate
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) mu+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K+) mu+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) mu- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau- with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau- with a fake proton
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake proton
     - Lb -> Lambda(-> K+ K-) mu+ tau- with a fake muon
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake muon
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) mu+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi+) mu+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi+) mu- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake muon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake muon and its charge conjugate
4. B+ decays to K+
     - Bu -> K+ mu+ tau- and its charge conjugate
     - Bu -> K+ mu- tau+ and its charge conjugate
     - Bu -> K+ mu- tau- and its charge conjugate
     - Bu -> K+ mu+ tau+ and its charge conjugate
     - Bu -> K+ mu+ tau- with a fake muon and its charge conjugate
     - Bu -> K+ mu- tau+ with a fake muon and its charge conjugate
     - Bu -> K+ mu+ tau- with a fake kaon and its charge conjugate
     - Bu -> K+ mu- tau+ with a fake kaon and its charge conjugate
Note a: fake particles are obtained through reversing the PID requirements
Note b: particles stemming directly from the b-hadron are combined with a ParticleCombiner. This is not intended to represent a given particle (hence the loose mass requirements)

Author: H. Tilquin      
Contact: hanae.tilquin@cern.ch
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV

import Functors as F

from Hlt2Conf.lines.rd.builders import rdbuilder_thor
from Hlt2Conf.lines.rd.builders import btosmutau_exclusive
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_and_children_isolation

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}

kwargs_protons = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_P > 3, ~F.ISMUON),
}
kwargs_protons_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_P < 3),
}

kwargs_kaons = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_K > 3, ~F.ISMUON),
}

kwargs_kaons_reverse_pid = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_K < 3),
}

kwargs_kaons_for_bu = {
    "pt_min": 500 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 3, ~F.ISMUON),
}

kwargs_kaons_for_bu_reverse_pid = {
    "pt_min": 500 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 3),
}

kwargs_kaons_for_kstar = {
    "p_min": 3000 * MeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_K > 3, ~F.ISMUON)
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_K < 3)
}

kwargs_pions = {
    "p_min": 0 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_K < -2, ~F.ISMUON)
}
kwargs_pions_reverse_pid = {
    "p_min": 0 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_K > -2)
}

kwargs_muons_from_tau = {
    "p_min": 0 * MeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_MU > 0, F.ISMUON)
}
kwargs_muons_from_tau_reverse_pid = {
    "p_min": 0 * MeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_MU < 0)
}

kwargs_muons = {
    "p_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_MU > 0, F.ISMUON),
    "pt_min": 500 * MeV
}

kwargs_muons_reverse_pid = {
    "p_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_MU < 0),
    "pt_min": 500 * MeV
}

kwargs_muons_for_bu = {
    "p_min": 0 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_MU > 0, F.ISMUON),
    "pt_min": 750 * MeV
}

kwargs_muons_for_bu_reverse_pid = {
    "p_min": 0 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_MU < 0),
    "pt_min": 750 * MeV
}


@register_line_builder(all_lines)
def bstophimutau_tautomu_line(name='Hlt2RD_BsToKKTauMu_TauToMu',
                              prescale=1,
                              persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(kaons, kaons, muons, pvs)
    bs = btosmutau_exclusive.make_bs(phimus, muons_from_tau, pvs)
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_same_sign_kaons_sskmu_line(
        name='Hlt2RD_BsToKKTauMu_TauToMu_SSK_SSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ K+ mu+]cc",
        name='rd_same_sign_dikaon_muon_for_btosmutau_{hash}')
    bs = btosmutau_exclusive.make_bs(
        phimus,
        muons_from_tau,
        pvs,
        name='rd_make_bs_to_kktaumu_same_sign_kaons_{hash}')
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_same_sign_kaons_oskmu_line(
        name='Hlt2RD_BsToKKTauMu_TauToMu_SSK_OSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> K- K- mu+]cc",
        name='rd_same_sign_dikaon_muon_for_btosmutau_{hash}')
    bs = btosmutau_exclusive.make_bs(
        phimus,
        muons_from_tau,
        pvs,
        name='rd_make_bs_to_kktaumu_same_sign_kaons_{hash}')
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_same_sign_muons_line(
        name='Hlt2RD_BsToKKTauMu_TauToMu_SSMu', prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons, kaons, muons, pvs, decay_descriptor="[B0 -> K+ K- mu+]cc")
    bs = btosmutau_exclusive.make_bs(
        phimus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bs_to_kktaumu_same_sign_muons_{hash}')
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_fakemuon_from_tau_line(
        name='Hlt2RD_BsToKKTauMu_TauToMu_FakeMuFromTau',
        prescale=0.015,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    phimus = btosmutau_exclusive.make_phimu(kaons, kaons, muons, pvs)
    bs = btosmutau_exclusive.make_bs(
        phimus,
        fake_muons_from_tau,
        pvs,
        name='rd_make_bs_to_kktaumu_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_fakemuon_from_b_line(
        name='Hlt2RD_BsToKKTauMu_TauToMu_FakeMuFromB',
        prescale=0.008,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    phimus_fake_muon = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        fake_muons,
        pvs,
        name='rd_dikaon_fake_muon_for_btosmutau_{hash}')
    bs = btosmutau_exclusive.make_bs(
        phimus_fake_muon,
        muons_from_tau,
        pvs,
        name='rd_make_bs_to_kktaumu_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [phimus_fake_muon, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautomu_fakekaon_line(name='Hlt2RD_BsToKKTauMu_TauToMu_FakeK',
                                       prescale=0.3,
                                       persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        fake_kaons,
        muons,
        pvs,
        name='rd_fake_dikaon_muon_for_btosmutau_{hash}')
    bs = btosmutau_exclusive.make_bs(
        phimus,
        muons_from_tau,
        pvs,
        name='rd_make_bs_to_kktaumu_fake_kaons_{hash}')
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_sspmu_line(name='Hlt2RD_LbToPKTauMu_TauToMu_SSPMu',
                                   prescale=1,
                                   persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus, muons_from_tau, pvs, decay_descriptor="[Lambda_b0 -> B0 mu-]cc")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_ospmu_line(name='Hlt2RD_LbToPKTauMu_TauToMu_OSPMu',
                                   prescale=1,
                                   persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus, muons_from_tau, pvs, decay_descriptor="[Lambda_b0 -> B0 mu+]cc")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_same_sign_pk_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_SSPK_SSPMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ mu+]cc",
        name='rd_same_sign_pk_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_same_sign_pk_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_same_sign_pk_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_SSPK_OSPMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ mu-]cc",
        name='rd_same_sign_pk_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_same_sign_pk_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_same_sign_muons_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_SSMu_SSPMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_same_sign_muons_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_SSMu_OSPMu',
        prescale=0.4,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakemuon_from_tau_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeMuFromTau_SSPMu',
        prescale=0.015,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakemuon_from_tau_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeMuFromTau_OSPMu',
        prescale=0.015,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakemuon_from_b_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeMuFromB_SSPMu',
        prescale=0.015,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    pkmus_fake_muon = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name='rd_pk_fake_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [pkmus_fake_muon, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakemuon_from_b_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeMuFromB_OSPMu',
        prescale=0.005,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    pkmus_fake_muon = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name='rd_pk_fake_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [pkmus_fake_muon, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakeproton_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeP_SSPMu',
        prescale=0.3,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name='rd_pk_fake_proton_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_fake_protons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakeproton_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeP_OSPMu',
        prescale=0.3,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name='rd_pk_fake_proton_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_fake_protons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakekaon_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeK_SSPMu',
        prescale=0.15,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name='rd_pk_fake_kaon_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name='rd_make_lb_to_pkmutau_fake_kaons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautomu_fakekaon_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToMu_FakeK_OSPMu',
        prescale=0.3,
        persistreco=False):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name='rd_pk_fake_kaon_muon_for_btosmutau_{hash}')
    lb = btosmutau_exclusive.make_lb(
        pkmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name='rd_make_lb_to_pkmutau_fake_kaons_{hash}')
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_sskmu_line(name='Hlt2RD_BdToKPiTauMu_TauToMu_SSKMu',
                                    prescale=1,
                                    persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus, muons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 mu-]cc")
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_oskmu_line(name='Hlt2RD_BdToKPiTauMu_TauToMu_OSKMu',
                                    prescale=1,
                                    persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus, muons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 mu+]cc")
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_same_sign_kpi_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_SSKPi_SSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ mu+]cc",
        name='rd_same_sign_kpi_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_same_sign_kpi_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_same_sign_kpi_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_SSKPi_OSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ mu-]cc",
        name='rd_same_sign_kpi_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_same_sign_kpi_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_same_sign_muons_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_SSMu_SSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_same_sign_muons_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_SSMu_OSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_same_sign_muons_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakemuon_from_tau_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeMuFromTau_SSKMu',
        prescale=0.005,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakemuon_from_tau_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeMuFromTau_OSKMu',
        prescale=0.008,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakemuon_from_b_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeMuFromB_SSKMu',
        prescale=0.008,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    kstmus_fake_muon = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name='rd_kpi_fake_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [kstmus_fake_muon, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakemuon_from_b_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeMuFromB_OSKMu',
        prescale=0.01,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    kstmus_fake_muon = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name='rd_kpi_fake_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [kstmus_fake_muon, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakekaon_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeK_SSKMu',
        prescale=0.05,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name='rd_kpi_fake_kaon_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_fake_kaons_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakekaon_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakeK_OSKMu',
        prescale=0.15,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name='rd_kpi_fake_kaon_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_fake_kaons_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakepion_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakePi_SSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name='rd_kpi_fake_pion_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name='rd_make_bd_to_kpimutau_fake_pions_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautomu_fakepion_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToMu_FakePi_OSKMu',
        prescale=0.75,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name='rd_kpi_fake_pion_muon_for_btosmutau_{hash}')
    bd = btosmutau_exclusive.make_bd(
        kstmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name='rd_make_bd_to_kpimutau_fake_pions_{hash}')
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_oskmu_line(name='Hlt2RD_BuToKTauMu_TauToMu_OSKMu',
                                  prescale=0.5,
                                  persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus, muons_from_tau, pvs, decay_descriptor="[B+ -> B0 mu+]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_sskmu_line(name='Hlt2RD_BuToKTauMu_TauToMu_SSKMu',
                                  prescale=0.5,
                                  persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu+ K+]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus, muons_from_tau, pvs, decay_descriptor="[B+ -> B0 mu-]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_same_sign_muons_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_SSMu_OSKMu',
        prescale=0.2,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name='rd_make_bu_to_kmutau_same_sign_muons_{hash}')

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_same_sign_muons_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_SSMu_SSKMu',
        prescale=0.2,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K-]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name='rd_make_bu_to_kmutau_same_sign_muons_{hash}')

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakemuon_from_tau_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeMuFromTau_OSKMu',
        prescale=0.005,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name='rd_make_bu_to_kmutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [kmus, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakemuon_from_tau_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeMuFromTau_SSKMu',
        prescale=0.005,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu+ K+]cc")
    bu = btosmutau_exclusive.make_bu(
        kmus,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name='rd_make_bu_to_kmutau_fake_muon_from_tau_{hash}')
    algs = rd_prefilter() + [kmus, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': fake_muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakemuon_from_b_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeMuFromB_OSKMu',
        prescale=0.003,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_for_bu_reverse_pid)
    kmus_fake_muon = btosmutau_exclusive.make_kmu(
        kaons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> mu- K+]cc",
        name='rd_kaon_fake_muon_for_btosmutau_{hash}')
    bu = btosmutau_exclusive.make_bu(
        kmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name='rd_make_bu_to_kmutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [kmus_fake_muon, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakemuon_from_b_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeMuFromB_SSKMu',
        prescale=0.003,
        persistreco=False):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_for_bu_reverse_pid)
    kmus_fake_muon = btosmutau_exclusive.make_kmu(
        kaons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> mu+ K+]cc",
        name='rd_kaon_fake_muon_for_btosmutau_{hash}')
    bu = btosmutau_exclusive.make_bu(
        kmus_fake_muon,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name='rd_make_bu_to_kmutau_fake_muon_from_b_{hash}')
    algs = rd_prefilter() + [kmus_fake_muon, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus_fake_muon
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakekaon_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeK_OSKMu',
        prescale=0.05,
        persistreco=False):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        fake_kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> mu- K+]cc",
        name='rd_fake_kaon_muon_for_btosmutau_{hash}')
    bu = btosmutau_exclusive.make_bu(
        kmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name='rd_make_bu_to_kmutau_fake_kaons_{hash}')

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautomu_fakekaon_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToMu_FakeK_SSKMu',
        prescale=0.1,
        persistreco=False):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        fake_kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> mu+ K+]cc",
        name='rd_fake_kaon_muon_for_btosmutau_{hash}')
    bu = btosmutau_exclusive.make_bu(
        kmus,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name='rd_make_bu_to_kmutau_fake_kaons_{hash}')

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'mu_f_tau': muons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES)

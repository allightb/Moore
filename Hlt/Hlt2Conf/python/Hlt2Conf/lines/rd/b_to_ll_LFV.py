###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Author: Olaf Massen
Contact: olaf.massen@cern.ch
Date: 17/01/2022
'''

######################################################################################
####                                                                              ####
#### The following exlusive lines Yb -> l l cover the LFV decays:                 ####
####                                                                              ####
#### B_(s)0 -> tau (-> 3pi nu_tau) mu                                             ####
#### B_(s)0 -> tau (-> 3pi nu_tau) e                                              ####
####                                                                              ####
######################################################################################

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from Hlt2Conf.lines.rd.builders.b_to_ll_LFV_builder import make_B_from_taumu, make_B_from_taue, make_taus, make_muons, make_electrons
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

hlt2_lines = {}


@register_line_builder(hlt2_lines)
@configurable
def Hlt2_Bs2TauMu_OS_ExclusiveLine(name='Hlt2RD_BsToTauMu_TauTo3Pi_OS',
                                   prescale=1):
    """
    HLT2 line for B(s) -> (tau+->pi+pi+pi-) mu- + CC

          and for B(s) -> (tau-->pi-pi-pi+) mu+ + CC
    """

    taus = make_taus()
    muons = make_muons()

    B_candidate = make_B_from_taumu(taus, muons, parent_id="B_s0")

    return Hlt2Line(
        name=name,
        prescale=prescale,
        extra_outputs=parent_isolation_output("B", B_candidate),
        algs=rd_prefilter() + [taus, B_candidate],
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(hlt2_lines)
@configurable
def Hlt2_Bs2TauE_OS_ExclusiveLine(name='Hlt2RD_BsToTauE_TauTo3Pi_OS',
                                  prescale=1):
    """
    HLT2 line for B(s) -> (tau+->pi+pi+pi-) e- + CC

          and for B(s) -> (tau-->pi-pi-pi+) e+ + CC
    """

    taus = make_taus()
    electrons = make_electrons()

    B_candidate = make_B_from_taue(taus, electrons, parent_id="B_s0")

    return Hlt2Line(
        name=name,
        prescale=prescale,
        extra_outputs=parent_isolation_output("B", B_candidate),
        algs=rd_prefilter() + [taus, B_candidate],
        monitoring_variables=_VRD_MONITORING_VARIABLES)

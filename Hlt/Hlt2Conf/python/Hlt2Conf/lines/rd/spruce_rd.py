# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines HLT2 lines for studies of rare decays.
"""

from . import b_to_multilepton_spruce
from . import b_to_xgamma_exclusive_spruce
from . import b_to_xll_spruce
from . import b_to_xll_spruce_hhll
from . import btosmutau_exclusive_spruce
from . import btostautau_exclusive_spruce

sprucing_lines = {}
sprucing_lines.update(b_to_multilepton_spruce.sprucing_lines)
sprucing_lines.update(b_to_xll_spruce.sprucing_lines)
sprucing_lines.update(b_to_xll_spruce_hhll.sprucing_lines)
sprucing_lines.update(b_to_xgamma_exclusive_spruce.sprucing_lines)
sprucing_lines.update(btosmutau_exclusive_spruce.sprucing_lines)
sprucing_lines.update(btostautau_exclusive_spruce.sprucing_lines)

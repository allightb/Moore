###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of rare baryonic lines.
Inspired by the StrippingRareBaryonicMuMu in Run 2.

The following modes are included at the moment:

+ Lb0 -> L0 pi+ pi- ll (Includes Xib0)
+ Lb0 -> p+ K- pi+ pi- ll (Includes Xib0)
+ Lb0 -> p+ pi- ll (includes Xib0)
+ Lb0 -> p KS0 pi- ll (includes Xib0)
+ Xib- -> Xi- ll
+ Omegab- -> Omega- ll
- Omegab- -> Xi- ll

where ll == {e-e+, mu-mu+} and identical channels with the dielectron. LLL {DDL/DDD removed for 2022} combinations are included.
Lb0 -> L0 ll (including Xib0) is currently implemented in b_to_v0ll
For the Xi- and Omega- reconstruction, the decay modes to Lambda pi- (Lambda K-) are used.
Both long and downstream tracks are allowed in the Xi/Omega and Lambda decay, which results in three track type categories (LLL, DDL and DDD).

TO DO:
- add downstream lines when UT will work 
"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from RecoConf.reconstruction_objects import make_pvs
from .builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES
from .builders.baryonic_builders import (
    make_dipion_for_lb, make_xim_to_lambda_pi_lll, make_omegam_to_lambda_k_lll,
    make_xim_to_lambda_pi_ddl, make_omegam_to_lambda_k_ddl,
    make_xim_to_lambda_pi_ddd, make_omegam_to_lambda_k_ddd, make_bhadron,
    make_bhadron_3body, make_bhadron_4body)
from .builders.rdbuilder_thor import make_rd_has_rich_detached_pions, make_rd_has_rich_detached_kaons, make_rd_has_rich_detached_protons, make_rd_detached_dielectron, make_rd_detached_dimuon, make_rd_lambda_lls, make_rd_ks0_lls, make_rd_lambda_dds, make_rd_ks0_dds
from Hlt2Conf.lines.rd.builders import rd_isolation
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}

######################
#### General cuts ####
######################

detached_pions_cuts = {
    "pt_min": 350 * MeV,
    "mipchi2dvprimary_min": 4.0,
    "pid": (F.PID_K < -1.),
}

detached_protons_cuts = {"mipchi2dvprimary_min": 9, "pt_min": 300 * MeV}

dimuon_cuts = {
    "parent_id": 'J/psi(1S)',
    "pt_dimuon_min": 0 * MeV,
    "pt_muon_min": 250 * MeV,
    "p_muon_min": 3000 * MeV,
    "ipchi2_muon_min": 4.,
    "pidmu_muon_min": 2.,
    "adocachi2cut_max": 20.,
    "bpvvdchi2_min": 20.,
    "vchi2pdof_max": 10.,
    "same_sign": False,
}

dielectron_cuts = {
    "vfaspfchi2ndof_max": 9.,
    "pid_e_min": 2.,
    "pt_e_min": 0.3 * GeV,
    "pt_diE_min": 0 * MeV
}

lambda_lls_cuts = {
    "mass_window": 35. * MeV,
    "vchi2pdof_max": 18.,
    "bpvvdchi2_min": 4.,
    "lambda_pt_min": 0.
}

lambda_dds_cuts = {
    "mass_window": 35. * MeV,
    "vchi2pdof_max": 18.,
    "bpvvdchi2_min": 4.,
    "lambda_pt_min": 0.
}

#############################
#### Lines with hyperons ####
#############################

### LL(L) lines ###


@register_line_builder(all_lines)
def Hlt2RD_LbToL0PipPimMuMu_LL(name="Hlt2RD_LbToL0PipPimMuMu_LL", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0 rho(770)0]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5700. * MeV, **dimuon_cuts)
    lambdas = make_rd_lambda_lls(**lambda_lls_cuts)

    dipion = make_dipion_for_lb(am_max=5770. * MeV)

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipimumu_ll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=lambdas,
        hadron2=dipion,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, lambdas, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKS0PimMuMu_LL(name="Hlt2RD_LbToPpKS0PimMuMu_LL", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ KS0 pi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5500. * MeV, **dimuon_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    ks0 = make_rd_ks0_lls()

    lb0 = make_bhadron_4body(
        name="lb0_to_pkspimumu_ll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=protons,
        hadron2=ks0,
        hadron3=pions,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=3)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, ks0, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_XibmToXimMuMu_LLL(name="Hlt2RD_XibmToXimMuMu_LLL", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5800. * MeV, **dimuon_cuts)
    xis = make_xim_to_lambda_pi_lll(name="xim_lll", pvs=pvs)
    xib = make_bhadron(
        name="xib_to_ximumu_lll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=15 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xis, xib],
        extra_outputs=parent_isolation_output("Xib", xib),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamMuMu_LLL(name="Hlt2RD_OmegabmToOmegamMuMu_LLL",
                                   prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=6000. * MeV, **dimuon_cuts)
    omegas = make_omegam_to_lambda_k_lll(name="omegam_lll", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_omegamumu_lll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToXimMuMu_LLL(name="Hlt2RD_OmegabmToXimMuMu_LLL",
                                prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=6300. * MeV, **dimuon_cuts)
    xim = make_xim_to_lambda_pi_lll(name="xim_lll", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_ximmumu_lll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xim,
        descriptor=descriptor,
        vtx_sep_min=15 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xim, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToL0PipPimEE_LL(name="Hlt2RD_LbToL0PipPimEE_LL", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0 rho(770)0]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5700 * MeV, **dielectron_cuts)
    lambdas = make_rd_lambda_lls(
        mass_window=35. * MeV,
        vchi2pdof_max=18.,
        bpvvdchi2_min=4.,
        lambda_pt_min=0.,
    )

    dipion = make_dipion_for_lb()

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipiee_ll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=lambdas,
        hadron2=dipion,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, lambdas, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKS0PimEE_LL(name="Hlt2RD_LbToPpKS0PimEE_LL", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ KS0 pi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5550 * MeV, **dielectron_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    ks0 = make_rd_ks0_lls()

    lb0 = make_bhadron_4body(
        name="lb0_to_pkpipiee_ll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=protons,
        hadron2=ks0,
        hadron3=pions,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, ks0, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


def Hlt2RD_XibmToXimEE_LLL(name="Hlt2RD_XibmToXimEE_LLL", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)

    xis = make_xim_to_lambda_pi_lll(name="xim_lll", pvs=pvs)
    xib = make_bhadron(
        name="xib_to_xiee_lll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=15 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, xis, xib],
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamEE_LLL(name="Hlt2RD_OmegabmToOmegamEE_LLL",
                                 prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()

    omegas = make_omegam_to_lambda_k_lll(name="omegam_lll", pvs=pvs)
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)
    omegab = make_bhadron(
        name="omegab_to_omegaee_lll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=15 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


### DDL lines ###


@register_line_builder(all_lines)
def Hlt2RD_XibmToXimMuMu_DDL(name="Hlt2RD_XibmToXimMuMu_DDL", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5700. * MeV, **dimuon_cuts)
    xis = make_xim_to_lambda_pi_ddl(name="xim_ddl", pvs=pvs)
    xib = make_bhadron(
        name="xib_to_ximumu_ddl",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xis, xib],
        extra_outputs=parent_isolation_output("Xib", xib),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamMuMu_DDL(name="Hlt2RD_OmegabmToOmegamMuMu_DDL",
                                   prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5300. * MeV, **dimuon_cuts)
    omegas = make_omegam_to_lambda_k_ddl(name="omegam_ddl", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_omegamumu_ddl",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToXimMuMu_DDL(name="Hlt2RD_OmegabmToXimMuMu_DDL",
                                prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=6300. * MeV, **dimuon_cuts)
    xim = make_xim_to_lambda_pi_ddl(name="xim_ddl", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_ximmumu_ddl",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xim,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xim, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_XibmToXimEE_DDL(name="Hlt2RD_XibmToXimEE_DDL", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)

    xis = make_xim_to_lambda_pi_ddl(name="xim_ddl", pvs=pvs)
    xib = make_bhadron(
        name="xib_to_xiee_ddl",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, xis, xib],
        extra_outputs=parent_isolation_output("Xib", xib),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamEE_DDL(name="Hlt2RD_OmegabmToOmegamEE_DDL",
                                 prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)
    omegas = make_omegam_to_lambda_k_ddl(name="omegam_ddl", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_omegaee_ddl",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


### DD(D) lines ###


@register_line_builder(all_lines)
def Hlt2RD_LbToL0PipPimMuMu_DD(name="Hlt2RD_LbToL0PipPimMuMu_DD",
                               prescale=1.0):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0 rho(770)0]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5700. * MeV, **dimuon_cuts)
    lambdas = make_rd_lambda_dds(**lambda_dds_cuts)

    dipion = make_dipion_for_lb(am_max=5770. * MeV)

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipimumu_dd",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=lambdas,
        hadron2=dipion,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, lambdas, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKS0PimMuMu_DD(name="Hlt2RD_LbToPpKS0PimMuMu_DD", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ KS0 pi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5500. * MeV, **dimuon_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    ks0 = make_rd_ks0_dds()

    lb0 = make_bhadron_4body(
        name="lb0_to_pkspimumu_ll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=protons,
        hadron2=ks0,
        hadron3=pions,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=3)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, ks0, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_XibmToXimMuMu_DDD(name="Hlt2RD_XibmToXimMuMu_DDD", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5600. * MeV, **dimuon_cuts)
    xis = make_xim_to_lambda_pi_ddd(name="xim_ddd", pvs=pvs)
    xib = make_bhadron(
        name="xib_to_ximumu_ddd",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xis, xib],
        extra_outputs=parent_isolation_output("Xib", xib),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamMuMu_DDD(name="Hlt2RD_OmegabmToOmegamMuMu_DDD",
                                   prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5700. * MeV, **dimuon_cuts)
    omegas = make_omegam_to_lambda_k_ddd(name="omegam_ddd", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_omegamumu_ddd",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=25 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToXimMuMu_DDD(name="Hlt2RD_OmegabmToXimMuMu_DDD",
                                prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=6300. * MeV, **dimuon_cuts)
    xim = make_xim_to_lambda_pi_ddd(name="xim_ddd", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_ximmumu_ddd",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron=xim,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, xim, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToL0PipPimEE_DD(name="Hlt2RD_LbToL0PipPimEE_DD", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0 rho(770)0]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5700 * MeV, **dielectron_cuts)
    lambdas = make_rd_lambda_dds(**lambda_dds_cuts)

    dipion = make_dipion_for_lb()

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipiee_dd",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=lambdas,
        hadron2=dipion,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, lambdas, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKS0PimEE_DD(name="Hlt2RD_LbToPpKS0PimEE_DD", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ KS0 pi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    ks0 = make_rd_ks0_dds()

    lb0 = make_bhadron_4body(
        name="lb0_to_pkpipiee_dd",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=protons,
        hadron2=ks0,
        hadron3=pions,
        descriptor=descriptor,
        vtx_sep_min=20 * mm,
        daughter_index=3)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, ks0, lb0],
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_XibmToXimEE_DDD(name="Hlt2RD_XibmToXimEE_DDD", prescale=1):
    descriptor = "[Xi_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()

    xis = make_xim_to_lambda_pi_ddd(name="xim_ddd", pvs=pvs)
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)
    xib = make_bhadron(
        name="xib_to_xiee_ddd",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=xis,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, xis, xib],
        extra_outputs=parent_isolation_output("Xib", xib),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToOmegamEE_DDD(name="Hlt2RD_OmegabmToOmegamEE_DDD",
                                 prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Omega-]cc"
    pvs = make_pvs()

    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)

    omegas = make_omegam_to_lambda_k_ddd(name="omegam_ddd", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_omegaee_ddd",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=omegas,
        descriptor=descriptor,
        vtx_sep_min=25 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, omegas, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def Hlt2RD_OmegabmToXimEE_DDD(name="Hlt2RD_OmegabmToXimEE_DDD", prescale=1):
    descriptor = "[Omega_b- -> J/psi(1S) Xi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)
    xim = make_xim_to_lambda_pi_ddd(name="xim_ddd", pvs=pvs)
    omegab = make_bhadron(
        name="omegab_to_ximee_ddd",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron=xim,
        descriptor=descriptor,
        vtx_sep_min=30 * mm,
        daughter_index=2)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, xim, omegab],
        extra_outputs=parent_isolation_output("Omegab", omegab),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


################################
#### Lines without hyperons ####
################################


@register_line_builder(all_lines)
def Hlt2RD_LbToPpPimMuMu(name="Hlt2RD_LbToPpPimMuMu", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ pi-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=6000. * MeV, **dimuon_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipimumu_ll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=protons,
        hadron2=pions,
        descriptor=descriptor)

    p_p_child = rd_isolation.find_in_decay(input=lb0, id='p+')
    pi_m_child = rd_isolation.find_in_decay(input=lb0, id='pi-')
    mu_p_child = rd_isolation.find_in_decay(input=lb0, id='mu+')
    mu_m_child = rd_isolation.find_in_decay(input=lb0, id='mu-')

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=['lb0'],
        candidates=[lb0],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=['p+', 'pi-', 'mu+', 'mu-'],
        candidates=[p_p_child, pi_m_child, mu_p_child, mu_m_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, lb0],
        monitoring_variables=_RD_MONITORING_VARIABLES,
        extra_outputs=iso_parts,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKmPipPimMuMu(name="Hlt2RD_LbToPpKmPipPimMuMu", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ rho(770)0 K-]cc"
    pvs = make_pvs()
    make_dimuon = make_rd_detached_dimuon(
        am_min=200. * MeV, am_max=5400. * MeV, **dimuon_cuts)

    dipion = make_dipion_for_lb(am_min=250. * MeV, am_max=5500. * MeV)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    kaons = make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pt_min=500 * MeV)

    lb0 = make_bhadron_4body(
        name="lb0_to_pkpipimumu_ll",
        pvs=pvs,
        dilepton=make_dimuon,
        hadron1=protons,
        hadron2=dipion,
        hadron3=kaons,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dimuon, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpKmPipPimEE(name="Hlt2RD_LbToPpKmPipPimEE", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ rho(770)0 K-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)
    dipion = make_dipion_for_lb(am_min=200. * MeV, am_max=4100. * MeV)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    kaons = make_rd_has_rich_detached_kaons(**detached_protons_cuts)

    lb0 = make_bhadron_4body(
        name="lb0_to_pkpipiee_ll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=protons,
        hadron2=dipion,
        hadron3=kaons,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, dipion, lb0],
        extra_outputs=parent_isolation_output("Lambdab", lb0),
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale)


@register_line_builder(all_lines)
def Hlt2RD_LbToPpPimEE(name="Hlt2RD_LbToPpPimEE", prescale=1):
    descriptor = "[Lambda_b0 -> J/psi(1S) p+ pi-]cc"
    pvs = make_pvs()
    make_dielectron = make_rd_detached_dielectron(
        am_min=0 * MeV, am_max=5500 * MeV, **dielectron_cuts)

    protons = make_rd_has_rich_detached_protons(**detached_protons_cuts)

    pions = make_rd_has_rich_detached_pions(**detached_pions_cuts)

    lb0 = make_bhadron_3body(
        name="lb0_to_l0pipimumu_ll",
        pvs=pvs,
        dilepton=make_dielectron,
        hadron1=protons,
        hadron2=pions,
        descriptor=descriptor)

    p_p_child = rd_isolation.find_in_decay(input=lb0, id='p+')
    pi_m_child = rd_isolation.find_in_decay(input=lb0, id='pi-')
    e_p_child = rd_isolation.find_in_decay(input=lb0, id='e+')
    e_m_child = rd_isolation.find_in_decay(input=lb0, id='e-')

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=['lb0'],
        candidates=[lb0],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=['p+', 'pi-', 'e+', 'e-'],
        candidates=[p_p_child, pi_m_child, e_p_child, e_m_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [make_dielectron, lb0],
        monitoring_variables=_RD_MONITORING_VARIABLES,
        extra_outputs=iso_parts,
        prescale=prescale)

###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s mu tau HLT2 lines.
Final states built are (all taus decay to electrons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) mu+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) mu+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) mu- tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake kaon and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake muon from the Bs and its charge conjugate
     - Bs -> phi(-> K+ K-) mu+ tau- with a fake muon from the tau and its charge conjugate
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) mu+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K+) mu+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) mu- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau- with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) mu+ tau- with a fake proton
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake proton
     - Lb -> Lambda(-> K+ K-) mu+ tau- with a fake muon
     - Lb -> Lambda(-> p+ K-) mu- tau+ with a fake muon
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) mu+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi+) mu+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi+) mu- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu+ tau- with a fake muon and its charge conjugate
     - Bd -> K*(-> K+ pi-) mu- tau+ with a fake muon and its charge conjugate
Note a: fake particles are obtained through reversing the PID requirements
Note b: particles stemming directly from the b-hadron are combined with a ParticleCombiner. This is not intended to represent a given particle (hence the loose mass requirements)
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV

import Functors as F

from Hlt2Conf.lines.rd.builders import rdbuilder_thor
from Hlt2Conf.lines.rd.builders import btosmutau_exclusive
from Hlt2Conf.lines.rd.builders import btosetau_exclusive
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_and_children_isolation

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}

kwargs_bd = {"bpvfdchi2_min": 75}

kwargs_protons = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_P > 4, ~F.ISMUON),
}
kwargs_protons_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_P < 4),
}

kwargs_kaons = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON),
}

kwargs_kaons_reverse_pid = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_bu = {
    "pt_min": 750 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 4, F.PID_K - F.PID_P > 0, ~F.ISMUON),
}

kwargs_kaons_for_bu_reverse_pid = {
    "pt_min": 750 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_kstar = {
    "p_min": 3000 * MeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON)
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250. * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K < 4)
}

kwargs_pions = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K < 0, ~F.ISMUON)
}
kwargs_pions_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K > 0)
}

kwargs_electrons_from_tau = {
    "p_min": 3000 * MeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_E > 5, ~F.ISMUON)
}
kwargs_electrons_from_tau_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 0 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_E < 5)
}

kwargs_muons = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_MU > 1, F.ISMUON),
    "pt_min": 500 * MeV
}

kwargs_muons_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_MU < 1),
    "pt_min": 500 * MeV
}

kwargs_muons_for_bu = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_MU > 1, F.ISMUON),
    "pt_min": 750 * MeV
}

kwargs_muons_for_bu_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_MU < 1),
    "pt_min": 750 * MeV
}


@register_line_builder(all_lines)
def bstophimutau_tautoe_line(name='Hlt2RD_BsToKKTauMu_TauToE', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(kaons, kaons, muons, pvs)
    bs = btosetau_exclusive.make_bs(phimus, electrons_from_tau, pvs)
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_same_sign_kaons_sskmu_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_SSK_SSKmu', prescale=0.8):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ K+ mu+]cc",
        name="rd_same_sign_dikaon_muon_for_btosmutau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phimus,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaumu_same_sign_kaons_{hash}")
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_same_sign_kaons_oskmu_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_SSK_OSKmu', prescale=0.4):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> K- K- mu+]cc",
        name="rd_same_sign_dikaon_muon_for_btosmutau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phimus,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaumu_same_sign_kaons_{hash}")
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_same_sign_leptons_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_SSl', prescale=0.75):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons, kaons, muons, pvs, decay_descriptor="[B0 -> K+ K- mu+]cc")
    bs = btosetau_exclusive.make_bs(
        phimus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bs_to_kktaumu_same_sign_leptons_{hash}")
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_fakeelectron_from_tau_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_FakeMuonFromTau', prescale=0.03):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    phimus = btosmutau_exclusive.make_phimu(kaons, kaons, muons, pvs)
    bs = btosetau_exclusive.make_bs(
        phimus,
        fake_electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaumu_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_fakemuon_from_b_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_FakeMuonFromB', prescale=0.02):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    phimus_fake_muon = btosmutau_exclusive.make_phimu(
        kaons,
        kaons,
        fake_muons,
        pvs,
        name="rd_dikaon_fake_muon_for_btosmutau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phimus_fake_muon,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaumu_fake_muon_from_b_{hash}")
    algs = rd_prefilter() + [phimus_fake_muon, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophimutau_tautoe_fakekaon_line(
        name='Hlt2RD_BsToKKTauMu_TauToE_FakeKaon', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    phimus = btosmutau_exclusive.make_phimu(
        kaons,
        fake_kaons,
        muons,
        pvs,
        name="rd_fake_dikaon_muon_for_btosmutau_{hash}")
    bs = btosetau_exclusive.make_bs(
        phimus,
        electrons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaumu_fake_kaons_{hash}")
    algs = rd_prefilter() + [phimus, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phimu': phimus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_sspmu_line(name='Hlt2RD_LbToPKTauMu_TauToE_SSpmu',
                                  prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_ospmu_line(name='Hlt2RD_LbToPKTauMu_TauToE_OSpmu',
                                  prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_same_sign_pk_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_SSpK_SSpmu', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ mu+]cc",
        name="rd_same_sign_pk_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_same_sign_pk_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_same_sign_pk_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_SSpK_OSpmu', prescale=0.1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ mu-]cc",
        name="rd_same_sign_pk_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_same_sign_pk_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_same_sign_leptons_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_SSl_SSpmu', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_same_sign_leptons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_same_sign_leptons_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_SSl_OSpmu', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_same_sign_leptons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakeelectron_from_tau_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeMuonFromTau_SSpmu',
        prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu+]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakeelectron_from_tau_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeMuonFromTau_OSpmu',
        prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons, protons, muons, pvs, decay_descriptor="[B0 -> p+ K- mu-]cc")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakemuon_from_b_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeMuonFromB_SSpmu', prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    pkmus_fake_muon = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name="rd_pk_fake_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_fake_muon_from_b_{hash}")
    algs = rd_prefilter() + [pkmus_fake_muon, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakemuon_from_b_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeMuonFromB_OSpmu', prescale=0.015):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    pkmus_fake_muon = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name="rd_pk_fake_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_fake_muon_from_b_{hash}")
    algs = rd_prefilter() + [pkmus_fake_muon, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakeproton_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeProton_SSpmu', prescale=0.08):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name="rd_pk_fake_proton_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_fake_protons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakeproton_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeProton_OSpmu', prescale=0.05):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name="rd_pk_fake_proton_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_fake_protons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakekaon_sspmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeKaon_SSpmu', prescale=0.15):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu+]cc",
        name="rd_pk_fake_kaon_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e-]cc",
        name="rd_make_lb_to_pkmutau_fake_kaons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopkmutau_tautoe_fakekaon_ospmu_line(
        name='Hlt2RD_LbToPKTauMu_TauToE_FakeKaon_OSpmu', prescale=0.01):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    pkmus = btosmutau_exclusive.make_pkmu(
        kaons,
        protons,
        muons,
        pvs,
        decay_descriptor="[B0 -> p+ K- mu-]cc",
        name="rd_pk_fake_kaon_muon_for_btosmutau_{hash}")
    lb = btosetau_exclusive.make_lb(
        pkmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 e+]cc",
        name="rd_make_lb_to_pkmutau_fake_kaons_{hash}")
    algs = rd_prefilter() + [pkmus, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pKmu': pkmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_sskmu_line(name='Hlt2RD_BdToKPiTauMu_TauToE_SSKmu',
                                   prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_oskmu_line(name='Hlt2RD_BdToKPiTauMu_TauToE_OSKmu',
                                   prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_same_sign_kpi_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_SSKpi_SSKmu', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ mu+]cc",
        name="rd_same_sign_kpi_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_same_sign_kpi_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_same_sign_kpi_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_SSKpi_OSKmu', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ mu-]cc",
        name="rd_same_sign_kpi_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_same_sign_kpi_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_same_sign_leptons_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_SSl_SSKmu', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_same_sign_leptons_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_same_sign_leptons_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_SSl_OSKmu', prescale=0.3):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_same_sign_leptons_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakeelectron_from_tau_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeMuonFromTau_SSKmu',
        prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu+]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_fake_electron_from_tau_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakeelectron_from_tau_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeMuonFromTau_OSKmu',
        prescale=0.015):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons, pions, muons, pvs, decay_descriptor="[B0 -> K+ pi- mu-]cc")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_fake_electron_from_tau_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakemuon_from_b_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeMuonFromB_SSKmu', prescale=0.01):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    kstmus_fake_muon = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name="rd_kpi_fake_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_fake_muon_from_b_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus_fake_muon, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakemuon_from_b_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeMuonFromB_OSKmu', prescale=0.008):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_reverse_pid)
    kstmus_fake_muon = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name="rd_kpi_fake_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_fake_muon_from_b_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus_fake_muon, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakekaon_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeKaon_SSKmu', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name="rd_kpi_fake_kaon_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_fake_kaons_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakekaon_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakeKaon_OSKmu', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name="rd_kpi_fake_kaon_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_fake_kaons_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakepion_sskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakePion_SSKmu', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu+]cc",
        name="rd_kpi_fake_pion_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e-]cc",
        name="rd_make_bd_to_kpimutau_fake_pions_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtokstmutau_tautoe_fakepion_oskmu_line(
        name='Hlt2RD_BdToKPiTauMu_TauToE_FakePion_OSKmu', prescale=0.15):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons)
    kstmus = btosmutau_exclusive.make_kstmu(
        kaons,
        pions,
        muons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- mu-]cc",
        name="rd_kpi_fake_pion_muon_for_btosmutau_{hash}")
    bd = btosetau_exclusive.make_bd(
        kstmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 e+]cc",
        name="rd_make_bd_to_kpimutau_fake_pions_{hash}",
        **kwargs_bd)
    algs = rd_prefilter() + [kstmus, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bd': bd,
            'Kstmu': kstmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_oskmu_line(name='Hlt2RD_BuToKTauMu_TauToE_OSKmu',
                                 prescale=0.3):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kmus, electrons_from_tau, pvs, decay_descriptor="[B+ -> B0 e+]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_sskmu_line(name='Hlt2RD_BuToKTauMu_TauToE_SSKmu',
                                 prescale=0.3):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu+ K+]cc")
    bu = btosetau_exclusive.make_bu(
        kmus, electrons_from_tau, pvs, decay_descriptor="[B+ -> B0 e-]cc")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_same_sign_leptons_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_SSl_OSKmu', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_kmutau_same_sign_leptons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_same_sign_leptons_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_SSl_SSKmu', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K-]cc")
    bu = btosetau_exclusive.make_bu(
        kmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_kmutau_same_sign_leptons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakeelectron_from_tau_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeMuonFromTau_OSKmu', prescale=0.005):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu- K+]cc")
    bu = btosetau_exclusive.make_bu(
        kmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_kmutau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [kmus, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakeelectron_from_tau_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeMuonFromTau_SSKmu', prescale=0.005):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    fake_electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau_reverse_pid)
    kmus = btosmutau_exclusive.make_kmu(
        kaons, muons, pvs, decay_descriptor="[B0 -> mu+ K+]cc")
    bu = btosetau_exclusive.make_bu(
        kmus,
        fake_electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_kmutau_fake_electron_from_tau_{hash}")
    algs = rd_prefilter() + [kmus, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': fake_electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakemuon_from_b_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeMuonFromB_OSKmu', prescale=0.005):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_for_bu_reverse_pid)
    kmus_fake_muon = btosmutau_exclusive.make_kmu(
        kaons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> mu- K+]cc",
        name="rd_kaon_fake_muon_for_btosmutau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_kmutau_fake_muon_from_b_{hash}")
    algs = rd_prefilter() + [kmus_fake_muon, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakemuon_from_b_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeMuonFromB_SSKmu', prescale=0.005):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    fake_muons = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_for_bu_reverse_pid)
    kmus_fake_muon = btosmutau_exclusive.make_kmu(
        kaons,
        fake_muons,
        pvs,
        decay_descriptor="[B0 -> mu+ K+]cc",
        name="rd_kaon_fake_muon_for_btosmutau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kmus_fake_muon,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_kmutau_fake_muon_from_b_{hash}")
    algs = rd_prefilter() + [kmus_fake_muon, bu]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus_fake_muon
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakekaon_oskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeKaon_OSKmu', prescale=0.02):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        fake_kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> mu- K+]cc",
        name="rd_fake_kaon_muon_for_btosmutau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e+]cc",
        name="rd_make_bu_to_kmutau_fake_kaons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butokmutau_tautoe_fakekaon_sskmu_line(
        name='Hlt2RD_BuToKTauMu_TauToE_FakeKaon_SSKmu', prescale=0.03):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    electrons_from_tau = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_from_tau)
    muons = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_for_bu)
    kmus = btosmutau_exclusive.make_kmu(
        fake_kaons,
        muons,
        pvs,
        decay_descriptor="[B0 -> mu+ K+]cc",
        name="rd_fake_kaon_muon_for_btosmutau_{hash}")
    bu = btosetau_exclusive.make_bu(
        kmus,
        electrons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 e-]cc",
        name="rd_make_bu_to_kmutau_fake_kaons_{hash}")

    iso_parts = parent_and_children_isolation(
        parents={
            'Bu': bu,
            'Kmus': kmus
        },
        decay_products={'e_f_tau': electrons_from_tau})

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kmus, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of spruce lines for the RD working group.
- Single-vertex Detached -> 4mu with persist-reco 
- Single-vertex Detached -> 4e with persist-reco 
- Single-vertex Detached -> 2e2mu with persist-reco 

"""
import Functors as F

from Moore.config import SpruceLine, register_line_builder

from RecoConf.reconstruction_objects import upfront_reconstruction

from .builders.Nmu_builders import make_b_4l, make_b_2mu2e
from .builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_detached_dimuon,
    make_rd_detached_dielectron, make_rd_detached_electrons)
from Hlt2Conf.lines.rd.builders import rd_isolation

sprucing_lines = {}


@register_line_builder(sprucing_lines)
def SpruceRD_Displaced4Mu_incl_line(name="SpruceRD_Displaced4Mu_Incl",
                                    prescale=1):
    """
    Displaced->4mu inclusive selection
    """

    lepton = make_rd_detached_muons(
        pid=F.require_all(F.PID_MU > 0, F.ISMUON), mipchi2dvprimary_min=16)
    b = make_b_4l(
        lepton, descriptor='B0 -> mu+ mu+ mu- mu-', name="make_rd_b24mu")

    mu_ids = ["mu+", "mu-"]
    iso_muons = [
        rd_isolation.find_in_decay(input=b, id=mu_id) for mu_id in mu_ids
    ]

    iso_outputs = rd_isolation.select_parts_for_isolation(
        names=mu_ids,
        candidates=iso_muons,
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    iso_outputs += rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[b],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [lepton, b],
        prescale=prescale,
        extra_outputs=iso_outputs,
        hlt2_filter_code="Hlt2RD_Displaced4Mu_InclDecision",
        tagging_particles=True)


@register_line_builder(sprucing_lines)
def SpruceRD_Displaced4E_incl_line(name="SpruceRD_Displaced4E_Incl",
                                   prescale=1):
    """
    Displaced->mumuee inclusive selection
    """

    lepton = make_rd_detached_electrons(
        pid=(F.PID_E > 2), mipchi2dvprimary_min=16)
    b = make_b_4l(lepton, descriptor='B0 -> e+ e+ e- e-', name="make_rd_b24e")

    e_ids = ["e+", "e-"]
    iso_electrons = [
        rd_isolation.find_in_decay(input=b, id=e_id) for e_id in e_ids
    ]

    iso_outputs = rd_isolation.select_parts_for_isolation(
        names=e_ids,
        candidates=iso_electrons,
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    iso_outputs += rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[b],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [lepton, b],
        prescale=prescale,
        extra_outputs=iso_outputs,
        hlt2_filter_code="Hlt2RD_Displaced4E_InclDecision",
        tagging_particles=True)


@register_line_builder(sprucing_lines)
def SpruceRD_DisplacedMuMuEE_incl_line(name="SpruceRD_Displaced2Mu2E_Incl",
                                       prescale=1):
    """
    Displaced->mumuee inclusive selection
    """

    dilepton1 = make_rd_detached_dimuon(
        pidmu_muon_min=0, ipchi2_muon_min=16, parent_id="J/psi(1S)")
    dilepton2 = make_rd_detached_dielectron(
        pid_e_min=2, ipchi2_e_min=16, parent_id="phi(1020)")
    b = make_b_2mu2e(
        dilepton1, dilepton2, descriptor='B0 -> J/psi(1S) phi(1020)')

    l_ids = ["e+", "e-", "mu+", "mu-"]
    iso_leptons = [
        rd_isolation.find_in_decay(input=b, id=l_id) for l_id in l_ids
    ]

    iso_outputs = rd_isolation.select_parts_for_isolation(
        names=l_ids,
        candidates=iso_leptons,
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    iso_outputs += rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[b],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [dilepton1, dilepton2, b],
        prescale=prescale,
        extra_outputs=iso_outputs,
        hlt2_filter_code="Hlt2RD_DisplacedMuMuEE_InclDecision",
        tagging_particles=True)

###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s tau tau HLT2 lines.
Final states built are (all taus decay to electrons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) tau+ tau-
     - Bs -> phi(-> K+ K+) tau+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K-) tau+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) tau+ tau- with a fake kaon
     - Bs -> phi(-> K+ K-) tau+ tau- with a fake electron
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) tau+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) tau+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) tau+ tau+, Lambda(-> p+ K-) tau- tau- and their charge conjugates
     - Lb -> Lambda(-> p+ K-) tau+ tau- with a fake kaon
     - Lb -> Lambda(-> p+ K-) tau+ tau- with a fake proton
     - Lb -> Lambda(-> K+ K-) tau+ tau- with a fake electron
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) tau+ tau-
     - Bd -> K*(-> K+ pi+) tau+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) tau+ tau+, K*(-> K+ pi-) tau- tau- and their charge conjugates
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake kaon
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake pion
     - Bd -> K*(-> K+ pi-) tau+ tau- with a fake electron
 4. Bs decays to K* K*
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau-
     - Bs -> K*(-> K+ pi-) K*(-> K+ pi-) tau+ tau- and its charge conjugate
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau+ and its charge conjugate
     - Bs -> K*(-> K+ pi-) K~*(-> K- pi+) tau+ tau- with a fake electron
 5. B+ decays to K+
     - B+ -> K+ tau+ tau-
     - B+ -> K+ tau- tau- and its charge conjugate
     - B+ -> K+ tau+ tau+ and its charge conjugate
     - B+ -> K+ tau+ tau- with a fake electron
     - B+ -> K+ tau+ tau- with a fake kaon
Note a: fake particles are obtained through reversing the PID requirements
Note b: electrons from the taus are combined with a ParticleCombiner with DecayDescriptor [D0 -> e+ e-]cc
Note c: in general, the dihadron is mainly used as a way to add more cuts on the particle combination, and is not intended to represent a given particle (hence the loose mass requirements)
"""
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import MeV, GeV

import Functors as F

from Hlt2Conf.lines.rd.builders import rdbuilder_thor
from Hlt2Conf.lines.rd.builders import btostautau_exclusive
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_and_children_isolation

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES

all_lines = {}

kwargs_phi = {"comb_pt_min": 750 * MeV, "bpvfdchi2_min": 50, "dira_min": 0.995}

kwargs_kst = {
    "comb_pt_min": 1250 * MeV,
    "dira_min": 0.998,
    "bpvfdchi2_min": 150
}

kwargs_pk = {"comb_pt_min": 750 * MeV, "bpvfdchi2_min": 50, "dira_min": 0.995}

kwargs_bu = {"vchi2pdof_max": 50}

kwargs_kst_bs2kstkst = {
    "comb_pt_min": 600 * MeV,
    "vchi2pdof_max": 4,
    "bpvfdchi2_min": 16
}

kwargs_kaons = {
    "p_min": 3 * GeV,
    "pt_min": 500. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON)
}
kwargs_kaons_reverse_pid = {
    "p_min": 3 * GeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_K < 4)
}

kwargs_kaons_for_bu = {
    "p_min": 5 * GeV,
    "pt_min": 750 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 5, F.PID_K - F.PID_P > 2, ~F.ISMUON)
}
kwargs_kaons_for_bu_reverse_pid = {
    "p_min": 5 * GeV,
    "pt_min": 750 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 5)
}

kwargs_kaons_for_kstar = {
    "p_min": 5 * GeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 5, F.PID_K - F.PID_P > 0, ~F.ISMUON)
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 5 * GeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 5)
}

kwargs_pions = {
    "p_min": 5 * GeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K < 0, ~F.ISMUON)
}
kwargs_pions_reverse_pid = {
    "p_min": 5 * GeV,
    "pt_min": 750. * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K > 0)
}
kwargs_protons = {
    "p_min": 3 * GeV,
    "pt_min": 500. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_P > 4, ~F.ISMUON)
}
kwargs_protons_reverse_pid = {
    "p_min": 3 * GeV,
    "pt_min": 500. * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_P < 4)
}
kwargs_electrons = {
    "p_min": 3 * GeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_E > 4, ~F.ISMUON)
}
kwargs_electrons_reverse_pid = {
    "p_min": 3 * GeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_E < 4)
}


@register_line_builder(all_lines)
def bstophitautau_tautoe_line(name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToE',
                              prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs, **kwargs_phi)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bs = btostautau_exclusive.make_bs(phis, dielectrons, pvs)
    algs = rd_prefilter() + [dielectrons, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phi': phis
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautoe_same_sign_kaons_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToE_SSK', prescale=0.75):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phis = btostautau_exclusive.make_phi(
        kaons,
        kaons,
        pvs,
        decay_descriptor="[phi(1020) -> K+ K+]cc",
        name="rd_same_sign_dikaons_for_btostautau_{hash}",
        **kwargs_phi)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bs = btostautau_exclusive.make_bs(
        phis,
        dielectrons,
        pvs,
        name="rd_make_bs_to_kktautau_same_sign_kaons_{hash}")
    algs = rd_prefilter() + [dielectrons, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phi': phis
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautoe_same_sign_electrons_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToE_SSe', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs, **kwargs_phi)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e+ e+]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bs = btostautau_exclusive.make_bs(
        phis,
        dielectrons,
        pvs,
        name="rd_make_bs_to_kktautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phi': phis
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautoe_fakeelectron_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToE_FakeElectron',
        prescale=0.04):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    phis = btostautau_exclusive.make_phi(kaons, kaons, pvs, **kwargs_phi)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        fake_electrons,
        pvs,
        name="rd_fake_dielectrons_for_btostautau_{hash}")
    bs = btostautau_exclusive.make_bs(
        phis,
        dielectrons,
        pvs,
        name="rd_make_bs_to_kktautau_fake_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, phis, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phi': phis
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstophitautau_tautoe_fakekaon_line(
        name='Hlt2RD_BsToPhiTauTau_PhiToKK_TauToE_FakeKaon', prescale=0.15):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_reverse_pid)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phis = btostautau_exclusive.make_phi(
        kaons,
        fake_kaons,
        pvs,
        name="rd_fake_dikaons_for_btostautau_{hash}",
        **kwargs_phi)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bs = btostautau_exclusive.make_bs(
        phis,
        dielectrons,
        pvs,
        name="rd_make_bs_to_kktautau_fake_kaons_{hash}")
    algs = rd_prefilter() + [dielectrons, phis, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'phi': phis
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_line(name='Hlt2RD_LbToPKTauTau_TauToE', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs, **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    lb = btostautau_exclusive.make_lb(pks, dielectrons, pvs)
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_same_sign_pK_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_SSpK', prescale=0.2):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(
        protons,
        kaons,
        pvs,
        decay_descriptor="[Lambda(1520)0 -> p+ K+]cc",
        name="rd_same_sign_pk_for_btostautau_{hash}",
        **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    lb = btostautau_exclusive.make_lb(
        pks,
        dielectrons,
        pvs,
        name="rd_make_lb_to_pktautau_same_sign_pk_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_same_sign_electrons_sspe_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_SSe_SSpe', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs, **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e+ e+]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    lb = btostautau_exclusive.make_lb(
        pks,
        dielectrons,
        pvs,
        name="rd_make_lb_to_pktautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_same_sign_electrons_ospe_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_SSe_OSpe', prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs, **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e- e-]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    lb = btostautau_exclusive.make_lb(
        pks,
        dielectrons,
        pvs,
        name="rd_make_lb_to_pktautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_fakeelectron_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_FakeElectron', prescale=0.05):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    pks = btostautau_exclusive.make_lst(protons, kaons, pvs, **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        fake_electrons,
        pvs,
        name="rd_fake_dielectrons_for_btostautau_{hash}")
    lb = btostautau_exclusive.make_lb(
        pks,
        dielectrons,
        pvs,
        name="rd_make_lb_to_pktautau_fake_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_fakeproton_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_FakeProton', prescale=0.15):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(
        **kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(
        protons,
        kaons,
        pvs,
        name="rd_pk_fake_proton_for_btostautau_{hash}",
        **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    lb = btostautau_exclusive.make_lb(
        pks,
        dielectrons,
        pvs,
        name="rd_make_lb_to_pktautau_fake_protons_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def lbtopktautau_tautoe_fakekaon_line(
        name='Hlt2RD_LbToPKTauTau_TauToE_FakeKaon', prescale=0.1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pks = btostautau_exclusive.make_lst(
        protons,
        kaons,
        pvs,
        name="rd_pk_fake_kaon_for_btostautau_{hash}",
        **kwargs_pk)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    lb = btostautau_exclusive.make_lb(
        pks, dielectrons, pvs, name="rd_make_lb_to_pktautau_fake_kaons_{hash}")
    algs = rd_prefilter() + [dielectrons, pks, lb]

    iso_parts = parent_and_children_isolation(
        parents={
            'Lb': lb,
            'pk': pks
        }, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_line(name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE',
                              prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs, **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bd = btostautau_exclusive.make_bd(ksts, dielectrons, pvs)
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_same_sign_Kpi_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_SSKpi', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        decay_descriptor="[K*(892)0 -> K+ pi+]cc",
        name="rd_same_sign_kpi_for_btostautau_{hash}",
        **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_same_sign_kpi_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_same_sign_electrons_sske_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_SSe_SSKe', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs, **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e+ e+]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_same_sign_electrons_oske_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_SSe_OSKe', prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs, **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e- e-]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_fakeelectron_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_FakeElectron',
        prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    ksts = btostautau_exclusive.make_kst(kaons, pions, pvs, **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        fake_electrons,
        pvs,
        name="rd_fake_dielectrons_for_btostautau_{hash}")
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_fake_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_fakekaon_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_FakeKaon', prescale=0.2):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        name="rd_kpi_fake_kaon_for_btostautau_{hash}",
        **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_fake_kaons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bdtoksttautau_tautoe_fakepion_line(
        name='Hlt2RD_BdToKstTauTau_KstToKPi_TauToE_FakePion', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        name="rd_kpi_fake_pion_for_btostautau_{hash}",
        **kwargs_kst)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bd = btostautau_exclusive.make_bd(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bd_to_kpitautau_fake_pions_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bd]

    iso_parts = parent_and_children_isolation(
        parents={
            'B0': bd,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautoe_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToE', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name="rd_kpi_kstkst_for_btostautau_{hash}")
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bs = btostautau_exclusive.make_bs_to_kstkst(ksts, dielectrons, pvs)
    algs = rd_prefilter() + [dielectrons, ksts, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautoe_same_sign_kstar_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToE_SSKst', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name="rd_kpi_kstkst_for_btostautau_{hash}")
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dielectrons,
        pvs,
        decay_descriptor="[B_s0 -> K*(892)0 K*(892)0 D0]cc",
        name="rd_make_bs_to_kstksttautau_same_sign_kst_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautoe_same_sign_electrons_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToE_SSe', prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name="rd_kpi_kstkst_for_btostautau_{hash}")
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e+ e+]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dielectrons,
        pvs,
        decay_descriptor="[B_s0 -> K*(892)0 K*(892)~0 D0]cc",
        name="rd_make_bs_to_kstksttautau_same_sign_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def bstokstksttautau_tautoe_fakeelectron_line(
        name='Hlt2RD_BsToKstKstTauTau_KstToKPi_TauToE_FakeElectron',
        prescale=0.7):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    ksts = btostautau_exclusive.make_kst(
        kaons,
        pions,
        pvs,
        **kwargs_kst_bs2kstkst,
        name="rd_kpi_kstkst_for_btostautau_{hash}")
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        fake_electrons,
        pvs,
        name="rd_fake_dielectrons_for_btostautau_{hash}")
    bs = btostautau_exclusive.make_bs_to_kstkst(
        ksts,
        dielectrons,
        pvs,
        name="rd_make_bs_to_kstksttautau_fake_electrons_{hash}")
    algs = rd_prefilter() + [dielectrons, ksts, bs]

    iso_parts = parent_and_children_isolation(
        parents={
            'Bs': bs,
            'kst': ksts
        },
        decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautoe_line(name='Hlt2RD_BuToKTauTau_TauToE', prescale=0.15):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bu = btostautau_exclusive.make_bu(kaons, dielectrons, pvs, **kwargs_bu)
    algs = rd_prefilter() + [dielectrons, bu]

    iso_parts = parent_and_children_isolation(
        parents={'B': bu}, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautoe_same_sign_electrons_oske_line(
        name='Hlt2RD_BuToKTauTau_TauToE_SSe_OSKe', prescale=0.05):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e- e-]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bu = btostautau_exclusive.make_bu(
        kaons,
        dielectrons,
        pvs,
        name="rd_make_bu_to_ktautau_same_sign_electrons_{hash}",
        **kwargs_bu)
    algs = rd_prefilter() + [dielectrons, bu]

    iso_parts = parent_and_children_isolation(
        parents={'B': bu}, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautoe_same_sign_electrons_sske_line(
        name='Hlt2RD_BuToKTauTau_TauToE_SSe_SSKe', prescale=0.5):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        electrons,
        pvs,
        decay_descriptor="[D0 -> e+ e+]cc",
        name="rd_same_sign_dielectrons_for_btostautau_{hash}")
    bu = btostautau_exclusive.make_bu(
        kaons,
        dielectrons,
        pvs,
        name="rd_make_bu_to_ktautau_same_sign_electrons_{hash}",
        **kwargs_bu)
    algs = rd_prefilter() + [dielectrons, bu]

    iso_parts = parent_and_children_isolation(
        parents={'B': bu}, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautoe_fakeelectron_line(
        name='Hlt2RD_BuToKTauTau_TauToE_FakeElectron', prescale=0.01):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid)
    dielectrons = btostautau_exclusive.make_dielectron(
        electrons,
        fake_electrons,
        pvs,
        name="rd_fake_dielectrons_for_btostautau_{hash}")
    bu = btostautau_exclusive.make_bu(
        kaons,
        dielectrons,
        pvs,
        name="rd_make_bu_to_ktautau_fake_electron_{hash}",
        **kwargs_bu)
    algs = rd_prefilter() + [dielectrons, bu]

    iso_parts = parent_and_children_isolation(
        parents={'B': bu}, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def butoktautau_tautoe_fakekaon_line(name='Hlt2RD_BuToKTauTau_TauToE_FakeKaon',
                                     prescale=0.03):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    dielectrons = btostautau_exclusive.make_dielectron(electrons, electrons,
                                                       pvs)
    bu = btostautau_exclusive.make_bu(
        fake_kaons,
        dielectrons,
        pvs,
        name="rd_make_bu_to_ktautau_fake_kaon_{hash}",
        **kwargs_bu)
    algs = rd_prefilter() + [dielectrons, bu]

    iso_parts = parent_and_children_isolation(
        parents={'B': bu}, decay_products={'electrons': electrons})

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of rare tau decay lines for the RD working group

The following modes are included:

    - Tau -> Mu Mu Mu
    - Tau -> Mu Mu E (OS+SS)
    - Tau -> Mu E E (OS+SS)
    - Tau -> Mu (Gamma -> E E)
    - Tau -> E E E
    - Tau -> P Mu Mu (OS+SS)
    - Tau -> (Phi(1020) -> K K) Mu
    - Tau -> 5 Mu
    - Tau -> 7 Mu

The following control modes are included:

    - Ds -> (Phi -> Mu Mu) Pi
    - Ds -> (Phi -> E E) Pi

"""
import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Hlt2Conf.algorithms_thor import ParticleContainersMerger
from .builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES
from .builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_detached_electrons, make_rd_detached_pions,
    make_rd_has_rich_detached_protons, make_rd_detached_phis,
    make_rd_detached_dimuon, make_rd_detached_dielectron)
from .builders.rare_tau_decay_builders import (
    _make_tau_to_3body, _make_tau_to_2body, _make_tau_to_nmu)
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}

track_cuts = {'mipchi2dvprimary_min': 9, 'pt_min': 300 * MeV}


@register_line_builder(all_lines)
@configurable
def tau_to_mumumu_line(name="Hlt2RD_TauToMuMuMu",
                       prescale=1,
                       persistreco=False):
    descriptor = "[tau+ -> mu+ mu+ mu-]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    tau = _make_tau_to_3body(
        muons, muons, muons, descriptor, name="Hlt2RD_TauToMuMuMu_Builder")
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_mumue_line(name="Hlt2RD_TauToMuMuE", prescale=1, persistreco=False):
    descriptor_os = "[tau+ -> mu+ mu- e+]cc"
    descriptor_ss = "[tau+ -> mu+ mu+ e-]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau_os = _make_tau_to_3body(
        muons,
        muons,
        electrons,
        descriptor_os,
        name="Hlt2RD_TauToMuMuE_OS_Builder")
    tau_ss = _make_tau_to_3body(
        muons,
        muons,
        electrons,
        descriptor_ss,
        name="Hlt2RD_TauToMuMuE_SS_Builder")
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_muee_line(name="Hlt2RD_TauToMuEE", prescale=1, persistreco=False):
    descriptor_os = "[tau+ -> mu+ e+ e-]cc"
    descriptor_ss = "[tau+ -> mu- e+ e+]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau_os = _make_tau_to_3body(
        muons,
        electrons,
        electrons,
        descriptor_os,
        name="Hlt2RD_TauToMuEE_OS_Builder")
    tau_ss = _make_tau_to_3body(
        muons,
        electrons,
        electrons,
        descriptor_ss,
        name="Hlt2RD_TauToMuEE_SS_Builder")
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_mugamma_ee_line(name="Hlt2RD_TauToMuGamma_EE",
                           prescale=1,
                           persistreco=False):
    descriptor = "[tau+ -> mu+ gamma]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    gammas = make_rd_detached_dielectron(
        parent_id='gamma',
        am_min=0 * MeV,
        am_max=100 * MeV,
        pt_e_min=300 * MeV,
        pid_e_min=4,
        ipchi2_e_min=9,
        with_brem=False)
    tau = _make_tau_to_2body(
        muons,
        gammas,
        descriptor,
        min_mass=1378 * MeV,
        max_mass=2078 * MeV,
        name="Hlt2RD_TauToMuGamma_Builder")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_eee_line(name="Hlt2RD_TauToEEE", prescale=1, persistreco=False):
    descriptor = "[tau+ -> e+ e+ e-]cc"
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau = _make_tau_to_3body(
        electrons,
        electrons,
        electrons,
        descriptor,
        name="Hlt2RD_TauToEEE_Builder")
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_pmumu_line(name="Hlt2RD_TauToPMuMu", prescale=1, persistreco=False):
    descriptor_os = "[tau+ -> p+ mu+ mu-]cc"
    descriptor_ss = "[tau+ -> p~- mu+ mu+]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    protons = make_rd_has_rich_detached_protons(**track_cuts)
    tau_os = _make_tau_to_3body(
        protons,
        muons,
        muons,
        descriptor_os,
        name="Hlt2RD_TauToPMuMu_OS_Builder")
    tau_ss = _make_tau_to_3body(
        protons,
        muons,
        muons,
        descriptor_ss,
        name="Hlt2RD_TauToPMuMu_SS_Builder")
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_phimu_line(name="Hlt2RD_TauToPhiMu", prescale=1, persistreco=False):
    """
    Modified by Domenico Riccardi
    Contact: domenico.riccardi@cern.ch
    """
    descriptor = "[tau+ -> phi(1020) mu+]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=F.ISMUON)
    phis = make_rd_detached_phis(
        am_min=990 * MeV,
        am_max=1050 * MeV,
        k_pt_min=300 * MeV,
        k_ipchi2_min=4,
        k_pid=(F.PID_K > 4),
        vchi2pdof_max=12)
    tau = _make_tau_to_2body(
        phis,
        muons,
        descriptor,
        name="Hlt2RD_TauToPhiMu_Builder",
        min_mass=1627 * MeV,
        max_mass=2040 * MeV,
        max_ipchi2=30,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_5mu_line(name="Hlt2RD_TauTo5Mu", prescale=1, persistreco=False):
    descriptor = "[tau+ -> Xu+ Xu0]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    muons_loose = make_rd_detached_muons(**track_cuts, pid=None)
    tau = _make_tau_to_nmu(
        muons,
        muons_loose,
        descriptor,
        name="Hlt2RD_TauTo5Mu_Builder",
        n_muons=5)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def tau_to_7mu_line(name="Hlt2RD_TauTo7Mu", prescale=1, persistreco=False):
    descriptor = "[tau+ -> Xu+ Xu0 Xu0]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    muons_loose = make_rd_detached_muons(**track_cuts, pid=None)
    tau = _make_tau_to_nmu(
        muons,
        muons_loose,
        descriptor,
        name="Hlt2RD_TauTo7Mu_Builder",
        n_muons=7)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def ds_to_phipi_mumu_line(name="Hlt2RD_DsToPhiPi_PhiToMuMu",
                          prescale=1,
                          persistreco=False):
    descriptor = "[D_s+ -> phi(1020) pi+]cc"
    pions = make_rd_detached_pions(**track_cuts)
    phis = make_rd_detached_dimuon(
        parent_id='phi(1020)',
        am_min=950 * MeV,
        am_max=1100 * MeV,
        pt_muon_min=300 * MeV,
        pidmu_muon_min=2)
    ds = _make_tau_to_2body(
        phis,
        pions,
        descriptor,
        name="Hlt2RD_DsToPhiPi_PhiToMuMu_Builder",
        min_mass=1718 * MeV,
        max_mass=2218 * MeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ds],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def ds_to_phipi_ee_line(name="Hlt2RD_DsToPhiPi_PhiToEE",
                        prescale=1,
                        persistreco=False):
    descriptor = "[D_s+ -> phi(1020) pi+]cc"
    pions = make_rd_detached_pions(**track_cuts)
    phis = make_rd_detached_dielectron(
        parent_id='phi(1020)',
        am_min=950 * MeV,
        am_max=1100 * MeV,
        pt_e_min=300 * MeV,
        pid_e_min=2)
    ds = _make_tau_to_2body(
        phis,
        pions,
        descriptor,
        name="Hlt2RD_DsToPhiPi_PhiToEE_Builder",
        min_mass=1718 * MeV,
        max_mass=2218 * MeV)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ds],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES)

###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 lines for radiative b-decays
Authors: Miguel Rebollo, Izaac Sanderswood, Pere Gironell

- Lambda_b0 -> Lambda0 gamma

- Xi_b- -> Xi- gamma
  Hlt2RD_XibmToXimGamma_XimToL0Pim_LLL
- Omega_b- -> Omega- gamma
  Hlt2RD_OmegabmToOmegamGamma_OmegamToLambda0Km_LLL
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.rd.builders.baryonic_builders import (
    make_xim_to_lambda_pi_lll,
    make_omegam_to_lambda_k_lll,
)

from Hlt2Conf.lines.rd.builders.baryonic_radiative_builders import (
    make_rd_xibm, make_rd_obm, make_rd_lb_to_lgamma_LL,
    filter_lambdasLL_for_lb_to_lg, LB_TMVA_BDT)

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import make_rd_photons

from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES, _VRD_MONITORING_VARIABLES

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleFilter
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}


@configurable
@register_line_builder(all_lines)
def lb_to_lgamma_ll_line(name='Hlt2RD_LbToLambdaGamma_LL',
                         prescale=1.,
                         prefilter=rd_prefilter):
    """Lambda_b0 -> Lambda gamma line"""

    pvs = make_pvs()

    lambda_ll = filter_lambdasLL_for_lb_to_lg(pvs=pvs)
    photons = make_rd_photons(
        et_min=2. * GeV,
        e_min=5. * GeV,
        IsPhoton_min=0.4,
        IsNotH_min=0.3,
        E19_min=0.2)

    lb_ll = make_rd_lb_to_lgamma_LL(Lambda=lambda_ll, gamma=photons, pvs=pvs)

    lb_ll_mva = ParticleFilter(
        lb_ll, Cut=F.FILTER(LB_TMVA_BDT(
            pvs=pvs,
            topo="LL",
        ) > 0.7))

    return Hlt2Line(
        name=name,
        algs=prefilter() + [lb_ll_mva],
        monitoring_variables=_RD_MONITORING_VARIABLES,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xibtoxigamma_xitolpi_lll_line(name='Hlt2RD_XibmToXimGamma_XimToL0Pim_LLL',
                                  prescale=1):
    pvs = make_pvs()

    xim = make_xim_to_lambda_pi_lll(
        name='rd_xim_lll',
        pvs=pvs,
        comb_m_min=1277 * MeV,
        comb_m_max=1367 * MeV,
        m_min=1297 * MeV,
        m_max=1347 * MeV,
        comb_p_min=9. * GeV,
        p_min=20. * GeV,
        docachi2_max=25.,
        vchi2pdof_max=9.,
        bpvvdz_min=1 * mm,
        bpvdira_min=0.95,
        lambda_mass_window=30. * MeV,
        lambda_vchi2pdof_max=6.,
        lambda_bpvvdchi2_min=4.,
        lambda_pt_min=1. * GeV,
        pion_p_min=0.,
        pion_pt_min=200. * MeV,
        pion_mipchi2dvprimary_min=9.,
        pion_PID=None,
    )
    photons = make_rd_photons(
        e_min=5 * GeV,
        et_min=2 * GeV,
        IsNotH_min=0.6,
    )

    xibm = make_rd_xibm(
        xim,
        photons,
        pvs,
        name='xibm_lll',
        comb_mass_window=1000. * MeV,
        mass_window=800. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5. * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xim, xibm],
        extra_outputs=parent_isolation_output("Xibm", xibm),
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
def obtoomgamma_omtol0k_omtol0k_lll_line(
        name='Hlt2RD_OmegabmToOmegamGamma_OmegamToLambda0Km_LLL', prescale=1):
    pvs = make_pvs()

    omegam = make_omegam_to_lambda_k_lll(
        name='rd_om_lll',
        pvs=pvs,
        comb_m_min=1622 * MeV,
        comb_m_max=1722 * MeV,
        m_min=1642 * MeV,
        m_max=1702 * MeV,
        comb_p_min=9. * GeV,
        p_min=9.5 * GeV,
        docachi2_max=25.,
        vchi2pdof_max=9.,
        bpvvdz_min=0.5 * mm,
        bpvdira_min=0.95,
        lambda_mass_window=35. * MeV,
        lambda_vchi2pdof_max=9.,
        lambda_bpvvdchi2_min=4.,
        lambda_pt_min=1. * GeV,
        kaon_p_min=5. * GeV,
        kaon_pt_min=400. * MeV,
        kaon_mipchi2dvprimary_min=9.,
        kaon_PID=(F.PID_K > 6.),
    )
    photons = make_rd_photons(
        e_min=5 * GeV,
        et_min=2 * GeV,
    )

    omegabm = make_rd_obm(
        omegam,
        photons,
        pvs,
        name='obm_lll',
        comb_mass_window=1200. * MeV,
        mass_window=1000. * MeV,
        pt_min=1.5 * GeV,
        p_min=30 * GeV,
        sum_pt_min=5. * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omegam, omegabm],
        extra_outputs=parent_isolation_output("Omegabm", omegabm),
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_VRD_MONITORING_VARIABLES)

###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Omega to Xi- X decay builder.

Athor: Chuangxin Lin
Email: chuangxin.lin@cern.ch
Date: 30/12/2021
"""

import Functors as F
from PyConf import configurable
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, ps, mm
from Hlt2Conf.algorithms_thor import ParticleCombiner
from .rdbuilder_thor import make_rd_detached_pions, make_rd_lambda_lls, make_rd_lambda_dds


@configurable
def make_xim_to_lambda_pim(pvs,
                           lambdas,
                           pions,
                           name="xim_to_lambda_pim",
                           comb_m_min=1262 * MeV,
                           comb_m_max=1382 * MeV,
                           m_min=1272 * MeV,
                           m_max=1372 * MeV,
                           comb_p_min=9. * GeV,
                           p_min=9.5 * GeV,
                           docachi2_max=25.,
                           vchi2pdof_max=20.,
                           bpvvdz_min=0.5 * mm,
                           bpvltime_min=2. * ps):
    """ 
    Builder for Xi- -> Lambda pi-. 
    """
    combination_code = F.require_all(
        F.MAXDOCACHI2CUT(docachi2_max), in_range(comb_m_min, F.MASS,
                                                 comb_m_max), F.P > comb_p_min)
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs) > bpvvdz_min,
    )
    return ParticleCombiner([lambdas, pions],
                            name=name,
                            DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


def make_xim_to_lambda_pim_lll(pvs, name="xim_to_lambda_pim_lll"):
    """ 
    Make LLL Xi- -> Lambda pi-. 
    """
    lambdas = make_rd_lambda_lls(bpvltime_min=2. * ps)
    pions = make_rd_detached_pions(mipchi2dvprimary_min=9.)
    return make_xim_to_lambda_pim(
        name=name, pvs=pvs, lambdas=lambdas, pions=pions)


@configurable
def make_xim_to_lambda_pim_ddl(pvs, name="xim_to_lambda_pim_ddl"):
    """ 
    Make DDL Xi- -> Lambda pi-. 
    """
    lambdas = make_rd_lambda_dds(bpvltime_min=2. * ps)
    pions = make_rd_detached_pions(mipchi2dvprimary_min=9.)
    return make_xim_to_lambda_pim(
        name=name, pvs=pvs, lambdas=lambdas, pions=pions)


@configurable
def make_omega_to_xi_dip(pvs,
                         hyperon,
                         particle1,
                         particle2,
                         descriptor,
                         name,
                         comb_m_min=1612 * MeV,
                         comb_m_max=1732 * MeV,
                         m_min=1622 * MeV,
                         m_max=1722 * MeV,
                         comb_p_min=9. * GeV,
                         p_min=9.5 * GeV,
                         docachi2_max=25.,
                         vchi2pdof_max=20.,
                         bpvvdz_min=0.5 * mm,
                         bpvltime_min=2. * ps):
    """
    Builder for Omega decaying to Xi plus dilepton or dipion.
    """
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.P > comb_p_min,
        F.MAXDOCACHI2CUT(docachi2_max))
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs) > bpvvdz_min,
    )
    return ParticleCombiner([hyperon, particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_omega_to_xi_gamma(pvs,
                           hyperon,
                           gamma,
                           descriptor,
                           name,
                           comb_m_min=1612 * MeV,
                           comb_m_max=1732 * MeV,
                           m_min=1622 * MeV,
                           m_max=1722 * MeV,
                           comb_p_min=9. * GeV,
                           p_min=9.5 * GeV,
                           bpvvdz_min=0.5 * mm,
                           bpvltime_min=2. * ps,
                           bipchi2_max=25.):
    """
    Builder for Omega decaying to Xi plus a photon.
    """
    combination_code = F.require_all(F.P > comb_p_min,
                                     in_range(comb_m_min, F.MASS, comb_m_max))
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.P > p_min,
        F.BPVIPCHI2() < bipchi2_max)
    return ParticleCombiner([hyperon, gamma],
                            ParticleCombiner="ParticleAdder",
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)

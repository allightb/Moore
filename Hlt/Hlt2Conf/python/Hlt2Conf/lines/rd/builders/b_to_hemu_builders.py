###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Filters for basic particles and combined objects for B->hemu lines
"""

from GaudiKernel.SystemOfUnits import MeV
import Functors as F
from PyConf import configurable

from Hlt2Conf.algorithms_thor import ParticleCombiner


@configurable
def make_dilepton(lepton1,
                  lepton2,
                  pvs,
                  decay_descriptor,
                  name="DileptonBuilder_For_HEMuLines_{hash}",
                  pt_min=0 * MeV,
                  m_min=0 * MeV,
                  m_max=5500 * MeV,
                  ipchi2_min=0,
                  fdchi2_min=30,
                  vtxchi2dof_max=9):
    """
    Selections for dilepton system in B2Xemu. Should not be used for
    dielectron, since it does not check for proper brem handling.
    Dielectron uses filter_dielectron_noMVA instead.
    """
    combination_code = F.require_all(
        F.math.in_range(m_min, F.MASS, m_max), F.PT > pt_min)
    vertex_code = F.require_all(F.CHI2DOF < vtxchi2dof_max,
                                F.BPVIPCHI2(pvs) > ipchi2_min,
                                F.BPVFDCHI2(pvs) > fdchi2_min)

    return ParticleCombiner([lepton1, lepton2],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


@configurable
def make_hh(kaons,
            pions,
            pvs,
            decay_descriptor,
            name="DiHadronBuilder_For_HEMuLines_{hash}",
            pt_min=500 * MeV,
            m_min=592 * MeV,
            m_max=1192 * MeV,
            vtxchi2_max=25,
            distchi2_max=30):
    """hh combiner for B->hhemu, defaults set for K*(892)0"""
    combination_code = F.require_all(
        F.DOCACHI2(1, 2) < distchi2_max, F.math.in_range(m_min, F.MASS, m_max))
    vertex_code = F.require_all(F.PT > pt_min, F.CHI2 < vtxchi2_max)
    return ParticleCombiner([kaons, pions],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)


@configurable
def make_btohemu(hadrons,
                 dileptons,
                 pvs,
                 decay_descriptor,
                 name="MainBuilder_For_HEMuLines_{hash}",
                 m_min=4000 * MeV,
                 m_max=6500 * MeV,
                 dira_min=0.995,
                 ipchi2_max=25,
                 vtxchi2dof_max=9,
                 fdchi2_min=100):
    """Builder for B->hemu, supports also multi-hadron"""
    combination_code = (F.math.in_range(m_min, F.MASS, m_max))
    vertex_code = F.require_all(F.CHI2DOF < vtxchi2dof_max,
                                F.BPVIPCHI2(pvs) < ipchi2_max,
                                F.BPVFDCHI2(pvs) > fdchi2_min,
                                F.BPVDIRA(pvs) > dira_min)

    return ParticleCombiner([dileptons, hadrons],
                            DecayDescriptor=decay_descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)

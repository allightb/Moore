###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive_radiative_b b TMVA selections (BDT)
"""
from PyConf import configurable

import Functors as F
from Functors.math import log
from Hlt2Conf.algorithms_thor import ParticleFilter
from GaudiKernel.SystemOfUnits import GeV


@configurable
def rad_BDT_functor(pvs, line):
    def sum_finalstates_hh(functor):
        return F.CHILD(1, F.SUM(functor)) + F.CHILD(2, functor)

    def sum_finalstates_hhh(functor):
        return F.CHILD(1, F.CHILD(1, (F.SUM(functor)))) + F.CHILD(
            1, F.CHILD(2, functor)) + F.CHILD(2, functor)

    hhg_vars = {
        'B_ipchi2': log(F.BPVIPCHI2(pvs)),
        'B_ipchi2_min': log(F.CHILD(1, F.MIN(F.BPVIPCHI2(pvs)))),
        'B_gamma_pt': F.CHILD(2, F.PT),
        'B_fdchi2': log(F.BPVFDCHI2(pvs)),
        'B_vtx_chi2': log(F.CHILD(1, F.CHI2)),
        'B_doca': F.CHILD(1, F.SDOCA(1, 2))
    }
    hhgee_vars = {
        'mcor':
        F.BPVCORRM(pvs),
        'chi2':
        F.CHILD(1, F.CHI2),
        'sumpt':
        sum_finalstates_hh(F.PT),
        'eta':
        F.BPVETA(pvs),
        'fdchi2':
        F.BPVFDCHI2(pvs),
        'minpt':
        F.MINTREE(((F.IS_ABS_ID('K+')) | (F.IS_ID('KS0')) |
                   (F.IS_ABS_ID('Lambda0')) | (F.IS_ABS_ID('gamma'))), F.PT),
        'nlt16':
        sum_finalstates_hh(F.BPVIPCHI2(pvs) < 16),
        'ipchi2':
        F.BPVIPCHI2(pvs),
        'n1trk':
        sum_finalstates_hh((F.PT > 1 * GeV) & (F.BPVIPCHI2(pvs) > 16))
    }

    hhhg_vars = {
        'ipchi2':
        log(F.BPVIPCHI2(pvs)) / log(10),
        'ipchi2_min':
        log(
            F.MINTREE((F.IS_ABS_ID('K+')) | (F.IS_ID('KS0')) |
                      (F.IS_ABS_ID('Lambda0')), F.BPVIPCHI2(pvs))) / log(10),
        'gamma_pt':
        F.CHILD(2, F.PT),
        'gamma_p':
        F.CHILD(2, F.P),
        'm_corrected':
        F.BPVCORRM(pvs),
        'fdchi2':
        log(F.BPVFDCHI2(pvs)) / log(10),
        'vtx_chi2':
        F.CHILD(1, F.CHI2) / log(10),
        'chi2dof_max':
        F.MAXTREE(F.IS_ABS_ID('K+'), F.CHI2DOF()),
    }

    hhhgee_vars = {
        'mcor':
        F.BPVCORRM(pvs),
        'chi2':
        F.CHILD(1, F.CHI2),
        'sumpt':
        sum_finalstates_hhh(F.PT),
        'eta':
        F.BPVETA(pvs),
        'fdchi2':
        F.BPVFDCHI2(pvs),
        'minpt':
        F.MINTREE(((F.IS_ABS_ID('K+')) | (F.IS_ID('KS0')) |
                   (F.IS_ABS_ID('Lambda0')) | (F.IS_ABS_ID('gamma'))), F.PT),
        'nlt16':
        sum_finalstates_hhh(F.BPVIPCHI2(pvs) < 16),
        'ipchi2':
        F.BPVIPCHI2(pvs),
        'n1trk':
        sum_finalstates_hhh((F.PT > 1 * GeV) & (F.BPVIPCHI2(pvs) > 16))
    }

    bdt_vars = {
        "HHgamma": hhg_vars,
        "HHgammaEE": hhgee_vars,
        "HHHgamma": hhhg_vars,
        "HHHgammaEE": hhhgee_vars
    }
    xml_files = {
        "HHgamma": 'paramfile://data/Hlt2_Radiative_%s_2024_03.xml' % line,
        "HHgammaEE": 'paramfile://data/Hlt2_Radiative_%s.xml' % line,
        "HHHgamma": 'paramfile://data/Hlt2_Radiative_%s.xml' % line,
        "HHHgammaEE": 'paramfile://data/Hlt2_Radiative_%s.xml' % line
    }

    return F.MVA(
        MVAType='TMVA',
        Config={
            'XMLFile': xml_files[line],
            'Name': 'BDT',
        },
        Inputs=bdt_vars[line])


@configurable
def make_b2(presel_b, pvs, line, bdt_cut, filter_name="rd_rad_incl_B_{hash}"):
    code = rad_BDT_functor(pvs, line) > bdt_cut
    return ParticleFilter(presel_b, F.FILTER(code), name=filter_name)

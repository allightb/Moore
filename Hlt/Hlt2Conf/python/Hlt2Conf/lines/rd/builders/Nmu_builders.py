###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Nmu(h) builders for the RD working group.
- Single-vertex Detached -> 4lepton
- Single-vertex Detached -> 2e2mu
- B -> X(ll) X(l'l')
- B -> X(ll) X(l'l') K 
- B -> X(ll) X(l'l') X(l''l'')
- B -> X(ll) X(l'l') X(l''l'') K 

author: Titus Mombächer
date: 18.10.2021
"""

import Functors as F

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleCombiner

from PyConf import configurable

# from .rdbuilder_thor import make_rd_detached_muons, make_rd_detached_kaons, make_rd_detached_dimuon, make_rd_dimuon_dds, make_rd_detached_dielectron, make_rd_detached_electrons


### generic displaced B24l, aimed at sprucing
@configurable
def make_b_4l(lepton,
              descriptor,
              name="make_rd_b24mu",
              min_BFDCHI2=25.,
              max_BIPCHI2=25.,
              max_BVTXCHI2=8.,
              MAXDOCA=0.5):
    """Builds displaced->4l inclusive selections"""

    combination_code = F.MAXSDOCACUT(MAXDOCA)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[lepton, lepton, lepton, lepton],
        DecayDescriptor=descriptor,
        Combination12Cut=combination_code,
        CompositeCut=vertex_code)


## generic B2eemumu, aimed at sprucing
@configurable
def make_b_2mu2e(dimuon,
                 dielectron,
                 descriptor,
                 name="make_rd_b22mu2e",
                 min_BFDCHI2=25.,
                 max_BIPCHI2=25.,
                 max_BVTXCHI2=8.,
                 MAXDOCA=0.5):
    """Builds displaced->mumuee inclusive selections"""

    combination_code = F.MAXSDOCACUT(MAXDOCA)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[dimuon, dielectron],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


### LLP 4l combinations
@configurable
def make_generic_4lepton_LLP(dilepton1,
                             dilepton2,
                             descriptor,
                             name="make_rd_generic_4mu_LL",
                             min_BFDCHI2=25.,
                             max_BIPCHI2=25.,
                             max_BVTXCHI2=8.,
                             max_DOCA=0.75):
    """Builds B -> X (ll) X (l'l')"""

    combination_code = F.require_all(F.MAXDOCACUT(max_DOCA))
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[dilepton1, dilepton2],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_B24lK_LLP(dilepton1,
                   dilepton2,
                   kaon,
                   descriptor,
                   name="make_rd_B24muK_LL",
                   min_BFDCHI2=25.,
                   max_BIPCHI2=25.,
                   max_BVTXCHI2=8.,
                   max_DOCA=0.5):
    """Builds B -> X (ll) X (l'l') K"""

    combination_code = F.require_all(F.MAXDOCACUT(max_DOCA))
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[dilepton1, dilepton2, kaon],
        DecayDescriptor=descriptor,
        Combination12Cut=combination_code,
        CompositeCut=vertex_code)


### LLP 6l combinations


@configurable
def make_B26l_LLP(dilepton1,
                  dilepton2,
                  dilepton3,
                  descriptor,
                  name="make_rd_B26mu_LLL",
                  min_BFDCHI2=25.,
                  max_BIPCHI2=25.,
                  max_BVTXCHI2=8.,
                  max_DOCA=0.5):
    """Builds B -> X (ll) X (l'l') X (l''l'')"""

    combination_code = F.require_all(F.MAXDOCACUT(max_DOCA))
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[dilepton1, dilepton2, dilepton3],
        DecayDescriptor=descriptor,
        Combination12Cut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_B26lK_LLP(dilepton1,
                   dilepton2,
                   dilepton3,
                   kaon,
                   descriptor,
                   name="make_rd_B26muK_LLL",
                   min_BFDCHI2=25.,
                   max_BIPCHI2=25.,
                   max_BVTXCHI2=8.,
                   max_DOCA=0.5):
    """Builds B -> X (mumu) X (mumu) X (mumu) K with long muon combinations"""

    combination_code = F.require_all(F.MAXDOCACUT(max_DOCA))
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_BFDCHI2,
        F.BPVIPCHI2(pvs) < max_BIPCHI2, F.CHI2DOF < max_BVTXCHI2)

    return ParticleCombiner(
        name=name,
        Inputs=[dilepton1, dilepton2, dilepton3, kaon],
        DecayDescriptor=descriptor,
        Combination12Cut=combination_code,
        CompositeCut=vertex_code)

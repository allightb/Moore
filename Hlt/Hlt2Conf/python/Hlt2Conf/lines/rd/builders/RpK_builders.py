###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf import configurable
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner
from .rdbuilder_thor import (make_rd_has_rich_detached_protons,
                             make_rd_has_rich_detached_kaons)


@configurable
def make_lambdab_to_pkll(dileptons,
                         dihadrons,
                         descriptor,
                         name="HLT2RD_LambdabToPKLL_Builder_{hash}",
                         make_pvs=make_pvs,
                         min_mass=3780 * MeV,
                         max_mass=6780 * MeV,
                         max_vtxchi2dof=9,
                         max_ipchi2=25,
                         min_dira=0.9995,
                         min_flightdist=100):
    """
    Builder for Lambdab decaying to proton, kaon, and two leptons
    """
    pvs = make_pvs()

    combination_code = in_range(min_mass, F.MASS, max_mass)

    vertex_code = F.require_all(F.CHI2DOF < max_vtxchi2dof,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVDIRA(pvs) > min_dira,
                                F.BPVFDCHI2(pvs) > min_flightdist)

    return ParticleCombiner([dileptons, dihadrons],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_dihadron_from_pK(name="HLT2RD_DiHadronFromPK_Builder_{hash}",
                          min_pt=400 * MeV,
                          max_mass=2600 * MeV,
                          max_docachi2=30.0,
                          max_vtxchi2dof=25,
                          same_sign=False):
    """
    Builder for a combination of proton and kaon
    """
    if same_sign:
        descriptor = "[Lambda(1520)0 -> p+ K+]cc"
    else:
        descriptor = "[Lambda(1520)0 -> p+ K-]cc"

    protons = make_rd_has_rich_detached_protons(
        mipchi2dvprimary_min=9, pt_min=300 * MeV)
    kaons = make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pt_min=300 * MeV)

    combination_code = F.require_all(F.PT > min_pt, F.MASS < max_mass,
                                     F.MAXSDOCACHI2CUT(max_docachi2))

    vertex_code = (F.CHI2DOF < max_vtxchi2dof)

    return ParticleCombiner([protons, kaons],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)

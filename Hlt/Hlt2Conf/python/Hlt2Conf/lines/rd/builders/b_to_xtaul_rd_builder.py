###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Author: Tommaso Fulghesu
Contact: tommaso.fulghesu@cern.ch
Date: 07/12/2021
'''

######################################################################################
####                                                                              ####
#### Builders for exlusive lines Yb -> X tau (-> 3pi nu_tau)                      ####
####                                                                              ####
######################################################################################

from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner

import Functors as F
from Functors.math import in_range

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_detached_muons, make_rd_detached_electrons, make_rd_detached_phis,
    make_rd_detached_rho0, make_rd_detached_etaprime, make_rd_detached_k1,
    make_rd_tauons_hadronic_decay, make_rd_has_rich_detached_kaons,
    make_rd_has_rich_detached_protons)


@configurable
def make_dihadron_from_pK(name='dihadron_from_pk_{hash}',
                          parent_id='Lambda(1520)0',
                          make_protons=make_rd_has_rich_detached_protons,
                          make_kaons=make_rd_has_rich_detached_kaons,
                          p_ipchi2_min=9.,
                          k_ipchi2_min=9.,
                          p_pid=F.require_all(F.PID_P > 2.,
                                              F.PID_P - F.PID_K > 0.),
                          k_pid=F.PID_K > 8,
                          pt_min=1 * GeV,
                          am_min=1300 * MeV,
                          am_max=5620 * MeV,
                          adocachi2cut_max=20.,
                          vtxchi2_max=9,
                          bpvfdchi2_min=16.):
    """
    Make pK combination
    """
    pvs = make_pvs()

    protons = make_protons(mipchi2dvprimary_min=p_ipchi2_min, pid=p_pid)
    kaons = make_kaons(mipchi2dvprimary_min=k_ipchi2_min, pid=k_pid)
    descriptor = f'[{parent_id} -> p+ K-]cc'
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2cut_max))

    vertex_code = F.require_all(F.PT > pt_min, F.CHI2DOF < vtxchi2_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner(
        Inputs=[protons, kaons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def filter_rd_detached_phis(name='rd_detached_phis_{hash}',
                            k_pt_min=250 * MeV,
                            k_ipchi2_min=9.,
                            k_pid=(F.PID_K > 8.),
                            pt_min=1 * GeV,
                            vtxchi2_max=9,
                            adocachi2cut=20.,
                            ipchi2_phi_min=9.):
    """
    Filter with IPCHI2 cut the phi(1020) built in rdbuilder_thor
    """

    phis = make_rd_detached_phis(
        k_pt_min=k_pt_min,
        k_ipchi2_min=k_ipchi2_min,
        k_pid=k_pid,
        phi_pt_min=pt_min,
        vchi2pdof_max=vtxchi2_max,
        adocachi2cut=adocachi2cut)
    pvs = make_pvs()
    code = F.MINIPCHI2CUT(IPChi2Cut=ipchi2_phi_min, Vertices=pvs)

    return ParticleFilter(phis, name=name + "_{hash}", Cut=F.FILTER(code))


@configurable
def filter_rd_detached_rho0(name='rd_detached_rhos_{hash}',
                            pi_pt_min=250 * MeV,
                            pi_p_min=1 * GeV,
                            pi_ipchi2_min=9.,
                            pi_pid=(F.PID_K <= 0.),
                            pt_min=1 * GeV,
                            vtxchi2_max=6,
                            ipchi2_rho_min=9.):
    """
    Filter with IPCHI2 cut the phi(1020) built in rdbuilder_thor
    """
    rho0 = make_rd_detached_rho0(
        pi_pt_min=pi_pt_min,
        pi_p_min=pi_p_min,
        pi_ipchi2_min=pi_ipchi2_min,
        pt_min=pt_min,
        vchi2pdof_max=vtxchi2_max)
    pvs = make_pvs()
    code = F.MINIPCHI2CUT(IPChi2Cut=ipchi2_rho_min, Vertices=pvs)

    return ParticleFilter(rho0, name=name + "_{hash}", Cut=F.FILTER(code))


@configurable
def filter_rd_detached_etaprime(name='rd_detached_etaprime_{hash}',
                                pi_pt_min=250 * MeV,
                                pi_ipchi2_min=9.0,
                                pt_min=1 * GeV,
                                ipchi2_etaprime_min=9.):
    """
    Filter with IPCHI2 cut the eta' built in rdbuilder_thor
    """
    etaprime = make_rd_detached_etaprime(
        pi_pt_min=pi_pt_min, pi_ipchi2_min=pi_ipchi2_min, pt_min=pt_min)
    pvs = make_pvs()
    code = F.MINIPCHI2CUT(IPChi2Cut=ipchi2_etaprime_min, Vertices=pvs)

    return ParticleFilter(etaprime, name=name + "_{hash}", Cut=F.FILTER(code))


@configurable
def filter_rd_detached_k1(name='rd_detached_k1_{hash}',
                          pi_pt_min=250 * MeV,
                          pi_ipchi2_min=9.0,
                          pi_pid=F.PID_K <= 0.,
                          k_pt_min=250 * MeV,
                          k_ipchi2_min=9.0,
                          k_pid=F.PID_K > 8.,
                          pt_min=1 * GeV,
                          ipchi2_k1_min=9.,
                          bpvfdchi2_min=16):
    """
    Filter with IPCHI2 cut the K1(1270) built in rdbuilder_thor
    """
    k1 = make_rd_detached_k1(
        pi_pt_min=pi_pt_min,
        pi_ipchi2_min=pi_ipchi2_min,
        pi_pid=pi_pid,
        k_pt_min=k_pt_min,
        k_pid=k_pid,
        pt_min=pt_min)
    pvs = make_pvs()
    code = F.require_all(
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_k1_min, Vertices=pvs),
        F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleFilter(k1, name=name + "_{hash}", Cut=F.FILTER(code))


def filter_BToTauTau(
        ditaus,
        name='filter_RD_B2tautau_{hash}',
        m_min=2 * GeV,
        m_max=7 * GeV,
        p_min=3 * GeV,
        pt_min=1 * GeV,
        bpvdira_min=0.995,
        bpvfdchi2_min=225,
        bpvipchi2_max=50,
        chi2_max=90,
        bpvfd_max=90 * mm,
):
    pvs = make_pvs()
    code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.P > p_min, F.PT > pt_min,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.CHI2() < chi2_max,
        F.BPVFD(pvs) < bpvfd_max)
    return ParticleFilter(ditaus, F.FILTER(code), name=name)


@configurable
def make_dilepton_from_tauls(parent_id="J/psi(1S)",
                             child_id="tau-",
                             name='dilepton_from_tauls_{hash}_{hash}',
                             pi_pt_min=250 * MeV,
                             pi_p_min=2000 * MeV,
                             pi_ipchi2_min=16.,
                             best_pi_pt_min=800 * MeV,
                             best_pi_ipchi2_min=25.,
                             pi_pid=F.require_all(F.PID_K < 0., F.PID_E < 5.,
                                                  F.PID_P < 10, F.PID_MU < 10),
                             tau_am_2pi_min=400.,
                             tau_am_2pi_max=1200.,
                             tau_am_coeff_min=0.8,
                             tau_am_coeff_max=1.1,
                             tau_m_min=800 * MeV,
                             tau_m_max=1600 * MeV,
                             tau_chi2_max=6.,
                             tau_maxdocacut=0.2 * mm,
                             tau_bpvfdchi2_min=100,
                             tau_pt_min=1000 * MeV,
                             mu_pt=500 * MeV,
                             e_pt=500 * MeV,
                             mu_p=3 * GeV,
                             e_p=3 * GeV,
                             mu_pid=F.require_all(F.ISMUON, F.PID_MU > 2.),
                             e_pid=F.require_all(F.PID_E > 3.,
                                                 F.PID_E - F.PID_K > 0.),
                             mu_mipchi2=9.,
                             e_mipchi2=9.,
                             am_min=200 * MeV,
                             am_max=5 * GeV):
    """
    Make tau-l combination.
    The tau is filtered from rdbuilder_thor with requirements on IPCHI2, VDRHO, VDZ and M
    """
    decay_descriptor = f'[{parent_id} -> tau- {child_id}]cc'
    if child_id == 'tau+':
        decay_descriptor = f'{parent_id} -> tau- {child_id}'
    pvs = make_pvs()
    combination_code = in_range(am_min, F.MASS, am_max)
    vertex_code = F.ALL
    filter_code = F.require_all(
        in_range(tau_m_min, F.MASS, tau_m_max), F.MAXSDOCACUT(tau_maxdocacut),
        F.BPVFDCHI2(pvs) > tau_bpvfdchi2_min,
        F.PT() > tau_pt_min)
    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=pi_pt_min,
            pi_p_min=pi_p_min,
            pi_ipchi2_min=pi_ipchi2_min,
            best_pi_pt_min=best_pi_pt_min,
            best_pi_ipchi2_min=best_pi_ipchi2_min,
            pi_pid=pi_pid,
            am_min=tau_am_coeff_min * tau_m_min,
            am_max=tau_am_coeff_max * tau_m_max,
            vchi2pdof_max=tau_chi2_max,
            am_2pi_min=tau_am_2pi_min,
            am_2pi_max=tau_am_2pi_max),
        name=name,
        Cut=F.FILTER(filter_code))
    if child_id == "tau+" or child_id == "tau-":
        leptons = taus
    if child_id == "mu+" or child_id == "mu-":
        leptons = make_rd_detached_muons(
            pt_min=mu_pt,
            p_min=mu_p,
            mipchi2dvprimary_min=mu_mipchi2,
            pid=mu_pid)
    if child_id == "e+" or child_id == "e-":
        leptons = make_rd_detached_electrons(
            pt_min=e_pt, p_min=e_p, mipchi2dvprimary_min=e_mipchi2, pid=e_pid)

    return ParticleCombiner(
        Inputs=[taus, leptons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_beauty2xtaul(particles,
                      descriptors,
                      name='beauty_to_xtaul_{hash}_{hash}',
                      am_factor_min=0.8,
                      am_factor_max=1.2,
                      m_min=2 * GeV,
                      m_max=10 * GeV,
                      p_min=3 * GeV,
                      pt_min=1 * GeV,
                      vchi2pdof_max=100,
                      ipchi2_max=50.,
                      fdchi2_min=120,
                      dira_min=0.995):
    """
    Builder for B0, Bu, Bs and Lambdab
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_factor_min * m_min, F.MASS, am_factor_max * m_max))
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.CHI2DOF < vchi2pdof_max, F.P > p_min,
        F.PT > pt_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

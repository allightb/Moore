###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf import ConfigurationError, configurable
from Functors.math import in_range
from GaudiKernel.PhysicalConstants import c_light
from GaudiKernel.SystemOfUnits import MeV, micrometer
from RecoConf.reconstruction_objects import make_pvs
from Hlt2Conf.algorithms_thor import ParticleCombiner


@configurable
def _make_tau_to_3body(particle1,
                       particle2,
                       particle3,
                       descriptor,
                       name="HLT2RD_TauTo3Body_Builder",
                       make_pvs=make_pvs,
                       pid_cut=(F.PID_MU > 0),
                       min_particle_with_pid=0,
                       min_mass12=0 * MeV,
                       max_mass12=1978 * MeV,
                       min_mass=1578 * MeV,
                       max_mass=1978 * MeV,
                       max_vtxchi2dof=9,
                       min_flightdist=100 * micrometer,
                       max_ipchi2=16):
    """
    Builder for tau decaying to three particles.
    """
    pvs = make_pvs()

    combination12_code = in_range(min_mass12, F.MASS, max_mass12)

    combination_code = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.SUM(pid_cut) >= min_particle_with_pid)

    vertex_code = F.require_all(F.CHI2DOF < max_vtxchi2dof,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVLTIME(pvs) * c_light > min_flightdist)

    return ParticleCombiner([particle1, particle2, particle3],
                            name=name,
                            DecayDescriptor=descriptor,
                            Combination12Cut=combination12_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def _make_tau_to_2body(particle1,
                       particle2,
                       descriptor,
                       name="HLT2RD_TauTo2Body_Builder",
                       make_pvs=make_pvs,
                       pid_cut=(F.PID_MU > 0),
                       min_particle_with_pid=0,
                       min_mass=1578 * MeV,
                       max_mass=1978 * MeV,
                       max_vtxchi2dof=9,
                       min_flightdist=100 * micrometer,
                       max_ipchi2=16):
    """
    Builder for tau decaying to two particles.
    """
    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.SUM(pid_cut) >= min_particle_with_pid)

    vertex_code = F.require_all(F.CHI2DOF < max_vtxchi2dof,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVLTIME(pvs) * c_light > min_flightdist)

    return ParticleCombiner([particle1, particle2],
                            name=name,
                            DecayDescriptor=descriptor,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def _make_tau_to_nmu(muons,
                     muons_loose,
                     descriptor,
                     name="HLT2RD_TauToNMu_Builder",
                     make_pvs=make_pvs,
                     n_muons=5,
                     min_mass=1378 * MeV,
                     max_mass=2178 * MeV,
                     max_vtxchi2dof=25,
                     min_flightdist=100 * micrometer,
                     max_ipchi2=16):
    """
    Builder for tau decaying to five, seven or nine muons with at least three
    muons fulfilling PID requirements
    """
    if n_muons not in {5, 7, 9}:
        raise ConfigurationError("Parameter n_muons must be 5, 7 or 9")

    pvs = make_pvs()

    dimuons = ParticleCombiner([muons_loose, muons_loose],
                               name="HLT2RD_TauToNMu_TwoMuonCombiner",
                               DecayDescriptor="Xu0 -> mu+ mu-",
                               CombinationCut=F.MASS < max_mass)

    trimuons = ParticleCombiner([muons, muons, muons],
                                name="HLT2RD_TauToNMu_ThreeMuonCombiner",
                                DecayDescriptor="[Xu+ -> mu+ mu+ mu-]cc",
                                CombinationCut=F.MASS < max_mass)

    combination_code = in_range(min_mass, F.MASS, max_mass)

    vertex_code = F.require_all(F.CHI2DOF < max_vtxchi2dof,
                                F.BPVIPCHI2(pvs) < max_ipchi2,
                                F.BPVLTIME(pvs) * c_light > min_flightdist)

    return ParticleCombiner(
        [trimuons] + int((n_muons - 3) / 2) * [dimuons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)

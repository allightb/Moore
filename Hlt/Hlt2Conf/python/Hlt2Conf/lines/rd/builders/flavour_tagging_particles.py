###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#adding for flavour tagging
from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_protons,
    make_long_electrons_with_brem, make_ismuon_long_muon)
from Hlt2Conf.flavourTagging import (
    #make_sameside_tagging_pions,
    #make_sameside_tagging_protons,
    #make_oppositeside_vertex_charge_tagging_particles,
    make_oppositeside_tagging_kaons,
    make_oppositeside_tagging_electrons,
    make_oppositeside_tagging_muons)
from PyConf.Algorithms import (Run2SSPionTagger, Run2SSProtonTagger,
                               Run2OSKaonTagger, Run2OSElectronTagger,
                               Run2OSMuonTagger, Run2OSVertexChargeTagger,
                               FlavourTagsMerger)
from RecoConf.reconstruction_objects import make_pvs
from GaudiKernel.SystemOfUnits import GeV
import Functors as F
from Hlt2Conf.algorithms_thor import ParticleFilter
from PyConf.Algorithms import AdvancedCloneKiller
from Functors.math import in_range


## For sstagger without UT
def _make_pions_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_protons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_electrons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def _make_muons_with_selection(make_particles, selection_code):
    return ParticleFilter(Input=make_particles(), Cut=F.FILTER(selection_code))


def noUT_sameside_tagging_pions(pvs=make_pvs,
                                p_min=2 * GeV,
                                p_max=200 * GeV,
                                pt_min=0.4 * GeV,
                                pt_max=10 * GeV,
                                eta_max=5.12,
                                pidp_max=5,
                                pidk_max=5):
    selection_code = F.require_all(
        in_range(p_min, F.P, p_max), in_range(pt_min, F.PT, pt_max),
        F.PID_P < pidp_max, F.PID_K < pidk_max, F.ETA < eta_max)

    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)

    return AdvancedCloneKiller(InputParticles=[long_pions])


def noUT_sameside_tagging_protons(pvs=make_pvs,
                                  p_min=2 * GeV,
                                  p_max=200 * GeV,
                                  pt_min=0.4 * GeV,
                                  pt_max=10 * GeV,
                                  eta_max=5.12,
                                  pidp_min=5):
    selection_code = F.require_all(
        in_range(p_min, F.P, p_max), in_range(pt_min, F.PT, pt_max),
        F.PID_P > pidp_min, F.ETA < eta_max)
    long_protons = _make_protons_with_selection(make_has_rich_long_protons,
                                                selection_code)

    return AdvancedCloneKiller(InputParticles=[long_protons])


def noUT_oppositeside_vertex_charge_tagging_particles(pvs=make_pvs,
                                                      p_min=2 * GeV,
                                                      p_max=200 * GeV,
                                                      pt_max=10 * GeV,
                                                      eta_max=5.12):
    selection_code = F.require_all(F.P > p_min, F.P < p_max, F.PT < pt_max,
                                   F.ETA < eta_max)
    long_pions = _make_pions_with_selection(make_has_rich_long_pions,
                                            selection_code)
    long_muons = _make_muons_with_selection(make_ismuon_long_muon,
                                            selection_code)
    long_electrons = _make_electrons_with_selection(
        make_long_electrons_with_brem, selection_code)

    return AdvancedCloneKiller(
        InputParticles=[long_pions, long_muons, long_electrons])


def extra_outputs_for_FlavourTagging_ForSprucing(BCandidates, pvs):
    ssPions = noUT_sameside_tagging_pions()
    # ssPions = make_sameside_tagging_pions()
    ssPionTagging = Run2SSPionTagger(
        BCandidates=BCandidates, TaggingPions=ssPions, PrimaryVertices=pvs)

    ssProtons = noUT_sameside_tagging_protons()
    # ssProtons = make_sameside_tagging_protons()
    ssProtonTagging = Run2SSProtonTagger(
        BCandidates=BCandidates, TaggingProtons=ssProtons, PrimaryVertices=pvs)

    osKaons = make_oppositeside_tagging_kaons()
    osKaonTagging = Run2OSKaonTagger(
        BCandidates=BCandidates, TaggingKaons=osKaons, PrimaryVertices=pvs)

    osElectrons = make_oppositeside_tagging_electrons()
    osElectronTagging = Run2OSElectronTagger(
        BCandidates=BCandidates,
        TaggingElectrons=osElectrons,
        PrimaryVertices=pvs)

    osMuons = make_oppositeside_tagging_muons()
    osMuonTagging = Run2OSMuonTagger(
        BCandidates=BCandidates, TaggingMuons=osMuons, PrimaryVertices=pvs)

    osVertexParticles = noUT_oppositeside_vertex_charge_tagging_particles()
    # osVertexParticles = make_oppositeside_vertex_charge_tagging_particles()
    osVertexChargeTagging = Run2OSVertexChargeTagger(
        BCandidates=BCandidates,
        TaggingParticles=osVertexParticles,
        PrimaryVertices=pvs)

    flavourTags = FlavourTagsMerger(FlavourTagsIterable=[
        ssPionTagging, ssProtonTagging, osKaonTagging, osElectronTagging,
        osMuonTagging, osVertexChargeTagging
    ])

    extra_outputs = [("SSTaggingPions", ssPions),
                     ("SSTaggingProtons", ssProtons),
                     ("OSTaggingKaons", osKaons),
                     ("OSTaggingElectron", osElectrons),
                     ("OSTaggingMuon", osMuons),
                     ("OSVertexChargeParticles", osVertexParticles),
                     ("FlavourTags", flavourTags)]

    return extra_outputs


def extra_outputs_for_FlavourTagging(BCandidates, persistreco=False):
    from Hlt2Conf.standard_particles import make_has_rich_up_pions
    longTaggingParticles = make_has_rich_long_pions()
    upstreamTaggingParticles = make_has_rich_up_pions()
    if persistreco:
        extra_outputs = [('UpstreamTaggingParticles',
                          upstreamTaggingParticles)]
    else:
        extra_outputs = [('LongTaggingParticles', longTaggingParticles),
                         ('UpstreamTaggingParticles',
                          upstreamTaggingParticles)]
    return extra_outputs

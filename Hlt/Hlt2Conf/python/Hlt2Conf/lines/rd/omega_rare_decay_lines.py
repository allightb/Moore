###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of rare omega baryonic lines (FCNC).

Athor: Chuangxin Lin
Email: chuangxin.lin@cern.ch
Date: 30/12/2021

The following modes are included (Xi- with LLL and DDL):
* Omega- -> Xi- mu+ mu-
* Omega- -> Xi- e+ e-
* Omega- -> Xi- gamma
* Omega- -> Xi- pi+ pi-
"""

from PyConf import configurable
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from GaudiKernel.SystemOfUnits import MeV
from RecoConf.reconstruction_objects import make_pvs
from .builders.rd_prefilters import rd_prefilter, _VRD_MONITORING_VARIABLES
from .builders.rdbuilder_thor import make_rd_detached_muons, make_rd_detached_electrons, make_rd_detached_pions, make_rd_photons
from .builders.omega_rare_decay_builder import make_xim_to_lambda_pim_lll, make_omega_to_xi_dip, make_omega_to_xi_gamma
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

all_lines = {}


@register_line_builder(all_lines)
@configurable
def omega_to_ximumu_lll_line(name="Hlt2RD_OmegamToXimMuMu_LLL", prescale=1):
    descriptor = "[Omega- -> Xi- mu+ mu-]cc"
    pvs = make_pvs()
    xim = make_xim_to_lambda_pim_lll(pvs=pvs)
    p1 = make_rd_detached_muons()
    p2 = make_rd_detached_muons()
    omega = make_omega_to_xi_dip(
        name="omega_to_ximumu_lll",
        pvs=pvs,
        hyperon=xim,
        particle1=p1,
        particle2=p2,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [xim, omega],
        extra_outputs=parent_isolation_output("Omega", omega),
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def omega_to_xiee_lll_line(name="Hlt2RD_OmegamToXimEE_LLL", prescale=1):
    descriptor = "[Omega- -> Xi- e+ e-]cc"
    pvs = make_pvs()
    xim = make_xim_to_lambda_pim_lll(pvs=pvs)
    p1 = make_rd_detached_electrons()
    p2 = make_rd_detached_electrons()
    omega = make_omega_to_xi_dip(
        name="omega_to_xiee_lll",
        pvs=pvs,
        hyperon=xim,
        particle1=p1,
        particle2=p2,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [xim, omega],
        extra_outputs=parent_isolation_output("Omega", omega),
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def omega_to_xipipi_lll_line(name="Hlt2RD_OmegamToXimPiPi_LLL", prescale=1):
    descriptor = "[Omega- -> Xi- pi+ pi-]cc"
    pvs = make_pvs()
    xim = make_xim_to_lambda_pim_lll(pvs=pvs)
    p1 = make_rd_detached_pions()
    p2 = make_rd_detached_pions()
    omega = make_omega_to_xi_dip(
        name="omega_to_xipipi_lll",
        pvs=pvs,
        hyperon=xim,
        particle1=p1,
        particle2=p2,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [xim, omega],
        extra_outputs=parent_isolation_output("Omega", omega),
        monitoring_variables=_VRD_MONITORING_VARIABLES)


@register_line_builder(all_lines)
@configurable
def omega_to_xigamma_lll_line(name="Hlt2RD_OmegamToXimGamma_LLL", prescale=1):
    descriptor = "[Omega- -> Xi- gamma]cc"
    pvs = make_pvs()
    xim = make_xim_to_lambda_pim_lll(pvs=pvs)
    gamma = make_rd_photons(et_min=330 * MeV, e_min=500 * MeV)
    omega = make_omega_to_xi_gamma(
        name="omega_to_xigamma_lll",
        pvs=pvs,
        hyperon=xim,
        gamma=gamma,
        descriptor=descriptor)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [xim, omega],
        extra_outputs=parent_isolation_output("Omega", omega),
        monitoring_variables=_VRD_MONITORING_VARIABLES)

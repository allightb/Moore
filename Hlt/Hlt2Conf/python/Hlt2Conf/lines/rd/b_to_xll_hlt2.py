###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from .builders.rd_prefilters import rd_prefilter, _RD_MONITORING_VARIABLES
from RecoConf.reconstruction_objects import make_pvs
from .builders import rdbuilder_thor, rd_isolation
from .builders import b_to_xll_builders
from PyConf import configurable

from GaudiKernel.SystemOfUnits import MeV
import Functors as F

all_lines = {}
"""
Definition of the b -> X ll exclusive lines

Includes:
    2024 hee lines
        Contact: Richard Williams (richard.morgan.williams@cern.ch)
        B+ -> J/psi(1S)  K+  (cc)
        B+ -> J/psi(1S) pi+ (cc)

        2 lines per, J/psi(1S) -> e+ e- (dielectron system only)

    2024 hmumu lines
        Contact: Fernando Abudinen (fernando.abudinen@cern.ch)
        B(c)+ -> J/psi(1S) pi+ (no PID requirement on pi+) (cc)

        2 lines per, J/psi(1S) -> mu+ mu- (dimuon system only)

    2024 Meson lines
        Contact: Richard Williams (richard.morgan.williams@cern.ch)
        B+ -> J/psi(1S) K*+ (-> K+ pi0(resolved) ) (cc)
        B0 -> J/psi(1S) K*0  (-> K+ pi- (cc))
        B0 -> J/psi(1S) phi0 (-> K+ K-)
        B0 -> J/psi(1S) rho0 (-> pi+ pi-)
        B+ -> J/psi(1S) K+ K+ K- (cc)
        B+ -> J/psi(1S) K+ pi+ pi- (cc)

        4 lines per, J/psi(1S) -> mu+ mu-, e+ e-, mu+ mu+, e+ e-

        Bc+ -> J/psi(1S)(-> mu+ mu-) 3h+ (cc)

    Hadron lines
        Contact: Jamie Gooding (jamie.gooding@cern.ch)
        B(s)0 -> J/psi(1S) (-> mu+ mu-) p+ p~-
        B(s)0 -> J/psi(1S) (-> mu+ mu+) p+ p~- (cc)
        B- -> J/psi(1S) (-> mu+ mu-) Lambda0 (-> p+ pi-) p~- (cc)
        B- -> J/psi(1S) (-> mu+ mu+) Lambda0 (-> p+ pi-) p~- (cc)
"""

####################################
#      B+ -> K+ ee HLT2 lines      #
####################################

#### Selections #####
BtoKee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "kaons": {
        "pt_min": 400. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 0. * MeV,
        "pid": (F.PID_K > -4.),
    }
}


@register_line_builder(all_lines)
def BuToKpEE_line(name="Hlt2RD_BuToKpEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], opposite_sign=True)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def BuToKpEE_SameSign_line(name="Hlt2RD_BuToKpEE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], opposite_sign=False)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#####################################
#      B+ -> pi+ ee HLT2 lines      #
#####################################

#### Selections #####
Btopiee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "pions": {
        "pt_min": 400. * MeV,
        "mipchi2dvprimary_min": 9.,
        "p_min": 0. * MeV,
        "pid": (F.PID_K <= 4.),
    }
}


@configurable
@register_line_builder(all_lines)
def BuToPipEE_line(name="Hlt2RD_BuToPipEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], opposite_sign=True)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )

    pi_p_child = rd_isolation.find_in_decay(input=B, id='pi+')
    e_p_child = rd_isolation.find_in_decay(input=B, id='e+')
    e_m_child = rd_isolation.find_in_decay(input=B, id='e-')

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=['pi+', 'e+', 'e-'],
        candidates=[pi_p_child, e_p_child, e_m_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@configurable
@register_line_builder(all_lines)
def BuToPipEE_SameSign_line(name="Hlt2RD_BuToPipEE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], opposite_sign=False)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#####################################
#      B(c)+ -> h+ mumu HLT2 lines      #
#####################################

#### Selections #####
Btohmumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_100. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 16.,
        "vchi2pdof_max": 36.,
        "bpvipchi2_max": 36.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 3.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 300. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 9.,
        "am_min": 0. * MeV,
        "am_max": 7_100. * MeV,
    },
    "pions": {
        "pt_min": 300. * MeV,
        "mipchi2dvprimary_min": 3.,
        "p_min": 0. * MeV,
        "pid": None,
    }
}


@configurable
@register_line_builder(all_lines)
def BuToHpMuMu_Incl_line(name="Hlt2RD_BuToHpMuMu_Incl", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btohmumu["dimuons"], same_sign=False)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btohmumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btohmumu["B"],
    )

    pi_p_child = rd_isolation.find_in_decay(input=B, id='pi+')
    mu_p_child = rd_isolation.find_in_decay(input=B, id='mu+')
    mu_m_child = rd_isolation.find_in_decay(input=B, id='mu-')

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=['B'],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=['pi+', 'mu+', 'mu-'],
        candidates=[pi_p_child, mu_p_child, mu_m_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES)


@configurable
@register_line_builder(all_lines)
def BuToHpMuMu_Incl_SameSign_line(name="Hlt2RD_BuToHpMuMu_Incl_SameSign",
                                  prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btohmumu["dimuons"], same_sign=True)
    pions = rdbuilder_thor.make_rd_detached_pions(**Btohmumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btohmumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
#      B+ -> K+ pi0 ll HLT2 lines      #
########################################

#### Selections #####
BtoKpResolvedPi0ee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "Kstps": {
        "PIDK_K_min": 1.,
        "p_K_min": 0. * MeV,
        "am_min": 600. * MeV,
        "am_max": 1_200. * MeV,
        "pi0_type": "resolved",
        "Kstp_PT": 400. * MeV,
        "min_ipchi2_k": 9.,
        "Kstp_max_vtxchi2": 36.,
        "pt_K_min": 400. * MeV,
        "pt_pi_min": 600. * MeV,
    }
}
BtoKpResolvedPi0mumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kstps": {
        "PIDK_K_min": 1.,
        "p_K_min": 0. * MeV,
        "am_min": 600. * MeV,
        "am_max": 1_200. * MeV,
        "pi0_type": "resolved",
        "Kstp_PT": 400. * MeV,
        "min_ipchi2_k": 9.,
        "Kstp_max_vtxchi2": 36.,
        "pt_K_min": 400. * MeV,
        "pt_pi_min": 600. * MeV,
    }
}


@register_line_builder(all_lines)
def BuToKpResolvedPi0EE_line(name="Hlt2RD_BuToKpResolvedPi0EE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], opposite_sign=True)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpResolvedPi0EE_SameSign_line(
        name="Hlt2RD_BuToKpResolvedPi0EE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], opposite_sign=False)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpResolvedPi0MuMu_line(name="Hlt2RD_BuToKpResolvedPi0MuMu",
                               prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=False)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(
        **BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpResolvedPi0MuMu_SameSign_line(
        name="Hlt2RD_BuToKpResolvedPi0MuMu_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=True)
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(
        **BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#########################################
#      B0 -> pi+ pi- ll HLT2 lines      #
#########################################

#### Selections #####
Btopipiee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "rhos": {
        "pi_pid": (F.PID_K <= 4.),
        "am_min": 0. * MeV,
        "am_max": 2_600. * MeV,
        "pi_pt_min": 350. * MeV,
        "pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
    }
}

Btopipimumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "rhos": {
        "pi_pid": (F.PID_K <= 4.),
        "am_min": 0. * MeV,
        "am_max": 2_600. * MeV,
        "pi_pt_min": 350. * MeV,
        "pt_min": 250. * MeV,
        "pi_ipchi2_min": 4.,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
    }
}


@register_line_builder(all_lines)
def B0ToPipPimEE_line(name="Hlt2RD_B0ToPipPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], opposite_sign=True)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipiee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToPipPimEE_SameSign_line(name="Hlt2RD_B0ToPipPimEE_SameSign",
                               prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], opposite_sign=False)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipiee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToPipPimMuMu_line(name="Hlt2RD_B0ToPipPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=False)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipimumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToPipPimMuMu_SameSign_line(name="Hlt2RD_B0ToPipPimMuMu_SameSign",
                                 prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=True)
    rhos = rdbuilder_thor.make_rd_detached_rho0(**Btopipimumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="[B0 -> J/psi(1S) rho(770)0]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


#######################################
#      B0 -> K+ K- ll HLT2 lines      #
#######################################

#### Selections #####
BtoKKee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "phis": {
        "k_pid": (F.PID_K > -4.),
        "am_min": 0. * MeV,
        "am_max": 1100. * MeV,
        "k_p_min": 0. * MeV,
        "k_pt_min": 0. * MeV,
        "k_ipchi2_min": 4.,
        "phi_pt_min": 0. * MeV,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
    }
}

BtoKKmumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "phis": {
        "k_pid": (F.PID_K > -4.),
        "am_min": 0. * MeV,
        "am_max": 1100. * MeV,
        "k_p_min": 0. * MeV,
        "k_pt_min": 0. * MeV,
        "k_ipchi2_min": 4.,
        "phi_pt_min": 0. * MeV,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
    }
}


@register_line_builder(all_lines)
def B0ToKpKmEE_line(name="Hlt2RD_B0ToKpKmEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], opposite_sign=True)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToKpKmEE_SameSign_line(name="Hlt2RD_B0ToKpKmEE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], opposite_sign=False)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToKpKmMuMu_line(name="Hlt2RD_B0ToKpKmMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=False)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def B0ToKpKmMuMu_SameSign_line(name="Hlt2RD_B0ToKpKmMuMu_SameSign",
                               prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=True)
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="[B0 -> J/psi(1S) phi(1020)]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


########################################
#      B0 -> K+ pi- ll HLT2 lines      #
########################################

#### Selections #####
BtoKPiee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "Kst0s": {
        "am_min": 0. * MeV,
        "am_max": 2_600. * MeV,
        "pi_pid": (F.PID_K < 4.),
        "k_pid": (F.PID_K > -4.),
        "pi_p_min": 2_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 9.,
        "k_p_min": 1_000. * MeV,
        "k_pt_min": 250. * MeV,
        "k_ipchi2_min": 9.,
        "kstar0_pt_min": 400. * MeV,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
        "bpvipchi2_min": 16.,
    }
}

BtoKPimumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "Kst0s": {
        "am_min": 0. * MeV,
        "am_max": 2_600. * MeV,
        "pi_pid": (F.PID_K < 4.),
        "k_pid": (F.PID_K > -4.),
        "pi_p_min": 2_000. * MeV,
        "pi_pt_min": 250. * MeV,
        "pi_ipchi2_min": 4.,
        "k_p_min": 1_000. * MeV,
        "k_pt_min": 250. * MeV,
        "k_ipchi2_min": 4.,
        "kstar0_pt_min": 400. * MeV,
        "adocachi2cut": 36.,
        "vchi2pdof_max": 25.,
        "bpvipchi2_min": 4.,
    }
}


@register_line_builder(all_lines)
def B0ToKpPimEE_line(name="Hlt2RD_B0ToKpPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], opposite_sign=True)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPiee["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def B0ToKpPimEE_SameSign_line(name="Hlt2RD_B0ToKpPimEE_SameSign", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], opposite_sign=False)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPiee["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def B0ToKpPimMuMu_line(name="Hlt2RD_B0ToKpPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=False)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPimumu["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def B0ToKpPimMuMu_SameSign_line(name="Hlt2RD_B0ToKpPimMuMu_SameSign",
                                prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=True)
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**BtoKPimumu["Kst0s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


############################################
#      B+ -> K+ pi+ pi- ll HLT2 lines      #
############################################

#### Selections #####
BtoKPiPiee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "K1s": {
        "pi_PIDK_max": 4.,
        "K_PIDK_min": -4.,
        "K_1_pt_min": 0. * MeV,
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 0. * MeV,
        "pi_ipchi2_min": 9.,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 0. * MeV,
        "K_ipchi2_min": 9.,
        "vchi2pdof_max": 9.,
        "adocachi2_max": 36.,
        "am_min": 0. * MeV,
        "am_max": 4200. * MeV,
        "K_1_min_DIRA": None,
        "K_1_min_bpvfdchi2": 81.,
        "bpvipchi2_min": 16.,
    },
}

BtoKPiPimumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K1s": {
        "pi_PIDK_max": 4.,
        "K_PIDK_min": -4.,
        "K_1_pt_min": 0. * MeV,
        "pi_p_min": 1_000. * MeV,
        "pi_pt_min": 0. * MeV,
        "pi_ipchi2_min": 9.,
        "K_p_min": 1_000. * MeV,
        "K_pt_min": 0. * MeV,
        "K_ipchi2_min": 9.,
        "vchi2pdof_max": 9.,
        "adocachi2_max": 36.,
        "am_min": 0. * MeV,
        "am_max": 4200. * MeV,
        "K_1_min_DIRA": None,
        "K_1_min_bpvfdchi2": 36.,
        "bpvipchi2_min": 9.,
    },
}


@register_line_builder(all_lines)
def BuToKpPipPimEE_line(name="Hlt2RD_BuToKpPipPimEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], opposite_sign=True)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPiee["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+ ]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, K1s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpPipPimEE_SameSign_line(name="Hlt2RD_BuToKpPipPimEE_SameSign",
                                 prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], opposite_sign=False)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPiee["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, K1s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpPipPimMuMu_line(name="Hlt2RD_BuToKpPipPimMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=False)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPimumu["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K1s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpPipPimMuMu_SameSign_line(name="Hlt2RD_BuToKpPipPimMuMu_SameSign",
                                   prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=True)
    K1s = rdbuilder_thor.make_rd_detached_K1(**BtoKPiPimumu["K1s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K1s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


##########################################
#      B+ -> K+ K+ K- ll HLT2 lines      #
##########################################

#### Selections #####
Bto3Kee = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 100.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dielectrons": {
        "adocachi2cut_max": 36.,
        "ipchi2_e_min": 9.,
        "pid_e_min": -4.0,
        "pt_diE_min": 0. * MeV,
        "pt_e_min": 350. * MeV,
        "p_e_min": 0. * MeV,
        "vfaspfchi2ndof_max": 9.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
        "bpvvdchi2_min": 36.,
    },
    "K2s": {
        "adocachi2_max": 36.,
        "am_min": 0. * MeV,
        "am_max": 4_200. * MeV,
        "K_PIDK": (F.PID_K > -2.),
        "K_p_min": 0. * MeV,
        "K_pt_min": 0. * MeV,
        "K_ipchi2_min": 9.,
        "K_2_pt_min": 0. * MeV,
        "vchi2pdof_max": 12.,
        "K_2_min_bpvfdchi2": 81.,
        "bpvipchi2_min": 16.,
    },
}

Bto3Kmumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_000. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 64.,
        "vchi2pdof_max": 9.,
        "bpvipchi2_max": 25.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 5_500. * MeV,
    },
    "K2s": {
        "adocachi2_max": 36.,
        "am_min": 0. * MeV,
        "am_max": 4_200. * MeV,
        "K_PIDK": (F.PID_K > -2.),
        "K_p_min": 0. * MeV,
        "K_pt_min": 0. * MeV,
        "K_ipchi2_min": 9.,
        "K_2_pt_min": 0. * MeV,
        "vchi2pdof_max": 12.,
        "K_2_min_bpvfdchi2": 0.,
        "bpvipchi2_min": 9.,
    },
}


@register_line_builder(all_lines)
def BuToKpKpKmEE_line(name="Hlt2RD_BuToKpKpKmEE", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], opposite_sign=True)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kee["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpKpKmEE_SameSign_line(name="Hlt2RD_BuToKpKpKmEE_SameSign",
                               prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], opposite_sign=False)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kee["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpKpKmMuMu_line(name="Hlt2RD_BuToKpKpKmMuMu", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=False)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToKpKpKmMuMu_SameSign_line(name="Hlt2RD_BuToKpKpKmMuMu_SameSign",
                                 prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=True)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3Kmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


###########################################
#      Bc+ -> h+ h+ h- ll HLT2 lines      #
###########################################

#### Selections #####
Bto3hmumu = {
    "B": {
        "am_min": 4_500. * MeV,
        "am_max": 7_100. * MeV,
        "B_pt_min": 0. * MeV,
        "FDchi2_min": 16.,
        "vchi2pdof_max": 36.,
        "bpvipchi2_max": 36.,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.,
        "ipchi2_muon_min": 9.,
        "pidmu_muon_min": -4.,
        "pt_dimuon_min": 0. * MeV,
        "pt_muon_min": 350. * MeV,
        "p_muon_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "bpvvdchi2_min": 16.,
        "am_min": 0. * MeV,
        "am_max": 7_100. * MeV,
    },
    "K2s": {
        "adocachi2_max": 16.,
        "am_min": 0. * MeV,
        "am_max": 7_100. * MeV,
        "K_PIDK": None,
        "K_p_min": 0. * MeV,
        "K_pt_min": 250. * MeV,
        "K_ipchi2_min": 9.,
        "K_2_pt_min": 0. * MeV,
        "vchi2pdof_max": 9.,
        "K_2_min_bpvfdchi2": 0.,
        "bpvipchi2_min": 4.,
    },
}


@register_line_builder(all_lines)
def BcToHpHpHmMuMu_line(name="Hlt2RD_BcToHpHpHmMuMu_Inclusive", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3hmumu["dimuons"], same_sign=False)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3hmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3hmumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=['B'],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BcToHpHpHmMuMu_SameSign_line(
        name="Hlt2RD_BcToHpHpHmMuMu_Inclusive_SameSign", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3hmumu["dimuons"], same_sign=True)
    K2s = rdbuilder_thor.make_rd_detached_K2(**Bto3hmumu["K2s"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3hmumu["B"])

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=['B'],
        candidates=[B],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, K2s, B],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#########################
# BToXpbarll HLT2 lines #
#########################


@register_line_builder(all_lines)
def BToPpPmEE_line(name="Hlt2RD_BToPpPmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bs"],
        candidates=[Bs],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def BToPpPmEE_SameSign_line(name="Hlt2RD_BToPpPmEESS", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bs"],
        candidates=[Bs],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def BuToL0PmEE_LL_line(name="Hlt2RD_BuToL0PmEE_LL", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        am_Xpbar_min=2000. * MeV,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToL0PmEE_LL_SameSign_line(name="Hlt2RD_BuToL0PmEESS_LL", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#@register_line_builder(all_lines)
def BuToL0PmEE_DD_line(name="Hlt2RD_BuToL0PmEE_DD", prescale=1):

    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#@register_line_builder(all_lines)
def BuToL0PmEE_DD_SameSign_line(name="Hlt2RD_BuToL0PmEESS_DD", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9., opposite_sign=False)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BToPpPmMuMu_line(name="Hlt2RD_BToPpPmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bs"],
        candidates=[Bs],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def BToPpPmMuMu_SameSign_line(name="Hlt2RD_BToPpPmMuMuSS", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bs"],
        candidates=[Bs],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def BuToL0PmMuMu_LL_line(name="Hlt2RD_BuToL0PmMuMu_LL", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BuToL0PmMuMu_LL_SameSign_line(name="Hlt2RD_BuToL0PmMuMuSS_LL", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#@register_line_builder(all_lines)
def BuToL0PmMuMu_DD_line(name="Hlt2RD_BuToL0PmMuMu_DD", prescale=1):

    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#@register_line_builder(all_lines)
def BuToL0PmMuMu_DD_SameSign_line(name="Hlt2RD_BuToL0PmMuMuSS_DD", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        same_sign=True, vchi2pdof_max=9.)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000. * MeV,
        pt_min=300. * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.))

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc")
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Bu"],
        candidates=[Bu],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def B0ToKstJpsiMuMu_looseB0forK1_line(
        name="Hlt2RD_B0ToKstJpsiMuMu_looseB0forK1", prescale=1):

    pvs = make_pvs()

    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        pt_dimuon_min=0. * MeV,
        pt_muon_min=400. * MeV,
        p_muon_min=4000. * MeV,
        ipchi2_muon_min=12,
        pidmu_muon_min=-4,
        adocachi2cut_max=9.,
        bpvvdchi2_min=100,
        vchi2pdof_max=9.,
        am_min=210. * MeV,
        am_max=4430. * MeV,
    )

    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=800. * MeV,
        am_max=1000. * MeV,
        pi_p_min=2000. * MeV,
        pi_pt_min=100. * MeV,
        pi_ipchi2_min=10,
        pi_pid=(F.PID_K < 0),
        k_p_min=2000. * MeV,
        k_pt_min=250. * MeV,
        k_ipchi2_min=10.,
        k_pid=(F.PID_K > 0),
        kstar0_pt_min=500. * MeV,
        adocachi2cut=9.,
        vchi2pdof_max=9.,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        am_min=3000 * MeV,
        am_max=6500 * MeV,
        B_pt_min=500 * MeV,
        bpvipchi2_max=50,
        min_cosine=0.9995,
        FDchi2_min=100,
        vchi2pdof_max=7.,
        low_factor=0.9,
        high_factor=1.1,
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0"],
        candidates=[B0],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, hadron, B0],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def B0ToKstJpsiMuMu_looseB0forK1_SameSign_line(
        name="Hlt2RD_B0ToKstJpsiMuMuSS_looseB0forK1", prescale=1):

    pvs = make_pvs()

    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        pt_dimuon_min=0. * MeV,
        pt_muon_min=400. * MeV,
        p_muon_min=4000. * MeV,
        ipchi2_muon_min=12,
        pidmu_muon_min=-4,
        adocachi2cut_max=9.,
        bpvvdchi2_min=100,
        vchi2pdof_max=9.,
        am_min=210. * MeV,
        am_max=4430. * MeV,
        same_sign=True,
    )

    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=800. * MeV,
        am_max=1000. * MeV,
        pi_p_min=2000. * MeV,
        pi_pt_min=100. * MeV,
        pi_ipchi2_min=10,
        pi_pid=(F.PID_K < 0),
        k_p_min=2000. * MeV,
        k_pt_min=250. * MeV,
        k_ipchi2_min=10.,
        k_pid=(F.PID_K > 0),
        kstar0_pt_min=500. * MeV,
        adocachi2cut=9.,
        vchi2pdof_max=9.,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        am_min=3000 * MeV,
        am_max=6500 * MeV,
        B_pt_min=500 * MeV,
        bpvipchi2_max=50,
        min_cosine=0.9995,
        FDchi2_min=100,
        vchi2pdof_max=7.,
        low_factor=0.9,
        high_factor=1.1,
    )

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0"],
        candidates=[B0],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()))

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, hadron, B0],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )

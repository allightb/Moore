###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test ThOr selection algorithm wrappers correctly validate input types."""
import pytest

import Functors as F

from .. import algorithms_thor as thor
from ..standard_particles import make_long_cb_pions

from PyConf.packing import persistreco_version
persistreco_version.global_bind(version=0.0)


def test_v1_filter_v2_inputs():
    """A v1 filter cannot be instantiated with v2 input."""
    v2_pions = make_long_cb_pions()
    with thor.thor_backend.bind(particle_api=thor.PARTICLE_V1):
        with pytest.raises(TypeError):
            thor.ParticleFilter(v2_pions, F.ALL)


def test_v1_combiner_v2_inputs():
    """A v1 combiner cannot be instantiated with v2 input."""
    v2_pions = make_long_cb_pions()
    with thor.thor_backend.bind(particle_api=thor.PARTICLE_V1):
        with pytest.raises(TypeError):
            thor.ParticleCombiner(
                [v2_pions, v2_pions],
                DecayDescriptor="KS0 -> pi+ pi-",
                CombinationCut=F.ALL,
                CompositeCut=F.ALL,
            )

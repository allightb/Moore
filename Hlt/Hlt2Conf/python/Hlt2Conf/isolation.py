###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#Central isolation algorithm
from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg, SelectionFromWeightedRelationTable


@configurable
def extra_outputs_for_isolation(name, ref_particles, extra_particles,
                                selection):
    """
    Save TES locations in extra outputs based on selection of extra particles information 
    Args:
        name: Name of the TES location to store extra particles
        ref_particles: Containers of reference particles 
        extra_particles: Containers of extra particles
        selection: Expression to select combinations
    """
    RelTableAlg = WeightedRelTableAlg(
        InputCandidates=extra_particles,
        ReferenceParticles=ref_particles,
        Cut=selection)

    RelTable = RelTableAlg.OutputRelations

    selection_to_persist = SelectionFromWeightedRelationTable(
        InputRelations=RelTable)

    extra_outputs = [(name, selection_to_persist.OutputLocation)]

    return extra_outputs

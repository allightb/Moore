###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options used for Sprucing 2022 data from Aug HLT2 reprocessing

from Moore import Options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines import sprucing_lines
from Moore.lines import PassLine
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.ift import sprucing_lines as spruce_ift_lines

from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import all_lines as inclusive_detached_dilepton_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import all_lines as charmonium_to_dimuon_detached_lines
from Hlt2Conf.lines.rd import full_lines as rd_full_lines
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.ift import ift_full_lines

from Moore.streams import DETECTORS, Stream, Streams

from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

public_tools = [stateProvider_with_simplified_geom()]

#######################################################
##Should only need to change the following dictionaries.

# Note that wg_lines are themselves dict types here
fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    "ift": spruce_ift_lines,
}

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    #bandq also has charmonium_to_dimuon_lines
    "bandq": [
        "Hlt2BandQ.*Decision", 'Hlt2_JpsiToMuMuDecision',
        'Hlt2_Psi2SToMuMuDecision', 'Hlt2_DiMuonPsi2STightDecision',
        'Hlt2_DiMuonJPsiTightDecision'
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    ##No SL as went to Full
    "charm": ["Hlt2Charm.*Decision"],
    #Note DiMuon lines will now be caught by "Hlt2QEE"
    "qee": ["Hlt2QEE.*Decision", "Hlt2.*DiMuonNoIP.*Decision"],
    #"rd": ["Hlt2RD.*Decision"],
    "rd": ["^(?!.*Gamma.*Incl).*Hlt2RD.*Decision"],
    "bnoc": ["Hlt2BnoC.*Decision"],
    "ift": ["Hlt2IFT_Femtoscopy.*Decision"]
}

# For Turcal make 2 sets of line dict which will create 6 streams
# The first 3 streams will contain all events but no det rawbanks
# The second 3 streams will have a 10% prescale but have all det rawbanks
turcallinedict = {
    "pid": ["Hlt2PID.*Decision"],
    "trackeff": [
        "Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision",
        "Hlt2KshortVeloLong.*Decision"
    ],
    "monitoring": ["Hlt2Monitoring.*Decision"],
    "pid_raw": ["Hlt2PID.*Decision"],
    "trackeff_raw": [
        "Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision",
        "Hlt2KshortVeloLong.*Decision"
    ],
    "monitoring_raw": ["Hlt2Monitoring.*Decision"],
}

## Following configures FULL lines that need TISTOS info in exclusive Sprucing
full_modules_for_TISTOS = [
    topological_b_lines,
    inclusive_detached_dilepton_lines,
    charmonium_to_dimuon_detached_lines,
    rd_full_lines,
    qee_full_lines,
    semileptonic_lines,
    ift_full_lines,
]


def lines_for_TISTOS(full_modules_for_TISTOS):
    """Configure FULL lines - with removals - for TISTOS persistency in exlusive Sprucing
    Note there will hopefully be nicer ways to configure this in the future"""
    from Hlt2Conf.settings.hlt2_pp_commissioning import to_remove

    full_lines_for_TISTOS = [
        linename for module in full_modules_for_TISTOS
        for linename in module.keys()
    ]

    ## for removing lines that are incompatible with DD4HEP
    filtered_full_line_list = [
        line for line in full_lines_for_TISTOS
        if not any(substring in line for substring in to_remove)
    ]
    removed_lines = [
        line for line in full_lines_for_TISTOS
        if any(substring in line for substring in to_remove)
    ]
    print(
        f"The following {len(filtered_full_line_list)} FULL lines will have TISTOS info persistency : {filtered_full_line_list}"
    )
    print(
        f"The following FULL {len(removed_lines)} lines were excluded from the TISTOS info persistency list  : {removed_lines}"
    )
    return filtered_full_line_list


#######################################################


def lines_running(linedict):
    """ Return the full list of lines to be run"""

    return [
        item for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def excl_spruce_production(options: Options):

    spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')

    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    def make_streams():
        streams = [
            Stream(
                wg,
                lines=[builder() for builder in fulllinedict[wg].values()],
                detectors=[]) for wg in fulllinedict
        ]

        return Streams(streams=streams)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    with list_of_full_stream_lines.bind(
            lines=lines_for_TISTOS(full_modules_for_TISTOS)):
        config = run_moore(options, make_streams, public_tools=[])
        return config


def pass_spruce_production(options: Options):
    def make_streams():
        streams = [
            Stream(
                wg,
                lines=[
                    PassLine(
                        name="Pass" + wg, hlt2_filter_code=turbolinedict[wg])
                ],
                detectors=DETECTORS) for wg in turbolinedict
        ]

        return Streams(streams=streams)

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : #{turbolinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config


def turcal_spruce_production(options: Options):
    def make_streams():
        streams = [
            Stream(
                name=wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turcallinedict[wg],
                    )
                ],
                detectors=[]) for wg in turcallinedict if "raw" not in wg
        ]

        streams += [
            Stream(
                name=wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turcallinedict[wg],
                        prescale=0.1)
                ],
                detectors=DETECTORS) for wg in turcallinedict if "raw" in wg
        ]

        return Streams(streams=streams)

    for wg in turcallinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turcallinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config

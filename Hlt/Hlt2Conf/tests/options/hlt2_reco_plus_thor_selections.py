###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example configuration of ThOr-based LHCb::Particle selection algorithms.

The thresholds in each selection algorithm are designed to accept and reject at
least some events to verify that the functors are not simply accepting or
rejecting all input.
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.test.hlt2_test import hlt2_test_lines
from Hlt2Conf.lines.topological_b import all_lines as all_lines_topo


def make_lines():
    general_lines = [builder() for builder in hlt2_test_lines.values()]
    topo_lines = [builder() for builder in all_lines_topo.values()]
    return general_lines + topo_lines


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)

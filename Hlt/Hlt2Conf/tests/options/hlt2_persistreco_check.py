###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if packed reco objects are persisted and persistreco flag works
as expected. Also tests the unpacking of the tracks from pRec/ to Rec/.

Runs over the output file passed as the last argument to this script.

A cleverer version of this script would check the number of packed objects
against the number of objects created upstream in Moore. But we cannot yet
run a GaudiPython job in Moore to get the container counts (see LBCOMP-101),
so instead we use heuristics to estimate the number of objects we should
expect.

"""
import argparse
import sys

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
    LHCb__UnpackRawEvent,
    createODIN,
)

from PyConf.packing import default_persistable_locations
from GaudiConf.reading import do_unpacking
from Hlt2Conf.check_output import check_persistreco, check_particles
from PyConf.application import configured_ann_svc

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('hlt2_manifest', help='HLT2 JSON manifest dump')
args = parser.parse_args()


##Helper function for finding positive line decisions
def advance_HLT(decision):
    while True:
        appMgr.run(1)
        if not TES['/Event']:
            sys.exit("Did not find positive {0} decision".format(decision))

        odin = TES["/Event/DAQ/ODIN"]
        print(f"Processing event {odin.eventNumber()}")
        # skip an event that seems to be selected in v3 but not v2 platforms
        # FIXME this is very fragile, as is the whole test, and it particular
        #       the check for a minimum size of containers.
        if odin.eventNumber() == 247685:
            continue

        reports = TES['/Event/Hlt2/DecReports']
        report = reports.decReport('{0}Decision'.format(decision))
        if report.decision() == 1:
            break

    return


##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

algs = do_unpacking(input_process='Hlt2')
algs += [
    LHCb__UnpackRawEvent(
        BankTypes=['ODIN'], RawBankLocations=["DAQ/RawBanks/ODIN"]),
    createODIN(RawBanks="DAQ/RawBanks/ODIN"),
]

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)
from Gaudi.Configuration import INFO
from Configurables import MessageSvc

MessageSvc().OutputLevel = INFO
appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)
## Locations we expect to be 0
unexpected_locs = [
    'SplitPhotons',
    'Neutrals',
    'MergedPi0s',
    'Electrons',
    'Photons',
    'Rec/Summary',
    'MuonPID',
    'ProtoP/Downstream',
    'ProtoP/Upstream',
    'Track/BestDownstream',
    'Track/BestUpstream',
]
##Here we expect the reconstruction of whole event to be persisted
print('Checking persistreco==True event...')

line = "Hlt2_test_persistreco"
nevents = 2

for ii in range(nevents):
    advance_HLT(line)
    if ii == 0:
        TES.dump()

    locations = default_persistable_locations(stream="/Event/HLT2")
    check_persistreco(TES, locations.values(), 10, unexpected_locs)

    # We should still be persisting the HLT2 line candidates
    check_particles(TES, "/Event/HLT2/" + line, 2)

##Here we do not expect to be able to access the reco directed but via dependencies
print('Checking persistreco==False event...')

line = "Hlt2_test_nopersistreco"
nevents = 2

for ii in range(nevents):
    advance_HLT(line)
    if ii == 0:
        TES.dump()

    # We should still be persisting the HLT2 line candidates
    check_particles(TES, "/Event/HLT2/" + line, 2)

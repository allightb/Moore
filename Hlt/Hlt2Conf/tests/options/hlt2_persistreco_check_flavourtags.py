###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if FlavourTags objects are persisted and the pointers to the taggedB()
point to valid candidates.

Runs over the output file passed as the last argument to this script.

"""
import GaudiPython as GP
from GaudiConf import IOExtension
from GaudiConf.reading import do_unpacking
from Configurables import ApplicationMgr, LHCbApp
from PyConf.application import configured_ann_svc
LHCb = GP.gbl.LHCb

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('hlt2_manifest', help='HLT2 JSON manifest dump')
args = parser.parse_args()

# Change the tags if required
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20201211",
    CondDBtag="sim-20201218-vc-md100",
)

IOExtension().inputFiles([args.input], clear=True)

stream = "/Event/HLT2"
flavour_tags = ['FlavourTags']

locations = [
    '/Event/HLT2/HltDecReports', '/Event/HLT2/Hlt2B2JpsiK/Particles',
    '/Event/HLT2/Hlt2B2JpsiKModified/Particles'
] + [f'/Event/HLT2/Hlt2B2JpsiKModified/{ft}' for ft in flavour_tags
     ] + [f'/Event/HLT2/Hlt2B2JpsiK/{ft}' for ft in flavour_tags]

algs = do_unpacking(input_process="Hlt2")

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc()]

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

while TES['/Event/HLT2']:
    dec_rep = TES['/Event/Hlt2/DecReports']
    if not dec_rep: print("could not obtain Hlt2 decisions")
    for i in ('Hlt2B2JpsiKDecision', 'Hlt2B2JpsiKModifiedDecision'):
        if not dec_rep.hasDecisionName(i):
            print("Missing decision {} in Hlt2DecReports".format(i))
    l = dec_rep.decReport('Hlt2B2JpsiKDecision')
    l_mod = dec_rep.decReport('Hlt2B2JpsiKModifiedDecision')
    if l.decision() or l_mod.decision():
        particles_default = TES['/Event/HLT2/Hlt2B2JpsiK/Particles']
        particles_modified = TES['/Event/HLT2/Hlt2B2JpsiKModified/Particles']

        for flavour_tag in flavour_tags:
            fts_default = TES[f'/Event/HLT2/Hlt2B2JpsiK/{flavour_tag}']
            if particles_default:
                if fts_default:
                    if particles_default.size() != fts_default.size():
                        print(particles_default.size(), fts_default.size())
                        print(
                            "ERROR: different size of particle container and flavourtag container for Hlt2B2JpsiK"
                        )
                    for i in range(len(fts_default)):
                        if not fts_default[i].taggedB():
                            print(
                                "ERROR: FlavourTag.taggedB() does not exist for Hlt2B2JpsiK"
                            )
                        elif fts_default[i].taggedB() != particles_default[i]:
                            print(
                                "ERROR: FlavourTag.taggedB() does not match corresponding B in particles container for Hlt2B2JpsiK"
                            )
                        for t in fts_default[i].taggers():
                            print("type", t.type())
                            print("omega", t.omega())
                            print("mvaValue", t.mvaValue())
                            print("N tag particles", t.taggerParts().size())
                else:
                    print("ERROR: no FlavourTags in Hlt2B2JpsiK")
            else:
                print("ERROR: no particles in Hlt2B2JpsiK")

            fts_modified = TES[
                f'/Event/HLT2/Hlt2B2JpsiKModified/{flavour_tag}']
            if particles_modified:
                if fts_modified:
                    if particles_modified.size() != fts_modified.size():
                        print(
                            "ERROR: different size of particle container and flavourtag container for Hlt2B2JpsiKModified"
                        )
                    for i in range(len(fts_modified)):
                        if not fts_default[i].taggedB():
                            print(
                                "ERROR: FlavourTag.taggedB() does not exist for Hlt2B2JpsiModifiedK"
                            )
                        elif fts_modified[i].taggedB(
                        ) != particles_modified[i]:
                            print(
                                "ERROR: FlavourTag.taggedB() does not match corresponding B in particles container for Hlt2B2JpsiKModified"
                            )
                        for t in fts_modified[i].taggers():
                            print("type", t.type())
                            print("omega", t.omega())
                            print("mvaValue", t.mvaValue())
                            print("N tag particles", t.taggerParts().size())
                else:
                    print("ERROR: no FlavourTags in Hlt2B2JpsiKModified")

    appMgr.run(1)

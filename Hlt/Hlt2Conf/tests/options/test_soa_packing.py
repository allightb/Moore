###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test the persistency of SOA objects
- Tracks
"""

from Configurables import (ApplicationMgr, LHCbApp, HltPackedBufferWriter,
                           HltPackedBufferDecoder, SOATrackPacker, SOAPVPacker)

from PyConf.application import configured_ann_svc, default_raw_event

from PyConf.filecontent_metadata import register_encoding_dictionary
from PRConfig import TestFileDB
from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT, convert_tracks_to_v3_from_v1, get_persistable_tracks_per_type
from RecoConf.legacy_rec_hlt1_tracking import make_reco_pvs
from PyConf.packing import persistable_location
from Configurables import GaudiSequencer
from PyConf.dataflow import dataflow_config


def _data_deps(handle):
    def __walk(visited, found, top):

        if top.name in visited: return
        visited.append(top.name)
        found.append(top)
        for handles in top.inputs.values():
            handles = handles if isinstance(handles, list) else [handles]
            for handle in handles:
                __walk(visited, found, handle.producer)

    visited = []
    found = []
    __walk(visited, found, handle.producer)
    return found


with default_raw_event.bind(raw_event_format=0.5):
    hlt2_tracks = make_hlt2_tracks_without_UT(
        light_reco=True, fast_reco=False, use_pr_kf=True)
    # track conversion to v3 for muon / calo
    persisted_tracks = get_persistable_tracks_per_type(hlt2_tracks)
    tracks_v3, tracks_rels = convert_tracks_to_v3_from_v1(persisted_tracks)

    # PVs
    pvs = make_reco_pvs(hlt2_tracks['Velo'], persistable_location('PVs'))
    pv_algs = _data_deps(pvs["v3"])
    track_algs = _data_deps(tracks_v3["Long"])
    track_algs.reverse()
    pv_algs.reverse()
    algs = track_algs + pv_algs

    configuration = dataflow_config()
    for alg in algs:
        configuration.update(alg.configuration())
    algs, _ = configuration.apply()

    TES = "/Event/Rec"
    locations = []
    locations.append(tracks_v3["Long"].location)
    locations.append(pvs["v3"].location)

    encoding_key = int(
        register_encoding_dictionary("PackedObjectLocations",
                                     sorted(locations)), 16)

    track_packer = SOATrackPacker(
        InputName=[tracks_v3["Long"].location],
        OutputName=TES + "/Buffers/Tracks",
        EnableCheck=True,
        OutputLevel=2,
        EncodingKey=encoding_key)

    pv_packer = SOAPVPacker(
        InputName=[pvs["v3"].location],
        OutputName=TES + "/Buffers/PVs",
        EnableCheck=True,
        OutputLevel=2,
        EncodingKey=encoding_key)

    # Writer
    bank_writer = HltPackedBufferWriter(
        PackedContainers=[
            track_packer.OutputName.Path, pv_packer.OutputName.Path
        ],
        OutputView="/Event/RawBanks/DstData",
        OutputLevel=2,
        SourceID="Hlt2")

    # Decoder
    bank_decoder = HltPackedBufferDecoder(
        RawBanks=bank_writer.OutputView, OutputLevel=2, SourceID="Hlt2")

    algs += [track_packer, pv_packer, bank_writer, bank_decoder]
    seq = GaudiSequencer("Seq", Members=algs)

    ApplicationMgr(TopAlg=[seq], ExtSvc=[configured_ann_svc(name='HltANNSvc')])

    app = LHCbApp(
        DataType="Upgrade",
        Simulation=True,
    )

    #app.OutputLevel=2
    app.EvtMax = 5
    from DDDB.CheckDD4Hep import UseDD4Hep
    if UseDD4Hep:
        from Configurables import DDDBConf
        DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

    # Pick a file that has the reconstruction available
    # Events are not really used, this is a dummy setup
    f = TestFileDB.test_file_db['upgrade_Sept2022_minbias_0fb_md_xdigi']

    f.setqualifiers(configurable=app)
    f.run(configurable=app, withDB=True)

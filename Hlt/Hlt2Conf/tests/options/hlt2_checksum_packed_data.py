###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple configuration that enables checksums and DEBUG output for HltPackedDataWriter.
"""

from Moore import options, run_moore
from Moore.persistence import persist_line_outputs
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

from Hlt2Conf.lines.test.hlt2_test import hlt2_test_lines
from Hlt2Conf.lines.topological_b import all_lines as all_lines_topo

options.output_file = "hlt2_checksum_packed_data.dst"
options.output_type = "ROOT"


def make_lines():
    general_lines = [builder() for builder in hlt2_test_lines.values()]
    topo_lines = [builder() for builder in all_lines_topo.values()]

    lines = []
    # enable all extra options for packing checks
    for line in general_lines + topo_lines:
        line.pv_tracks = True
        line.calo_digits = True
        line.calo_clusters = True
        line.pv_tracks = True
        line.track_ancestors = True
        lines.append(line)
    return lines


public_tools = [stateProvider_with_simplified_geom()]

with persist_line_outputs.bind(enable_checksum=True), reconstruction.bind(
        from_file=False):
    config = run_moore(options, make_lines, public_tools)

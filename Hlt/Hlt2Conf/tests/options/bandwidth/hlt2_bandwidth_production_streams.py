###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from Hlt2Conf.settings.hlt2_pp_2024 import make_streams
import json
from PRConfig.bandwidth_helpers import FileNameHelper

fname_helper = FileNameHelper(process="hlt2")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(
    stream_config="production", ext=".mdf")
options.output_type = 'MDF'
options.output_manifest_file = fname_helper.tck(stream_config="production")


def make_main_streams():
    '''
        Picks up production stream settings as in hlt2_pp_2024.py
        Removes lumi, no_bias, turboraw and passthrough streams
    '''

    streams_to_keep = ['turbo', 'full', 'turcal']

    streams = make_streams()
    streams.streams = [
        stream for stream in streams.streams if stream.name in streams_to_keep
    ]

    #Write out stream configuration to JSON file for use later in the test
    with open(
            fname_helper.stream_config_json_path(stream_config="production"),
            'w') as f:
        json.dump({
            stream.name: [line.name for line in stream.lines]
            for stream in streams.streams
        }, f)

    return streams


options.scheduler_legacy_mode = False

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom()
]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_main_streams, public_tools=public_tools)

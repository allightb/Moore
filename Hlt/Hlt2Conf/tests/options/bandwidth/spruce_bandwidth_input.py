###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configure input files for the Sprucing bandwidth tests

This is needed for the qmtest in Moore.
For the test in LHCbPR, the input files are configured by
$PRCONFIGROOT/python/MooreTests/run_bandwidth_test_jobs.py

If updating, please also update spruce_bandwidth_input.yaml
'''

from Moore import options
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Hlt2Conf.sprucing_settings.fixed_line_configs import lines_for_TISTOS_BW_Aug2023 as lines_for_TISTOS
from RecoConf.calorimeter_reconstruction import make_digits

options.set_input_and_conds_from_testfiledb(
    'upgrade-minbias-hlt2-full-output-Dec2023')
options.input_raw_format = 0.3
options.input_type = "MDF"
options.evt_max = 1000
options.n_threads = 1
make_digits.global_bind(calo_raw_bank=False)

list_of_full_stream_lines.global_bind(lines=lines_for_TISTOS)

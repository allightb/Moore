###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configure input files for the spruce_latest bandwidth tests

This is needed for the qmtest in Moore.
For the test in LHCbPR, the input files are configured by
$PRCONFIGROOT/python/MooreTests/run_bandwidth_test_jobs.py

'''
import json
from Moore import options
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from PRConfig.bandwidth_helpers import FileNameHelper

stream = 'full'
stream_config = 'production'

# From hlt2_bandwidth_input_2024.py
options.dddb_tag = "dddb-20231017"
options.conddb_tag = "sim-20231017-vc-md100"
options.simulation = True
options.input_type = 'MDF'
options.evt_max = 100
options.n_threads = 1
options.input_raw_format = 0.5

fname_helper = FileNameHelper(process="hlt2")
options.input_files = [
    fname_helper.mdfdst_fname_for_Moore(stream_config=stream_config).format(
        stream=stream)
]
options.input_manifest_file = fname_helper.tck(stream_config=stream_config)

with open(
        fname_helper.stream_config_json_path(stream_config=stream_config),
        "r") as f:
    lines_for_TISTOS = json.load(f)[stream]

list_of_full_stream_lines.global_bind(lines=lines_for_TISTOS)

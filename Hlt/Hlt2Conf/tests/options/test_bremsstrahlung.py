###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options to test bremsstrahlung corrections as applied by ParticleWithBremMaker
and FunctionalDiElectronMaker, both relying on SelectiveBremAdder.

Definition of B2JpsiK, with Jpsi2ee, HLT2 lines to test bremsstrahlung corrections.
Three lines are defined for the cases: no bremsstrahlung, bremsstrahlung
applied to individual electrons and bremsstrahlung applied to electron pairs.
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line

from GaudiKernel.SystemOfUnits import MeV, picosecond

from RecoConf.decoders import default_VeloCluster_source
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction,
                                             reconstruction)
from RecoConf.hlt2_global_reco import make_light_reconstruction

from Hlt2Conf.algorithms_thor import ParticleFilter, ParticleCombiner
from RecoConf.event_filters import require_pvs
from PyConf import configurable
from PyConf.Algorithms import ParticleMassMonitor, ParticleMassDTFMonitor

# get the basic particles
from Hlt2Conf.standard_particles import (
    make_long_electrons_with_brem,
    _make_dielectron_with_brem,
    make_detached_dielectron_with_brem,
    make_detached_dielectron,
    make_has_rich_long_kaons,
)

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom

from RecoConf.decoders import default_ft_decoding_version

import Functors as F

###########
# Definition of the line
###########
all_lines = {}


@configurable
def make_B2JpsiK(Jpsi,
                 kaons,
                 pvs,
                 massWind_B=200 * MeV,
                 maxVertexChi2=25,
                 lifetime=0.3 * picosecond):
    """Make the B --> Jpsi K candidate"""

    # mass window around the B mass
    combination_cut = F.require_all(F.ABS_DELTA_MASS('B+') < massWind_B)

    # require the Jpsi and K to come from the same vertex + lifetime cut
    mother_cut = F.require_all(F.CHI2DOF < maxVertexChi2,
                               F.BPVLTIME(pvs) > lifetime)

    return ParticleCombiner([Jpsi, kaons],
                            DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
                            CombinationCut=combination_cut,
                            CompositeCut=mother_cut)


@configurable
def filter_kaons(
        kaons,
        pvs,
        name='filter_kaons_{hash}',
        pidCut=0,
        mipChi2Cut=4,
):
    code = F.require_all(F.PID_K > pidCut, F.MINIPCHI2(pvs) > mipChi2Cut)
    return ParticleFilter(kaons, F.FILTER(code), name=name)


@configurable
def filter_jpsi_ee(dielectron,
                   pvs,
                   name='jpsi_ee_{hash}',
                   massWind_Jpsi=1000 * MeV,
                   minPt_Jpsi=0 * MeV):

    code = F.require_all(
        F.ABS_DELTA_MASS('J/psi(1S)') < massWind_Jpsi, F.PT > minPt_Jpsi)

    return ParticleFilter(dielectron, F.FILTER(code), name=name)


@register_line_builder(all_lines)
def b2jpsik_eeline(name='Hlt2BToJpsiK_JpsiToEE',
                   prescale=1,
                   useDiElectronMaker=False):
    """B+ --> Jpsi K+ line"""

    # get the PVs
    pvs = make_pvs()

    # get detached diElectrons
    if useDiElectronMaker:
        detached_dielectron = make_detached_dielectron_with_brem()
    else:
        detached_dielectron = make_detached_dielectron()

    # filter the detached Jpsi candidates
    Jpsi = filter_jpsi_ee(detached_dielectron, pvs)

    # get the basic kaons
    kaons = make_has_rich_long_kaons()
    filtered_kaons = filter_kaons(kaons, pvs)

    # get the BJpsiK candidates
    B2JpsiK = make_B2JpsiK(
        Jpsi=Jpsi, kaons=filtered_kaons, pvs=pvs, massWind_B=1000 * MeV)

    # mass monitors
    checkJpsiM = ParticleMassMonitor(
        name=name + "JpsiMonitor",
        Particles=Jpsi,
        MassMean=3096.9 * MeV,
        MassSigma=200 * MeV,
        GenerateTuples=False,
        Bins=50,
        ParticleType=["Jpsi", "ep", "em"])
    checkBuM = ParticleMassMonitor(
        name=name + "BMonitor",
        Particles=B2JpsiK,
        MassMean=5279.34 * MeV,
        MassSigma=200 * MeV,
        GenerateTuples=False,
        Bins=20,
        ParticleType=["Bu", "Jpsi", "K"])
    checkBuDTFM = ParticleMassDTFMonitor(
        name=name + "BDTFMonitor",
        histoName="BDTFMass",
        Particles=B2JpsiK,
        MassMean=5279.34 * MeV,
        MassSigma=50 * MeV,
        Bins=40,
        MassConstraints=['J/psi(1S)'])

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), B2JpsiK] +
        [checkJpsiM, checkBuM, checkBuDTFM],
        prescale=prescale,
    )


###########
# Moore options
###########
def all_lines():
    noBremLine = b2jpsik_eeline(name='Hlt2NoBrem')

    bremadder_type = 'SelectiveBremAdder'
    with make_detached_dielectron.bind(
            electron_maker=make_long_electrons_with_brem
    ), make_long_electrons_with_brem.bind(bremadder=bremadder_type):
        singleEBremLine = b2jpsik_eeline(name="Hlt2SingleBrem")

    with _make_dielectron_with_brem.bind(bremadder=bremadder_type):
        diEBremLine = b2jpsik_eeline(
            name="Hlt2DiEBrem", useDiElectronMaker=True)
    return [noBremLine, singleEBremLine, diEBremLine]


public_tools = [stateProvider_with_simplified_geom()]

# ft decoding Version
default_ft_decoding_version.global_bind(value=6)
# Use velo retina decoding:
default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")

options.set_input_and_conds_from_testfiledb('expected-2024_B2JpsiK_ee_MD')
options.input_type = 'ROOT'
options.evt_max = 1000

# run
with reconstruction.bind(from_file=False),\
    make_light_reconstruction.bind(usePatPVFuture=True):
    run_moore(
        options,
        all_lines,
        public_tools=public_tools,
        exclude_incompatible=False)

###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test relations when not filtering MC particles.

See test_relations.py for general details.
"""
import json
import os

from Configurables import ApplicationMgr, LHCbApp
from GaudiConf import IOHelper
import GaudiPython as GP
from Moore.persistence import DEFAULT_OUTPUT_PREFIX
from Moore.persistence.reading import mc_unpackers, unpackers
from Moore.persistence.truth_matching import CHARGED_PP2MC_LOC, NEUTRAL_PP2MC_LOC


def add_tes_root(path, prefix=DEFAULT_OUTPUT_PREFIX):
    """Return `path` prefixed by ROOT_IN_TES."""
    return os.path.join(prefix, path)


def protoparticle_locations_from_particles(particles):
    """Return the set of ProtoParticle locations referenced by an iterable of Particle objects."""
    locs = set()
    for p in particles:
        if p.proto():
            # Charged/neutral basic Particle
            locs.add(p.proto().parent().registry().identifier())
        else:
            # Composite Particle
            for c in p.daughters():
                locs |= protoparticle_locations_from_particles([c])
    return locs


def protoparticle_locations_from_relations(relations):
    """Return the set of ProtoParticle locations referenced by a Relation1DWeighted table."""
    relations = relations.relations()
    return {
        getattr(relations.at(i), "from")().parent().registry().identifier()
        for i in range(relations.size())
    }


IOHelper('ROOT').inputFiles(["hlt2_lines_reco_mix_full_mc.dst"])
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
ApplicationMgr(TopAlg=mc_unpackers(filtered_mc=False) + unpackers())

# Build the list of all Particle locations, based on the outputs of the lines
particle_locations = []
with open("hlt2_lines_reco_mix_full_mc.lines.json") as f:
    lines = json.load(f)
    for line_info in lines:
        line_name = str(line_info["name"])
        extra_output_names = list(map(str, line_info["extra_outputs"]))
        particle_locations.append(
            add_tes_root(os.path.join(line_name, "Particles")))
        for extra_name in extra_output_names:
            particle_locations.append(
                add_tes_root(os.path.join(line_name, extra_name, "Particles")))
# These aren't passed in from the previous test as these locations are part of
# the Moore's output contract; the tables must only be in these fixed locations
relations_locations = list(
    map(add_tes_root, [CHARGED_PP2MC_LOC, NEUTRAL_PP2MC_LOC]))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

while TES["/Event"]:
    # Step 1: Gather ProtoParticle containers by going via Particle objects
    proto_locs_from_particles = set()
    for container in filter(None, map(lambda l: TES[l], particle_locations)):
        proto_locs_from_particles |= protoparticle_locations_from_particles(
            container)
    # Step 2: Gather ProtoParticle containers by going via RelationWeighted1D tables
    proto_locs_from_relations = set()
    for relations in filter(None, map(lambda l: TES[l], relations_locations)):
        proto_locs_from_relations |= protoparticle_locations_from_relations(
            relations)
    # Step 3: Assert the relations tables have relations for all ProtoParticle
    # locations.
    assert proto_locs_from_relations == proto_locs_from_particles
    appMgr.run(1)

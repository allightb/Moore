###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the implementation of v2 particle objects in HLT2
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
    reconstruction,
)
from Hlt2Conf.standard_particles import (
    make_has_rich_long_cb_kaons,
    make_has_rich_long_cb_pions,
)
from RecoConf.core_algorithms import make_unique_id_generator
from PyConf.Algorithms import ChargedBasicsFilter, ThOrCombiner__2ChargedBasics

all_lines = {}


def filter_kaons(particles,
                 pvs,
                 pt_min=0.5 * GeV,
                 mipchi2_min=9,
                 dllk_min=+0.5):
    cut = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K > dllk_min,
    )
    return ChargedBasicsFilter(Input=particles, Cut=F.FILTER(cut)).Output


def filter_pions(particles,
                 pvs,
                 pt_min=0.5 * GeV,
                 mipchi2_min=9,
                 dllk_max=-0.5):
    cut = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
        F.PID_K < dllk_max,
    )
    return ChargedBasicsFilter(Input=particles, Cut=F.FILTER(cut)).Output


def make_kst(kaons,
             pions,
             pvs,
             two_body_comb_maxdocachi2=9.0,
             comb_m_min=700 * MeV,
             comb_m_max=1000 * MeV,
             comb_pt_min=2 * GeV,
             comb_maxdoca=0.1 * mm,
             vchi2pdof_max=10,
             bpvvdchi2_min=25):
    combination_code = F.require_all(
        F.MAXSDOCACHI2 < two_body_comb_maxdocachi2,
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
        F.MAXSDOCA < comb_maxdoca,
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )
    return ThOrCombiner__2ChargedBasics(
        InputUniqueIDGenerator=make_unique_id_generator(),
        Input1=kaons,
        Input2=pions,
        DecayDescriptor="[K*(892)0 -> K+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    ).Output


@register_line_builder(all_lines)
def kst2kpi_line(name="Hlt2Kst2Kpi", prescale=1):
    pvs = make_pvs()
    kaons = filter_kaons(make_has_rich_long_cb_kaons(), pvs)
    pions = filter_pions(make_has_rich_long_cb_pions(), pvs)
    kst = make_kst(kaons, pions, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [kst],
        prescale=prescale,
    )


# Moore configuration
from Moore import options, run_moore

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom
public_tools = [stateProvider_with_simplified_geom()]


def all_lines():
    return [kst2kpi_line()]


with reconstruction.bind(from_file=False):
    run_moore(options, all_lines, public_tools)

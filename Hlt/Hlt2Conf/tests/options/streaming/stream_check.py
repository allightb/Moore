###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if streaming functionality for Hlt2 or Sprucing works as expected.

   For testing HLT2 streaming this test is  using the output of Hlt/Hlt2Conf/tests/qmtest/streaming.qms/test_hlt2_streaming.qmt
   For testing Sprucing streaming this test is using the output of Hlt/Hlt2Conf/tests/qmtest/streaming.qms/test_sprucing_streaming.qmt
"""
import argparse

import GaudiPython as GP
LHCb = GP.gbl.LHCb
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from GaudiConf.reading import do_unpacking
from PyConf.application import configured_ann_svc

from Hlt2Conf.check_output import check_particles, check_decreports
from DDDB.CheckDD4Hep import UseDD4Hep


def error(msg):
    print("CheckOutput ERROR", msg)


def routing_bits(rbbanks):
    """Return a list with the routing bits that are set."""
    (rbbank, ) = rbbanks  # stop if we have multiple rb banks
    d = rbbank.data()
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    ordered = list(map(int, reversed(bits)))
    return [bit for bit, value in enumerate(ordered) if value]


#Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('manifest', help='JSON manifest dump')
parser.add_argument('input_process', help='Hlt2 or Spruce')
parser.add_argument('stream', help='Stream to test as defined in options')

args = parser.parse_args()

#Check job configuration
if not (args.input_process == "Hlt2" or args.input_process == "Spruce"):
    error("input_process not supported")

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
if not UseDD4Hep:
    from Configurables import CondDB
    CondDB(Upgrade=True)

algs = do_unpacking(input_process=args.input_process, stream=args.stream)

mgr = ApplicationMgr(TopAlg=algs)
mgr.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

#Raw event location
if args.input_process == "Hlt2":
    raweventloc = '/Event/DAQ/RawEvent'
else:
    raweventloc = f'/Event/{args.stream}/RawEvent'

first_event = True
while True:
    appMgr.run(1)
    if not TES['/Event']:
        break
    if first_event:
        TES.dump()
        first_event = False

    print("Looking in RawEvent ", raweventloc)

    # Every event must have an ODIN bank
    ODIN_banks = TES[raweventloc].banks(LHCb.RawBank.ODIN)
    if ODIN_banks.size() != 1:
        error("Expected one ODIN bank in this stream")

    if args.input_process == "Hlt2":
        on_bits = routing_bits(TES[raweventloc].banks(
            LHCb.RawBank.HltRoutingBits))
        print(on_bits)
        if 95 not in on_bits:  # this must be a lumi exclusive event
            if 94 not in on_bits:
                error("Bit 94 must be set when 95 is not set")
            # there are no other banks, so nothing more to check
            continue

    # HLT2 reports: 'HltDecReports', 'HltLumiSummary', 'HltRoutingBits'
    # In the test stream A: ODIN', 'VP', 'UT', 'FTCluster', 'Rich', 'EcalPacked', 'HcalPacked', 'Muon'
    # In the test stream B: ODIN', 'VP', 'UT', 'FTCluster', 'EcalPacked', 'HcalPacked', 'Muon' # Rich removed, FTCluster added via RawBank SP

    # Check that RawBank configuration per stream is working
    Rich_banks = TES[raweventloc].banks(LHCb.RawBank.Rich)
    FT_banks = TES[raweventloc].banks(LHCb.RawBank.FTCluster)
    print("Rich_banks ", Rich_banks.size())
    if "A" in args.stream and Rich_banks.size() == 0:
        error("Expected RICH rawbanks in this stream")
    if "B" in args.stream and Rich_banks.size() != 0:
        error("Expected no RICH rawbanks in this stream")
    if "B" in args.stream and FT_banks.size() == 0:
        error("Expected selectively persisted FT rawbanks in this stream")

    # check that only expected raw banks are non-empty
    if "A" in args.stream:
        # RawBank IDs as per LHCb/Event/DAQEvent/include/Event/RawBank.h
        if args.input_process == 'Hlt2':
            saved_banks = {9, 13, 15, 16, 17, 21, 22, 53, 54, 60, 63, 64, 66}
        elif args.input_process == 'Spruce':
            saved_banks = {9, 13, 16, 17, 21, 22, 53, 54, 60, 63, 64, 66}
        else:
            error("Undefined process")
        for i in range(0, 100):
            if i in saved_banks:
                if TES[raweventloc].banks(i).size() == 0:
                    error(
                        f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be non-empty but has a size of {TES[raweventloc].banks(i).size()}"
                    )
            else:
                if TES[raweventloc].banks(i).size() != 0:
                    error(
                        f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be empty but has a size of {TES[raweventloc].banks(i).size()}"
                    )

    if "B" in args.stream:
        # RawBank IDs as per LHCb/Event/DAQEvent/include/Event/RawBank.h
        # Rich bank removed
        if args.input_process == 'Hlt2':
            saved_banks = {13, 15, 16, 17, 21, 22, 53, 54, 60, 63, 64, 66}
        elif args.input_process == 'Spruce':
            saved_banks = {13, 16, 17, 21, 22, 53, 54, 60, 63, 64, 66}
        else:
            error("Undefined process")
        for i in range(0, 100):
            if i in saved_banks:
                if TES[raweventloc].banks(i).size() == 0:
                    error(
                        f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be non-empty but has a size {TES[raweventloc].banks(i).size()}"
                    )
            else:
                if TES[raweventloc].banks(i).size() != 0:
                    error(
                        f"Bank {GP.gbl.LHCb.RawBank.typeName(i)} is expected to be empty but has a size {TES[raweventloc].banks(i).size()}"
                    )

    # Check that routing bits per stream are working
    if args.input_process == "Hlt2":
        if "A" in args.stream:
            if 85 not in on_bits:
                error("Expected bit 85 in stream A")
            if 90 in on_bits:
                error("Did NOT expect bit 90 in stream A")
        elif "B" in args.stream:
            if 90 not in on_bits:
                error("Expected bit 90 in stream B")
            if 85 in on_bits:
                error("Did NOT expect bit 85 in stream B")
        else:
            error("Only streams A and B supported")

    # Check dec reports
    dec_to_check = args.input_process + '_' + args.stream + '_line'

    if args.input_process == 'Hlt2':
        hlt2decisions = check_decreports(TES, decs=[dec_to_check])
        for k, v in hlt2decisions.items():
            if v:
                prefix = '/Event/HLT2/' + k
                check_particles(TES, prefix.removesuffix("Decision"))
    if args.input_process == "Spruce":
        hlt2decisions = check_decreports(
            TES, decs=['Hlt2Topo2Body', 'Hlt2Topo3Body'])
        sprucedecisions = check_decreports(
            TES, decs=[dec_to_check], stage='Spruce')
        for k, v in sprucedecisions.items():
            if v:
                prefix = '/Event/Spruce/' + k
                check_particles(TES, prefix.removesuffix("Decision"))

    # Check rec summary
    if args.input_process == "Hlt2":
        rec_summary = TES["/Event/HLT2/Rec/Summary"]
        print(" getting TES['/Event/HLT2/Rec/Summary']")
    elif args.input_process == "Spruce":
        rec_summary = TES["/Event/Spruce/HLT2/Rec/Summary"]
        print(" getting TES['/Event/Spruce/HLT2/Rec/Summary']")
    else:
        rec_summary = None
        print("its none")
    if not rec_summary:
        error("Could not find RecSummary for this stream")
    if (rec_summary.info(rec_summary.nLongTracks, -1) < 1) or (rec_summary.info(rec_summary.nPVs, -1) < 1)\
        or (rec_summary.info(rec_summary.nFTClusters, -1) < 1):
        error("Wrong values in RecSummary for this stream")
        print(rec_summary)

    appMgr.run(1)

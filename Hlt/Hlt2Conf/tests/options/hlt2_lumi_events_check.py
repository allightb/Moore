###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if Hlt2LuminosityLine line streaming functionality for Hlt2

TODO the validation of this test for the LUMI stream case should check
that the counter values make sense by comparing to the output of the
hlt2_lumi_events test. This is not trivial as the encoding of some counters
is lossy.

"""
import argparse
import os
import sys
from pathlib import Path

from GaudiConf import IOExtension
from Configurables import LHCbApp, IODataManager, HltLumiSummaryMonitor

parser = argparse.ArgumentParser()
parser.add_argument('input', help='Input filename')
parser.add_argument('--lumi-stream', action="store_true", help='Lumi stream')
parser.add_argument('--dir', default="", help='Directory containing the input')

args = parser.parse_args()
args.input = str(Path(os.path.expandvars(args.dir)) / args.input)


def error(msg):
    print("Check ERROR", msg)


# FIXME which part of LHCbApp is actually needed?
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    #DDDBtag="master",
    #CondDBtag="2022_12_HLT2Processing",
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)

if args.lumi_stream:
    from Configurables import LHCb__UnpackRawEvent, LHCb__SelectViewForHltSourceID, createODIN, ApplicationMgr, HltLumiSummaryDecoder
    unpacker = LHCb__UnpackRawEvent("UnpackRawEvent")
    unpacker.RawBankLocations = [
        "DAQ/RawBanks/HltLumiSummary", "DAQ/RawBanks/ODIN"
    ]
    unpacker.BankTypes = ["HltLumiSummary", "ODIN"]

    decodeODIN = createODIN(RawBanks="DAQ/RawBanks/ODIN")

    sel1 = LHCb__SelectViewForHltSourceID(
        "SelectHlt1Lumi",
        Input="DAQ/RawBanks/HltLumiSummary",
        SourceID="Hlt1",
        Output="DAQ/RawBanks/Hlt1LumiSummary")
    sel2 = LHCb__SelectViewForHltSourceID(
        "SelectHlt2Lumi",
        Input="DAQ/RawBanks/HltLumiSummary",
        SourceID="Hlt2",
        Output="DAQ/RawBanks/Hlt2LumiSummary")
    dec1 = HltLumiSummaryDecoder(
        "DecodeHlt1Lumi",
        RawBanks="DAQ/RawBanks/Hlt1LumiSummary",
        OutputContainerName="Hlt1LumiSummary")
    dec2 = HltLumiSummaryDecoder(
        "DecodeHlt2Lumi",
        RawBanks="DAQ/RawBanks/Hlt2LumiSummary",
        OutputContainerName="Hlt2LumiSummary")

    from PyConf.application import configured_ann_svc

    from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
    from Configurables import Gaudi__Monitoring__JSONSink as JSONSink

    ApplicationMgr().TopAlg = [
        unpacker,
        decodeODIN,
        sel1,
        sel2,
        dec1,
        dec2,
        # HltLumiSummaryMonitor(name="Hlt1LumiSummaryMonitor", Input="Hlt1LumiSummary", ODIN="/Event/DAQ/ODIN", OutputLevel=1),
        HltLumiSummaryMonitor(
            name="Hlt2LumiSummaryMonitor",
            Input="Hlt2LumiSummary",
            ODIN="/Event/DAQ/ODIN",
            OutputLevel=1,
        ),
    ]
    ApplicationMgr().HistogramPersistency = "ROOT"
    ApplicationMgr().ExtSvc = [
        configured_ann_svc(),
        MessageSvcSink(),
        JSONSink(
            FileName="error_monitoring.json",
            NamesToSave=[
                ".*errorsPerBankLocation",
                ".*ErrorsPerErrorBankType",
            ],
        ),
    ]

import GaudiPython as GP
LHCb = GP.gbl.LHCb
RawBank = LHCb.RawBank


def events(appMgr):
    TES = appMgr.evtsvc()
    appMgr.run(1)
    while TES["/Event"]:
        yield TES
        appMgr.run(1)


def test_equals(a, b, message="Values are not equal"):
    if a != b:
        error(f"FAILED: {a!r} == {b!r}: {message}")


def get_hlt_source_id(bank):
    return (bank.sourceID() >> 8) & 0b111


def select_hlt_banks(banks, source):
    return [b for b in banks if get_hlt_source_id(b) == source]


def routing_bits(rbbanks):
    """Return a list with the routing bits that are set."""
    (rbbank, ) = rbbanks  # stop if we have multiple rb banks
    d = rbbank.data()
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    ordered = list(map(int, reversed(bits)))
    return [bit for bit, value in enumerate(ordered) if value]


for TES in events(GP.AppMgr()):
    raw_event = TES["/Event/DAQ/RawEvent"]

    # TODO also check the bank versions

    test_equals(
        raw_event.banks(RawBank.DAQ).size(), 1,
        "Every raw event must have exactly one DAQ (MDF header) bank")

    test_equals(
        raw_event.banks(RawBank.ODIN).size(), 1,
        "Every raw event must have exactly one ODIN bank")

    test_equals(
        raw_event.banks(RawBank.HltRoutingBits).size(), 1,
        "Every raw event must have exactly one HltRoutingBits bank")

    rb_bank, = raw_event.banks(RawBank.HltRoutingBits)
    assert get_hlt_source_id(rb_bank) == 2

    on_bits = routing_bits(raw_event.banks(RawBank.HltRoutingBits))
    if 95 not in on_bits:
        # this is not a "content event", so it must be a nanofied event
        # and we don't have any other banks.
        if 94 not in on_bits:
            error("Bit 94 must be set when 95 is not set")
        continue

    hlt1_dec_reports_bank, = select_hlt_banks(
        raw_event.banks(RawBank.HltDecReports), 1)
    hlt2_dec_reports_bank, = select_hlt_banks(
        raw_event.banks(RawBank.HltDecReports), 2)
    assert raw_event.banks(RawBank.HltDecReports).size() == 2

    hlt1_sel_reports_banks = select_hlt_banks(
        raw_event.banks(RawBank.HltSelReports), 1)
    assert len(hlt1_sel_reports_banks) <= 1
    # TODO check for the presence of some banks overall
    #      check that when one of a set of lines fired, we always have the sel reports
    # if 14 in on_bits:
    #     assert len(hlt1_sel_reports_banks) == 1

    hlt2_sel_reports_banks = select_hlt_banks(
        raw_event.banks(RawBank.HltSelReports), 2)
    test_equals(
        len(hlt2_sel_reports_banks), 0,
        "There should be no HLT2 HltSelReports banks")

    if not args.lumi_stream:
        test_equals(
            raw_event.banks(RawBank.HltLumiSummary).size(), 0,
            "There should be no HltLumiSummary banks")
    else:
        hlt1_lumi_banks = select_hlt_banks(
            raw_event.banks(RawBank.HltLumiSummary), 1)
        hlt2_lumi_banks = select_hlt_banks(
            raw_event.banks(RawBank.HltLumiSummary), 2)
        assert len(hlt1_lumi_banks) <= 1
        assert len(hlt2_lumi_banks) <= 1
        test_equals(
            len(hlt1_lumi_banks), len(hlt2_lumi_banks),
            "There should be equal number of HLT1 and HLT2 HltLumiSummary banks"
        )

    sys.stdout.flush()

###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test exclusive and passthrough (Turbo) Sprucing output.

Can check if RawBanks are present and not present
Find number->RawBank info at https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h

./run python Hlt/Hlt2Conf/tests/options/sprucing/spruce_check.py -i spruce_example_realtimereco.test.dst -t spruce_example_realtime.tck.json
-p Spruce -s test -rb 9 21 -notrb 13 73
"""
import argparse

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from GaudiConf.reading import do_unpacking
from PyConf.application import configured_ann_svc
from Hlt2Conf.check_output import (
    check_MCoutput,
    check_banks,
    check_not_banks,
    check_particles,
    check_decreports,
    check_hlt2_topo_candidates,
)
from DDDB.CheckDD4Hep import UseDD4Hep

parser = argparse.ArgumentParser()
parser.add_argument('-i', type=str, help='Input MDF or DST')
parser.add_argument('-t', type=str, help='.tck.json file from the job')
parser.add_argument(
    '-p', type=str, help='input_process can be Spruce or Turbo')
parser.add_argument('-s', type=str, help='Stream to test')
parser.add_argument(
    '-n', type=int, required=False, help='Number of events to check')
parser.add_argument('-rb', type=int, nargs='+', help='RawBanks to check')
parser.add_argument(
    '-notrb', type=int, nargs='+', help='RawBanks to check are NOT in output')
parser.add_argument(
    '-topo',
    action="store_true",
    help='Check that Topo candidates from HLT2 are present')

args = parser.parse_args()

assert args.p == "Spruce" or args.p == "Turbo", 'input_process is Turbo (passthrough sprucing) or Spruce'

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
if not UseDD4Hep:
    from Configurables import CondDB
    CondDB(Upgrade=True)

input_process = args.p
stream = args.s

if input_process == "Spruce":
    RECO_ROOT = "/Event/Spruce/HLT2"
    dec_to_check = "Spruce_Test_line"

if input_process == "Turbo":
    RECO_ROOT = "/Event/HLT2"
    dec_to_check = "Passthrough"

algs = do_unpacking(
    input_process=input_process,
    stream=stream,
    simulation=True,
    raw_event_format=0.3)

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc(json_file=args.t)]

IOExtension().inputFiles([args.i], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

#from Configurables import MessageSvc
#MessageSvc().setDebug = ['UnpackDstDataBank']

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = args.n if args.n else 1

for ii in range(nevents):
    print('Checking next event.')
    appMgr.run(1)
    if not TES['/Event']:
        break
    if ii == 0:
        TES.dump()

    hlt2reportloc = TES['/Event/Hlt2/DecReports']
    hlt2_decs_to_check = hlt2reportloc.decReports().keys()
    ##Note this ^^ will not check streaming only that you can access hlt2 decisions
    print("hlt2_decs_to_check ", hlt2_decs_to_check)

    # Check dec reports
    if input_process == 'Turbo':
        decisions = check_decreports(TES, decs=hlt2_decs_to_check)
        check_decreports(TES, decs=[dec_to_check], stage='Spruce')
    elif input_process == 'Spruce':
        decisions = check_decreports(TES, decs=[dec_to_check], stage='Spruce')
        check_decreports(TES, decs=hlt2_decs_to_check)

    # Check particles and relations between them
    if input_process == 'Turbo':
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = '/Event/HLT2/' + k
                check_particles(TES, prefix.removesuffix("Decision"))
    elif input_process == 'Spruce':
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = '/Event/Spruce/' + k
                check_particles(TES, prefix.removesuffix("Decision"))

    # Check MC locations if simulation like
    if "_dstinput" in args.i:
        check_MCoutput(TES, RECO_ROOT)

    #Check rawbanks specific to this stream
    print("args.rb ", args.rb)

    if args.rb:
        check_banks(TES, args.s, banks=args.rb)

    print("args.notrb ", args.notrb)

    if args.notrb:
        check_not_banks(TES, args.s, banks=args.notrb)

    if args.topo:
        check_hlt2_topo_candidates(TES)

###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test sprucing over 2022 data.

"""
from Hlt2Conf.check_output import (
    check_particles,
    check_decreports,
    check_decreports_regex,
    check_banks,
    check_not_banks,
)
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
spruce_b2oc_lines.pop('SpruceB2OC_BdToDsmK_DsmToHHH_FEST')
from Hlt2Conf.sprucing_settings.Sprucing_2022_1_production import fulllinedict, turbolinedict, turcallinedict
from PyConf.application import configured_ann_svc
from GaudiConf.reading import do_unpacking
from Configurables import (
    ApplicationMgr,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)
from GaudiConf import IOExtension
import argparse

import cppyy

import GaudiPython as GP
LHCb = GP.gbl.LHCb


def error(msg):
    print("CheckOutput ERROR", msg)


def spruce_lines_running(wg):
    return [item for item in list(fulllinedict[wg].keys())]


# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-input', type=str, required=True, help='Input filename')
parser.add_argument(
    '-manifest', type=str, required=False, help='JSON manifest dump')
parser.add_argument('-job_type', type=str, required=True, help='excl or pass')
parser.add_argument(
    '-stream',
    type=str,
    required=True,
    help='Stream to test as defined in options')
parser.add_argument(
    '-rb_to_check',
    type=int,
    nargs='+',
    required=False,
    help='RawBanks to check are in output')
parser.add_argument(
    '-rb_to_check_not',
    type=int,
    nargs='+',
    required=False,
    help='RawBanks to check are NOT in output')

args = parser.parse_args()

assert args.job_type == "excl" or args.job_type == "pass" or args.job_type == "pass_turcal", 'job_type is "excl" or "pass" (passthrough sprucing) or "pass_turcal" (special version of passthrough sprucing)'

print("input ", args.input)
print("manifest ", args.manifest)
print("job_type ", args.job_type)
print("stream ", args.stream)
print("rb_to_check ", args.rb_to_check)
print("rb_to_check_not ", args.rb_to_check_not)

# Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=False,
)

# settings for passthrough data. Only one fired line of first evt is checked in a test.
process = "Spruce" if args.job_type == "excl" else "Turbo"

algs = do_unpacking(
    input_process=process,
    stream=args.stream,
    simulation=False,
    raw_event_format=0.3)

mgr = ApplicationMgr(TopAlg=algs)
mgr.ExtSvc += [configured_ann_svc(json_file=args.manifest)]

IOExtension().inputFiles([args.input], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

raweventloc = f'/Event/{args.stream}/RawEvent'

appMgr.run(1)
TES.dump()
nevt = 5
for i in range(nevt):
    print('Checking next event.')
    if not TES['/Event']:
        break

    # Check HLT2 DstData bank is persisted through the Sprucing, together with checks of other banks
    #RawBank numbers can be found here: https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h
    if args.job_type == "excl" or args.job_type == "pass":
        dstdata_banks = TES[raweventloc].banks(LHCb.RawBank.DstData)
        print("DstData bank ", dstdata_banks.size())
        if dstdata_banks.size() == 0:
            error("Expected number of DstData rawbanks > 0")
        hltdecrepo_banks = TES[raweventloc].banks(LHCb.RawBank.HltDecReports)
        print("HltDecReports bank ", hltdecrepo_banks.size())
        if hltdecrepo_banks.size() != 3:
            error("Expected number of HltDecReports rawbanks == 3")
        hltselrepo_banks = TES[raweventloc].banks(LHCb.RawBank.HltSelReports)
        print("HltSelReports bank ", hltselrepo_banks.size())
        if hltselrepo_banks.size() != 1:
            error("Expected number of HltSelReports rawbanks == 1")
        odin_banks = TES[raweventloc].banks(LHCb.RawBank.ODIN)
        print("ODIN bank ", odin_banks.size())
        if odin_banks.size() != 1:
            error("Expected exactly 1 ODIN rawbanks")

    else:
        #Check rawbanks specific to this stream
        if args.rb_to_check:
            check_banks(TES, args.stream, banks=args.rb_to_check)
        if args.rb_to_check_not:
            check_not_banks(TES, args.stream, banks=args.rb_to_check_not)
    '''
    # for future use
    hltlumisum_banks = TES[raweventloc].banks(LHCb.RawBank.HltLumiSummary)
    print("HltLumiSummary bank ", hltlumisum_banks.size())
    hltroutingbits_banks = TES[raweventloc].banks(LHCb.RawBank.HltRoutingBits)
    print("HltRoutingBits bank ", hltroutingbits_banks.size())
    '''

    # Check dec reports
    if args.job_type == "excl":
        dec_to_check = spruce_lines_running(args.stream)
    elif args.job_type == "pass":
        dec_to_check = turbolinedict[args.stream]
        pass_dec_check = ["Pass" + args.stream]
    else:
        dec_to_check = turcallinedict[args.stream]
        pass_dec_check = ["Pass" + args.stream]

    if args.job_type == 'pass' or args.job_type == 'pass_turcal':
        hlt2_fired = check_decreports_regex(TES, regdecs=dec_to_check)
        print("Hlt2 Fired ", hlt2_fired)
        # the particles checks reimplemented
        for k in hlt2_fired:
            prefix = '/Event/HLT2/' + k
            check_particles(TES, prefix.removesuffix("Decision"))
        sprucedecisions = check_decreports(
            TES, decs=pass_dec_check, stage='Spruce')
        passfired = [k for k, v in sprucedecisions.items() if v]
        print("spruce Fired ", passfired)
    else:
        sprucedecisions = check_decreports(
            TES, decs=dec_to_check, stage='Spruce')
        sprucefired = [k for k, v in sprucedecisions.items() if v]
        print("Fired ", sprucefired)
        for k in sprucefired:
            prefix = '/Event/Spruce/' + k
            check_particles(TES, prefix.removesuffix("Decision"))
    appMgr.run(1)

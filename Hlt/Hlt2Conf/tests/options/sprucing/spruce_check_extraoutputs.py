###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test Sprucing output including LongTracks, calo_clusters, calo_digits, pv_tracks ('extra_ouputs').

"""
import argparse

import GaudiPython as GP
from GaudiConf import IOExtension
from Configurables import (
    ApplicationMgr,
    CondDB,
    LHCbApp,
    IODataManager,
    HistogramPersistencySvc,
)

from PyConf.application import configured_ann_svc
from PyConf.packing import default_persistable_locations
from GaudiConf.reading import do_unpacking

from Hlt2Conf.check_output import (
    check_banks,
    check_particles,
    check_decreports,
)


def error(msg):
    print("CheckOutput ERROR", msg)


parser = argparse.ArgumentParser()
parser.add_argument('i', type=str, help='Input MDF or DST')
parser.add_argument('t', type=str, help='.tck.json file from the job')

args = parser.parse_args()

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20171126",
    CondDBtag="sim-20171127-vc-md100",
)
CondDB(Upgrade=True)

input_process = "Spruce"
stream = 'default'
dec_to_check = "Spruce_Test_line_extraoutputs"

algs = do_unpacking(input_process=input_process, raw_event_format=0.3)

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc()]

IOExtension().inputFiles([args.i], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy
cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

# needed to check if we didn't find something in TES
not_found = cppyy.bind_object(0, cppyy.gbl.DataObject)

nevents = 1
for ii in range(nevents):
    print('Checking next event.')
    appMgr.run(1)
    if not TES['/Event']:
        break
    if ii == 0:
        TES.dump()

    # Check dec reports
    hlt2decisions = check_decreports(
        TES, decs=['Hlt2_Topo2Body_with_extras', 'Hlt2_Topo3Body_with_extras'])
    sprucedecisions = check_decreports(
        TES, decs=[dec_to_check], stage='Spruce')

    for k, v in sprucedecisions.items():
        if v:
            prefix = '/Event/Spruce/' + k
            check_particles(TES, prefix.removesuffix("Decision"))

    # Check persistency of packed containers
    locations = default_persistable_locations(stream="/Event/Spruce/HLT2")

    # Specific check of logic for extra_outputs
    longtracks_loc = '/Event/Spruce/Spruce_Test_line_extraoutputs/LongTracks/Particles'
    mothers_LT = TES[longtracks_loc].size()
    if not mothers_LT or mothers_LT == 0:
        error("extra_outputs not being saved correctly.")
    print(longtracks_loc, " extra_outputs size is ", mothers_LT)

    # Check the Rich (=9) RawBank is present
    check_banks(TES, stream, banks=[9])

    # Check of calo_clusters, calo_digits, pv_tracks in sprucing output
    calo = TES["/Event/Spruce/HLT2/Rec/Calo/Electrons"]
    if not calo:
        error("Calo info not being saved correctly.")
    else:
        calo_clusters_size = []
        calo_digits_size = []
        for calo_hypo in calo:
            if not calo_hypo.clusters():
                error("Calo clusters not being saved correctly.")
                break
            elif calo_hypo.clusters().size() == 0:
                error("Calo clusters containers is empty.")
                break
            else:
                calo_clusters_size.append(calo_hypo.clusters().size())
        print("Calo_clusters size is ", calo_clusters_size)
        for calo_hypo in calo:
            if not calo_hypo.digits():
                error("Calo digits not being saved correctly.")
                break
            elif calo_hypo.digits().size() == 0:
                error("Calo digits containers is empty.")
                break
            else:
                calo_digits_size.append(calo_hypo.digits().size())
        print("Calo_digits size is ", calo_digits_size)

    pv = TES["/Event/Spruce/HLT2/Rec/Vertex/Primary"]
    if not pv:
        error("pv info not being saved correctly.")
    else:
        pv_tracks_size = []
        for pv_hypo in pv:
            if not pv_hypo.tracks():
                error("pv tracks not being saved correctly.")
                break
            elif pv_hypo.tracks().size() == 0:
                error("PV tracks containers is empty.")
                break
            else:
                pv_tracks_size.append(pv_hypo.tracks().size())
        print("pv_tracks size is ", pv_tracks_size)

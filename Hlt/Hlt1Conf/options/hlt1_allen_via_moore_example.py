###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore.config import run_allen
from Moore.options import options
from RecoConf.hlt1_allen import allen_gaudi_config as allen_sequence
from RecoConf.decoders import default_ft_decoding_version

default_ft_decoding_version.global_bind(value=4)
options.set_input_and_conds_from_testfiledb(
    "MiniBrunel_2018_MinBias_FTv4_DIGI")
options.evt_max = 100
options.output_file = 'HLT1.dst'
options.output_type = 'ROOT'

with allen_sequence.bind(sequence="hlt1_pp_forward_then_matching_no_ut"):
    run_allen(options)

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for Allen-x86 comparison.

See :func:`~Hlt1Conf.settings.comparison_lines`.

"""
from Moore import options, run_moore
from Hlt1Conf.settings import comparison_lines

options.scheduler_legacy_mode = False
run_moore(options, comparison_lines)

<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Make sure the HLT1 luminosity and minimum bias lines accept the same number of
events as there are respective bits set by the ODIN emulator.

For a moment, HLT1 luminosity line is excluded, so its test is removed
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/default_input_and_conds_hlt1.py</text>
  <text>$HLT1CONFROOT/options/hlt1_lumi_minbias.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
from Moore.qmtest.exclusions import remove_known_warnings
from PyConf.components import unique_name_ext_re

countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

# Find expected counts, i.e. the number of events with the bit set by the ODIN emulator
m = re.search(r'ODINEmulator' + unique_name_ext_re() + ' .* #accept\(4,3\): .* Sum=(\d+)', stdout)
nnobias_exp = m.group(1) if m else 0

# Find actual counts, i.e. the number of events selected by lines
m = re.search(r'\s+LAZY_AND: Hlt1NoBias .* Sum=(\d+)', stdout)
nnobias = m.group(1) if m else 0

# Compare expected to actual
if nnobias != nnobias_exp or nnobias == 0:
    causes.append('expected {} NoBias decisions, found {}'.format(nnobias_exp, nnobias))

</text></argument>
</extension>

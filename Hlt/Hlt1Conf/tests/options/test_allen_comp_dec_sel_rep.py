###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check the consistency of the line decisions and presence of SelReport

Takes two outputs from previously-run jobs:
  1. The MDF
  2. The TCK in TCKData/config.cdb
For each line with a decision in the DecReport, the presence of a
corresponding SelReport is checked.
Two error conditions are recognized and considered failures of the test:
  - A positive line decision without a corresponding SelReport
  - A SelReport for a line with a negative decision
"""
import argparse
from collections import defaultdict
from Configurables import (ApplicationMgr, HistogramPersistencySvc,
                           IODataManager, LHCbApp)
from GaudiConf import IOHelper
import GaudiPython
from PyConf.application import configured_ann_svc

parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
args = parser.parse_args()

# Configure basic application with inputs
LHCbApp(DataType="Upgrade", Simulation=True)
IOHelper("MDF").inputFiles([args.input_mdf])
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)
# Decode Hlt DecReports
from Configurables import LHCb__UnpackRawEvent, HltDecReportsDecoder, HltSelReportsDecoder
unpacker = LHCb__UnpackRawEvent(
    "UnpackRawEvent",
    OutputLevel=2,
    RawBankLocations=[
        "DAQ/RawBanks/HltDecReports", "DAQ/RawBanks/HltSelReports"
    ],
    BankTypes=["HltDecReports", "HltSelReports"])

decDec = HltDecReportsDecoder(
    "HltDecReportsDecoder/Hlt1DecReportsDecoder",
    SourceID="Hlt1",
    RawBanks=unpacker.RawBankLocations[0])

selDec = HltSelReportsDecoder(
    "HltSelReportsDecoder/Hlt1SelReportsDecoder",
    SourceID="Hlt1",
    DecReports=unpacker.RawBankLocations[0],
    RawBanks=unpacker.RawBankLocations[1])

app = ApplicationMgr(
    TopAlg=[unpacker, decDec, selDec],
    ExtSvc=[configured_ann_svc(name='HltANNSvc')])

# decoderDB wants TCKANNSvc as name...
# Set up counters for recording decisions and selreport existence from MDF
counts_from_mdf = defaultdict(lambda: defaultdict(int))

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()
gaudi.run(1)

error = False
while TES["/Event"]:
    decs = TES[str(decDec.OutputHltDecReportsLocation)]
    if not decs:
        print("DecReports TES location not found")
        error = True
        break

    sels = TES[str(selDec.OutputHltSelReportsLocation)]
    if not sels:
        print("SelReports TES location not found")
        error = True
        break

    for key in decs.decisionNames():
        report = decs.decReport(key)
        decision = report.decision()
        hassel = sels.hasSelectionName(key)
        counts_from_mdf[key][(decision, hassel)] += 1

    gaudi.run(1)

# Check for any inconsistent instances:
missing_sel = (1, False)  # Event accepted by line without a SelReport
extra_sel = (0, True)  # SelReport for a line that did not accept the event
for key in counts_from_mdf.keys():
    counters = counts_from_mdf[key]
    line_name = key
    if missing_sel in counters.keys():
        error = True
        print("Test ERROR: Missing SelReport for {}".format(line_name))
    if extra_sel in counters.keys():
        error = True
        print("Test ERROR: Spurious SelReport for {}".format(line_name))

    print("Coincidence counts for {}:  {}".format(line_name, counters))

if error:
    exit("Test failed")  # exit with a non-zero code

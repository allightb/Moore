###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Read a DST file created by the `hlt1_dst_output.py` options."""
from Moore import options

options.input_files = ['test_allen_hlt1_persistence_dst_write.dst']
options.input_type = 'ROOT'
options.evt_max = -1

###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore.options import options
from Moore.config import allen_control_flow
from RecoConf.hlt1_allen import allen_gaudi_config
from PyConf.application import configure_input, configure
from AllenConf.hlt1_muon_lines import make_one_muon_track_line
from AllenConf.hlt1_calibration_lines import make_passthrough_line
from AllenCore.gaudi_allen_generator import allen_runtime_options

config = configure_input(options)

output_type = os.path.splitext(options.output_file)[1][1:]

with (allen_gaudi_config.bind(sequence="hlt1_pp_no_ut"),
      make_one_muon_track_line.bind(pre_scaler=0.01),
      allen_runtime_options.bind(
          filename=f"allen_write_{output_type}_monitor.root"),
      make_passthrough_line.bind(pre_scaler=0.01)):
    allen_node = allen_control_flow(options)

    config.update(configure(options, allen_node))

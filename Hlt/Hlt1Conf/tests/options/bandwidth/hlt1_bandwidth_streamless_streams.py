###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test option for the Hlt1 bandwidth test in LHCbPR

The streaming configuration in this test is `streamless`,
which means one stream.

To launch it in Moore, run with
    ./run gaudirun.py path/to/hlt1_bandwidth_input.py path/to/hlt1_bandwidth_streamless_streams.py
'''
from Moore.options import options
from Moore.config import allen_control_flow
from RecoConf.hlt1_allen import allen_gaudi_config, get_allen_line_names
from AllenCore.gaudi_allen_generator import allen_runtime_options
from PyConf.application import configure_input, configure
from PRConfig.bandwidth_helpers import FileNameHelper
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import DDDBConf
import json

fname_helper = FileNameHelper(process="hlt1")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(
    stream_config="streamless", ext=".mdf").format(stream="streamless")
options.output_type = 'MDF'
options.output_manifest_file = fname_helper.tck(stream_config="streamless")
options.use_iosvc = False

if UseDD4Hep:
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

config = configure_input(options)
with (allen_gaudi_config.bind(sequence="hlt1_pp_forward_then_matching_no_ut"),
      allen_runtime_options.bind(
          filename="allen_write_hlt1_bandwidth_test_monitor.root")):
    line_names = get_allen_line_names()
    allen_node = allen_control_flow(options)
    config.update(configure(options, allen_node))

# Write out stream configuration to JSON file for use later in the test
with open(fname_helper.stream_config_json_path('streamless'), 'w') as f:
    hlt1_linenames = [
        linename.replace("Decision", "") for linename in line_names
    ]
    json.dump({"streamless": hlt1_linenames}, f)

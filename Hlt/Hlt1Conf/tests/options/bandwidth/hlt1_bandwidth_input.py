###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configure input files for the Hlt1 bandwidth tests

This is needed for the qmtest in Moore.
For the test in LHCbPR, the input files are configured by
$PRCONFIGROOT/python/MooreTests/run_bandwidth_test_jobs.py

If updating, please also update hlt1_bandwidth_input.yaml
'''

from Moore import options
options.set_input_and_conds_from_testfiledb("expected_2024_min_bias_mdf")
options.input_raw_format = 0.5
options.evt_max = 100
options.n_threads = 1

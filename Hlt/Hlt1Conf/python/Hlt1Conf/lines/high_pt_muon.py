###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from Moore.lines import Hlt1Line
from RecoConf.event_filters import require_gec
from RecoConf.legacy_rec_hlt1_tracking import make_fitted_tracks_with_muon_id
from ..algorithms import Filter

from Functors import P, PT, ISMUON


@configurable
def track_muon_prefilters():
    return [require_gec()]


@configurable
def one_track_muon_highpt_line(
        name='Hlt1SingleHighPtMuon',
        prescale=1,
        make_input_tracks=make_fitted_tracks_with_muon_id,
        min_pt=6 * GeV,
        min_p=6 * GeV):
    pre_sel = (ISMUON) & (PT > min_pt) & (P > min_p)
    full_sel = pre_sel
    tracks_with_muon_id = make_input_tracks()
    track_filter = Filter(tracks_with_muon_id,
                          full_sel)['PrFittedForwardWithMuonID']
    return Hlt1Line(
        name=name,
        algs=track_muon_prefilters() + [track_filter],
        prescale=prescale)

###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math
from GaudiKernel.SystemOfUnits import MeV, GeV
from PyConf import configurable
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.legacy_rec_hlt1_tracking import (
    make_pvs, make_legacy_rec_hlt1_tracks, make_legacy_rec_hlt1_fitted_tracks,
    make_fitted_forward_tracks_with_pv_relations)
from ..algorithms import (CombineTracks, Filter)

from Moore.lines import Hlt1Line

from Functors import (
    BPVDIRA,
    BPVCORRM,
    BPVETA,
    BPVFDCHI2,
    BPVIPCHI2,
    CHI2DOF,
    COMB,
    MAXSDOCACHI2,
    MVA,
    P,
    PT,
    SUM,
)
import Functors.math as fmath


def make_tracks_mva_tracks():
    tracks = make_legacy_rec_hlt1_fitted_tracks(make_legacy_rec_hlt1_tracks())
    return make_fitted_forward_tracks_with_pv_relations(tracks)


@configurable
def track_mva_prefilters(pvs):
    return [require_gec(), require_pvs(pvs)]


@configurable
def one_track_mva_line(
        name='Hlt1TrackMVA',
        prescale=1,
        make_input_tracks=make_tracks_mva_tracks,
        make_pvs=make_pvs,
        # TrackMVALoose cuts from ZombieMoore
        max_chi2dof=2.5,
        min_pt=2.0 * GeV,
        max_pt=26 * GeV,
        min_ipchi2=7.4,
        param1=1.0,
        param2=2.0,
        param3=1.248):
    pvs = make_pvs()
    pre_sel = (CHI2DOF < max_chi2dof)
    hard_sel = (PT > max_pt) & (BPVIPCHI2(pvs) > min_ipchi2)
    bulk_sel = fmath.in_range(min_pt, PT, max_pt) & (
        fmath.log(BPVIPCHI2(pvs)) >
        (param1 / ((PT / GeV - param2) * (PT / GeV - param2)) +
         (param3 / max_pt) * (max_pt - PT) + math.log(min_ipchi2)))
    full_sel = pre_sel & (hard_sel | bulk_sel)
    track_filter = Filter(make_input_tracks(),
                          full_sel)['PrFittedForwardWithPVs']
    return Hlt1Line(
        name=name,
        algs=track_mva_prefilters(pvs) + [track_filter],
        prescale=prescale)


@configurable
def two_track_mva_line(name='Hlt1TwoTrackMVA',
                       prescale=1,
                       make_input_tracks=make_tracks_mva_tracks,
                       make_pvs=make_pvs):
    # This corresponds to TwoTrackMVALoose in ZombieMoore
    pvs = make_pvs()
    ChildCut = ((PT > 800. * MeV) & (P > 5. * GeV) & (CHI2DOF < 2.5) &
                (BPVIPCHI2(pvs) > 4.))
    # returns a dict of different types
    children = Filter(make_input_tracks(), ChildCut)['PrFittedForwardWithPVs']
    CombinationCut = (PT > 2. * GeV) & (MAXSDOCACHI2 < 10.)

    def apply_to_combination(functor):
        """Helper for applying 'Combination' cuts to a composite."""
        return COMB(Functor=functor, ChildContainers=[children])

    VertexCut = ((CHI2DOF < 10.) & (BPVDIRA(pvs) > 0) & (BPVCORRM(
        Vertices=pvs) > 1. * GeV) & fmath.in_range(2., BPVETA(pvs), 5.) & (MVA(
            MVAType='MatrixNet',
            Config={'MatrixnetFile': "paramfile://data/Hlt1TwoTrackMVA.mx"},
            Inputs={
                'chi2': CHI2DOF,
                'fdchi2': BPVFDCHI2(pvs),
                'sumpt': apply_to_combination(SUM(PT)),
                'nlt16': apply_to_combination(SUM(BPVIPCHI2(pvs) < 16.)),
            }) > 0.96))

    combination_filter = CombineTracks(
        AssumedMass='pi+',
        CombinationCut=CombinationCut,
        InputTracks=children,
        NBodies=2,
        VertexCut=VertexCut)
    return Hlt1Line(
        name=name,
        algs=track_mva_prefilters(pvs) + [combination_filter],
        prescale=prescale)
